#!/usr/bin/python
import subprocess

X = 7
Y = 4
grid = [[0 for x in range(2)] for x in range(X*Y)]
count = 0
for i in range(X):
    for j in range(Y):
        grid[count][0] = i
        grid[count][1] = j
        count += 1

# Adding theoretical unreachable corners
#for i in range(4):
#    grid.append([X+i,Y+i])

def work(b,d,o1,j):
    threads = []
    for k in range(j+1,len(grid)):
        if k != b:
            #arg = ["prism" , "uav_sim_mdp.prism" , "-s" , "uav_mdp.props" , "-const" , "Basex=%d,Basey=%d,Depotx=%d,Depoty=%d,Objx1=%d,Objy1=%d,Objx2=%d,Objy2=%d,"%(grid[b][0], grid[b][1], d[0], d[1], o1[0], o1[1], grid[k][0], grid[k][1])]
            arg = ["/home/ruth/prism-4.3-linux64/bin/prism" , "uav_sim_mdp.prism" , "-s" , "uav_mdp.props" , "-const" , "Basex=%d,Basey=%d,Depotx=%d,Depoty=%d,Objx1=%d,Objy1=%d,Objx2=%d,Objy2=%d,"%(grid[b][0], grid[b][1], d[0], d[1], o1[0], o1[1], grid[k][0], grid[k][1])]
            outfile = open("result-%d%d-%d%d-%d%d-%d%d.txt"%(grid[b][0], grid[b][1], d[0], d[1], o1[0], o1[1], grid[k][0], grid[k][1]), "w")
            threads.append(subprocess.Popen(arg,stdout=outfile))

    for p in threads:
        p.wait()


for b in range(len(grid)):
    depot = range(len(grid))
    del depot[b]
    for d in depot:
        for i in range(len(grid)-1):
                if i != b:
                    work(b,grid[d],grid[i],i)
