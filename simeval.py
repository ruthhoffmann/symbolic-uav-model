#!/usr/bin/python

import subprocess

NOPROPS = 18
X = 7
Y = 4
grid = [[0 for x in range(2)] for x in range(X*Y)]
count = 0
for i in range(X):
    for j in range(Y):
        grid[count][0] = i
        grid[count][1] = j
        count += 1

finalres = []
for b in [[0,0],[1,0]]:
    tmpout = [[] for x in range(NOPROPS)]
    tmpfile = open("sim-tmp-%d%d.txt"%(b[0],b[1]),"w")
    d = [2,3]
    for i in range(len(grid)-1):
        if i != grid.index(b):
            for j in range(i+1,len(grid)):
                if j != grid.index(b):
                    grarg = ["grep", "Result", "./simres/result-%d%d-%d%d-%d%d-%d%d.txt"%( b[0], b[1], d[0], d[1], grid[i][0], grid[i][1], grid[j][0], grid[j][1])]
                    p1 = subprocess.Popen(grarg, stdout=tmpfile)

    tmpfile.close()
    tmpfile = open("sim-tmp-%d%d.txt"%(b[0],b[1]),"r")
    count = 0
    for line in tmpfile:
        fields = line.strip().split()
        tmpout[count%NOPROPS].append(fields[1])
        count += 1
    tmpfile.close()

    # Averages
    avs = []
    minval = []
    maxval = []
    for i in range(len(tmpout)):
        avs.append(sum(float(j) for j in tmpout[i]))
        avs[i] = avs[i]/len(tmpout[i])
        minval.append(min(map(float,tmpout[i])))
        maxval.append(max(map(float,tmpout[i])))

    resout = []
    for i in range(len(avs)):
        resout.append([avs[i],minval[i],maxval[i]])

    finalres.append(resout)

finfile = open("simres.txt","w")
f1 = finalres[0]
f2 = finalres[1]
for j in range(NOPROPS):
        finfile.write(str(j+1) + " " + str((f1[j][0]+f2[j][0])/2) + " " + str(min(f1[j][1],f2[j][1])) + " " + str(max(f1[j][2],f2[j][2])) + "\n")
finfile.close()
