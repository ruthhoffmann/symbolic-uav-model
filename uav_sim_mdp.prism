mdp

// If object is on deposit coords or corners, then ignore and finish mission unsuccessful.


// To run scenario 2 uncomment all lines ending with //Scenario 2 and
// comment all lines ending with //Scenario 1

//////////////////////////
// Constants/Parameters //
//////////////////////////
// COORDINATES
// Room size
const int Xcoord = 7-1; // We start indexing at 0
const int Ycoord = 4-1; // We start indexing at 0
// For search, what are the last coordinates we can reach?
const int lastX = (mod(Ycoord,2)=0 ? Xcoord : 0);
const int lastY = Ycoord;
// Start/Base coordinates
const int Basex;
const int Basey;
// Depot coordinates
const int Depotx;
const int Depoty;
// Object Placements
const int Objx1;
const int Objy1;

const int Objx2;
const int Objy2;

//const int Objx3;
//const int Objy3;

// TIME
// Max mission time
//const int Miss = 900; //Scenario 1
const int Miss = 500; //Scenario 2
// Time to charge one unit of battery (see Tid formula towards the end)
const int Tchar = 2;
// Time to take off to search height
const int Thu = 2;
const int Thl = 1;
// Initialisation
const int Tinu = 1;
const int Tinl = 0;
// Positioning over target
const int Tpu = 8;
const int Tpl = 7;
// Descending from search H to grab H
const int Tdsu = 4;
const int Tdsl = 3;
// Grabbing/Releasing
const int Tgu = 1;
const int Tgl = 0;
// Ascending from grab H to transport H
const int Tatu = 4;
const int Tatl = 3;
// Descending from transport H to grab H
const int Tdtu = 4;
const int Tdtl = 3;
// Ascending from grab H to search H
const int Tasu = 5;
const int Tasl = 4;
// Landing from search H
const int Tlau = 4;
const int Tlal = 3;
// Time to find lost object
const int Tlou = 1;
const int Tlol = 0;
// Time step based on velocity, currently it's a constant,
// might need to be a formula
const int dt = 1;

// BATTERY (scaled for discrete model)
// Battery time
const int Batt = 120;
// Warning limit
const int Blow = 30;
// Rate of capacity used in one timestep
const int db = 1;




///////////////////
// Probabilities //
///////////////////
// Probability of the system being OK
const double pb = 1;
// Probability of finding the lost object
const double pc = 0.818182;
// Probability of not dropping the object
const double pd = 0.973386;
// Probability of not crashing per second (time step.)
const double pf = 0.99982;
// Probability of losing sight of the object while approaching
const double pl = 0.0706226;
// Probability of taking to long while descending to grasp
const double pt = 0.00859;
// Probability of dropping the object on depot site early
const double pe = 0.00105;


// Formula to determine whether an object lies in the coordinates that are currently searched.
//formula FOUND = (((posx=Objx1 & posy=Objy1) | (posx=Objx2 & posy=Objy2) | (posx=Objx3 & posy=Objy3)) & !(posx=Depotx & posy=Depoty));
formula FOUND = (((posx=Objx1 & posy=Objy1) | (posx=Objx2 & posy=Objy2)) & !(posx=Depotx & posy=Depoty));

// When we found something, we do not have to re-search those coordinates again.
formula RETX = (mod(rety,2)=0) ? ((retx<Xcoord) ? retx+1 : retx) : ((retx>0) ? retx-1 : retx);
formula RETY = ((retx=Xcoord & mod(rety,2)=0 & rety!=lastY)| (retx=0 & mod(rety,2)=1 & rety!=lastY)) ? rety+1 : rety;

// Return coordinates
formula dRETX = ((posy > rety) | (posy = rety & mod(posy,2) = 0 & posx >= retx) | (posy = rety & mod(posy,2) = 1 & posx <= retx))? posx : retx;
formula dRETY =  ((posy > rety) | (posy = rety & mod(posy,2) = 0 & posx >= retx) | (posy = rety & mod(posy,2) = 1 & posx <= retx))? posy : rety;

///////////////////
// OBJECT MODULE //
///////////////////
module Obj

//	NoOfObjs : [0..3] init 3; //Scenario 1
	NoOfObjs : [0..2] init 2; //Scenario 2

// If an object has been deposited, we have fewer objects to find.
	[grab] (s1!=10) -> (NoOfObjs'=NoOfObjs);
	[grab] (s1=10 & NoOfObjs > 0) -> (NoOfObjs'=NoOfObjs-1);
// mission over (reset variable to reduce state space)
	[fin] true -> (NoOfObjs'=0);

endmodule



/////////////////////
// MOVEMENT MODULE //
/////////////////////
module Move

	posx : [0..Xcoord] init Basex;
	posy : [0..Ycoord] init Basey;

// Search
	[srch] (posx>=0 & posx<Xcoord & posy<=Ycoord & mod(posy,2)=0 & !FOUND) -> (posx'=posx+1);
	[srch] (posx=Xcoord & mod(posy,2)=0 & posy<Ycoord & !FOUND) -> (posy'=posy+1);
	[srch] (posx>0 & posx<=Xcoord & posy<=Ycoord & mod(posy,2)=1 & !FOUND) -> (posx'=posx-1);
	[srch] (posx=0 & mod(posy,2)=1 & posy<Ycoord & !FOUND) -> (posy'=posy+1);
	[srch] (FOUND)  -> true;
	[srch] (posx=lastX & posy=lastY) -> true;
// Approaching Depot site
	[appD] (s1=8 & posx>Depotx & posy>Depoty) -> (posx'=posx-1)&(posy'=posy-1);
	[appD] (s1=8 & posx<Depotx & posy>Depoty) -> (posx'=posx+1)&(posy'=posy-1);
	[appD] (s1=8 & posx=Depotx & posy>Depoty) -> (posx'=posx)&(posy'=posy-1);
	[appD] (s1=8 & posx>Depotx & posy<Depoty) -> (posx'=posx-1)&(posy'=posy+1);
	[appD] (s1=8 & posx<Depotx & posy<Depoty) -> (posx'=posx+1)&(posy'=posy+1);
	[appD] (s1=8 & posx=Depotx & posy<Depoty) -> (posx'=posx)&(posy'=posy+1);
	[appD] (s1=8 & posx>Depotx & posy=Depoty) -> (posx'=posx-1)&(posy'=posy);
	[appD] (s1=8 & posx<Depotx & posy=Depoty) -> (posx'=posx+1)&(posy'=posy);
	[appD] (s1=8 & posx=Depotx & posy=Depoty) -> true;
// Approaching base
	[appB] (s1=12) -> (posx'=Basex)&(posy'=Basey);
// Approaching where we left of searching
	[appS] (s1=14) -> (posx'=retx) & (posy'=rety);
// Time up!
	[tmup] (t+Timerbu>=Miss & s1!=13) -> (posx'=Basex)&(posy'=Basey);
	[tmup] (t+Timerbl>=Miss & s1!=13) -> (posx'=Basex)&(posy'=Basey);
// mission over (reset variables to reduce state space)
	[fin] true -> (posx'=0)&(posy'=0);

endmodule



//////////////////
// ROBOT MODULE //
//////////////////
module Robot

// Modes of automaton
	s1 : [0..16] init 0;
// 0 Idle
// 1 Take Off
// 2 Initialise
// 3 Search
// 4 Target Approach
// 5 Descend Target
// 6 Grab
// 7 Ascend
// 8 Transport
// 9 Descend Depot
// 10 Deposit
// 11 Ascend
// 12 Return to Base
// 13 Land
// 14 Go to Search
// 15 Return to where object has been dropped/lost
// 16 Ultimate final state

// records last searching coordinates
	retx : [0..Xcoord] init 0;
	rety : [0..Ycoord] init 0;

// Idle/Resting
	[start] (s1=0 & NoOfObjs > 0 & !(retx=lastX & rety=lastY)) -> (s1'=1);

// Taking Off
	[TO] (s1=1) -> (s1'=2);

// Initialise
    [ini] (s1=2) -> (pb):(s1'=14) + (1-pb):(s1'=13);

// Search
    [srch] (s1=3 & !(posx=lastX & posy=lastY) & !FOUND) -> (s1'=3);
    [srch] (s1=3 & !(posx=lastX & posy=lastY) & FOUND) -> (s1'=4)&(retx'=posx)&(rety'=posy);
    [srch] (s1=3 & (posx=lastX & posy=lastY) & !FOUND) -> (s1'=12)&(retx'=posx)&(rety'=posy);
    [srch] (s1=3 & (posx=lastX & posy=lastY) & FOUND) -> (s1'=4)&(retx'=posx)&(rety'=posy);


// Target Approach (Approach to target is time based not position based)
	[appO] (s1=4) -> (pl):(s1'=14) + (1-pl):(s1'=5);

// Descend
	[desc] (s1=5) -> (pt):(s1'=14) + (1-pt):(s1'=6);

// Grab
	[grab] (s1=6) -> (s1'=7);

// Ascend
	[asce] (s1=7) -> (pd):(s1'=8) + (1-pd):(s1'=15);

// Transport
    [appD] (s1=8 & !(posx=Depotx & posy=Depoty)) -> (pd):(s1'=8) + (1-pd):(s1'=15);
	[appD] (s1=8 & (posx=Depotx & posy=Depoty)) -> (s1'=9);

// Descend
	[desc] (s1=9) -> (pe):(s1'=11) + (1-pe):(s1'=10);

// Deposit
	[grab] (s1=10) -> (s1'=11);

// Ascend
	[asce] (s1=11 & NoOfObjs>0) -> (s1'=14)&(retx'=RETX)&(rety'=RETY);
	[asce] (s1=11 & NoOfObjs=0) -> (s1'=12)&(retx'=RETX)&(rety'=RETY);

// Return to Base
	[appB] (s1=12) -> (s1'=13);

// Land
	[land] (s1=13) -> (s1'=0);

// Return to Search
	[appS] (s1=14) -> (s1'=3) & (retx'=0) & (rety'=0);

// Return to where object has been dropped/lost
	[appL] (s1=15) -> (pc):(s1'=4) + (1-pc):(s1'=14);

//////////////
// FAILURES //
//////////////
// Mission Time Up, we move to base and safely land
	[tmup] (s1!=0 & s1!=13 & s1!=16) -> (s1'=13);
	[tmup] (s1=13) -> (s1'=0);
// Battery low
	[batt] (s1!=0 & s1!=12 & s1!=13 & s1!=16 & s1!=3) -> (s1'=12);
	[batt] (s1=3) -> (s1'=12)&(retx'=posx)&(rety'=posy);
	[batt] (s1=4 | s1=5 | s1=6 | s1=7 | s1=8 | s1=9) -> (s1'=12)&(retx'=dRETX)&(rety'=dRETY);

// mission (reset variables to reduce state space) over either:
	// - at base and have all objects
	// - at base ran out of time
	// - at base and finished search
	// - crashed
	[fin] (s1=0 & (NoOfObjs=0 | t>=Miss | (retx=lastX & rety=lastY))) | c=1 -> (s1'=16)&(retx'=0)&(rety'=0);

endmodule



/////////////////
// TIME MODULE //
/////////////////
// includes both battery and crash state as these based on per unit of time
module T

	b : [0..Batt] init Batt; // level of battery charge
	t : [0..Miss+Tlau] init 0; // timer
	c : [0..1] init 0; // crash variable

// Idle & Starting off
	[start] c=0 & (t+Tid<Miss) -> pow(pf,Tid) : (t'=t+Tid)&(b'=Batt) + (1 - pow(pf,Tid)) : (c'=1);
	[start] c=0 & (t+Tid>=Miss & t<Miss) -> pow(pf,Tid) : (t'=Miss) + (1 - pow(pf,Tid)) : (c'=1);
// Taking Off
	[TO] c=0 & b>Blow & (t+Thu<Miss) -> pow(pf,Thu) : (t'=t+Thu)&(b'=b-db*Thu) + (1 - pow(pf,Thu)) : (c'=1);
	[TO] c=0 & b>Blow & (t+Thu>=Miss & t<Miss) -> pow(pf,Thu) : (t'=Miss) + (1 - pow(pf,Thu)) : (c'=1);
	[TO] c=0 & b>Blow & (t+Thl<Miss) -> pow(pf,Thl) : (t'=t+Thl)&(b'=b-db*Thl) + (1 - pow(pf,Thl)) : (c'=1);
	[TO] c=0 & b>Blow & (t+Thl>=Miss & t<Miss) -> pow(pf,Thl) : (t'=Miss) + (1 - pow(pf,Thl)) : (c'=1);
// Initialising
	[ini] c=0 & b>Blow & (t+Tinu<Miss) -> pow(pf,Tinu) : (t'=t+Tinu)&(b'=b-db*Tinu) + (1 - pow(pf,Tinu)) : (c'=1);
	[ini] c=0 & b>Blow & (t+Tinu>=Miss & t<Miss) -> pow(pf,Tinu) : (t'=Miss) + (1 - pow(pf,Tinu)) : (c'=1);
	[ini] c=0 & b>Blow & (t+Tinl<Miss) -> pow(pf,Tinl) : (t'=t+Tinl)&(b'=b-db*Tinl) + (1 - pow(pf,Tinl)) : (c'=1);
	[ini] c=0 & b>Blow & (t+Tinl>=Miss & t<Miss) -> pow(pf,Tinl) : (t'=Miss) + (1 - pow(pf,Tinl)) : (c'=1);
// Search
	[srch] c=0 & b>Blow & (t+dt<Miss) -> pow(pf,dt) : (t'=t+dt)&(b'=b-db*dt) + (1 - pow(pf,dt)) : (c'=1);
	[srch] c=0 & b>Blow & (t+dt>=Miss & t<Miss) -> pow(pf,dt) : (t'=Miss) + (1 - pow(pf,dt)) : (c'=1);
// Approach Target
	[appO] c=0 & b>Blow & (t+Tpu<Miss) -> pow(pf,Tpu) : (t'=t+Tpu)&(b'=b-db*Tpu) + (1 - pow(pf,Tpu)) : (c'=1);
	[appO] c=0 & b>Blow & (t+Tpu>=Miss & t<Miss) -> pow(pf,Tpu) : (t'=Miss) + (1 - pow(pf,Tpu)) : (c'=1);
	[appO] c=0 & b>Blow & (t+Tpl<Miss) -> pow(pf,Tpl) : (t'=t+Tpl)&(b'=b-db*Tpl) + (1 - pow(pf,Tpl)) : (c'=1);
	[appO] c=0 & b>Blow & (t+Tpl>=Miss & t<Miss) -> pow(pf,Tpl) : (t'=Miss) + (1 - pow(pf,Tpl)) : (c'=1);
// Descending to grabbing H from search H
	[desc] c=0 & b>Blow & (s1=5 & t+Tdsu<Miss) -> pow(pf,Tdsu) : (t'=t+Tdsu)&(b'=b-db*Tdsu) + (1 - pow(pf,Tdsu)) : (c'=1);
	[desc] c=0 & b>Blow & (s1=5 & t+Tdsu>=Miss & t<Miss) -> pow(pf,Tdsu) : (t'=Miss) + (1 - pow(pf,Tdsu)) : (c'=1);
	[desc] c=0 & b>Blow & (s1=5 & t+Tdsl<Miss) -> pow(pf,Tdsl) : (t'=t+Tdsl)&(b'=b-db*Tdsl) + (1 - pow(pf,Tdsl)) : (c'=1);
	[desc] c=0 & b>Blow & (s1=5 & t+Tdsl>=Miss & t<Miss) -> pow(pf,Tdsl) : (t'=Miss) + (1 - pow(pf,Tdsl)) : (c'=1);
// Descending to grabbing H from transport H
	[desc] c=0 & b>Blow & (s1=9 & t+Tdtu<Miss) -> pow(pf,Tdtu) : (t'=t+Tdtu)&(b'=b-db*Tdtu) + (1 - pow(pf,Tdtu)) : (c'=1);
	[desc] c=0 & b>Blow & (s1=9 & t+Tdtu>=Miss & t<Miss) -> pow(pf,Tdtu) : (t'=Miss) + (1 - pow(pf,Tdtu)) : (c'=1);
	[desc] c=0 & b>Blow & (s1=9 & t+Tdtl<Miss) -> pow(pf,Tdtl) : (t'=t+Tdtl)&(b'=b-db*Tdtl) + (1 - pow(pf,Tdtl)) : (c'=1);
	[desc] c=0 & b>Blow & (s1=9 & t+Tdtl>=Miss & t<Miss) -> pow(pf,Tdtl) : (t'=Miss) + (1 - pow(pf,Tdtl)) : (c'=1);
// Grabbing/Releasing
	[grab] c=0 & b>Blow & (t+Tgu<Miss) -> pow(pf,Tgu) : (t'=t+Tgu)&(b'=b-db*Tgu) + (1 - pow(pf,Tgu)) : (c'=1);
	[grab] c=0 & b>Blow & (t+Tgu>=Miss & t<Miss) -> pow(pf,Tgu) : (t'=Miss) + (1 - pow(pf,Tgu)) : (c'=1);
	[grab] c=0 & b>Blow & (t+Tgl<Miss) -> pow(pf,Tgl) : (t'=t+Tgl)&(b'=b-db*Tgl) + (1 - pow(pf,Tgl)) : (c'=1);
	[grab] c=0 & b>Blow & (t+Tgl>=Miss & t<Miss) -> pow(pf,Tgl) : (t'=Miss) + (1 - pow(pf,Tgl)) : (c'=1); // not enabled because Tgl=0
// Ascending to transportation height
	[asce] c=0 & b>Blow & (s1=7 & t+Tatu<Miss) -> pow(pf,Tatu) : (t'=t+Tatu)&(b'=b-db*Tatu) + (1 - pow(pf,Tatu)) : (c'=1);
	[asce] c=0 & b>Blow & (s1=7 & t+Tatu>=Miss & t<Miss) -> pow(pf,Tatu) : (t'=Miss) + (1 - pow(pf,Tatu)) : (c'=1);
	[asce] c=0 & b>Blow & (s1=7 & t+Tatl<Miss) -> pow(pf,Tatl) : (t'=t+Tatl)&(b'=b-db*Tatl) + (1 - pow(pf,Tatl)) : (c'=1);
	[asce] c=0 & b>Blow & (s1=7 & t+Tatl>=Miss & t<Miss) -> pow(pf,Tatl) : (t'=Miss) + (1 - pow(pf,Tatl)) : (c'=1);
// Ascending to search height
	[asce] c=0 & b>Blow & (s1=11 & t+Tasu<Miss) -> pow(pf,Tasu) : (t'=t+Tasu)&(b'=b-db*Tasu) + (1 - pow(pf,Tasu)) : (c'=1);
	[asce] c=0 & b>Blow & (s1=11 & t+Tasu>=Miss & t<Miss) -> pow(pf,Tasu) : (t'=Miss) + (1 - pow(pf,Tasu)) : (c'=1);
	[asce] c=0 & b>Blow & (s1=11 & t+Tasl<Miss) -> pow(pf,Tasl) : (t'=t+Tasl)&(b'=b-db*Tasl) + (1 - pow(pf,Tasl)) : (c'=1);
	[asce] c=0 & b>Blow & (s1=11 & t+Tasl>=Miss & t<Miss) -> pow(pf,Tasl) : (t'=Miss) + (1 - pow(pf,Tasl)) : (c'=1);
// Approaching depot site
	[appD] c=0 & b>Blow & (t+dt<Miss) -> pow(pf,dt) : (t'=t+dt)&(b'=b-db*dt) + (1 - pow(pf,dt)) : (c'=1);
	[appD] c=0 & b>Blow & (t+dt>=Miss & t<Miss) -> pow(pf,dt) : (t'=Miss) + (1 - pow(pf,dt)) : (c'=1);
// Approaching base
	[appB] c=0 & b>=db*Timerbu & (t+Timerbu<Miss) -> pow(pf,Timerbu) : (t'=t+Timerbu)&(b'=b-db*Timerbu) + (1 - pow(pf,Timerbu)) : (c'=1);
	[appB] c=0 & b>=db*Timerbu & (t+Timerbu>=Miss & t<Miss) -> pow(pf,Timerbu) : (t'=Miss) + (1 - pow(pf,Timerbu)) : (c'=1);
	[appB] c=0 & b>=db*Timerbl & (t+Timerbl<Miss) -> pow(pf,Timerbl) : (t'=t+Timerbl)&(b'=b-db*Timerbl) + (1 - pow(pf,Timerbl)) : (c'=1);
	[appB] c=0 & b>=db*Timerbl & (t+Timerbl>=Miss & t<Miss) -> pow(pf,Timerbl) : (t'=Miss) + (1 - pow(pf,Timerbl)) : (c'=1);
// Landing
	[land] c=0 & b>=db*Tlau & (t<=Miss) -> pow(pf,Tlau) : (t'=t+Tlau)&(b'=b-db*Tlau) + (1 - pow(pf,Tlau)) : (c'=1);
	[land] c=0 & b>=db*Tlal & (t<=Miss) -> pow(pf,Tlal) : (t'=t+Tlal)&(b'=b-db*Tlal) + (1 - pow(pf,Tlal)) : (c'=1);
// Approach where we left off searching
	[appS] c=0 & b>Blow & (t+Timersu<Miss) -> pow(pf,Timersu) : (t'=t+Timersu)&(b'=b-db*Timersu) + (1 - pow(pf,Timersu)) : (c'=1);
	[appS] c=0 & b>Blow & (t+Timersu>=Miss & t<Miss) -> pow(pf,Timersu) : (t'=Miss) + (1 - pow(pf,Timersu)) : (c'=1);
	[appS] c=0 & b>Blow & (t+Timersl<Miss) -> pow(pf,Timersl) : (t'=t+Timersl)&(b'=b-db*Timersl) + (1 - pow(pf,Timersl)) : (c'=1);
	[appS] c=0 & b>Blow & (t+Timersl>=Miss & t<Miss) -> pow(pf,Timersl) : (t'=Miss) + (1 - pow(pf,Timersl)) : (c'=1);
// Approaching site where object was dropped/lost
	[appL] c=0 & b>Blow & (t+Tlou<Miss) -> pow(pf,Tlou) : (t'=t+Tlou)&(b'=b-db*Tlou) + (1 - pow(pf,Tlou)) : (c'=1);
	[appL] c=0 & b>Blow & (t+Tlou>=Miss & t<Miss) -> pow(pf,Tlou) : (t'=Miss) + (1 - pow(pf,Tlou)) : (c'=1);
	[appL] c=0 & b>Blow & (t+Tlol<Miss) -> pow(pf,Tlol) : (t'=t+Tlol)&(b'=b-db*Tlol) + (1 - pow(pf,Tlol)) : (c'=1);
	[appL] c=0 & b>Blow & (t+Tlol>=Miss & t<Miss) -> pow(pf,Tlol) : (t'=Miss) + (1 - pow(pf,Tlol)) : (c'=1);
// Time has run out
	[tmup] c=0 & t=Miss -> pow(pf,Timerbu) : true + (1 - pow(pf,Timerbu)) : (c'=1);
	[tmup] c=0 & t=Miss -> pow(pf,Timerbl) : true + (1 - pow(pf,Timerbl)) : (c'=1);
// Battery is low
	[batt] c=0 & b<=Blow & t<Miss -> true;
// mission over (reset variable to reduce state space)
	[fin] true -> (t'=0) & (c'=0) & (b'=0);

endmodule



//////////////
// Formulas //
//////////////
// Calculations of distances, flexible times and changing probabilities
// Calculation of how long we would have to be in the Idle state to recharge batteries/fix things.
formula Tid = (Batt - b) * Tchar;

// Distance to base is equal to the ceiling integer length of the vector from the base to the current position
formula dbase = ceil(pow(pow(posx-Basex,2)+pow(posy-Basey,2),0.5));

// Calculation of integer distance from agent to depot.
formula ddepot = ceil(pow(pow(posx-Depotx,2)+pow(posy-Depoty,2),0.5));

// Calculation of distance from depot to base.
formula deptobase = ceil(pow(pow(Depotx-Basex,2)+pow(Depoty-Basey,2),0.5));

// Calculation of time that will pass getting from current coordinates to base.
// (Currently time = distance as velocity is 1m/s)
formula Timerbu = ceil(pow(pow(posx-Basex,2)+pow(posy-Basey,2),0.5));
formula Timerbl = floor(pow(pow(posx-Basex,2)+pow(posy-Basey,2),0.5));

// Calculation of time that will pass getting from current coordinates to where we left off
// searching (Currently time = distance as velocity is 1m/s)
formula Timersu = ceil(pow(pow(posx-retx,2)+pow(posy-rety,2),0.5));
formula Timersl = floor(pow(pow(posx-retx,2)+pow(posy-rety,2),0.5));



/////////////
// Rewards //
/////////////
rewards "drop"
	s1=15 : 1;
endrewards

// expected time for the mission
rewards "time"
	[fin] true : t;
endrewards

// time to complete the mission successfully
rewards "success"
	[fin] s1=0 & NoOfObjs=0 : t;
endrewards

// number of times we recharge the battery during the mission
rewards "charge"
	[batt] s1!=0 & s1!=12 & s1!=13 & s1!=16 & s1!=3 : 1;
endrewards

// number of times we recharge the battery during the mission
rewards "battery"
	true : b;
endrewards
