#!/usr/bin/python

import subprocess

NOPROPS = 18
X = 7
Y = 4
grid = [[0 for x in range(2)] for x in range(X*Y)]
count = 0
for i in range(X):
    for j in range(Y):
        grid[count][0] = i
        grid[count][1] = j
        count += 1

finalres = []
tmpfile = open("ft_simtmptime.txt","w")
for b in [[0,0],[1,0]]:
    d = [2,3]
    for i in range(len(grid)-1):
        if i != grid.index(b):
            for j in range(i+1,len(grid)):
                if j != grid.index(b):
                    grarg = ["grep", "Time for model", "./ft_simres/result-%d%d-%d%d-%d%d-%d%d.txt"%( b[0], b[1], d[0], d[1], grid[i][0], grid[i][1], grid[j][0], grid[j][1])]
                    p1 = subprocess.Popen(grarg, stdout=tmpfile)

tmpfile.close()
tmpfile = open("ft_simtmptime.txt","r")
everything = []
construct = []
count = 0
for line in tmpfile:
    fields = line.strip().split()
    everything.append(fields[4])
    if count % (NOPROPS+1) == 0:
        construct.append(fields[4])
    count += 1
tmpfile.close()

totaltime = sum(float(j) for j in everything)
totalconstrtime = sum(float(j) for j in construct)
avconsttime = totalconstrtime/(count/(NOPROPS+1))


outfile = open("ft_simtime.txt","w")
outfile.write("Total time to construct and model check all models (min) " + str(totaltime/60))
outfile.write("Total time to construct all models (min) " + str(totalconstrtime/60))
outfile.write("Average time to construct a model (sec) " + str(avconsttime))
outfile.close()
