#!/usr/bin/python
import subprocess

def work(b,d,o1,j):
    threads = []
    for k in range(j+1,len(grid)):
        if k != grid.index(b):
            #arg = ["prism" , "uav_sim_ft_mdp.prism" , "-s" , "uav_mdp.props" , "-const" , "Basex=%d,Basey=%d,Depotx=%d,Depoty=%d,Objx1=%d,Objy1=%d,Objx2=%d,Objy2=%d,"%(b[0], b[1], d[0], d[1], o1[0], o1[1], grid[k][0], grid[k][1])]
            arg = ["/home/ruth/prism-4.3-linux64/bin/prism" , "uav_sim_ft_mdp.prism" , "-s" , "uav_mdp.props" , "-const" , "Basex=%d,Basey=%d,Depotx=%d,Depoty=%d,Objx1=%d,Objy1=%d,Objx2=%d,Objy2=%d,"%(b[0], b[1], d[0], d[1], o1[0], o1[1], grid[k][0], grid[k][1])]
            outfile = open("result-%d%d-%d%d-%d%d-%d%d.txt"%(b[0], b[1], d[0], d[1], o1[0], o1[1], grid[k][0], grid[k][1]), "w")
            threads.append(subprocess.Popen(arg,stdout=outfile))

    for p in threads:
        p.wait()

X = 7
Y = 4
grid = [[0 for x in range(2)] for x in range(X*Y)]
count = 0
for i in range(X):
    for j in range(Y):
        grid[count][0] = i
        grid[count][1] = j
        count += 1

base = [[0,0],[1,0]]
depot = [2,3]

for b in base:
    for i in range(len(grid)-1):
        if i != grid.index(b):
            work(b,depot,grid[i],i)
