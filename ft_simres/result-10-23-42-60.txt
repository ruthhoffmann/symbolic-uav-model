PRISM
=====

Version: 4.3
Date: Tue Aug 02 12:42:19 BST 2016
Hostname: eglinton
Memory limits: cudd=1g, java(heap)=981.5m
Command line: prism uav_sim_ft_mdp.prism -s uav_mdp.props -const 'Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=4,Objy1=2,Objx2=6,Objy2=0,'

Parsing model file "uav_sim_ft_mdp.prism"...

Parsing properties file "uav_mdp.props"...

18 properties:
(1) Pmax=? [ F "MissionSuccessful" ]
(2) Pmin=? [ F "MissionSuccessful" ]
(3) Pmax=? [ F "MissionTimeUp" ]
(4) Pmin=? [ F "MissionTimeUp" ]
(5) Pmax=? [ F "Crash" ]
(6) Pmin=? [ F "Crash" ]
(7) Pmax=? [ F "AllAreaSearched" ]
(8) Pmin=? [ F "AllAreaSearched" ]
(9) Pmax=? [ F "MissionTimeUp"|"AllAreaSearched"|"Crash" ]
(10) Pmin=? [ F "MissionTimeUp"|"AllAreaSearched"|"Crash" ]
(11) R{"drop"}max=? [ F "deadlock" ]
(12) R{"drop"}min=? [ F "deadlock" ]
(13) R{"charge"}max=? [ F "deadlock" ]
(14) R{"charge"}min=? [ F "deadlock" ]
(15) R{"time"}max=? [ F "deadlock" ]
(16) R{"time"}min=? [ F "deadlock" ]
(17) R{"success"}max=? [ F "deadlock" ]/Pmin=? [ F "MissionSuccessful" ]
(18) R{"success"}min=? [ F "deadlock" ]/Pmax=? [ F "MissionSuccessful" ]

Type:        MDP
Modules:     Obj Move Robot T 
Variables:   NoOfObjs posx posy s1 retx rety b t c 

---------------------------------------------------------------------

Model checking: Pmax=? [ F "MissionSuccessful" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=4,Objy1=2,Objx2=6,Objy2=0

Building model...
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=4,Objy1=2,Objx2=6,Objy2=0

Warning: Guard for command 7 of module "Move" is never satisfied.

Warning: Guard for command 8 of module "Move" is never satisfied.

Warning: Guard for command 9 of module "Move" is never satisfied.

Warning: Guard for command 7 of module "Robot" is never satisfied.

Warning: Update 2 of command 3 of module "Robot" doesn't do anything ("(s1'=13)", line 249, column 44)

Warning: Update 2 of command 27 of module "T" doesn't do anything ("(c'=1)", line 372, column 108)

Warning: Guard for command 28 of module "T" is never satisfied.

Warning: Update 2 of command 9 of module "T" doesn't do anything ("(c'=1)", line 349, column 112)

Warning: Guard for command 10 of module "T" is never satisfied.

Warning: Update 2 of command 51 of module "T" doesn't do anything ("(c'=1)", line 403, column 113)

Warning: Guard for command 52 of module "T" is never satisfied.

Computing reachable states...

Reachability (BFS): 77 iterations in 0.72 seconds (average 0.009299, setup 0.00)

Time for model construction: 1.677 seconds.

Warning: Deadlocks detected and fixed in 1 states

Type:        MDP
States:      229875 (1 initial)
Transitions: 617860
Choices:     304766

Transition matrix: 175518 nodes (76 terminal), 617860 minterms, vars: 34r/34c/18nd

Prob0A: 52 iterations in 0.31 seconds (average 0.006000, setup 0.00)

Prob1E: 4 iterations in 0.04 seconds (average 0.011000, setup 0.00)

yes = 941, no = 194692, maybe = 34242

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=229875, nc=59326, nnz=169842, k=2] [2.2 MB]
Creating vector for yes... [1.8 MB]
Allocating iteration vectors... [2 x 1.8 MB]
TOTAL: [7.5 MB]

Starting iterations...

Iterative method: 96 iterations in 0.35 seconds (average 0.001542, setup 0.20)

Value in the initial state: 0.9783604139433923

Time for model checking: 0.742 seconds.

Result: 0.9783604139433923 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmin=? [ F "MissionSuccessful" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=4,Objy1=2,Objx2=6,Objy2=0

Prob0E: 54 iterations in 0.43 seconds (average 0.007926, setup 0.00)

Prob1A: 3 iterations in 0.10 seconds (average 0.032000, setup 0.00)

yes = 941, no = 200514, maybe = 28420

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=229875, nc=49074, nnz=139541, k=2] [1.9 MB]
Creating vector for yes... [1.8 MB]
Allocating iteration vectors... [2 x 1.8 MB]
TOTAL: [7.1 MB]

Starting iterations...

Iterative method: 90 iterations in 0.45 seconds (average 0.001644, setup 0.30)

Value in the initial state: 0.733278060864442

Time for model checking: 1.057 seconds.

Result: 0.733278060864442 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmax=? [ F "MissionTimeUp" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=4,Objy1=2,Objx2=6,Objy2=0

Prob0A: 59 iterations in 0.48 seconds (average 0.008203, setup 0.00)

Prob1E: 4 iterations in 0.04 seconds (average 0.009000, setup 0.00)

yes = 424, no = 135704, maybe = 93747

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=229875, nc=158784, nnz=438872, k=4] [5.4 MB]
Creating vector for yes... [1.8 MB]
Allocating iteration vectors... [2 x 1.8 MB]
TOTAL: [10.7 MB]

Starting iterations...

Iterative method: 92 iterations in 0.45 seconds (average 0.002043, setup 0.26)

Value in the initial state: 0.001650517991644989

Time for model checking: 0.985 seconds.

Result: 0.001650517991644989 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmin=? [ F "MissionTimeUp" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=4,Objy1=2,Objx2=6,Objy2=0

Prob0E: 66 iterations in 0.63 seconds (average 0.009515, setup 0.00)

Prob1A: 3 iterations in 0.04 seconds (average 0.014667, setup 0.00)

yes = 424, no = 136061, maybe = 93390

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=229875, nc=158146, nnz=437563, k=4] [5.4 MB]
Creating vector for yes... [1.8 MB]
Allocating iteration vectors... [2 x 1.8 MB]
TOTAL: [10.6 MB]

Starting iterations...

Iterative method: 108 iterations in 0.47 seconds (average 0.002037, setup 0.25)

Value in the initial state: 1.1658369506678496E-5

Time for model checking: 1.179 seconds.

Result: 1.1658369506678496E-5 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmax=? [ F "Crash" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=4,Objy1=2,Objx2=6,Objy2=0

Prob0A: 3 iterations in 0.07 seconds (average 0.024000, setup 0.00)

Prob1E: 198 iterations in 2.27 seconds (average 0.011455, setup 0.00)

yes = 112517, no = 2610, maybe = 114748

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=229875, nc=189639, nnz=502733, k=4] [6.2 MB]
Creating vector for yes... [1.8 MB]
Allocating iteration vectors... [2 x 1.8 MB]
TOTAL: [11.4 MB]

Starting iterations...

Iterative method: 91 iterations in 0.52 seconds (average 0.002154, setup 0.32)

Value in the initial state: 0.03282416485484703

Time for model checking: 2.875 seconds.

Result: 0.03282416485484703 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmin=? [ F "Crash" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=4,Objy1=2,Objx2=6,Objy2=0

Prob0E: 3 iterations in 0.05 seconds (average 0.017333, setup 0.00)

Prob1A: 46 iterations in 0.46 seconds (average 0.010087, setup 0.00)

yes = 112517, no = 2610, maybe = 114748

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=229875, nc=189639, nnz=502733, k=4] [6.2 MB]
Creating vector for yes... [1.8 MB]
Allocating iteration vectors... [2 x 1.8 MB]
TOTAL: [11.4 MB]

Starting iterations...

Iterative method: 96 iterations in 0.47 seconds (average 0.002208, setup 0.26)

Value in the initial state: 0.019649130597911336

Time for model checking: 0.998 seconds.

Result: 0.019649130597911336 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmax=? [ F "AllAreaSearched" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=4,Objy1=2,Objx2=6,Objy2=0

Prob0A: 58 iterations in 0.41 seconds (average 0.007034, setup 0.00)

Prob1E: 4 iterations in 0.04 seconds (average 0.009000, setup 0.00)

yes = 1244, no = 184085, maybe = 44546

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=229875, nc=69547, nnz=183998, k=2] [2.4 MB]
Creating vector for yes... [1.8 MB]
Allocating iteration vectors... [2 x 1.8 MB]
TOTAL: [7.7 MB]

Starting iterations...

Iterative method: 91 iterations in 0.31 seconds (average 0.001582, setup 0.16)

Value in the initial state: 0.23720935187327008

Time for model checking: 0.774 seconds.

Result: 0.23720935187327008 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmin=? [ F "AllAreaSearched" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=4,Objy1=2,Objx2=6,Objy2=0

Prob0E: 64 iterations in 0.32 seconds (average 0.005000, setup 0.00)

Prob1A: 3 iterations in 0.03 seconds (average 0.009333, setup 0.00)

yes = 1244, no = 190742, maybe = 37889

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=229875, nc=58814, nnz=154427, k=2] [2.0 MB]
Creating vector for yes... [1.8 MB]
Allocating iteration vectors... [2 x 1.8 MB]
TOTAL: [7.3 MB]

Starting iterations...

Iterative method: 91 iterations in 0.25 seconds (average 0.001495, setup 0.12)

Value in the initial state: 0.0017523426225814224

Time for model checking: 0.614 seconds.

Result: 0.0017523426225814224 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmax=? [ F "MissionTimeUp"|"AllAreaSearched"|"Crash" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=4,Objy1=2,Objx2=6,Objy2=0

Prob0A: 3 iterations in 0.10 seconds (average 0.033333, setup 0.00)

Prob1E: 212 iterations in 2.20 seconds (average 0.010358, setup 0.00)

yes = 200513, no = 942, maybe = 28420

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=229875, nc=49074, nnz=139541, k=2] [1.9 MB]
Creating vector for yes... [1.8 MB]
Allocating iteration vectors... [2 x 1.8 MB]
TOTAL: [7.1 MB]

Starting iterations...

Iterative method: 84 iterations in 0.31 seconds (average 0.001429, setup 0.19)

Value in the initial state: 0.2667212285507439

Time for model checking: 2.6 seconds.

Result: 0.2667212285507439 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmin=? [ F "MissionTimeUp"|"AllAreaSearched"|"Crash" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=4,Objy1=2,Objx2=6,Objy2=0

Prob0E: 3 iterations in 0.06 seconds (average 0.018667, setup 0.00)

Prob1A: 52 iterations in 0.44 seconds (average 0.008462, setup 0.00)

yes = 194691, no = 942, maybe = 34242

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=229875, nc=59326, nnz=169842, k=2] [2.2 MB]
Creating vector for yes... [1.8 MB]
Allocating iteration vectors... [2 x 1.8 MB]
TOTAL: [7.5 MB]

Starting iterations...

Iterative method: 94 iterations in 0.39 seconds (average 0.001660, setup 0.24)

Value in the initial state: 0.021638947961425244

Time for model checking: 0.922 seconds.

Result: 0.021638947961425244 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"drop"}max=? [ F "deadlock" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=4,Objy1=2,Objx2=6,Objy2=0

Prob0E: 4 iterations in 0.08 seconds (average 0.019000, setup 0.00)

Prob1A: 1 iterations in 0.01 seconds (average 0.008000, setup 0.00)

goal = 1, inf = 0, maybe = 229874

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=229875, nc=304765, nnz=617859, k=4] [9.1 MB]
Building sparse matrix (transition rewards)... [n=229875, nc=304765, nnz=0, k=4] [2.0 MB]
Creating vector for state rewards... [1.8 MB]
Creating vector for inf... [1.8 MB]
Allocating iteration vectors... [2 x 1.8 MB]
TOTAL: [18.2 MB]

Starting iterations...

Iterative method: 93 iterations in 0.73 seconds (average 0.003398, setup 0.42)

Value in the initial state: 0.23929268670040982

Time for model checking: 0.83 seconds.

Result: 0.23929268670040982 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"drop"}min=? [ F "deadlock" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=4,Objy1=2,Objx2=6,Objy2=0

Prob0A: 4 iterations in 0.04 seconds (average 0.009000, setup 0.00)

Prob1E: 5 iterations in 0.04 seconds (average 0.008000, setup 0.00)

Warning: PRISM hasn't checked for zero-reward loops.
Your minimum rewards may be too low...

goal = 1, inf = 0, maybe = 229874

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=229875, nc=304765, nnz=617859, k=4] [9.1 MB]
Building sparse matrix (transition rewards)... [n=229875, nc=304765, nnz=0, k=4] [2.0 MB]
Creating vector for state rewards... [1.8 MB]
Creating vector for inf... [1.8 MB]
Allocating iteration vectors... [2 x 1.8 MB]
TOTAL: [18.2 MB]

Starting iterations...

Iterative method: 83 iterations in 0.60 seconds (average 0.003325, setup 0.32)

Value in the initial state: 0.21202891984401967

Time for model checking: 0.688 seconds.

Result: 0.21202891984401967 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"charge"}max=? [ F "deadlock" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=4,Objy1=2,Objx2=6,Objy2=0

Prob0E: 4 iterations in 0.02 seconds (average 0.006000, setup 0.00)

Prob1A: 1 iterations in 0.00 seconds (average 0.004000, setup 0.00)

goal = 1, inf = 0, maybe = 229874

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=229875, nc=304765, nnz=617859, k=4] [9.1 MB]
Building sparse matrix (transition rewards)... [n=229875, nc=304765, nnz=7130, k=4] [2.1 MB]
Creating vector for state rewards... [1.8 MB]
Creating vector for inf... [1.8 MB]
Allocating iteration vectors... [2 x 1.8 MB]
TOTAL: [18.2 MB]

Starting iterations...

Iterative method: 91 iterations in 0.72 seconds (average 0.004352, setup 0.32)

Value in the initial state: 0.31022043117162984

Time for model checking: 0.778 seconds.

Result: 0.31022043117162984 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"charge"}min=? [ F "deadlock" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=4,Objy1=2,Objx2=6,Objy2=0

Prob0A: 4 iterations in 0.04 seconds (average 0.009000, setup 0.00)

Prob1E: 5 iterations in 0.06 seconds (average 0.011200, setup 0.00)

Warning: PRISM hasn't checked for zero-reward loops.
Your minimum rewards may be too low...

goal = 1, inf = 0, maybe = 229874

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=229875, nc=304765, nnz=617859, k=4] [9.1 MB]
Building sparse matrix (transition rewards)... [n=229875, nc=304765, nnz=7130, k=4] [2.1 MB]
Creating vector for state rewards... [1.8 MB]
Creating vector for inf... [1.8 MB]
Allocating iteration vectors... [2 x 1.8 MB]
TOTAL: [18.2 MB]

Starting iterations...

Iterative method: 95 iterations in 0.66 seconds (average 0.003411, setup 0.34)

Value in the initial state: 0.06383612031110468

Time for model checking: 0.776 seconds.

Result: 0.06383612031110468 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"time"}max=? [ F "deadlock" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=4,Objy1=2,Objx2=6,Objy2=0

Prob0E: 4 iterations in 0.02 seconds (average 0.005000, setup 0.00)

Prob1A: 1 iterations in 0.00 seconds (average 0.004000, setup 0.00)

goal = 1, inf = 0, maybe = 229874

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=229875, nc=304765, nnz=617859, k=4] [9.1 MB]
Building sparse matrix (transition rewards)... [n=229875, nc=304765, nnz=115125, k=4] [3.4 MB]
Creating vector for state rewards... [1.8 MB]
Creating vector for inf... [1.8 MB]
Allocating iteration vectors... [2 x 1.8 MB]
TOTAL: [19.5 MB]

Starting iterations...

Iterative method: 94 iterations in 0.68 seconds (average 0.003404, setup 0.36)

Value in the initial state: 180.95554974181945

Time for model checking: 0.727 seconds.

Result: 180.95554974181945 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"time"}min=? [ F "deadlock" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=4,Objy1=2,Objx2=6,Objy2=0

Prob0A: 4 iterations in 0.02 seconds (average 0.005000, setup 0.00)

Prob1E: 5 iterations in 0.03 seconds (average 0.006400, setup 0.00)

Warning: PRISM hasn't checked for zero-reward loops.
Your minimum rewards may be too low...

goal = 1, inf = 0, maybe = 229874

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=229875, nc=304765, nnz=617859, k=4] [9.1 MB]
Building sparse matrix (transition rewards)... [n=229875, nc=304765, nnz=115125, k=4] [3.4 MB]
Creating vector for state rewards... [1.8 MB]
Creating vector for inf... [1.8 MB]
Allocating iteration vectors... [2 x 1.8 MB]
TOTAL: [19.5 MB]

Starting iterations...

Iterative method: 101 iterations in 0.69 seconds (average 0.003366, setup 0.35)

Value in the initial state: 108.79334412768547

Time for model checking: 0.762 seconds.

Result: 108.79334412768547 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"success"}max=? [ F "deadlock" ]/Pmin=? [ F "MissionSuccessful" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=4,Objy1=2,Objx2=6,Objy2=0

Prob0E: 4 iterations in 0.02 seconds (average 0.005000, setup 0.00)

Prob1A: 1 iterations in 0.00 seconds (average 0.004000, setup 0.00)

goal = 1, inf = 0, maybe = 229874

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=229875, nc=304765, nnz=617859, k=4] [9.1 MB]
Building sparse matrix (transition rewards)... [n=229875, nc=304765, nnz=1854, k=4] [2.1 MB]
Creating vector for state rewards... [1.8 MB]
Creating vector for inf... [1.8 MB]
Allocating iteration vectors... [2 x 1.8 MB]
TOTAL: [18.2 MB]

Starting iterations...

Iterative method: 97 iterations in 0.62 seconds (average 0.003175, setup 0.32)

Prob0E: 54 iterations in 0.30 seconds (average 0.005556, setup 0.00)

Prob1A: 3 iterations in 0.03 seconds (average 0.010667, setup 0.00)

yes = 941, no = 200514, maybe = 28420

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=229875, nc=49074, nnz=139541, k=2] [1.9 MB]
Creating vector for yes... [1.8 MB]
Allocating iteration vectors... [2 x 1.8 MB]
TOTAL: [7.1 MB]

Starting iterations...

Iterative method: 90 iterations in 0.26 seconds (average 0.001333, setup 0.14)

Value in the initial state: 241.5120633082687

Time for model checking: 1.266 seconds.

Result: 241.5120633082687 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"success"}min=? [ F "deadlock" ]/Pmax=? [ F "MissionSuccessful" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=4,Objy1=2,Objx2=6,Objy2=0

Prob0A: 4 iterations in 0.07 seconds (average 0.017000, setup 0.00)

Prob1E: 5 iterations in 0.04 seconds (average 0.007200, setup 0.00)

Warning: PRISM hasn't checked for zero-reward loops.
Your minimum rewards may be too low...

goal = 1, inf = 0, maybe = 229874

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=229875, nc=304765, nnz=617859, k=4] [9.1 MB]
Building sparse matrix (transition rewards)... [n=229875, nc=304765, nnz=1854, k=4] [2.1 MB]
Creating vector for state rewards... [1.8 MB]
Creating vector for inf... [1.8 MB]
Allocating iteration vectors... [2 x 1.8 MB]
TOTAL: [18.2 MB]

Starting iterations...

Iterative method: 94 iterations in 0.63 seconds (average 0.003149, setup 0.33)

Prob0A: 52 iterations in 0.27 seconds (average 0.005231, setup 0.00)

Prob1E: 4 iterations in 0.03 seconds (average 0.008000, setup 0.00)

yes = 941, no = 194692, maybe = 34242

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=229875, nc=59326, nnz=169842, k=2] [2.2 MB]
Creating vector for yes... [1.8 MB]
Allocating iteration vectors... [2 x 1.8 MB]
TOTAL: [7.5 MB]

Starting iterations...

Iterative method: 96 iterations in 0.32 seconds (average 0.001542, setup 0.17)

Value in the initial state: 81.90080423834034

Time for model checking: 1.388 seconds.

Result: 81.90080423834034 (value in the initial state)

---------------------------------------------------------------------

Note: There were 16 warnings during computation.

