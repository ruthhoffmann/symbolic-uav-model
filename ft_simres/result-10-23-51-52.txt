PRISM
=====

Version: 4.3
Date: Tue Aug 02 12:43:38 BST 2016
Hostname: eglinton
Memory limits: cudd=1g, java(heap)=981.5m
Command line: prism uav_sim_ft_mdp.prism -s uav_mdp.props -const 'Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=5,Objy1=1,Objx2=5,Objy2=2,'

Parsing model file "uav_sim_ft_mdp.prism"...

Parsing properties file "uav_mdp.props"...

18 properties:
(1) Pmax=? [ F "MissionSuccessful" ]
(2) Pmin=? [ F "MissionSuccessful" ]
(3) Pmax=? [ F "MissionTimeUp" ]
(4) Pmin=? [ F "MissionTimeUp" ]
(5) Pmax=? [ F "Crash" ]
(6) Pmin=? [ F "Crash" ]
(7) Pmax=? [ F "AllAreaSearched" ]
(8) Pmin=? [ F "AllAreaSearched" ]
(9) Pmax=? [ F "MissionTimeUp"|"AllAreaSearched"|"Crash" ]
(10) Pmin=? [ F "MissionTimeUp"|"AllAreaSearched"|"Crash" ]
(11) R{"drop"}max=? [ F "deadlock" ]
(12) R{"drop"}min=? [ F "deadlock" ]
(13) R{"charge"}max=? [ F "deadlock" ]
(14) R{"charge"}min=? [ F "deadlock" ]
(15) R{"time"}max=? [ F "deadlock" ]
(16) R{"time"}min=? [ F "deadlock" ]
(17) R{"success"}max=? [ F "deadlock" ]/Pmin=? [ F "MissionSuccessful" ]
(18) R{"success"}min=? [ F "deadlock" ]/Pmax=? [ F "MissionSuccessful" ]

Type:        MDP
Modules:     Obj Move Robot T 
Variables:   NoOfObjs posx posy s1 retx rety b t c 

---------------------------------------------------------------------

Model checking: Pmax=? [ F "MissionSuccessful" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=5,Objy1=1,Objx2=5,Objy2=2

Building model...
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=5,Objy1=1,Objx2=5,Objy2=2

Warning: Guard for command 7 of module "Move" is never satisfied.

Warning: Guard for command 8 of module "Move" is never satisfied.

Warning: Guard for command 9 of module "Move" is never satisfied.

Warning: Guard for command 7 of module "Robot" is never satisfied.

Warning: Update 2 of command 3 of module "Robot" doesn't do anything ("(s1'=13)", line 249, column 44)

Warning: Update 2 of command 27 of module "T" doesn't do anything ("(c'=1)", line 372, column 108)

Warning: Guard for command 28 of module "T" is never satisfied.

Warning: Update 2 of command 9 of module "T" doesn't do anything ("(c'=1)", line 349, column 112)

Warning: Guard for command 10 of module "T" is never satisfied.

Warning: Update 2 of command 51 of module "T" doesn't do anything ("(c'=1)", line 403, column 113)

Warning: Guard for command 52 of module "T" is never satisfied.

Computing reachable states...

Reachability (BFS): 78 iterations in 0.88 seconds (average 0.011282, setup 0.00)

Time for model construction: 1.915 seconds.

Warning: Deadlocks detected and fixed in 1 states

Type:        MDP
States:      232477 (1 initial)
Transitions: 634441
Choices:     310400

Transition matrix: 173752 nodes (76 terminal), 634441 minterms, vars: 34r/34c/18nd

Prob0A: 51 iterations in 0.38 seconds (average 0.007451, setup 0.00)

Prob1E: 4 iterations in 0.06 seconds (average 0.015000, setup 0.00)

yes = 933, no = 195995, maybe = 35549

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=232477, nc=61975, nnz=178566, k=2] [2.3 MB]
Creating vector for yes... [1.8 MB]
Allocating iteration vectors... [2 x 1.8 MB]
TOTAL: [7.6 MB]

Starting iterations...

Iterative method: 95 iterations in 0.43 seconds (average 0.001600, setup 0.28)

Value in the initial state: 0.9781884602611849

Time for model checking: 0.921 seconds.

Result: 0.9781884602611849 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmin=? [ F "MissionSuccessful" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=5,Objy1=1,Objx2=5,Objy2=2

Prob0E: 53 iterations in 0.46 seconds (average 0.008679, setup 0.00)

Prob1A: 3 iterations in 0.04 seconds (average 0.014667, setup 0.00)

yes = 933, no = 201368, maybe = 30176

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=232477, nc=52468, nnz=150671, k=2] [2.0 MB]
Creating vector for yes... [1.8 MB]
Allocating iteration vectors... [2 x 1.8 MB]
TOTAL: [7.3 MB]

Starting iterations...

Iterative method: 89 iterations in 0.31 seconds (average 0.001483, setup 0.18)

Value in the initial state: 0.7291250234408883

Time for model checking: 0.851 seconds.

Result: 0.7291250234408883 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmax=? [ F "MissionTimeUp" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=5,Objy1=1,Objx2=5,Objy2=2

Prob0A: 60 iterations in 0.44 seconds (average 0.007400, setup 0.00)

Prob1E: 4 iterations in 0.04 seconds (average 0.009000, setup 0.00)

yes = 428, no = 135905, maybe = 96144

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=232477, nc=164181, nnz=456193, k=4] [5.6 MB]
Creating vector for yes... [1.8 MB]
Allocating iteration vectors... [2 x 1.8 MB]
TOTAL: [10.9 MB]

Starting iterations...

Iterative method: 91 iterations in 0.44 seconds (average 0.001978, setup 0.26)

Value in the initial state: 0.0014452719725210554

Time for model checking: 0.933 seconds.

Result: 0.0014452719725210554 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmin=? [ F "MissionTimeUp" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=5,Objy1=1,Objx2=5,Objy2=2

Prob0E: 68 iterations in 0.63 seconds (average 0.009235, setup 0.00)

Prob1A: 3 iterations in 0.04 seconds (average 0.013333, setup 0.00)

yes = 428, no = 136300, maybe = 95749

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=232477, nc=163467, nnz=454806, k=4] [5.6 MB]
Creating vector for yes... [1.8 MB]
Allocating iteration vectors... [2 x 1.8 MB]
TOTAL: [10.9 MB]

Starting iterations...

Iterative method: 107 iterations in 0.47 seconds (average 0.002019, setup 0.26)

Value in the initial state: 1.171833275941428E-5

Time for model checking: 1.166 seconds.

Result: 1.171833275941428E-5 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmax=? [ F "Crash" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=5,Objy1=1,Objx2=5,Objy2=2

Prob0A: 3 iterations in 0.06 seconds (average 0.021333, setup 0.00)

Prob1E: 198 iterations in 2.00 seconds (average 0.010121, setup 0.00)

yes = 113745, no = 2598, maybe = 116134

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=232477, nc=194057, nnz=518098, k=4] [6.3 MB]
Creating vector for yes... [1.8 MB]
Allocating iteration vectors... [2 x 1.8 MB]
TOTAL: [11.7 MB]

Starting iterations...

Iterative method: 90 iterations in 0.49 seconds (average 0.002133, setup 0.30)

Value in the initial state: 0.03288033871654208

Time for model checking: 2.589 seconds.

Result: 0.03288033871654208 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmin=? [ F "Crash" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=5,Objy1=1,Objx2=5,Objy2=2

Prob0E: 3 iterations in 0.05 seconds (average 0.017333, setup 0.00)

Prob1A: 45 iterations in 0.41 seconds (average 0.009067, setup 0.00)

yes = 113745, no = 2598, maybe = 116134

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=232477, nc=194057, nnz=518098, k=4] [6.3 MB]
Creating vector for yes... [1.8 MB]
Allocating iteration vectors... [2 x 1.8 MB]
TOTAL: [11.7 MB]

Starting iterations...

Iterative method: 96 iterations in 0.45 seconds (average 0.002125, setup 0.24)

Value in the initial state: 0.019805737936933368

Time for model checking: 0.943 seconds.

Result: 0.019805737936933368 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmax=? [ F "AllAreaSearched" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=5,Objy1=1,Objx2=5,Objy2=2

Prob0A: 56 iterations in 0.38 seconds (average 0.006857, setup 0.00)

Prob1E: 4 iterations in 0.03 seconds (average 0.008000, setup 0.00)

yes = 1236, no = 182838, maybe = 48403

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=232477, nc=77562, nnz=210891, k=2] [2.7 MB]
Creating vector for yes... [1.8 MB]
Allocating iteration vectors... [2 x 1.8 MB]
TOTAL: [8.0 MB]

Starting iterations...

Iterative method: 89 iterations in 0.29 seconds (average 0.001573, setup 0.15)

Value in the initial state: 0.24133938945258393

Time for model checking: 0.731 seconds.

Result: 0.24133938945258393 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmin=? [ F "AllAreaSearched" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=5,Objy1=1,Objx2=5,Objy2=2

Prob0E: 64 iterations in 0.30 seconds (average 0.004750, setup 0.00)

Prob1A: 3 iterations in 0.03 seconds (average 0.009333, setup 0.00)

yes = 1236, no = 189774, maybe = 41467

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=232477, nc=66013, nnz=178554, k=2] [2.3 MB]
Creating vector for yes... [1.8 MB]
Allocating iteration vectors... [2 x 1.8 MB]
TOTAL: [7.6 MB]

Starting iterations...

Iterative method: 92 iterations in 0.26 seconds (average 0.001522, setup 0.12)

Value in the initial state: 0.001753559120607421

Time for model checking: 0.607 seconds.

Result: 0.001753559120607421 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmax=? [ F "MissionTimeUp"|"AllAreaSearched"|"Crash" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=5,Objy1=1,Objx2=5,Objy2=2

Prob0A: 3 iterations in 0.09 seconds (average 0.029333, setup 0.00)

Prob1E: 208 iterations in 1.87 seconds (average 0.009000, setup 0.00)

yes = 201367, no = 934, maybe = 30176

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=232477, nc=52468, nnz=150671, k=2] [2.0 MB]
Creating vector for yes... [1.8 MB]
Allocating iteration vectors... [2 x 1.8 MB]
TOTAL: [7.3 MB]

Starting iterations...

Iterative method: 83 iterations in 0.28 seconds (average 0.001398, setup 0.17)

Value in the initial state: 0.2708745187517514

Time for model checking: 2.253 seconds.

Result: 0.2708745187517514 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmin=? [ F "MissionTimeUp"|"AllAreaSearched"|"Crash" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=5,Objy1=1,Objx2=5,Objy2=2

Prob0E: 3 iterations in 0.04 seconds (average 0.014667, setup 0.00)

Prob1A: 51 iterations in 0.35 seconds (average 0.006824, setup 0.00)

yes = 195994, no = 934, maybe = 35549

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=232477, nc=61975, nnz=178566, k=2] [2.3 MB]
Creating vector for yes... [1.8 MB]
Allocating iteration vectors... [2 x 1.8 MB]
TOTAL: [7.6 MB]

Starting iterations...

Iterative method: 92 iterations in 0.30 seconds (average 0.001522, setup 0.16)

Value in the initial state: 0.021810954153151166

Time for model checking: 0.705 seconds.

Result: 0.021810954153151166 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"drop"}max=? [ F "deadlock" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=5,Objy1=1,Objx2=5,Objy2=2

Prob0E: 4 iterations in 0.05 seconds (average 0.012000, setup 0.00)

Prob1A: 1 iterations in 0.00 seconds (average 0.004000, setup 0.00)

goal = 1, inf = 0, maybe = 232476

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=232477, nc=310399, nnz=634440, k=4] [9.3 MB]
Building sparse matrix (transition rewards)... [n=232477, nc=310399, nnz=0, k=4] [2.1 MB]
Creating vector for state rewards... [1.8 MB]
Creating vector for inf... [1.8 MB]
Allocating iteration vectors... [2 x 1.8 MB]
TOTAL: [18.5 MB]

Starting iterations...

Iterative method: 92 iterations in 0.69 seconds (average 0.003304, setup 0.38)

Value in the initial state: 0.24538027188720626

Time for model checking: 0.763 seconds.

Result: 0.24538027188720626 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"drop"}min=? [ F "deadlock" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=5,Objy1=1,Objx2=5,Objy2=2

Prob0A: 4 iterations in 0.05 seconds (average 0.012000, setup 0.00)

Prob1E: 5 iterations in 0.04 seconds (average 0.007200, setup 0.00)

Warning: PRISM hasn't checked for zero-reward loops.
Your minimum rewards may be too low...

goal = 1, inf = 0, maybe = 232476

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=232477, nc=310399, nnz=634440, k=4] [9.3 MB]
Building sparse matrix (transition rewards)... [n=232477, nc=310399, nnz=0, k=4] [2.1 MB]
Creating vector for state rewards... [1.8 MB]
Creating vector for inf... [1.8 MB]
Allocating iteration vectors... [2 x 1.8 MB]
TOTAL: [18.5 MB]

Starting iterations...

Iterative method: 83 iterations in 0.59 seconds (average 0.003325, setup 0.31)

Value in the initial state: 0.20510950092366162

Time for model checking: 0.684 seconds.

Result: 0.20510950092366162 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"charge"}max=? [ F "deadlock" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=5,Objy1=1,Objx2=5,Objy2=2

Prob0E: 4 iterations in 0.02 seconds (average 0.005000, setup 0.00)

Prob1A: 1 iterations in 0.00 seconds (average 0.004000, setup 0.00)

goal = 1, inf = 0, maybe = 232476

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=232477, nc=310399, nnz=634440, k=4] [9.3 MB]
Building sparse matrix (transition rewards)... [n=232477, nc=310399, nnz=7514, k=4] [2.2 MB]
Creating vector for state rewards... [1.8 MB]
Creating vector for inf... [1.8 MB]
Allocating iteration vectors... [2 x 1.8 MB]
TOTAL: [18.6 MB]

Starting iterations...

Iterative method: 86 iterations in 0.59 seconds (average 0.003302, setup 0.30)

Value in the initial state: 0.9827294278543918

Time for model checking: 0.636 seconds.

Result: 0.9827294278543918 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"charge"}min=? [ F "deadlock" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=5,Objy1=1,Objx2=5,Objy2=2

Prob0A: 4 iterations in 0.02 seconds (average 0.005000, setup 0.00)

Prob1E: 5 iterations in 0.04 seconds (average 0.008000, setup 0.00)

Warning: PRISM hasn't checked for zero-reward loops.
Your minimum rewards may be too low...

goal = 1, inf = 0, maybe = 232476

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=232477, nc=310399, nnz=634440, k=4] [9.3 MB]
Building sparse matrix (transition rewards)... [n=232477, nc=310399, nnz=7514, k=4] [2.2 MB]
Creating vector for state rewards... [1.8 MB]
Creating vector for inf... [1.8 MB]
Allocating iteration vectors... [2 x 1.8 MB]
TOTAL: [18.6 MB]

Starting iterations...

Iterative method: 96 iterations in 0.63 seconds (average 0.003292, setup 0.32)

Value in the initial state: 0.07117709704426196

Time for model checking: 0.708 seconds.

Result: 0.07117709704426196 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"time"}max=? [ F "deadlock" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=5,Objy1=1,Objx2=5,Objy2=2

Prob0E: 4 iterations in 0.02 seconds (average 0.006000, setup 0.00)

Prob1A: 1 iterations in 0.00 seconds (average 0.004000, setup 0.00)

goal = 1, inf = 0, maybe = 232476

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=232477, nc=310399, nnz=634440, k=4] [9.3 MB]
Building sparse matrix (transition rewards)... [n=232477, nc=310399, nnz=116341, k=4] [3.4 MB]
Creating vector for state rewards... [1.8 MB]
Creating vector for inf... [1.8 MB]
Allocating iteration vectors... [2 x 1.8 MB]
TOTAL: [19.8 MB]

Starting iterations...

Iterative method: 92 iterations in 0.67 seconds (average 0.003391, setup 0.36)

Value in the initial state: 181.28746353416537

Time for model checking: 0.712 seconds.

Result: 181.28746353416537 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"time"}min=? [ F "deadlock" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=5,Objy1=1,Objx2=5,Objy2=2

Prob0A: 4 iterations in 0.02 seconds (average 0.004000, setup 0.00)

Prob1E: 5 iterations in 0.04 seconds (average 0.007200, setup 0.00)

Warning: PRISM hasn't checked for zero-reward loops.
Your minimum rewards may be too low...

goal = 1, inf = 0, maybe = 232476

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=232477, nc=310399, nnz=634440, k=4] [9.3 MB]
Building sparse matrix (transition rewards)... [n=232477, nc=310399, nnz=116341, k=4] [3.4 MB]
Creating vector for state rewards... [1.8 MB]
Creating vector for inf... [1.8 MB]
Allocating iteration vectors... [2 x 1.8 MB]
TOTAL: [19.8 MB]

Starting iterations...

Iterative method: 101 iterations in 0.70 seconds (average 0.003446, setup 0.35)

Value in the initial state: 109.6629339078171

Time for model checking: 0.771 seconds.

Result: 109.6629339078171 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"success"}max=? [ F "deadlock" ]/Pmin=? [ F "MissionSuccessful" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=5,Objy1=1,Objx2=5,Objy2=2

Prob0E: 4 iterations in 0.02 seconds (average 0.006000, setup 0.00)

Prob1A: 1 iterations in 0.00 seconds (average 0.004000, setup 0.00)

goal = 1, inf = 0, maybe = 232476

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=232477, nc=310399, nnz=634440, k=4] [9.3 MB]
Building sparse matrix (transition rewards)... [n=232477, nc=310399, nnz=1838, k=4] [2.1 MB]
Creating vector for state rewards... [1.8 MB]
Creating vector for inf... [1.8 MB]
Allocating iteration vectors... [2 x 1.8 MB]
TOTAL: [18.5 MB]

Starting iterations...

Iterative method: 95 iterations in 0.62 seconds (average 0.003200, setup 0.32)

Prob0E: 53 iterations in 0.36 seconds (average 0.006717, setup 0.00)

Prob1A: 3 iterations in 0.03 seconds (average 0.010667, setup 0.00)

yes = 933, no = 201368, maybe = 30176

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=232477, nc=52468, nnz=150671, k=2] [2.0 MB]
Creating vector for yes... [1.8 MB]
Allocating iteration vectors... [2 x 1.8 MB]
TOTAL: [7.3 MB]

Starting iterations...

Iterative method: 89 iterations in 0.26 seconds (average 0.001438, setup 0.13)

Value in the initial state: 243.31002695371637

Time for model checking: 1.326 seconds.

Result: 243.31002695371637 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"success"}min=? [ F "deadlock" ]/Pmax=? [ F "MissionSuccessful" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=5,Objy1=1,Objx2=5,Objy2=2

Prob0A: 4 iterations in 0.10 seconds (average 0.025000, setup 0.00)

Prob1E: 5 iterations in 0.04 seconds (average 0.008000, setup 0.00)

Warning: PRISM hasn't checked for zero-reward loops.
Your minimum rewards may be too low...

goal = 1, inf = 0, maybe = 232476

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=232477, nc=310399, nnz=634440, k=4] [9.3 MB]
Building sparse matrix (transition rewards)... [n=232477, nc=310399, nnz=1838, k=4] [2.1 MB]
Creating vector for state rewards... [1.8 MB]
Creating vector for inf... [1.8 MB]
Allocating iteration vectors... [2 x 1.8 MB]
TOTAL: [18.5 MB]

Starting iterations...

Iterative method: 94 iterations in 0.77 seconds (average 0.003404, setup 0.45)

Prob0A: 51 iterations in 0.25 seconds (average 0.004863, setup 0.00)

Prob1E: 4 iterations in 0.06 seconds (average 0.016000, setup 0.00)

yes = 933, no = 195995, maybe = 35549

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=232477, nc=61975, nnz=178566, k=2] [2.3 MB]
Creating vector for yes... [1.8 MB]
Allocating iteration vectors... [2 x 1.8 MB]
TOTAL: [7.6 MB]

Starting iterations...

Iterative method: 95 iterations in 0.33 seconds (average 0.001432, setup 0.19)

Value in the initial state: 80.56644465385149

Time for model checking: 1.559 seconds.

Result: 80.56644465385149 (value in the initial state)

---------------------------------------------------------------------

Note: There were 16 warnings during computation.

