PRISM
=====

Version: 4.3
Date: Tue Aug 02 12:25:33 BST 2016
Hostname: eglinton
Memory limits: cudd=1g, java(heap)=981.5m
Command line: prism uav_sim_ft_mdp.prism -s uav_mdp.props -const 'Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=1,Objy1=1,Objx2=4,Objy2=1,'

Parsing model file "uav_sim_ft_mdp.prism"...

Parsing properties file "uav_mdp.props"...

18 properties:
(1) Pmax=? [ F "MissionSuccessful" ]
(2) Pmin=? [ F "MissionSuccessful" ]
(3) Pmax=? [ F "MissionTimeUp" ]
(4) Pmin=? [ F "MissionTimeUp" ]
(5) Pmax=? [ F "Crash" ]
(6) Pmin=? [ F "Crash" ]
(7) Pmax=? [ F "AllAreaSearched" ]
(8) Pmin=? [ F "AllAreaSearched" ]
(9) Pmax=? [ F "MissionTimeUp"|"AllAreaSearched"|"Crash" ]
(10) Pmin=? [ F "MissionTimeUp"|"AllAreaSearched"|"Crash" ]
(11) R{"drop"}max=? [ F "deadlock" ]
(12) R{"drop"}min=? [ F "deadlock" ]
(13) R{"charge"}max=? [ F "deadlock" ]
(14) R{"charge"}min=? [ F "deadlock" ]
(15) R{"time"}max=? [ F "deadlock" ]
(16) R{"time"}min=? [ F "deadlock" ]
(17) R{"success"}max=? [ F "deadlock" ]/Pmin=? [ F "MissionSuccessful" ]
(18) R{"success"}min=? [ F "deadlock" ]/Pmax=? [ F "MissionSuccessful" ]

Type:        MDP
Modules:     Obj Move Robot T 
Variables:   NoOfObjs posx posy s1 retx rety b t c 

---------------------------------------------------------------------

Model checking: Pmax=? [ F "MissionSuccessful" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=1,Objy1=1,Objx2=4,Objy2=1

Building model...
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=1,Objy1=1,Objx2=4,Objy2=1

Warning: Guard for command 7 of module "Move" is never satisfied.

Warning: Guard for command 8 of module "Move" is never satisfied.

Warning: Guard for command 9 of module "Move" is never satisfied.

Warning: Guard for command 7 of module "Robot" is never satisfied.

Warning: Update 2 of command 3 of module "Robot" doesn't do anything ("(s1'=13)", line 249, column 44)

Warning: Update 2 of command 27 of module "T" doesn't do anything ("(c'=1)", line 372, column 108)

Warning: Guard for command 28 of module "T" is never satisfied.

Warning: Update 2 of command 9 of module "T" doesn't do anything ("(c'=1)", line 349, column 112)

Warning: Guard for command 10 of module "T" is never satisfied.

Warning: Update 2 of command 51 of module "T" doesn't do anything ("(c'=1)", line 403, column 113)

Warning: Guard for command 52 of module "T" is never satisfied.

Computing reachable states...

Reachability (BFS): 81 iterations in 1.01 seconds (average 0.012494, setup 0.00)

Time for model construction: 2.798 seconds.

Warning: Deadlocks detected and fixed in 1 states

Type:        MDP
States:      196673 (1 initial)
Transitions: 521238
Choices:     260391

Transition matrix: 166618 nodes (76 terminal), 521238 minterms, vars: 34r/34c/18nd

Prob0A: 41 iterations in 0.29 seconds (average 0.007024, setup 0.00)

Prob1E: 4 iterations in 0.05 seconds (average 0.013000, setup 0.00)

yes = 955, no = 165717, maybe = 30001

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=196673, nc=54111, nnz=155605, k=2] [2.0 MB]
Creating vector for yes... [1.5 MB]
Allocating iteration vectors... [2 x 1.5 MB]
TOTAL: [6.5 MB]

Starting iterations...

Iterative method: 85 iterations in 0.33 seconds (average 0.001365, setup 0.22)

Value in the initial state: 0.9823149418485769

Time for model checking: 0.72 seconds.

Result: 0.9823149418485769 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmin=? [ F "MissionSuccessful" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=1,Objy1=1,Objx2=4,Objy2=1

Prob0E: 42 iterations in 0.34 seconds (average 0.008190, setup 0.00)

Prob1A: 3 iterations in 0.06 seconds (average 0.020000, setup 0.00)

yes = 955, no = 170005, maybe = 25713

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=196673, nc=46230, nnz=132650, k=2] [1.7 MB]
Creating vector for yes... [1.5 MB]
Allocating iteration vectors... [2 x 1.5 MB]
TOTAL: [6.3 MB]

Starting iterations...

Iterative method: 80 iterations in 0.28 seconds (average 0.001300, setup 0.17)

Value in the initial state: 0.7532953642598168

Time for model checking: 0.709 seconds.

Result: 0.7532953642598168 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmax=? [ F "MissionTimeUp" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=1,Objy1=1,Objx2=4,Objy2=1

Prob0A: 61 iterations in 0.58 seconds (average 0.009443, setup 0.00)

Prob1E: 4 iterations in 0.04 seconds (average 0.009000, setup 0.00)

yes = 388, no = 125024, maybe = 71261

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=196673, nc=125242, nnz=348222, k=4] [4.3 MB]
Creating vector for yes... [1.5 MB]
Allocating iteration vectors... [2 x 1.5 MB]
TOTAL: [8.8 MB]

Starting iterations...

Iterative method: 89 iterations in 0.40 seconds (average 0.001708, setup 0.24)

Value in the initial state: 3.503878903594576E-4

Time for model checking: 1.028 seconds.

Result: 3.503878903594576E-4 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmin=? [ F "MissionTimeUp" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=1,Objy1=1,Objx2=4,Objy2=1

Prob0E: 69 iterations in 0.52 seconds (average 0.007536, setup 0.00)

Prob1A: 3 iterations in 0.05 seconds (average 0.017333, setup 0.00)

yes = 388, no = 125381, maybe = 70904

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=196673, nc=124604, nnz=346913, k=4] [4.3 MB]
Creating vector for yes... [1.5 MB]
Allocating iteration vectors... [2 x 1.5 MB]
TOTAL: [8.8 MB]

Starting iterations...

Iterative method: 102 iterations in 0.41 seconds (average 0.001725, setup 0.24)

Value in the initial state: 1.0737090307180765E-6

Time for model checking: 1.012 seconds.

Result: 1.0737090307180765E-6 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmax=? [ F "Crash" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=1,Objy1=1,Objx2=4,Objy2=1

Prob0A: 3 iterations in 0.08 seconds (average 0.025333, setup 0.00)

Prob1E: 165 iterations in 2.37 seconds (average 0.014352, setup 0.00)

yes = 96340, no = 2569, maybe = 97764

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=196673, nc=161482, nnz=422329, k=4] [5.2 MB]
Creating vector for yes... [1.5 MB]
Allocating iteration vectors... [2 x 1.5 MB]
TOTAL: [9.7 MB]

Starting iterations...

Iterative method: 84 iterations in 0.46 seconds (average 0.001857, setup 0.30)

Value in the initial state: 0.03000042328868348

Time for model checking: 3.112 seconds.

Result: 0.03000042328868348 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmin=? [ F "Crash" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=1,Objy1=1,Objx2=4,Objy2=1

Prob0E: 3 iterations in 0.06 seconds (average 0.018667, setup 0.00)

Prob1A: 41 iterations in 0.39 seconds (average 0.009561, setup 0.00)

yes = 96340, no = 2569, maybe = 97764

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=196673, nc=161482, nnz=422329, k=4] [5.2 MB]
Creating vector for yes... [1.5 MB]
Allocating iteration vectors... [2 x 1.5 MB]
TOTAL: [9.7 MB]

Starting iterations...

Iterative method: 79 iterations in 0.36 seconds (average 0.001873, setup 0.21)

Value in the initial state: 0.015646235860784334

Time for model checking: 0.847 seconds.

Result: 0.015646235860784334 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmax=? [ F "AllAreaSearched" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=1,Objy1=1,Objx2=4,Objy2=1

Prob0A: 53 iterations in 0.41 seconds (average 0.007774, setup 0.00)

Prob1E: 4 iterations in 0.04 seconds (average 0.011000, setup 0.00)

yes = 1225, no = 152185, maybe = 43263

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=196673, nc=65259, nnz=168756, k=2] [2.2 MB]
Creating vector for yes... [1.5 MB]
Allocating iteration vectors... [2 x 1.5 MB]
TOTAL: [6.7 MB]

Starting iterations...

Iterative method: 85 iterations in 0.30 seconds (average 0.001459, setup 0.17)

Value in the initial state: 0.21900041169197382

Time for model checking: 0.78 seconds.

Result: 0.21900041169197382 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmin=? [ F "AllAreaSearched" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=1,Objy1=1,Objx2=4,Objy2=1

Prob0E: 62 iterations in 0.42 seconds (average 0.006839, setup 0.00)

Prob1A: 3 iterations in 0.03 seconds (average 0.010667, setup 0.00)

yes = 1225, no = 157015, maybe = 38433

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=196673, nc=56597, nnz=143924, k=2] [1.9 MB]
Creating vector for yes... [1.5 MB]
Allocating iteration vectors... [2 x 1.5 MB]
TOTAL: [6.4 MB]

Starting iterations...

Iterative method: 90 iterations in 0.24 seconds (average 0.001467, setup 0.10)

Value in the initial state: 0.00175750309282579

Time for model checking: 0.728 seconds.

Result: 0.00175750309282579 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmax=? [ F "MissionTimeUp"|"AllAreaSearched"|"Crash" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=1,Objy1=1,Objx2=4,Objy2=1

Prob0A: 3 iterations in 0.09 seconds (average 0.029333, setup 0.00)

Prob1E: 164 iterations in 2.07 seconds (average 0.012610, setup 0.00)

yes = 170004, no = 956, maybe = 25713

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=196673, nc=46230, nnz=132650, k=2] [1.7 MB]
Creating vector for yes... [1.5 MB]
Allocating iteration vectors... [2 x 1.5 MB]
TOTAL: [6.3 MB]

Starting iterations...

Iterative method: 72 iterations in 0.27 seconds (average 0.001222, setup 0.18)

Value in the initial state: 0.24670435423366013

Time for model checking: 2.503 seconds.

Result: 0.24670435423366013 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmin=? [ F "MissionTimeUp"|"AllAreaSearched"|"Crash" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=1,Objy1=1,Objx2=4,Objy2=1

Prob0E: 3 iterations in 0.04 seconds (average 0.014667, setup 0.00)

Prob1A: 41 iterations in 0.30 seconds (average 0.007415, setup 0.00)

yes = 165716, no = 956, maybe = 30001

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=196673, nc=54111, nnz=155605, k=2] [2.0 MB]
Creating vector for yes... [1.5 MB]
Allocating iteration vectors... [2 x 1.5 MB]
TOTAL: [6.5 MB]

Starting iterations...

Iterative method: 83 iterations in 0.26 seconds (average 0.001349, setup 0.14)

Value in the initial state: 0.017684226896591406

Time for model checking: 0.634 seconds.

Result: 0.017684226896591406 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"drop"}max=? [ F "deadlock" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=1,Objy1=1,Objx2=4,Objy2=1

Prob0E: 4 iterations in 0.05 seconds (average 0.012000, setup 0.00)

Prob1A: 1 iterations in 0.00 seconds (average 0.004000, setup 0.00)

goal = 1, inf = 0, maybe = 196672

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=196673, nc=260390, nnz=521237, k=4] [7.7 MB]
Building sparse matrix (transition rewards)... [n=196673, nc=260390, nnz=0, k=4] [1.7 MB]
Creating vector for state rewards... [1.5 MB]
Creating vector for inf... [1.5 MB]
Allocating iteration vectors... [2 x 1.5 MB]
TOTAL: [15.5 MB]

Starting iterations...

Iterative method: 81 iterations in 0.60 seconds (average 0.002864, setup 0.36)

Value in the initial state: 0.18240065495446126

Time for model checking: 0.661 seconds.

Result: 0.18240065495446126 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"drop"}min=? [ F "deadlock" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=1,Objy1=1,Objx2=4,Objy2=1

Prob0A: 4 iterations in 0.04 seconds (average 0.009000, setup 0.00)

Prob1E: 5 iterations in 0.04 seconds (average 0.008000, setup 0.00)

Warning: PRISM hasn't checked for zero-reward loops.
Your minimum rewards may be too low...

goal = 1, inf = 0, maybe = 196672

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=196673, nc=260390, nnz=521237, k=4] [7.7 MB]
Building sparse matrix (transition rewards)... [n=196673, nc=260390, nnz=0, k=4] [1.7 MB]
Creating vector for state rewards... [1.5 MB]
Creating vector for inf... [1.5 MB]
Allocating iteration vectors... [2 x 1.5 MB]
TOTAL: [15.5 MB]

Starting iterations...

Iterative method: 73 iterations in 0.48 seconds (average 0.002795, setup 0.28)

Value in the initial state: 0.16015458337278426

Time for model checking: 0.577 seconds.

Result: 0.16015458337278426 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"charge"}max=? [ F "deadlock" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=1,Objy1=1,Objx2=4,Objy2=1

Prob0E: 4 iterations in 0.02 seconds (average 0.006000, setup 0.00)

Prob1A: 1 iterations in 0.00 seconds (average 0.004000, setup 0.00)

goal = 1, inf = 0, maybe = 196672

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=196673, nc=260390, nnz=521237, k=4] [7.7 MB]
Building sparse matrix (transition rewards)... [n=196673, nc=260390, nnz=5786, k=4] [1.8 MB]
Creating vector for state rewards... [1.5 MB]
Creating vector for inf... [1.5 MB]
Allocating iteration vectors... [2 x 1.5 MB]
TOTAL: [15.5 MB]

Starting iterations...

Iterative method: 78 iterations in 0.51 seconds (average 0.002923, setup 0.28)

Value in the initial state: 0.27399179941172974

Time for model checking: 0.548 seconds.

Result: 0.27399179941172974 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"charge"}min=? [ F "deadlock" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=1,Objy1=1,Objx2=4,Objy2=1

Prob0A: 4 iterations in 0.03 seconds (average 0.007000, setup 0.00)

Prob1E: 5 iterations in 0.04 seconds (average 0.008000, setup 0.00)

Warning: PRISM hasn't checked for zero-reward loops.
Your minimum rewards may be too low...

goal = 1, inf = 0, maybe = 196672

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=196673, nc=260390, nnz=521237, k=4] [7.7 MB]
Building sparse matrix (transition rewards)... [n=196673, nc=260390, nnz=5786, k=4] [1.8 MB]
Creating vector for state rewards... [1.5 MB]
Creating vector for inf... [1.5 MB]
Allocating iteration vectors... [2 x 1.5 MB]
TOTAL: [15.5 MB]

Starting iterations...

Iterative method: 87 iterations in 0.54 seconds (average 0.002943, setup 0.29)

Value in the initial state: 0.027867605876052897

Time for model checking: 0.619 seconds.

Result: 0.027867605876052897 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"time"}max=? [ F "deadlock" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=1,Objy1=1,Objx2=4,Objy2=1

Prob0E: 4 iterations in 0.02 seconds (average 0.006000, setup 0.00)

Prob1A: 1 iterations in 0.00 seconds (average 0.004000, setup 0.00)

goal = 1, inf = 0, maybe = 196672

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=196673, nc=260390, nnz=521237, k=4] [7.7 MB]
Building sparse matrix (transition rewards)... [n=196673, nc=260390, nnz=98907, k=4] [2.9 MB]
Creating vector for state rewards... [1.5 MB]
Creating vector for inf... [1.5 MB]
Allocating iteration vectors... [2 x 1.5 MB]
TOTAL: [16.6 MB]

Starting iterations...

Iterative method: 88 iterations in 0.58 seconds (average 0.003000, setup 0.32)

Value in the initial state: 165.53723455385116

Time for model checking: 0.624 seconds.

Result: 165.53723455385116 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"time"}min=? [ F "deadlock" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=1,Objy1=1,Objx2=4,Objy2=1

Prob0A: 4 iterations in 0.02 seconds (average 0.006000, setup 0.00)

Prob1E: 5 iterations in 0.04 seconds (average 0.008000, setup 0.00)

Warning: PRISM hasn't checked for zero-reward loops.
Your minimum rewards may be too low...

goal = 1, inf = 0, maybe = 196672

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=196673, nc=260390, nnz=521237, k=4] [7.7 MB]
Building sparse matrix (transition rewards)... [n=196673, nc=260390, nnz=98907, k=4] [2.9 MB]
Creating vector for state rewards... [1.5 MB]
Creating vector for inf... [1.5 MB]
Allocating iteration vectors... [2 x 1.5 MB]
TOTAL: [16.6 MB]

Starting iterations...

Iterative method: 91 iterations in 0.59 seconds (average 0.002989, setup 0.32)

Value in the initial state: 86.77597579102851

Time for model checking: 0.661 seconds.

Result: 86.77597579102851 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"success"}max=? [ F "deadlock" ]/Pmin=? [ F "MissionSuccessful" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=1,Objy1=1,Objx2=4,Objy2=1

Prob0E: 4 iterations in 0.02 seconds (average 0.005000, setup 0.00)

Prob1A: 1 iterations in 0.01 seconds (average 0.008000, setup 0.00)

goal = 1, inf = 0, maybe = 196672

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=196673, nc=260390, nnz=521237, k=4] [7.7 MB]
Building sparse matrix (transition rewards)... [n=196673, nc=260390, nnz=1882, k=4] [1.8 MB]
Creating vector for state rewards... [1.5 MB]
Creating vector for inf... [1.5 MB]
Allocating iteration vectors... [2 x 1.5 MB]
TOTAL: [15.5 MB]

Starting iterations...

Iterative method: 86 iterations in 0.53 seconds (average 0.002930, setup 0.28)

Prob0E: 42 iterations in 0.27 seconds (average 0.006476, setup 0.00)

Prob1A: 3 iterations in 0.04 seconds (average 0.014667, setup 0.00)

yes = 955, no = 170005, maybe = 25713

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=196673, nc=46230, nnz=132650, k=2] [1.7 MB]
Creating vector for yes... [1.5 MB]
Allocating iteration vectors... [2 x 1.5 MB]
TOTAL: [6.3 MB]

Starting iterations...

Iterative method: 80 iterations in 0.22 seconds (average 0.001300, setup 0.11)

Value in the initial state: 215.41275645597935

Time for model checking: 1.138 seconds.

Result: 215.41275645597935 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"success"}min=? [ F "deadlock" ]/Pmax=? [ F "MissionSuccessful" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=1,Objy1=1,Objx2=4,Objy2=1

Prob0A: 4 iterations in 0.03 seconds (average 0.007000, setup 0.00)

Prob1E: 5 iterations in 0.04 seconds (average 0.008000, setup 0.00)

Warning: PRISM hasn't checked for zero-reward loops.
Your minimum rewards may be too low...

goal = 1, inf = 0, maybe = 196672

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=196673, nc=260390, nnz=521237, k=4] [7.7 MB]
Building sparse matrix (transition rewards)... [n=196673, nc=260390, nnz=1882, k=4] [1.8 MB]
Creating vector for state rewards... [1.5 MB]
Creating vector for inf... [1.5 MB]
Allocating iteration vectors... [2 x 1.5 MB]
TOTAL: [15.5 MB]

Starting iterations...

Iterative method: 85 iterations in 0.55 seconds (average 0.002965, setup 0.30)

Prob0A: 41 iterations in 0.24 seconds (average 0.005951, setup 0.00)

Prob1E: 4 iterations in 0.03 seconds (average 0.007000, setup 0.00)

yes = 955, no = 165717, maybe = 30001

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=196673, nc=54111, nnz=155605, k=2] [2.0 MB]
Creating vector for yes... [1.5 MB]
Allocating iteration vectors... [2 x 1.5 MB]
TOTAL: [6.5 MB]

Starting iterations...

Iterative method: 85 iterations in 0.24 seconds (average 0.001365, setup 0.13)

Value in the initial state: 77.95281923655993

Time for model checking: 1.157 seconds.

Result: 77.95281923655993 (value in the initial state)

---------------------------------------------------------------------

Note: There were 16 warnings during computation.

