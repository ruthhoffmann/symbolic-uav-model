PRISM
=====

Version: 4.3
Date: Tue Aug 02 12:30:46 BST 2016
Hostname: eglinton
Memory limits: cudd=1g, java(heap)=981.5m
Command line: prism uav_sim_ft_mdp.prism -s uav_mdp.props -const 'Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=4,Objy1=1,Objx2=4,Objy2=2,'

Parsing model file "uav_sim_ft_mdp.prism"...

Parsing properties file "uav_mdp.props"...

18 properties:
(1) Pmax=? [ F "MissionSuccessful" ]
(2) Pmin=? [ F "MissionSuccessful" ]
(3) Pmax=? [ F "MissionTimeUp" ]
(4) Pmin=? [ F "MissionTimeUp" ]
(5) Pmax=? [ F "Crash" ]
(6) Pmin=? [ F "Crash" ]
(7) Pmax=? [ F "AllAreaSearched" ]
(8) Pmin=? [ F "AllAreaSearched" ]
(9) Pmax=? [ F "MissionTimeUp"|"AllAreaSearched"|"Crash" ]
(10) Pmin=? [ F "MissionTimeUp"|"AllAreaSearched"|"Crash" ]
(11) R{"drop"}max=? [ F "deadlock" ]
(12) R{"drop"}min=? [ F "deadlock" ]
(13) R{"charge"}max=? [ F "deadlock" ]
(14) R{"charge"}min=? [ F "deadlock" ]
(15) R{"time"}max=? [ F "deadlock" ]
(16) R{"time"}min=? [ F "deadlock" ]
(17) R{"success"}max=? [ F "deadlock" ]/Pmin=? [ F "MissionSuccessful" ]
(18) R{"success"}min=? [ F "deadlock" ]/Pmax=? [ F "MissionSuccessful" ]

Type:        MDP
Modules:     Obj Move Robot T 
Variables:   NoOfObjs posx posy s1 retx rety b t c 

---------------------------------------------------------------------

Model checking: Pmax=? [ F "MissionSuccessful" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=4,Objy1=1,Objx2=4,Objy2=2

Building model...
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=4,Objy1=1,Objx2=4,Objy2=2

Warning: Guard for command 7 of module "Move" is never satisfied.

Warning: Guard for command 8 of module "Move" is never satisfied.

Warning: Guard for command 9 of module "Move" is never satisfied.

Warning: Guard for command 7 of module "Robot" is never satisfied.

Warning: Update 2 of command 3 of module "Robot" doesn't do anything ("(s1'=13)", line 249, column 44)

Warning: Update 2 of command 27 of module "T" doesn't do anything ("(c'=1)", line 372, column 108)

Warning: Guard for command 28 of module "T" is never satisfied.

Warning: Update 2 of command 9 of module "T" doesn't do anything ("(c'=1)", line 349, column 112)

Warning: Guard for command 10 of module "T" is never satisfied.

Warning: Update 2 of command 51 of module "T" doesn't do anything ("(c'=1)", line 403, column 113)

Warning: Guard for command 52 of module "T" is never satisfied.

Computing reachable states...

Reachability (BFS): 75 iterations in 0.74 seconds (average 0.009920, setup 0.00)

Time for model construction: 1.755 seconds.

Warning: Deadlocks detected and fixed in 1 states

Type:        MDP
States:      198156 (1 initial)
Transitions: 524406
Choices:     262081

Transition matrix: 168587 nodes (76 terminal), 524406 minterms, vars: 34r/34c/18nd

Prob0A: 47 iterations in 0.32 seconds (average 0.006723, setup 0.00)

Prob1E: 4 iterations in 0.05 seconds (average 0.012000, setup 0.00)

yes = 949, no = 166422, maybe = 30785

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=198156, nc=53664, nnz=152000, k=2] [2.0 MB]
Creating vector for yes... [1.5 MB]
Allocating iteration vectors... [2 x 1.5 MB]
TOTAL: [6.5 MB]

Starting iterations...

Iterative method: 90 iterations in 0.30 seconds (average 0.001289, setup 0.18)

Value in the initial state: 0.9801795608564634

Time for model checking: 0.688 seconds.

Result: 0.9801795608564634 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmin=? [ F "MissionSuccessful" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=4,Objy1=1,Objx2=4,Objy2=2

Prob0E: 48 iterations in 0.36 seconds (average 0.007417, setup 0.00)

Prob1A: 3 iterations in 0.03 seconds (average 0.010667, setup 0.00)

yes = 949, no = 170915, maybe = 26292

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=198156, nc=45730, nnz=129061, k=2] [1.7 MB]
Creating vector for yes... [1.5 MB]
Allocating iteration vectors... [2 x 1.5 MB]
TOTAL: [6.2 MB]

Starting iterations...

Iterative method: 84 iterations in 0.25 seconds (average 0.001238, setup 0.14)

Value in the initial state: 0.7536483558214274

Time for model checking: 0.655 seconds.

Result: 0.7536483558214274 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmax=? [ F "MissionTimeUp" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=4,Objy1=1,Objx2=4,Objy2=2

Prob0A: 61 iterations in 0.40 seconds (average 0.006623, setup 0.00)

Prob1E: 4 iterations in 0.04 seconds (average 0.009000, setup 0.00)

yes = 388, no = 120454, maybe = 77314

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=198156, nc=131306, nnz=360345, k=4] [4.4 MB]
Creating vector for yes... [1.5 MB]
Allocating iteration vectors... [2 x 1.5 MB]
TOTAL: [9.0 MB]

Starting iterations...

Iterative method: 88 iterations in 0.40 seconds (average 0.001727, setup 0.25)

Value in the initial state: 7.418307418151661E-4

Time for model checking: 0.853 seconds.

Result: 7.418307418151661E-4 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmin=? [ F "MissionTimeUp" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=4,Objy1=1,Objx2=4,Objy2=2

Prob0E: 69 iterations in 0.63 seconds (average 0.009159, setup 0.00)

Prob1A: 3 iterations in 0.04 seconds (average 0.012000, setup 0.00)

yes = 388, no = 120792, maybe = 76976

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=198156, nc=130706, nnz=359075, k=4] [4.4 MB]
Creating vector for yes... [1.5 MB]
Allocating iteration vectors... [2 x 1.5 MB]
TOTAL: [9.0 MB]

Starting iterations...

Iterative method: 103 iterations in 0.40 seconds (average 0.001748, setup 0.22)

Value in the initial state: 2.066304515773752E-6

Time for model checking: 1.07 seconds.

Result: 2.066304515773752E-6 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmax=? [ F "Crash" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=4,Objy1=1,Objx2=4,Objy2=2

Prob0A: 3 iterations in 0.06 seconds (average 0.020000, setup 0.00)

Prob1E: 183 iterations in 1.90 seconds (average 0.010361, setup 0.00)

yes = 97119, no = 2586, maybe = 98451

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=198156, nc=162376, nnz=424701, k=4] [5.2 MB]
Creating vector for yes... [1.5 MB]
Allocating iteration vectors... [2 x 1.5 MB]
TOTAL: [9.7 MB]

Starting iterations...

Iterative method: 85 iterations in 0.43 seconds (average 0.001835, setup 0.27)

Value in the initial state: 0.03080623218362625

Time for model checking: 2.405 seconds.

Result: 0.03080623218362625 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmin=? [ F "Crash" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=4,Objy1=1,Objx2=4,Objy2=2

Prob0E: 3 iterations in 0.05 seconds (average 0.016000, setup 0.00)

Prob1A: 45 iterations in 0.37 seconds (average 0.008267, setup 0.00)

yes = 97119, no = 2586, maybe = 98451

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=198156, nc=162376, nnz=424701, k=4] [5.2 MB]
Creating vector for yes... [1.5 MB]
Allocating iteration vectors... [2 x 1.5 MB]
TOTAL: [9.7 MB]

Starting iterations...

Iterative method: 85 iterations in 0.37 seconds (average 0.001929, setup 0.20)

Value in the initial state: 0.0178434702021412

Time for model checking: 0.801 seconds.

Result: 0.0178434702021412 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmax=? [ F "AllAreaSearched" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=4,Objy1=1,Objx2=4,Objy2=2

Prob0A: 53 iterations in 0.35 seconds (average 0.006642, setup 0.00)

Prob1E: 4 iterations in 0.04 seconds (average 0.009000, setup 0.00)

yes = 1248, no = 153241, maybe = 43667

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=198156, nc=68467, nnz=181337, k=2] [2.3 MB]
Creating vector for yes... [1.5 MB]
Allocating iteration vectors... [2 x 1.5 MB]
TOTAL: [6.9 MB]

Starting iterations...

Iterative method: 85 iterations in 0.28 seconds (average 0.001412, setup 0.16)

Value in the initial state: 0.2185931862704482

Time for model checking: 0.678 seconds.

Result: 0.2185931862704482 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmin=? [ F "AllAreaSearched" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=4,Objy1=1,Objx2=4,Objy2=2

Prob0E: 62 iterations in 0.37 seconds (average 0.005935, setup 0.00)

Prob1A: 3 iterations in 0.04 seconds (average 0.012000, setup 0.00)

yes = 1248, no = 158851, maybe = 38057

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=198156, nc=59025, nnz=154945, k=2] [2.0 MB]
Creating vector for yes... [1.5 MB]
Allocating iteration vectors... [2 x 1.5 MB]
TOTAL: [6.6 MB]

Starting iterations...

Iterative method: 91 iterations in 0.25 seconds (average 0.001319, setup 0.13)

Value in the initial state: 0.0017657576067771938

Time for model checking: 0.666 seconds.

Result: 0.0017657576067771938 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmax=? [ F "MissionTimeUp"|"AllAreaSearched"|"Crash" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=4,Objy1=1,Objx2=4,Objy2=2

Prob0A: 3 iterations in 0.06 seconds (average 0.020000, setup 0.00)

Prob1E: 188 iterations in 1.76 seconds (average 0.009362, setup 0.00)

yes = 170914, no = 950, maybe = 26292

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=198156, nc=45730, nnz=129061, k=2] [1.7 MB]
Creating vector for yes... [1.5 MB]
Allocating iteration vectors... [2 x 1.5 MB]
TOTAL: [6.2 MB]

Starting iterations...

Iterative method: 78 iterations in 0.26 seconds (average 0.001231, setup 0.16)

Value in the initial state: 0.24635147577289185

Time for model checking: 2.11 seconds.

Result: 0.24635147577289185 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmin=? [ F "MissionTimeUp"|"AllAreaSearched"|"Crash" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=4,Objy1=1,Objx2=4,Objy2=2

Prob0E: 3 iterations in 0.04 seconds (average 0.013333, setup 0.00)

Prob1A: 47 iterations in 0.32 seconds (average 0.006723, setup 0.00)

yes = 166421, no = 950, maybe = 30785

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=198156, nc=53664, nnz=152000, k=2] [2.0 MB]
Creating vector for yes... [1.5 MB]
Allocating iteration vectors... [2 x 1.5 MB]
TOTAL: [6.5 MB]

Starting iterations...

Iterative method: 88 iterations in 0.25 seconds (average 0.001273, setup 0.14)

Value in the initial state: 0.019819890462588807

Time for model checking: 0.631 seconds.

Result: 0.019819890462588807 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"drop"}max=? [ F "deadlock" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=4,Objy1=1,Objx2=4,Objy2=2

Prob0E: 4 iterations in 0.06 seconds (average 0.014000, setup 0.00)

Prob1A: 1 iterations in 0.00 seconds (average 0.004000, setup 0.00)

goal = 1, inf = 0, maybe = 198155

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=198156, nc=262080, nnz=524405, k=4] [7.8 MB]
Building sparse matrix (transition rewards)... [n=198156, nc=262080, nnz=0, k=4] [1.8 MB]
Creating vector for state rewards... [1.5 MB]
Creating vector for inf... [1.5 MB]
Allocating iteration vectors... [2 x 1.5 MB]
TOTAL: [15.6 MB]

Starting iterations...

Iterative method: 86 iterations in 0.60 seconds (average 0.002884, setup 0.36)

Value in the initial state: 0.18201120922436367

Time for model checking: 0.67 seconds.

Result: 0.18201120922436367 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"drop"}min=? [ F "deadlock" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=4,Objy1=1,Objx2=4,Objy2=2

Prob0A: 4 iterations in 0.03 seconds (average 0.008000, setup 0.00)

Prob1E: 5 iterations in 0.04 seconds (average 0.007200, setup 0.00)

Warning: PRISM hasn't checked for zero-reward loops.
Your minimum rewards may be too low...

goal = 1, inf = 0, maybe = 198155

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=198156, nc=262080, nnz=524405, k=4] [7.8 MB]
Building sparse matrix (transition rewards)... [n=198156, nc=262080, nnz=0, k=4] [1.8 MB]
Creating vector for state rewards... [1.5 MB]
Creating vector for inf... [1.5 MB]
Allocating iteration vectors... [2 x 1.5 MB]
TOTAL: [15.6 MB]

Starting iterations...

Iterative method: 78 iterations in 0.50 seconds (average 0.002769, setup 0.28)

Value in the initial state: 0.1569882540082519

Time for model checking: 0.574 seconds.

Result: 0.1569882540082519 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"charge"}max=? [ F "deadlock" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=4,Objy1=1,Objx2=4,Objy2=2

Prob0E: 4 iterations in 0.02 seconds (average 0.004000, setup 0.00)

Prob1A: 1 iterations in 0.00 seconds (average 0.004000, setup 0.00)

goal = 1, inf = 0, maybe = 198155

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=198156, nc=262080, nnz=524405, k=4] [7.8 MB]
Building sparse matrix (transition rewards)... [n=198156, nc=262080, nnz=5786, k=4] [1.8 MB]
Creating vector for state rewards... [1.5 MB]
Creating vector for inf... [1.5 MB]
Allocating iteration vectors... [2 x 1.5 MB]
TOTAL: [15.6 MB]

Starting iterations...

Iterative method: 83 iterations in 0.51 seconds (average 0.002795, setup 0.28)

Value in the initial state: 0.2745800067000481

Time for model checking: 0.548 seconds.

Result: 0.2745800067000481 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"charge"}min=? [ F "deadlock" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=4,Objy1=1,Objx2=4,Objy2=2

Prob0A: 4 iterations in 0.02 seconds (average 0.005000, setup 0.00)

Prob1E: 5 iterations in 0.04 seconds (average 0.007200, setup 0.00)

Warning: PRISM hasn't checked for zero-reward loops.
Your minimum rewards may be too low...

goal = 1, inf = 0, maybe = 198155

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=198156, nc=262080, nnz=524405, k=4] [7.8 MB]
Building sparse matrix (transition rewards)... [n=198156, nc=262080, nnz=5786, k=4] [1.8 MB]
Creating vector for state rewards... [1.5 MB]
Creating vector for inf... [1.5 MB]
Allocating iteration vectors... [2 x 1.5 MB]
TOTAL: [15.6 MB]

Starting iterations...

Iterative method: 91 iterations in 0.54 seconds (average 0.002857, setup 0.28)

Value in the initial state: 0.0517563628105878

Time for model checking: 0.605 seconds.

Result: 0.0517563628105878 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"time"}max=? [ F "deadlock" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=4,Objy1=1,Objx2=4,Objy2=2

Prob0E: 4 iterations in 0.02 seconds (average 0.005000, setup 0.00)

Prob1A: 1 iterations in 0.00 seconds (average 0.004000, setup 0.00)

goal = 1, inf = 0, maybe = 198155

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=198156, nc=262080, nnz=524405, k=4] [7.8 MB]
Building sparse matrix (transition rewards)... [n=198156, nc=262080, nnz=99703, k=4] [2.9 MB]
Creating vector for state rewards... [1.5 MB]
Creating vector for inf... [1.5 MB]
Allocating iteration vectors... [2 x 1.5 MB]
TOTAL: [16.7 MB]

Starting iterations...

Iterative method: 87 iterations in 0.56 seconds (average 0.002897, setup 0.31)

Value in the initial state: 169.9629528789803

Time for model checking: 0.599 seconds.

Result: 169.9629528789803 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"time"}min=? [ F "deadlock" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=4,Objy1=1,Objx2=4,Objy2=2

Prob0A: 4 iterations in 0.02 seconds (average 0.005000, setup 0.00)

Prob1E: 5 iterations in 0.03 seconds (average 0.005600, setup 0.00)

Warning: PRISM hasn't checked for zero-reward loops.
Your minimum rewards may be too low...

goal = 1, inf = 0, maybe = 198155

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=198156, nc=262080, nnz=524405, k=4] [7.8 MB]
Building sparse matrix (transition rewards)... [n=198156, nc=262080, nnz=99703, k=4] [2.9 MB]
Creating vector for state rewards... [1.5 MB]
Creating vector for inf... [1.5 MB]
Allocating iteration vectors... [2 x 1.5 MB]
TOTAL: [16.7 MB]

Starting iterations...

Iterative method: 95 iterations in 0.59 seconds (average 0.002947, setup 0.31)

Value in the initial state: 98.87725357241708

Time for model checking: 0.652 seconds.

Result: 98.87725357241708 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"success"}max=? [ F "deadlock" ]/Pmin=? [ F "MissionSuccessful" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=4,Objy1=1,Objx2=4,Objy2=2

Prob0E: 4 iterations in 0.02 seconds (average 0.005000, setup 0.00)

Prob1A: 1 iterations in 0.00 seconds (average 0.004000, setup 0.00)

goal = 1, inf = 0, maybe = 198155

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=198156, nc=262080, nnz=524405, k=4] [7.8 MB]
Building sparse matrix (transition rewards)... [n=198156, nc=262080, nnz=1870, k=4] [1.8 MB]
Creating vector for state rewards... [1.5 MB]
Creating vector for inf... [1.5 MB]
Allocating iteration vectors... [2 x 1.5 MB]
TOTAL: [15.6 MB]

Starting iterations...

Iterative method: 91 iterations in 0.53 seconds (average 0.002769, setup 0.28)

Prob0E: 48 iterations in 0.30 seconds (average 0.006333, setup 0.00)

Prob1A: 3 iterations in 0.04 seconds (average 0.012000, setup 0.00)

yes = 949, no = 170915, maybe = 26292

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=198156, nc=45730, nnz=129061, k=2] [1.7 MB]
Creating vector for yes... [1.5 MB]
Allocating iteration vectors... [2 x 1.5 MB]
TOTAL: [6.2 MB]

Starting iterations...

Iterative method: 84 iterations in 0.23 seconds (average 0.001238, setup 0.12)

Value in the initial state: 220.95184627907466

Time for model checking: 1.153 seconds.

Result: 220.95184627907466 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"success"}min=? [ F "deadlock" ]/Pmax=? [ F "MissionSuccessful" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=4,Objy1=1,Objx2=4,Objy2=2

Prob0A: 4 iterations in 0.04 seconds (average 0.009000, setup 0.00)

Prob1E: 5 iterations in 0.04 seconds (average 0.008000, setup 0.00)

Warning: PRISM hasn't checked for zero-reward loops.
Your minimum rewards may be too low...

goal = 1, inf = 0, maybe = 198155

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=198156, nc=262080, nnz=524405, k=4] [7.8 MB]
Building sparse matrix (transition rewards)... [n=198156, nc=262080, nnz=1870, k=4] [1.8 MB]
Creating vector for state rewards... [1.5 MB]
Creating vector for inf... [1.5 MB]
Allocating iteration vectors... [2 x 1.5 MB]
TOTAL: [15.6 MB]

Starting iterations...

Iterative method: 88 iterations in 0.55 seconds (average 0.002909, setup 0.30)

Prob0A: 47 iterations in 0.28 seconds (average 0.005872, setup 0.00)

Prob1E: 4 iterations in 0.03 seconds (average 0.007000, setup 0.00)

yes = 949, no = 166422, maybe = 30785

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=198156, nc=53664, nnz=152000, k=2] [2.0 MB]
Creating vector for yes... [1.5 MB]
Allocating iteration vectors... [2 x 1.5 MB]
TOTAL: [6.5 MB]

Starting iterations...

Iterative method: 90 iterations in 0.25 seconds (average 0.001289, setup 0.13)

Value in the initial state: 79.27570927522382

Time for model checking: 1.207 seconds.

Result: 79.27570927522382 (value in the initial state)

---------------------------------------------------------------------

Note: There were 16 warnings during computation.

