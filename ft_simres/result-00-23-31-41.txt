PRISM
=====

Version: 4.3
Date: Tue Aug 02 12:29:05 BST 2016
Hostname: eglinton
Memory limits: cudd=1g, java(heap)=981.5m
Command line: prism uav_sim_ft_mdp.prism -s uav_mdp.props -const 'Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=3,Objy1=1,Objx2=4,Objy2=1,'

Parsing model file "uav_sim_ft_mdp.prism"...

Parsing properties file "uav_mdp.props"...

18 properties:
(1) Pmax=? [ F "MissionSuccessful" ]
(2) Pmin=? [ F "MissionSuccessful" ]
(3) Pmax=? [ F "MissionTimeUp" ]
(4) Pmin=? [ F "MissionTimeUp" ]
(5) Pmax=? [ F "Crash" ]
(6) Pmin=? [ F "Crash" ]
(7) Pmax=? [ F "AllAreaSearched" ]
(8) Pmin=? [ F "AllAreaSearched" ]
(9) Pmax=? [ F "MissionTimeUp"|"AllAreaSearched"|"Crash" ]
(10) Pmin=? [ F "MissionTimeUp"|"AllAreaSearched"|"Crash" ]
(11) R{"drop"}max=? [ F "deadlock" ]
(12) R{"drop"}min=? [ F "deadlock" ]
(13) R{"charge"}max=? [ F "deadlock" ]
(14) R{"charge"}min=? [ F "deadlock" ]
(15) R{"time"}max=? [ F "deadlock" ]
(16) R{"time"}min=? [ F "deadlock" ]
(17) R{"success"}max=? [ F "deadlock" ]/Pmin=? [ F "MissionSuccessful" ]
(18) R{"success"}min=? [ F "deadlock" ]/Pmax=? [ F "MissionSuccessful" ]

Type:        MDP
Modules:     Obj Move Robot T 
Variables:   NoOfObjs posx posy s1 retx rety b t c 

---------------------------------------------------------------------

Model checking: Pmax=? [ F "MissionSuccessful" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=3,Objy1=1,Objx2=4,Objy2=1

Building model...
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=3,Objy1=1,Objx2=4,Objy2=1

Warning: Guard for command 7 of module "Move" is never satisfied.

Warning: Guard for command 8 of module "Move" is never satisfied.

Warning: Guard for command 9 of module "Move" is never satisfied.

Warning: Guard for command 7 of module "Robot" is never satisfied.

Warning: Update 2 of command 3 of module "Robot" doesn't do anything ("(s1'=13)", line 249, column 44)

Warning: Update 2 of command 27 of module "T" doesn't do anything ("(c'=1)", line 372, column 108)

Warning: Guard for command 28 of module "T" is never satisfied.

Warning: Update 2 of command 9 of module "T" doesn't do anything ("(c'=1)", line 349, column 112)

Warning: Guard for command 10 of module "T" is never satisfied.

Warning: Update 2 of command 51 of module "T" doesn't do anything ("(c'=1)", line 403, column 113)

Warning: Guard for command 52 of module "T" is never satisfied.

Computing reachable states...

Reachability (BFS): 80 iterations in 0.75 seconds (average 0.009400, setup 0.00)

Time for model construction: 1.751 seconds.

Warning: Deadlocks detected and fixed in 1 states

Type:        MDP
States:      190841 (1 initial)
Transitions: 507699
Choices:     252523

Transition matrix: 163722 nodes (76 terminal), 507699 minterms, vars: 34r/34c/18nd

Prob0A: 39 iterations in 0.29 seconds (average 0.007487, setup 0.00)

Prob1E: 4 iterations in 0.05 seconds (average 0.012000, setup 0.00)

yes = 948, no = 160948, maybe = 28945

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=190841, nc=52874, nnz=153714, k=2] [2.0 MB]
Creating vector for yes... [1.5 MB]
Allocating iteration vectors... [2 x 1.5 MB]
TOTAL: [6.4 MB]

Starting iterations...

Iterative method: 83 iterations in 0.29 seconds (average 0.001253, setup 0.19)

Value in the initial state: 0.9831254965509769

Time for model checking: 0.656 seconds.

Result: 0.9831254965509769 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmin=? [ F "MissionSuccessful" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=3,Objy1=1,Objx2=4,Objy2=1

Prob0E: 40 iterations in 0.30 seconds (average 0.007500, setup 0.00)

Prob1A: 3 iterations in 0.04 seconds (average 0.014667, setup 0.00)

yes = 948, no = 165234, maybe = 24659

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=190841, nc=44865, nnz=129997, k=2] [1.7 MB]
Creating vector for yes... [1.5 MB]
Allocating iteration vectors... [2 x 1.5 MB]
TOTAL: [6.1 MB]

Starting iterations...

Iterative method: 78 iterations in 0.28 seconds (average 0.001231, setup 0.18)

Value in the initial state: 0.8401738356068807

Time for model checking: 0.641 seconds.

Result: 0.8401738356068807 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmax=? [ F "MissionTimeUp" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=3,Objy1=1,Objx2=4,Objy2=1

Prob0A: 61 iterations in 0.34 seconds (average 0.005574, setup 0.00)

Prob1E: 4 iterations in 0.04 seconds (average 0.010000, setup 0.00)

yes = 372, no = 123320, maybe = 67149

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=190841, nc=119302, nnz=335888, k=4] [4.1 MB]
Creating vector for yes... [1.5 MB]
Allocating iteration vectors... [2 x 1.5 MB]
TOTAL: [8.5 MB]

Starting iterations...

Iterative method: 87 iterations in 0.37 seconds (average 0.001609, setup 0.23)

Value in the initial state: 3.529594029855967E-4

Time for model checking: 0.762 seconds.

Result: 3.529594029855967E-4 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmin=? [ F "MissionTimeUp" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=3,Objy1=1,Objx2=4,Objy2=1

Prob0E: 69 iterations in 0.52 seconds (average 0.007478, setup 0.00)

Prob1A: 3 iterations in 0.04 seconds (average 0.014667, setup 0.00)

yes = 372, no = 123677, maybe = 66792

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=190841, nc=118664, nnz=334579, k=4] [4.1 MB]
Creating vector for yes... [1.5 MB]
Allocating iteration vectors... [2 x 1.5 MB]
TOTAL: [8.5 MB]

Starting iterations...

Iterative method: 101 iterations in 0.40 seconds (average 0.001663, setup 0.23)

Value in the initial state: 8.908468324585819E-7

Time for model checking: 0.98 seconds.

Result: 8.908468324585819E-7 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmax=? [ F "Crash" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=3,Objy1=1,Objx2=4,Objy2=1

Prob0A: 3 iterations in 0.08 seconds (average 0.025333, setup 0.00)

Prob1E: 158 iterations in 1.99 seconds (average 0.012582, setup 0.00)

yes = 93484, no = 2534, maybe = 94823

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=190841, nc=156505, nnz=411681, k=4] [5.0 MB]
Creating vector for yes... [1.5 MB]
Allocating iteration vectors... [2 x 1.5 MB]
TOTAL: [9.4 MB]

Starting iterations...

Iterative method: 85 iterations in 0.42 seconds (average 0.001788, setup 0.27)

Value in the initial state: 0.029682009236404603

Time for model checking: 2.502 seconds.

Result: 0.029682009236404603 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmin=? [ F "Crash" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=3,Objy1=1,Objx2=4,Objy2=1

Prob0E: 3 iterations in 0.05 seconds (average 0.016000, setup 0.00)

Prob1A: 39 iterations in 0.32 seconds (average 0.008308, setup 0.00)

yes = 93484, no = 2534, maybe = 94823

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=190841, nc=156505, nnz=411681, k=4] [5.0 MB]
Creating vector for yes... [1.5 MB]
Allocating iteration vectors... [2 x 1.5 MB]
TOTAL: [9.4 MB]

Starting iterations...

Iterative method: 77 iterations in 0.34 seconds (average 0.001870, setup 0.19)

Value in the initial state: 0.014839243625183593

Time for model checking: 0.725 seconds.

Result: 0.014839243625183593 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmax=? [ F "AllAreaSearched" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=3,Objy1=1,Objx2=4,Objy2=1

Prob0A: 53 iterations in 0.33 seconds (average 0.006189, setup 0.00)

Prob1E: 4 iterations in 0.04 seconds (average 0.010000, setup 0.00)

yes = 1213, no = 147694, maybe = 41934

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=190841, nc=62302, nnz=160096, k=2] [2.1 MB]
Creating vector for yes... [1.5 MB]
Allocating iteration vectors... [2 x 1.5 MB]
TOTAL: [6.4 MB]

Starting iterations...

Iterative method: 87 iterations in 0.27 seconds (average 0.001379, setup 0.15)

Value in the initial state: 0.13392416649813924

Time for model checking: 0.659 seconds.

Result: 0.13392416649813924 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmin=? [ F "AllAreaSearched" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=3,Objy1=1,Objx2=4,Objy2=1

Prob0E: 62 iterations in 0.32 seconds (average 0.005226, setup 0.00)

Prob1A: 3 iterations in 0.03 seconds (average 0.010667, setup 0.00)

yes = 1213, no = 152134, maybe = 37494

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=190841, nc=54160, nnz=136304, k=2] [1.8 MB]
Creating vector for yes... [1.5 MB]
Allocating iteration vectors... [2 x 1.5 MB]
TOTAL: [6.2 MB]

Starting iterations...

Iterative method: 90 iterations in 0.22 seconds (average 0.001289, setup 0.10)

Value in the initial state: 0.001754554072393775

Time for model checking: 0.598 seconds.

Result: 0.001754554072393775 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmax=? [ F "MissionTimeUp"|"AllAreaSearched"|"Crash" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=3,Objy1=1,Objx2=4,Objy2=1

Prob0A: 3 iterations in 0.09 seconds (average 0.029333, setup 0.00)

Prob1E: 156 iterations in 1.75 seconds (average 0.011231, setup 0.00)

yes = 165233, no = 949, maybe = 24659

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=190841, nc=44865, nnz=129997, k=2] [1.7 MB]
Creating vector for yes... [1.5 MB]
Allocating iteration vectors... [2 x 1.5 MB]
TOTAL: [6.1 MB]

Starting iterations...

Iterative method: 72 iterations in 0.24 seconds (average 0.001222, setup 0.16)

Value in the initial state: 0.15982600378046796

Time for model checking: 2.092 seconds.

Result: 0.15982600378046796 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmin=? [ F "MissionTimeUp"|"AllAreaSearched"|"Crash" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=3,Objy1=1,Objx2=4,Objy2=1

Prob0E: 3 iterations in 0.04 seconds (average 0.012000, setup 0.00)

Prob1A: 39 iterations in 0.25 seconds (average 0.006462, setup 0.00)

yes = 160947, no = 949, maybe = 28945

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=190841, nc=52874, nnz=153714, k=2] [2.0 MB]
Creating vector for yes... [1.5 MB]
Allocating iteration vectors... [2 x 1.5 MB]
TOTAL: [6.4 MB]

Starting iterations...

Iterative method: 81 iterations in 0.23 seconds (average 0.001284, setup 0.13)

Value in the initial state: 0.016873762275439765

Time for model checking: 0.543 seconds.

Result: 0.016873762275439765 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"drop"}max=? [ F "deadlock" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=3,Objy1=1,Objx2=4,Objy2=1

Prob0E: 4 iterations in 0.04 seconds (average 0.009000, setup 0.00)

Prob1A: 1 iterations in 0.01 seconds (average 0.008000, setup 0.00)

goal = 1, inf = 0, maybe = 190840

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=190841, nc=252522, nnz=507698, k=4] [7.5 MB]
Building sparse matrix (transition rewards)... [n=190841, nc=252522, nnz=0, k=4] [1.7 MB]
Creating vector for state rewards... [1.5 MB]
Creating vector for inf... [1.5 MB]
Allocating iteration vectors... [2 x 1.5 MB]
TOTAL: [15.0 MB]

Starting iterations...

Iterative method: 79 iterations in 0.55 seconds (average 0.002684, setup 0.34)

Value in the initial state: 0.182378364886679

Time for model checking: 0.612 seconds.

Result: 0.182378364886679 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"drop"}min=? [ F "deadlock" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=3,Objy1=1,Objx2=4,Objy2=1

Prob0A: 4 iterations in 0.03 seconds (average 0.008000, setup 0.00)

Prob1E: 5 iterations in 0.03 seconds (average 0.006400, setup 0.00)

Warning: PRISM hasn't checked for zero-reward loops.
Your minimum rewards may be too low...

goal = 1, inf = 0, maybe = 190840

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=190841, nc=252522, nnz=507698, k=4] [7.5 MB]
Building sparse matrix (transition rewards)... [n=190841, nc=252522, nnz=0, k=4] [1.7 MB]
Creating vector for state rewards... [1.5 MB]
Creating vector for inf... [1.5 MB]
Allocating iteration vectors... [2 x 1.5 MB]
TOTAL: [15.0 MB]

Starting iterations...

Iterative method: 71 iterations in 0.47 seconds (average 0.002817, setup 0.27)

Value in the initial state: 0.1601827847999911

Time for model checking: 0.545 seconds.

Result: 0.1601827847999911 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"charge"}max=? [ F "deadlock" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=3,Objy1=1,Objx2=4,Objy2=1

Prob0E: 4 iterations in 0.02 seconds (average 0.005000, setup 0.00)

Prob1A: 1 iterations in 0.00 seconds (average 0.004000, setup 0.00)

goal = 1, inf = 0, maybe = 190840

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=190841, nc=252522, nnz=507698, k=4] [7.5 MB]
Building sparse matrix (transition rewards)... [n=190841, nc=252522, nnz=5676, k=4] [1.8 MB]
Creating vector for state rewards... [1.5 MB]
Creating vector for inf... [1.5 MB]
Allocating iteration vectors... [2 x 1.5 MB]
TOTAL: [15.1 MB]

Starting iterations...

Iterative method: 77 iterations in 0.48 seconds (average 0.002753, setup 0.27)

Value in the initial state: 0.27380135904626224

Time for model checking: 0.519 seconds.

Result: 0.27380135904626224 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"charge"}min=? [ F "deadlock" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=3,Objy1=1,Objx2=4,Objy2=1

Prob0A: 4 iterations in 0.02 seconds (average 0.005000, setup 0.00)

Prob1E: 5 iterations in 0.04 seconds (average 0.007200, setup 0.00)

Warning: PRISM hasn't checked for zero-reward loops.
Your minimum rewards may be too low...

goal = 1, inf = 0, maybe = 190840

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=190841, nc=252522, nnz=507698, k=4] [7.5 MB]
Building sparse matrix (transition rewards)... [n=190841, nc=252522, nnz=5676, k=4] [1.8 MB]
Creating vector for state rewards... [1.5 MB]
Creating vector for inf... [1.5 MB]
Allocating iteration vectors... [2 x 1.5 MB]
TOTAL: [15.1 MB]

Starting iterations...

Iterative method: 86 iterations in 0.52 seconds (average 0.002791, setup 0.28)

Value in the initial state: 0.01704325183937368

Time for model checking: 0.573 seconds.

Result: 0.01704325183937368 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"time"}max=? [ F "deadlock" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=3,Objy1=1,Objx2=4,Objy2=1

Prob0E: 4 iterations in 0.02 seconds (average 0.005000, setup 0.00)

Prob1A: 1 iterations in 0.00 seconds (average 0.004000, setup 0.00)

goal = 1, inf = 0, maybe = 190840

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=190841, nc=252522, nnz=507698, k=4] [7.5 MB]
Building sparse matrix (transition rewards)... [n=190841, nc=252522, nnz=96016, k=4] [2.8 MB]
Creating vector for state rewards... [1.5 MB]
Creating vector for inf... [1.5 MB]
Allocating iteration vectors... [2 x 1.5 MB]
TOTAL: [16.1 MB]

Starting iterations...

Iterative method: 88 iterations in 0.56 seconds (average 0.002818, setup 0.31)

Value in the initial state: 163.7765134490672

Time for model checking: 0.586 seconds.

Result: 163.7765134490672 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"time"}min=? [ F "deadlock" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=3,Objy1=1,Objx2=4,Objy2=1

Prob0A: 4 iterations in 0.02 seconds (average 0.005000, setup 0.00)

Prob1E: 5 iterations in 0.03 seconds (average 0.006400, setup 0.00)

Warning: PRISM hasn't checked for zero-reward loops.
Your minimum rewards may be too low...

goal = 1, inf = 0, maybe = 190840

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=190841, nc=252522, nnz=507698, k=4] [7.5 MB]
Building sparse matrix (transition rewards)... [n=190841, nc=252522, nnz=96016, k=4] [2.8 MB]
Creating vector for state rewards... [1.5 MB]
Creating vector for inf... [1.5 MB]
Allocating iteration vectors... [2 x 1.5 MB]
TOTAL: [16.1 MB]

Starting iterations...

Iterative method: 89 iterations in 0.56 seconds (average 0.002876, setup 0.30)

Value in the initial state: 82.33474621136375

Time for model checking: 0.618 seconds.

Result: 82.33474621136375 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"success"}max=? [ F "deadlock" ]/Pmin=? [ F "MissionSuccessful" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=3,Objy1=1,Objx2=4,Objy2=1

Prob0E: 4 iterations in 0.02 seconds (average 0.005000, setup 0.00)

Prob1A: 1 iterations in 0.00 seconds (average 0.000000, setup 0.00)

goal = 1, inf = 0, maybe = 190840

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=190841, nc=252522, nnz=507698, k=4] [7.5 MB]
Building sparse matrix (transition rewards)... [n=190841, nc=252522, nnz=1871, k=4] [1.7 MB]
Creating vector for state rewards... [1.5 MB]
Creating vector for inf... [1.5 MB]
Allocating iteration vectors... [2 x 1.5 MB]
TOTAL: [15.0 MB]

Starting iterations...

Iterative method: 84 iterations in 0.50 seconds (average 0.002810, setup 0.27)

Prob0E: 40 iterations in 0.21 seconds (average 0.005200, setup 0.00)

Prob1A: 3 iterations in 0.04 seconds (average 0.012000, setup 0.00)

yes = 948, no = 165234, maybe = 24659

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=190841, nc=44865, nnz=129997, k=2] [1.7 MB]
Creating vector for yes... [1.5 MB]
Allocating iteration vectors... [2 x 1.5 MB]
TOTAL: [6.1 MB]

Starting iterations...

Iterative method: 78 iterations in 0.20 seconds (average 0.001231, setup 0.10)

Value in the initial state: 191.09170328958035

Time for model checking: 0.999 seconds.

Result: 191.09170328958035 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"success"}min=? [ F "deadlock" ]/Pmax=? [ F "MissionSuccessful" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=3,Objy1=1,Objx2=4,Objy2=1

Prob0A: 4 iterations in 0.05 seconds (average 0.013000, setup 0.00)

Prob1E: 5 iterations in 0.03 seconds (average 0.006400, setup 0.00)

Warning: PRISM hasn't checked for zero-reward loops.
Your minimum rewards may be too low...

goal = 1, inf = 0, maybe = 190840

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=190841, nc=252522, nnz=507698, k=4] [7.5 MB]
Building sparse matrix (transition rewards)... [n=190841, nc=252522, nnz=1871, k=4] [1.7 MB]
Creating vector for state rewards... [1.5 MB]
Creating vector for inf... [1.5 MB]
Allocating iteration vectors... [2 x 1.5 MB]
TOTAL: [15.0 MB]

Starting iterations...

Iterative method: 83 iterations in 0.51 seconds (average 0.002843, setup 0.28)

Prob0A: 39 iterations in 0.18 seconds (average 0.004615, setup 0.00)

Prob1E: 4 iterations in 0.02 seconds (average 0.006000, setup 0.00)

yes = 948, no = 160948, maybe = 28945

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=190841, nc=52874, nnz=153714, k=2] [2.0 MB]
Creating vector for yes... [1.5 MB]
Allocating iteration vectors... [2 x 1.5 MB]
TOTAL: [6.4 MB]

Starting iterations...

Iterative method: 83 iterations in 0.22 seconds (average 0.001253, setup 0.11)

Value in the initial state: 75.8060223980109

Time for model checking: 1.026 seconds.

Result: 75.8060223980109 (value in the initial state)

---------------------------------------------------------------------

Note: There were 16 warnings during computation.

