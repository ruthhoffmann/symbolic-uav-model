PRISM
=====

Version: 4.3
Date: Tue Aug 02 12:35:17 BST 2016
Hostname: eglinton
Memory limits: cudd=1g, java(heap)=981.5m
Command line: prism uav_sim_ft_mdp.prism -s uav_mdp.props -const 'Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=0,Objy1=1,Objx2=6,Objy2=1,'

Parsing model file "uav_sim_ft_mdp.prism"...

Parsing properties file "uav_mdp.props"...

18 properties:
(1) Pmax=? [ F "MissionSuccessful" ]
(2) Pmin=? [ F "MissionSuccessful" ]
(3) Pmax=? [ F "MissionTimeUp" ]
(4) Pmin=? [ F "MissionTimeUp" ]
(5) Pmax=? [ F "Crash" ]
(6) Pmin=? [ F "Crash" ]
(7) Pmax=? [ F "AllAreaSearched" ]
(8) Pmin=? [ F "AllAreaSearched" ]
(9) Pmax=? [ F "MissionTimeUp"|"AllAreaSearched"|"Crash" ]
(10) Pmin=? [ F "MissionTimeUp"|"AllAreaSearched"|"Crash" ]
(11) R{"drop"}max=? [ F "deadlock" ]
(12) R{"drop"}min=? [ F "deadlock" ]
(13) R{"charge"}max=? [ F "deadlock" ]
(14) R{"charge"}min=? [ F "deadlock" ]
(15) R{"time"}max=? [ F "deadlock" ]
(16) R{"time"}min=? [ F "deadlock" ]
(17) R{"success"}max=? [ F "deadlock" ]/Pmin=? [ F "MissionSuccessful" ]
(18) R{"success"}min=? [ F "deadlock" ]/Pmax=? [ F "MissionSuccessful" ]

Type:        MDP
Modules:     Obj Move Robot T 
Variables:   NoOfObjs posx posy s1 retx rety b t c 

---------------------------------------------------------------------

Model checking: Pmax=? [ F "MissionSuccessful" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=0,Objy1=1,Objx2=6,Objy2=1

Building model...
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=0,Objy1=1,Objx2=6,Objy2=1

Warning: Guard for command 4 of module "Move" is never satisfied.

Warning: Guard for command 7 of module "Move" is never satisfied.

Warning: Guard for command 8 of module "Move" is never satisfied.

Warning: Guard for command 9 of module "Move" is never satisfied.

Warning: Guard for command 7 of module "Robot" is never satisfied.

Warning: Update 2 of command 3 of module "Robot" doesn't do anything ("(s1'=13)", line 249, column 44)

Warning: Update 2 of command 27 of module "T" doesn't do anything ("(c'=1)", line 372, column 108)

Warning: Guard for command 28 of module "T" is never satisfied.

Warning: Update 2 of command 9 of module "T" doesn't do anything ("(c'=1)", line 349, column 112)

Warning: Guard for command 10 of module "T" is never satisfied.

Warning: Update 2 of command 51 of module "T" doesn't do anything ("(c'=1)", line 403, column 113)

Warning: Guard for command 52 of module "T" is never satisfied.

Computing reachable states...

Reachability (BFS): 82 iterations in 0.97 seconds (average 0.011854, setup 0.00)

Time for model construction: 2.72 seconds.

Warning: Deadlocks detected and fixed in 1 states

Type:        MDP
States:      229851 (1 initial)
Transitions: 619951
Choices:     305474

Transition matrix: 175936 nodes (76 terminal), 619951 minterms, vars: 34r/34c/18nd

Prob0A: 46 iterations in 0.39 seconds (average 0.008435, setup 0.00)

Prob1E: 4 iterations in 0.06 seconds (average 0.015000, setup 0.00)

yes = 946, no = 193993, maybe = 34912

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=229851, nc=62002, nnz=180042, k=2] [2.3 MB]
Creating vector for yes... [1.8 MB]
Allocating iteration vectors... [2 x 1.8 MB]
TOTAL: [7.6 MB]

Starting iterations...

Iterative method: 92 iterations in 0.40 seconds (average 0.001652, setup 0.25)

Value in the initial state: 0.9800512697116531

Time for model checking: 1.179 seconds.

Result: 0.9800512697116531 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmin=? [ F "MissionSuccessful" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=0,Objy1=1,Objx2=6,Objy2=1

Prob0E: 48 iterations in 0.46 seconds (average 0.009583, setup 0.00)

Prob1A: 3 iterations in 0.06 seconds (average 0.018667, setup 0.00)

yes = 946, no = 199918, maybe = 28987

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=229851, nc=51222, nnz=148132, k=2] [2.0 MB]
Creating vector for yes... [1.8 MB]
Allocating iteration vectors... [2 x 1.8 MB]
TOTAL: [7.2 MB]

Starting iterations...

Iterative method: 84 iterations in 0.34 seconds (average 0.001571, setup 0.21)

Value in the initial state: 0.7287397075895189

Time for model checking: 1.219 seconds.

Result: 0.7287397075895189 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmax=? [ F "MissionTimeUp" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=0,Objy1=1,Objx2=6,Objy2=1

Prob0A: 60 iterations in 0.58 seconds (average 0.009667, setup 0.00)

Prob1E: 4 iterations in 0.04 seconds (average 0.011000, setup 0.00)

yes = 468, no = 140723, maybe = 88660

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=229851, nc=154363, nnz=431126, k=4] [5.3 MB]
Creating vector for yes... [1.8 MB]
Allocating iteration vectors... [2 x 1.8 MB]
TOTAL: [10.6 MB]

Starting iterations...

Iterative method: 90 iterations in 0.48 seconds (average 0.002089, setup 0.29)

Value in the initial state: 0.001677889857944649

Time for model checking: 1.179 seconds.

Result: 0.001677889857944649 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmin=? [ F "MissionTimeUp" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=0,Objy1=1,Objx2=6,Objy2=1

Prob0E: 67 iterations in 0.71 seconds (average 0.010567, setup 0.00)

Prob1A: 3 iterations in 0.07 seconds (average 0.024000, setup 0.00)

yes = 468, no = 141118, maybe = 88265

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=229851, nc=153649, nnz=429739, k=4] [5.3 MB]
Creating vector for yes... [1.8 MB]
Allocating iteration vectors... [2 x 1.8 MB]
TOTAL: [10.5 MB]

Starting iterations...

Iterative method: 107 iterations in 0.53 seconds (average 0.002056, setup 0.31)

Value in the initial state: 4.686859472416261E-6

Time for model checking: 1.63 seconds.

Result: 4.686859472416261E-6 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmax=? [ F "Crash" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=0,Objy1=1,Objx2=6,Objy2=1

Prob0A: 3 iterations in 0.11 seconds (average 0.037333, setup 0.00)

Prob1E: 179 iterations in 2.78 seconds (average 0.015553, setup 0.00)

yes = 112511, no = 2655, maybe = 114685

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=229851, nc=190308, nnz=504785, k=4] [6.2 MB]
Creating vector for yes... [1.8 MB]
Allocating iteration vectors... [2 x 1.8 MB]
TOTAL: [11.4 MB]

Starting iterations...

Iterative method: 88 iterations in 0.55 seconds (average 0.002227, setup 0.36)

Value in the initial state: 0.03220457374925743

Time for model checking: 4.884 seconds.

Result: 0.03220457374925743 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmin=? [ F "Crash" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=0,Objy1=1,Objx2=6,Objy2=1

Prob0E: 3 iterations in 0.07 seconds (average 0.024000, setup 0.00)

Prob1A: 44 iterations in 0.58 seconds (average 0.013182, setup 0.00)

yes = 112511, no = 2655, maybe = 114685

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=229851, nc=190308, nnz=504785, k=4] [6.2 MB]
Creating vector for yes... [1.8 MB]
Allocating iteration vectors... [2 x 1.8 MB]
TOTAL: [11.4 MB]

Starting iterations...

Iterative method: 91 iterations in 0.48 seconds (average 0.002286, setup 0.27)

Value in the initial state: 0.01784207842245122

Time for model checking: 1.569 seconds.

Result: 0.01784207842245122 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmax=? [ F "AllAreaSearched" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=0,Objy1=1,Objx2=6,Objy2=1

Prob0A: 57 iterations in 0.48 seconds (average 0.008421, setup 0.00)

Prob1E: 4 iterations in 0.05 seconds (average 0.013000, setup 0.00)

yes = 1240, no = 183470, maybe = 45141

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=229851, nc=68330, nnz=177621, k=2] [2.3 MB]
Creating vector for yes... [1.8 MB]
Allocating iteration vectors... [2 x 1.8 MB]
TOTAL: [7.6 MB]

Starting iterations...

Iterative method: 89 iterations in 0.33 seconds (average 0.001663, setup 0.18)

Value in the initial state: 0.24150669266017152

Time for model checking: 1.023 seconds.

Result: 0.24150669266017152 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmin=? [ F "AllAreaSearched" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=0,Objy1=1,Objx2=6,Objy2=1

Prob0E: 64 iterations in 0.41 seconds (average 0.006437, setup 0.00)

Prob1A: 3 iterations in 0.04 seconds (average 0.012000, setup 0.00)

yes = 1240, no = 189277, maybe = 39334

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=229851, nc=58457, nnz=149770, k=2] [2.0 MB]
Creating vector for yes... [1.8 MB]
Allocating iteration vectors... [2 x 1.8 MB]
TOTAL: [7.2 MB]

Starting iterations...

Iterative method: 95 iterations in 0.28 seconds (average 0.001600, setup 0.12)

Value in the initial state: 0.0017345167581919283

Time for model checking: 0.95 seconds.

Result: 0.0017345167581919283 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmax=? [ F "MissionTimeUp"|"AllAreaSearched"|"Crash" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=0,Objy1=1,Objx2=6,Objy2=1

Prob0A: 3 iterations in 0.09 seconds (average 0.029333, setup 0.00)

Prob1E: 188 iterations in 3.00 seconds (average 0.015936, setup 0.00)

yes = 199917, no = 947, maybe = 28987

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=229851, nc=51222, nnz=148132, k=2] [2.0 MB]
Creating vector for yes... [1.8 MB]
Allocating iteration vectors... [2 x 1.8 MB]
TOTAL: [7.2 MB]

Starting iterations...

Iterative method: 78 iterations in 0.34 seconds (average 0.001487, setup 0.22)

Value in the initial state: 0.27126010724746247

Time for model checking: 4.874 seconds.

Result: 0.27126010724746247 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmin=? [ F "MissionTimeUp"|"AllAreaSearched"|"Crash" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=0,Objy1=1,Objx2=6,Objy2=1

Prob0E: 3 iterations in 0.07 seconds (average 0.022667, setup 0.00)

Prob1A: 46 iterations in 0.53 seconds (average 0.011478, setup 0.00)

yes = 193992, no = 947, maybe = 34912

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=229851, nc=62002, nnz=180042, k=2] [2.3 MB]
Creating vector for yes... [1.8 MB]
Allocating iteration vectors... [2 x 1.8 MB]
TOTAL: [7.6 MB]

Starting iterations...

Iterative method: 91 iterations in 0.35 seconds (average 0.001670, setup 0.20)

Value in the initial state: 0.019948258108791677

Time for model checking: 1.087 seconds.

Result: 0.019948258108791677 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"drop"}max=? [ F "deadlock" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=0,Objy1=1,Objx2=6,Objy2=1

Prob0E: 4 iterations in 0.10 seconds (average 0.025000, setup 0.00)

Prob1A: 1 iterations in 0.01 seconds (average 0.008000, setup 0.00)

goal = 1, inf = 0, maybe = 229850

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=229851, nc=305473, nnz=619950, k=4] [9.1 MB]
Building sparse matrix (transition rewards)... [n=229851, nc=305473, nnz=0, k=4] [2.0 MB]
Creating vector for state rewards... [1.8 MB]
Creating vector for inf... [1.8 MB]
Allocating iteration vectors... [2 x 1.8 MB]
TOTAL: [18.2 MB]

Starting iterations...

Iterative method: 89 iterations in 0.72 seconds (average 0.003371, setup 0.42)

Value in the initial state: 0.24036213186309718

Time for model checking: 0.848 seconds.

Result: 0.24036213186309718 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"drop"}min=? [ F "deadlock" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=0,Objy1=1,Objx2=6,Objy2=1

Prob0A: 4 iterations in 0.03 seconds (average 0.008000, setup 0.00)

Prob1E: 5 iterations in 0.05 seconds (average 0.010400, setup 0.00)

Warning: PRISM hasn't checked for zero-reward loops.
Your minimum rewards may be too low...

goal = 1, inf = 0, maybe = 229850

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=229851, nc=305473, nnz=619950, k=4] [9.1 MB]
Building sparse matrix (transition rewards)... [n=229851, nc=305473, nnz=0, k=4] [2.0 MB]
Creating vector for state rewards... [1.8 MB]
Creating vector for inf... [1.8 MB]
Allocating iteration vectors... [2 x 1.8 MB]
TOTAL: [18.2 MB]

Starting iterations...

Iterative method: 78 iterations in 0.59 seconds (average 0.003333, setup 0.33)

Value in the initial state: 0.21170947697281625

Time for model checking: 0.691 seconds.

Result: 0.21170947697281625 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"charge"}max=? [ F "deadlock" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=0,Objy1=1,Objx2=6,Objy2=1

Prob0E: 4 iterations in 0.03 seconds (average 0.007000, setup 0.00)

Prob1A: 1 iterations in 0.00 seconds (average 0.004000, setup 0.00)

goal = 1, inf = 0, maybe = 229850

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=229851, nc=305473, nnz=619950, k=4] [9.1 MB]
Building sparse matrix (transition rewards)... [n=229851, nc=305473, nnz=7130, k=4] [2.1 MB]
Creating vector for state rewards... [1.8 MB]
Creating vector for inf... [1.8 MB]
Allocating iteration vectors... [2 x 1.8 MB]
TOTAL: [18.3 MB]

Starting iterations...

Iterative method: 85 iterations in 0.66 seconds (average 0.003765, setup 0.34)

Value in the initial state: 0.3124811210279567

Time for model checking: 0.715 seconds.

Result: 0.3124811210279567 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"charge"}min=? [ F "deadlock" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=0,Objy1=1,Objx2=6,Objy2=1

Prob0A: 4 iterations in 0.03 seconds (average 0.008000, setup 0.00)

Prob1E: 5 iterations in 0.06 seconds (average 0.012800, setup 0.00)

Warning: PRISM hasn't checked for zero-reward loops.
Your minimum rewards may be too low...

goal = 1, inf = 0, maybe = 229850

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=229851, nc=305473, nnz=619950, k=4] [9.1 MB]
Building sparse matrix (transition rewards)... [n=229851, nc=305473, nnz=7130, k=4] [2.1 MB]
Creating vector for state rewards... [1.8 MB]
Creating vector for inf... [1.8 MB]
Allocating iteration vectors... [2 x 1.8 MB]
TOTAL: [18.3 MB]

Starting iterations...

Iterative method: 92 iterations in 0.73 seconds (average 0.004130, setup 0.35)

Value in the initial state: 0.05899602902795169

Time for model checking: 0.846 seconds.

Result: 0.05899602902795169 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"time"}max=? [ F "deadlock" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=0,Objy1=1,Objx2=6,Objy2=1

Prob0E: 4 iterations in 0.03 seconds (average 0.007000, setup 0.00)

Prob1A: 1 iterations in 0.01 seconds (average 0.008000, setup 0.00)

goal = 1, inf = 0, maybe = 229850

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=229851, nc=305473, nnz=619950, k=4] [9.1 MB]
Building sparse matrix (transition rewards)... [n=229851, nc=305473, nnz=115164, k=4] [3.4 MB]
Creating vector for state rewards... [1.8 MB]
Creating vector for inf... [1.8 MB]
Allocating iteration vectors... [2 x 1.8 MB]
TOTAL: [19.5 MB]

Starting iterations...

Iterative method: 90 iterations in 0.71 seconds (average 0.003511, setup 0.39)

Value in the initial state: 177.47251584873325

Time for model checking: 0.767 seconds.

Result: 177.47251584873325 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"time"}min=? [ F "deadlock" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=0,Objy1=1,Objx2=6,Objy2=1

Prob0A: 4 iterations in 0.03 seconds (average 0.007000, setup 0.00)

Prob1E: 5 iterations in 0.05 seconds (average 0.009600, setup 0.00)

Warning: PRISM hasn't checked for zero-reward loops.
Your minimum rewards may be too low...

goal = 1, inf = 0, maybe = 229850

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=229851, nc=305473, nnz=619950, k=4] [9.1 MB]
Building sparse matrix (transition rewards)... [n=229851, nc=305473, nnz=115164, k=4] [3.4 MB]
Creating vector for state rewards... [1.8 MB]
Creating vector for inf... [1.8 MB]
Allocating iteration vectors... [2 x 1.8 MB]
TOTAL: [19.5 MB]

Starting iterations...

Iterative method: 98 iterations in 0.70 seconds (average 0.003429, setup 0.36)

Value in the initial state: 98.83401985767868

Time for model checking: 0.799 seconds.

Result: 98.83401985767868 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"success"}max=? [ F "deadlock" ]/Pmin=? [ F "MissionSuccessful" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=0,Objy1=1,Objx2=6,Objy2=1

Prob0E: 4 iterations in 0.02 seconds (average 0.006000, setup 0.00)

Prob1A: 1 iterations in 0.01 seconds (average 0.008000, setup 0.00)

goal = 1, inf = 0, maybe = 229850

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=229851, nc=305473, nnz=619950, k=4] [9.1 MB]
Building sparse matrix (transition rewards)... [n=229851, nc=305473, nnz=1864, k=4] [2.1 MB]
Creating vector for state rewards... [1.8 MB]
Creating vector for inf... [1.8 MB]
Allocating iteration vectors... [2 x 1.8 MB]
TOTAL: [18.2 MB]

Starting iterations...

Iterative method: 94 iterations in 0.63 seconds (average 0.003319, setup 0.32)

Prob0E: 48 iterations in 0.38 seconds (average 0.007833, setup 0.00)

Prob1A: 3 iterations in 0.06 seconds (average 0.018667, setup 0.00)

yes = 946, no = 199918, maybe = 28987

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=229851, nc=51222, nnz=148132, k=2] [2.0 MB]
Creating vector for yes... [1.8 MB]
Allocating iteration vectors... [2 x 1.8 MB]
TOTAL: [7.2 MB]

Starting iterations...

Iterative method: 84 iterations in 0.32 seconds (average 0.001476, setup 0.19)

Value in the initial state: 238.36397218761383

Time for model checking: 1.455 seconds.

Result: 238.36397218761383 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"success"}min=? [ F "deadlock" ]/Pmax=? [ F "MissionSuccessful" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=0,Objy1=1,Objx2=6,Objy2=1

Prob0A: 4 iterations in 0.09 seconds (average 0.023000, setup 0.00)

Prob1E: 5 iterations in 0.05 seconds (average 0.009600, setup 0.00)

Warning: PRISM hasn't checked for zero-reward loops.
Your minimum rewards may be too low...

goal = 1, inf = 0, maybe = 229850

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=229851, nc=305473, nnz=619950, k=4] [9.1 MB]
Building sparse matrix (transition rewards)... [n=229851, nc=305473, nnz=1864, k=4] [2.1 MB]
Creating vector for state rewards... [1.8 MB]
Creating vector for inf... [1.8 MB]
Allocating iteration vectors... [2 x 1.8 MB]
TOTAL: [18.2 MB]

Starting iterations...

Iterative method: 88 iterations in 0.67 seconds (average 0.003455, setup 0.37)

Prob0A: 46 iterations in 0.28 seconds (average 0.006000, setup 0.00)

Prob1E: 4 iterations in 0.03 seconds (average 0.008000, setup 0.00)

yes = 946, no = 193993, maybe = 34912

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=229851, nc=62002, nnz=180042, k=2] [2.3 MB]
Creating vector for yes... [1.8 MB]
Allocating iteration vectors... [2 x 1.8 MB]
TOTAL: [7.6 MB]

Starting iterations...

Iterative method: 92 iterations in 0.30 seconds (average 0.001565, setup 0.15)

Value in the initial state: 79.75102090801218

Time for model checking: 1.463 seconds.

Result: 79.75102090801218 (value in the initial state)

---------------------------------------------------------------------

Note: There were 17 warnings during computation.

