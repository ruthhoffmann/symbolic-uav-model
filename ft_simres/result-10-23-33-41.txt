PRISM
=====

Version: 4.3
Date: Tue Aug 02 12:41:04 BST 2016
Hostname: eglinton
Memory limits: cudd=1g, java(heap)=981.5m
Command line: prism uav_sim_ft_mdp.prism -s uav_mdp.props -const 'Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=3,Objy1=3,Objx2=4,Objy2=1,'

Parsing model file "uav_sim_ft_mdp.prism"...

Parsing properties file "uav_mdp.props"...

18 properties:
(1) Pmax=? [ F "MissionSuccessful" ]
(2) Pmin=? [ F "MissionSuccessful" ]
(3) Pmax=? [ F "MissionTimeUp" ]
(4) Pmin=? [ F "MissionTimeUp" ]
(5) Pmax=? [ F "Crash" ]
(6) Pmin=? [ F "Crash" ]
(7) Pmax=? [ F "AllAreaSearched" ]
(8) Pmin=? [ F "AllAreaSearched" ]
(9) Pmax=? [ F "MissionTimeUp"|"AllAreaSearched"|"Crash" ]
(10) Pmin=? [ F "MissionTimeUp"|"AllAreaSearched"|"Crash" ]
(11) R{"drop"}max=? [ F "deadlock" ]
(12) R{"drop"}min=? [ F "deadlock" ]
(13) R{"charge"}max=? [ F "deadlock" ]
(14) R{"charge"}min=? [ F "deadlock" ]
(15) R{"time"}max=? [ F "deadlock" ]
(16) R{"time"}min=? [ F "deadlock" ]
(17) R{"success"}max=? [ F "deadlock" ]/Pmin=? [ F "MissionSuccessful" ]
(18) R{"success"}min=? [ F "deadlock" ]/Pmax=? [ F "MissionSuccessful" ]

Type:        MDP
Modules:     Obj Move Robot T 
Variables:   NoOfObjs posx posy s1 retx rety b t c 

---------------------------------------------------------------------

Model checking: Pmax=? [ F "MissionSuccessful" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=3,Objy1=3,Objx2=4,Objy2=1

Building model...
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=3,Objy1=3,Objx2=4,Objy2=1

Warning: Guard for command 7 of module "Move" is never satisfied.

Warning: Guard for command 8 of module "Move" is never satisfied.

Warning: Guard for command 9 of module "Move" is never satisfied.

Warning: Guard for command 7 of module "Robot" is never satisfied.

Warning: Update 2 of command 3 of module "Robot" doesn't do anything ("(s1'=13)", line 249, column 44)

Warning: Update 2 of command 27 of module "T" doesn't do anything ("(c'=1)", line 372, column 108)

Warning: Guard for command 28 of module "T" is never satisfied.

Warning: Update 2 of command 9 of module "T" doesn't do anything ("(c'=1)", line 349, column 112)

Warning: Guard for command 10 of module "T" is never satisfied.

Warning: Update 2 of command 51 of module "T" doesn't do anything ("(c'=1)", line 403, column 113)

Warning: Guard for command 52 of module "T" is never satisfied.

Computing reachable states...

Reachability (BFS): 80 iterations in 0.77 seconds (average 0.009600, setup 0.00)

Time for model construction: 1.849 seconds.

Warning: Deadlocks detected and fixed in 1 states

Type:        MDP
States:      176155 (1 initial)
Transitions: 452910
Choices:     230726

Transition matrix: 167622 nodes (76 terminal), 452910 minterms, vars: 34r/34c/18nd

Prob0A: 52 iterations in 0.36 seconds (average 0.007000, setup 0.00)

Prob1E: 4 iterations in 0.04 seconds (average 0.011000, setup 0.00)

yes = 954, no = 147920, maybe = 27281

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=176155, nc=45940, nnz=125723, k=2] [1.7 MB]
Creating vector for yes... [1.3 MB]
Allocating iteration vectors... [2 x 1.3 MB]
TOTAL: [5.7 MB]

Starting iterations...

Iterative method: 92 iterations in 0.29 seconds (average 0.001174, setup 0.18)

Value in the initial state: 0.9788595855682716

Time for model checking: 0.723 seconds.

Result: 0.9788595855682716 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmin=? [ F "MissionSuccessful" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=3,Objy1=3,Objx2=4,Objy2=1

Prob0E: 65 iterations in 0.46 seconds (average 0.007138, setup 0.00)

Prob1A: 3 iterations in 0.03 seconds (average 0.010667, setup 0.00)

yes = 954, no = 152118, maybe = 23083

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=176155, nc=38856, nnz=105460, k=2] [1.4 MB]
Creating vector for yes... [1.3 MB]
Allocating iteration vectors... [2 x 1.3 MB]
TOTAL: [5.4 MB]

Starting iterations...

Iterative method: 88 iterations in 0.23 seconds (average 0.001045, setup 0.14)

Value in the initial state: 0.776856549290818

Time for model checking: 0.743 seconds.

Result: 0.776856549290818 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmax=? [ F "MissionTimeUp" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=3,Objy1=3,Objx2=4,Objy2=1

Prob0A: 61 iterations in 0.41 seconds (average 0.006754, setup 0.00)

Prob1E: 4 iterations in 0.04 seconds (average 0.009000, setup 0.00)

yes = 344, no = 103664, maybe = 72147

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=176155, nc=116649, nnz=311082, k=4] [3.8 MB]
Creating vector for yes... [1.3 MB]
Allocating iteration vectors... [2 x 1.3 MB]
TOTAL: [7.9 MB]

Starting iterations...

Iterative method: 90 iterations in 0.36 seconds (average 0.001511, setup 0.22)

Value in the initial state: 7.785317174417914E-4

Time for model checking: 0.816 seconds.

Result: 7.785317174417914E-4 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmin=? [ F "MissionTimeUp" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=3,Objy1=3,Objx2=4,Objy2=1

Prob0E: 69 iterations in 0.64 seconds (average 0.009333, setup 0.00)

Prob1A: 3 iterations in 0.04 seconds (average 0.012000, setup 0.00)

yes = 344, no = 103964, maybe = 71847

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=176155, nc=116125, nnz=309890, k=4] [3.8 MB]
Creating vector for yes... [1.3 MB]
Allocating iteration vectors... [2 x 1.3 MB]
TOTAL: [7.9 MB]

Starting iterations...

Iterative method: 105 iterations in 0.37 seconds (average 0.001524, setup 0.21)

Value in the initial state: 3.1551155002299656E-6

Time for model checking: 1.067 seconds.

Result: 3.1551155002299656E-6 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmax=? [ F "Crash" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=3,Objy1=3,Objx2=4,Objy2=1

Prob0A: 3 iterations in 0.06 seconds (average 0.021333, setup 0.00)

Prob1E: 248 iterations in 2.24 seconds (average 0.009016, setup 0.00)

yes = 86499, no = 2599, maybe = 87057

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=176155, nc=141628, nnz=363812, k=4] [4.5 MB]
Creating vector for yes... [1.3 MB]
Allocating iteration vectors... [2 x 1.3 MB]
TOTAL: [8.5 MB]

Starting iterations...

Iterative method: 88 iterations in 0.39 seconds (average 0.001636, setup 0.25)

Value in the initial state: 0.062048705183092134

Time for model checking: 2.717 seconds.

Result: 0.062048705183092134 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmin=? [ F "Crash" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=3,Objy1=3,Objx2=4,Objy2=1

Prob0E: 3 iterations in 0.04 seconds (average 0.014667, setup 0.00)

Prob1A: 45 iterations in 0.36 seconds (average 0.008000, setup 0.00)

yes = 86499, no = 2599, maybe = 87057

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=176155, nc=141628, nnz=363812, k=4] [4.5 MB]
Creating vector for yes... [1.3 MB]
Allocating iteration vectors... [2 x 1.3 MB]
TOTAL: [8.5 MB]

Starting iterations...

Iterative method: 90 iterations in 0.33 seconds (average 0.001644, setup 0.18)

Value in the initial state: 0.01916048664954718

Time for model checking: 0.756 seconds.

Result: 0.01916048664954718 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmax=? [ F "AllAreaSearched" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=3,Objy1=3,Objx2=4,Objy2=1

Prob0A: 52 iterations in 0.34 seconds (average 0.006462, setup 0.00)

Prob1E: 4 iterations in 0.04 seconds (average 0.010000, setup 0.00)

yes = 1300, no = 137431, maybe = 37424

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=176155, nc=59833, nnz=156610, k=2] [2.0 MB]
Creating vector for yes... [1.3 MB]
Allocating iteration vectors... [2 x 1.3 MB]
TOTAL: [6.0 MB]

Starting iterations...

Iterative method: 82 iterations in 0.26 seconds (average 0.001220, setup 0.16)

Value in the initial state: 0.19623219848046325

Time for model checking: 0.651 seconds.

Result: 0.19623219848046325 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmin=? [ F "AllAreaSearched" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=3,Objy1=3,Objx2=4,Objy2=1

Prob0E: 67 iterations in 0.45 seconds (average 0.006746, setup 0.00)

Prob1A: 3 iterations in 0.03 seconds (average 0.010667, setup 0.00)

yes = 1300, no = 143157, maybe = 31698

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=176155, nc=50879, nnz=132530, k=2] [1.7 MB]
Creating vector for yes... [1.3 MB]
Allocating iteration vectors... [2 x 1.3 MB]
TOTAL: [5.8 MB]

Starting iterations...

Iterative method: 86 iterations in 0.22 seconds (average 0.001209, setup 0.11)

Value in the initial state: 0.0012317198384986768

Time for model checking: 0.71 seconds.

Result: 0.0012317198384986768 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmax=? [ F "MissionTimeUp"|"AllAreaSearched"|"Crash" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=3,Objy1=3,Objx2=4,Objy2=1

Prob0A: 3 iterations in 0.09 seconds (average 0.029333, setup 0.00)

Prob1E: 252 iterations in 2.33 seconds (average 0.009238, setup 0.00)

yes = 152117, no = 955, maybe = 23083

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=176155, nc=38856, nnz=105460, k=2] [1.4 MB]
Creating vector for yes... [1.3 MB]
Allocating iteration vectors... [2 x 1.3 MB]
TOTAL: [5.4 MB]

Starting iterations...

Iterative method: 82 iterations in 0.24 seconds (average 0.001122, setup 0.14)

Value in the initial state: 0.22314317918713072

Time for model checking: 2.649 seconds.

Result: 0.22314317918713072 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmin=? [ F "MissionTimeUp"|"AllAreaSearched"|"Crash" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=3,Objy1=3,Objx2=4,Objy2=1

Prob0E: 3 iterations in 0.03 seconds (average 0.010667, setup 0.00)

Prob1A: 52 iterations in 0.36 seconds (average 0.006923, setup 0.00)

yes = 147919, no = 955, maybe = 27281

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=176155, nc=45940, nnz=125723, k=2] [1.7 MB]
Creating vector for yes... [1.3 MB]
Allocating iteration vectors... [2 x 1.3 MB]
TOTAL: [5.7 MB]

Starting iterations...

Iterative method: 91 iterations in 0.22 seconds (average 0.001099, setup 0.12)

Value in the initial state: 0.02113961321949243

Time for model checking: 0.646 seconds.

Result: 0.02113961321949243 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"drop"}max=? [ F "deadlock" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=3,Objy1=3,Objx2=4,Objy2=1

Prob0E: 4 iterations in 0.05 seconds (average 0.013000, setup 0.00)

Prob1A: 1 iterations in 0.00 seconds (average 0.004000, setup 0.00)

goal = 1, inf = 0, maybe = 176154

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=176155, nc=230725, nnz=452909, k=4] [6.7 MB]
Building sparse matrix (transition rewards)... [n=176155, nc=230725, nnz=0, k=4] [1.6 MB]
Creating vector for state rewards... [1.3 MB]
Creating vector for inf... [1.3 MB]
Allocating iteration vectors... [2 x 1.3 MB]
TOTAL: [13.7 MB]

Starting iterations...

Iterative method: 88 iterations in 0.54 seconds (average 0.002500, setup 0.32)

Value in the initial state: 0.1801629817895781

Time for model checking: 0.613 seconds.

Result: 0.1801629817895781 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"drop"}min=? [ F "deadlock" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=3,Objy1=3,Objx2=4,Objy2=1

Prob0A: 4 iterations in 0.04 seconds (average 0.010000, setup 0.00)

Prob1E: 5 iterations in 0.04 seconds (average 0.007200, setup 0.00)

Warning: PRISM hasn't checked for zero-reward loops.
Your minimum rewards may be too low...

goal = 1, inf = 0, maybe = 176154

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=176155, nc=230725, nnz=452909, k=4] [6.7 MB]
Building sparse matrix (transition rewards)... [n=176155, nc=230725, nnz=0, k=4] [1.6 MB]
Creating vector for state rewards... [1.3 MB]
Creating vector for inf... [1.3 MB]
Allocating iteration vectors... [2 x 1.3 MB]
TOTAL: [13.7 MB]

Starting iterations...

Iterative method: 80 iterations in 0.45 seconds (average 0.002500, setup 0.25)

Value in the initial state: 0.13502618991762247

Time for model checking: 0.53 seconds.

Result: 0.13502618991762247 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"charge"}max=? [ F "deadlock" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=3,Objy1=3,Objx2=4,Objy2=1

Prob0E: 4 iterations in 0.02 seconds (average 0.006000, setup 0.00)

Prob1A: 1 iterations in 0.00 seconds (average 0.004000, setup 0.00)

goal = 1, inf = 0, maybe = 176154

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=176155, nc=230725, nnz=452909, k=4] [6.7 MB]
Building sparse matrix (transition rewards)... [n=176155, nc=230725, nnz=4730, k=4] [1.6 MB]
Creating vector for state rewards... [1.3 MB]
Creating vector for inf... [1.3 MB]
Allocating iteration vectors... [2 x 1.3 MB]
TOTAL: [13.7 MB]

Starting iterations...

Iterative method: 85 iterations in 0.46 seconds (average 0.002447, setup 0.25)

Value in the initial state: 0.9802345795252382

Time for model checking: 0.495 seconds.

Result: 0.9802345795252382 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"charge"}min=? [ F "deadlock" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=3,Objy1=3,Objx2=4,Objy2=1

Prob0A: 4 iterations in 0.02 seconds (average 0.005000, setup 0.00)

Prob1E: 5 iterations in 0.03 seconds (average 0.006400, setup 0.00)

Warning: PRISM hasn't checked for zero-reward loops.
Your minimum rewards may be too low...

goal = 1, inf = 0, maybe = 176154

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=176155, nc=230725, nnz=452909, k=4] [6.7 MB]
Building sparse matrix (transition rewards)... [n=176155, nc=230725, nnz=4730, k=4] [1.6 MB]
Creating vector for state rewards... [1.3 MB]
Creating vector for inf... [1.3 MB]
Allocating iteration vectors... [2 x 1.3 MB]
TOTAL: [13.7 MB]

Starting iterations...

Iterative method: 93 iterations in 0.48 seconds (average 0.002495, setup 0.25)

Value in the initial state: 0.052914238447896216

Time for model checking: 0.545 seconds.

Result: 0.052914238447896216 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"time"}max=? [ F "deadlock" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=3,Objy1=3,Objx2=4,Objy2=1

Prob0E: 4 iterations in 0.02 seconds (average 0.005000, setup 0.00)

Prob1A: 1 iterations in 0.00 seconds (average 0.004000, setup 0.00)

goal = 1, inf = 0, maybe = 176154

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=176155, nc=230725, nnz=452909, k=4] [6.7 MB]
Building sparse matrix (transition rewards)... [n=176155, nc=230725, nnz=89096, k=4] [2.6 MB]
Creating vector for state rewards... [1.3 MB]
Creating vector for inf... [1.3 MB]
Allocating iteration vectors... [2 x 1.3 MB]
TOTAL: [14.7 MB]

Starting iterations...

Iterative method: 90 iterations in 0.50 seconds (average 0.002533, setup 0.27)

Value in the initial state: 340.9705481641951

Time for model checking: 0.54 seconds.

Result: 340.9705481641951 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"time"}min=? [ F "deadlock" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=3,Objy1=3,Objx2=4,Objy2=1

Prob0A: 4 iterations in 0.02 seconds (average 0.005000, setup 0.00)

Prob1E: 5 iterations in 0.04 seconds (average 0.007200, setup 0.00)

Warning: PRISM hasn't checked for zero-reward loops.
Your minimum rewards may be too low...

goal = 1, inf = 0, maybe = 176154

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=176155, nc=230725, nnz=452909, k=4] [6.7 MB]
Building sparse matrix (transition rewards)... [n=176155, nc=230725, nnz=89096, k=4] [2.6 MB]
Creating vector for state rewards... [1.3 MB]
Creating vector for inf... [1.3 MB]
Allocating iteration vectors... [2 x 1.3 MB]
TOTAL: [14.7 MB]

Starting iterations...

Iterative method: 99 iterations in 0.54 seconds (average 0.002586, setup 0.28)

Value in the initial state: 106.14552115589417

Time for model checking: 0.595 seconds.

Result: 106.14552115589417 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"success"}max=? [ F "deadlock" ]/Pmin=? [ F "MissionSuccessful" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=3,Objy1=3,Objx2=4,Objy2=1

Prob0E: 4 iterations in 0.02 seconds (average 0.005000, setup 0.00)

Prob1A: 1 iterations in 0.00 seconds (average 0.004000, setup 0.00)

goal = 1, inf = 0, maybe = 176154

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=176155, nc=230725, nnz=452909, k=4] [6.7 MB]
Building sparse matrix (transition rewards)... [n=176155, nc=230725, nnz=1880, k=4] [1.6 MB]
Creating vector for state rewards... [1.3 MB]
Creating vector for inf... [1.3 MB]
Allocating iteration vectors... [2 x 1.3 MB]
TOTAL: [13.7 MB]

Starting iterations...

Iterative method: 93 iterations in 0.47 seconds (average 0.002495, setup 0.24)

Prob0E: 65 iterations in 0.36 seconds (average 0.005600, setup 0.00)

Prob1A: 3 iterations in 0.04 seconds (average 0.012000, setup 0.00)

yes = 954, no = 152118, maybe = 23083

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=176155, nc=38856, nnz=105460, k=2] [1.4 MB]
Creating vector for yes... [1.3 MB]
Allocating iteration vectors... [2 x 1.3 MB]
TOTAL: [5.4 MB]

Starting iterations...

Iterative method: 88 iterations in 0.21 seconds (average 0.001091, setup 0.12)

Value in the initial state: 429.04383174278706

Time for model checking: 1.143 seconds.

Result: 429.04383174278706 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"success"}min=? [ F "deadlock" ]/Pmax=? [ F "MissionSuccessful" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=3,Objy1=3,Objx2=4,Objy2=1

Prob0A: 4 iterations in 0.05 seconds (average 0.013000, setup 0.00)

Prob1E: 5 iterations in 0.04 seconds (average 0.007200, setup 0.00)

Warning: PRISM hasn't checked for zero-reward loops.
Your minimum rewards may be too low...

goal = 1, inf = 0, maybe = 176154

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=176155, nc=230725, nnz=452909, k=4] [6.7 MB]
Building sparse matrix (transition rewards)... [n=176155, nc=230725, nnz=1880, k=4] [1.6 MB]
Creating vector for state rewards... [1.3 MB]
Creating vector for inf... [1.3 MB]
Allocating iteration vectors... [2 x 1.3 MB]
TOTAL: [13.7 MB]

Starting iterations...

Iterative method: 91 iterations in 0.49 seconds (average 0.002505, setup 0.26)

Prob0A: 52 iterations in 0.26 seconds (average 0.005077, setup 0.00)

Prob1E: 4 iterations in 0.02 seconds (average 0.006000, setup 0.00)

yes = 954, no = 147920, maybe = 27281

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=176155, nc=45940, nnz=125723, k=2] [1.7 MB]
Creating vector for yes... [1.3 MB]
Allocating iteration vectors... [2 x 1.3 MB]
TOTAL: [5.7 MB]

Starting iterations...

Iterative method: 92 iterations in 0.21 seconds (average 0.001130, setup 0.10)

Value in the initial state: 82.41477855005125

Time for model checking: 1.08 seconds.

Result: 82.41477855005125 (value in the initial state)

---------------------------------------------------------------------

Note: There were 16 warnings during computation.

