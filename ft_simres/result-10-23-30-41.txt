PRISM
=====

Version: 4.3
Date: Tue Aug 02 12:39:46 BST 2016
Hostname: eglinton
Memory limits: cudd=1g, java(heap)=981.5m
Command line: prism uav_sim_ft_mdp.prism -s uav_mdp.props -const 'Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=3,Objy1=0,Objx2=4,Objy2=1,'

Parsing model file "uav_sim_ft_mdp.prism"...

Parsing properties file "uav_mdp.props"...

18 properties:
(1) Pmax=? [ F "MissionSuccessful" ]
(2) Pmin=? [ F "MissionSuccessful" ]
(3) Pmax=? [ F "MissionTimeUp" ]
(4) Pmin=? [ F "MissionTimeUp" ]
(5) Pmax=? [ F "Crash" ]
(6) Pmin=? [ F "Crash" ]
(7) Pmax=? [ F "AllAreaSearched" ]
(8) Pmin=? [ F "AllAreaSearched" ]
(9) Pmax=? [ F "MissionTimeUp"|"AllAreaSearched"|"Crash" ]
(10) Pmin=? [ F "MissionTimeUp"|"AllAreaSearched"|"Crash" ]
(11) R{"drop"}max=? [ F "deadlock" ]
(12) R{"drop"}min=? [ F "deadlock" ]
(13) R{"charge"}max=? [ F "deadlock" ]
(14) R{"charge"}min=? [ F "deadlock" ]
(15) R{"time"}max=? [ F "deadlock" ]
(16) R{"time"}min=? [ F "deadlock" ]
(17) R{"success"}max=? [ F "deadlock" ]/Pmin=? [ F "MissionSuccessful" ]
(18) R{"success"}min=? [ F "deadlock" ]/Pmax=? [ F "MissionSuccessful" ]

Type:        MDP
Modules:     Obj Move Robot T 
Variables:   NoOfObjs posx posy s1 retx rety b t c 

---------------------------------------------------------------------

Model checking: Pmax=? [ F "MissionSuccessful" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=3,Objy1=0,Objx2=4,Objy2=1

Building model...
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=3,Objy1=0,Objx2=4,Objy2=1

Warning: Guard for command 7 of module "Move" is never satisfied.

Warning: Guard for command 8 of module "Move" is never satisfied.

Warning: Guard for command 9 of module "Move" is never satisfied.

Warning: Guard for command 7 of module "Robot" is never satisfied.

Warning: Update 2 of command 3 of module "Robot" doesn't do anything ("(s1'=13)", line 249, column 44)

Warning: Update 2 of command 27 of module "T" doesn't do anything ("(c'=1)", line 372, column 108)

Warning: Guard for command 28 of module "T" is never satisfied.

Warning: Update 2 of command 9 of module "T" doesn't do anything ("(c'=1)", line 349, column 112)

Warning: Guard for command 10 of module "T" is never satisfied.

Warning: Update 2 of command 51 of module "T" doesn't do anything ("(c'=1)", line 403, column 113)

Warning: Guard for command 52 of module "T" is never satisfied.

Computing reachable states...

Reachability (BFS): 81 iterations in 0.91 seconds (average 0.011259, setup 0.00)

Time for model construction: 1.997 seconds.

Warning: Deadlocks detected and fixed in 1 states

Type:        MDP
States:      227819 (1 initial)
Transitions: 595611
Choices:     298576

Transition matrix: 174947 nodes (76 terminal), 595611 minterms, vars: 34r/34c/18nd

Prob0A: 45 iterations in 0.29 seconds (average 0.006489, setup 0.00)

Prob1E: 4 iterations in 0.05 seconds (average 0.012000, setup 0.00)

yes = 954, no = 193615, maybe = 33250

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=227819, nc=58985, nnz=169979, k=2] [2.2 MB]
Creating vector for yes... [1.7 MB]
Allocating iteration vectors... [2 x 1.7 MB]
TOTAL: [7.4 MB]

Starting iterations...

Iterative method: 85 iterations in 0.34 seconds (average 0.001506, setup 0.21)

Value in the initial state: 0.9828577966938342

Time for model checking: 0.701 seconds.

Result: 0.9828577966938342 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmin=? [ F "MissionSuccessful" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=3,Objy1=0,Objx2=4,Objy2=1

Prob0E: 47 iterations in 0.36 seconds (average 0.007745, setup 0.00)

Prob1A: 3 iterations in 0.06 seconds (average 0.018667, setup 0.00)

yes = 954, no = 198903, maybe = 27962

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=227819, nc=49420, nnz=141962, k=2] [1.9 MB]
Creating vector for yes... [1.7 MB]
Allocating iteration vectors... [2 x 1.7 MB]
TOTAL: [7.1 MB]

Starting iterations...

Iterative method: 79 iterations in 0.37 seconds (average 0.001570, setup 0.24)

Value in the initial state: 0.8249740036995958

Time for model checking: 0.815 seconds.

Result: 0.8249740036995958 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmax=? [ F "MissionTimeUp" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=3,Objy1=0,Objx2=4,Objy2=1

Prob0A: 56 iterations in 0.58 seconds (average 0.010429, setup 0.00)

Prob1E: 4 iterations in 0.04 seconds (average 0.011000, setup 0.00)

yes = 488, no = 142766, maybe = 84565

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=227819, nc=145583, nnz=402466, k=4] [5.0 MB]
Creating vector for yes... [1.7 MB]
Allocating iteration vectors... [2 x 1.7 MB]
TOTAL: [10.2 MB]

Starting iterations...

Iterative method: 90 iterations in 0.43 seconds (average 0.001956, setup 0.26)

Value in the initial state: 5.896228008396207E-4

Time for model checking: 1.07 seconds.

Result: 5.896228008396207E-4 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmin=? [ F "MissionTimeUp" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=3,Objy1=0,Objx2=4,Objy2=1

Prob0E: 64 iterations in 0.57 seconds (average 0.008937, setup 0.00)

Prob1A: 3 iterations in 0.05 seconds (average 0.016000, setup 0.00)

yes = 488, no = 143142, maybe = 84189

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=227819, nc=144907, nnz=401118, k=4] [4.9 MB]
Creating vector for yes... [1.7 MB]
Allocating iteration vectors... [2 x 1.7 MB]
TOTAL: [10.2 MB]

Starting iterations...

Iterative method: 104 iterations in 0.47 seconds (average 0.002038, setup 0.26)

Value in the initial state: 1.423903481807592E-6

Time for model checking: 1.117 seconds.

Result: 1.423903481807592E-6 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmax=? [ F "Crash" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=3,Objy1=0,Objx2=4,Objy2=1

Prob0A: 3 iterations in 0.10 seconds (average 0.034667, setup 0.00)

Prob1E: 178 iterations in 2.56 seconds (average 0.014382, setup 0.00)

yes = 111652, no = 2663, maybe = 113504

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=227819, nc=184261, nnz=481296, k=4] [5.9 MB]
Creating vector for yes... [1.7 MB]
Allocating iteration vectors... [2 x 1.7 MB]
TOTAL: [11.1 MB]

Starting iterations...

Iterative method: 85 iterations in 0.50 seconds (average 0.002118, setup 0.32)

Value in the initial state: 0.030590381980003065

Time for model checking: 3.191 seconds.

Result: 0.030590381980003065 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmin=? [ F "Crash" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=3,Objy1=0,Objx2=4,Objy2=1

Prob0E: 3 iterations in 0.06 seconds (average 0.020000, setup 0.00)

Prob1A: 42 iterations in 0.44 seconds (average 0.010571, setup 0.00)

yes = 111652, no = 2663, maybe = 113504

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=227819, nc=184261, nnz=481296, k=4] [5.9 MB]
Creating vector for yes... [1.7 MB]
Allocating iteration vectors... [2 x 1.7 MB]
TOTAL: [11.1 MB]

Starting iterations...

Iterative method: 81 iterations in 0.42 seconds (average 0.002272, setup 0.24)

Value in the initial state: 0.015104539442758969

Time for model checking: 0.956 seconds.

Result: 0.015104539442758969 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmax=? [ F "AllAreaSearched" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=3,Objy1=0,Objx2=4,Objy2=1

Prob0A: 60 iterations in 0.48 seconds (average 0.007933, setup 0.00)

Prob1E: 4 iterations in 0.04 seconds (average 0.010000, setup 0.00)

yes = 1220, no = 180882, maybe = 45717

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=227819, nc=67067, nnz=170966, k=2] [2.2 MB]
Creating vector for yes... [1.7 MB]
Allocating iteration vectors... [2 x 1.7 MB]
TOTAL: [7.5 MB]

Starting iterations...

Iterative method: 88 iterations in 0.30 seconds (average 0.001591, setup 0.16)

Value in the initial state: 0.14825431000911857

Time for model checking: 0.833 seconds.

Result: 0.14825431000911857 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmin=? [ F "AllAreaSearched" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=3,Objy1=0,Objx2=4,Objy2=1

Prob0E: 63 iterations in 0.41 seconds (average 0.006476, setup 0.00)

Prob1A: 3 iterations in 0.04 seconds (average 0.012000, setup 0.00)

yes = 1220, no = 186268, maybe = 40331

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=227819, nc=57858, nnz=144999, k=2] [1.9 MB]
Creating vector for yes... [1.7 MB]
Allocating iteration vectors... [2 x 1.7 MB]
TOTAL: [7.1 MB]

Starting iterations...

Iterative method: 92 iterations in 0.26 seconds (average 0.001522, setup 0.12)

Value in the initial state: 0.0017428358767250841

Time for model checking: 0.717 seconds.

Result: 0.0017428358767250841 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmax=? [ F "MissionTimeUp"|"AllAreaSearched"|"Crash" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=3,Objy1=0,Objx2=4,Objy2=1

Prob0A: 3 iterations in 0.10 seconds (average 0.032000, setup 0.00)

Prob1E: 184 iterations in 2.38 seconds (average 0.012913, setup 0.00)

yes = 198902, no = 955, maybe = 27962

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=227819, nc=49420, nnz=141962, k=2] [1.9 MB]
Creating vector for yes... [1.7 MB]
Allocating iteration vectors... [2 x 1.7 MB]
TOTAL: [7.1 MB]

Starting iterations...

Iterative method: 73 iterations in 0.31 seconds (average 0.001425, setup 0.20)

Value in the initial state: 0.17502587788055068

Time for model checking: 2.794 seconds.

Result: 0.17502587788055068 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmin=? [ F "MissionTimeUp"|"AllAreaSearched"|"Crash" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=3,Objy1=0,Objx2=4,Objy2=1

Prob0E: 3 iterations in 0.06 seconds (average 0.020000, setup 0.00)

Prob1A: 45 iterations in 0.42 seconds (average 0.009422, setup 0.00)

yes = 193614, no = 955, maybe = 33250

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=227819, nc=58985, nnz=169979, k=2] [2.2 MB]
Creating vector for yes... [1.7 MB]
Allocating iteration vectors... [2 x 1.7 MB]
TOTAL: [7.4 MB]

Starting iterations...

Iterative method: 84 iterations in 0.29 seconds (average 0.001476, setup 0.16)

Value in the initial state: 0.01714143629685192

Time for model checking: 0.798 seconds.

Result: 0.01714143629685192 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"drop"}max=? [ F "deadlock" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=3,Objy1=0,Objx2=4,Objy2=1

Prob0E: 4 iterations in 0.06 seconds (average 0.016000, setup 0.00)

Prob1A: 1 iterations in 0.00 seconds (average 0.004000, setup 0.00)

goal = 1, inf = 0, maybe = 227818

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=227819, nc=298575, nnz=595610, k=4] [8.8 MB]
Building sparse matrix (transition rewards)... [n=227819, nc=298575, nnz=0, k=4] [2.0 MB]
Creating vector for state rewards... [1.7 MB]
Creating vector for inf... [1.7 MB]
Allocating iteration vectors... [2 x 1.7 MB]
TOTAL: [17.8 MB]

Starting iterations...

Iterative method: 81 iterations in 0.76 seconds (average 0.003309, setup 0.49)

Value in the initial state: 0.21132688657883633

Time for model checking: 0.837 seconds.

Result: 0.21132688657883633 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"drop"}min=? [ F "deadlock" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=3,Objy1=0,Objx2=4,Objy2=1

Prob0A: 4 iterations in 0.08 seconds (average 0.020000, setup 0.00)

Prob1E: 5 iterations in 0.05 seconds (average 0.009600, setup 0.00)

Warning: PRISM hasn't checked for zero-reward loops.
Your minimum rewards may be too low...

goal = 1, inf = 0, maybe = 227818

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=227819, nc=298575, nnz=595610, k=4] [8.8 MB]
Building sparse matrix (transition rewards)... [n=227819, nc=298575, nnz=0, k=4] [2.0 MB]
Creating vector for state rewards... [1.7 MB]
Creating vector for inf... [1.7 MB]
Allocating iteration vectors... [2 x 1.7 MB]
TOTAL: [17.8 MB]

Starting iterations...

Iterative method: 72 iterations in 0.56 seconds (average 0.003278, setup 0.32)

Value in the initial state: 0.18753323442824768

Time for model checking: 0.69 seconds.

Result: 0.18753323442824768 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"charge"}max=? [ F "deadlock" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=3,Objy1=0,Objx2=4,Objy2=1

Prob0E: 4 iterations in 0.03 seconds (average 0.007000, setup 0.00)

Prob1A: 1 iterations in 0.00 seconds (average 0.004000, setup 0.00)

goal = 1, inf = 0, maybe = 227818

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=227819, nc=298575, nnz=595610, k=4] [8.8 MB]
Building sparse matrix (transition rewards)... [n=227819, nc=298575, nnz=6458, k=4] [2.1 MB]
Creating vector for state rewards... [1.7 MB]
Creating vector for inf... [1.7 MB]
Allocating iteration vectors... [2 x 1.7 MB]
TOTAL: [17.9 MB]

Starting iterations...

Iterative method: 78 iterations in 0.57 seconds (average 0.003179, setup 0.32)

Value in the initial state: 0.2931201880146013

Time for model checking: 0.619 seconds.

Result: 0.2931201880146013 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"charge"}min=? [ F "deadlock" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=3,Objy1=0,Objx2=4,Objy2=1

Prob0A: 4 iterations in 0.02 seconds (average 0.005000, setup 0.00)

Prob1E: 5 iterations in 0.04 seconds (average 0.008000, setup 0.00)

Warning: PRISM hasn't checked for zero-reward loops.
Your minimum rewards may be too low...

goal = 1, inf = 0, maybe = 227818

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=227819, nc=298575, nnz=595610, k=4] [8.8 MB]
Building sparse matrix (transition rewards)... [n=227819, nc=298575, nnz=6458, k=4] [2.1 MB]
Creating vector for state rewards... [1.7 MB]
Creating vector for inf... [1.7 MB]
Allocating iteration vectors... [2 x 1.7 MB]
TOTAL: [17.9 MB]

Starting iterations...

Iterative method: 88 iterations in 0.62 seconds (average 0.003318, setup 0.32)

Value in the initial state: 0.02021340561486364

Time for model checking: 0.682 seconds.

Result: 0.02021340561486364 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"time"}max=? [ F "deadlock" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=3,Objy1=0,Objx2=4,Objy2=1

Prob0E: 4 iterations in 0.02 seconds (average 0.006000, setup 0.00)

Prob1A: 1 iterations in 0.00 seconds (average 0.004000, setup 0.00)

goal = 1, inf = 0, maybe = 227818

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=227819, nc=298575, nnz=595610, k=4] [8.8 MB]
Building sparse matrix (transition rewards)... [n=227819, nc=298575, nnz=114313, k=4] [3.3 MB]
Creating vector for state rewards... [1.7 MB]
Creating vector for inf... [1.7 MB]
Allocating iteration vectors... [2 x 1.7 MB]
TOTAL: [19.1 MB]

Starting iterations...

Iterative method: 88 iterations in 0.66 seconds (average 0.003409, setup 0.36)

Value in the initial state: 168.7218140586188

Time for model checking: 0.7 seconds.

Result: 168.7218140586188 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"time"}min=? [ F "deadlock" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=3,Objy1=0,Objx2=4,Objy2=1

Prob0A: 4 iterations in 0.02 seconds (average 0.006000, setup 0.00)

Prob1E: 5 iterations in 0.04 seconds (average 0.008000, setup 0.00)

Warning: PRISM hasn't checked for zero-reward loops.
Your minimum rewards may be too low...

goal = 1, inf = 0, maybe = 227818

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=227819, nc=298575, nnz=595610, k=4] [8.8 MB]
Building sparse matrix (transition rewards)... [n=227819, nc=298575, nnz=114313, k=4] [3.3 MB]
Creating vector for state rewards... [1.7 MB]
Creating vector for inf... [1.7 MB]
Allocating iteration vectors... [2 x 1.7 MB]
TOTAL: [19.1 MB]

Starting iterations...

Iterative method: 92 iterations in 0.68 seconds (average 0.003435, setup 0.36)

Value in the initial state: 83.79113789088274

Time for model checking: 0.744 seconds.

Result: 83.79113789088274 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"success"}max=? [ F "deadlock" ]/Pmin=? [ F "MissionSuccessful" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=3,Objy1=0,Objx2=4,Objy2=1

Prob0E: 4 iterations in 0.02 seconds (average 0.006000, setup 0.00)

Prob1A: 1 iterations in 0.00 seconds (average 0.004000, setup 0.00)

goal = 1, inf = 0, maybe = 227818

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=227819, nc=298575, nnz=595610, k=4] [8.8 MB]
Building sparse matrix (transition rewards)... [n=227819, nc=298575, nnz=1880, k=4] [2.0 MB]
Creating vector for state rewards... [1.7 MB]
Creating vector for inf... [1.7 MB]
Allocating iteration vectors... [2 x 1.7 MB]
TOTAL: [17.8 MB]

Starting iterations...

Iterative method: 87 iterations in 0.59 seconds (average 0.003218, setup 0.31)

Prob0E: 47 iterations in 0.28 seconds (average 0.005957, setup 0.00)

Prob1A: 3 iterations in 0.03 seconds (average 0.009333, setup 0.00)

yes = 954, no = 198903, maybe = 27962

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=227819, nc=49420, nnz=141962, k=2] [1.9 MB]
Creating vector for yes... [1.7 MB]
Allocating iteration vectors... [2 x 1.7 MB]
TOTAL: [7.1 MB]

Starting iterations...

Iterative method: 79 iterations in 0.26 seconds (average 0.001367, setup 0.15)

Value in the initial state: 200.4110884354368

Time for model checking: 1.233 seconds.

Result: 200.4110884354368 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"success"}min=? [ F "deadlock" ]/Pmax=? [ F "MissionSuccessful" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=3,Objy1=0,Objx2=4,Objy2=1

Prob0A: 4 iterations in 0.06 seconds (average 0.015000, setup 0.00)

Prob1E: 5 iterations in 0.04 seconds (average 0.008800, setup 0.00)

Warning: PRISM hasn't checked for zero-reward loops.
Your minimum rewards may be too low...

goal = 1, inf = 0, maybe = 227818

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=227819, nc=298575, nnz=595610, k=4] [8.8 MB]
Building sparse matrix (transition rewards)... [n=227819, nc=298575, nnz=1880, k=4] [2.0 MB]
Creating vector for state rewards... [1.7 MB]
Creating vector for inf... [1.7 MB]
Allocating iteration vectors... [2 x 1.7 MB]
TOTAL: [17.8 MB]

Starting iterations...

Iterative method: 84 iterations in 0.63 seconds (average 0.003286, setup 0.36)

Prob0A: 45 iterations in 0.29 seconds (average 0.006400, setup 0.00)

Prob1E: 4 iterations in 0.04 seconds (average 0.010000, setup 0.00)

yes = 954, no = 193615, maybe = 33250

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=227819, nc=58985, nnz=169979, k=2] [2.2 MB]
Creating vector for yes... [1.7 MB]
Allocating iteration vectors... [2 x 1.7 MB]
TOTAL: [7.4 MB]

Starting iterations...

Iterative method: 85 iterations in 0.30 seconds (average 0.001506, setup 0.18)

Value in the initial state: 76.14360803766633

Time for model checking: 1.384 seconds.

Result: 76.14360803766633 (value in the initial state)

---------------------------------------------------------------------

Note: There were 16 warnings during computation.

