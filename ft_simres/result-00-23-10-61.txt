PRISM
=====

Version: 4.3
Date: Tue Aug 02 12:25:00 BST 2016
Hostname: eglinton
Memory limits: cudd=1g, java(heap)=981.5m
Command line: prism uav_sim_ft_mdp.prism -s uav_mdp.props -const 'Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=1,Objy1=0,Objx2=6,Objy2=1,'

Parsing model file "uav_sim_ft_mdp.prism"...

Parsing properties file "uav_mdp.props"...

18 properties:
(1) Pmax=? [ F "MissionSuccessful" ]
(2) Pmin=? [ F "MissionSuccessful" ]
(3) Pmax=? [ F "MissionTimeUp" ]
(4) Pmin=? [ F "MissionTimeUp" ]
(5) Pmax=? [ F "Crash" ]
(6) Pmin=? [ F "Crash" ]
(7) Pmax=? [ F "AllAreaSearched" ]
(8) Pmin=? [ F "AllAreaSearched" ]
(9) Pmax=? [ F "MissionTimeUp"|"AllAreaSearched"|"Crash" ]
(10) Pmin=? [ F "MissionTimeUp"|"AllAreaSearched"|"Crash" ]
(11) R{"drop"}max=? [ F "deadlock" ]
(12) R{"drop"}min=? [ F "deadlock" ]
(13) R{"charge"}max=? [ F "deadlock" ]
(14) R{"charge"}min=? [ F "deadlock" ]
(15) R{"time"}max=? [ F "deadlock" ]
(16) R{"time"}min=? [ F "deadlock" ]
(17) R{"success"}max=? [ F "deadlock" ]/Pmin=? [ F "MissionSuccessful" ]
(18) R{"success"}min=? [ F "deadlock" ]/Pmax=? [ F "MissionSuccessful" ]

Type:        MDP
Modules:     Obj Move Robot T 
Variables:   NoOfObjs posx posy s1 retx rety b t c 

---------------------------------------------------------------------

Model checking: Pmax=? [ F "MissionSuccessful" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=1,Objy1=0,Objx2=6,Objy2=1

Building model...
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=1,Objy1=0,Objx2=6,Objy2=1

Warning: Guard for command 7 of module "Move" is never satisfied.

Warning: Guard for command 8 of module "Move" is never satisfied.

Warning: Guard for command 9 of module "Move" is never satisfied.

Warning: Guard for command 7 of module "Robot" is never satisfied.

Warning: Update 2 of command 3 of module "Robot" doesn't do anything ("(s1'=13)", line 249, column 44)

Warning: Update 2 of command 27 of module "T" doesn't do anything ("(c'=1)", line 372, column 108)

Warning: Guard for command 28 of module "T" is never satisfied.

Warning: Update 2 of command 9 of module "T" doesn't do anything ("(c'=1)", line 349, column 112)

Warning: Guard for command 10 of module "T" is never satisfied.

Warning: Update 2 of command 51 of module "T" doesn't do anything ("(c'=1)", line 403, column 113)

Warning: Guard for command 52 of module "T" is never satisfied.

Computing reachable states...

Reachability (BFS): 83 iterations in 1.33 seconds (average 0.016000, setup 0.00)

Time for model construction: 3.099 seconds.

Warning: Deadlocks detected and fixed in 1 states

Type:        MDP
States:      272649 (1 initial)
Transitions: 736627
Choices:     361224

Transition matrix: 179711 nodes (76 terminal), 736627 minterms, vars: 34r/34c/18nd

Prob0A: 48 iterations in 0.47 seconds (average 0.009833, setup 0.00)

Prob1E: 4 iterations in 0.06 seconds (average 0.016000, setup 0.00)

yes = 934, no = 232047, maybe = 39668

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=272649, nc=70725, nnz=207868, k=2] [2.7 MB]
Creating vector for yes... [2.1 MB]
Allocating iteration vectors... [2 x 2.1 MB]
TOTAL: [8.9 MB]

Starting iterations...

Iterative method: 89 iterations in 0.44 seconds (average 0.001843, setup 0.28)

Value in the initial state: 0.9822803836624804

Time for model checking: 1.23 seconds.

Result: 0.9822803836624804 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmin=? [ F "MissionSuccessful" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=1,Objy1=0,Objx2=6,Objy2=1

Prob0E: 49 iterations in 0.50 seconds (average 0.010204, setup 0.00)

Prob1A: 3 iterations in 0.06 seconds (average 0.020000, setup 0.00)

yes = 934, no = 237976, maybe = 33739

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=272649, nc=59929, nnz=176099, k=2] [2.3 MB]
Creating vector for yes... [2.1 MB]
Allocating iteration vectors... [2 x 2.1 MB]
TOTAL: [8.6 MB]

Starting iterations...

Iterative method: 81 iterations in 0.37 seconds (average 0.001778, setup 0.23)

Value in the initial state: 0.7848316617771646

Time for model checking: 0.991 seconds.

Result: 0.7848316617771646 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmax=? [ F "MissionTimeUp" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=1,Objy1=0,Objx2=6,Objy2=1

Prob0A: 55 iterations in 0.55 seconds (average 0.010036, setup 0.00)

Prob1E: 4 iterations in 0.05 seconds (average 0.013000, setup 0.00)

yes = 568, no = 165100, maybe = 106981

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=272649, nc=185927, nnz=521002, k=4] [6.4 MB]
Creating vector for yes... [2.1 MB]
Allocating iteration vectors... [2 x 2.1 MB]
TOTAL: [12.6 MB]

Starting iterations...

Iterative method: 94 iterations in 0.57 seconds (average 0.002468, setup 0.34)

Value in the initial state: 0.001215847420548496

Time for model checking: 1.86 seconds.

Result: 0.001215847420548496 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmin=? [ F "MissionTimeUp" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=1,Objy1=0,Objx2=6,Objy2=1

Prob0E: 62 iterations in 0.76 seconds (average 0.012323, setup 0.00)

Prob1A: 3 iterations in 0.06 seconds (average 0.020000, setup 0.00)

yes = 568, no = 165552, maybe = 106529

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=272649, nc=185099, nnz=519498, k=4] [6.4 MB]
Creating vector for yes... [2.1 MB]
Allocating iteration vectors... [2 x 2.1 MB]
TOTAL: [12.6 MB]

Starting iterations...

Iterative method: 107 iterations in 0.60 seconds (average 0.002542, setup 0.33)

Value in the initial state: 4.240998906941511E-6

Time for model checking: 1.648 seconds.

Result: 4.240998906941511E-6 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmax=? [ F "Crash" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=1,Objy1=0,Objx2=6,Objy2=1

Prob0A: 3 iterations in 0.10 seconds (average 0.032000, setup 0.00)

Prob1E: 189 iterations in 2.98 seconds (average 0.015788, setup 0.00)

yes = 133426, no = 2673, maybe = 136550

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=272649, nc=225125, nnz=600528, k=4] [7.3 MB]
Creating vector for yes... [2.1 MB]
Allocating iteration vectors... [2 x 2.1 MB]
TOTAL: [13.6 MB]

Starting iterations...

Iterative method: 91 iterations in 0.66 seconds (average 0.002725, setup 0.41)

Value in the initial state: 0.03248396098263285

Time for model checking: 3.944 seconds.

Result: 0.03248396098263285 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmin=? [ F "Crash" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=1,Objy1=0,Objx2=6,Objy2=1

Prob0E: 3 iterations in 0.08 seconds (average 0.025333, setup 0.00)

Prob1A: 42 iterations in 0.65 seconds (average 0.015429, setup 0.00)

yes = 133426, no = 2673, maybe = 136550

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=272649, nc=225125, nnz=600528, k=4] [7.3 MB]
Creating vector for yes... [2.1 MB]
Allocating iteration vectors... [2 x 2.1 MB]
TOTAL: [13.6 MB]

Starting iterations...

Iterative method: 87 iterations in 0.60 seconds (average 0.002759, setup 0.36)

Value in the initial state: 0.015642788486485575

Time for model checking: 1.387 seconds.

Result: 0.015642788486485575 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmax=? [ F "AllAreaSearched" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=1,Objy1=0,Objx2=6,Objy2=1

Prob0A: 64 iterations in 0.50 seconds (average 0.007875, setup 0.00)

Prob1E: 4 iterations in 0.05 seconds (average 0.012000, setup 0.00)

yes = 1170, no = 219879, maybe = 51600

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=272649, nc=77306, nnz=203683, k=2] [2.7 MB]
Creating vector for yes... [2.1 MB]
Allocating iteration vectors... [2 x 2.1 MB]
TOTAL: [8.9 MB]

Starting iterations...

Iterative method: 92 iterations in 0.37 seconds (average 0.002000, setup 0.18)

Value in the initial state: 0.18882682462646544

Time for model checking: 1.03 seconds.

Result: 0.18882682462646544 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmin=? [ F "AllAreaSearched" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=1,Objy1=0,Objx2=6,Objy2=1

Prob0E: 68 iterations in 0.53 seconds (average 0.007824, setup 0.00)

Prob1A: 3 iterations in 0.04 seconds (average 0.013333, setup 0.00)

yes = 1170, no = 225902, maybe = 45577

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=272649, nc=66322, nnz=171571, k=2] [2.3 MB]
Creating vector for yes... [2.1 MB]
Allocating iteration vectors... [2 x 2.1 MB]
TOTAL: [8.5 MB]

Starting iterations...

Iterative method: 96 iterations in 0.33 seconds (average 0.001917, setup 0.15)

Value in the initial state: 0.001716402529937546

Time for model checking: 1.088 seconds.

Result: 0.001716402529937546 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmax=? [ F "MissionTimeUp"|"AllAreaSearched"|"Crash" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=1,Objy1=0,Objx2=6,Objy2=1

Prob0A: 3 iterations in 0.10 seconds (average 0.034667, setup 0.00)

Prob1E: 192 iterations in 3.08 seconds (average 0.016021, setup 0.00)

yes = 237975, no = 935, maybe = 33739

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=272649, nc=59929, nnz=176099, k=2] [2.3 MB]
Creating vector for yes... [2.1 MB]
Allocating iteration vectors... [2 x 2.1 MB]
TOTAL: [8.6 MB]

Starting iterations...

Iterative method: 74 iterations in 0.35 seconds (average 0.001784, setup 0.22)

Value in the initial state: 0.21516805634210628

Time for model checking: 4.009 seconds.

Result: 0.21516805634210628 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmin=? [ F "MissionTimeUp"|"AllAreaSearched"|"Crash" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=1,Objy1=0,Objx2=6,Objy2=1

Prob0E: 3 iterations in 0.05 seconds (average 0.017333, setup 0.00)

Prob1A: 48 iterations in 0.56 seconds (average 0.011583, setup 0.00)

yes = 232046, no = 935, maybe = 39668

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=272649, nc=70725, nnz=207868, k=2] [2.7 MB]
Creating vector for yes... [2.1 MB]
Allocating iteration vectors... [2 x 2.1 MB]
TOTAL: [8.9 MB]

Starting iterations...

Iterative method: 86 iterations in 0.36 seconds (average 0.001953, setup 0.19)

Value in the initial state: 0.01771890061138557

Time for model checking: 1.053 seconds.

Result: 0.01771890061138557 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"drop"}max=? [ F "deadlock" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=1,Objy1=0,Objx2=6,Objy2=1

Prob0E: 4 iterations in 0.08 seconds (average 0.020000, setup 0.00)

Prob1A: 1 iterations in 0.01 seconds (average 0.012000, setup 0.00)

goal = 1, inf = 0, maybe = 272648

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=272649, nc=361223, nnz=736626, k=4] [10.8 MB]
Building sparse matrix (transition rewards)... [n=272649, nc=361223, nnz=0, k=4] [2.4 MB]
Creating vector for state rewards... [2.1 MB]
Creating vector for inf... [2.1 MB]
Allocating iteration vectors... [2 x 2.1 MB]
TOTAL: [21.6 MB]

Starting iterations...

Iterative method: 84 iterations in 0.83 seconds (average 0.004095, setup 0.48)

Value in the initial state: 0.28310489868952654

Time for model checking: 0.99 seconds.

Result: 0.28310489868952654 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"drop"}min=? [ F "deadlock" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=1,Objy1=0,Objx2=6,Objy2=1

Prob0A: 4 iterations in 0.06 seconds (average 0.014000, setup 0.00)

Prob1E: 5 iterations in 0.07 seconds (average 0.013600, setup 0.00)

Warning: PRISM hasn't checked for zero-reward loops.
Your minimum rewards may be too low...

goal = 1, inf = 0, maybe = 272648

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=272649, nc=361223, nnz=736626, k=4] [10.8 MB]
Building sparse matrix (transition rewards)... [n=272649, nc=361223, nnz=0, k=4] [2.4 MB]
Creating vector for state rewards... [2.1 MB]
Creating vector for inf... [2.1 MB]
Allocating iteration vectors... [2 x 2.1 MB]
TOTAL: [21.6 MB]

Starting iterations...

Iterative method: 74 iterations in 0.68 seconds (average 0.004108, setup 0.38)

Value in the initial state: 0.239127054391122

Time for model checking: 0.826 seconds.

Result: 0.239127054391122 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"charge"}max=? [ F "deadlock" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=1,Objy1=0,Objx2=6,Objy2=1

Prob0E: 4 iterations in 0.04 seconds (average 0.009000, setup 0.00)

Prob1A: 1 iterations in 0.01 seconds (average 0.008000, setup 0.00)

goal = 1, inf = 0, maybe = 272648

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=272649, nc=361223, nnz=736626, k=4] [10.8 MB]
Building sparse matrix (transition rewards)... [n=272649, nc=361223, nnz=8570, k=4] [2.5 MB]
Creating vector for state rewards... [2.1 MB]
Creating vector for inf... [2.1 MB]
Allocating iteration vectors... [2 x 2.1 MB]
TOTAL: [21.7 MB]

Starting iterations...

Iterative method: 81 iterations in 0.72 seconds (average 0.004099, setup 0.39)

Value in the initial state: 0.32994327216815605

Time for model checking: 0.906 seconds.

Result: 0.32994327216815605 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"charge"}min=? [ F "deadlock" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=1,Objy1=0,Objx2=6,Objy2=1

Prob0A: 4 iterations in 0.03 seconds (average 0.008000, setup 0.00)

Prob1E: 5 iterations in 0.06 seconds (average 0.012000, setup 0.00)

Warning: PRISM hasn't checked for zero-reward loops.
Your minimum rewards may be too low...

goal = 1, inf = 0, maybe = 272648

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=272649, nc=361223, nnz=736626, k=4] [10.8 MB]
Building sparse matrix (transition rewards)... [n=272649, nc=361223, nnz=8570, k=4] [2.5 MB]
Creating vector for state rewards... [2.1 MB]
Creating vector for inf... [2.1 MB]
Allocating iteration vectors... [2 x 2.1 MB]
TOTAL: [21.7 MB]

Starting iterations...

Iterative method: 92 iterations in 0.76 seconds (average 0.004130, setup 0.38)

Value in the initial state: 0.031791340272098866

Time for model checking: 0.889 seconds.

Result: 0.031791340272098866 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"time"}max=? [ F "deadlock" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=1,Objy1=0,Objx2=6,Objy2=1

Prob0E: 4 iterations in 0.04 seconds (average 0.009000, setup 0.00)

Prob1A: 1 iterations in 0.01 seconds (average 0.008000, setup 0.00)

goal = 1, inf = 0, maybe = 272648

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=272649, nc=361223, nnz=736626, k=4] [10.8 MB]
Building sparse matrix (transition rewards)... [n=272649, nc=361223, nnz=136097, k=4] [4.0 MB]
Creating vector for state rewards... [2.1 MB]
Creating vector for inf... [2.1 MB]
Allocating iteration vectors... [2 x 2.1 MB]
TOTAL: [23.1 MB]

Starting iterations...

Iterative method: 94 iterations in 0.82 seconds (average 0.004043, setup 0.44)

Value in the initial state: 179.0264813989765

Time for model checking: 1.028 seconds.

Result: 179.0264813989765 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"time"}min=? [ F "deadlock" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=1,Objy1=0,Objx2=6,Objy2=1

Prob0A: 4 iterations in 0.03 seconds (average 0.008000, setup 0.00)

Prob1E: 5 iterations in 0.06 seconds (average 0.012800, setup 0.00)

Warning: PRISM hasn't checked for zero-reward loops.
Your minimum rewards may be too low...

goal = 1, inf = 0, maybe = 272648

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=272649, nc=361223, nnz=736626, k=4] [10.8 MB]
Building sparse matrix (transition rewards)... [n=272649, nc=361223, nnz=136097, k=4] [4.0 MB]
Creating vector for state rewards... [2.1 MB]
Creating vector for inf... [2.1 MB]
Allocating iteration vectors... [2 x 2.1 MB]
TOTAL: [23.1 MB]

Starting iterations...

Iterative method: 95 iterations in 0.84 seconds (average 0.004211, setup 0.44)

Value in the initial state: 86.74432933625164

Time for model checking: 0.955 seconds.

Result: 86.74432933625164 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"success"}max=? [ F "deadlock" ]/Pmin=? [ F "MissionSuccessful" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=1,Objy1=0,Objx2=6,Objy2=1

Prob0E: 4 iterations in 0.03 seconds (average 0.008000, setup 0.00)

Prob1A: 1 iterations in 0.01 seconds (average 0.008000, setup 0.00)

goal = 1, inf = 0, maybe = 272648

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=272649, nc=361223, nnz=736626, k=4] [10.8 MB]
Building sparse matrix (transition rewards)... [n=272649, nc=361223, nnz=1840, k=4] [2.4 MB]
Creating vector for state rewards... [2.1 MB]
Creating vector for inf... [2.1 MB]
Allocating iteration vectors... [2 x 2.1 MB]
TOTAL: [21.6 MB]

Starting iterations...

Iterative method: 88 iterations in 0.73 seconds (average 0.003955, setup 0.38)

Prob0E: 49 iterations in 0.35 seconds (average 0.007102, setup 0.00)

Prob1A: 3 iterations in 0.04 seconds (average 0.014667, setup 0.00)

yes = 934, no = 237976, maybe = 33739

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=272649, nc=59929, nnz=176099, k=2] [2.3 MB]
Creating vector for yes... [2.1 MB]
Allocating iteration vectors... [2 x 2.1 MB]
TOTAL: [8.6 MB]

Starting iterations...

Iterative method: 81 iterations in 0.30 seconds (average 0.001778, setup 0.16)

Value in the initial state: 223.19609803517216

Time for model checking: 1.509 seconds.

Result: 223.19609803517216 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"success"}min=? [ F "deadlock" ]/Pmax=? [ F "MissionSuccessful" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=1,Objy1=0,Objx2=6,Objy2=1

Prob0A: 4 iterations in 0.09 seconds (average 0.023000, setup 0.00)

Prob1E: 5 iterations in 0.06 seconds (average 0.011200, setup 0.00)

Warning: PRISM hasn't checked for zero-reward loops.
Your minimum rewards may be too low...

goal = 1, inf = 0, maybe = 272648

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=272649, nc=361223, nnz=736626, k=4] [10.8 MB]
Building sparse matrix (transition rewards)... [n=272649, nc=361223, nnz=1840, k=4] [2.4 MB]
Creating vector for state rewards... [2.1 MB]
Creating vector for inf... [2.1 MB]
Allocating iteration vectors... [2 x 2.1 MB]
TOTAL: [21.6 MB]

Starting iterations...

Iterative method: 88 iterations in 0.76 seconds (average 0.004045, setup 0.41)

Prob0A: 48 iterations in 0.28 seconds (average 0.005917, setup 0.00)

Prob1E: 4 iterations in 0.04 seconds (average 0.009000, setup 0.00)

yes = 934, no = 232047, maybe = 39668

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=272649, nc=70725, nnz=207868, k=2] [2.7 MB]
Creating vector for yes... [2.1 MB]
Allocating iteration vectors... [2 x 2.1 MB]
TOTAL: [8.9 MB]

Starting iterations...

Iterative method: 89 iterations in 0.34 seconds (average 0.001843, setup 0.17)

Value in the initial state: 76.04964556385873

Time for model checking: 1.58 seconds.

Result: 76.04964556385873 (value in the initial state)

---------------------------------------------------------------------

Note: There were 16 warnings during computation.

