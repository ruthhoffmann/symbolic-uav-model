PRISM
=====

Version: 4.3
Date: Tue Aug 02 12:33:11 BST 2016
Hostname: eglinton
Memory limits: cudd=1g, java(heap)=981.5m
Command line: prism uav_sim_ft_mdp.prism -s uav_mdp.props -const 'Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=5,Objy1=3,Objx2=6,Objy2=2,'

Parsing model file "uav_sim_ft_mdp.prism"...

Parsing properties file "uav_mdp.props"...

18 properties:
(1) Pmax=? [ F "MissionSuccessful" ]
(2) Pmin=? [ F "MissionSuccessful" ]
(3) Pmax=? [ F "MissionTimeUp" ]
(4) Pmin=? [ F "MissionTimeUp" ]
(5) Pmax=? [ F "Crash" ]
(6) Pmin=? [ F "Crash" ]
(7) Pmax=? [ F "AllAreaSearched" ]
(8) Pmin=? [ F "AllAreaSearched" ]
(9) Pmax=? [ F "MissionTimeUp"|"AllAreaSearched"|"Crash" ]
(10) Pmin=? [ F "MissionTimeUp"|"AllAreaSearched"|"Crash" ]
(11) R{"drop"}max=? [ F "deadlock" ]
(12) R{"drop"}min=? [ F "deadlock" ]
(13) R{"charge"}max=? [ F "deadlock" ]
(14) R{"charge"}min=? [ F "deadlock" ]
(15) R{"time"}max=? [ F "deadlock" ]
(16) R{"time"}min=? [ F "deadlock" ]
(17) R{"success"}max=? [ F "deadlock" ]/Pmin=? [ F "MissionSuccessful" ]
(18) R{"success"}min=? [ F "deadlock" ]/Pmax=? [ F "MissionSuccessful" ]

Type:        MDP
Modules:     Obj Move Robot T 
Variables:   NoOfObjs posx posy s1 retx rety b t c 

---------------------------------------------------------------------

Model checking: Pmax=? [ F "MissionSuccessful" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=5,Objy1=3,Objx2=6,Objy2=2

Building model...
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=5,Objy1=3,Objx2=6,Objy2=2

Warning: Guard for command 7 of module "Move" is never satisfied.

Warning: Guard for command 8 of module "Move" is never satisfied.

Warning: Guard for command 9 of module "Move" is never satisfied.

Warning: Guard for command 7 of module "Robot" is never satisfied.

Warning: Update 2 of command 3 of module "Robot" doesn't do anything ("(s1'=13)", line 249, column 44)

Warning: Update 2 of command 27 of module "T" doesn't do anything ("(c'=1)", line 372, column 108)

Warning: Guard for command 28 of module "T" is never satisfied.

Warning: Update 2 of command 9 of module "T" doesn't do anything ("(c'=1)", line 349, column 112)

Warning: Guard for command 10 of module "T" is never satisfied.

Warning: Update 2 of command 51 of module "T" doesn't do anything ("(c'=1)", line 403, column 113)

Warning: Guard for command 52 of module "T" is never satisfied.

Computing reachable states...

Reachability (BFS): 82 iterations in 0.61 seconds (average 0.007463, setup 0.00)

Time for model construction: 1.35 seconds.

Warning: Deadlocks detected and fixed in 1 states

Type:        MDP
States:      207254 (1 initial)
Transitions: 610368
Choices:     287249

Transition matrix: 164321 nodes (76 terminal), 610368 minterms, vars: 34r/34c/18nd

Prob0A: 54 iterations in 0.39 seconds (average 0.007259, setup 0.00)

Prob1E: 4 iterations in 0.05 seconds (average 0.012000, setup 0.00)

yes = 926, no = 169756, maybe = 36572

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=207254, nc=66326, nnz=195867, k=2] [2.5 MB]
Creating vector for yes... [1.6 MB]
Allocating iteration vectors... [2 x 1.6 MB]
TOTAL: [7.2 MB]

Starting iterations...

Iterative method: 100 iterations in 0.34 seconds (average 0.001440, setup 0.20)

Value in the initial state: 0.9716728897726856

Time for model checking: 0.802 seconds.

Result: 0.9716728897726856 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmin=? [ F "MissionSuccessful" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=5,Objy1=3,Objx2=6,Objy2=2

Prob0E: 68 iterations in 0.36 seconds (average 0.005294, setup 0.00)

Prob1A: 3 iterations in 0.03 seconds (average 0.009333, setup 0.00)

yes = 926, no = 175640, maybe = 30688

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=207254, nc=55330, nnz=163326, k=2] [2.1 MB]
Creating vector for yes... [1.6 MB]
Allocating iteration vectors... [2 x 1.6 MB]
TOTAL: [6.9 MB]

Starting iterations...

Iterative method: 93 iterations in 0.26 seconds (average 0.001247, setup 0.14)

Value in the initial state: 0.7250849156234408

Time for model checking: 0.672 seconds.

Result: 0.7250849156234408 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmax=? [ F "MissionTimeUp" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=5,Objy1=3,Objx2=6,Objy2=2

Prob0A: 70 iterations in 0.29 seconds (average 0.004171, setup 0.00)

Prob1E: 4 iterations in 0.03 seconds (average 0.007000, setup 0.00)

yes = 240, no = 119939, maybe = 87075

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=207254, nc=157298, nnz=451656, k=4] [5.5 MB]
Creating vector for yes... [1.6 MB]
Allocating iteration vectors... [2 x 1.6 MB]
TOTAL: [10.3 MB]

Starting iterations...

Iterative method: 95 iterations in 0.39 seconds (average 0.001768, setup 0.22)

Value in the initial state: 0.005586102812273968

Time for model checking: 0.714 seconds.

Result: 0.005586102812273968 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmin=? [ F "MissionTimeUp" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=5,Objy1=3,Objx2=6,Objy2=2

Prob0E: 77 iterations in 0.43 seconds (average 0.005558, setup 0.00)

Prob1A: 3 iterations in 0.04 seconds (average 0.012000, setup 0.00)

yes = 240, no = 120353, maybe = 86661

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=207254, nc=156546, nnz=450230, k=4] [5.5 MB]
Creating vector for yes... [1.6 MB]
Allocating iteration vectors... [2 x 1.6 MB]
TOTAL: [10.2 MB]

Starting iterations...

Iterative method: 111 iterations in 0.42 seconds (average 0.001766, setup 0.23)

Value in the initial state: 2.4566910170406462E-5

Time for model checking: 0.911 seconds.

Result: 2.4566910170406462E-5 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmax=? [ F "Crash" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=5,Objy1=3,Objx2=6,Objy2=2

Prob0A: 3 iterations in 0.06 seconds (average 0.018667, setup 0.00)

Prob1E: 260 iterations in 1.71 seconds (average 0.006585, setup 0.00)

yes = 101066, no = 2419, maybe = 103769

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=207254, nc=183764, nnz=506883, k=4] [6.2 MB]
Creating vector for yes... [1.6 MB]
Allocating iteration vectors... [2 x 1.6 MB]
TOTAL: [10.9 MB]

Starting iterations...

Iterative method: 95 iterations in 0.43 seconds (average 0.001768, setup 0.26)

Value in the initial state: 0.06370479581478743

Time for model checking: 2.212 seconds.

Result: 0.06370479581478743 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmin=? [ F "Crash" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=5,Objy1=3,Objx2=6,Objy2=2

Prob0E: 3 iterations in 0.04 seconds (average 0.012000, setup 0.00)

Prob1A: 54 iterations in 0.29 seconds (average 0.005407, setup 0.00)

yes = 101066, no = 2419, maybe = 103769

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=207254, nc=183764, nnz=506883, k=4] [6.2 MB]
Creating vector for yes... [1.6 MB]
Allocating iteration vectors... [2 x 1.6 MB]
TOTAL: [10.9 MB]

Starting iterations...

Iterative method: 101 iterations in 0.39 seconds (average 0.001861, setup 0.20)

Value in the initial state: 0.025635683806047655

Time for model checking: 0.731 seconds.

Result: 0.025635683806047655 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmax=? [ F "AllAreaSearched" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=5,Objy1=3,Objx2=6,Objy2=2

Prob0A: 55 iterations in 0.24 seconds (average 0.004364, setup 0.00)

Prob1E: 4 iterations in 0.03 seconds (average 0.007000, setup 0.00)

yes = 1252, no = 159309, maybe = 46693

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=207254, nc=79787, nnz=226215, k=2] [2.9 MB]
Creating vector for yes... [1.6 MB]
Allocating iteration vectors... [2 x 1.6 MB]
TOTAL: [7.6 MB]

Starting iterations...

Iterative method: 93 iterations in 0.27 seconds (average 0.001376, setup 0.14)

Value in the initial state: 0.2440272883427208

Time for model checking: 0.559 seconds.

Result: 0.2440272883427208 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmin=? [ F "AllAreaSearched" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=5,Objy1=3,Objx2=6,Objy2=2

Prob0E: 72 iterations in 0.35 seconds (average 0.004889, setup 0.00)

Prob1A: 3 iterations in 0.02 seconds (average 0.008000, setup 0.00)

yes = 1252, no = 167489, maybe = 38513

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=207254, nc=64727, nnz=180127, k=2] [2.3 MB]
Creating vector for yes... [1.6 MB]
Allocating iteration vectors... [2 x 1.6 MB]
TOTAL: [7.1 MB]

Starting iterations...

Iterative method: 94 iterations in 0.23 seconds (average 0.001277, setup 0.11)

Value in the initial state: 0.0012840128403187308

Time for model checking: 0.618 seconds.

Result: 0.0012840128403187308 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmax=? [ F "MissionTimeUp"|"AllAreaSearched"|"Crash" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=5,Objy1=3,Objx2=6,Objy2=2

Prob0A: 3 iterations in 0.06 seconds (average 0.021333, setup 0.00)

Prob1E: 264 iterations in 1.78 seconds (average 0.006727, setup 0.00)

yes = 175639, no = 927, maybe = 30688

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=207254, nc=55330, nnz=163326, k=2] [2.1 MB]
Creating vector for yes... [1.6 MB]
Allocating iteration vectors... [2 x 1.6 MB]
TOTAL: [6.9 MB]

Starting iterations...

Iterative method: 88 iterations in 0.25 seconds (average 0.001227, setup 0.14)

Value in the initial state: 0.27491471601246253

Time for model checking: 2.098 seconds.

Result: 0.27491471601246253 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmin=? [ F "MissionTimeUp"|"AllAreaSearched"|"Crash" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=5,Objy1=3,Objx2=6,Objy2=2

Prob0E: 3 iterations in 0.03 seconds (average 0.010667, setup 0.00)

Prob1A: 54 iterations in 0.26 seconds (average 0.004815, setup 0.00)

yes = 169755, no = 927, maybe = 36572

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=207254, nc=66326, nnz=195867, k=2] [2.5 MB]
Creating vector for yes... [1.6 MB]
Allocating iteration vectors... [2 x 1.6 MB]
TOTAL: [7.2 MB]

Starting iterations...

Iterative method: 98 iterations in 0.26 seconds (average 0.001347, setup 0.13)

Value in the initial state: 0.028326761027336486

Time for model checking: 0.569 seconds.

Result: 0.028326761027336486 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"drop"}max=? [ F "deadlock" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=5,Objy1=3,Objx2=6,Objy2=2

Prob0E: 4 iterations in 0.04 seconds (average 0.009000, setup 0.00)

Prob1A: 1 iterations in 0.00 seconds (average 0.004000, setup 0.00)

goal = 1, inf = 0, maybe = 207253

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=207254, nc=287248, nnz=610367, k=4] [8.9 MB]
Building sparse matrix (transition rewards)... [n=207254, nc=287248, nnz=0, k=4] [1.9 MB]
Creating vector for state rewards... [1.6 MB]
Creating vector for inf... [1.6 MB]
Allocating iteration vectors... [2 x 1.6 MB]
TOTAL: [17.1 MB]

Starting iterations...

Iterative method: 96 iterations in 0.61 seconds (average 0.002833, setup 0.34)

Value in the initial state: 0.33251386380988157

Time for model checking: 0.656 seconds.

Result: 0.33251386380988157 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"drop"}min=? [ F "deadlock" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=5,Objy1=3,Objx2=6,Objy2=2

Prob0A: 4 iterations in 0.02 seconds (average 0.006000, setup 0.00)

Prob1E: 5 iterations in 0.02 seconds (average 0.004800, setup 0.00)

Warning: PRISM hasn't checked for zero-reward loops.
Your minimum rewards may be too low...

goal = 1, inf = 0, maybe = 207253

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=207254, nc=287248, nnz=610367, k=4] [8.9 MB]
Building sparse matrix (transition rewards)... [n=207254, nc=287248, nnz=0, k=4] [1.9 MB]
Creating vector for state rewards... [1.6 MB]
Creating vector for inf... [1.6 MB]
Allocating iteration vectors... [2 x 1.6 MB]
TOTAL: [17.1 MB]

Starting iterations...

Iterative method: 86 iterations in 0.52 seconds (average 0.002837, setup 0.28)

Value in the initial state: 0.23186637481381062

Time for model checking: 0.577 seconds.

Result: 0.23186637481381062 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"charge"}max=? [ F "deadlock" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=5,Objy1=3,Objx2=6,Objy2=2

Prob0E: 4 iterations in 0.02 seconds (average 0.004000, setup 0.00)

Prob1A: 1 iterations in 0.00 seconds (average 0.004000, setup 0.00)

goal = 1, inf = 0, maybe = 207253

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=207254, nc=287248, nnz=610367, k=4] [8.9 MB]
Building sparse matrix (transition rewards)... [n=207254, nc=287248, nnz=8186, k=4] [2.0 MB]
Creating vector for state rewards... [1.6 MB]
Creating vector for inf... [1.6 MB]
Allocating iteration vectors... [2 x 1.6 MB]
TOTAL: [17.2 MB]

Starting iterations...

Iterative method: 90 iterations in 0.54 seconds (average 0.002800, setup 0.28)

Value in the initial state: 0.9897302263859915

Time for model checking: 0.561 seconds.

Result: 0.9897302263859915 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"charge"}min=? [ F "deadlock" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=5,Objy1=3,Objx2=6,Objy2=2

Prob0A: 4 iterations in 0.02 seconds (average 0.004000, setup 0.00)

Prob1E: 5 iterations in 0.03 seconds (average 0.005600, setup 0.00)

Warning: PRISM hasn't checked for zero-reward loops.
Your minimum rewards may be too low...

goal = 1, inf = 0, maybe = 207253

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=207254, nc=287248, nnz=610367, k=4] [8.9 MB]
Building sparse matrix (transition rewards)... [n=207254, nc=287248, nnz=8186, k=4] [2.0 MB]
Creating vector for state rewards... [1.6 MB]
Creating vector for inf... [1.6 MB]
Allocating iteration vectors... [2 x 1.6 MB]
TOTAL: [17.2 MB]

Starting iterations...

Iterative method: 99 iterations in 0.56 seconds (average 0.002869, setup 0.28)

Value in the initial state: 0.19904598540190024

Time for model checking: 0.61 seconds.

Result: 0.19904598540190024 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"time"}max=? [ F "deadlock" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=5,Objy1=3,Objx2=6,Objy2=2

Prob0E: 4 iterations in 0.02 seconds (average 0.004000, setup 0.00)

Prob1A: 1 iterations in 0.00 seconds (average 0.004000, setup 0.00)

goal = 1, inf = 0, maybe = 207253

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=207254, nc=287248, nnz=610367, k=4] [8.9 MB]
Building sparse matrix (transition rewards)... [n=207254, nc=287248, nnz=103483, k=4] [3.1 MB]
Creating vector for state rewards... [1.6 MB]
Creating vector for inf... [1.6 MB]
Allocating iteration vectors... [2 x 1.6 MB]
TOTAL: [18.3 MB]

Starting iterations...

Iterative method: 97 iterations in 0.59 seconds (average 0.002887, setup 0.31)

Value in the initial state: 349.4558772527488

Time for model checking: 0.613 seconds.

Result: 349.4558772527488 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"time"}min=? [ F "deadlock" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=5,Objy1=3,Objx2=6,Objy2=2

Prob0A: 4 iterations in 0.02 seconds (average 0.004000, setup 0.00)

Prob1E: 5 iterations in 0.02 seconds (average 0.004800, setup 0.00)

Warning: PRISM hasn't checked for zero-reward loops.
Your minimum rewards may be too low...

goal = 1, inf = 0, maybe = 207253

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=207254, nc=287248, nnz=610367, k=4] [8.9 MB]
Building sparse matrix (transition rewards)... [n=207254, nc=287248, nnz=103483, k=4] [3.1 MB]
Creating vector for state rewards... [1.6 MB]
Creating vector for inf... [1.6 MB]
Allocating iteration vectors... [2 x 1.6 MB]
TOTAL: [18.3 MB]

Starting iterations...

Iterative method: 105 iterations in 0.61 seconds (average 0.002933, setup 0.30)

Value in the initial state: 141.57495018514948

Time for model checking: 0.661 seconds.

Result: 141.57495018514948 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"success"}max=? [ F "deadlock" ]/Pmin=? [ F "MissionSuccessful" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=5,Objy1=3,Objx2=6,Objy2=2

Prob0E: 4 iterations in 0.02 seconds (average 0.005000, setup 0.00)

Prob1A: 1 iterations in 0.00 seconds (average 0.004000, setup 0.00)

goal = 1, inf = 0, maybe = 207253

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=207254, nc=287248, nnz=610367, k=4] [8.9 MB]
Building sparse matrix (transition rewards)... [n=207254, nc=287248, nnz=1824, k=4] [1.9 MB]
Creating vector for state rewards... [1.6 MB]
Creating vector for inf... [1.6 MB]
Allocating iteration vectors... [2 x 1.6 MB]
TOTAL: [17.1 MB]

Starting iterations...

Iterative method: 100 iterations in 0.56 seconds (average 0.002840, setup 0.28)

Prob0E: 68 iterations in 0.29 seconds (average 0.004235, setup 0.00)

Prob1A: 3 iterations in 0.03 seconds (average 0.009333, setup 0.00)

yes = 926, no = 175640, maybe = 30688

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=207254, nc=55330, nnz=163326, k=2] [2.1 MB]
Creating vector for yes... [1.6 MB]
Allocating iteration vectors... [2 x 1.6 MB]
TOTAL: [6.9 MB]

Starting iterations...

Iterative method: 93 iterations in 0.23 seconds (average 0.001247, setup 0.12)

Value in the initial state: 469.43571469819074

Time for model checking: 1.151 seconds.

Result: 469.43571469819074 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"success"}min=? [ F "deadlock" ]/Pmax=? [ F "MissionSuccessful" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=5,Objy1=3,Objx2=6,Objy2=2

Prob0A: 4 iterations in 0.06 seconds (average 0.014000, setup 0.00)

Prob1E: 5 iterations in 0.03 seconds (average 0.005600, setup 0.00)

Warning: PRISM hasn't checked for zero-reward loops.
Your minimum rewards may be too low...

goal = 1, inf = 0, maybe = 207253

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=207254, nc=287248, nnz=610367, k=4] [8.9 MB]
Building sparse matrix (transition rewards)... [n=207254, nc=287248, nnz=1824, k=4] [1.9 MB]
Creating vector for state rewards... [1.6 MB]
Creating vector for inf... [1.6 MB]
Allocating iteration vectors... [2 x 1.6 MB]
TOTAL: [17.1 MB]

Starting iterations...

Iterative method: 97 iterations in 0.58 seconds (average 0.002887, setup 0.30)

Prob0A: 54 iterations in 0.29 seconds (average 0.005407, setup 0.00)

Prob1E: 4 iterations in 0.03 seconds (average 0.008000, setup 0.00)

yes = 926, no = 169756, maybe = 36572

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=207254, nc=66326, nnz=195867, k=2] [2.5 MB]
Creating vector for yes... [1.6 MB]
Allocating iteration vectors... [2 x 1.6 MB]
TOTAL: [7.2 MB]

Starting iterations...

Iterative method: 100 iterations in 0.27 seconds (average 0.001240, setup 0.15)

Value in the initial state: 85.07038651732026

Time for model checking: 1.254 seconds.

Result: 85.07038651732026 (value in the initial state)

---------------------------------------------------------------------

Note: There were 16 warnings during computation.

