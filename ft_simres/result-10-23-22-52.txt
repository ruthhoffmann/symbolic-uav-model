PRISM
=====

Version: 4.3
Date: Tue Aug 02 12:39:06 BST 2016
Hostname: eglinton
Memory limits: cudd=1g, java(heap)=981.5m
Command line: prism uav_sim_ft_mdp.prism -s uav_mdp.props -const 'Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=2,Objy1=2,Objx2=5,Objy2=2,'

Parsing model file "uav_sim_ft_mdp.prism"...

Parsing properties file "uav_mdp.props"...

18 properties:
(1) Pmax=? [ F "MissionSuccessful" ]
(2) Pmin=? [ F "MissionSuccessful" ]
(3) Pmax=? [ F "MissionTimeUp" ]
(4) Pmin=? [ F "MissionTimeUp" ]
(5) Pmax=? [ F "Crash" ]
(6) Pmin=? [ F "Crash" ]
(7) Pmax=? [ F "AllAreaSearched" ]
(8) Pmin=? [ F "AllAreaSearched" ]
(9) Pmax=? [ F "MissionTimeUp"|"AllAreaSearched"|"Crash" ]
(10) Pmin=? [ F "MissionTimeUp"|"AllAreaSearched"|"Crash" ]
(11) R{"drop"}max=? [ F "deadlock" ]
(12) R{"drop"}min=? [ F "deadlock" ]
(13) R{"charge"}max=? [ F "deadlock" ]
(14) R{"charge"}min=? [ F "deadlock" ]
(15) R{"time"}max=? [ F "deadlock" ]
(16) R{"time"}min=? [ F "deadlock" ]
(17) R{"success"}max=? [ F "deadlock" ]/Pmin=? [ F "MissionSuccessful" ]
(18) R{"success"}min=? [ F "deadlock" ]/Pmax=? [ F "MissionSuccessful" ]

Type:        MDP
Modules:     Obj Move Robot T 
Variables:   NoOfObjs posx posy s1 retx rety b t c 

---------------------------------------------------------------------

Model checking: Pmax=? [ F "MissionSuccessful" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=2,Objy1=2,Objx2=5,Objy2=2

Building model...
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=2,Objy1=2,Objx2=5,Objy2=2

Warning: Guard for command 7 of module "Move" is never satisfied.

Warning: Guard for command 8 of module "Move" is never satisfied.

Warning: Guard for command 9 of module "Move" is never satisfied.

Warning: Guard for command 7 of module "Robot" is never satisfied.

Warning: Update 2 of command 3 of module "Robot" doesn't do anything ("(s1'=13)", line 249, column 44)

Warning: Update 2 of command 27 of module "T" doesn't do anything ("(c'=1)", line 372, column 108)

Warning: Guard for command 28 of module "T" is never satisfied.

Warning: Update 2 of command 9 of module "T" doesn't do anything ("(c'=1)", line 349, column 112)

Warning: Guard for command 10 of module "T" is never satisfied.

Warning: Update 2 of command 51 of module "T" doesn't do anything ("(c'=1)", line 403, column 113)

Warning: Guard for command 52 of module "T" is never satisfied.

Computing reachable states...

Reachability (BFS): 81 iterations in 0.74 seconds (average 0.009136, setup 0.00)

Time for model construction: 2.201 seconds.

Warning: Deadlocks detected and fixed in 1 states

Type:        MDP
States:      183588 (1 initial)
Transitions: 510381
Choices:     248926

Transition matrix: 160796 nodes (76 terminal), 510381 minterms, vars: 34r/34c/18nd

Prob0A: 48 iterations in 0.36 seconds (average 0.007500, setup 0.00)

Prob1E: 4 iterations in 0.05 seconds (average 0.012000, setup 0.00)

yes = 941, no = 152439, maybe = 30208

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=183588, nc=54521, nnz=157180, k=2] [2.0 MB]
Creating vector for yes... [1.4 MB]
Allocating iteration vectors... [2 x 1.4 MB]
TOTAL: [6.2 MB]

Starting iterations...

Iterative method: 93 iterations in 0.32 seconds (average 0.001247, setup 0.21)

Value in the initial state: 0.9797364738674883

Time for model checking: 0.76 seconds.

Result: 0.9797364738674883 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmin=? [ F "MissionSuccessful" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=2,Objy1=2,Objx2=5,Objy2=2

Prob0E: 48 iterations in 0.36 seconds (average 0.007417, setup 0.00)

Prob1A: 3 iterations in 0.04 seconds (average 0.014667, setup 0.00)

yes = 941, no = 156113, maybe = 26534

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=183588, nc=47761, nnz=137625, k=2] [1.8 MB]
Creating vector for yes... [1.4 MB]
Allocating iteration vectors... [2 x 1.4 MB]
TOTAL: [6.0 MB]

Starting iterations...

Iterative method: 86 iterations in 0.27 seconds (average 0.001209, setup 0.16)

Value in the initial state: 0.7489124918851459

Time for model checking: 0.686 seconds.

Result: 0.7489124918851459 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmax=? [ F "MissionTimeUp" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=2,Objy1=2,Objx2=5,Objy2=2

Prob0A: 67 iterations in 0.42 seconds (average 0.006269, setup 0.00)

Prob1E: 4 iterations in 0.04 seconds (average 0.009000, setup 0.00)

yes = 304, no = 111961, maybe = 71323

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=183588, nc=126775, nnz=356265, k=4] [4.4 MB]
Creating vector for yes... [1.4 MB]
Allocating iteration vectors... [2 x 1.4 MB]
TOTAL: [8.6 MB]

Starting iterations...

Iterative method: 90 iterations in 0.38 seconds (average 0.001644, setup 0.23)

Value in the initial state: 5.561828442569151E-4

Time for model checking: 0.846 seconds.

Result: 5.561828442569151E-4 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmin=? [ F "MissionTimeUp" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=2,Objy1=2,Objx2=5,Objy2=2

Prob0E: 74 iterations in 0.51 seconds (average 0.006919, setup 0.00)

Prob1A: 3 iterations in 0.04 seconds (average 0.012000, setup 0.00)

yes = 304, no = 112337, maybe = 70947

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=183588, nc=126099, nnz=354917, k=4] [4.4 MB]
Creating vector for yes... [1.4 MB]
Allocating iteration vectors... [2 x 1.4 MB]
TOTAL: [8.6 MB]

Starting iterations...

Iterative method: 104 iterations in 0.39 seconds (average 0.001577, setup 0.23)

Value in the initial state: 3.272545213819308E-6

Time for model checking: 0.963 seconds.

Result: 3.272545213819308E-6 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmax=? [ F "Crash" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=2,Objy1=2,Objx2=5,Objy2=2

Prob0A: 3 iterations in 0.08 seconds (average 0.025333, setup 0.00)

Prob1E: 184 iterations in 2.29 seconds (average 0.012457, setup 0.00)

yes = 89796, no = 2486, maybe = 91306

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=183588, nc=156644, nnz=418099, k=4] [5.1 MB]
Creating vector for yes... [1.4 MB]
Allocating iteration vectors... [2 x 1.4 MB]
TOTAL: [9.3 MB]

Starting iterations...

Iterative method: 86 iterations in 0.43 seconds (average 0.001721, setup 0.28)

Value in the initial state: 0.030988735738874777

Time for model checking: 2.826 seconds.

Result: 0.030988735738874777 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmin=? [ F "Crash" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=2,Objy1=2,Objx2=5,Objy2=2

Prob0E: 3 iterations in 0.06 seconds (average 0.018667, setup 0.00)

Prob1A: 48 iterations in 0.50 seconds (average 0.010500, setup 0.00)

yes = 89796, no = 2486, maybe = 91306

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=183588, nc=156644, nnz=418099, k=4] [5.1 MB]
Creating vector for yes... [1.4 MB]
Allocating iteration vectors... [2 x 1.4 MB]
TOTAL: [9.3 MB]

Starting iterations...

Iterative method: 89 iterations in 0.39 seconds (average 0.001798, setup 0.23)

Value in the initial state: 0.01823835294949043

Time for model checking: 0.968 seconds.

Result: 0.01823835294949043 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmax=? [ F "AllAreaSearched" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=2,Objy1=2,Objx2=5,Objy2=2

Prob0A: 50 iterations in 0.26 seconds (average 0.005200, setup 0.00)

Prob1E: 4 iterations in 0.03 seconds (average 0.007000, setup 0.00)

yes = 1240, no = 136537, maybe = 45811

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=183588, nc=75342, nnz=207183, k=2] [2.6 MB]
Creating vector for yes... [1.4 MB]
Allocating iteration vectors... [2 x 1.4 MB]
TOTAL: [6.8 MB]

Starting iterations...

Iterative method: 86 iterations in 0.27 seconds (average 0.001395, setup 0.15)

Value in the initial state: 0.22345087848487138

Time for model checking: 0.578 seconds.

Result: 0.22345087848487138 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmin=? [ F "AllAreaSearched" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=2,Objy1=2,Objx2=5,Objy2=2

Prob0E: 62 iterations in 0.33 seconds (average 0.005290, setup 0.00)

Prob1A: 3 iterations in 0.02 seconds (average 0.006667, setup 0.00)

yes = 1240, no = 142520, maybe = 39828

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=183588, nc=64387, nnz=174924, k=2] [2.2 MB]
Creating vector for yes... [1.4 MB]
Allocating iteration vectors... [2 x 1.4 MB]
TOTAL: [6.4 MB]

Starting iterations...

Iterative method: 91 iterations in 0.22 seconds (average 0.001319, setup 0.10)

Value in the initial state: 0.0017712391631941784

Time for model checking: 0.586 seconds.

Result: 0.0017712391631941784 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmax=? [ F "MissionTimeUp"|"AllAreaSearched"|"Crash" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=2,Objy1=2,Objx2=5,Objy2=2

Prob0A: 3 iterations in 0.10 seconds (average 0.033333, setup 0.00)

Prob1E: 188 iterations in 2.14 seconds (average 0.011404, setup 0.00)

yes = 156112, no = 942, maybe = 26534

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=183588, nc=47761, nnz=137625, k=2] [1.8 MB]
Creating vector for yes... [1.4 MB]
Allocating iteration vectors... [2 x 1.4 MB]
TOTAL: [6.0 MB]

Starting iterations...

Iterative method: 79 iterations in 0.26 seconds (average 0.001215, setup 0.16)

Value in the initial state: 0.2510867477235036

Time for model checking: 2.498 seconds.

Result: 0.2510867477235036 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmin=? [ F "MissionTimeUp"|"AllAreaSearched"|"Crash" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=2,Objy1=2,Objx2=5,Objy2=2

Prob0E: 3 iterations in 0.04 seconds (average 0.013333, setup 0.00)

Prob1A: 48 iterations in 0.38 seconds (average 0.008000, setup 0.00)

yes = 152438, no = 942, maybe = 30208

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=183588, nc=54521, nnz=157180, k=2] [2.0 MB]
Creating vector for yes... [1.4 MB]
Allocating iteration vectors... [2 x 1.4 MB]
TOTAL: [6.2 MB]

Starting iterations...

Iterative method: 89 iterations in 0.24 seconds (average 0.001303, setup 0.13)

Value in the initial state: 0.020262901238852174

Time for model checking: 0.691 seconds.

Result: 0.020262901238852174 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"drop"}max=? [ F "deadlock" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=2,Objy1=2,Objx2=5,Objy2=2

Prob0E: 4 iterations in 0.05 seconds (average 0.012000, setup 0.00)

Prob1A: 1 iterations in 0.00 seconds (average 0.004000, setup 0.00)

goal = 1, inf = 0, maybe = 183587

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=183588, nc=248925, nnz=510380, k=4] [7.5 MB]
Building sparse matrix (transition rewards)... [n=183588, nc=248925, nnz=0, k=4] [1.6 MB]
Creating vector for state rewards... [1.4 MB]
Creating vector for inf... [1.4 MB]
Allocating iteration vectors... [2 x 1.4 MB]
TOTAL: [14.7 MB]

Starting iterations...

Iterative method: 90 iterations in 0.58 seconds (average 0.002667, setup 0.34)

Value in the initial state: 0.18790111960378567

Time for model checking: 0.65 seconds.

Result: 0.18790111960378567 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"drop"}min=? [ F "deadlock" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=2,Objy1=2,Objx2=5,Objy2=2

Prob0A: 4 iterations in 0.03 seconds (average 0.007000, setup 0.00)

Prob1E: 5 iterations in 0.04 seconds (average 0.008800, setup 0.00)

Warning: PRISM hasn't checked for zero-reward loops.
Your minimum rewards may be too low...

goal = 1, inf = 0, maybe = 183587

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=183588, nc=248925, nnz=510380, k=4] [7.5 MB]
Building sparse matrix (transition rewards)... [n=183588, nc=248925, nnz=0, k=4] [1.6 MB]
Creating vector for state rewards... [1.4 MB]
Creating vector for inf... [1.4 MB]
Allocating iteration vectors... [2 x 1.4 MB]
TOTAL: [14.7 MB]

Starting iterations...

Iterative method: 79 iterations in 0.47 seconds (average 0.002633, setup 0.26)

Value in the initial state: 0.1510462935764439

Time for model checking: 0.568 seconds.

Result: 0.1510462935764439 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"charge"}max=? [ F "deadlock" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=2,Objy1=2,Objx2=5,Objy2=2

Prob0E: 4 iterations in 0.02 seconds (average 0.005000, setup 0.00)

Prob1A: 1 iterations in 0.01 seconds (average 0.008000, setup 0.00)

goal = 1, inf = 0, maybe = 183587

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=183588, nc=248925, nnz=510380, k=4] [7.5 MB]
Building sparse matrix (transition rewards)... [n=183588, nc=248925, nnz=6170, k=4] [1.7 MB]
Creating vector for state rewards... [1.4 MB]
Creating vector for inf... [1.4 MB]
Allocating iteration vectors... [2 x 1.4 MB]
TOTAL: [14.8 MB]

Starting iterations...

Iterative method: 85 iterations in 0.50 seconds (average 0.002729, setup 0.27)

Value in the initial state: 0.27478634869681395

Time for model checking: 0.535 seconds.

Result: 0.27478634869681395 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"charge"}min=? [ F "deadlock" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=2,Objy1=2,Objx2=5,Objy2=2

Prob0A: 4 iterations in 0.02 seconds (average 0.005000, setup 0.00)

Prob1E: 5 iterations in 0.04 seconds (average 0.008800, setup 0.00)

Warning: PRISM hasn't checked for zero-reward loops.
Your minimum rewards may be too low...

goal = 1, inf = 0, maybe = 183587

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=183588, nc=248925, nnz=510380, k=4] [7.5 MB]
Building sparse matrix (transition rewards)... [n=183588, nc=248925, nnz=6170, k=4] [1.7 MB]
Creating vector for state rewards... [1.4 MB]
Creating vector for inf... [1.4 MB]
Allocating iteration vectors... [2 x 1.4 MB]
TOTAL: [14.8 MB]

Starting iterations...

Iterative method: 96 iterations in 0.53 seconds (average 0.002667, setup 0.27)

Value in the initial state: 0.06146317832159221

Time for model checking: 0.606 seconds.

Result: 0.06146317832159221 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"time"}max=? [ F "deadlock" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=2,Objy1=2,Objx2=5,Objy2=2

Prob0E: 4 iterations in 0.02 seconds (average 0.006000, setup 0.00)

Prob1A: 1 iterations in 0.00 seconds (average 0.004000, setup 0.00)

goal = 1, inf = 0, maybe = 183587

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=183588, nc=248925, nnz=510380, k=4] [7.5 MB]
Building sparse matrix (transition rewards)... [n=183588, nc=248925, nnz=92280, k=4] [2.7 MB]
Creating vector for state rewards... [1.4 MB]
Creating vector for inf... [1.4 MB]
Allocating iteration vectors... [2 x 1.4 MB]
TOTAL: [15.8 MB]

Starting iterations...

Iterative method: 88 iterations in 0.54 seconds (average 0.002773, setup 0.30)

Value in the initial state: 170.99906479255682

Time for model checking: 0.58 seconds.

Result: 170.99906479255682 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"time"}min=? [ F "deadlock" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=2,Objy1=2,Objx2=5,Objy2=2

Prob0A: 4 iterations in 0.02 seconds (average 0.005000, setup 0.00)

Prob1E: 5 iterations in 0.04 seconds (average 0.008800, setup 0.00)

Warning: PRISM hasn't checked for zero-reward loops.
Your minimum rewards may be too low...

goal = 1, inf = 0, maybe = 183587

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=183588, nc=248925, nnz=510380, k=4] [7.5 MB]
Building sparse matrix (transition rewards)... [n=183588, nc=248925, nnz=92280, k=4] [2.7 MB]
Creating vector for state rewards... [1.4 MB]
Creating vector for inf... [1.4 MB]
Allocating iteration vectors... [2 x 1.4 MB]
TOTAL: [15.8 MB]

Starting iterations...

Iterative method: 97 iterations in 0.56 seconds (average 0.002804, setup 0.29)

Value in the initial state: 101.04950598564659

Time for model checking: 0.644 seconds.

Result: 101.04950598564659 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"success"}max=? [ F "deadlock" ]/Pmin=? [ F "MissionSuccessful" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=2,Objy1=2,Objx2=5,Objy2=2

Prob0E: 4 iterations in 0.02 seconds (average 0.006000, setup 0.00)

Prob1A: 1 iterations in 0.00 seconds (average 0.004000, setup 0.00)

goal = 1, inf = 0, maybe = 183587

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=183588, nc=248925, nnz=510380, k=4] [7.5 MB]
Building sparse matrix (transition rewards)... [n=183588, nc=248925, nnz=1854, k=4] [1.7 MB]
Creating vector for state rewards... [1.4 MB]
Creating vector for inf... [1.4 MB]
Allocating iteration vectors... [2 x 1.4 MB]
TOTAL: [14.8 MB]

Starting iterations...

Iterative method: 90 iterations in 0.49 seconds (average 0.002667, setup 0.25)

Prob0E: 48 iterations in 0.31 seconds (average 0.006417, setup 0.00)

Prob1A: 3 iterations in 0.03 seconds (average 0.010667, setup 0.00)

yes = 941, no = 156113, maybe = 26534

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=183588, nc=47761, nnz=137625, k=2] [1.8 MB]
Creating vector for yes... [1.4 MB]
Allocating iteration vectors... [2 x 1.4 MB]
TOTAL: [6.0 MB]

Starting iterations...

Iterative method: 86 iterations in 0.20 seconds (average 0.001209, setup 0.10)

Value in the initial state: 223.65406816905573

Time for model checking: 1.113 seconds.

Result: 223.65406816905573 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"success"}min=? [ F "deadlock" ]/Pmax=? [ F "MissionSuccessful" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=2,Objy1=2,Objx2=5,Objy2=2

Prob0A: 4 iterations in 0.05 seconds (average 0.012000, setup 0.00)

Prob1E: 5 iterations in 0.04 seconds (average 0.008800, setup 0.00)

Warning: PRISM hasn't checked for zero-reward loops.
Your minimum rewards may be too low...

goal = 1, inf = 0, maybe = 183587

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=183588, nc=248925, nnz=510380, k=4] [7.5 MB]
Building sparse matrix (transition rewards)... [n=183588, nc=248925, nnz=1854, k=4] [1.7 MB]
Creating vector for state rewards... [1.4 MB]
Creating vector for inf... [1.4 MB]
Allocating iteration vectors... [2 x 1.4 MB]
TOTAL: [14.8 MB]

Starting iterations...

Iterative method: 91 iterations in 0.52 seconds (average 0.002725, setup 0.27)

Prob0A: 48 iterations in 0.28 seconds (average 0.005917, setup 0.00)

Prob1E: 4 iterations in 0.04 seconds (average 0.009000, setup 0.00)

yes = 941, no = 152439, maybe = 30208

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=183588, nc=54521, nnz=157180, k=2] [2.0 MB]
Creating vector for yes... [1.4 MB]
Allocating iteration vectors... [2 x 1.4 MB]
TOTAL: [6.2 MB]

Starting iterations...

Iterative method: 93 iterations in 0.28 seconds (average 0.001204, setup 0.16)

Value in the initial state: 77.70549118762854

Time for model checking: 1.211 seconds.

Result: 77.70549118762854 (value in the initial state)

---------------------------------------------------------------------

Note: There were 16 warnings during computation.

