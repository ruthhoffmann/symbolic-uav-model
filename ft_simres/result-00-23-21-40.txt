PRISM
=====

Version: 4.3
Date: Tue Aug 02 12:27:28 BST 2016
Hostname: eglinton
Memory limits: cudd=1g, java(heap)=981.5m
Command line: prism uav_sim_ft_mdp.prism -s uav_mdp.props -const 'Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=2,Objy1=1,Objx2=4,Objy2=0,'

Parsing model file "uav_sim_ft_mdp.prism"...

Parsing properties file "uav_mdp.props"...

18 properties:
(1) Pmax=? [ F "MissionSuccessful" ]
(2) Pmin=? [ F "MissionSuccessful" ]
(3) Pmax=? [ F "MissionTimeUp" ]
(4) Pmin=? [ F "MissionTimeUp" ]
(5) Pmax=? [ F "Crash" ]
(6) Pmin=? [ F "Crash" ]
(7) Pmax=? [ F "AllAreaSearched" ]
(8) Pmin=? [ F "AllAreaSearched" ]
(9) Pmax=? [ F "MissionTimeUp"|"AllAreaSearched"|"Crash" ]
(10) Pmin=? [ F "MissionTimeUp"|"AllAreaSearched"|"Crash" ]
(11) R{"drop"}max=? [ F "deadlock" ]
(12) R{"drop"}min=? [ F "deadlock" ]
(13) R{"charge"}max=? [ F "deadlock" ]
(14) R{"charge"}min=? [ F "deadlock" ]
(15) R{"time"}max=? [ F "deadlock" ]
(16) R{"time"}min=? [ F "deadlock" ]
(17) R{"success"}max=? [ F "deadlock" ]/Pmin=? [ F "MissionSuccessful" ]
(18) R{"success"}min=? [ F "deadlock" ]/Pmax=? [ F "MissionSuccessful" ]

Type:        MDP
Modules:     Obj Move Robot T 
Variables:   NoOfObjs posx posy s1 retx rety b t c 

---------------------------------------------------------------------

Model checking: Pmax=? [ F "MissionSuccessful" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=2,Objy1=1,Objx2=4,Objy2=0

Building model...
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=2,Objy1=1,Objx2=4,Objy2=0

Warning: Guard for command 7 of module "Move" is never satisfied.

Warning: Guard for command 8 of module "Move" is never satisfied.

Warning: Guard for command 9 of module "Move" is never satisfied.

Warning: Guard for command 7 of module "Robot" is never satisfied.

Warning: Update 2 of command 3 of module "Robot" doesn't do anything ("(s1'=13)", line 249, column 44)

Warning: Update 2 of command 27 of module "T" doesn't do anything ("(c'=1)", line 372, column 108)

Warning: Guard for command 28 of module "T" is never satisfied.

Warning: Update 2 of command 9 of module "T" doesn't do anything ("(c'=1)", line 349, column 112)

Warning: Guard for command 10 of module "T" is never satisfied.

Warning: Update 2 of command 51 of module "T" doesn't do anything ("(c'=1)", line 403, column 113)

Warning: Guard for command 52 of module "T" is never satisfied.

Computing reachable states...

Reachability (BFS): 75 iterations in 1.13 seconds (average 0.015040, setup 0.00)

Time for model construction: 2.488 seconds.

Warning: Deadlocks detected and fixed in 1 states

Type:        MDP
States:      224476 (1 initial)
Transitions: 589689
Choices:     294782

Transition matrix: 173869 nodes (76 terminal), 589689 minterms, vars: 34r/34c/18nd

Prob0A: 46 iterations in 0.41 seconds (average 0.008957, setup 0.00)

Prob1E: 4 iterations in 0.06 seconds (average 0.015000, setup 0.00)

yes = 952, no = 190345, maybe = 33179

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=224476, nc=58557, nnz=168284, k=2] [2.2 MB]
Creating vector for yes... [1.7 MB]
Allocating iteration vectors... [2 x 1.7 MB]
TOTAL: [7.3 MB]

Starting iterations...

Iterative method: 87 iterations in 0.38 seconds (average 0.001563, setup 0.24)

Value in the initial state: 0.9819795411720827

Time for model checking: 0.896 seconds.

Result: 0.9819795411720827 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmin=? [ F "MissionSuccessful" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=2,Objy1=1,Objx2=4,Objy2=0

Prob0E: 48 iterations in 0.54 seconds (average 0.011167, setup 0.00)

Prob1A: 3 iterations in 0.06 seconds (average 0.020000, setup 0.00)

yes = 952, no = 195619, maybe = 27905

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=224476, nc=49073, nnz=140515, k=2] [1.9 MB]
Creating vector for yes... [1.7 MB]
Allocating iteration vectors... [2 x 1.7 MB]
TOTAL: [7.0 MB]

Starting iterations...

Iterative method: 79 iterations in 0.32 seconds (average 0.001468, setup 0.20)

Value in the initial state: 0.7411896956131099

Time for model checking: 0.936 seconds.

Result: 0.7411896956131099 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmax=? [ F "MissionTimeUp" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=2,Objy1=1,Objx2=4,Objy2=0

Prob0A: 57 iterations in 0.66 seconds (average 0.011509, setup 0.00)

Prob1E: 4 iterations in 0.04 seconds (average 0.010000, setup 0.00)

yes = 448, no = 139419, maybe = 84609

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=224476, nc=145234, nnz=401558, k=4] [4.9 MB]
Creating vector for yes... [1.7 MB]
Allocating iteration vectors... [2 x 1.7 MB]
TOTAL: [10.1 MB]

Starting iterations...

Iterative method: 90 iterations in 0.45 seconds (average 0.001911, setup 0.28)

Value in the initial state: 8.523478984435298E-4

Time for model checking: 1.175 seconds.

Result: 8.523478984435298E-4 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmin=? [ F "MissionTimeUp" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=2,Objy1=1,Objx2=4,Objy2=0

Prob0E: 65 iterations in 0.57 seconds (average 0.008738, setup 0.00)

Prob1A: 3 iterations in 0.04 seconds (average 0.014667, setup 0.00)

yes = 448, no = 139776, maybe = 84252

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=224476, nc=144596, nnz=400249, k=4] [4.9 MB]
Creating vector for yes... [1.7 MB]
Allocating iteration vectors... [2 x 1.7 MB]
TOTAL: [10.1 MB]

Starting iterations...

Iterative method: 104 iterations in 0.46 seconds (average 0.002000, setup 0.25)

Value in the initial state: 1.8191090148640996E-6

Time for model checking: 1.088 seconds.

Result: 1.8191090148640996E-6 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmax=? [ F "Crash" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=2,Objy1=1,Objx2=4,Objy2=0

Prob0A: 3 iterations in 0.08 seconds (average 0.026667, setup 0.00)

Prob1E: 180 iterations in 2.55 seconds (average 0.014156, setup 0.00)

yes = 110021, no = 2611, maybe = 111844

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=224476, nc=182150, nnz=477057, k=4] [5.8 MB]
Creating vector for yes... [1.7 MB]
Allocating iteration vectors... [2 x 1.7 MB]
TOTAL: [11.0 MB]

Starting iterations...

Iterative method: 86 iterations in 0.51 seconds (average 0.002140, setup 0.33)

Value in the initial state: 0.030912612554401808

Time for model checking: 3.168 seconds.

Result: 0.030912612554401808 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmin=? [ F "Crash" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=2,Objy1=1,Objx2=4,Objy2=0

Prob0E: 3 iterations in 0.06 seconds (average 0.020000, setup 0.00)

Prob1A: 43 iterations in 0.49 seconds (average 0.011349, setup 0.00)

yes = 110021, no = 2611, maybe = 111844

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=224476, nc=182150, nnz=477057, k=4] [5.8 MB]
Creating vector for yes... [1.7 MB]
Allocating iteration vectors... [2 x 1.7 MB]
TOTAL: [11.0 MB]

Starting iterations...

Iterative method: 82 iterations in 0.42 seconds (average 0.002195, setup 0.24)

Value in the initial state: 0.015972172809943355

Time for model checking: 0.993 seconds.

Result: 0.015972172809943355 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmax=? [ F "AllAreaSearched" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=2,Objy1=1,Objx2=4,Objy2=0

Prob0A: 59 iterations in 0.51 seconds (average 0.008678, setup 0.00)

Prob1E: 4 iterations in 0.04 seconds (average 0.010000, setup 0.00)

yes = 1210, no = 177743, maybe = 45523

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=224476, nc=67675, nnz=174123, k=2] [2.3 MB]
Creating vector for yes... [1.7 MB]
Allocating iteration vectors... [2 x 1.7 MB]
TOTAL: [7.4 MB]

Starting iterations...

Iterative method: 87 iterations in 0.31 seconds (average 0.001655, setup 0.16)

Value in the initial state: 0.2301052687760488

Time for model checking: 0.885 seconds.

Result: 0.2301052687760488 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmin=? [ F "AllAreaSearched" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=2,Objy1=1,Objx2=4,Objy2=0

Prob0E: 63 iterations in 0.37 seconds (average 0.005841, setup 0.00)

Prob1A: 3 iterations in 0.03 seconds (average 0.009333, setup 0.00)

yes = 1210, no = 183384, maybe = 39882

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=224476, nc=58148, nnz=147400, k=2] [2.0 MB]
Creating vector for yes... [1.7 MB]
Allocating iteration vectors... [2 x 1.7 MB]
TOTAL: [7.1 MB]

Starting iterations...

Iterative method: 92 iterations in 0.25 seconds (average 0.001478, setup 0.12)

Value in the initial state: 0.0017452738100720243

Time for model checking: 0.664 seconds.

Result: 0.0017452738100720243 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmax=? [ F "MissionTimeUp"|"AllAreaSearched"|"Crash" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=2,Objy1=1,Objx2=4,Objy2=0

Prob0A: 3 iterations in 0.12 seconds (average 0.041333, setup 0.00)

Prob1E: 188 iterations in 2.46 seconds (average 0.013085, setup 0.00)

yes = 195618, no = 953, maybe = 27905

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=224476, nc=49073, nnz=140515, k=2] [1.9 MB]
Creating vector for yes... [1.7 MB]
Allocating iteration vectors... [2 x 1.7 MB]
TOTAL: [7.0 MB]

Starting iterations...

Iterative method: 73 iterations in 0.31 seconds (average 0.001479, setup 0.20)

Value in the initial state: 0.25880999844033037

Time for model checking: 2.88 seconds.

Result: 0.25880999844033037 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmin=? [ F "MissionTimeUp"|"AllAreaSearched"|"Crash" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=2,Objy1=1,Objx2=4,Objy2=0

Prob0E: 3 iterations in 0.06 seconds (average 0.018667, setup 0.00)

Prob1A: 46 iterations in 0.45 seconds (average 0.009826, setup 0.00)

yes = 190344, no = 953, maybe = 33179

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=224476, nc=58557, nnz=168284, k=2] [2.2 MB]
Creating vector for yes... [1.7 MB]
Allocating iteration vectors... [2 x 1.7 MB]
TOTAL: [7.3 MB]

Starting iterations...

Iterative method: 86 iterations in 0.30 seconds (average 0.001488, setup 0.17)

Value in the initial state: 0.01801982197344987

Time for model checking: 0.834 seconds.

Result: 0.01801982197344987 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"drop"}max=? [ F "deadlock" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=2,Objy1=1,Objx2=4,Objy2=0

Prob0E: 4 iterations in 0.06 seconds (average 0.014000, setup 0.00)

Prob1A: 1 iterations in 0.00 seconds (average 0.004000, setup 0.00)

goal = 1, inf = 0, maybe = 224475

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=224476, nc=294781, nnz=589688, k=4] [8.7 MB]
Building sparse matrix (transition rewards)... [n=224476, nc=294781, nnz=0, k=4] [2.0 MB]
Creating vector for state rewards... [1.7 MB]
Creating vector for inf... [1.7 MB]
Allocating iteration vectors... [2 x 1.7 MB]
TOTAL: [17.6 MB]

Starting iterations...

Iterative method: 83 iterations in 0.69 seconds (average 0.003277, setup 0.42)

Value in the initial state: 0.21134057547194626

Time for model checking: 0.762 seconds.

Result: 0.21134057547194626 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"drop"}min=? [ F "deadlock" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=2,Objy1=1,Objx2=4,Objy2=0

Prob0A: 4 iterations in 0.03 seconds (average 0.008000, setup 0.00)

Prob1E: 5 iterations in 0.05 seconds (average 0.010400, setup 0.00)

Warning: PRISM hasn't checked for zero-reward loops.
Your minimum rewards may be too low...

goal = 1, inf = 0, maybe = 224475

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=224476, nc=294781, nnz=589688, k=4] [8.7 MB]
Building sparse matrix (transition rewards)... [n=224476, nc=294781, nnz=0, k=4] [2.0 MB]
Creating vector for state rewards... [1.7 MB]
Creating vector for inf... [1.7 MB]
Allocating iteration vectors... [2 x 1.7 MB]
TOTAL: [17.6 MB]

Starting iterations...

Iterative method: 74 iterations in 0.56 seconds (average 0.003297, setup 0.32)

Value in the initial state: 0.1874950032272212

Time for model checking: 0.66 seconds.

Result: 0.1874950032272212 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"charge"}max=? [ F "deadlock" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=2,Objy1=1,Objx2=4,Objy2=0

Prob0E: 4 iterations in 0.02 seconds (average 0.006000, setup 0.00)

Prob1A: 1 iterations in 0.01 seconds (average 0.008000, setup 0.00)

goal = 1, inf = 0, maybe = 224475

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=224476, nc=294781, nnz=589688, k=4] [8.7 MB]
Building sparse matrix (transition rewards)... [n=224476, nc=294781, nnz=6458, k=4] [2.1 MB]
Creating vector for state rewards... [1.7 MB]
Creating vector for inf... [1.7 MB]
Allocating iteration vectors... [2 x 1.7 MB]
TOTAL: [17.6 MB]

Starting iterations...

Iterative method: 80 iterations in 0.58 seconds (average 0.003250, setup 0.32)

Value in the initial state: 0.2931552730489881

Time for model checking: 0.628 seconds.

Result: 0.2931552730489881 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"charge"}min=? [ F "deadlock" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=2,Objy1=1,Objx2=4,Objy2=0

Prob0A: 4 iterations in 0.03 seconds (average 0.007000, setup 0.00)

Prob1E: 5 iterations in 0.05 seconds (average 0.009600, setup 0.00)

Warning: PRISM hasn't checked for zero-reward loops.
Your minimum rewards may be too low...

goal = 1, inf = 0, maybe = 224475

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=224476, nc=294781, nnz=589688, k=4] [8.7 MB]
Building sparse matrix (transition rewards)... [n=224476, nc=294781, nnz=6458, k=4] [2.1 MB]
Creating vector for state rewards... [1.7 MB]
Creating vector for inf... [1.7 MB]
Allocating iteration vectors... [2 x 1.7 MB]
TOTAL: [17.6 MB]

Starting iterations...

Iterative method: 88 iterations in 0.62 seconds (average 0.003318, setup 0.32)

Value in the initial state: 0.03227921904211501

Time for model checking: 0.7 seconds.

Result: 0.03227921904211501 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"time"}max=? [ F "deadlock" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=2,Objy1=1,Objx2=4,Objy2=0

Prob0E: 4 iterations in 0.02 seconds (average 0.006000, setup 0.00)

Prob1A: 1 iterations in 0.00 seconds (average 0.004000, setup 0.00)

goal = 1, inf = 0, maybe = 224475

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=224476, nc=294781, nnz=589688, k=4] [8.7 MB]
Building sparse matrix (transition rewards)... [n=224476, nc=294781, nnz=112630, k=4] [3.3 MB]
Creating vector for state rewards... [1.7 MB]
Creating vector for inf... [1.7 MB]
Allocating iteration vectors... [2 x 1.7 MB]
TOTAL: [18.8 MB]

Starting iterations...

Iterative method: 89 iterations in 0.78 seconds (average 0.004674, setup 0.36)

Value in the initial state: 170.47320834281163

Time for model checking: 0.816 seconds.

Result: 170.47320834281163 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"time"}min=? [ F "deadlock" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=2,Objy1=1,Objx2=4,Objy2=0

Prob0A: 4 iterations in 0.03 seconds (average 0.007000, setup 0.00)

Prob1E: 5 iterations in 0.05 seconds (average 0.009600, setup 0.00)

Warning: PRISM hasn't checked for zero-reward loops.
Your minimum rewards may be too low...

goal = 1, inf = 0, maybe = 224475

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=224476, nc=294781, nnz=589688, k=4] [8.7 MB]
Building sparse matrix (transition rewards)... [n=224476, nc=294781, nnz=112630, k=4] [3.3 MB]
Creating vector for state rewards... [1.7 MB]
Creating vector for inf... [1.7 MB]
Allocating iteration vectors... [2 x 1.7 MB]
TOTAL: [18.8 MB]

Starting iterations...

Iterative method: 93 iterations in 0.66 seconds (average 0.003355, setup 0.35)

Value in the initial state: 88.56374207557009

Time for model checking: 0.748 seconds.

Result: 88.56374207557009 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"success"}max=? [ F "deadlock" ]/Pmin=? [ F "MissionSuccessful" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=2,Objy1=1,Objx2=4,Objy2=0

Prob0E: 4 iterations in 0.03 seconds (average 0.007000, setup 0.00)

Prob1A: 1 iterations in 0.01 seconds (average 0.008000, setup 0.00)

goal = 1, inf = 0, maybe = 224475

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=224476, nc=294781, nnz=589688, k=4] [8.7 MB]
Building sparse matrix (transition rewards)... [n=224476, nc=294781, nnz=1876, k=4] [2.0 MB]
Creating vector for state rewards... [1.7 MB]
Creating vector for inf... [1.7 MB]
Allocating iteration vectors... [2 x 1.7 MB]
TOTAL: [17.6 MB]

Starting iterations...

Iterative method: 89 iterations in 0.60 seconds (average 0.003191, setup 0.32)

Prob0E: 48 iterations in 0.35 seconds (average 0.007250, setup 0.00)

Prob1A: 3 iterations in 0.04 seconds (average 0.013333, setup 0.00)

yes = 952, no = 195619, maybe = 27905

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=224476, nc=49073, nnz=140515, k=2] [1.9 MB]
Creating vector for yes... [1.7 MB]
Allocating iteration vectors... [2 x 1.7 MB]
TOTAL: [7.0 MB]

Starting iterations...

Iterative method: 79 iterations in 0.27 seconds (average 0.001418, setup 0.16)

Value in the initial state: 225.3349710618097

Time for model checking: 1.331 seconds.

Result: 225.3349710618097 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"success"}min=? [ F "deadlock" ]/Pmax=? [ F "MissionSuccessful" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=2,Objy1=1,Objx2=4,Objy2=0

Prob0A: 4 iterations in 0.08 seconds (average 0.021000, setup 0.00)

Prob1E: 5 iterations in 0.05 seconds (average 0.010400, setup 0.00)

Warning: PRISM hasn't checked for zero-reward loops.
Your minimum rewards may be too low...

goal = 1, inf = 0, maybe = 224475

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=224476, nc=294781, nnz=589688, k=4] [8.7 MB]
Building sparse matrix (transition rewards)... [n=224476, nc=294781, nnz=1876, k=4] [2.0 MB]
Creating vector for state rewards... [1.7 MB]
Creating vector for inf... [1.7 MB]
Allocating iteration vectors... [2 x 1.7 MB]
TOTAL: [17.6 MB]

Starting iterations...

Iterative method: 85 iterations in 0.66 seconds (average 0.003247, setup 0.38)

Prob0A: 46 iterations in 0.21 seconds (average 0.004522, setup 0.00)

Prob1E: 4 iterations in 0.03 seconds (average 0.007000, setup 0.00)

yes = 952, no = 190345, maybe = 33179

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=224476, nc=58557, nnz=168284, k=2] [2.2 MB]
Creating vector for yes... [1.7 MB]
Allocating iteration vectors... [2 x 1.7 MB]
TOTAL: [7.3 MB]

Starting iterations...

Iterative method: 87 iterations in 0.42 seconds (average 0.002253, setup 0.23)

Value in the initial state: 78.38970354846542

Time for model checking: 1.486 seconds.

Result: 78.38970354846542 (value in the initial state)

---------------------------------------------------------------------

Note: There were 16 warnings during computation.

