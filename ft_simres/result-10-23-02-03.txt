PRISM
=====

Version: 4.3
Date: Tue Aug 02 12:35:49 BST 2016
Hostname: eglinton
Memory limits: cudd=1g, java(heap)=981.5m
Command line: prism uav_sim_ft_mdp.prism -s uav_mdp.props -const 'Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=0,Objy1=2,Objx2=0,Objy2=3,'

Parsing model file "uav_sim_ft_mdp.prism"...

Parsing properties file "uav_mdp.props"...

18 properties:
(1) Pmax=? [ F "MissionSuccessful" ]
(2) Pmin=? [ F "MissionSuccessful" ]
(3) Pmax=? [ F "MissionTimeUp" ]
(4) Pmin=? [ F "MissionTimeUp" ]
(5) Pmax=? [ F "Crash" ]
(6) Pmin=? [ F "Crash" ]
(7) Pmax=? [ F "AllAreaSearched" ]
(8) Pmin=? [ F "AllAreaSearched" ]
(9) Pmax=? [ F "MissionTimeUp"|"AllAreaSearched"|"Crash" ]
(10) Pmin=? [ F "MissionTimeUp"|"AllAreaSearched"|"Crash" ]
(11) R{"drop"}max=? [ F "deadlock" ]
(12) R{"drop"}min=? [ F "deadlock" ]
(13) R{"charge"}max=? [ F "deadlock" ]
(14) R{"charge"}min=? [ F "deadlock" ]
(15) R{"time"}max=? [ F "deadlock" ]
(16) R{"time"}min=? [ F "deadlock" ]
(17) R{"success"}max=? [ F "deadlock" ]/Pmin=? [ F "MissionSuccessful" ]
(18) R{"success"}min=? [ F "deadlock" ]/Pmax=? [ F "MissionSuccessful" ]

Type:        MDP
Modules:     Obj Move Robot T 
Variables:   NoOfObjs posx posy s1 retx rety b t c 

---------------------------------------------------------------------

Model checking: Pmax=? [ F "MissionSuccessful" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=0,Objy1=2,Objx2=0,Objy2=3

Building model...
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=0,Objy1=2,Objx2=0,Objy2=3

Warning: Guard for command 7 of module "Move" is never satisfied.

Warning: Guard for command 8 of module "Move" is never satisfied.

Warning: Guard for command 9 of module "Move" is never satisfied.

Warning: Guard for command 6 of module "Robot" is never satisfied.

Warning: Update 2 of command 3 of module "Robot" doesn't do anything ("(s1'=13)", line 249, column 44)

Warning: Update 2 of command 27 of module "T" doesn't do anything ("(c'=1)", line 372, column 108)

Warning: Guard for command 28 of module "T" is never satisfied.

Warning: Update 2 of command 9 of module "T" doesn't do anything ("(c'=1)", line 349, column 112)

Warning: Guard for command 10 of module "T" is never satisfied.

Warning: Update 2 of command 51 of module "T" doesn't do anything ("(c'=1)", line 403, column 113)

Warning: Guard for command 52 of module "T" is never satisfied.

Computing reachable states...

Reachability (BFS): 78 iterations in 0.73 seconds (average 0.009333, setup 0.00)

Time for model construction: 2.726 seconds.

Warning: Deadlocks detected and fixed in 1 states

Type:        MDP
States:      161508 (1 initial)
Transitions: 447369
Choices:     217690

Transition matrix: 176490 nodes (76 terminal), 447369 minterms, vars: 34r/34c/18nd

Prob0A: 56 iterations in 0.47 seconds (average 0.008357, setup 0.00)

Prob1E: 4 iterations in 0.06 seconds (average 0.016000, setup 0.00)

yes = 769, no = 127079, maybe = 33660

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=161508, nc=58382, nnz=163897, k=2] [2.1 MB]
Creating vector for yes... [1.2 MB]
Allocating iteration vectors... [2 x 1.2 MB]
TOTAL: [5.8 MB]

Starting iterations...

Iterative method: 95 iterations in 0.36 seconds (average 0.001221, setup 0.24)

Value in the initial state: 0.7387433791889775

Time for model checking: 1.22 seconds.

Result: 0.7387433791889775 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmin=? [ F "MissionSuccessful" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=0,Objy1=2,Objx2=0,Objy2=3

Prob0E: 72 iterations in 0.62 seconds (average 0.008611, setup 0.00)

Prob1A: 3 iterations in 0.06 seconds (average 0.020000, setup 0.00)

yes = 769, no = 134223, maybe = 26516

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=161508, nc=45491, nnz=126190, k=2] [1.6 MB]
Creating vector for yes... [1.2 MB]
Allocating iteration vectors... [2 x 1.2 MB]
TOTAL: [5.3 MB]

Starting iterations...

Iterative method: 93 iterations in 0.29 seconds (average 0.001075, setup 0.19)

Value in the initial state: 0.004871513227818975

Time for model checking: 1.038 seconds.

Result: 0.004871513227818975 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmax=? [ F "MissionTimeUp" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=0,Objy1=2,Objx2=0,Objy2=3

Prob0A: 65 iterations in 0.50 seconds (average 0.007754, setup 0.00)

Prob1E: 4 iterations in 0.04 seconds (average 0.010000, setup 0.00)

yes = 256, no = 133756, maybe = 27496

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=161508, nc=48359, nnz=134178, k=4] [1.7 MB]
Creating vector for yes... [1.2 MB]
Allocating iteration vectors... [2 x 1.2 MB]
TOTAL: [5.4 MB]

Starting iterations...

Iterative method: 87 iterations in 0.24 seconds (average 0.001057, setup 0.14)

Value in the initial state: 3.0468860838926276E-5

Time for model checking: 0.94 seconds.

Result: 3.0468860838926276E-5 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmin=? [ F "MissionTimeUp" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=0,Objy1=2,Objx2=0,Objy2=3

Prob0E: 72 iterations in 0.39 seconds (average 0.005444, setup 0.00)

Prob1A: 3 iterations in 0.02 seconds (average 0.008000, setup 0.00)

yes = 256, no = 133989, maybe = 27263

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=161508, nc=47945, nnz=133253, k=4] [1.7 MB]
Creating vector for yes... [1.2 MB]
Allocating iteration vectors... [2 x 1.2 MB]
TOTAL: [5.4 MB]

Starting iterations...

Iterative method: 105 iterations in 0.19 seconds (average 0.001105, setup 0.08)

Value in the initial state: 5.650303077564997E-8

Time for model checking: 0.804 seconds.

Result: 5.650303077564997E-8 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmax=? [ F "Crash" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=0,Objy1=2,Objx2=0,Objy2=3

Prob0A: 3 iterations in 0.08 seconds (average 0.026667, setup 0.00)

Prob1E: 200 iterations in 2.51 seconds (average 0.012560, setup 0.00)

yes = 78985, no = 1246, maybe = 81277

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=161508, nc=137459, nnz=367138, k=4] [4.5 MB]
Creating vector for yes... [1.2 MB]
Allocating iteration vectors... [2 x 1.2 MB]
TOTAL: [8.2 MB]

Starting iterations...

Iterative method: 89 iterations in 0.42 seconds (average 0.001573, setup 0.28)

Value in the initial state: 0.020022150464377258

Time for model checking: 3.699 seconds.

Result: 0.020022150464377258 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmin=? [ F "Crash" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=0,Objy1=2,Objx2=0,Objy2=3

Prob0E: 3 iterations in 0.06 seconds (average 0.021333, setup 0.00)

Prob1A: 50 iterations in 0.55 seconds (average 0.011040, setup 0.00)

yes = 78985, no = 1246, maybe = 81277

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=161508, nc=137459, nnz=367138, k=4] [4.5 MB]
Creating vector for yes... [1.2 MB]
Allocating iteration vectors... [2 x 1.2 MB]
TOTAL: [8.2 MB]

Starting iterations...

Iterative method: 91 iterations in 0.39 seconds (average 0.001582, setup 0.25)

Value in the initial state: 0.016790693658751986

Time for model checking: 1.269 seconds.

Result: 0.016790693658751986 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmax=? [ F "AllAreaSearched" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=0,Objy1=2,Objx2=0,Objy2=3

Prob0A: 50 iterations in 0.40 seconds (average 0.008000, setup 0.00)

Prob1E: 4 iterations in 0.04 seconds (average 0.010000, setup 0.00)

yes = 220, no = 99626, maybe = 61662

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=161508, nc=102947, nnz=282594, k=2] [3.5 MB]
Creating vector for yes... [1.2 MB]
Allocating iteration vectors... [2 x 1.2 MB]
TOTAL: [7.2 MB]

Starting iterations...

Iterative method: 90 iterations in 0.35 seconds (average 0.001422, setup 0.22)

Value in the initial state: 0.97735726084708

Time for model checking: 1.02 seconds.

Result: 0.97735726084708 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmin=? [ F "AllAreaSearched" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=0,Objy1=2,Objx2=0,Objy2=3

Prob0E: 56 iterations in 0.53 seconds (average 0.009500, setup 0.00)

Prob1A: 3 iterations in 0.04 seconds (average 0.013333, setup 0.00)

yes = 220, no = 101445, maybe = 59843

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=161508, nc=99517, nnz=272371, k=2] [3.4 MB]
Creating vector for yes... [1.2 MB]
Allocating iteration vectors... [2 x 1.2 MB]
TOTAL: [7.1 MB]

Starting iterations...

Iterative method: 96 iterations in 0.34 seconds (average 0.001375, setup 0.21)

Value in the initial state: 0.24255981967286458

Time for model checking: 1.251 seconds.

Result: 0.24255981967286458 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmax=? [ F "MissionTimeUp"|"AllAreaSearched"|"Crash" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=0,Objy1=2,Objx2=0,Objy2=3

Prob0A: 3 iterations in 0.08 seconds (average 0.026667, setup 0.00)

Prob1E: 280 iterations in 3.53 seconds (average 0.012600, setup 0.00)

yes = 134222, no = 770, maybe = 26516

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=161508, nc=45491, nnz=126190, k=2] [1.6 MB]
Creating vector for yes... [1.2 MB]
Allocating iteration vectors... [2 x 1.2 MB]
TOTAL: [5.3 MB]

Starting iterations...

Iterative method: 83 iterations in 0.28 seconds (average 0.001108, setup 0.18)

Value in the initial state: 0.9951269490620173

Time for model checking: 4.132 seconds.

Result: 0.9951269490620173 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmin=? [ F "MissionTimeUp"|"AllAreaSearched"|"Crash" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=0,Objy1=2,Objx2=0,Objy2=3

Prob0E: 3 iterations in 0.06 seconds (average 0.018667, setup 0.00)

Prob1A: 56 iterations in 0.41 seconds (average 0.007286, setup 0.00)

yes = 127078, no = 770, maybe = 33660

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=161508, nc=58382, nnz=163897, k=2] [2.1 MB]
Creating vector for yes... [1.2 MB]
Allocating iteration vectors... [2 x 1.2 MB]
TOTAL: [5.8 MB]

Starting iterations...

Iterative method: 87 iterations in 0.26 seconds (average 0.001195, setup 0.16)

Value in the initial state: 0.26125610888702877

Time for model checking: 0.785 seconds.

Result: 0.26125610888702877 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"drop"}max=? [ F "deadlock" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=0,Objy1=2,Objx2=0,Objy2=3

Prob0E: 4 iterations in 0.04 seconds (average 0.010000, setup 0.00)

Prob1A: 1 iterations in 0.00 seconds (average 0.004000, setup 0.00)

goal = 1, inf = 0, maybe = 161507

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=161508, nc=217689, nnz=447368, k=4] [6.6 MB]
Building sparse matrix (transition rewards)... [n=161508, nc=217689, nnz=0, k=4] [1.4 MB]
Creating vector for state rewards... [1.2 MB]
Creating vector for inf... [1.2 MB]
Allocating iteration vectors... [2 x 1.2 MB]
TOTAL: [12.9 MB]

Starting iterations...

Iterative method: 93 iterations in 0.56 seconds (average 0.002409, setup 0.33)

Value in the initial state: 0.15968839100852933

Time for model checking: 0.641 seconds.

Result: 0.15968839100852933 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"drop"}min=? [ F "deadlock" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=0,Objy1=2,Objx2=0,Objy2=3

Prob0A: 4 iterations in 0.03 seconds (average 0.008000, setup 0.00)

Prob1E: 5 iterations in 0.04 seconds (average 0.008800, setup 0.00)

Warning: PRISM hasn't checked for zero-reward loops.
Your minimum rewards may be too low...

goal = 1, inf = 0, maybe = 161507

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=161508, nc=217689, nnz=447368, k=4] [6.6 MB]
Building sparse matrix (transition rewards)... [n=161508, nc=217689, nnz=0, k=4] [1.4 MB]
Creating vector for state rewards... [1.2 MB]
Creating vector for inf... [1.2 MB]
Allocating iteration vectors... [2 x 1.2 MB]
TOTAL: [12.9 MB]

Starting iterations...

Iterative method: 87 iterations in 0.46 seconds (average 0.002391, setup 0.25)

Value in the initial state: 0.14244195403091253

Time for model checking: 0.579 seconds.

Result: 0.14244195403091253 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"charge"}max=? [ F "deadlock" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=0,Objy1=2,Objx2=0,Objy2=3

Prob0E: 4 iterations in 0.02 seconds (average 0.006000, setup 0.00)

Prob1A: 1 iterations in 0.00 seconds (average 0.004000, setup 0.00)

goal = 1, inf = 0, maybe = 161507

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=161508, nc=217689, nnz=447368, k=4] [6.6 MB]
Building sparse matrix (transition rewards)... [n=161508, nc=217689, nnz=5676, k=4] [1.5 MB]
Creating vector for state rewards... [1.2 MB]
Creating vector for inf... [1.2 MB]
Allocating iteration vectors... [2 x 1.2 MB]
TOTAL: [13.0 MB]

Starting iterations...

Iterative method: 89 iterations in 0.46 seconds (average 0.002382, setup 0.25)

Value in the initial state: 0.9812157110707211

Time for model checking: 0.558 seconds.

Result: 0.9812157110707211 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"charge"}min=? [ F "deadlock" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=0,Objy1=2,Objx2=0,Objy2=3

Prob0A: 4 iterations in 0.03 seconds (average 0.007000, setup 0.00)

Prob1E: 5 iterations in 0.04 seconds (average 0.008800, setup 0.00)

Warning: PRISM hasn't checked for zero-reward loops.
Your minimum rewards may be too low...

goal = 1, inf = 0, maybe = 161507

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=161508, nc=217689, nnz=447368, k=4] [6.6 MB]
Building sparse matrix (transition rewards)... [n=161508, nc=217689, nnz=5676, k=4] [1.5 MB]
Creating vector for state rewards... [1.2 MB]
Creating vector for inf... [1.2 MB]
Allocating iteration vectors... [2 x 1.2 MB]
TOTAL: [13.0 MB]

Starting iterations...

Iterative method: 95 iterations in 0.48 seconds (average 0.002400, setup 0.25)

Value in the initial state: 0.22226251105965286

Time for model checking: 0.574 seconds.

Result: 0.22226251105965286 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"time"}max=? [ F "deadlock" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=0,Objy1=2,Objx2=0,Objy2=3

Prob0E: 4 iterations in 0.02 seconds (average 0.006000, setup 0.00)

Prob1A: 1 iterations in 0.01 seconds (average 0.008000, setup 0.00)

goal = 1, inf = 0, maybe = 161507

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=161508, nc=217689, nnz=447368, k=4] [6.6 MB]
Building sparse matrix (transition rewards)... [n=161508, nc=217689, nnz=80229, k=4] [2.4 MB]
Creating vector for state rewards... [1.2 MB]
Creating vector for inf... [1.2 MB]
Allocating iteration vectors... [2 x 1.2 MB]
TOTAL: [13.9 MB]

Starting iterations...

Iterative method: 91 iterations in 0.50 seconds (average 0.002418, setup 0.28)

Value in the initial state: 111.07244384996716

Time for model checking: 0.593 seconds.

Result: 111.07244384996716 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"time"}min=? [ F "deadlock" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=0,Objy1=2,Objx2=0,Objy2=3

Prob0A: 4 iterations in 0.02 seconds (average 0.006000, setup 0.00)

Prob1E: 5 iterations in 0.04 seconds (average 0.008000, setup 0.00)

Warning: PRISM hasn't checked for zero-reward loops.
Your minimum rewards may be too low...

goal = 1, inf = 0, maybe = 161507

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=161508, nc=217689, nnz=447368, k=4] [6.6 MB]
Building sparse matrix (transition rewards)... [n=161508, nc=217689, nnz=80229, k=4] [2.4 MB]
Creating vector for state rewards... [1.2 MB]
Creating vector for inf... [1.2 MB]
Allocating iteration vectors... [2 x 1.2 MB]
TOTAL: [13.9 MB]

Starting iterations...

Iterative method: 101 iterations in 0.52 seconds (average 0.002495, setup 0.27)

Value in the initial state: 93.23064740403468

Time for model checking: 0.611 seconds.

Result: 93.23064740403468 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"success"}max=? [ F "deadlock" ]/Pmin=? [ F "MissionSuccessful" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=0,Objy1=2,Objx2=0,Objy2=3

Prob0E: 4 iterations in 0.03 seconds (average 0.007000, setup 0.00)

Prob1A: 1 iterations in 0.00 seconds (average 0.004000, setup 0.00)

goal = 1, inf = 0, maybe = 161507

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=161508, nc=217689, nnz=447368, k=4] [6.6 MB]
Building sparse matrix (transition rewards)... [n=161508, nc=217689, nnz=1521, k=4] [1.5 MB]
Creating vector for state rewards... [1.2 MB]
Creating vector for inf... [1.2 MB]
Allocating iteration vectors... [2 x 1.2 MB]
TOTAL: [13.0 MB]

Starting iterations...

Iterative method: 95 iterations in 0.48 seconds (average 0.002400, setup 0.26)

Prob0E: 72 iterations in 0.44 seconds (average 0.006056, setup 0.00)

Prob1A: 3 iterations in 0.05 seconds (average 0.017333, setup 0.00)

yes = 769, no = 134223, maybe = 26516

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=161508, nc=45491, nnz=126190, k=2] [1.6 MB]
Creating vector for yes... [1.2 MB]
Allocating iteration vectors... [2 x 1.2 MB]
TOTAL: [5.3 MB]

Starting iterations...

Iterative method: 93 iterations in 0.21 seconds (average 0.001075, setup 0.11)

Value in the initial state: 17373.026455140214

Time for model checking: 1.249 seconds.

Result: 17373.026455140214 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"success"}min=? [ F "deadlock" ]/Pmax=? [ F "MissionSuccessful" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=0,Objy1=2,Objx2=0,Objy2=3

Prob0A: 4 iterations in 0.04 seconds (average 0.010000, setup 0.00)

Prob1E: 5 iterations in 0.04 seconds (average 0.008800, setup 0.00)

Warning: PRISM hasn't checked for zero-reward loops.
Your minimum rewards may be too low...

goal = 1, inf = 0, maybe = 161507

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=161508, nc=217689, nnz=447368, k=4] [6.6 MB]
Building sparse matrix (transition rewards)... [n=161508, nc=217689, nnz=1521, k=4] [1.5 MB]
Creating vector for state rewards... [1.2 MB]
Creating vector for inf... [1.2 MB]
Allocating iteration vectors... [2 x 1.2 MB]
TOTAL: [13.0 MB]

Starting iterations...

Iterative method: 98 iterations in 0.50 seconds (average 0.002408, setup 0.26)

Prob0A: 56 iterations in 0.27 seconds (average 0.004857, setup 0.00)

Prob1E: 4 iterations in 0.03 seconds (average 0.007000, setup 0.00)

yes = 769, no = 127079, maybe = 33660

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=161508, nc=58382, nnz=163897, k=2] [2.1 MB]
Creating vector for yes... [1.2 MB]
Allocating iteration vectors... [2 x 1.2 MB]
TOTAL: [5.8 MB]

Starting iterations...

Iterative method: 95 iterations in 0.25 seconds (average 0.001179, setup 0.14)

Value in the initial state: 2.29997406865627

Time for model checking: 1.154 seconds.

Result: 2.29997406865627 (value in the initial state)

---------------------------------------------------------------------

Note: There were 16 warnings during computation.

