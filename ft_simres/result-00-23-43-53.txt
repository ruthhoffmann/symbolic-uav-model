PRISM
=====

Version: 4.3
Date: Tue Aug 02 12:31:33 BST 2016
Hostname: eglinton
Memory limits: cudd=1g, java(heap)=981.5m
Command line: prism uav_sim_ft_mdp.prism -s uav_mdp.props -const 'Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=4,Objy1=3,Objx2=5,Objy2=3,'

Parsing model file "uav_sim_ft_mdp.prism"...

Parsing properties file "uav_mdp.props"...

18 properties:
(1) Pmax=? [ F "MissionSuccessful" ]
(2) Pmin=? [ F "MissionSuccessful" ]
(3) Pmax=? [ F "MissionTimeUp" ]
(4) Pmin=? [ F "MissionTimeUp" ]
(5) Pmax=? [ F "Crash" ]
(6) Pmin=? [ F "Crash" ]
(7) Pmax=? [ F "AllAreaSearched" ]
(8) Pmin=? [ F "AllAreaSearched" ]
(9) Pmax=? [ F "MissionTimeUp"|"AllAreaSearched"|"Crash" ]
(10) Pmin=? [ F "MissionTimeUp"|"AllAreaSearched"|"Crash" ]
(11) R{"drop"}max=? [ F "deadlock" ]
(12) R{"drop"}min=? [ F "deadlock" ]
(13) R{"charge"}max=? [ F "deadlock" ]
(14) R{"charge"}min=? [ F "deadlock" ]
(15) R{"time"}max=? [ F "deadlock" ]
(16) R{"time"}min=? [ F "deadlock" ]
(17) R{"success"}max=? [ F "deadlock" ]/Pmin=? [ F "MissionSuccessful" ]
(18) R{"success"}min=? [ F "deadlock" ]/Pmax=? [ F "MissionSuccessful" ]

Type:        MDP
Modules:     Obj Move Robot T 
Variables:   NoOfObjs posx posy s1 retx rety b t c 

---------------------------------------------------------------------

Model checking: Pmax=? [ F "MissionSuccessful" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=4,Objy1=3,Objx2=5,Objy2=3

Building model...
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=4,Objy1=3,Objx2=5,Objy2=3

Warning: Guard for command 7 of module "Move" is never satisfied.

Warning: Guard for command 8 of module "Move" is never satisfied.

Warning: Guard for command 9 of module "Move" is never satisfied.

Warning: Guard for command 7 of module "Robot" is never satisfied.

Warning: Update 2 of command 3 of module "Robot" doesn't do anything ("(s1'=13)", line 249, column 44)

Warning: Update 2 of command 27 of module "T" doesn't do anything ("(c'=1)", line 372, column 108)

Warning: Guard for command 28 of module "T" is never satisfied.

Warning: Update 2 of command 9 of module "T" doesn't do anything ("(c'=1)", line 349, column 112)

Warning: Guard for command 10 of module "T" is never satisfied.

Warning: Update 2 of command 51 of module "T" doesn't do anything ("(c'=1)", line 403, column 113)

Warning: Guard for command 52 of module "T" is never satisfied.

Computing reachable states...

Reachability (BFS): 81 iterations in 0.51 seconds (average 0.006321, setup 0.00)

Time for model construction: 1.407 seconds.

Warning: Deadlocks detected and fixed in 1 states

Type:        MDP
States:      165460 (1 initial)
Transitions: 485150
Choices:     229553

Transition matrix: 155403 nodes (76 terminal), 485150 minterms, vars: 34r/34c/18nd

Prob0A: 53 iterations in 0.26 seconds (average 0.004981, setup 0.00)

Prob1E: 4 iterations in 0.04 seconds (average 0.010000, setup 0.00)

yes = 931, no = 133914, maybe = 30615

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=165460, nc=55810, nnz=163502, k=2] [2.1 MB]
Creating vector for yes... [1.3 MB]
Allocating iteration vectors... [2 x 1.3 MB]
TOTAL: [5.9 MB]

Starting iterations...

Iterative method: 97 iterations in 0.29 seconds (average 0.001155, setup 0.18)

Value in the initial state: 0.9778094531503158

Time for model checking: 0.614 seconds.

Result: 0.9778094531503158 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmin=? [ F "MissionSuccessful" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=4,Objy1=3,Objx2=5,Objy2=3

Prob0E: 66 iterations in 0.36 seconds (average 0.005455, setup 0.00)

Prob1A: 3 iterations in 0.04 seconds (average 0.013333, setup 0.00)

yes = 931, no = 138772, maybe = 25757

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=165460, nc=46708, nnz=136541, k=2] [1.8 MB]
Creating vector for yes... [1.3 MB]
Allocating iteration vectors... [2 x 1.3 MB]
TOTAL: [5.6 MB]

Starting iterations...

Iterative method: 91 iterations in 0.26 seconds (average 0.001099, setup 0.16)

Value in the initial state: 0.7478180321624229

Time for model checking: 0.67 seconds.

Result: 0.7478180321624229 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmax=? [ F "MissionTimeUp" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=4,Objy1=3,Objx2=5,Objy2=3

Prob0A: 71 iterations in 0.35 seconds (average 0.004901, setup 0.00)

Prob1E: 4 iterations in 0.03 seconds (average 0.007000, setup 0.00)

yes = 172, no = 98635, maybe = 66653

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=165460, nc=120963, nnz=348717, k=4] [4.3 MB]
Creating vector for yes... [1.3 MB]
Allocating iteration vectors... [2 x 1.3 MB]
TOTAL: [8.1 MB]

Starting iterations...

Iterative method: 94 iterations in 0.35 seconds (average 0.001489, setup 0.21)

Value in the initial state: 0.0018964208321514261

Time for model checking: 0.725 seconds.

Result: 0.0018964208321514261 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmin=? [ F "MissionTimeUp" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=4,Objy1=3,Objx2=5,Objy2=3

Prob0E: 78 iterations in 0.38 seconds (average 0.004872, setup 0.00)

Prob1A: 3 iterations in 0.03 seconds (average 0.009333, setup 0.00)

yes = 172, no = 98992, maybe = 66296

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=165460, nc=120325, nnz=347408, k=4] [4.2 MB]
Creating vector for yes... [1.3 MB]
Allocating iteration vectors... [2 x 1.3 MB]
TOTAL: [8.0 MB]

Starting iterations...

Iterative method: 107 iterations in 0.38 seconds (average 0.001495, setup 0.22)

Value in the initial state: 9.551077328772053E-6

Time for model checking: 0.798 seconds.

Result: 9.551077328772053E-6 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmax=? [ F "Crash" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=4,Objy1=3,Objx2=5,Objy2=3

Prob0A: 3 iterations in 0.06 seconds (average 0.021333, setup 0.00)

Prob1E: 252 iterations in 2.06 seconds (average 0.008175, setup 0.00)

yes = 80795, no = 2380, maybe = 82285

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=165460, nc=146378, nnz=401975, k=4] [4.9 MB]
Creating vector for yes... [1.3 MB]
Allocating iteration vectors... [2 x 1.3 MB]
TOTAL: [8.7 MB]

Starting iterations...

Iterative method: 92 iterations in 0.39 seconds (average 0.001565, setup 0.24)

Value in the initial state: 0.0628637536283047

Time for model checking: 2.54 seconds.

Result: 0.0628637536283047 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmin=? [ F "Crash" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=4,Objy1=3,Objx2=5,Objy2=3

Prob0E: 3 iterations in 0.03 seconds (average 0.010667, setup 0.00)

Prob1A: 53 iterations in 0.25 seconds (average 0.004755, setup 0.00)

yes = 80795, no = 2380, maybe = 82285

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=165460, nc=146378, nnz=401975, k=4] [4.9 MB]
Creating vector for yes... [1.3 MB]
Allocating iteration vectors... [2 x 1.3 MB]
TOTAL: [8.7 MB]

Starting iterations...

Iterative method: 97 iterations in 0.34 seconds (average 0.001649, setup 0.18)

Value in the initial state: 0.020179873901551442

Time for model checking: 0.625 seconds.

Result: 0.020179873901551442 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmax=? [ F "AllAreaSearched" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=4,Objy1=3,Objx2=5,Objy2=3

Prob0A: 56 iterations in 0.24 seconds (average 0.004357, setup 0.00)

Prob1E: 4 iterations in 0.03 seconds (average 0.007000, setup 0.00)

yes = 1276, no = 124764, maybe = 39420

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=165460, nc=67910, nnz=191086, k=2] [2.4 MB]
Creating vector for yes... [1.3 MB]
Allocating iteration vectors... [2 x 1.3 MB]
TOTAL: [6.2 MB]

Starting iterations...

Iterative method: 90 iterations in 0.25 seconds (average 0.001200, setup 0.14)

Value in the initial state: 0.22363248326693122

Time for model checking: 0.54 seconds.

Result: 0.22363248326693122 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmin=? [ F "AllAreaSearched" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=4,Objy1=3,Objx2=5,Objy2=3

Prob0E: 69 iterations in 0.27 seconds (average 0.003942, setup 0.00)

Prob1A: 3 iterations in 0.02 seconds (average 0.006667, setup 0.00)

yes = 1276, no = 131837, maybe = 32347

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=165460, nc=54786, nnz=151129, k=2] [1.9 MB]
Creating vector for yes... [1.3 MB]
Allocating iteration vectors... [2 x 1.3 MB]
TOTAL: [5.7 MB]

Starting iterations...

Iterative method: 90 iterations in 0.20 seconds (average 0.001200, setup 0.10)

Value in the initial state: 0.0012571263127669021

Time for model checking: 0.504 seconds.

Result: 0.0012571263127669021 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmax=? [ F "MissionTimeUp"|"AllAreaSearched"|"Crash" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=4,Objy1=3,Objx2=5,Objy2=3

Prob0A: 3 iterations in 0.07 seconds (average 0.024000, setup 0.00)

Prob1E: 256 iterations in 2.02 seconds (average 0.007891, setup 0.00)

yes = 138771, no = 932, maybe = 25757

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=165460, nc=46708, nnz=136541, k=2] [1.8 MB]
Creating vector for yes... [1.3 MB]
Allocating iteration vectors... [2 x 1.3 MB]
TOTAL: [5.6 MB]

Starting iterations...

Iterative method: 86 iterations in 0.24 seconds (average 0.001070, setup 0.14)

Value in the initial state: 0.2521817296163419

Time for model checking: 2.323 seconds.

Result: 0.2521817296163419 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmin=? [ F "MissionTimeUp"|"AllAreaSearched"|"Crash" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=4,Objy1=3,Objx2=5,Objy2=3

Prob0E: 3 iterations in 0.03 seconds (average 0.010667, setup 0.00)

Prob1A: 53 iterations in 0.24 seconds (average 0.004453, setup 0.00)

yes = 133913, no = 932, maybe = 30615

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=165460, nc=55810, nnz=163502, k=2] [2.1 MB]
Creating vector for yes... [1.3 MB]
Allocating iteration vectors... [2 x 1.3 MB]
TOTAL: [5.9 MB]

Starting iterations...

Iterative method: 94 iterations in 0.22 seconds (average 0.001149, setup 0.12)

Value in the initial state: 0.022190104670396726

Time for model checking: 0.509 seconds.

Result: 0.022190104670396726 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"drop"}max=? [ F "deadlock" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=4,Objy1=3,Objx2=5,Objy2=3

Prob0E: 4 iterations in 0.03 seconds (average 0.007000, setup 0.00)

Prob1A: 1 iterations in 0.00 seconds (average 0.000000, setup 0.00)

goal = 1, inf = 0, maybe = 165459

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=165460, nc=229552, nnz=485149, k=4] [7.1 MB]
Building sparse matrix (transition rewards)... [n=165460, nc=229552, nnz=0, k=4] [1.5 MB]
Creating vector for state rewards... [1.3 MB]
Creating vector for inf... [1.3 MB]
Allocating iteration vectors... [2 x 1.3 MB]
TOTAL: [13.6 MB]

Starting iterations...

Iterative method: 93 iterations in 0.52 seconds (average 0.002409, setup 0.30)

Value in the initial state: 0.2573641025236473

Time for model checking: 0.567 seconds.

Result: 0.2573641025236473 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"drop"}min=? [ F "deadlock" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=4,Objy1=3,Objx2=5,Objy2=3

Prob0A: 4 iterations in 0.02 seconds (average 0.004000, setup 0.00)

Prob1E: 5 iterations in 0.03 seconds (average 0.005600, setup 0.00)

Warning: PRISM hasn't checked for zero-reward loops.
Your minimum rewards may be too low...

goal = 1, inf = 0, maybe = 165459

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=165460, nc=229552, nnz=485149, k=4] [7.1 MB]
Building sparse matrix (transition rewards)... [n=165460, nc=229552, nnz=0, k=4] [1.5 MB]
Creating vector for state rewards... [1.3 MB]
Creating vector for inf... [1.3 MB]
Allocating iteration vectors... [2 x 1.3 MB]
TOTAL: [13.6 MB]

Starting iterations...

Iterative method: 84 iterations in 0.45 seconds (average 0.002476, setup 0.24)

Value in the initial state: 0.18392424121370338

Time for model checking: 0.508 seconds.

Result: 0.18392424121370338 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"charge"}max=? [ F "deadlock" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=4,Objy1=3,Objx2=5,Objy2=3

Prob0E: 4 iterations in 0.02 seconds (average 0.004000, setup 0.00)

Prob1A: 1 iterations in 0.00 seconds (average 0.004000, setup 0.00)

goal = 1, inf = 0, maybe = 165459

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=165460, nc=229552, nnz=485149, k=4] [7.1 MB]
Building sparse matrix (transition rewards)... [n=165460, nc=229552, nnz=6348, k=4] [1.6 MB]
Creating vector for state rewards... [1.3 MB]
Creating vector for inf... [1.3 MB]
Allocating iteration vectors... [2 x 1.3 MB]
TOTAL: [13.7 MB]

Starting iterations...

Iterative method: 87 iterations in 0.46 seconds (average 0.002437, setup 0.25)

Value in the initial state: 0.9859580779494486

Time for model checking: 0.489 seconds.

Result: 0.9859580779494486 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"charge"}min=? [ F "deadlock" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=4,Objy1=3,Objx2=5,Objy2=3

Prob0A: 4 iterations in 0.02 seconds (average 0.005000, setup 0.00)

Prob1E: 5 iterations in 0.03 seconds (average 0.005600, setup 0.00)

Warning: PRISM hasn't checked for zero-reward loops.
Your minimum rewards may be too low...

goal = 1, inf = 0, maybe = 165459

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=165460, nc=229552, nnz=485149, k=4] [7.1 MB]
Building sparse matrix (transition rewards)... [n=165460, nc=229552, nnz=6348, k=4] [1.6 MB]
Creating vector for state rewards... [1.3 MB]
Creating vector for inf... [1.3 MB]
Allocating iteration vectors... [2 x 1.3 MB]
TOTAL: [13.7 MB]

Starting iterations...

Iterative method: 97 iterations in 0.49 seconds (average 0.002515, setup 0.25)

Value in the initial state: 0.07015175932036417

Time for model checking: 0.541 seconds.

Result: 0.07015175932036417 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"time"}max=? [ F "deadlock" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=4,Objy1=3,Objx2=5,Objy2=3

Prob0E: 4 iterations in 0.02 seconds (average 0.004000, setup 0.00)

Prob1A: 1 iterations in 0.00 seconds (average 0.004000, setup 0.00)

goal = 1, inf = 0, maybe = 165459

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=165460, nc=229552, nnz=485149, k=4] [7.1 MB]
Building sparse matrix (transition rewards)... [n=165460, nc=229552, nnz=83173, k=4] [2.5 MB]
Creating vector for state rewards... [1.3 MB]
Creating vector for inf... [1.3 MB]
Allocating iteration vectors... [2 x 1.3 MB]
TOTAL: [14.6 MB]

Starting iterations...

Iterative method: 94 iterations in 0.51 seconds (average 0.002511, setup 0.28)

Value in the initial state: 345.28794008015774

Time for model checking: 0.541 seconds.

Result: 345.28794008015774 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"time"}min=? [ F "deadlock" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=4,Objy1=3,Objx2=5,Objy2=3

Prob0A: 4 iterations in 0.02 seconds (average 0.004000, setup 0.00)

Prob1E: 5 iterations in 0.03 seconds (average 0.006400, setup 0.00)

Warning: PRISM hasn't checked for zero-reward loops.
Your minimum rewards may be too low...

goal = 1, inf = 0, maybe = 165459

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=165460, nc=229552, nnz=485149, k=4] [7.1 MB]
Building sparse matrix (transition rewards)... [n=165460, nc=229552, nnz=83173, k=4] [2.5 MB]
Creating vector for state rewards... [1.3 MB]
Creating vector for inf... [1.3 MB]
Allocating iteration vectors... [2 x 1.3 MB]
TOTAL: [14.6 MB]

Starting iterations...

Iterative method: 102 iterations in 0.53 seconds (average 0.002549, setup 0.27)

Value in the initial state: 111.73901957057474

Time for model checking: 0.586 seconds.

Result: 111.73901957057474 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"success"}max=? [ F "deadlock" ]/Pmin=? [ F "MissionSuccessful" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=4,Objy1=3,Objx2=5,Objy2=3

Prob0E: 4 iterations in 0.02 seconds (average 0.005000, setup 0.00)

Prob1A: 1 iterations in 0.00 seconds (average 0.004000, setup 0.00)

goal = 1, inf = 0, maybe = 165459

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=165460, nc=229552, nnz=485149, k=4] [7.1 MB]
Building sparse matrix (transition rewards)... [n=165460, nc=229552, nnz=1837, k=4] [1.5 MB]
Creating vector for state rewards... [1.3 MB]
Creating vector for inf... [1.3 MB]
Allocating iteration vectors... [2 x 1.3 MB]
TOTAL: [13.6 MB]

Starting iterations...

Iterative method: 98 iterations in 0.48 seconds (average 0.002449, setup 0.24)

Prob0E: 66 iterations in 0.30 seconds (average 0.004606, setup 0.00)

Prob1A: 3 iterations in 0.02 seconds (average 0.006667, setup 0.00)

yes = 931, no = 138772, maybe = 25757

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=165460, nc=46708, nnz=136541, k=2] [1.8 MB]
Creating vector for yes... [1.3 MB]
Allocating iteration vectors... [2 x 1.3 MB]
TOTAL: [5.6 MB]

Starting iterations...

Iterative method: 91 iterations in 0.19 seconds (average 0.001099, setup 0.09)

Value in the initial state: 450.73132986161295

Time for model checking: 1.051 seconds.

Result: 450.73132986161295 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"success"}min=? [ F "deadlock" ]/Pmax=? [ F "MissionSuccessful" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=4,Objy1=3,Objx2=5,Objy2=3

Prob0A: 4 iterations in 0.04 seconds (average 0.009000, setup 0.00)

Prob1E: 5 iterations in 0.04 seconds (average 0.007200, setup 0.00)

Warning: PRISM hasn't checked for zero-reward loops.
Your minimum rewards may be too low...

goal = 1, inf = 0, maybe = 165459

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=165460, nc=229552, nnz=485149, k=4] [7.1 MB]
Building sparse matrix (transition rewards)... [n=165460, nc=229552, nnz=1837, k=4] [1.5 MB]
Creating vector for state rewards... [1.3 MB]
Creating vector for inf... [1.3 MB]
Allocating iteration vectors... [2 x 1.3 MB]
TOTAL: [13.6 MB]

Starting iterations...

Iterative method: 95 iterations in 0.49 seconds (average 0.002484, setup 0.25)

Prob0A: 53 iterations in 0.18 seconds (average 0.003321, setup 0.00)

Prob1E: 4 iterations in 0.02 seconds (average 0.005000, setup 0.00)

yes = 931, no = 133914, maybe = 30615

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=165460, nc=55810, nnz=163502, k=2] [2.1 MB]
Creating vector for yes... [1.3 MB]
Allocating iteration vectors... [2 x 1.3 MB]
TOTAL: [5.9 MB]

Starting iterations...

Iterative method: 97 iterations in 0.22 seconds (average 0.001155, setup 0.11)

Value in the initial state: 82.26271132565336

Time for model checking: 0.982 seconds.

Result: 82.26271132565336 (value in the initial state)

---------------------------------------------------------------------

Note: There were 16 warnings during computation.

