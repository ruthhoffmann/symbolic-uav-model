PRISM
=====

Version: 4.3
Date: Tue Aug 02 12:36:20 BST 2016
Hostname: eglinton
Memory limits: cudd=1g, java(heap)=981.5m
Command line: prism uav_sim_ft_mdp.prism -s uav_mdp.props -const 'Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=0,Objy1=3,Objx2=3,Objy2=2,'

Parsing model file "uav_sim_ft_mdp.prism"...

Parsing properties file "uav_mdp.props"...

18 properties:
(1) Pmax=? [ F "MissionSuccessful" ]
(2) Pmin=? [ F "MissionSuccessful" ]
(3) Pmax=? [ F "MissionTimeUp" ]
(4) Pmin=? [ F "MissionTimeUp" ]
(5) Pmax=? [ F "Crash" ]
(6) Pmin=? [ F "Crash" ]
(7) Pmax=? [ F "AllAreaSearched" ]
(8) Pmin=? [ F "AllAreaSearched" ]
(9) Pmax=? [ F "MissionTimeUp"|"AllAreaSearched"|"Crash" ]
(10) Pmin=? [ F "MissionTimeUp"|"AllAreaSearched"|"Crash" ]
(11) R{"drop"}max=? [ F "deadlock" ]
(12) R{"drop"}min=? [ F "deadlock" ]
(13) R{"charge"}max=? [ F "deadlock" ]
(14) R{"charge"}min=? [ F "deadlock" ]
(15) R{"time"}max=? [ F "deadlock" ]
(16) R{"time"}min=? [ F "deadlock" ]
(17) R{"success"}max=? [ F "deadlock" ]/Pmin=? [ F "MissionSuccessful" ]
(18) R{"success"}min=? [ F "deadlock" ]/Pmax=? [ F "MissionSuccessful" ]

Type:        MDP
Modules:     Obj Move Robot T 
Variables:   NoOfObjs posx posy s1 retx rety b t c 

---------------------------------------------------------------------

Model checking: Pmax=? [ F "MissionSuccessful" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=0,Objy1=3,Objx2=3,Objy2=2

Building model...
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=0,Objy1=3,Objx2=3,Objy2=2

Warning: Guard for command 7 of module "Move" is never satisfied.

Warning: Guard for command 8 of module "Move" is never satisfied.

Warning: Guard for command 9 of module "Move" is never satisfied.

Warning: Guard for command 6 of module "Robot" is never satisfied.

Warning: Update 2 of command 3 of module "Robot" doesn't do anything ("(s1'=13)", line 249, column 44)

Warning: Update 2 of command 27 of module "T" doesn't do anything ("(c'=1)", line 372, column 108)

Warning: Guard for command 28 of module "T" is never satisfied.

Warning: Update 2 of command 9 of module "T" doesn't do anything ("(c'=1)", line 349, column 112)

Warning: Guard for command 10 of module "T" is never satisfied.

Warning: Update 2 of command 51 of module "T" doesn't do anything ("(c'=1)", line 403, column 113)

Warning: Guard for command 52 of module "T" is never satisfied.

Computing reachable states...

Reachability (BFS): 78 iterations in 0.63 seconds (average 0.008103, setup 0.00)

Time for model construction: 2.035 seconds.

Warning: Deadlocks detected and fixed in 1 states

Type:        MDP
States:      140698 (1 initial)
Transitions: 391878
Choices:     190810

Transition matrix: 175357 nodes (76 terminal), 391878 minterms, vars: 34r/34c/18nd

Prob0A: 55 iterations in 0.41 seconds (average 0.007491, setup 0.00)

Prob1E: 4 iterations in 0.06 seconds (average 0.015000, setup 0.00)

yes = 782, no = 108511, maybe = 31405

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=140698, nc=54943, nnz=153858, k=2] [1.9 MB]
Creating vector for yes... [1.1 MB]
Allocating iteration vectors... [2 x 1.1 MB]
TOTAL: [5.2 MB]

Starting iterations...

Iterative method: 92 iterations in 0.30 seconds (average 0.001043, setup 0.20)

Value in the initial state: 0.7497045302051196

Time for model checking: 0.8 seconds.

Result: 0.7497045302051196 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmin=? [ F "MissionSuccessful" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=0,Objy1=3,Objx2=3,Objy2=2

Prob0E: 71 iterations in 0.57 seconds (average 0.008000, setup 0.00)

Prob1A: 3 iterations in 0.06 seconds (average 0.020000, setup 0.00)

yes = 782, no = 115002, maybe = 24914

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=140698, nc=43189, nnz=119853, k=2] [1.5 MB]
Creating vector for yes... [1.1 MB]
Allocating iteration vectors... [2 x 1.1 MB]
TOTAL: [4.8 MB]

Starting iterations...

Iterative method: 89 iterations in 0.26 seconds (average 0.000944, setup 0.18)

Value in the initial state: 0.0026237191715753868

Time for model checking: 1.065 seconds.

Result: 0.0026237191715753868 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmax=? [ F "MissionTimeUp" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=0,Objy1=3,Objx2=3,Objy2=2

Prob0A: 67 iterations in 0.43 seconds (average 0.006448, setup 0.00)

Prob1E: 4 iterations in 0.04 seconds (average 0.011000, setup 0.00)

yes = 192, no = 121261, maybe = 19245

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=140698, nc=33835, nnz=92646, k=4] [1.2 MB]
Creating vector for yes... [1.1 MB]
Allocating iteration vectors... [2 x 1.1 MB]
TOTAL: [4.4 MB]

Starting iterations...

Iterative method: 86 iterations in 0.21 seconds (average 0.000884, setup 0.13)

Value in the initial state: 7.623194334941997E-6

Time for model checking: 0.71 seconds.

Result: 7.623194334941997E-6 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmin=? [ F "MissionTimeUp" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=0,Objy1=3,Objx2=3,Objy2=2

Prob0E: 74 iterations in 0.45 seconds (average 0.006054, setup 0.00)

Prob1A: 3 iterations in 0.04 seconds (average 0.012000, setup 0.00)

yes = 192, no = 121475, maybe = 19031

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=140698, nc=33459, nnz=91760, k=4] [1.2 MB]
Creating vector for yes... [1.1 MB]
Allocating iteration vectors... [2 x 1.1 MB]
TOTAL: [4.4 MB]

Starting iterations...

Iterative method: 102 iterations in 0.20 seconds (average 0.000902, setup 0.11)

Value in the initial state: 1.3803022536599693E-8

Time for model checking: 0.71 seconds.

Result: 1.3803022536599693E-8 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmax=? [ F "Crash" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=0,Objy1=3,Objx2=3,Objy2=2

Prob0A: 3 iterations in 0.08 seconds (average 0.026667, setup 0.00)

Prob1E: 196 iterations in 2.36 seconds (average 0.012061, setup 0.00)

yes = 68824, no = 1195, maybe = 70679

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=140698, nc=120791, nnz=321859, k=4] [3.9 MB]
Creating vector for yes... [1.1 MB]
Allocating iteration vectors... [2 x 1.1 MB]
TOTAL: [7.2 MB]

Starting iterations...

Iterative method: 87 iterations in 0.40 seconds (average 0.001379, setup 0.28)

Value in the initial state: 0.019591183243531304

Time for model checking: 3.257 seconds.

Result: 0.019591183243531304 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmin=? [ F "Crash" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=0,Objy1=3,Objx2=3,Objy2=2

Prob0E: 3 iterations in 0.07 seconds (average 0.024000, setup 0.00)

Prob1A: 49 iterations in 0.49 seconds (average 0.009959, setup 0.00)

yes = 68824, no = 1195, maybe = 70679

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=140698, nc=120791, nnz=321859, k=4] [3.9 MB]
Creating vector for yes... [1.1 MB]
Allocating iteration vectors... [2 x 1.1 MB]
TOTAL: [7.2 MB]

Starting iterations...

Iterative method: 87 iterations in 0.28 seconds (average 0.001425, setup 0.16)

Value in the initial state: 0.016531378363508428

Time for model checking: 1.002 seconds.

Result: 0.016531378363508428 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmax=? [ F "AllAreaSearched" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=0,Objy1=3,Objx2=3,Objy2=2

Prob0A: 49 iterations in 0.51 seconds (average 0.010367, setup 0.00)

Prob1E: 4 iterations in 0.04 seconds (average 0.010000, setup 0.00)

yes = 220, no = 83799, maybe = 56679

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=140698, nc=96031, nnz=263684, k=2] [3.2 MB]
Creating vector for yes... [1.1 MB]
Allocating iteration vectors... [2 x 1.1 MB]
TOTAL: [6.5 MB]

Starting iterations...

Iterative method: 88 iterations in 0.33 seconds (average 0.001273, setup 0.22)

Value in the initial state: 0.9797188713619002

Time for model checking: 1.305 seconds.

Result: 0.9797188713619002 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmin=? [ F "AllAreaSearched" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=0,Objy1=3,Objx2=3,Objy2=2

Prob0E: 52 iterations in 0.41 seconds (average 0.007846, setup 0.00)

Prob1A: 3 iterations in 0.03 seconds (average 0.010667, setup 0.00)

yes = 220, no = 85085, maybe = 55393

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=140698, nc=93628, nnz=256620, k=2] [3.2 MB]
Creating vector for yes... [1.1 MB]
Allocating iteration vectors... [2 x 1.1 MB]
TOTAL: [6.4 MB]

Starting iterations...

Iterative method: 95 iterations in 0.30 seconds (average 0.001263, setup 0.18)

Value in the initial state: 0.23200206989823074

Time for model checking: 0.772 seconds.

Result: 0.23200206989823074 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmax=? [ F "MissionTimeUp"|"AllAreaSearched"|"Crash" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=0,Objy1=3,Objx2=3,Objy2=2

Prob0A: 3 iterations in 0.07 seconds (average 0.024000, setup 0.00)

Prob1E: 276 iterations in 3.30 seconds (average 0.011971, setup 0.00)

yes = 115001, no = 783, maybe = 24914

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=140698, nc=43189, nnz=119853, k=2] [1.5 MB]
Creating vector for yes... [1.1 MB]
Allocating iteration vectors... [2 x 1.1 MB]
TOTAL: [4.8 MB]

Starting iterations...

Iterative method: 80 iterations in 0.25 seconds (average 0.000900, setup 0.18)

Value in the initial state: 0.9973749681120935

Time for model checking: 3.839 seconds.

Result: 0.9973749681120935 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmin=? [ F "MissionTimeUp"|"AllAreaSearched"|"Crash" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=0,Objy1=3,Objx2=3,Objy2=2

Prob0E: 3 iterations in 0.05 seconds (average 0.016000, setup 0.00)

Prob1A: 55 iterations in 0.44 seconds (average 0.008000, setup 0.00)

yes = 108510, no = 783, maybe = 31405

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=140698, nc=54943, nnz=153858, k=2] [1.9 MB]
Creating vector for yes... [1.1 MB]
Allocating iteration vectors... [2 x 1.1 MB]
TOTAL: [5.2 MB]

Starting iterations...

Iterative method: 85 iterations in 0.23 seconds (average 0.001035, setup 0.14)

Value in the initial state: 0.2502947924630142

Time for model checking: 0.752 seconds.

Result: 0.2502947924630142 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"drop"}max=? [ F "deadlock" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=0,Objy1=3,Objx2=3,Objy2=2

Prob0E: 4 iterations in 0.05 seconds (average 0.013000, setup 0.00)

Prob1A: 1 iterations in 0.00 seconds (average 0.004000, setup 0.00)

goal = 1, inf = 0, maybe = 140697

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=140698, nc=190809, nnz=391877, k=4] [5.7 MB]
Building sparse matrix (transition rewards)... [n=140698, nc=190809, nnz=0, k=4] [1.3 MB]
Creating vector for state rewards... [1.1 MB]
Creating vector for inf... [1.1 MB]
Allocating iteration vectors... [2 x 1.1 MB]
TOTAL: [11.3 MB]

Starting iterations...

Iterative method: 88 iterations in 0.50 seconds (average 0.002091, setup 0.31)

Value in the initial state: 0.13259816453966522

Time for model checking: 0.565 seconds.

Result: 0.13259816453966522 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"drop"}min=? [ F "deadlock" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=0,Objy1=3,Objx2=3,Objy2=2

Prob0A: 4 iterations in 0.03 seconds (average 0.008000, setup 0.00)

Prob1E: 5 iterations in 0.05 seconds (average 0.010400, setup 0.00)

Warning: PRISM hasn't checked for zero-reward loops.
Your minimum rewards may be too low...

goal = 1, inf = 0, maybe = 140697

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=140698, nc=190809, nnz=391877, k=4] [5.7 MB]
Building sparse matrix (transition rewards)... [n=140698, nc=190809, nnz=0, k=4] [1.3 MB]
Creating vector for state rewards... [1.1 MB]
Creating vector for inf... [1.1 MB]
Allocating iteration vectors... [2 x 1.1 MB]
TOTAL: [11.3 MB]

Starting iterations...

Iterative method: 83 iterations in 0.39 seconds (average 0.002072, setup 0.22)

Value in the initial state: 0.11630771116373678

Time for model checking: 0.488 seconds.

Result: 0.11630771116373678 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"charge"}max=? [ F "deadlock" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=0,Objy1=3,Objx2=3,Objy2=2

Prob0E: 4 iterations in 0.03 seconds (average 0.007000, setup 0.00)

Prob1A: 1 iterations in 0.00 seconds (average 0.004000, setup 0.00)

goal = 1, inf = 0, maybe = 140697

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=140698, nc=190809, nnz=391877, k=4] [5.7 MB]
Building sparse matrix (transition rewards)... [n=140698, nc=190809, nnz=5004, k=4] [1.3 MB]
Creating vector for state rewards... [1.1 MB]
Creating vector for inf... [1.1 MB]
Allocating iteration vectors... [2 x 1.1 MB]
TOTAL: [11.4 MB]

Starting iterations...

Iterative method: 85 iterations in 0.40 seconds (average 0.002118, setup 0.22)

Value in the initial state: 0.9830284197328698

Time for model checking: 0.445 seconds.

Result: 0.9830284197328698 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"charge"}min=? [ F "deadlock" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=0,Objy1=3,Objx2=3,Objy2=2

Prob0A: 4 iterations in 0.03 seconds (average 0.007000, setup 0.00)

Prob1E: 5 iterations in 0.04 seconds (average 0.008800, setup 0.00)

Warning: PRISM hasn't checked for zero-reward loops.
Your minimum rewards may be too low...

goal = 1, inf = 0, maybe = 140697

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=140698, nc=190809, nnz=391877, k=4] [5.7 MB]
Building sparse matrix (transition rewards)... [n=140698, nc=190809, nnz=5004, k=4] [1.3 MB]
Creating vector for state rewards... [1.1 MB]
Creating vector for inf... [1.1 MB]
Allocating iteration vectors... [2 x 1.1 MB]
TOTAL: [11.4 MB]

Starting iterations...

Iterative method: 93 iterations in 0.42 seconds (average 0.002108, setup 0.22)

Value in the initial state: 0.2121931304915955

Time for model checking: 0.502 seconds.

Result: 0.2121931304915955 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"time"}max=? [ F "deadlock" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=0,Objy1=3,Objx2=3,Objy2=2

Prob0E: 4 iterations in 0.02 seconds (average 0.006000, setup 0.00)

Prob1A: 1 iterations in 0.00 seconds (average 0.004000, setup 0.00)

goal = 1, inf = 0, maybe = 140697

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=140698, nc=190809, nnz=391877, k=4] [5.7 MB]
Building sparse matrix (transition rewards)... [n=140698, nc=190809, nnz=70017, k=4] [2.1 MB]
Creating vector for state rewards... [1.1 MB]
Creating vector for inf... [1.1 MB]
Allocating iteration vectors... [2 x 1.1 MB]
TOTAL: [12.1 MB]

Starting iterations...

Iterative method: 89 iterations in 0.44 seconds (average 0.002112, setup 0.26)

Value in the initial state: 108.71196666982469

Time for model checking: 0.485 seconds.

Result: 108.71196666982469 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"time"}min=? [ F "deadlock" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=0,Objy1=3,Objx2=3,Objy2=2

Prob0A: 4 iterations in 0.02 seconds (average 0.006000, setup 0.00)

Prob1E: 5 iterations in 0.05 seconds (average 0.009600, setup 0.00)

Warning: PRISM hasn't checked for zero-reward loops.
Your minimum rewards may be too low...

goal = 1, inf = 0, maybe = 140697

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=140698, nc=190809, nnz=391877, k=4] [5.7 MB]
Building sparse matrix (transition rewards)... [n=140698, nc=190809, nnz=70017, k=4] [2.1 MB]
Creating vector for state rewards... [1.1 MB]
Creating vector for inf... [1.1 MB]
Allocating iteration vectors... [2 x 1.1 MB]
TOTAL: [12.1 MB]

Starting iterations...

Iterative method: 98 iterations in 0.46 seconds (average 0.002122, setup 0.25)

Value in the initial state: 91.79868836824889

Time for model checking: 0.544 seconds.

Result: 91.79868836824889 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"success"}max=? [ F "deadlock" ]/Pmin=? [ F "MissionSuccessful" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=0,Objy1=3,Objx2=3,Objy2=2

Prob0E: 4 iterations in 0.02 seconds (average 0.006000, setup 0.00)

Prob1A: 1 iterations in 0.01 seconds (average 0.008000, setup 0.00)

goal = 1, inf = 0, maybe = 140697

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=140698, nc=190809, nnz=391877, k=4] [5.7 MB]
Building sparse matrix (transition rewards)... [n=140698, nc=190809, nnz=1547, k=4] [1.3 MB]
Creating vector for state rewards... [1.1 MB]
Creating vector for inf... [1.1 MB]
Allocating iteration vectors... [2 x 1.1 MB]
TOTAL: [11.3 MB]

Starting iterations...

Iterative method: 95 iterations in 0.43 seconds (average 0.002105, setup 0.23)

Prob0E: 71 iterations in 0.49 seconds (average 0.006930, setup 0.00)

Prob1A: 3 iterations in 0.04 seconds (average 0.012000, setup 0.00)

yes = 782, no = 115002, maybe = 24914

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=140698, nc=43189, nnz=119853, k=2] [1.5 MB]
Creating vector for yes... [1.1 MB]
Allocating iteration vectors... [2 x 1.1 MB]
TOTAL: [4.8 MB]

Starting iterations...

Iterative method: 89 iterations in 0.21 seconds (average 0.000944, setup 0.12)

Value in the initial state: 31857.497233330396

Time for model checking: 1.244 seconds.

Result: 31857.497233330396 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"success"}min=? [ F "deadlock" ]/Pmax=? [ F "MissionSuccessful" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=0,Objy1=3,Objx2=3,Objy2=2

Prob0A: 4 iterations in 0.03 seconds (average 0.008000, setup 0.00)

Prob1E: 5 iterations in 0.05 seconds (average 0.009600, setup 0.00)

Warning: PRISM hasn't checked for zero-reward loops.
Your minimum rewards may be too low...

goal = 1, inf = 0, maybe = 140697

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=140698, nc=190809, nnz=391877, k=4] [5.7 MB]
Building sparse matrix (transition rewards)... [n=140698, nc=190809, nnz=1547, k=4] [1.3 MB]
Creating vector for state rewards... [1.1 MB]
Creating vector for inf... [1.1 MB]
Allocating iteration vectors... [2 x 1.1 MB]
TOTAL: [11.3 MB]

Starting iterations...

Iterative method: 96 iterations in 0.44 seconds (average 0.002083, setup 0.24)

Prob0A: 55 iterations in 0.38 seconds (average 0.006836, setup 0.00)

Prob1E: 4 iterations in 0.03 seconds (average 0.008000, setup 0.00)

yes = 782, no = 108511, maybe = 31405

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=140698, nc=54943, nnz=153858, k=2] [1.9 MB]
Creating vector for yes... [1.1 MB]
Allocating iteration vectors... [2 x 1.1 MB]
TOTAL: [5.2 MB]

Starting iterations...

Iterative method: 92 iterations in 0.23 seconds (average 0.001043, setup 0.14)

Value in the initial state: 1.2453695875835615

Time for model checking: 1.193 seconds.

Result: 1.2453695875835615 (value in the initial state)

---------------------------------------------------------------------

Note: There were 16 warnings during computation.

