PRISM
=====

Version: 4.3
Date: Tue Aug 02 12:27:53 BST 2016
Hostname: eglinton
Memory limits: cudd=1g, java(heap)=981.5m
Command line: prism uav_sim_ft_mdp.prism -s uav_mdp.props -const 'Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=2,Objy1=2,Objx2=4,Objy2=1,'

Parsing model file "uav_sim_ft_mdp.prism"...

Parsing properties file "uav_mdp.props"...

18 properties:
(1) Pmax=? [ F "MissionSuccessful" ]
(2) Pmin=? [ F "MissionSuccessful" ]
(3) Pmax=? [ F "MissionTimeUp" ]
(4) Pmin=? [ F "MissionTimeUp" ]
(5) Pmax=? [ F "Crash" ]
(6) Pmin=? [ F "Crash" ]
(7) Pmax=? [ F "AllAreaSearched" ]
(8) Pmin=? [ F "AllAreaSearched" ]
(9) Pmax=? [ F "MissionTimeUp"|"AllAreaSearched"|"Crash" ]
(10) Pmin=? [ F "MissionTimeUp"|"AllAreaSearched"|"Crash" ]
(11) R{"drop"}max=? [ F "deadlock" ]
(12) R{"drop"}min=? [ F "deadlock" ]
(13) R{"charge"}max=? [ F "deadlock" ]
(14) R{"charge"}min=? [ F "deadlock" ]
(15) R{"time"}max=? [ F "deadlock" ]
(16) R{"time"}min=? [ F "deadlock" ]
(17) R{"success"}max=? [ F "deadlock" ]/Pmin=? [ F "MissionSuccessful" ]
(18) R{"success"}min=? [ F "deadlock" ]/Pmax=? [ F "MissionSuccessful" ]

Type:        MDP
Modules:     Obj Move Robot T 
Variables:   NoOfObjs posx posy s1 retx rety b t c 

---------------------------------------------------------------------

Model checking: Pmax=? [ F "MissionSuccessful" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=2,Objy1=2,Objx2=4,Objy2=1

Building model...
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=2,Objy1=2,Objx2=4,Objy2=1

Warning: Guard for command 7 of module "Move" is never satisfied.

Warning: Guard for command 8 of module "Move" is never satisfied.

Warning: Guard for command 9 of module "Move" is never satisfied.

Warning: Guard for command 7 of module "Robot" is never satisfied.

Warning: Update 2 of command 3 of module "Robot" doesn't do anything ("(s1'=13)", line 249, column 44)

Warning: Update 2 of command 27 of module "T" doesn't do anything ("(c'=1)", line 372, column 108)

Warning: Guard for command 28 of module "T" is never satisfied.

Warning: Update 2 of command 9 of module "T" doesn't do anything ("(c'=1)", line 349, column 112)

Warning: Guard for command 10 of module "T" is never satisfied.

Warning: Update 2 of command 51 of module "T" doesn't do anything ("(c'=1)", line 403, column 113)

Warning: Guard for command 52 of module "T" is never satisfied.

Computing reachable states...

Reachability (BFS): 80 iterations in 0.90 seconds (average 0.011300, setup 0.00)

Time for model construction: 2.362 seconds.

Warning: Deadlocks detected and fixed in 1 states

Type:        MDP
States:      177147 (1 initial)
Transitions: 455752
Choices:     232086

Transition matrix: 166950 nodes (76 terminal), 455752 minterms, vars: 34r/34c/18nd

Prob0A: 44 iterations in 0.36 seconds (average 0.008273, setup 0.00)

Prob1E: 4 iterations in 0.05 seconds (average 0.013000, setup 0.00)

yes = 962, no = 148853, maybe = 27332

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=177147, nc=47881, nnz=133947, k=2] [1.7 MB]
Creating vector for yes... [1.4 MB]
Allocating iteration vectors... [2 x 1.4 MB]
TOTAL: [5.8 MB]

Starting iterations...

Iterative method: 85 iterations in 0.30 seconds (average 0.001224, setup 0.19)

Value in the initial state: 0.9812032749694528

Time for model checking: 0.743 seconds.

Result: 0.9812032749694528 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmin=? [ F "MissionSuccessful" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=2,Objy1=2,Objx2=4,Objy2=1

Prob0E: 45 iterations in 0.39 seconds (average 0.008622, setup 0.00)

Prob1A: 3 iterations in 0.05 seconds (average 0.016000, setup 0.00)

yes = 962, no = 153003, maybe = 23182

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=177147, nc=40510, nnz=112680, k=2] [1.5 MB]
Creating vector for yes... [1.4 MB]
Allocating iteration vectors... [2 x 1.4 MB]
TOTAL: [5.6 MB]

Starting iterations...

Iterative method: 80 iterations in 0.26 seconds (average 0.001150, setup 0.17)

Value in the initial state: 0.7701135245791303

Time for model checking: 0.722 seconds.

Result: 0.7701135245791303 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmax=? [ F "MissionTimeUp" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=2,Objy1=2,Objx2=4,Objy2=1

Prob0A: 61 iterations in 0.46 seconds (average 0.007475, setup 0.00)

Prob1E: 4 iterations in 0.04 seconds (average 0.010000, setup 0.00)

yes = 344, no = 112320, maybe = 64483

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=177147, nc=109524, nnz=297980, k=4] [3.7 MB]
Creating vector for yes... [1.4 MB]
Allocating iteration vectors... [2 x 1.4 MB]
TOTAL: [7.7 MB]

Starting iterations...

Iterative method: 87 iterations in 0.34 seconds (average 0.001471, setup 0.22)

Value in the initial state: 3.696696667794467E-4

Time for model checking: 0.855 seconds.

Result: 3.696696667794467E-4 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmin=? [ F "MissionTimeUp" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=2,Objy1=2,Objx2=4,Objy2=1

Prob0E: 69 iterations in 0.56 seconds (average 0.008058, setup 0.00)

Prob1A: 3 iterations in 0.03 seconds (average 0.010667, setup 0.00)

yes = 344, no = 112639, maybe = 64164

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=177147, nc=108962, nnz=296749, k=4] [3.7 MB]
Creating vector for yes... [1.4 MB]
Allocating iteration vectors... [2 x 1.4 MB]
TOTAL: [7.7 MB]

Starting iterations...

Iterative method: 101 iterations in 0.32 seconds (average 0.001505, setup 0.17)

Value in the initial state: 7.789015933095132E-7

Time for model checking: 0.916 seconds.

Result: 7.789015933095132E-7 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmax=? [ F "Crash" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=2,Objy1=2,Objx2=4,Objy2=1

Prob0A: 3 iterations in 0.07 seconds (average 0.024000, setup 0.00)

Prob1E: 171 iterations in 2.40 seconds (average 0.014035, setup 0.00)

yes = 86930, no = 2561, maybe = 87656

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=177147, nc=142595, nnz=366261, k=4] [4.5 MB]
Creating vector for yes... [1.4 MB]
Allocating iteration vectors... [2 x 1.4 MB]
TOTAL: [8.6 MB]

Starting iterations...

Iterative method: 83 iterations in 0.41 seconds (average 0.001639, setup 0.27)

Value in the initial state: 0.029437344514847058

Time for model checking: 2.921 seconds.

Result: 0.029437344514847058 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmin=? [ F "Crash" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=2,Objy1=2,Objx2=4,Objy2=1

Prob0E: 3 iterations in 0.16 seconds (average 0.054667, setup 0.00)

Prob1A: 44 iterations in 0.37 seconds (average 0.008364, setup 0.00)

yes = 86930, no = 2561, maybe = 87656

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=177147, nc=142595, nnz=366261, k=4] [4.5 MB]
Creating vector for yes... [1.4 MB]
Allocating iteration vectors... [2 x 1.4 MB]
TOTAL: [8.6 MB]

Starting iterations...

Iterative method: 79 iterations in 0.34 seconds (average 0.001722, setup 0.21)

Value in the initial state: 0.016730260425368314

Time for model checking: 0.894 seconds.

Result: 0.016730260425368314 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmax=? [ F "AllAreaSearched" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=2,Objy1=2,Objx2=4,Objy2=1

Prob0A: 52 iterations in 0.30 seconds (average 0.005846, setup 0.00)

Prob1E: 4 iterations in 0.04 seconds (average 0.010000, setup 0.00)

yes = 1254, no = 136750, maybe = 39143

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=177147, nc=59228, nnz=150895, k=2] [2.0 MB]
Creating vector for yes... [1.4 MB]
Allocating iteration vectors... [2 x 1.4 MB]
TOTAL: [6.0 MB]

Starting iterations...

Iterative method: 82 iterations in 0.25 seconds (average 0.001268, setup 0.14)

Value in the initial state: 0.20299500642366655

Time for model checking: 0.603 seconds.

Result: 0.20299500642366655 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmin=? [ F "AllAreaSearched" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=2,Objy1=2,Objx2=4,Objy2=1

Prob0E: 61 iterations in 0.34 seconds (average 0.005574, setup 0.00)

Prob1A: 3 iterations in 0.04 seconds (average 0.014667, setup 0.00)

yes = 1254, no = 141436, maybe = 34457

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=177147, nc=51314, nnz=128895, k=2] [1.7 MB]
Creating vector for yes... [1.4 MB]
Allocating iteration vectors... [2 x 1.4 MB]
TOTAL: [5.7 MB]

Starting iterations...

Iterative method: 89 iterations in 0.20 seconds (average 0.001169, setup 0.10)

Value in the initial state: 0.0017716324472842242

Time for model checking: 0.598 seconds.

Result: 0.0017716324472842242 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmax=? [ F "MissionTimeUp"|"AllAreaSearched"|"Crash" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=2,Objy1=2,Objx2=4,Objy2=1

Prob0A: 3 iterations in 0.08 seconds (average 0.026667, setup 0.00)

Prob1E: 176 iterations in 2.08 seconds (average 0.011818, setup 0.00)

yes = 153002, no = 963, maybe = 23182

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=177147, nc=40510, nnz=112680, k=2] [1.5 MB]
Creating vector for yes... [1.4 MB]
Allocating iteration vectors... [2 x 1.4 MB]
TOTAL: [5.6 MB]

Starting iterations...

Iterative method: 74 iterations in 0.24 seconds (average 0.001081, setup 0.16)

Value in the initial state: 0.2298860472860259

Time for model checking: 2.424 seconds.

Result: 0.2298860472860259 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmin=? [ F "MissionTimeUp"|"AllAreaSearched"|"Crash" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=2,Objy1=2,Objx2=4,Objy2=1

Prob0E: 3 iterations in 0.07 seconds (average 0.024000, setup 0.00)

Prob1A: 44 iterations in 0.38 seconds (average 0.008727, setup 0.00)

yes = 148852, no = 963, maybe = 27332

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=177147, nc=47881, nnz=133947, k=2] [1.7 MB]
Creating vector for yes... [1.4 MB]
Allocating iteration vectors... [2 x 1.4 MB]
TOTAL: [5.8 MB]

Starting iterations...

Iterative method: 84 iterations in 0.22 seconds (average 0.001190, setup 0.12)

Value in the initial state: 0.018795562563539155

Time for model checking: 0.693 seconds.

Result: 0.018795562563539155 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"drop"}max=? [ F "deadlock" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=2,Objy1=2,Objx2=4,Objy2=1

Prob0E: 4 iterations in 0.06 seconds (average 0.014000, setup 0.00)

Prob1A: 1 iterations in 0.01 seconds (average 0.008000, setup 0.00)

goal = 1, inf = 0, maybe = 177146

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=177147, nc=232085, nnz=455751, k=4] [6.8 MB]
Building sparse matrix (transition rewards)... [n=177147, nc=232085, nnz=0, k=4] [1.6 MB]
Creating vector for state rewards... [1.4 MB]
Creating vector for inf... [1.4 MB]
Allocating iteration vectors... [2 x 1.4 MB]
TOTAL: [13.7 MB]

Starting iterations...

Iterative method: 82 iterations in 0.54 seconds (average 0.002585, setup 0.33)

Value in the initial state: 0.1478328669853759

Time for model checking: 0.613 seconds.

Result: 0.1478328669853759 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"drop"}min=? [ F "deadlock" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=2,Objy1=2,Objx2=4,Objy2=1

Prob0A: 4 iterations in 0.04 seconds (average 0.010000, setup 0.00)

Prob1E: 5 iterations in 0.05 seconds (average 0.009600, setup 0.00)

Warning: PRISM hasn't checked for zero-reward loops.
Your minimum rewards may be too low...

goal = 1, inf = 0, maybe = 177146

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=177147, nc=232085, nnz=455751, k=4] [6.8 MB]
Building sparse matrix (transition rewards)... [n=177147, nc=232085, nnz=0, k=4] [1.6 MB]
Creating vector for state rewards... [1.4 MB]
Creating vector for inf... [1.4 MB]
Allocating iteration vectors... [2 x 1.4 MB]
TOTAL: [13.7 MB]

Starting iterations...

Iterative method: 74 iterations in 0.44 seconds (average 0.002541, setup 0.25)

Value in the initial state: 0.1352660395800636

Time for model checking: 0.536 seconds.

Result: 0.1352660395800636 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"charge"}max=? [ F "deadlock" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=2,Objy1=2,Objx2=4,Objy2=1

Prob0E: 4 iterations in 0.02 seconds (average 0.006000, setup 0.00)

Prob1A: 1 iterations in 0.01 seconds (average 0.008000, setup 0.00)

goal = 1, inf = 0, maybe = 177146

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=177147, nc=232085, nnz=455751, k=4] [6.8 MB]
Building sparse matrix (transition rewards)... [n=177147, nc=232085, nnz=4730, k=4] [1.6 MB]
Creating vector for state rewards... [1.4 MB]
Creating vector for inf... [1.4 MB]
Allocating iteration vectors... [2 x 1.4 MB]
TOTAL: [13.8 MB]

Starting iterations...

Iterative method: 80 iterations in 0.46 seconds (average 0.002550, setup 0.26)

Value in the initial state: 0.254374676695934

Time for model checking: 0.495 seconds.

Result: 0.254374676695934 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"charge"}min=? [ F "deadlock" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=2,Objy1=2,Objx2=4,Objy2=1

Prob0A: 4 iterations in 0.02 seconds (average 0.006000, setup 0.00)

Prob1E: 5 iterations in 0.04 seconds (average 0.008800, setup 0.00)

Warning: PRISM hasn't checked for zero-reward loops.
Your minimum rewards may be too low...

goal = 1, inf = 0, maybe = 177146

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=177147, nc=232085, nnz=455751, k=4] [6.8 MB]
Building sparse matrix (transition rewards)... [n=177147, nc=232085, nnz=4730, k=4] [1.6 MB]
Creating vector for state rewards... [1.4 MB]
Creating vector for inf... [1.4 MB]
Allocating iteration vectors... [2 x 1.4 MB]
TOTAL: [13.8 MB]

Starting iterations...

Iterative method: 86 iterations in 0.48 seconds (average 0.002605, setup 0.26)

Value in the initial state: 0.03771803401853096

Time for model checking: 0.551 seconds.

Result: 0.03771803401853096 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"time"}max=? [ F "deadlock" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=2,Objy1=2,Objx2=4,Objy2=1

Prob0E: 4 iterations in 0.03 seconds (average 0.007000, setup 0.00)

Prob1A: 1 iterations in 0.00 seconds (average 0.004000, setup 0.00)

goal = 1, inf = 0, maybe = 177146

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=177147, nc=232085, nnz=455751, k=4] [6.8 MB]
Building sparse matrix (transition rewards)... [n=177147, nc=232085, nnz=89489, k=4] [2.6 MB]
Creating vector for state rewards... [1.4 MB]
Creating vector for inf... [1.4 MB]
Allocating iteration vectors... [2 x 1.4 MB]
TOTAL: [14.8 MB]

Starting iterations...

Iterative method: 86 iterations in 0.50 seconds (average 0.002605, setup 0.28)

Value in the initial state: 162.47987813295708

Time for model checking: 0.549 seconds.

Result: 162.47987813295708 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"time"}min=? [ F "deadlock" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=2,Objy1=2,Objx2=4,Objy2=1

Prob0A: 4 iterations in 0.02 seconds (average 0.005000, setup 0.00)

Prob1E: 5 iterations in 0.04 seconds (average 0.008800, setup 0.00)

Warning: PRISM hasn't checked for zero-reward loops.
Your minimum rewards may be too low...

goal = 1, inf = 0, maybe = 177146

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=177147, nc=232085, nnz=455751, k=4] [6.8 MB]
Building sparse matrix (transition rewards)... [n=177147, nc=232085, nnz=89489, k=4] [2.6 MB]
Creating vector for state rewards... [1.4 MB]
Creating vector for inf... [1.4 MB]
Allocating iteration vectors... [2 x 1.4 MB]
TOTAL: [14.8 MB]

Starting iterations...

Iterative method: 91 iterations in 0.52 seconds (average 0.002681, setup 0.27)

Value in the initial state: 92.7399011251221

Time for model checking: 0.596 seconds.

Result: 92.7399011251221 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"success"}max=? [ F "deadlock" ]/Pmin=? [ F "MissionSuccessful" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=2,Objy1=2,Objx2=4,Objy2=1

Prob0E: 4 iterations in 0.02 seconds (average 0.006000, setup 0.00)

Prob1A: 1 iterations in 0.00 seconds (average 0.004000, setup 0.00)

goal = 1, inf = 0, maybe = 177146

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=177147, nc=232085, nnz=455751, k=4] [6.8 MB]
Building sparse matrix (transition rewards)... [n=177147, nc=232085, nnz=1896, k=4] [1.6 MB]
Creating vector for state rewards... [1.4 MB]
Creating vector for inf... [1.4 MB]
Allocating iteration vectors... [2 x 1.4 MB]
TOTAL: [13.8 MB]

Starting iterations...

Iterative method: 88 iterations in 0.47 seconds (average 0.002455, setup 0.25)

Prob0E: 45 iterations in 0.31 seconds (average 0.006933, setup 0.00)

Prob1A: 3 iterations in 0.04 seconds (average 0.014667, setup 0.00)

yes = 962, no = 153003, maybe = 23182

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=177147, nc=40510, nnz=112680, k=2] [1.5 MB]
Creating vector for yes... [1.4 MB]
Allocating iteration vectors... [2 x 1.4 MB]
TOTAL: [5.6 MB]

Starting iterations...

Iterative method: 80 iterations in 0.19 seconds (average 0.001100, setup 0.10)

Value in the initial state: 206.89757478339968

Time for model checking: 1.091 seconds.

Result: 206.89757478339968 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"success"}min=? [ F "deadlock" ]/Pmax=? [ F "MissionSuccessful" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=2,Objy1=2,Objx2=4,Objy2=1

Prob0A: 4 iterations in 0.03 seconds (average 0.008000, setup 0.00)

Prob1E: 5 iterations in 0.04 seconds (average 0.008800, setup 0.00)

Warning: PRISM hasn't checked for zero-reward loops.
Your minimum rewards may be too low...

goal = 1, inf = 0, maybe = 177146

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=177147, nc=232085, nnz=455751, k=4] [6.8 MB]
Building sparse matrix (transition rewards)... [n=177147, nc=232085, nnz=1896, k=4] [1.6 MB]
Creating vector for state rewards... [1.4 MB]
Creating vector for inf... [1.4 MB]
Allocating iteration vectors... [2 x 1.4 MB]
TOTAL: [13.8 MB]

Starting iterations...

Iterative method: 84 iterations in 0.47 seconds (average 0.002524, setup 0.26)

Prob0A: 44 iterations in 0.34 seconds (average 0.007636, setup 0.00)

Prob1E: 4 iterations in 0.04 seconds (average 0.011000, setup 0.00)

yes = 962, no = 148853, maybe = 27332

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=177147, nc=47881, nnz=133947, k=2] [1.7 MB]
Creating vector for yes... [1.4 MB]
Allocating iteration vectors... [2 x 1.4 MB]
TOTAL: [5.8 MB]

Starting iterations...

Iterative method: 85 iterations in 0.25 seconds (average 0.001176, setup 0.15)

Value in the initial state: 79.18685766079247

Time for model checking: 1.203 seconds.

Result: 79.18685766079247 (value in the initial state)

---------------------------------------------------------------------

Note: There were 16 warnings during computation.

