PRISM
=====

Version: 4.3
Date: Tue Aug 02 12:28:17 BST 2016
Hostname: eglinton
Memory limits: cudd=1g, java(heap)=981.5m
Command line: prism uav_sim_ft_mdp.prism -s uav_mdp.props -const 'Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=2,Objy1=3,Objx2=6,Objy2=1,'

Parsing model file "uav_sim_ft_mdp.prism"...

Parsing properties file "uav_mdp.props"...

18 properties:
(1) Pmax=? [ F "MissionSuccessful" ]
(2) Pmin=? [ F "MissionSuccessful" ]
(3) Pmax=? [ F "MissionTimeUp" ]
(4) Pmin=? [ F "MissionTimeUp" ]
(5) Pmax=? [ F "Crash" ]
(6) Pmin=? [ F "Crash" ]
(7) Pmax=? [ F "AllAreaSearched" ]
(8) Pmin=? [ F "AllAreaSearched" ]
(9) Pmax=? [ F "MissionTimeUp"|"AllAreaSearched"|"Crash" ]
(10) Pmin=? [ F "MissionTimeUp"|"AllAreaSearched"|"Crash" ]
(11) R{"drop"}max=? [ F "deadlock" ]
(12) R{"drop"}min=? [ F "deadlock" ]
(13) R{"charge"}max=? [ F "deadlock" ]
(14) R{"charge"}min=? [ F "deadlock" ]
(15) R{"time"}max=? [ F "deadlock" ]
(16) R{"time"}min=? [ F "deadlock" ]
(17) R{"success"}max=? [ F "deadlock" ]/Pmin=? [ F "MissionSuccessful" ]
(18) R{"success"}min=? [ F "deadlock" ]/Pmax=? [ F "MissionSuccessful" ]

Type:        MDP
Modules:     Obj Move Robot T 
Variables:   NoOfObjs posx posy s1 retx rety b t c 

---------------------------------------------------------------------

Model checking: Pmax=? [ F "MissionSuccessful" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=2,Objy1=3,Objx2=6,Objy2=1

Building model...
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=2,Objy1=3,Objx2=6,Objy2=1

Warning: Guard for command 7 of module "Move" is never satisfied.

Warning: Guard for command 8 of module "Move" is never satisfied.

Warning: Guard for command 9 of module "Move" is never satisfied.

Warning: Guard for command 7 of module "Robot" is never satisfied.

Warning: Update 2 of command 3 of module "Robot" doesn't do anything ("(s1'=13)", line 249, column 44)

Warning: Update 2 of command 27 of module "T" doesn't do anything ("(c'=1)", line 372, column 108)

Warning: Guard for command 28 of module "T" is never satisfied.

Warning: Update 2 of command 9 of module "T" doesn't do anything ("(c'=1)", line 349, column 112)

Warning: Guard for command 10 of module "T" is never satisfied.

Warning: Update 2 of command 51 of module "T" doesn't do anything ("(c'=1)", line 403, column 113)

Warning: Guard for command 52 of module "T" is never satisfied.

Computing reachable states...

Reachability (BFS): 80 iterations in 0.62 seconds (average 0.007750, setup 0.00)

Time for model construction: 1.711 seconds.

Warning: Deadlocks detected and fixed in 1 states

Type:        MDP
States:      141936 (1 initial)
Transitions: 354969
Choices:     181626

Transition matrix: 161217 nodes (76 terminal), 354969 minterms, vars: 34r/34c/18nd

Prob0A: 57 iterations in 0.22 seconds (average 0.003789, setup 0.00)

Prob1E: 4 iterations in 0.04 seconds (average 0.011000, setup 0.00)

yes = 207, no = 134071, maybe = 7658

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=141936, nc=14025, nnz=42206, k=2] [646.9 KB]
Creating vector for yes... [1.1 MB]
Allocating iteration vectors... [2 x 1.1 MB]
TOTAL: [3.9 MB]

Starting iterations...

Iterative method: 92 iterations in 0.17 seconds (average 0.000783, setup 0.10)

Value in the initial state: 0.0033574409503086505

Time for model checking: 0.458 seconds.

Result: 0.0033574409503086505 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmin=? [ F "MissionSuccessful" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=2,Objy1=3,Objx2=6,Objy2=1

Prob0E: 23 iterations in 0.13 seconds (average 0.005739, setup 0.00)

Prob1A: 3 iterations in 0.03 seconds (average 0.009333, setup 0.00)

yes = 207, no = 135782, maybe = 5947

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=141936, nc=10867, nnz=32446, k=2] [529.4 KB]
Creating vector for yes... [1.1 MB]
Allocating iteration vectors... [2 x 1.1 MB]
TOTAL: [3.8 MB]

Starting iterations...

Iterative method: 43 iterations in 0.07 seconds (average 0.000744, setup 0.04)

Value in the initial state: 0.0

Time for model checking: 0.25 seconds.

Result: 0.0 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmax=? [ F "MissionTimeUp" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=2,Objy1=3,Objx2=6,Objy2=1

Prob0A: 60 iterations in 0.53 seconds (average 0.008800, setup 0.00)

Prob1E: 4 iterations in 0.03 seconds (average 0.008000, setup 0.00)

yes = 396, no = 96551, maybe = 44989

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=141936, nc=78431, nnz=219025, k=4] [2.7 MB]
Creating vector for yes... [1.1 MB]
Allocating iteration vectors... [2 x 1.1 MB]
TOTAL: [6.0 MB]

Starting iterations...

Iterative method: 96 iterations in 0.30 seconds (average 0.001125, setup 0.19)

Value in the initial state: 2.00207737060343E-4

Time for model checking: 0.901 seconds.

Result: 2.00207737060343E-4 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmin=? [ F "MissionTimeUp" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=2,Objy1=3,Objx2=6,Objy2=1

Prob0E: 67 iterations in 0.44 seconds (average 0.006507, setup 0.00)

Prob1A: 3 iterations in 0.04 seconds (average 0.013333, setup 0.00)

yes = 396, no = 96822, maybe = 44718

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=141936, nc=77941, nnz=218022, k=4] [2.7 MB]
Creating vector for yes... [1.1 MB]
Allocating iteration vectors... [2 x 1.1 MB]
TOTAL: [6.0 MB]

Starting iterations...

Iterative method: 110 iterations in 0.31 seconds (average 0.001200, setup 0.18)

Value in the initial state: 7.747933557074975E-7

Time for model checking: 0.803 seconds.

Result: 7.747933557074975E-7 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmax=? [ F "Crash" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=2,Objy1=3,Objx2=6,Objy2=1

Prob0A: 3 iterations in 0.08 seconds (average 0.028000, setup 0.00)

Prob1E: 180 iterations in 2.36 seconds (average 0.013133, setup 0.00)

yes = 69541, no = 1702, maybe = 70693

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=141936, nc=110383, nnz=283726, k=4] [3.5 MB]
Creating vector for yes... [1.1 MB]
Allocating iteration vectors... [2 x 1.1 MB]
TOTAL: [6.7 MB]

Starting iterations...

Iterative method: 94 iterations in 0.40 seconds (average 0.001702, setup 0.24)

Value in the initial state: 0.01749438224567027

Time for model checking: 2.878 seconds.

Result: 0.01749438224567027 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmin=? [ F "Crash" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=2,Objy1=3,Objx2=6,Objy2=1

Prob0E: 3 iterations in 0.05 seconds (average 0.017333, setup 0.00)

Prob1A: 45 iterations in 0.35 seconds (average 0.007822, setup 0.00)

yes = 69541, no = 1702, maybe = 70693

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=141936, nc=110383, nnz=283726, k=4] [3.5 MB]
Creating vector for yes... [1.1 MB]
Allocating iteration vectors... [2 x 1.1 MB]
TOTAL: [6.7 MB]

Starting iterations...

Iterative method: 87 iterations in 0.28 seconds (average 0.001379, setup 0.16)

Value in the initial state: 0.013519400681577467

Time for model checking: 0.692 seconds.

Result: 0.013519400681577467 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmax=? [ F "AllAreaSearched" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=2,Objy1=3,Objx2=6,Objy2=1

Prob0A: 47 iterations in 0.30 seconds (average 0.006468, setup 0.00)

Prob1E: 4 iterations in 0.04 seconds (average 0.010000, setup 0.00)

yes = 1098, no = 102491, maybe = 38347

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=141936, nc=54673, nnz=137488, k=2] [1.8 MB]
Creating vector for yes... [1.1 MB]
Allocating iteration vectors... [2 x 1.1 MB]
TOTAL: [5.0 MB]

Starting iterations...

Iterative method: 92 iterations in 0.26 seconds (average 0.001000, setup 0.16)

Value in the initial state: 0.9864773103539772

Time for model checking: 0.617 seconds.

Result: 0.9864773103539772 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmin=? [ F "AllAreaSearched" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=2,Objy1=3,Objx2=6,Objy2=1

Prob0E: 51 iterations in 0.35 seconds (average 0.006902, setup 0.00)

Prob1A: 3 iterations in 0.03 seconds (average 0.009333, setup 0.00)

yes = 1098, no = 105376, maybe = 35462

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=141936, nc=49189, nnz=120947, k=2] [1.6 MB]
Creating vector for yes... [1.1 MB]
Allocating iteration vectors... [2 x 1.1 MB]
TOTAL: [4.8 MB]

Starting iterations...

Iterative method: 93 iterations in 0.20 seconds (average 0.001032, setup 0.10)

Value in the initial state: 0.9791982136603973

Time for model checking: 0.558 seconds.

Result: 0.9791982136603973 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmax=? [ F "MissionTimeUp"|"AllAreaSearched"|"Crash" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=2,Objy1=3,Objx2=6,Objy2=1

Prob0A: 3 iterations in 0.06 seconds (average 0.021333, setup 0.00)

Prob1E: 88 iterations in 0.80 seconds (average 0.009045, setup 0.00)

yes = 135781, no = 208, maybe = 5947

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=141936, nc=10867, nnz=32446, k=2] [529.4 KB]
Creating vector for yes... [1.1 MB]
Allocating iteration vectors... [2 x 1.1 MB]
TOTAL: [3.8 MB]

Starting iterations...

Iterative method: 42 iterations in 0.08 seconds (average 0.000667, setup 0.06)

Value in the initial state: 1.0

Time for model checking: 0.955 seconds.

Result: 1.0 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmin=? [ F "MissionTimeUp"|"AllAreaSearched"|"Crash" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=2,Objy1=3,Objx2=6,Objy2=1

Prob0E: 3 iterations in 0.03 seconds (average 0.010667, setup 0.00)

Prob1A: 57 iterations in 0.32 seconds (average 0.005614, setup 0.00)

yes = 134070, no = 208, maybe = 7658

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=141936, nc=14025, nnz=42206, k=2] [646.9 KB]
Creating vector for yes... [1.1 MB]
Allocating iteration vectors... [2 x 1.1 MB]
TOTAL: [3.9 MB]

Starting iterations...

Iterative method: 62 iterations in 0.12 seconds (average 0.000774, setup 0.07)

Value in the initial state: 0.9966398400004824

Time for model checking: 0.477 seconds.

Result: 0.9966398400004824 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"drop"}max=? [ F "deadlock" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=2,Objy1=3,Objx2=6,Objy2=1

Prob0E: 4 iterations in 0.04 seconds (average 0.011000, setup 0.00)

Prob1A: 1 iterations in 0.00 seconds (average 0.004000, setup 0.00)

goal = 1, inf = 0, maybe = 141935

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=141936, nc=181625, nnz=354968, k=4] [5.3 MB]
Building sparse matrix (transition rewards)... [n=141936, nc=181625, nnz=0, k=4] [1.2 MB]
Creating vector for state rewards... [1.1 MB]
Creating vector for inf... [1.1 MB]
Allocating iteration vectors... [2 x 1.1 MB]
TOTAL: [10.9 MB]

Starting iterations...

Iterative method: 75 iterations in 0.45 seconds (average 0.002027, setup 0.30)

Value in the initial state: 0.13827396011999724

Time for model checking: 0.51 seconds.

Result: 0.13827396011999724 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"drop"}min=? [ F "deadlock" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=2,Objy1=3,Objx2=6,Objy2=1

Prob0A: 4 iterations in 0.02 seconds (average 0.005000, setup 0.00)

Prob1E: 5 iterations in 0.03 seconds (average 0.006400, setup 0.00)

Warning: PRISM hasn't checked for zero-reward loops.
Your minimum rewards may be too low...

goal = 1, inf = 0, maybe = 141935

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=141936, nc=181625, nnz=354968, k=4] [5.3 MB]
Building sparse matrix (transition rewards)... [n=141936, nc=181625, nnz=0, k=4] [1.2 MB]
Creating vector for state rewards... [1.1 MB]
Creating vector for inf... [1.1 MB]
Allocating iteration vectors... [2 x 1.1 MB]
TOTAL: [10.9 MB]

Starting iterations...

Iterative method: 65 iterations in 0.33 seconds (average 0.001969, setup 0.20)

Value in the initial state: 0.13751577746525828

Time for model checking: 0.402 seconds.

Result: 0.13751577746525828 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"charge"}max=? [ F "deadlock" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=2,Objy1=3,Objx2=6,Objy2=1

Prob0E: 4 iterations in 0.02 seconds (average 0.006000, setup 0.00)

Prob1A: 1 iterations in 0.00 seconds (average 0.004000, setup 0.00)

goal = 1, inf = 0, maybe = 141935

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=141936, nc=181625, nnz=354968, k=4] [5.3 MB]
Building sparse matrix (transition rewards)... [n=141936, nc=181625, nnz=3621, k=4] [1.3 MB]
Creating vector for state rewards... [1.1 MB]
Creating vector for inf... [1.1 MB]
Allocating iteration vectors... [2 x 1.1 MB]
TOTAL: [10.9 MB]

Starting iterations...

Iterative method: 82 iterations in 0.37 seconds (average 0.002000, setup 0.20)

Value in the initial state: 0.009483357015174547

Time for model checking: 0.408 seconds.

Result: 0.009483357015174547 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"charge"}min=? [ F "deadlock" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=2,Objy1=3,Objx2=6,Objy2=1

Prob0A: 4 iterations in 0.02 seconds (average 0.004000, setup 0.00)

Prob1E: 5 iterations in 0.03 seconds (average 0.006400, setup 0.00)

Warning: PRISM hasn't checked for zero-reward loops.
Your minimum rewards may be too low...

goal = 1, inf = 0, maybe = 141935

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=141936, nc=181625, nnz=354968, k=4] [5.3 MB]
Building sparse matrix (transition rewards)... [n=141936, nc=181625, nnz=3621, k=4] [1.3 MB]
Creating vector for state rewards... [1.1 MB]
Creating vector for inf... [1.1 MB]
Allocating iteration vectors... [2 x 1.1 MB]
TOTAL: [10.9 MB]

Starting iterations...

Iterative method: 94 iterations in 0.40 seconds (average 0.002000, setup 0.21)

Value in the initial state: 8.799217696335096E-4

Time for model checking: 0.454 seconds.

Result: 8.799217696335096E-4 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"time"}max=? [ F "deadlock" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=2,Objy1=3,Objx2=6,Objy2=1

Prob0E: 4 iterations in 0.02 seconds (average 0.006000, setup 0.00)

Prob1A: 1 iterations in 0.00 seconds (average 0.004000, setup 0.00)

goal = 1, inf = 0, maybe = 141935

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=141936, nc=181625, nnz=354968, k=4] [5.3 MB]
Building sparse matrix (transition rewards)... [n=141936, nc=181625, nnz=71241, k=4] [2.0 MB]
Creating vector for state rewards... [1.1 MB]
Creating vector for inf... [1.1 MB]
Allocating iteration vectors... [2 x 1.1 MB]
TOTAL: [11.7 MB]

Starting iterations...

Iterative method: 98 iterations in 0.43 seconds (average 0.002000, setup 0.23)

Value in the initial state: 96.95322139517698

Time for model checking: 0.473 seconds.

Result: 96.95322139517698 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"time"}min=? [ F "deadlock" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=2,Objy1=3,Objx2=6,Objy2=1

Prob0A: 4 iterations in 0.02 seconds (average 0.005000, setup 0.00)

Prob1E: 5 iterations in 0.03 seconds (average 0.005600, setup 0.00)

Warning: PRISM hasn't checked for zero-reward loops.
Your minimum rewards may be too low...

goal = 1, inf = 0, maybe = 141935

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=141936, nc=181625, nnz=354968, k=4] [5.3 MB]
Building sparse matrix (transition rewards)... [n=141936, nc=181625, nnz=71241, k=4] [2.0 MB]
Creating vector for state rewards... [1.1 MB]
Creating vector for inf... [1.1 MB]
Allocating iteration vectors... [2 x 1.1 MB]
TOTAL: [11.7 MB]

Starting iterations...

Iterative method: 101 iterations in 0.43 seconds (average 0.002020, setup 0.22)

Value in the initial state: 75.04265585242952

Time for model checking: 0.484 seconds.

Result: 75.04265585242952 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"success"}max=? [ F "deadlock" ]/Pmin=? [ F "MissionSuccessful" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=2,Objy1=3,Objx2=6,Objy2=1

Prob0E: 4 iterations in 0.02 seconds (average 0.005000, setup 0.00)

Prob1A: 1 iterations in 0.00 seconds (average 0.004000, setup 0.00)

goal = 1, inf = 0, maybe = 141935

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=141936, nc=181625, nnz=354968, k=4] [5.3 MB]
Building sparse matrix (transition rewards)... [n=141936, nc=181625, nnz=408, k=4] [1.2 MB]
Creating vector for state rewards... [1.1 MB]
Creating vector for inf... [1.1 MB]
Allocating iteration vectors... [2 x 1.1 MB]
TOTAL: [10.9 MB]

Starting iterations...

Iterative method: 89 iterations in 0.49 seconds (average 0.002382, setup 0.28)

Prob0E: 23 iterations in 0.12 seconds (average 0.005391, setup 0.00)

Prob1A: 3 iterations in 0.02 seconds (average 0.005333, setup 0.00)

yes = 207, no = 135782, maybe = 5947

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=141936, nc=10867, nnz=32446, k=2] [529.4 KB]
Creating vector for yes... [1.1 MB]
Allocating iteration vectors... [2 x 1.1 MB]
TOTAL: [3.8 MB]

Starting iterations...

Iterative method: 43 iterations in 0.06 seconds (average 0.000744, setup 0.03)

Value in the initial state: Infinity

Time for model checking: 0.736 seconds.

Result: Infinity (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"success"}min=? [ F "deadlock" ]/Pmax=? [ F "MissionSuccessful" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=2,Objy1=3,Objx2=6,Objy2=1

Prob0A: 4 iterations in 0.07 seconds (average 0.018000, setup 0.00)

Prob1E: 5 iterations in 0.03 seconds (average 0.006400, setup 0.00)

Warning: PRISM hasn't checked for zero-reward loops.
Your minimum rewards may be too low...

goal = 1, inf = 0, maybe = 141935

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=141936, nc=181625, nnz=354968, k=4] [5.3 MB]
Building sparse matrix (transition rewards)... [n=141936, nc=181625, nnz=408, k=4] [1.2 MB]
Creating vector for state rewards... [1.1 MB]
Creating vector for inf... [1.1 MB]
Allocating iteration vectors... [2 x 1.1 MB]
TOTAL: [10.9 MB]

Starting iterations...

Iterative method: 48 iterations in 0.34 seconds (average 0.002083, setup 0.24)

Prob0A: 57 iterations in 0.13 seconds (average 0.002316, setup 0.00)

Prob1E: 4 iterations in 0.02 seconds (average 0.004000, setup 0.00)

yes = 207, no = 134071, maybe = 7658

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=141936, nc=14025, nnz=42206, k=2] [646.9 KB]
Creating vector for yes... [1.1 MB]
Allocating iteration vectors... [2 x 1.1 MB]
TOTAL: [3.9 MB]

Starting iterations...

Iterative method: 92 iterations in 0.12 seconds (average 0.000696, setup 0.05)

Value in the initial state: 0.0

Time for model checking: 0.708 seconds.

Result: 0.0 (value in the initial state)

---------------------------------------------------------------------

Note: There were 16 warnings during computation.

