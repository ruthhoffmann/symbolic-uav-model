PRISM
=====

Version: 4.3
Date: Tue Aug 02 12:35:17 BST 2016
Hostname: eglinton
Memory limits: cudd=1g, java(heap)=981.5m
Command line: prism uav_sim_ft_mdp.prism -s uav_mdp.props -const 'Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=0,Objy1=1,Objx2=0,Objy2=2,'

Parsing model file "uav_sim_ft_mdp.prism"...

Parsing properties file "uav_mdp.props"...

18 properties:
(1) Pmax=? [ F "MissionSuccessful" ]
(2) Pmin=? [ F "MissionSuccessful" ]
(3) Pmax=? [ F "MissionTimeUp" ]
(4) Pmin=? [ F "MissionTimeUp" ]
(5) Pmax=? [ F "Crash" ]
(6) Pmin=? [ F "Crash" ]
(7) Pmax=? [ F "AllAreaSearched" ]
(8) Pmin=? [ F "AllAreaSearched" ]
(9) Pmax=? [ F "MissionTimeUp"|"AllAreaSearched"|"Crash" ]
(10) Pmin=? [ F "MissionTimeUp"|"AllAreaSearched"|"Crash" ]
(11) R{"drop"}max=? [ F "deadlock" ]
(12) R{"drop"}min=? [ F "deadlock" ]
(13) R{"charge"}max=? [ F "deadlock" ]
(14) R{"charge"}min=? [ F "deadlock" ]
(15) R{"time"}max=? [ F "deadlock" ]
(16) R{"time"}min=? [ F "deadlock" ]
(17) R{"success"}max=? [ F "deadlock" ]/Pmin=? [ F "MissionSuccessful" ]
(18) R{"success"}min=? [ F "deadlock" ]/Pmax=? [ F "MissionSuccessful" ]

Type:        MDP
Modules:     Obj Move Robot T 
Variables:   NoOfObjs posx posy s1 retx rety b t c 

---------------------------------------------------------------------

Model checking: Pmax=? [ F "MissionSuccessful" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=0,Objy1=1,Objx2=0,Objy2=2

Building model...
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=0,Objy1=1,Objx2=0,Objy2=2

Warning: Guard for command 4 of module "Move" is never satisfied.

Warning: Guard for command 7 of module "Move" is never satisfied.

Warning: Guard for command 8 of module "Move" is never satisfied.

Warning: Guard for command 9 of module "Move" is never satisfied.

Warning: Guard for command 7 of module "Robot" is never satisfied.

Warning: Update 2 of command 3 of module "Robot" doesn't do anything ("(s1'=13)", line 249, column 44)

Warning: Update 2 of command 27 of module "T" doesn't do anything ("(c'=1)", line 372, column 108)

Warning: Guard for command 28 of module "T" is never satisfied.

Warning: Update 2 of command 9 of module "T" doesn't do anything ("(c'=1)", line 349, column 112)

Warning: Guard for command 10 of module "T" is never satisfied.

Warning: Update 2 of command 51 of module "T" doesn't do anything ("(c'=1)", line 403, column 113)

Warning: Guard for command 52 of module "T" is never satisfied.

Computing reachable states...

Reachability (BFS): 80 iterations in 0.69 seconds (average 0.008650, setup 0.00)

Time for model construction: 2.205 seconds.

Warning: Deadlocks detected and fixed in 1 states

Type:        MDP
States:      178970 (1 initial)
Transitions: 487636
Choices:     239731

Transition matrix: 160273 nodes (76 terminal), 487636 minterms, vars: 34r/34c/18nd

Prob0A: 43 iterations in 0.30 seconds (average 0.007070, setup 0.00)

Prob1E: 4 iterations in 0.06 seconds (average 0.014000, setup 0.00)

yes = 944, no = 149343, maybe = 28683

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=178970, nc=52378, nnz=152226, k=2] [2.0 MB]
Creating vector for yes... [1.4 MB]
Allocating iteration vectors... [2 x 1.4 MB]
TOTAL: [6.1 MB]

Starting iterations...

Iterative method: 86 iterations in 0.31 seconds (average 0.001209, setup 0.20)

Value in the initial state: 0.9809756298828908

Time for model checking: 0.721 seconds.

Result: 0.9809756298828908 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmin=? [ F "MissionSuccessful" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=0,Objy1=1,Objx2=0,Objy2=2

Prob0E: 43 iterations in 0.34 seconds (average 0.007907, setup 0.00)

Prob1A: 3 iterations in 0.05 seconds (average 0.016000, setup 0.00)

yes = 944, no = 153559, maybe = 24467

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=178970, nc=44497, nnz=128893, k=2] [1.7 MB]
Creating vector for yes... [1.4 MB]
Allocating iteration vectors... [2 x 1.4 MB]
TOTAL: [5.8 MB]

Starting iterations...

Iterative method: 81 iterations in 0.27 seconds (average 0.001235, setup 0.17)

Value in the initial state: 0.7528493211413795

Time for model checking: 0.927 seconds.

Result: 0.7528493211413795 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmax=? [ F "MissionTimeUp" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=0,Objy1=1,Objx2=0,Objy2=2

Prob0A: 64 iterations in 0.47 seconds (average 0.007313, setup 0.00)

Prob1E: 4 iterations in 0.05 seconds (average 0.013000, setup 0.00)

yes = 308, no = 113989, maybe = 64673

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=178970, nc=115842, nnz=328240, k=4] [4.0 MB]
Creating vector for yes... [1.4 MB]
Allocating iteration vectors... [2 x 1.4 MB]
TOTAL: [8.1 MB]

Starting iterations...

Iterative method: 88 iterations in 0.37 seconds (average 0.001591, setup 0.23)

Value in the initial state: 4.321835542858925E-4

Time for model checking: 1.264 seconds.

Result: 4.321835542858925E-4 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmin=? [ F "MissionTimeUp" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=0,Objy1=1,Objx2=0,Objy2=2

Prob0E: 71 iterations in 0.55 seconds (average 0.007775, setup 0.00)

Prob1A: 3 iterations in 0.04 seconds (average 0.013333, setup 0.00)

yes = 308, no = 114346, maybe = 64316

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=178970, nc=115204, nnz=326931, k=4] [4.0 MB]
Creating vector for yes... [1.4 MB]
Allocating iteration vectors... [2 x 1.4 MB]
TOTAL: [8.1 MB]

Starting iterations...

Iterative method: 104 iterations in 0.39 seconds (average 0.001615, setup 0.22)

Value in the initial state: 1.4708328759659925E-6

Time for model checking: 1.107 seconds.

Result: 1.4708328759659925E-6 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmax=? [ F "Crash" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=0,Objy1=1,Objx2=0,Objy2=2

Prob0A: 3 iterations in 0.09 seconds (average 0.029333, setup 0.00)

Prob1E: 164 iterations in 2.33 seconds (average 0.014195, setup 0.00)

yes = 87618, no = 2471, maybe = 88881

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=178970, nc=149642, nnz=397547, k=4] [4.9 MB]
Creating vector for yes... [1.4 MB]
Allocating iteration vectors... [2 x 1.4 MB]
TOTAL: [9.0 MB]

Starting iterations...

Iterative method: 86 iterations in 0.42 seconds (average 0.001674, setup 0.28)

Value in the initial state: 0.030273508546361447

Time for model checking: 3.543 seconds.

Result: 0.030273508546361447 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmin=? [ F "Crash" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=0,Objy1=1,Objx2=0,Objy2=2

Prob0E: 3 iterations in 0.05 seconds (average 0.016000, setup 0.00)

Prob1A: 43 iterations in 0.33 seconds (average 0.007628, setup 0.00)

yes = 87618, no = 2471, maybe = 88881

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=178970, nc=149642, nnz=397547, k=4] [4.9 MB]
Creating vector for yes... [1.4 MB]
Allocating iteration vectors... [2 x 1.4 MB]
TOTAL: [9.0 MB]

Starting iterations...

Iterative method: 80 iterations in 0.32 seconds (average 0.001800, setup 0.18)

Value in the initial state: 0.016907557552792673

Time for model checking: 0.728 seconds.

Result: 0.016907557552792673 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmax=? [ F "AllAreaSearched" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=0,Objy1=1,Objx2=0,Objy2=2

Prob0A: 49 iterations in 0.32 seconds (average 0.006449, setup 0.00)

Prob1E: 4 iterations in 0.04 seconds (average 0.010000, setup 0.00)

yes = 1218, no = 136632, maybe = 41120

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=178970, nc=63779, nnz=168682, k=2] [2.2 MB]
Creating vector for yes... [1.4 MB]
Allocating iteration vectors... [2 x 1.4 MB]
TOTAL: [6.3 MB]

Starting iterations...

Iterative method: 85 iterations in 0.27 seconds (average 0.001365, setup 0.16)

Value in the initial state: 0.21945510870147739

Time for model checking: 1.145 seconds.

Result: 0.21945510870147739 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmin=? [ F "AllAreaSearched" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=0,Objy1=1,Objx2=0,Objy2=2

Prob0E: 62 iterations in 0.37 seconds (average 0.006000, setup 0.00)

Prob1A: 3 iterations in 0.03 seconds (average 0.009333, setup 0.00)

yes = 1218, no = 141077, maybe = 36675

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=178970, nc=55521, nnz=143744, k=2] [1.9 MB]
Creating vector for yes... [1.4 MB]
Allocating iteration vectors... [2 x 1.4 MB]
TOTAL: [6.0 MB]

Starting iterations...

Iterative method: 91 iterations in 0.22 seconds (average 0.001319, setup 0.10)

Value in the initial state: 0.0017575113501283898

Time for model checking: 0.696 seconds.

Result: 0.0017575113501283898 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmax=? [ F "MissionTimeUp"|"AllAreaSearched"|"Crash" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=0,Objy1=1,Objx2=0,Objy2=2

Prob0A: 3 iterations in 0.07 seconds (average 0.022667, setup 0.00)

Prob1E: 168 iterations in 1.86 seconds (average 0.011071, setup 0.00)

yes = 153558, no = 945, maybe = 24467

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=178970, nc=44497, nnz=128893, k=2] [1.7 MB]
Creating vector for yes... [1.4 MB]
Allocating iteration vectors... [2 x 1.4 MB]
TOTAL: [5.8 MB]

Starting iterations...

Iterative method: 75 iterations in 0.26 seconds (average 0.001227, setup 0.17)

Value in the initial state: 0.24715053863029432

Time for model checking: 2.45 seconds.

Result: 0.24715053863029432 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmin=? [ F "MissionTimeUp"|"AllAreaSearched"|"Crash" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=0,Objy1=1,Objx2=0,Objy2=2

Prob0E: 3 iterations in 0.14 seconds (average 0.046667, setup 0.00)

Prob1A: 43 iterations in 0.28 seconds (average 0.006512, setup 0.00)

yes = 149342, no = 945, maybe = 28683

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=178970, nc=52378, nnz=152226, k=2] [2.0 MB]
Creating vector for yes... [1.4 MB]
Allocating iteration vectors... [2 x 1.4 MB]
TOTAL: [6.1 MB]

Starting iterations...

Iterative method: 84 iterations in 0.26 seconds (average 0.001238, setup 0.16)

Value in the initial state: 0.019023475457122494

Time for model checking: 0.819 seconds.

Result: 0.019023475457122494 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"drop"}max=? [ F "deadlock" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=0,Objy1=1,Objx2=0,Objy2=2

Prob0E: 4 iterations in 0.04 seconds (average 0.011000, setup 0.00)

Prob1A: 1 iterations in 0.00 seconds (average 0.004000, setup 0.00)

goal = 1, inf = 0, maybe = 178969

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=178970, nc=239730, nnz=487635, k=4] [7.2 MB]
Building sparse matrix (transition rewards)... [n=178970, nc=239730, nnz=0, k=4] [1.6 MB]
Creating vector for state rewards... [1.4 MB]
Creating vector for inf... [1.4 MB]
Allocating iteration vectors... [2 x 1.4 MB]
TOTAL: [14.2 MB]

Starting iterations...

Iterative method: 83 iterations in 0.53 seconds (average 0.002602, setup 0.32)

Value in the initial state: 0.18237706461173223

Time for model checking: 0.602 seconds.

Result: 0.18237706461173223 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"drop"}min=? [ F "deadlock" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=0,Objy1=1,Objx2=0,Objy2=2

Prob0A: 4 iterations in 0.03 seconds (average 0.007000, setup 0.00)

Prob1E: 5 iterations in 0.04 seconds (average 0.008000, setup 0.00)

Warning: PRISM hasn't checked for zero-reward loops.
Your minimum rewards may be too low...

goal = 1, inf = 0, maybe = 178969

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=178970, nc=239730, nnz=487635, k=4] [7.2 MB]
Building sparse matrix (transition rewards)... [n=178970, nc=239730, nnz=0, k=4] [1.6 MB]
Creating vector for state rewards... [1.4 MB]
Creating vector for inf... [1.4 MB]
Allocating iteration vectors... [2 x 1.4 MB]
TOTAL: [14.2 MB]

Starting iterations...

Iterative method: 74 iterations in 0.46 seconds (average 0.002649, setup 0.27)

Value in the initial state: 0.1568282220418697

Time for model checking: 0.77 seconds.

Result: 0.1568282220418697 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"charge"}max=? [ F "deadlock" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=0,Objy1=1,Objx2=0,Objy2=2

Prob0E: 4 iterations in 0.02 seconds (average 0.004000, setup 0.00)

Prob1A: 1 iterations in 0.00 seconds (average 0.004000, setup 0.00)

goal = 1, inf = 0, maybe = 178969

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=178970, nc=239730, nnz=487635, k=4] [7.2 MB]
Building sparse matrix (transition rewards)... [n=178970, nc=239730, nnz=5676, k=4] [1.7 MB]
Creating vector for state rewards... [1.4 MB]
Creating vector for inf... [1.4 MB]
Allocating iteration vectors... [2 x 1.4 MB]
TOTAL: [14.3 MB]

Starting iterations...

Iterative method: 80 iterations in 0.47 seconds (average 0.002600, setup 0.26)

Value in the initial state: 0.2738843513357749

Time for model checking: 0.508 seconds.

Result: 0.2738843513357749 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"charge"}min=? [ F "deadlock" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=0,Objy1=1,Objx2=0,Objy2=2

Prob0A: 4 iterations in 0.02 seconds (average 0.005000, setup 0.00)

Prob1E: 5 iterations in 0.03 seconds (average 0.006400, setup 0.00)

Warning: PRISM hasn't checked for zero-reward loops.
Your minimum rewards may be too low...

goal = 1, inf = 0, maybe = 178969

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=178970, nc=239730, nnz=487635, k=4] [7.2 MB]
Building sparse matrix (transition rewards)... [n=178970, nc=239730, nnz=5676, k=4] [1.7 MB]
Creating vector for state rewards... [1.4 MB]
Creating vector for inf... [1.4 MB]
Allocating iteration vectors... [2 x 1.4 MB]
TOTAL: [14.3 MB]

Starting iterations...

Iterative method: 87 iterations in 0.50 seconds (average 0.002667, setup 0.27)

Value in the initial state: 0.047965291592203085

Time for model checking: 0.784 seconds.

Result: 0.047965291592203085 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"time"}max=? [ F "deadlock" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=0,Objy1=1,Objx2=0,Objy2=2

Prob0E: 4 iterations in 0.02 seconds (average 0.006000, setup 0.00)

Prob1A: 1 iterations in 0.00 seconds (average 0.004000, setup 0.00)

goal = 1, inf = 0, maybe = 178969

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=178970, nc=239730, nnz=487635, k=4] [7.2 MB]
Building sparse matrix (transition rewards)... [n=178970, nc=239730, nnz=90087, k=4] [2.6 MB]
Creating vector for state rewards... [1.4 MB]
Creating vector for inf... [1.4 MB]
Allocating iteration vectors... [2 x 1.4 MB]
TOTAL: [15.3 MB]

Starting iterations...

Iterative method: 88 iterations in 0.54 seconds (average 0.002682, setup 0.30)

Value in the initial state: 167.04204509768508

Time for model checking: 0.764 seconds.

Result: 167.04204509768508 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"time"}min=? [ F "deadlock" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=0,Objy1=1,Objx2=0,Objy2=2

Prob0A: 4 iterations in 0.02 seconds (average 0.006000, setup 0.00)

Prob1E: 5 iterations in 0.04 seconds (average 0.008800, setup 0.00)

Warning: PRISM hasn't checked for zero-reward loops.
Your minimum rewards may be too low...

goal = 1, inf = 0, maybe = 178969

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=178970, nc=239730, nnz=487635, k=4] [7.2 MB]
Building sparse matrix (transition rewards)... [n=178970, nc=239730, nnz=90087, k=4] [2.6 MB]
Creating vector for state rewards... [1.4 MB]
Creating vector for inf... [1.4 MB]
Allocating iteration vectors... [2 x 1.4 MB]
TOTAL: [15.3 MB]

Starting iterations...

Iterative method: 92 iterations in 0.55 seconds (average 0.002696, setup 0.30)

Value in the initial state: 93.69922180473769

Time for model checking: 0.886 seconds.

Result: 93.69922180473769 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"success"}max=? [ F "deadlock" ]/Pmin=? [ F "MissionSuccessful" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=0,Objy1=1,Objx2=0,Objy2=2

Prob0E: 4 iterations in 0.02 seconds (average 0.006000, setup 0.00)

Prob1A: 1 iterations in 0.00 seconds (average 0.004000, setup 0.00)

goal = 1, inf = 0, maybe = 178969

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=178970, nc=239730, nnz=487635, k=4] [7.2 MB]
Building sparse matrix (transition rewards)... [n=178970, nc=239730, nnz=1863, k=4] [1.6 MB]
Creating vector for state rewards... [1.4 MB]
Creating vector for inf... [1.4 MB]
Allocating iteration vectors... [2 x 1.4 MB]
TOTAL: [14.3 MB]

Starting iterations...

Iterative method: 87 iterations in 0.49 seconds (average 0.002575, setup 0.26)

Prob0E: 43 iterations in 0.24 seconds (average 0.005581, setup 0.00)

Prob1A: 3 iterations in 0.04 seconds (average 0.012000, setup 0.00)

yes = 944, no = 153559, maybe = 24467

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=178970, nc=44497, nnz=128893, k=2] [1.7 MB]
Creating vector for yes... [1.4 MB]
Allocating iteration vectors... [2 x 1.4 MB]
TOTAL: [5.8 MB]

Starting iterations...

Iterative method: 81 iterations in 0.23 seconds (average 0.001185, setup 0.14)

Value in the initial state: 217.47453379521363

Time for model checking: 1.19 seconds.

Result: 217.47453379521363 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"success"}min=? [ F "deadlock" ]/Pmax=? [ F "MissionSuccessful" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=0,Objy1=1,Objx2=0,Objy2=2

Prob0A: 4 iterations in 0.03 seconds (average 0.008000, setup 0.00)

Prob1E: 5 iterations in 0.04 seconds (average 0.008000, setup 0.00)

Warning: PRISM hasn't checked for zero-reward loops.
Your minimum rewards may be too low...

goal = 1, inf = 0, maybe = 178969

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=178970, nc=239730, nnz=487635, k=4] [7.2 MB]
Building sparse matrix (transition rewards)... [n=178970, nc=239730, nnz=1863, k=4] [1.6 MB]
Creating vector for state rewards... [1.4 MB]
Creating vector for inf... [1.4 MB]
Allocating iteration vectors... [2 x 1.4 MB]
TOTAL: [14.3 MB]

Starting iterations...

Iterative method: 86 iterations in 0.50 seconds (average 0.002698, setup 0.26)

Prob0A: 43 iterations in 0.21 seconds (average 0.004930, setup 0.00)

Prob1E: 4 iterations in 0.03 seconds (average 0.007000, setup 0.00)

yes = 944, no = 149343, maybe = 28683

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=178970, nc=52378, nnz=152226, k=2] [2.0 MB]
Creating vector for yes... [1.4 MB]
Allocating iteration vectors... [2 x 1.4 MB]
TOTAL: [6.1 MB]

Starting iterations...

Iterative method: 86 iterations in 0.22 seconds (average 0.001256, setup 0.11)

Value in the initial state: 78.30254687410365

Time for model checking: 1.137 seconds.

Result: 78.30254687410365 (value in the initial state)

---------------------------------------------------------------------

Note: There were 17 warnings during computation.

