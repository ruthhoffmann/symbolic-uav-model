PRISM
=====

Version: 4.3
Date: Tue Aug 02 12:38:41 BST 2016
Hostname: eglinton
Memory limits: cudd=1g, java(heap)=981.5m
Command line: prism uav_sim_ft_mdp.prism -s uav_mdp.props -const 'Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=2,Objy1=1,Objx2=4,Objy2=2,'

Parsing model file "uav_sim_ft_mdp.prism"...

Parsing properties file "uav_mdp.props"...

18 properties:
(1) Pmax=? [ F "MissionSuccessful" ]
(2) Pmin=? [ F "MissionSuccessful" ]
(3) Pmax=? [ F "MissionTimeUp" ]
(4) Pmin=? [ F "MissionTimeUp" ]
(5) Pmax=? [ F "Crash" ]
(6) Pmin=? [ F "Crash" ]
(7) Pmax=? [ F "AllAreaSearched" ]
(8) Pmin=? [ F "AllAreaSearched" ]
(9) Pmax=? [ F "MissionTimeUp"|"AllAreaSearched"|"Crash" ]
(10) Pmin=? [ F "MissionTimeUp"|"AllAreaSearched"|"Crash" ]
(11) R{"drop"}max=? [ F "deadlock" ]
(12) R{"drop"}min=? [ F "deadlock" ]
(13) R{"charge"}max=? [ F "deadlock" ]
(14) R{"charge"}min=? [ F "deadlock" ]
(15) R{"time"}max=? [ F "deadlock" ]
(16) R{"time"}min=? [ F "deadlock" ]
(17) R{"success"}max=? [ F "deadlock" ]/Pmin=? [ F "MissionSuccessful" ]
(18) R{"success"}min=? [ F "deadlock" ]/Pmax=? [ F "MissionSuccessful" ]

Type:        MDP
Modules:     Obj Move Robot T 
Variables:   NoOfObjs posx posy s1 retx rety b t c 

---------------------------------------------------------------------

Model checking: Pmax=? [ F "MissionSuccessful" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=2,Objy1=1,Objx2=4,Objy2=2

Building model...
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=2,Objy1=1,Objx2=4,Objy2=2

Warning: Guard for command 7 of module "Move" is never satisfied.

Warning: Guard for command 8 of module "Move" is never satisfied.

Warning: Guard for command 9 of module "Move" is never satisfied.

Warning: Guard for command 7 of module "Robot" is never satisfied.

Warning: Update 2 of command 3 of module "Robot" doesn't do anything ("(s1'=13)", line 249, column 44)

Warning: Update 2 of command 27 of module "T" doesn't do anything ("(c'=1)", line 372, column 108)

Warning: Guard for command 28 of module "T" is never satisfied.

Warning: Update 2 of command 9 of module "T" doesn't do anything ("(c'=1)", line 349, column 112)

Warning: Guard for command 10 of module "T" is never satisfied.

Warning: Update 2 of command 51 of module "T" doesn't do anything ("(c'=1)", line 403, column 113)

Warning: Guard for command 52 of module "T" is never satisfied.

Computing reachable states...

Reachability (BFS): 77 iterations in 0.72 seconds (average 0.009403, setup 0.00)

Time for model construction: 1.831 seconds.

Warning: Deadlocks detected and fixed in 1 states

Type:        MDP
States:      192705 (1 initial)
Transitions: 515807
Choices:     256388

Transition matrix: 167524 nodes (76 terminal), 515807 minterms, vars: 34r/34c/18nd

Prob0A: 47 iterations in 0.40 seconds (average 0.008426, setup 0.00)

Prob1E: 4 iterations in 0.06 seconds (average 0.014000, setup 0.00)

yes = 949, no = 161222, maybe = 30534

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=192705, nc=53774, nnz=153050, k=2] [2.0 MB]
Creating vector for yes... [1.5 MB]
Allocating iteration vectors... [2 x 1.5 MB]
TOTAL: [6.4 MB]

Starting iterations...

Iterative method: 90 iterations in 0.34 seconds (average 0.001378, setup 0.22)

Value in the initial state: 0.9801791640303706

Time for model checking: 0.832 seconds.

Result: 0.9801791640303706 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmin=? [ F "MissionSuccessful" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=2,Objy1=1,Objx2=4,Objy2=2

Prob0E: 47 iterations in 0.38 seconds (average 0.008170, setup 0.00)

Prob1A: 3 iterations in 0.06 seconds (average 0.018667, setup 0.00)

yes = 949, no = 165627, maybe = 26129

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=192705, nc=45888, nnz=130207, k=2] [1.7 MB]
Creating vector for yes... [1.5 MB]
Allocating iteration vectors... [2 x 1.5 MB]
TOTAL: [6.1 MB]

Starting iterations...

Iterative method: 84 iterations in 0.28 seconds (average 0.001190, setup 0.18)

Value in the initial state: 0.7535097787974302

Time for model checking: 0.741 seconds.

Result: 0.7535097787974302 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmax=? [ F "MissionTimeUp" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=2,Objy1=1,Objx2=4,Objy2=2

Prob0A: 63 iterations in 0.50 seconds (average 0.007873, setup 0.00)

Prob1E: 4 iterations in 0.04 seconds (average 0.010000, setup 0.00)

yes = 364, no = 117785, maybe = 74556

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=192705, nc=128291, nnz=354382, k=4] [4.4 MB]
Creating vector for yes... [1.5 MB]
Allocating iteration vectors... [2 x 1.5 MB]
TOTAL: [8.8 MB]

Starting iterations...

Iterative method: 88 iterations in 0.40 seconds (average 0.001636, setup 0.26)

Value in the initial state: 6.526551157335241E-4

Time for model checking: 0.942 seconds.

Result: 6.526551157335241E-4 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmin=? [ F "MissionTimeUp" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=2,Objy1=1,Objx2=4,Objy2=2

Prob0E: 70 iterations in 0.65 seconds (average 0.009257, setup 0.00)

Prob1A: 3 iterations in 0.05 seconds (average 0.017333, setup 0.00)

yes = 364, no = 118123, maybe = 74218

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=192705, nc=127691, nnz=353112, k=4] [4.3 MB]
Creating vector for yes... [1.5 MB]
Allocating iteration vectors... [2 x 1.5 MB]
TOTAL: [8.8 MB]

Starting iterations...

Iterative method: 103 iterations in 0.42 seconds (average 0.001709, setup 0.24)

Value in the initial state: 2.15574858731566E-6

Time for model checking: 1.15 seconds.

Result: 2.15574858731566E-6 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmax=? [ F "Crash" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=2,Objy1=1,Objx2=4,Objy2=2

Prob0A: 3 iterations in 0.08 seconds (average 0.025333, setup 0.00)

Prob1E: 180 iterations in 2.26 seconds (average 0.012533, setup 0.00)

yes = 94432, no = 2562, maybe = 95711

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=192705, nc=159394, nnz=418813, k=4] [5.1 MB]
Creating vector for yes... [1.5 MB]
Allocating iteration vectors... [2 x 1.5 MB]
TOTAL: [9.5 MB]

Starting iterations...

Iterative method: 86 iterations in 0.44 seconds (average 0.001814, setup 0.28)

Value in the initial state: 0.030806620246606752

Time for model checking: 2.802 seconds.

Result: 0.030806620246606752 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmin=? [ F "Crash" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=2,Objy1=1,Objx2=4,Objy2=2

Prob0E: 3 iterations in 0.05 seconds (average 0.017333, setup 0.00)

Prob1A: 46 iterations in 0.40 seconds (average 0.008609, setup 0.00)

yes = 94432, no = 2562, maybe = 95711

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=192705, nc=159394, nnz=418813, k=4] [5.1 MB]
Creating vector for yes... [1.5 MB]
Allocating iteration vectors... [2 x 1.5 MB]
TOTAL: [9.5 MB]

Starting iterations...

Iterative method: 86 iterations in 0.36 seconds (average 0.001860, setup 0.20)

Value in the initial state: 0.017842867228867635

Time for model checking: 0.831 seconds.

Result: 0.017842867228867635 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmax=? [ F "AllAreaSearched" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=2,Objy1=1,Objx2=4,Objy2=2

Prob0A: 51 iterations in 0.35 seconds (average 0.006824, setup 0.00)

Prob1E: 4 iterations in 0.04 seconds (average 0.010000, setup 0.00)

yes = 1248, no = 147880, maybe = 43577

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=192705, nc=68845, nnz=183331, k=2] [2.3 MB]
Creating vector for yes... [1.5 MB]
Allocating iteration vectors... [2 x 1.5 MB]
TOTAL: [6.8 MB]

Starting iterations...

Iterative method: 85 iterations in 0.30 seconds (average 0.001459, setup 0.18)

Value in the initial state: 0.21876389689374914

Time for model checking: 0.698 seconds.

Result: 0.21876389689374914 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmin=? [ F "AllAreaSearched" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=2,Objy1=1,Objx2=4,Objy2=2

Prob0E: 62 iterations in 0.44 seconds (average 0.007097, setup 0.00)

Prob1A: 3 iterations in 0.03 seconds (average 0.010667, setup 0.00)

yes = 1248, no = 153529, maybe = 37928

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=192705, nc=59065, nnz=155535, k=2] [2.0 MB]
Creating vector for yes... [1.5 MB]
Allocating iteration vectors... [2 x 1.5 MB]
TOTAL: [6.4 MB]

Starting iterations...

Iterative method: 91 iterations in 0.24 seconds (average 0.001363, setup 0.12)

Value in the initial state: 0.001765012496791629

Time for model checking: 0.73 seconds.

Result: 0.001765012496791629 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmax=? [ F "MissionTimeUp"|"AllAreaSearched"|"Crash" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=2,Objy1=1,Objx2=4,Objy2=2

Prob0A: 3 iterations in 0.11 seconds (average 0.036000, setup 0.00)

Prob1E: 184 iterations in 2.03 seconds (average 0.011022, setup 0.00)

yes = 165626, no = 950, maybe = 26129

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=192705, nc=45888, nnz=130207, k=2] [1.7 MB]
Creating vector for yes... [1.5 MB]
Allocating iteration vectors... [2 x 1.5 MB]
TOTAL: [6.1 MB]

Starting iterations...

Iterative method: 78 iterations in 0.26 seconds (average 0.001179, setup 0.17)

Value in the initial state: 0.24648991325597638

Time for model checking: 2.384 seconds.

Result: 0.24648991325597638 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmin=? [ F "MissionTimeUp"|"AllAreaSearched"|"Crash" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=2,Objy1=1,Objx2=4,Objy2=2

Prob0E: 3 iterations in 0.04 seconds (average 0.013333, setup 0.00)

Prob1A: 47 iterations in 0.34 seconds (average 0.007149, setup 0.00)

yes = 161221, no = 950, maybe = 30534

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=192705, nc=53774, nnz=153050, k=2] [2.0 MB]
Creating vector for yes... [1.5 MB]
Allocating iteration vectors... [2 x 1.5 MB]
TOTAL: [6.4 MB]

Starting iterations...

Iterative method: 87 iterations in 0.24 seconds (average 0.001333, setup 0.13)

Value in the initial state: 0.01982036346633776

Time for model checking: 0.647 seconds.

Result: 0.01982036346633776 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"drop"}max=? [ F "deadlock" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=2,Objy1=1,Objx2=4,Objy2=2

Prob0E: 4 iterations in 0.05 seconds (average 0.012000, setup 0.00)

Prob1A: 1 iterations in 0.00 seconds (average 0.004000, setup 0.00)

goal = 1, inf = 0, maybe = 192704

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=192705, nc=256387, nnz=515806, k=4] [7.6 MB]
Building sparse matrix (transition rewards)... [n=192705, nc=256387, nnz=0, k=4] [1.7 MB]
Creating vector for state rewards... [1.5 MB]
Creating vector for inf... [1.5 MB]
Allocating iteration vectors... [2 x 1.5 MB]
TOTAL: [15.2 MB]

Starting iterations...

Iterative method: 86 iterations in 0.60 seconds (average 0.002791, setup 0.36)

Value in the initial state: 0.18199210948402628

Time for model checking: 0.662 seconds.

Result: 0.18199210948402628 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"drop"}min=? [ F "deadlock" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=2,Objy1=1,Objx2=4,Objy2=2

Prob0A: 4 iterations in 0.03 seconds (average 0.008000, setup 0.00)

Prob1E: 5 iterations in 0.04 seconds (average 0.007200, setup 0.00)

Warning: PRISM hasn't checked for zero-reward loops.
Your minimum rewards may be too low...

goal = 1, inf = 0, maybe = 192704

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=192705, nc=256387, nnz=515806, k=4] [7.6 MB]
Building sparse matrix (transition rewards)... [n=192705, nc=256387, nnz=0, k=4] [1.7 MB]
Creating vector for state rewards... [1.5 MB]
Creating vector for inf... [1.5 MB]
Allocating iteration vectors... [2 x 1.5 MB]
TOTAL: [15.2 MB]

Starting iterations...

Iterative method: 78 iterations in 0.49 seconds (average 0.002769, setup 0.27)

Value in the initial state: 0.15693874395337765

Time for model checking: 0.573 seconds.

Result: 0.15693874395337765 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"charge"}max=? [ F "deadlock" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=2,Objy1=1,Objx2=4,Objy2=2

Prob0E: 4 iterations in 0.02 seconds (average 0.005000, setup 0.00)

Prob1A: 1 iterations in 0.00 seconds (average 0.004000, setup 0.00)

goal = 1, inf = 0, maybe = 192704

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=192705, nc=256387, nnz=515806, k=4] [7.6 MB]
Building sparse matrix (transition rewards)... [n=192705, nc=256387, nnz=5786, k=4] [1.8 MB]
Creating vector for state rewards... [1.5 MB]
Creating vector for inf... [1.5 MB]
Allocating iteration vectors... [2 x 1.5 MB]
TOTAL: [15.3 MB]

Starting iterations...

Iterative method: 83 iterations in 0.51 seconds (average 0.002843, setup 0.28)

Value in the initial state: 0.27496987065603046

Time for model checking: 0.549 seconds.

Result: 0.27496987065603046 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"charge"}min=? [ F "deadlock" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=2,Objy1=1,Objx2=4,Objy2=2

Prob0A: 4 iterations in 0.02 seconds (average 0.005000, setup 0.00)

Prob1E: 5 iterations in 0.03 seconds (average 0.006400, setup 0.00)

Warning: PRISM hasn't checked for zero-reward loops.
Your minimum rewards may be too low...

goal = 1, inf = 0, maybe = 192704

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=192705, nc=256387, nnz=515806, k=4] [7.6 MB]
Building sparse matrix (transition rewards)... [n=192705, nc=256387, nnz=5786, k=4] [1.8 MB]
Creating vector for state rewards... [1.5 MB]
Creating vector for inf... [1.5 MB]
Allocating iteration vectors... [2 x 1.5 MB]
TOTAL: [15.3 MB]

Starting iterations...

Iterative method: 91 iterations in 0.54 seconds (average 0.002901, setup 0.27)

Value in the initial state: 0.05188143077409497

Time for model checking: 0.601 seconds.

Result: 0.05188143077409497 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"time"}max=? [ F "deadlock" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=2,Objy1=1,Objx2=4,Objy2=2

Prob0E: 4 iterations in 0.02 seconds (average 0.006000, setup 0.00)

Prob1A: 1 iterations in 0.00 seconds (average 0.004000, setup 0.00)

goal = 1, inf = 0, maybe = 192704

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=192705, nc=256387, nnz=515806, k=4] [7.6 MB]
Building sparse matrix (transition rewards)... [n=192705, nc=256387, nnz=96992, k=4] [2.8 MB]
Creating vector for state rewards... [1.5 MB]
Creating vector for inf... [1.5 MB]
Allocating iteration vectors... [2 x 1.5 MB]
TOTAL: [16.3 MB]

Starting iterations...

Iterative method: 88 iterations in 0.57 seconds (average 0.002909, setup 0.31)

Value in the initial state: 169.97444729879214

Time for model checking: 0.601 seconds.

Result: 169.97444729879214 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"time"}min=? [ F "deadlock" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=2,Objy1=1,Objx2=4,Objy2=2

Prob0A: 4 iterations in 0.02 seconds (average 0.005000, setup 0.00)

Prob1E: 5 iterations in 0.04 seconds (average 0.007200, setup 0.00)

Warning: PRISM hasn't checked for zero-reward loops.
Your minimum rewards may be too low...

goal = 1, inf = 0, maybe = 192704

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=192705, nc=256387, nnz=515806, k=4] [7.6 MB]
Building sparse matrix (transition rewards)... [n=192705, nc=256387, nnz=96992, k=4] [2.8 MB]
Creating vector for state rewards... [1.5 MB]
Creating vector for inf... [1.5 MB]
Allocating iteration vectors... [2 x 1.5 MB]
TOTAL: [16.3 MB]

Starting iterations...

Iterative method: 95 iterations in 0.58 seconds (average 0.002905, setup 0.30)

Value in the initial state: 98.87383189275239

Time for model checking: 0.653 seconds.

Result: 98.87383189275239 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"success"}max=? [ F "deadlock" ]/Pmin=? [ F "MissionSuccessful" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=2,Objy1=1,Objx2=4,Objy2=2

Prob0E: 4 iterations in 0.02 seconds (average 0.006000, setup 0.00)

Prob1A: 1 iterations in 0.01 seconds (average 0.008000, setup 0.00)

goal = 1, inf = 0, maybe = 192704

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=192705, nc=256387, nnz=515806, k=4] [7.6 MB]
Building sparse matrix (transition rewards)... [n=192705, nc=256387, nnz=1870, k=4] [1.7 MB]
Creating vector for state rewards... [1.5 MB]
Creating vector for inf... [1.5 MB]
Allocating iteration vectors... [2 x 1.5 MB]
TOTAL: [15.2 MB]

Starting iterations...

Iterative method: 91 iterations in 0.53 seconds (average 0.002769, setup 0.28)

Prob0E: 47 iterations in 0.30 seconds (average 0.006298, setup 0.00)

Prob1A: 3 iterations in 0.04 seconds (average 0.012000, setup 0.00)

yes = 949, no = 165627, maybe = 26129

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=192705, nc=45888, nnz=130207, k=2] [1.7 MB]
Creating vector for yes... [1.5 MB]
Allocating iteration vectors... [2 x 1.5 MB]
TOTAL: [6.1 MB]

Starting iterations...

Iterative method: 84 iterations in 0.24 seconds (average 0.001286, setup 0.13)

Value in the initial state: 221.00308481168796

Time for model checking: 1.164 seconds.

Result: 221.00308481168796 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"success"}min=? [ F "deadlock" ]/Pmax=? [ F "MissionSuccessful" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=2,Objy1=1,Objx2=4,Objy2=2

Prob0A: 4 iterations in 0.06 seconds (average 0.014000, setup 0.00)

Prob1E: 5 iterations in 0.04 seconds (average 0.008000, setup 0.00)

Warning: PRISM hasn't checked for zero-reward loops.
Your minimum rewards may be too low...

goal = 1, inf = 0, maybe = 192704

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=192705, nc=256387, nnz=515806, k=4] [7.6 MB]
Building sparse matrix (transition rewards)... [n=192705, nc=256387, nnz=1870, k=4] [1.7 MB]
Creating vector for state rewards... [1.5 MB]
Creating vector for inf... [1.5 MB]
Allocating iteration vectors... [2 x 1.5 MB]
TOTAL: [15.2 MB]

Starting iterations...

Iterative method: 88 iterations in 0.54 seconds (average 0.002773, setup 0.29)

Prob0A: 47 iterations in 0.29 seconds (average 0.006213, setup 0.00)

Prob1E: 4 iterations in 0.03 seconds (average 0.007000, setup 0.00)

yes = 949, no = 161222, maybe = 30534

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=192705, nc=53774, nnz=153050, k=2] [2.0 MB]
Creating vector for yes... [1.5 MB]
Allocating iteration vectors... [2 x 1.5 MB]
TOTAL: [6.4 MB]

Starting iterations...

Iterative method: 90 iterations in 0.24 seconds (average 0.001289, setup 0.12)

Value in the initial state: 79.22272576864503

Time for model checking: 1.193 seconds.

Result: 79.22272576864503 (value in the initial state)

---------------------------------------------------------------------

Note: There were 16 warnings during computation.

