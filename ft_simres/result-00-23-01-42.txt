PRISM
=====

Version: 4.3
Date: Tue Aug 02 12:23:25 BST 2016
Hostname: eglinton
Memory limits: cudd=1g, java(heap)=981.5m
Command line: prism uav_sim_ft_mdp.prism -s uav_mdp.props -const 'Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=0,Objy1=1,Objx2=4,Objy2=2,'

Parsing model file "uav_sim_ft_mdp.prism"...

Parsing properties file "uav_mdp.props"...

18 properties:
(1) Pmax=? [ F "MissionSuccessful" ]
(2) Pmin=? [ F "MissionSuccessful" ]
(3) Pmax=? [ F "MissionTimeUp" ]
(4) Pmin=? [ F "MissionTimeUp" ]
(5) Pmax=? [ F "Crash" ]
(6) Pmin=? [ F "Crash" ]
(7) Pmax=? [ F "AllAreaSearched" ]
(8) Pmin=? [ F "AllAreaSearched" ]
(9) Pmax=? [ F "MissionTimeUp"|"AllAreaSearched"|"Crash" ]
(10) Pmin=? [ F "MissionTimeUp"|"AllAreaSearched"|"Crash" ]
(11) R{"drop"}max=? [ F "deadlock" ]
(12) R{"drop"}min=? [ F "deadlock" ]
(13) R{"charge"}max=? [ F "deadlock" ]
(14) R{"charge"}min=? [ F "deadlock" ]
(15) R{"time"}max=? [ F "deadlock" ]
(16) R{"time"}min=? [ F "deadlock" ]
(17) R{"success"}max=? [ F "deadlock" ]/Pmin=? [ F "MissionSuccessful" ]
(18) R{"success"}min=? [ F "deadlock" ]/Pmax=? [ F "MissionSuccessful" ]

Type:        MDP
Modules:     Obj Move Robot T 
Variables:   NoOfObjs posx posy s1 retx rety b t c 

---------------------------------------------------------------------

Model checking: Pmax=? [ F "MissionSuccessful" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=0,Objy1=1,Objx2=4,Objy2=2

Building model...
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=0,Objy1=1,Objx2=4,Objy2=2

Warning: Guard for command 4 of module "Move" is never satisfied.

Warning: Guard for command 7 of module "Move" is never satisfied.

Warning: Guard for command 8 of module "Move" is never satisfied.

Warning: Guard for command 9 of module "Move" is never satisfied.

Warning: Guard for command 7 of module "Robot" is never satisfied.

Warning: Update 2 of command 3 of module "Robot" doesn't do anything ("(s1'=13)", line 249, column 44)

Warning: Update 2 of command 27 of module "T" doesn't do anything ("(c'=1)", line 372, column 108)

Warning: Guard for command 28 of module "T" is never satisfied.

Warning: Update 2 of command 9 of module "T" doesn't do anything ("(c'=1)", line 349, column 112)

Warning: Guard for command 10 of module "T" is never satisfied.

Warning: Update 2 of command 51 of module "T" doesn't do anything ("(c'=1)", line 403, column 113)

Warning: Guard for command 52 of module "T" is never satisfied.

Computing reachable states...

Reachability (BFS): 78 iterations in 0.79 seconds (average 0.010154, setup 0.00)

Time for model construction: 2.358 seconds.

Warning: Deadlocks detected and fixed in 1 states

Type:        MDP
States:      186810 (1 initial)
Transitions: 506136
Choices:     250143

Transition matrix: 165905 nodes (76 terminal), 506136 minterms, vars: 34r/34c/18nd

Prob0A: 47 iterations in 0.29 seconds (average 0.006213, setup 0.00)

Prob1E: 4 iterations in 0.05 seconds (average 0.012000, setup 0.00)

yes = 949, no = 155686, maybe = 30175

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=186810, nc=53732, nnz=153730, k=2] [2.0 MB]
Creating vector for yes... [1.4 MB]
Allocating iteration vectors... [2 x 1.4 MB]
TOTAL: [6.3 MB]

Starting iterations...

Iterative method: 90 iterations in 0.31 seconds (average 0.001244, setup 0.20)

Value in the initial state: 0.9801786592160718

Time for model checking: 0.674 seconds.

Result: 0.9801786592160718 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmin=? [ F "MissionSuccessful" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=0,Objy1=1,Objx2=4,Objy2=2

Prob0E: 47 iterations in 0.35 seconds (average 0.007489, setup 0.00)

Prob1A: 3 iterations in 0.05 seconds (average 0.017333, setup 0.00)

yes = 949, no = 160003, maybe = 25858

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=186810, nc=45894, nnz=130983, k=2] [1.7 MB]
Creating vector for yes... [1.4 MB]
Allocating iteration vectors... [2 x 1.4 MB]
TOTAL: [6.0 MB]

Starting iterations...

Iterative method: 84 iterations in 0.27 seconds (average 0.001238, setup 0.17)

Value in the initial state: 0.7532226493159193

Time for model checking: 0.766 seconds.

Result: 0.7532226493159193 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmax=? [ F "MissionTimeUp" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=0,Objy1=1,Objx2=4,Objy2=2

Prob0A: 64 iterations in 0.51 seconds (average 0.008000, setup 0.00)

Prob1E: 4 iterations in 0.04 seconds (average 0.010000, setup 0.00)

yes = 340, no = 114870, maybe = 71600

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=186810, nc=124976, nnz=347611, k=4] [4.3 MB]
Creating vector for yes... [1.4 MB]
Allocating iteration vectors... [2 x 1.4 MB]
TOTAL: [8.6 MB]

Starting iterations...

Iterative method: 89 iterations in 0.39 seconds (average 0.001663, setup 0.24)

Value in the initial state: 7.276300622080407E-4

Time for model checking: 1.248 seconds.

Result: 7.276300622080407E-4 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmin=? [ F "MissionTimeUp" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=0,Objy1=1,Objx2=4,Objy2=2

Prob0E: 71 iterations in 0.59 seconds (average 0.008338, setup 0.00)

Prob1A: 3 iterations in 0.04 seconds (average 0.014667, setup 0.00)

yes = 340, no = 115208, maybe = 71262

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=186810, nc=124376, nnz=346341, k=4] [4.3 MB]
Creating vector for yes... [1.4 MB]
Allocating iteration vectors... [2 x 1.4 MB]
TOTAL: [8.5 MB]

Starting iterations...

Iterative method: 104 iterations in 0.42 seconds (average 0.001692, setup 0.25)

Value in the initial state: 2.147752169528517E-6

Time for model checking: 1.251 seconds.

Result: 2.147752169528517E-6 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmax=? [ F "Crash" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=0,Objy1=1,Objx2=4,Objy2=2

Prob0A: 3 iterations in 0.10 seconds (average 0.034667, setup 0.00)

Prob1E: 180 iterations in 2.28 seconds (average 0.012689, setup 0.00)

yes = 91505, no = 2538, maybe = 92767

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=186810, nc=156100, nnz=412093, k=4] [5.0 MB]
Creating vector for yes... [1.4 MB]
Allocating iteration vectors... [2 x 1.4 MB]
TOTAL: [9.3 MB]

Starting iterations...

Iterative method: 86 iterations in 0.44 seconds (average 0.001721, setup 0.29)

Value in the initial state: 0.03080900217537491

Time for model checking: 3.453 seconds.

Result: 0.03080900217537491 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmin=? [ F "Crash" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=0,Objy1=1,Objx2=4,Objy2=2

Prob0E: 3 iterations in 0.04 seconds (average 0.014667, setup 0.00)

Prob1A: 47 iterations in 0.39 seconds (average 0.008340, setup 0.00)

yes = 91505, no = 2538, maybe = 92767

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=186810, nc=156100, nnz=412093, k=4] [5.0 MB]
Creating vector for yes... [1.4 MB]
Allocating iteration vectors... [2 x 1.4 MB]
TOTAL: [9.3 MB]

Starting iterations...

Iterative method: 86 iterations in 0.36 seconds (average 0.001860, setup 0.20)

Value in the initial state: 0.017842511715601396

Time for model checking: 0.819 seconds.

Result: 0.017842511715601396 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmax=? [ F "AllAreaSearched" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=0,Objy1=1,Objx2=4,Objy2=2

Prob0A: 49 iterations in 0.34 seconds (average 0.006939, setup 0.00)

Prob1E: 4 iterations in 0.04 seconds (average 0.010000, setup 0.00)

yes = 1248, no = 142359, maybe = 43203

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=186810, nc=68831, nnz=184333, k=2] [2.4 MB]
Creating vector for yes... [1.4 MB]
Allocating iteration vectors... [2 x 1.4 MB]
TOTAL: [6.6 MB]

Starting iterations...

Iterative method: 87 iterations in 0.30 seconds (average 0.001379, setup 0.18)

Value in the initial state: 0.21904066189687216

Time for model checking: 0.761 seconds.

Result: 0.21904066189687216 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmin=? [ F "AllAreaSearched" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=0,Objy1=1,Objx2=4,Objy2=2

Prob0E: 62 iterations in 0.45 seconds (average 0.007290, setup 0.00)

Prob1A: 3 iterations in 0.04 seconds (average 0.012000, setup 0.00)

yes = 1248, no = 147454, maybe = 38108

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=186810, nc=59793, nnz=157835, k=2] [2.0 MB]
Creating vector for yes... [1.4 MB]
Allocating iteration vectors... [2 x 1.4 MB]
TOTAL: [6.3 MB]

Starting iterations...

Iterative method: 91 iterations in 0.26 seconds (average 0.001407, setup 0.14)

Value in the initial state: 0.001764154153216827

Time for model checking: 1.051 seconds.

Result: 0.001764154153216827 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmax=? [ F "MissionTimeUp"|"AllAreaSearched"|"Crash" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=0,Objy1=1,Objx2=4,Objy2=2

Prob0A: 3 iterations in 0.07 seconds (average 0.022667, setup 0.00)

Prob1E: 184 iterations in 1.99 seconds (average 0.010826, setup 0.00)

yes = 160002, no = 950, maybe = 25858

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=186810, nc=45894, nnz=130983, k=2] [1.7 MB]
Creating vector for yes... [1.4 MB]
Allocating iteration vectors... [2 x 1.4 MB]
TOTAL: [6.0 MB]

Starting iterations...

Iterative method: 78 iterations in 0.28 seconds (average 0.001231, setup 0.18)

Value in the initial state: 0.2467770938611747

Time for model checking: 2.881 seconds.

Result: 0.2467770938611747 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmin=? [ F "MissionTimeUp"|"AllAreaSearched"|"Crash" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=0,Objy1=1,Objx2=4,Objy2=2

Prob0E: 3 iterations in 0.04 seconds (average 0.013333, setup 0.00)

Prob1A: 47 iterations in 0.38 seconds (average 0.008085, setup 0.00)

yes = 155685, no = 950, maybe = 30175

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=186810, nc=53732, nnz=153730, k=2] [2.0 MB]
Creating vector for yes... [1.4 MB]
Allocating iteration vectors... [2 x 1.4 MB]
TOTAL: [6.3 MB]

Starting iterations...

Iterative method: 88 iterations in 0.25 seconds (average 0.001273, setup 0.14)

Value in the initial state: 0.01982075064706162

Time for model checking: 0.735 seconds.

Result: 0.01982075064706162 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"drop"}max=? [ F "deadlock" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=0,Objy1=1,Objx2=4,Objy2=2

Prob0E: 4 iterations in 0.06 seconds (average 0.016000, setup 0.00)

Prob1A: 1 iterations in 0.00 seconds (average 0.004000, setup 0.00)

goal = 1, inf = 0, maybe = 186809

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=186810, nc=250142, nnz=506135, k=4] [7.5 MB]
Building sparse matrix (transition rewards)... [n=186810, nc=250142, nnz=0, k=4] [1.7 MB]
Creating vector for state rewards... [1.4 MB]
Creating vector for inf... [1.4 MB]
Allocating iteration vectors... [2 x 1.4 MB]
TOTAL: [14.8 MB]

Starting iterations...

Iterative method: 87 iterations in 0.59 seconds (average 0.002713, setup 0.36)

Value in the initial state: 0.18196968258113533

Time for model checking: 0.751 seconds.

Result: 0.18196968258113533 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"drop"}min=? [ F "deadlock" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=0,Objy1=1,Objx2=4,Objy2=2

Prob0A: 4 iterations in 0.03 seconds (average 0.007000, setup 0.00)

Prob1E: 5 iterations in 0.04 seconds (average 0.008800, setup 0.00)

Warning: PRISM hasn't checked for zero-reward loops.
Your minimum rewards may be too low...

goal = 1, inf = 0, maybe = 186809

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=186810, nc=250142, nnz=506135, k=4] [7.5 MB]
Building sparse matrix (transition rewards)... [n=186810, nc=250142, nnz=0, k=4] [1.7 MB]
Creating vector for state rewards... [1.4 MB]
Creating vector for inf... [1.4 MB]
Allocating iteration vectors... [2 x 1.4 MB]
TOTAL: [14.8 MB]

Starting iterations...

Iterative method: 78 iterations in 0.48 seconds (average 0.002718, setup 0.26)

Value in the initial state: 0.15687318456545762

Time for model checking: 0.695 seconds.

Result: 0.15687318456545762 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"charge"}max=? [ F "deadlock" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=0,Objy1=1,Objx2=4,Objy2=2

Prob0E: 4 iterations in 0.02 seconds (average 0.006000, setup 0.00)

Prob1A: 1 iterations in 0.00 seconds (average 0.004000, setup 0.00)

goal = 1, inf = 0, maybe = 186809

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=186810, nc=250142, nnz=506135, k=4] [7.5 MB]
Building sparse matrix (transition rewards)... [n=186810, nc=250142, nnz=5786, k=4] [1.7 MB]
Creating vector for state rewards... [1.4 MB]
Creating vector for inf... [1.4 MB]
Allocating iteration vectors... [2 x 1.4 MB]
TOTAL: [14.9 MB]

Starting iterations...

Iterative method: 82 iterations in 0.50 seconds (average 0.002780, setup 0.27)

Value in the initial state: 0.27480463565376057

Time for model checking: 0.723 seconds.

Result: 0.27480463565376057 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"charge"}min=? [ F "deadlock" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=0,Objy1=1,Objx2=4,Objy2=2

Prob0A: 4 iterations in 0.02 seconds (average 0.005000, setup 0.00)

Prob1E: 5 iterations in 0.03 seconds (average 0.006400, setup 0.00)

Warning: PRISM hasn't checked for zero-reward loops.
Your minimum rewards may be too low...

goal = 1, inf = 0, maybe = 186809

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=186810, nc=250142, nnz=506135, k=4] [7.5 MB]
Building sparse matrix (transition rewards)... [n=186810, nc=250142, nnz=5786, k=4] [1.7 MB]
Creating vector for state rewards... [1.4 MB]
Creating vector for inf... [1.4 MB]
Allocating iteration vectors... [2 x 1.4 MB]
TOTAL: [14.9 MB]

Starting iterations...

Iterative method: 90 iterations in 0.52 seconds (average 0.002800, setup 0.27)

Value in the initial state: 0.052194174985998196

Time for model checking: 0.74 seconds.

Result: 0.052194174985998196 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"time"}max=? [ F "deadlock" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=0,Objy1=1,Objx2=4,Objy2=2

Prob0E: 4 iterations in 0.02 seconds (average 0.005000, setup 0.00)

Prob1A: 1 iterations in 0.00 seconds (average 0.004000, setup 0.00)

goal = 1, inf = 0, maybe = 186809

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=186810, nc=250142, nnz=506135, k=4] [7.5 MB]
Building sparse matrix (transition rewards)... [n=186810, nc=250142, nnz=94041, k=4] [2.7 MB]
Creating vector for state rewards... [1.4 MB]
Creating vector for inf... [1.4 MB]
Allocating iteration vectors... [2 x 1.4 MB]
TOTAL: [15.9 MB]

Starting iterations...

Iterative method: 89 iterations in 0.56 seconds (average 0.002831, setup 0.30)

Value in the initial state: 169.9807229797778

Time for model checking: 0.663 seconds.

Result: 169.9807229797778 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"time"}min=? [ F "deadlock" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=0,Objy1=1,Objx2=4,Objy2=2

Prob0A: 4 iterations in 0.02 seconds (average 0.005000, setup 0.00)

Prob1E: 5 iterations in 0.04 seconds (average 0.008000, setup 0.00)

Warning: PRISM hasn't checked for zero-reward loops.
Your minimum rewards may be too low...

goal = 1, inf = 0, maybe = 186809

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=186810, nc=250142, nnz=506135, k=4] [7.5 MB]
Building sparse matrix (transition rewards)... [n=186810, nc=250142, nnz=94041, k=4] [2.7 MB]
Creating vector for state rewards... [1.4 MB]
Creating vector for inf... [1.4 MB]
Allocating iteration vectors... [2 x 1.4 MB]
TOTAL: [15.9 MB]

Starting iterations...

Iterative method: 95 iterations in 0.56 seconds (average 0.002779, setup 0.30)

Value in the initial state: 98.87184986175372

Time for model checking: 0.712 seconds.

Result: 98.87184986175372 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"success"}max=? [ F "deadlock" ]/Pmin=? [ F "MissionSuccessful" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=0,Objy1=1,Objx2=4,Objy2=2

Prob0E: 4 iterations in 0.02 seconds (average 0.006000, setup 0.00)

Prob1A: 1 iterations in 0.00 seconds (average 0.004000, setup 0.00)

goal = 1, inf = 0, maybe = 186809

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=186810, nc=250142, nnz=506135, k=4] [7.5 MB]
Building sparse matrix (transition rewards)... [n=186810, nc=250142, nnz=1870, k=4] [1.7 MB]
Creating vector for state rewards... [1.4 MB]
Creating vector for inf... [1.4 MB]
Allocating iteration vectors... [2 x 1.4 MB]
TOTAL: [14.8 MB]

Starting iterations...

Iterative method: 90 iterations in 0.52 seconds (average 0.002711, setup 0.27)

Prob0E: 47 iterations in 0.33 seconds (average 0.007064, setup 0.00)

Prob1A: 3 iterations in 0.05 seconds (average 0.017333, setup 0.00)

yes = 949, no = 160003, maybe = 25858

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=186810, nc=45894, nnz=130983, k=2] [1.7 MB]
Creating vector for yes... [1.4 MB]
Allocating iteration vectors... [2 x 1.4 MB]
TOTAL: [6.0 MB]

Starting iterations...

Iterative method: 84 iterations in 0.23 seconds (average 0.001238, setup 0.13)

Value in the initial state: 221.0940348222312

Time for model checking: 1.294 seconds.

Result: 221.0940348222312 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"success"}min=? [ F "deadlock" ]/Pmax=? [ F "MissionSuccessful" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=0,Objy1=1,Objx2=4,Objy2=2

Prob0A: 4 iterations in 0.03 seconds (average 0.008000, setup 0.00)

Prob1E: 5 iterations in 0.04 seconds (average 0.008000, setup 0.00)

Warning: PRISM hasn't checked for zero-reward loops.
Your minimum rewards may be too low...

goal = 1, inf = 0, maybe = 186809

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=186810, nc=250142, nnz=506135, k=4] [7.5 MB]
Building sparse matrix (transition rewards)... [n=186810, nc=250142, nnz=1870, k=4] [1.7 MB]
Creating vector for state rewards... [1.4 MB]
Creating vector for inf... [1.4 MB]
Allocating iteration vectors... [2 x 1.4 MB]
TOTAL: [14.8 MB]

Starting iterations...

Iterative method: 88 iterations in 0.53 seconds (average 0.002864, setup 0.28)

Prob0A: 47 iterations in 0.31 seconds (average 0.006553, setup 0.00)

Prob1E: 4 iterations in 0.04 seconds (average 0.009000, setup 0.00)

yes = 949, no = 155686, maybe = 30175

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=186810, nc=53732, nnz=153730, k=2] [2.0 MB]
Creating vector for yes... [1.4 MB]
Allocating iteration vectors... [2 x 1.4 MB]
TOTAL: [6.3 MB]

Starting iterations...

Iterative method: 90 iterations in 0.27 seconds (average 0.001333, setup 0.15)

Value in the initial state: 79.1363234587264

Time for model checking: 1.499 seconds.

Result: 79.1363234587264 (value in the initial state)

---------------------------------------------------------------------

Note: There were 17 warnings during computation.

