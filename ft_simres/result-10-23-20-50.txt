PRISM
=====

Version: 4.3
Date: Tue Aug 02 12:38:09 BST 2016
Hostname: eglinton
Memory limits: cudd=1g, java(heap)=981.5m
Command line: prism uav_sim_ft_mdp.prism -s uav_mdp.props -const 'Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=2,Objy1=0,Objx2=5,Objy2=0,'

Parsing model file "uav_sim_ft_mdp.prism"...

Parsing properties file "uav_mdp.props"...

18 properties:
(1) Pmax=? [ F "MissionSuccessful" ]
(2) Pmin=? [ F "MissionSuccessful" ]
(3) Pmax=? [ F "MissionTimeUp" ]
(4) Pmin=? [ F "MissionTimeUp" ]
(5) Pmax=? [ F "Crash" ]
(6) Pmin=? [ F "Crash" ]
(7) Pmax=? [ F "AllAreaSearched" ]
(8) Pmin=? [ F "AllAreaSearched" ]
(9) Pmax=? [ F "MissionTimeUp"|"AllAreaSearched"|"Crash" ]
(10) Pmin=? [ F "MissionTimeUp"|"AllAreaSearched"|"Crash" ]
(11) R{"drop"}max=? [ F "deadlock" ]
(12) R{"drop"}min=? [ F "deadlock" ]
(13) R{"charge"}max=? [ F "deadlock" ]
(14) R{"charge"}min=? [ F "deadlock" ]
(15) R{"time"}max=? [ F "deadlock" ]
(16) R{"time"}min=? [ F "deadlock" ]
(17) R{"success"}max=? [ F "deadlock" ]/Pmin=? [ F "MissionSuccessful" ]
(18) R{"success"}min=? [ F "deadlock" ]/Pmax=? [ F "MissionSuccessful" ]

Type:        MDP
Modules:     Obj Move Robot T 
Variables:   NoOfObjs posx posy s1 retx rety b t c 

---------------------------------------------------------------------

Model checking: Pmax=? [ F "MissionSuccessful" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=2,Objy1=0,Objx2=5,Objy2=0

Building model...
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=2,Objy1=0,Objx2=5,Objy2=0

Warning: Guard for command 7 of module "Move" is never satisfied.

Warning: Guard for command 8 of module "Move" is never satisfied.

Warning: Guard for command 9 of module "Move" is never satisfied.

Warning: Guard for command 7 of module "Robot" is never satisfied.

Warning: Update 2 of command 3 of module "Robot" doesn't do anything ("(s1'=13)", line 249, column 44)

Warning: Update 2 of command 27 of module "T" doesn't do anything ("(c'=1)", line 372, column 108)

Warning: Guard for command 28 of module "T" is never satisfied.

Warning: Update 2 of command 9 of module "T" doesn't do anything ("(c'=1)", line 349, column 112)

Warning: Guard for command 10 of module "T" is never satisfied.

Warning: Update 2 of command 51 of module "T" doesn't do anything ("(c'=1)", line 403, column 113)

Warning: Guard for command 52 of module "T" is never satisfied.

Computing reachable states...

Reachability (BFS): 82 iterations in 1.26 seconds (average 0.015366, setup 0.00)

Time for model construction: 2.529 seconds.

Warning: Deadlocks detected and fixed in 1 states

Type:        MDP
States:      249292 (1 initial)
Transitions: 663832
Choices:     328794

Transition matrix: 175257 nodes (76 terminal), 663832 minterms, vars: 34r/34c/18nd

Prob0A: 43 iterations in 0.38 seconds (average 0.008930, setup 0.00)

Prob1E: 4 iterations in 0.08 seconds (average 0.019000, setup 0.00)

yes = 947, no = 212127, maybe = 36218

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=249292, nc=65432, nnz=191886, k=2] [2.5 MB]
Creating vector for yes... [1.9 MB]
Allocating iteration vectors... [2 x 1.9 MB]
TOTAL: [8.2 MB]

Starting iterations...

Iterative method: 84 iterations in 0.42 seconds (average 0.001810, setup 0.26)

Value in the initial state: 0.9833103320711944

Time for model checking: 0.927 seconds.

Result: 0.9833103320711944 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmin=? [ F "MissionSuccessful" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=2,Objy1=0,Objx2=5,Objy2=0

Prob0E: 45 iterations in 0.40 seconds (average 0.008978, setup 0.00)

Prob1A: 3 iterations in 0.05 seconds (average 0.017333, setup 0.00)

yes = 947, no = 217701, maybe = 30644

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=249292, nc=55126, nnz=161566, k=2] [2.1 MB]
Creating vector for yes... [1.9 MB]
Allocating iteration vectors... [2 x 1.9 MB]
TOTAL: [7.8 MB]

Starting iterations...

Iterative method: 79 iterations in 0.34 seconds (average 0.001570, setup 0.21)

Value in the initial state: 0.9030355248607238

Time for model checking: 0.823 seconds.

Result: 0.9030355248607238 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmax=? [ F "MissionTimeUp" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=2,Objy1=0,Objx2=5,Objy2=0

Prob0A: 56 iterations in 0.50 seconds (average 0.009000, setup 0.00)

Prob1E: 4 iterations in 0.04 seconds (average 0.011000, setup 0.00)

yes = 528, no = 155617, maybe = 93147

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=249292, nc=163097, nnz=456163, k=4] [5.6 MB]
Creating vector for yes... [1.9 MB]
Allocating iteration vectors... [2 x 1.9 MB]
TOTAL: [11.3 MB]

Starting iterations...

Iterative method: 91 iterations in 0.50 seconds (average 0.002198, setup 0.30)

Value in the initial state: 6.581564146629959E-4

Time for model checking: 1.065 seconds.

Result: 6.581564146629959E-4 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmin=? [ F "MissionTimeUp" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=2,Objy1=0,Objx2=5,Objy2=0

Prob0E: 63 iterations in 0.68 seconds (average 0.010857, setup 0.00)

Prob1A: 3 iterations in 0.07 seconds (average 0.022667, setup 0.00)

yes = 528, no = 156031, maybe = 92733

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=249292, nc=162345, nnz=454737, k=4] [5.6 MB]
Creating vector for yes... [1.9 MB]
Allocating iteration vectors... [2 x 1.9 MB]
TOTAL: [11.3 MB]

Starting iterations...

Iterative method: 107 iterations in 0.54 seconds (average 0.002280, setup 0.30)

Value in the initial state: 1.8805294725642866E-6

Time for model checking: 1.287 seconds.

Result: 1.8805294725642866E-6 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmax=? [ F "Crash" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=2,Objy1=0,Objx2=5,Objy2=0

Prob0A: 3 iterations in 0.10 seconds (average 0.034667, setup 0.00)

Prob1E: 179 iterations in 2.69 seconds (average 0.015039, setup 0.00)

yes = 122025, no = 2670, maybe = 124597

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=249292, nc=204099, nnz=539137, k=4] [6.6 MB]
Creating vector for yes... [1.9 MB]
Allocating iteration vectors... [2 x 1.9 MB]
TOTAL: [12.3 MB]

Starting iterations...

Iterative method: 89 iterations in 0.58 seconds (average 0.002382, setup 0.36)

Value in the initial state: 0.026894989824710325

Time for model checking: 3.404 seconds.

Result: 0.026894989824710325 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmin=? [ F "Crash" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=2,Objy1=0,Objx2=5,Objy2=0

Prob0E: 3 iterations in 0.07 seconds (average 0.024000, setup 0.00)

Prob1A: 39 iterations in 0.63 seconds (average 0.016103, setup 0.00)

yes = 122025, no = 2670, maybe = 124597

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=249292, nc=204099, nnz=539137, k=4] [6.6 MB]
Creating vector for yes... [1.9 MB]
Allocating iteration vectors... [2 x 1.9 MB]
TOTAL: [12.3 MB]

Starting iterations...

Iterative method: 82 iterations in 0.52 seconds (average 0.002488, setup 0.32)

Value in the initial state: 0.014649565699406293

Time for model checking: 1.238 seconds.

Result: 0.014649565699406293 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmax=? [ F "AllAreaSearched" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=2,Objy1=0,Objx2=5,Objy2=0

Prob0A: 62 iterations in 0.45 seconds (average 0.007226, setup 0.00)

Prob1E: 4 iterations in 0.04 seconds (average 0.010000, setup 0.00)

yes = 1194, no = 199852, maybe = 48246

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=249292, nc=70171, nnz=179534, k=2] [2.4 MB]
Creating vector for yes... [1.9 MB]
Allocating iteration vectors... [2 x 1.9 MB]
TOTAL: [8.1 MB]

Starting iterations...

Iterative method: 92 iterations in 0.34 seconds (average 0.001957, setup 0.16)

Value in the initial state: 0.07700587885902616

Time for model checking: 0.852 seconds.

Result: 0.07700587885902616 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmin=? [ F "AllAreaSearched" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=2,Objy1=0,Objx2=5,Objy2=0

Prob0E: 66 iterations in 0.45 seconds (average 0.006788, setup 0.00)

Prob1A: 3 iterations in 0.03 seconds (average 0.010667, setup 0.00)

yes = 1194, no = 205289, maybe = 42809

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=249292, nc=60316, nnz=150906, k=2] [2.0 MB]
Creating vector for yes... [1.9 MB]
Allocating iteration vectors... [2 x 1.9 MB]
TOTAL: [7.7 MB]

Starting iterations...

Iterative method: 93 iterations in 0.28 seconds (average 0.001763, setup 0.12)

Value in the initial state: 0.0018119028711014828

Time for model checking: 0.778 seconds.

Result: 0.0018119028711014828 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmax=? [ F "MissionTimeUp"|"AllAreaSearched"|"Crash" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=2,Objy1=0,Objx2=5,Objy2=0

Prob0A: 3 iterations in 0.10 seconds (average 0.033333, setup 0.00)

Prob1E: 176 iterations in 2.43 seconds (average 0.013795, setup 0.00)

yes = 217700, no = 948, maybe = 30644

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=249292, nc=55126, nnz=161566, k=2] [2.1 MB]
Creating vector for yes... [1.9 MB]
Allocating iteration vectors... [2 x 1.9 MB]
TOTAL: [7.8 MB]

Starting iterations...

Iterative method: 73 iterations in 0.32 seconds (average 0.001589, setup 0.21)

Value in the initial state: 0.09696419710982926

Time for model checking: 2.888 seconds.

Result: 0.09696419710982926 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmin=? [ F "MissionTimeUp"|"AllAreaSearched"|"Crash" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=2,Objy1=0,Objx2=5,Objy2=0

Prob0E: 3 iterations in 0.17 seconds (average 0.056000, setup 0.00)

Prob1A: 43 iterations in 0.35 seconds (average 0.008093, setup 0.00)

yes = 212126, no = 948, maybe = 36218

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=249292, nc=65432, nnz=191886, k=2] [2.5 MB]
Creating vector for yes... [1.9 MB]
Allocating iteration vectors... [2 x 1.9 MB]
TOTAL: [8.2 MB]

Starting iterations...

Iterative method: 82 iterations in 0.32 seconds (average 0.001659, setup 0.18)

Value in the initial state: 0.016688739611170563

Time for model checking: 0.851 seconds.

Result: 0.016688739611170563 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"drop"}max=? [ F "deadlock" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=2,Objy1=0,Objx2=5,Objy2=0

Prob0E: 4 iterations in 0.06 seconds (average 0.015000, setup 0.00)

Prob1A: 1 iterations in 0.01 seconds (average 0.008000, setup 0.00)

goal = 1, inf = 0, maybe = 249291

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=249292, nc=328793, nnz=663831, k=4] [9.8 MB]
Building sparse matrix (transition rewards)... [n=249292, nc=328793, nnz=0, k=4] [2.2 MB]
Creating vector for state rewards... [1.9 MB]
Creating vector for inf... [1.9 MB]
Allocating iteration vectors... [2 x 1.9 MB]
TOTAL: [19.6 MB]

Starting iterations...

Iterative method: 80 iterations in 0.73 seconds (average 0.003700, setup 0.43)

Value in the initial state: 0.23832491800720107

Time for model checking: 0.808 seconds.

Result: 0.23832491800720107 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"drop"}min=? [ F "deadlock" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=2,Objy1=0,Objx2=5,Objy2=0

Prob0A: 4 iterations in 0.03 seconds (average 0.008000, setup 0.00)

Prob1E: 5 iterations in 0.04 seconds (average 0.008000, setup 0.00)

Warning: PRISM hasn't checked for zero-reward loops.
Your minimum rewards may be too low...

goal = 1, inf = 0, maybe = 249291

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=249292, nc=328793, nnz=663831, k=4] [9.8 MB]
Building sparse matrix (transition rewards)... [n=249292, nc=328793, nnz=0, k=4] [2.2 MB]
Creating vector for state rewards... [1.9 MB]
Creating vector for inf... [1.9 MB]
Allocating iteration vectors... [2 x 1.9 MB]
TOTAL: [19.6 MB]

Starting iterations...

Iterative method: 71 iterations in 0.60 seconds (average 0.003718, setup 0.34)

Value in the initial state: 0.2148844480406268

Time for model checking: 0.698 seconds.

Result: 0.2148844480406268 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"charge"}max=? [ F "deadlock" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=2,Objy1=0,Objx2=5,Objy2=0

Prob0E: 4 iterations in 0.02 seconds (average 0.006000, setup 0.00)

Prob1A: 1 iterations in 0.00 seconds (average 0.004000, setup 0.00)

goal = 1, inf = 0, maybe = 249291

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=249292, nc=328793, nnz=663831, k=4] [9.8 MB]
Building sparse matrix (transition rewards)... [n=249292, nc=328793, nnz=7514, k=4] [2.3 MB]
Creating vector for state rewards... [1.9 MB]
Creating vector for inf... [1.9 MB]
Allocating iteration vectors... [2 x 1.9 MB]
TOTAL: [19.7 MB]

Starting iterations...

Iterative method: 75 iterations in 0.63 seconds (average 0.003733, setup 0.35)

Value in the initial state: 0.31134822592866795

Time for model checking: 0.668 seconds.

Result: 0.31134822592866795 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"charge"}min=? [ F "deadlock" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=2,Objy1=0,Objx2=5,Objy2=0

Prob0A: 4 iterations in 0.02 seconds (average 0.006000, setup 0.00)

Prob1E: 5 iterations in 0.04 seconds (average 0.008000, setup 0.00)

Warning: PRISM hasn't checked for zero-reward loops.
Your minimum rewards may be too low...

goal = 1, inf = 0, maybe = 249291

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=249292, nc=328793, nnz=663831, k=4] [9.8 MB]
Building sparse matrix (transition rewards)... [n=249292, nc=328793, nnz=7514, k=4] [2.3 MB]
Creating vector for state rewards... [1.9 MB]
Creating vector for inf... [1.9 MB]
Allocating iteration vectors... [2 x 1.9 MB]
TOTAL: [19.7 MB]

Starting iterations...

Iterative method: 87 iterations in 0.68 seconds (average 0.003770, setup 0.35)

Value in the initial state: 0.021152416165091052

Time for model checking: 0.748 seconds.

Result: 0.021152416165091052 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"time"}max=? [ F "deadlock" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=2,Objy1=0,Objx2=5,Objy2=0

Prob0E: 4 iterations in 0.02 seconds (average 0.006000, setup 0.00)

Prob1A: 1 iterations in 0.00 seconds (average 0.004000, setup 0.00)

goal = 1, inf = 0, maybe = 249291

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=249292, nc=328793, nnz=663831, k=4] [9.8 MB]
Building sparse matrix (transition rewards)... [n=249292, nc=328793, nnz=124693, k=4] [3.6 MB]
Creating vector for state rewards... [1.9 MB]
Creating vector for inf... [1.9 MB]
Allocating iteration vectors... [2 x 1.9 MB]
TOTAL: [21.0 MB]

Starting iterations...

Iterative method: 91 iterations in 0.72 seconds (average 0.003648, setup 0.39)

Value in the initial state: 148.4670536711332

Time for model checking: 0.778 seconds.

Result: 148.4670536711332 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"time"}min=? [ F "deadlock" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=2,Objy1=0,Objx2=5,Objy2=0

Prob0A: 4 iterations in 0.02 seconds (average 0.005000, setup 0.00)

Prob1E: 5 iterations in 0.04 seconds (average 0.008000, setup 0.00)

Warning: PRISM hasn't checked for zero-reward loops.
Your minimum rewards may be too low...

goal = 1, inf = 0, maybe = 249291

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=249292, nc=328793, nnz=663831, k=4] [9.8 MB]
Building sparse matrix (transition rewards)... [n=249292, nc=328793, nnz=124693, k=4] [3.6 MB]
Creating vector for state rewards... [1.9 MB]
Creating vector for inf... [1.9 MB]
Allocating iteration vectors... [2 x 1.9 MB]
TOTAL: [21.0 MB]

Starting iterations...

Iterative method: 93 iterations in 0.75 seconds (average 0.003871, setup 0.39)

Value in the initial state: 81.26482918080113

Time for model checking: 0.822 seconds.

Result: 81.26482918080113 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"success"}max=? [ F "deadlock" ]/Pmin=? [ F "MissionSuccessful" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=2,Objy1=0,Objx2=5,Objy2=0

Prob0E: 4 iterations in 0.02 seconds (average 0.006000, setup 0.00)

Prob1A: 1 iterations in 0.01 seconds (average 0.008000, setup 0.00)

goal = 1, inf = 0, maybe = 249291

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=249292, nc=328793, nnz=663831, k=4] [9.8 MB]
Building sparse matrix (transition rewards)... [n=249292, nc=328793, nnz=1866, k=4] [2.2 MB]
Creating vector for state rewards... [1.9 MB]
Creating vector for inf... [1.9 MB]
Allocating iteration vectors... [2 x 1.9 MB]
TOTAL: [19.6 MB]

Starting iterations...

Iterative method: 85 iterations in 0.67 seconds (average 0.003671, setup 0.36)

Prob0E: 45 iterations in 0.30 seconds (average 0.006578, setup 0.00)

Prob1A: 3 iterations in 0.04 seconds (average 0.013333, setup 0.00)

yes = 947, no = 217701, maybe = 30644

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=249292, nc=55126, nnz=161566, k=2] [2.1 MB]
Creating vector for yes... [1.9 MB]
Allocating iteration vectors... [2 x 1.9 MB]
TOTAL: [7.8 MB]

Starting iterations...

Iterative method: 79 iterations in 0.30 seconds (average 0.001620, setup 0.17)

Value in the initial state: 161.1585672015915

Time for model checking: 1.355 seconds.

Result: 161.1585672015915 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"success"}min=? [ F "deadlock" ]/Pmax=? [ F "MissionSuccessful" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=2,Objy1=0,Objx2=5,Objy2=0

Prob0A: 4 iterations in 0.06 seconds (average 0.014000, setup 0.00)

Prob1E: 5 iterations in 0.04 seconds (average 0.008800, setup 0.00)

Warning: PRISM hasn't checked for zero-reward loops.
Your minimum rewards may be too low...

goal = 1, inf = 0, maybe = 249291

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=249292, nc=328793, nnz=663831, k=4] [9.8 MB]
Building sparse matrix (transition rewards)... [n=249292, nc=328793, nnz=1866, k=4] [2.2 MB]
Creating vector for state rewards... [1.9 MB]
Creating vector for inf... [1.9 MB]
Allocating iteration vectors... [2 x 1.9 MB]
TOTAL: [19.6 MB]

Starting iterations...

Iterative method: 84 iterations in 0.68 seconds (average 0.003667, setup 0.38)

Prob0A: 43 iterations in 0.27 seconds (average 0.006233, setup 0.00)

Prob1E: 4 iterations in 0.04 seconds (average 0.009000, setup 0.00)

yes = 947, no = 212127, maybe = 36218

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=249292, nc=65432, nnz=191886, k=2] [2.5 MB]
Creating vector for yes... [1.9 MB]
Allocating iteration vectors... [2 x 1.9 MB]
TOTAL: [8.2 MB]

Starting iterations...

Iterative method: 84 iterations in 0.30 seconds (average 0.001667, setup 0.16)

Value in the initial state: 74.21393251195286

Time for model checking: 1.398 seconds.

Result: 74.21393251195286 (value in the initial state)

---------------------------------------------------------------------

Note: There were 16 warnings during computation.

