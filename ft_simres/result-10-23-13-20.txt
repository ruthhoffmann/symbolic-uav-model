PRISM
=====

Version: 4.3
Date: Tue Aug 02 12:37:42 BST 2016
Hostname: eglinton
Memory limits: cudd=1g, java(heap)=981.5m
Command line: prism uav_sim_ft_mdp.prism -s uav_mdp.props -const 'Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=1,Objy1=3,Objx2=2,Objy2=0,'

Parsing model file "uav_sim_ft_mdp.prism"...

Parsing properties file "uav_mdp.props"...

18 properties:
(1) Pmax=? [ F "MissionSuccessful" ]
(2) Pmin=? [ F "MissionSuccessful" ]
(3) Pmax=? [ F "MissionTimeUp" ]
(4) Pmin=? [ F "MissionTimeUp" ]
(5) Pmax=? [ F "Crash" ]
(6) Pmin=? [ F "Crash" ]
(7) Pmax=? [ F "AllAreaSearched" ]
(8) Pmin=? [ F "AllAreaSearched" ]
(9) Pmax=? [ F "MissionTimeUp"|"AllAreaSearched"|"Crash" ]
(10) Pmin=? [ F "MissionTimeUp"|"AllAreaSearched"|"Crash" ]
(11) R{"drop"}max=? [ F "deadlock" ]
(12) R{"drop"}min=? [ F "deadlock" ]
(13) R{"charge"}max=? [ F "deadlock" ]
(14) R{"charge"}min=? [ F "deadlock" ]
(15) R{"time"}max=? [ F "deadlock" ]
(16) R{"time"}min=? [ F "deadlock" ]
(17) R{"success"}max=? [ F "deadlock" ]/Pmin=? [ F "MissionSuccessful" ]
(18) R{"success"}min=? [ F "deadlock" ]/Pmax=? [ F "MissionSuccessful" ]

Type:        MDP
Modules:     Obj Move Robot T 
Variables:   NoOfObjs posx posy s1 retx rety b t c 

---------------------------------------------------------------------

Model checking: Pmax=? [ F "MissionSuccessful" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=1,Objy1=3,Objx2=2,Objy2=0

Building model...
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=1,Objy1=3,Objx2=2,Objy2=0

Warning: Guard for command 7 of module "Move" is never satisfied.

Warning: Guard for command 8 of module "Move" is never satisfied.

Warning: Guard for command 9 of module "Move" is never satisfied.

Warning: Guard for command 7 of module "Robot" is never satisfied.

Warning: Update 2 of command 3 of module "Robot" doesn't do anything ("(s1'=13)", line 249, column 44)

Warning: Update 2 of command 27 of module "T" doesn't do anything ("(c'=1)", line 372, column 108)

Warning: Guard for command 28 of module "T" is never satisfied.

Warning: Update 2 of command 9 of module "T" doesn't do anything ("(c'=1)", line 349, column 112)

Warning: Guard for command 10 of module "T" is never satisfied.

Warning: Update 2 of command 51 of module "T" doesn't do anything ("(c'=1)", line 403, column 113)

Warning: Guard for command 52 of module "T" is never satisfied.

Computing reachable states...

Reachability (BFS): 81 iterations in 1.02 seconds (average 0.012543, setup 0.00)

Time for model construction: 2.322 seconds.

Warning: Deadlocks detected and fixed in 1 states

Type:        MDP
States:      205609 (1 initial)
Transitions: 522592
Choices:     266830

Transition matrix: 173909 nodes (76 terminal), 522592 minterms, vars: 34r/34c/18nd

Prob0A: 62 iterations in 0.57 seconds (average 0.009226, setup 0.00)

Prob1E: 4 iterations in 0.06 seconds (average 0.016000, setup 0.00)

yes = 948, no = 176859, maybe = 27802

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=205609, nc=46009, nnz=125095, k=2] [1.7 MB]
Creating vector for yes... [1.6 MB]
Allocating iteration vectors... [2 x 1.6 MB]
TOTAL: [6.4 MB]

Starting iterations...

Iterative method: 98 iterations in 0.35 seconds (average 0.001306, setup 0.22)

Value in the initial state: 0.9704084749086723

Time for model checking: 1.036 seconds.

Result: 0.9704084749086723 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmin=? [ F "MissionSuccessful" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=1,Objy1=3,Objx2=2,Objy2=0

Prob0E: 68 iterations in 0.52 seconds (average 0.007706, setup 0.00)

Prob1A: 3 iterations in 0.04 seconds (average 0.014667, setup 0.00)

yes = 948, no = 180702, maybe = 23959

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=205609, nc=40067, nnz=109183, k=2] [1.5 MB]
Creating vector for yes... [1.6 MB]
Allocating iteration vectors... [2 x 1.6 MB]
TOTAL: [6.2 MB]

Starting iterations...

Iterative method: 87 iterations in 0.28 seconds (average 0.001379, setup 0.16)

Value in the initial state: 0.9331301886720418

Time for model checking: 0.871 seconds.

Result: 0.9331301886720418 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmax=? [ F "MissionTimeUp" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=1,Objy1=3,Objx2=2,Objy2=0

Prob0A: 65 iterations in 0.62 seconds (average 0.009538, setup 0.00)

Prob1E: 4 iterations in 0.05 seconds (average 0.012000, setup 0.00)

yes = 456, no = 116471, maybe = 88682

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=205609, nc=139242, nnz=367629, k=4] [4.5 MB]
Creating vector for yes... [1.6 MB]
Allocating iteration vectors... [2 x 1.6 MB]
TOTAL: [9.2 MB]

Starting iterations...

Iterative method: 94 iterations in 0.45 seconds (average 0.001830, setup 0.28)

Value in the initial state: 0.0019236168089686741

Time for model checking: 1.147 seconds.

Result: 0.0019236168089686741 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmin=? [ F "MissionTimeUp" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=1,Objy1=3,Objx2=2,Objy2=0

Prob0E: 71 iterations in 0.88 seconds (average 0.012451, setup 0.00)

Prob1A: 3 iterations in 0.04 seconds (average 0.013333, setup 0.00)

yes = 456, no = 116548, maybe = 88605

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=205609, nc=139121, nnz=367266, k=4] [4.5 MB]
Creating vector for yes... [1.6 MB]
Allocating iteration vectors... [2 x 1.6 MB]
TOTAL: [9.2 MB]

Starting iterations...

Iterative method: 109 iterations in 0.44 seconds (average 0.001872, setup 0.24)

Value in the initial state: 7.016629864119725E-6

Time for model checking: 1.391 seconds.

Result: 7.016629864119725E-6 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmax=? [ F "Crash" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=1,Objy1=3,Objx2=2,Objy2=0

Prob0A: 3 iterations in 0.06 seconds (average 0.020000, setup 0.00)

Prob1E: 232 iterations in 2.97 seconds (average 0.012793, setup 0.00)

yes = 100883, no = 2705, maybe = 102021

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=205609, nc=163242, nnz=419004, k=4] [5.1 MB]
Creating vector for yes... [1.6 MB]
Allocating iteration vectors... [2 x 1.6 MB]
TOTAL: [9.9 MB]

Starting iterations...

Iterative method: 93 iterations in 0.50 seconds (average 0.001935, setup 0.32)

Value in the initial state: 0.06302080585667615

Time for model checking: 3.566 seconds.

Result: 0.06302080585667615 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmin=? [ F "Crash" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=1,Objy1=3,Objx2=2,Objy2=0

Prob0E: 3 iterations in 0.06 seconds (average 0.020000, setup 0.00)

Prob1A: 52 iterations in 0.70 seconds (average 0.013385, setup 0.00)

yes = 100883, no = 2705, maybe = 102021

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=205609, nc=163242, nnz=419004, k=4] [5.1 MB]
Creating vector for yes... [1.6 MB]
Allocating iteration vectors... [2 x 1.6 MB]
TOTAL: [9.9 MB]

Starting iterations...

Iterative method: 95 iterations in 0.46 seconds (average 0.001937, setup 0.28)

Value in the initial state: 0.027739602749899818

Time for model checking: 1.244 seconds.

Result: 0.027739602749899818 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmax=? [ F "AllAreaSearched" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=1,Objy1=3,Objx2=2,Objy2=0

Prob0A: 59 iterations in 0.42 seconds (average 0.007119, setup 0.00)

Prob1E: 4 iterations in 0.04 seconds (average 0.011000, setup 0.00)

yes = 1300, no = 160199, maybe = 44110

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=205609, nc=69871, nnz=182691, k=2] [2.4 MB]
Creating vector for yes... [1.6 MB]
Allocating iteration vectors... [2 x 1.6 MB]
TOTAL: [7.1 MB]

Starting iterations...

Iterative method: 94 iterations in 0.31 seconds (average 0.001532, setup 0.17)

Value in the initial state: 0.003009657475658844

Time for model checking: 0.771 seconds.

Result: 0.003009657475658844 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmin=? [ F "AllAreaSearched" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=1,Objy1=3,Objx2=2,Objy2=0

Prob0E: 68 iterations in 0.45 seconds (average 0.006588, setup 0.00)

Prob1A: 3 iterations in 0.03 seconds (average 0.010667, setup 0.00)

yes = 1300, no = 165243, maybe = 39066

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=205609, nc=62380, nnz=163139, k=2] [2.1 MB]
Creating vector for yes... [1.6 MB]
Allocating iteration vectors... [2 x 1.6 MB]
TOTAL: [6.8 MB]

Starting iterations...

Iterative method: 89 iterations in 0.27 seconds (average 0.001438, setup 0.14)

Value in the initial state: 0.0012523104927565094

Time for model checking: 0.768 seconds.

Result: 0.0012523104927565094 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmax=? [ F "MissionTimeUp"|"AllAreaSearched"|"Crash" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=1,Objy1=3,Objx2=2,Objy2=0

Prob0A: 3 iterations in 0.09 seconds (average 0.029333, setup 0.00)

Prob1E: 264 iterations in 3.46 seconds (average 0.013106, setup 0.00)

yes = 180701, no = 949, maybe = 23959

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=205609, nc=40067, nnz=109183, k=2] [1.5 MB]
Creating vector for yes... [1.6 MB]
Allocating iteration vectors... [2 x 1.6 MB]
TOTAL: [6.2 MB]

Starting iterations...

Iterative method: 85 iterations in 0.28 seconds (average 0.001318, setup 0.17)

Value in the initial state: 0.06686952752994549

Time for model checking: 3.873 seconds.

Result: 0.06686952752994549 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmin=? [ F "MissionTimeUp"|"AllAreaSearched"|"Crash" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=1,Objy1=3,Objx2=2,Objy2=0

Prob0E: 3 iterations in 0.05 seconds (average 0.016000, setup 0.00)

Prob1A: 62 iterations in 0.62 seconds (average 0.010000, setup 0.00)

yes = 176858, no = 949, maybe = 27802

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=205609, nc=46009, nnz=125095, k=2] [1.7 MB]
Creating vector for yes... [1.6 MB]
Allocating iteration vectors... [2 x 1.6 MB]
TOTAL: [6.4 MB]

Starting iterations...

Iterative method: 96 iterations in 0.28 seconds (average 0.001292, setup 0.16)

Value in the initial state: 0.02959131267300957

Time for model checking: 0.993 seconds.

Result: 0.02959131267300957 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"drop"}max=? [ F "deadlock" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=1,Objy1=3,Objx2=2,Objy2=0

Prob0E: 4 iterations in 0.08 seconds (average 0.019000, setup 0.00)

Prob1A: 1 iterations in 0.01 seconds (average 0.008000, setup 0.00)

goal = 1, inf = 0, maybe = 205608

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=205609, nc=266829, nnz=522591, k=4] [7.8 MB]
Building sparse matrix (transition rewards)... [n=205609, nc=266829, nnz=0, k=4] [1.8 MB]
Creating vector for state rewards... [1.6 MB]
Creating vector for inf... [1.6 MB]
Allocating iteration vectors... [2 x 1.6 MB]
TOTAL: [15.9 MB]

Starting iterations...

Iterative method: 93 iterations in 0.66 seconds (average 0.002882, setup 0.39)

Value in the initial state: 0.20760482071482309

Time for model checking: 0.759 seconds.

Result: 0.20760482071482309 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"drop"}min=? [ F "deadlock" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=1,Objy1=3,Objx2=2,Objy2=0

Prob0A: 4 iterations in 0.04 seconds (average 0.010000, setup 0.00)

Prob1E: 5 iterations in 0.04 seconds (average 0.008800, setup 0.00)

Warning: PRISM hasn't checked for zero-reward loops.
Your minimum rewards may be too low...

goal = 1, inf = 0, maybe = 205608

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=205609, nc=266829, nnz=522591, k=4] [7.8 MB]
Building sparse matrix (transition rewards)... [n=205609, nc=266829, nnz=0, k=4] [1.8 MB]
Creating vector for state rewards... [1.6 MB]
Creating vector for inf... [1.6 MB]
Allocating iteration vectors... [2 x 1.6 MB]
TOTAL: [15.9 MB]

Starting iterations...

Iterative method: 85 iterations in 0.54 seconds (average 0.002965, setup 0.29)

Value in the initial state: 0.1636078411175685

Time for model checking: 0.637 seconds.

Result: 0.1636078411175685 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"charge"}max=? [ F "deadlock" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=1,Objy1=3,Objx2=2,Objy2=0

Prob0E: 4 iterations in 0.03 seconds (average 0.007000, setup 0.00)

Prob1A: 1 iterations in 0.00 seconds (average 0.004000, setup 0.00)

goal = 1, inf = 0, maybe = 205608

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=205609, nc=266829, nnz=522591, k=4] [7.8 MB]
Building sparse matrix (transition rewards)... [n=205609, nc=266829, nnz=5402, k=4] [1.9 MB]
Creating vector for state rewards... [1.6 MB]
Creating vector for inf... [1.6 MB]
Allocating iteration vectors... [2 x 1.6 MB]
TOTAL: [15.9 MB]

Starting iterations...

Iterative method: 91 iterations in 0.56 seconds (average 0.002989, setup 0.28)

Value in the initial state: 0.9752364961232602

Time for model checking: 0.602 seconds.

Result: 0.9752364961232602 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"charge"}min=? [ F "deadlock" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=1,Objy1=3,Objx2=2,Objy2=0

Prob0A: 4 iterations in 0.02 seconds (average 0.006000, setup 0.00)

Prob1E: 5 iterations in 0.04 seconds (average 0.008000, setup 0.00)

Warning: PRISM hasn't checked for zero-reward loops.
Your minimum rewards may be too low...

goal = 1, inf = 0, maybe = 205608

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=205609, nc=266829, nnz=522591, k=4] [7.8 MB]
Building sparse matrix (transition rewards)... [n=205609, nc=266829, nnz=5402, k=4] [1.9 MB]
Creating vector for state rewards... [1.6 MB]
Creating vector for inf... [1.6 MB]
Allocating iteration vectors... [2 x 1.6 MB]
TOTAL: [15.9 MB]

Starting iterations...

Iterative method: 95 iterations in 0.56 seconds (average 0.002905, setup 0.29)

Value in the initial state: 0.2122744506733327

Time for model checking: 0.645 seconds.

Result: 0.2122744506733327 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"time"}max=? [ F "deadlock" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=1,Objy1=3,Objx2=2,Objy2=0

Prob0E: 4 iterations in 0.02 seconds (average 0.006000, setup 0.00)

Prob1A: 1 iterations in 0.00 seconds (average 0.004000, setup 0.00)

goal = 1, inf = 0, maybe = 205608

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=205609, nc=266829, nnz=522591, k=4] [7.8 MB]
Building sparse matrix (transition rewards)... [n=205609, nc=266829, nnz=103586, k=4] [3.0 MB]
Creating vector for state rewards... [1.6 MB]
Creating vector for inf... [1.6 MB]
Allocating iteration vectors... [2 x 1.6 MB]
TOTAL: [17.0 MB]

Starting iterations...

Iterative method: 96 iterations in 0.61 seconds (average 0.003000, setup 0.32)

Value in the initial state: 346.128620897427

Time for model checking: 0.648 seconds.

Result: 346.128620897427 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"time"}min=? [ F "deadlock" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=1,Objy1=3,Objx2=2,Objy2=0

Prob0A: 4 iterations in 0.02 seconds (average 0.005000, setup 0.00)

Prob1E: 5 iterations in 0.04 seconds (average 0.007200, setup 0.00)

Warning: PRISM hasn't checked for zero-reward loops.
Your minimum rewards may be too low...

goal = 1, inf = 0, maybe = 205608

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=205609, nc=266829, nnz=522591, k=4] [7.8 MB]
Building sparse matrix (transition rewards)... [n=205609, nc=266829, nnz=103586, k=4] [3.0 MB]
Creating vector for state rewards... [1.6 MB]
Creating vector for inf... [1.6 MB]
Allocating iteration vectors... [2 x 1.6 MB]
TOTAL: [17.0 MB]

Starting iterations...

Iterative method: 104 iterations in 0.64 seconds (average 0.003077, setup 0.32)

Value in the initial state: 153.1404091537752

Time for model checking: 0.703 seconds.

Result: 153.1404091537752 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"success"}max=? [ F "deadlock" ]/Pmin=? [ F "MissionSuccessful" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=1,Objy1=3,Objx2=2,Objy2=0

Prob0E: 4 iterations in 0.02 seconds (average 0.005000, setup 0.00)

Prob1A: 1 iterations in 0.00 seconds (average 0.004000, setup 0.00)

goal = 1, inf = 0, maybe = 205608

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=205609, nc=266829, nnz=522591, k=4] [7.8 MB]
Building sparse matrix (transition rewards)... [n=205609, nc=266829, nnz=1868, k=4] [1.8 MB]
Creating vector for state rewards... [1.6 MB]
Creating vector for inf... [1.6 MB]
Allocating iteration vectors... [2 x 1.6 MB]
TOTAL: [15.9 MB]

Starting iterations...

Iterative method: 97 iterations in 0.55 seconds (average 0.002887, setup 0.27)

Prob0E: 68 iterations in 0.46 seconds (average 0.006706, setup 0.00)

Prob1A: 3 iterations in 0.04 seconds (average 0.013333, setup 0.00)

yes = 948, no = 180702, maybe = 23959

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=205609, nc=40067, nnz=109183, k=2] [1.5 MB]
Creating vector for yes... [1.6 MB]
Allocating iteration vectors... [2 x 1.6 MB]
TOTAL: [6.2 MB]

Starting iterations...

Iterative method: 87 iterations in 0.32 seconds (average 0.001977, setup 0.15)

Value in the initial state: 362.3819507771827

Time for model checking: 1.438 seconds.

Result: 362.3819507771827 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"success"}min=? [ F "deadlock" ]/Pmax=? [ F "MissionSuccessful" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=1,Objy1=3,Objx2=2,Objy2=0

Prob0A: 4 iterations in 0.08 seconds (average 0.019000, setup 0.00)

Prob1E: 5 iterations in 0.06 seconds (average 0.011200, setup 0.00)

Warning: PRISM hasn't checked for zero-reward loops.
Your minimum rewards may be too low...

goal = 1, inf = 0, maybe = 205608

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=205609, nc=266829, nnz=522591, k=4] [7.8 MB]
Building sparse matrix (transition rewards)... [n=205609, nc=266829, nnz=1868, k=4] [1.8 MB]
Creating vector for state rewards... [1.6 MB]
Creating vector for inf... [1.6 MB]
Allocating iteration vectors... [2 x 1.6 MB]
TOTAL: [15.9 MB]

Starting iterations...

Iterative method: 94 iterations in 0.81 seconds (average 0.003106, setup 0.52)

Prob0A: 62 iterations in 0.33 seconds (average 0.005290, setup 0.00)

Prob1E: 4 iterations in 0.05 seconds (average 0.012000, setup 0.00)

yes = 948, no = 176859, maybe = 27802

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=205609, nc=46009, nnz=125095, k=2] [1.7 MB]
Creating vector for yes... [1.6 MB]
Allocating iteration vectors... [2 x 1.6 MB]
TOTAL: [6.4 MB]

Starting iterations...

Iterative method: 98 iterations in 0.36 seconds (average 0.002000, setup 0.16)

Value in the initial state: 154.99273074101887

Time for model checking: 1.732 seconds.

Result: 154.99273074101887 (value in the initial state)

---------------------------------------------------------------------

Note: There were 16 warnings during computation.

