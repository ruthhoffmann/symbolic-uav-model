PRISM
=====

Version: 4.3
Date: Tue Aug 02 12:26:57 BST 2016
Hostname: eglinton
Memory limits: cudd=1g, java(heap)=981.5m
Command line: prism uav_sim_ft_mdp.prism -s uav_mdp.props -const 'Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=2,Objy1=0,Objx2=6,Objy2=3,'

Parsing model file "uav_sim_ft_mdp.prism"...

Parsing properties file "uav_mdp.props"...

18 properties:
(1) Pmax=? [ F "MissionSuccessful" ]
(2) Pmin=? [ F "MissionSuccessful" ]
(3) Pmax=? [ F "MissionTimeUp" ]
(4) Pmin=? [ F "MissionTimeUp" ]
(5) Pmax=? [ F "Crash" ]
(6) Pmin=? [ F "Crash" ]
(7) Pmax=? [ F "AllAreaSearched" ]
(8) Pmin=? [ F "AllAreaSearched" ]
(9) Pmax=? [ F "MissionTimeUp"|"AllAreaSearched"|"Crash" ]
(10) Pmin=? [ F "MissionTimeUp"|"AllAreaSearched"|"Crash" ]
(11) R{"drop"}max=? [ F "deadlock" ]
(12) R{"drop"}min=? [ F "deadlock" ]
(13) R{"charge"}max=? [ F "deadlock" ]
(14) R{"charge"}min=? [ F "deadlock" ]
(15) R{"time"}max=? [ F "deadlock" ]
(16) R{"time"}min=? [ F "deadlock" ]
(17) R{"success"}max=? [ F "deadlock" ]/Pmin=? [ F "MissionSuccessful" ]
(18) R{"success"}min=? [ F "deadlock" ]/Pmax=? [ F "MissionSuccessful" ]

Type:        MDP
Modules:     Obj Move Robot T 
Variables:   NoOfObjs posx posy s1 retx rety b t c 

---------------------------------------------------------------------

Model checking: Pmax=? [ F "MissionSuccessful" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=2,Objy1=0,Objx2=6,Objy2=3

Building model...
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=2,Objy1=0,Objx2=6,Objy2=3

Warning: Guard for command 7 of module "Move" is never satisfied.

Warning: Guard for command 8 of module "Move" is never satisfied.

Warning: Guard for command 9 of module "Move" is never satisfied.

Warning: Guard for command 7 of module "Robot" is never satisfied.

Warning: Update 2 of command 3 of module "Robot" doesn't do anything ("(s1'=13)", line 249, column 44)

Warning: Update 2 of command 27 of module "T" doesn't do anything ("(c'=1)", line 372, column 108)

Warning: Guard for command 28 of module "T" is never satisfied.

Warning: Update 2 of command 9 of module "T" doesn't do anything ("(c'=1)", line 349, column 112)

Warning: Guard for command 10 of module "T" is never satisfied.

Warning: Update 2 of command 51 of module "T" doesn't do anything ("(c'=1)", line 403, column 113)

Warning: Guard for command 52 of module "T" is never satisfied.

Computing reachable states...

Reachability (BFS): 81 iterations in 1.31 seconds (average 0.016198, setup 0.00)

Time for model construction: 2.753 seconds.

Warning: Deadlocks detected and fixed in 1 states

Type:        MDP
States:      268413 (1 initial)
Transitions: 725577
Choices:     355741

Transition matrix: 179963 nodes (76 terminal), 725577 minterms, vars: 34r/34c/18nd

Prob0A: 60 iterations in 0.57 seconds (average 0.009467, setup 0.00)

Prob1E: 4 iterations in 0.07 seconds (average 0.018000, setup 0.00)

yes = 920, no = 229838, maybe = 37655

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=268413, nc=64738, nnz=186201, k=2] [2.4 MB]
Creating vector for yes... [2.0 MB]
Allocating iteration vectors... [2 x 2.0 MB]
TOTAL: [8.6 MB]

Starting iterations...

Iterative method: 99 iterations in 0.45 seconds (average 0.001818, setup 0.27)

Value in the initial state: 0.9768516087192433

Time for model checking: 1.121 seconds.

Result: 0.9768516087192433 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmin=? [ F "MissionSuccessful" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=2,Objy1=0,Objx2=6,Objy2=3

Prob0E: 67 iterations in 0.58 seconds (average 0.008716, setup 0.00)

Prob1A: 3 iterations in 0.04 seconds (average 0.014667, setup 0.00)

yes = 920, no = 234684, maybe = 32809

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=268413, nc=56594, nnz=163466, k=2] [2.2 MB]
Creating vector for yes... [2.0 MB]
Allocating iteration vectors... [2 x 2.0 MB]
TOTAL: [8.3 MB]

Starting iterations...

Iterative method: 93 iterations in 0.45 seconds (average 0.002839, setup 0.19)

Value in the initial state: 0.7143735814315274

Time for model checking: 1.101 seconds.

Result: 0.7143735814315274 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmax=? [ F "MissionTimeUp" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=2,Objy1=0,Objx2=6,Objy2=3

Prob0A: 62 iterations in 0.71 seconds (average 0.011419, setup 0.00)

Prob1E: 4 iterations in 0.06 seconds (average 0.015000, setup 0.00)

yes = 540, no = 151324, maybe = 116549

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=268413, nc=194035, nnz=533963, k=4] [6.6 MB]
Creating vector for yes... [2.0 MB]
Allocating iteration vectors... [2 x 2.0 MB]
TOTAL: [12.7 MB]

Starting iterations...

Iterative method: 95 iterations in 0.59 seconds (average 0.002400, setup 0.36)

Value in the initial state: 0.0038881825495190917

Time for model checking: 1.396 seconds.

Result: 0.0038881825495190917 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmin=? [ F "MissionTimeUp" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=2,Objy1=0,Objx2=6,Objy2=3

Prob0E: 67 iterations in 1.00 seconds (average 0.014866, setup 0.00)

Prob1A: 3 iterations in 0.04 seconds (average 0.013333, setup 0.00)

yes = 540, no = 151738, maybe = 116135

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=268413, nc=193283, nnz=532537, k=4] [6.5 MB]
Creating vector for yes... [2.0 MB]
Allocating iteration vectors... [2 x 2.0 MB]
TOTAL: [12.7 MB]

Starting iterations...

Iterative method: 111 iterations in 0.58 seconds (average 0.002450, setup 0.31)

Value in the initial state: 1.9176165590835427E-5

Time for model checking: 1.657 seconds.

Result: 1.9176165590835427E-5 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmax=? [ F "Crash" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=2,Objy1=0,Objx2=6,Objy2=3

Prob0A: 3 iterations in 0.07 seconds (average 0.022667, setup 0.00)

Prob1E: 232 iterations in 3.45 seconds (average 0.014879, setup 0.00)

yes = 131358, no = 2693, maybe = 134362

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=268413, nc=221690, nnz=591526, k=4] [7.2 MB]
Creating vector for yes... [2.0 MB]
Allocating iteration vectors... [2 x 2.0 MB]
TOTAL: [13.4 MB]

Starting iterations...

Iterative method: 93 iterations in 0.61 seconds (average 0.002624, setup 0.36)

Value in the initial state: 0.06337728147154192

Time for model checking: 4.157 seconds.

Result: 0.06337728147154192 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmin=? [ F "Crash" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=2,Objy1=0,Objx2=6,Objy2=3

Prob0E: 3 iterations in 0.06 seconds (average 0.020000, setup 0.00)

Prob1A: 52 iterations in 0.62 seconds (average 0.011846, setup 0.00)

yes = 131358, no = 2693, maybe = 134362

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=268413, nc=221690, nnz=591526, k=4] [7.2 MB]
Creating vector for yes... [2.0 MB]
Allocating iteration vectors... [2 x 2.0 MB]
TOTAL: [13.4 MB]

Starting iterations...

Iterative method: 100 iterations in 0.60 seconds (average 0.002720, setup 0.33)

Value in the initial state: 0.02106436069367759

Time for model checking: 1.312 seconds.

Result: 0.02106436069367759 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmax=? [ F "AllAreaSearched" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=2,Objy1=0,Objx2=6,Objy2=3

Prob0A: 63 iterations in 0.60 seconds (average 0.009587, setup 0.00)

Prob1E: 4 iterations in 0.05 seconds (average 0.012000, setup 0.00)

yes = 1232, no = 213251, maybe = 53930

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=268413, nc=88240, nnz=245056, k=2] [3.1 MB]
Creating vector for yes... [2.0 MB]
Allocating iteration vectors... [2 x 2.0 MB]
TOTAL: [9.3 MB]

Starting iterations...

Iterative method: 91 iterations in 0.40 seconds (average 0.002066, setup 0.21)

Value in the initial state: 0.255184237690757

Time for model checking: 1.1 seconds.

Result: 0.255184237690757 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmin=? [ F "AllAreaSearched" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=2,Objy1=0,Objx2=6,Objy2=3

Prob0E: 72 iterations in 0.59 seconds (average 0.008167, setup 0.00)

Prob1A: 3 iterations in 0.05 seconds (average 0.016000, setup 0.00)

yes = 1232, no = 220594, maybe = 46587

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=268413, nc=75821, nnz=209811, k=2] [2.7 MB]
Creating vector for yes... [2.0 MB]
Allocating iteration vectors... [2 x 2.0 MB]
TOTAL: [8.9 MB]

Starting iterations...

Iterative method: 91 iterations in 0.40 seconds (average 0.001758, setup 0.24)

Value in the initial state: 0.0012920065582077422

Time for model checking: 1.07 seconds.

Result: 0.0012920065582077422 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmax=? [ F "MissionTimeUp"|"AllAreaSearched"|"Crash" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=2,Objy1=0,Objx2=6,Objy2=3

Prob0A: 3 iterations in 0.12 seconds (average 0.040000, setup 0.00)

Prob1E: 260 iterations in 3.54 seconds (average 0.013631, setup 0.00)

yes = 234683, no = 921, maybe = 32809

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=268413, nc=56594, nnz=163466, k=2] [2.2 MB]
Creating vector for yes... [2.0 MB]
Allocating iteration vectors... [2 x 2.0 MB]
TOTAL: [8.3 MB]

Starting iterations...

Iterative method: 86 iterations in 0.36 seconds (average 0.001721, setup 0.21)

Value in the initial state: 0.2856257760806546

Time for model checking: 4.03 seconds.

Result: 0.2856257760806546 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmin=? [ F "MissionTimeUp"|"AllAreaSearched"|"Crash" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=2,Objy1=0,Objx2=6,Objy2=3

Prob0E: 3 iterations in 0.06 seconds (average 0.018667, setup 0.00)

Prob1A: 60 iterations in 0.66 seconds (average 0.011000, setup 0.00)

yes = 229837, no = 921, maybe = 37655

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=268413, nc=64738, nnz=186201, k=2] [2.4 MB]
Creating vector for yes... [2.0 MB]
Allocating iteration vectors... [2 x 2.0 MB]
TOTAL: [8.6 MB]

Starting iterations...

Iterative method: 96 iterations in 0.39 seconds (average 0.001917, setup 0.20)

Value in the initial state: 0.02314777643551958

Time for model checking: 1.125 seconds.

Result: 0.02314777643551958 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"drop"}max=? [ F "deadlock" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=2,Objy1=0,Objx2=6,Objy2=3

Prob0E: 4 iterations in 0.08 seconds (average 0.020000, setup 0.00)

Prob1A: 1 iterations in 0.01 seconds (average 0.008000, setup 0.00)

goal = 1, inf = 0, maybe = 268412

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=268413, nc=355740, nnz=725576, k=4] [10.7 MB]
Building sparse matrix (transition rewards)... [n=268413, nc=355740, nnz=0, k=4] [2.4 MB]
Creating vector for state rewards... [2.0 MB]
Creating vector for inf... [2.0 MB]
Allocating iteration vectors... [2 x 2.0 MB]
TOTAL: [21.3 MB]

Starting iterations...

Iterative method: 94 iterations in 0.87 seconds (average 0.004043, setup 0.49)

Value in the initial state: 0.35227221895137856

Time for model checking: 0.971 seconds.

Result: 0.35227221895137856 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"drop"}min=? [ F "deadlock" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=2,Objy1=0,Objx2=6,Objy2=3

Prob0A: 4 iterations in 0.05 seconds (average 0.012000, setup 0.00)

Prob1E: 5 iterations in 0.05 seconds (average 0.009600, setup 0.00)

Warning: PRISM hasn't checked for zero-reward loops.
Your minimum rewards may be too low...

goal = 1, inf = 0, maybe = 268412

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=268413, nc=355740, nnz=725576, k=4] [10.7 MB]
Building sparse matrix (transition rewards)... [n=268413, nc=355740, nnz=0, k=4] [2.4 MB]
Creating vector for state rewards... [2.0 MB]
Creating vector for inf... [2.0 MB]
Allocating iteration vectors... [2 x 2.0 MB]
TOTAL: [21.3 MB]

Starting iterations...

Iterative method: 86 iterations in 0.73 seconds (average 0.004047, setup 0.38)

Value in the initial state: 0.22580208331134552

Time for model checking: 0.84 seconds.

Result: 0.22580208331134552 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"charge"}max=? [ F "deadlock" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=2,Objy1=0,Objx2=6,Objy2=3

Prob0E: 4 iterations in 0.03 seconds (average 0.007000, setup 0.00)

Prob1A: 1 iterations in 0.00 seconds (average 0.000000, setup 0.00)

goal = 1, inf = 0, maybe = 268412

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=268413, nc=355740, nnz=725576, k=4] [10.7 MB]
Building sparse matrix (transition rewards)... [n=268413, nc=355740, nnz=8570, k=4] [2.5 MB]
Creating vector for state rewards... [2.0 MB]
Creating vector for inf... [2.0 MB]
Allocating iteration vectors... [2 x 2.0 MB]
TOTAL: [21.4 MB]

Starting iterations...

Iterative method: 90 iterations in 0.74 seconds (average 0.003956, setup 0.38)

Value in the initial state: 0.9817744228504998

Time for model checking: 0.792 seconds.

Result: 0.9817744228504998 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"charge"}min=? [ F "deadlock" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=2,Objy1=0,Objx2=6,Objy2=3

Prob0A: 4 iterations in 0.02 seconds (average 0.006000, setup 0.00)

Prob1E: 5 iterations in 0.05 seconds (average 0.009600, setup 0.00)

Warning: PRISM hasn't checked for zero-reward loops.
Your minimum rewards may be too low...

goal = 1, inf = 0, maybe = 268412

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=268413, nc=355740, nnz=725576, k=4] [10.7 MB]
Building sparse matrix (transition rewards)... [n=268413, nc=355740, nnz=8570, k=4] [2.5 MB]
Creating vector for state rewards... [2.0 MB]
Creating vector for inf... [2.0 MB]
Allocating iteration vectors... [2 x 2.0 MB]
TOTAL: [21.4 MB]

Starting iterations...

Iterative method: 100 iterations in 0.78 seconds (average 0.004000, setup 0.38)

Value in the initial state: 0.09845458837585856

Time for model checking: 0.865 seconds.

Result: 0.09845458837585856 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"time"}max=? [ F "deadlock" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=2,Objy1=0,Objx2=6,Objy2=3

Prob0E: 4 iterations in 0.03 seconds (average 0.007000, setup 0.00)

Prob1A: 1 iterations in 0.00 seconds (average 0.004000, setup 0.00)

goal = 1, inf = 0, maybe = 268412

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=268413, nc=355740, nnz=725576, k=4] [10.7 MB]
Building sparse matrix (transition rewards)... [n=268413, nc=355740, nnz=134049, k=4] [3.9 MB]
Creating vector for state rewards... [2.0 MB]
Creating vector for inf... [2.0 MB]
Allocating iteration vectors... [2 x 2.0 MB]
TOTAL: [22.8 MB]

Starting iterations...

Iterative method: 96 iterations in 0.80 seconds (average 0.004083, setup 0.41)

Value in the initial state: 347.9451208619316

Time for model checking: 0.848 seconds.

Result: 347.9451208619316 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"time"}min=? [ F "deadlock" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=2,Objy1=0,Objx2=6,Objy2=3

Prob0A: 4 iterations in 0.02 seconds (average 0.006000, setup 0.00)

Prob1E: 5 iterations in 0.04 seconds (average 0.008800, setup 0.00)

Warning: PRISM hasn't checked for zero-reward loops.
Your minimum rewards may be too low...

goal = 1, inf = 0, maybe = 268412

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=268413, nc=355740, nnz=725576, k=4] [10.7 MB]
Building sparse matrix (transition rewards)... [n=268413, nc=355740, nnz=134049, k=4] [3.9 MB]
Creating vector for state rewards... [2.0 MB]
Creating vector for inf... [2.0 MB]
Allocating iteration vectors... [2 x 2.0 MB]
TOTAL: [22.8 MB]

Starting iterations...

Iterative method: 105 iterations in 0.84 seconds (average 0.004076, setup 0.41)

Value in the initial state: 116.58191979939934

Time for model checking: 0.92 seconds.

Result: 116.58191979939934 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"success"}max=? [ F "deadlock" ]/Pmin=? [ F "MissionSuccessful" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=2,Objy1=0,Objx2=6,Objy2=3

Prob0E: 4 iterations in 0.02 seconds (average 0.006000, setup 0.00)

Prob1A: 1 iterations in 0.00 seconds (average 0.004000, setup 0.00)

goal = 1, inf = 0, maybe = 268412

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=268413, nc=355740, nnz=725576, k=4] [10.7 MB]
Building sparse matrix (transition rewards)... [n=268413, nc=355740, nnz=1812, k=4] [2.4 MB]
Creating vector for state rewards... [2.0 MB]
Creating vector for inf... [2.0 MB]
Allocating iteration vectors... [2 x 2.0 MB]
TOTAL: [21.3 MB]

Starting iterations...

Iterative method: 95 iterations in 0.73 seconds (average 0.003789, setup 0.37)

Prob0E: 67 iterations in 0.34 seconds (average 0.005134, setup 0.00)

Prob1A: 3 iterations in 0.04 seconds (average 0.012000, setup 0.00)

yes = 920, no = 234684, maybe = 32809

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=268413, nc=56594, nnz=163466, k=2] [2.2 MB]
Creating vector for yes... [2.0 MB]
Allocating iteration vectors... [2 x 2.0 MB]
TOTAL: [8.3 MB]

Starting iterations...

Iterative method: 93 iterations in 0.28 seconds (average 0.001505, setup 0.14)

Value in the initial state: 474.85908150401985

Time for model checking: 1.442 seconds.

Result: 474.85908150401985 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"success"}min=? [ F "deadlock" ]/Pmax=? [ F "MissionSuccessful" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=2,Objy1=0,Objx2=6,Objy2=3

Prob0A: 4 iterations in 0.06 seconds (average 0.015000, setup 0.00)

Prob1E: 5 iterations in 0.04 seconds (average 0.007200, setup 0.00)

Warning: PRISM hasn't checked for zero-reward loops.
Your minimum rewards may be too low...

goal = 1, inf = 0, maybe = 268412

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=268413, nc=355740, nnz=725576, k=4] [10.7 MB]
Building sparse matrix (transition rewards)... [n=268413, nc=355740, nnz=1812, k=4] [2.4 MB]
Creating vector for state rewards... [2.0 MB]
Creating vector for inf... [2.0 MB]
Allocating iteration vectors... [2 x 2.0 MB]
TOTAL: [21.3 MB]

Starting iterations...

Iterative method: 98 iterations in 0.94 seconds (average 0.004000, setup 0.55)

Prob0A: 60 iterations in 0.32 seconds (average 0.005267, setup 0.00)

Prob1E: 4 iterations in 0.04 seconds (average 0.009000, setup 0.00)

yes = 920, no = 229838, maybe = 37655

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=268413, nc=64738, nnz=186201, k=2] [2.4 MB]
Creating vector for yes... [2.0 MB]
Allocating iteration vectors... [2 x 2.0 MB]
TOTAL: [8.6 MB]

Starting iterations...

Iterative method: 99 iterations in 0.30 seconds (average 0.001576, setup 0.14)

Value in the initial state: 80.2183709711651

Time for model checking: 1.712 seconds.

Result: 80.2183709711651 (value in the initial state)

---------------------------------------------------------------------

Note: There were 16 warnings during computation.

