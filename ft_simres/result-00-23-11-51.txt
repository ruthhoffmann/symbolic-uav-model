PRISM
=====

Version: 4.3
Date: Tue Aug 02 12:25:34 BST 2016
Hostname: eglinton
Memory limits: cudd=1g, java(heap)=981.5m
Command line: prism uav_sim_ft_mdp.prism -s uav_mdp.props -const 'Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=1,Objy1=1,Objx2=5,Objy2=1,'

Parsing model file "uav_sim_ft_mdp.prism"...

Parsing properties file "uav_mdp.props"...

18 properties:
(1) Pmax=? [ F "MissionSuccessful" ]
(2) Pmin=? [ F "MissionSuccessful" ]
(3) Pmax=? [ F "MissionTimeUp" ]
(4) Pmin=? [ F "MissionTimeUp" ]
(5) Pmax=? [ F "Crash" ]
(6) Pmin=? [ F "Crash" ]
(7) Pmax=? [ F "AllAreaSearched" ]
(8) Pmin=? [ F "AllAreaSearched" ]
(9) Pmax=? [ F "MissionTimeUp"|"AllAreaSearched"|"Crash" ]
(10) Pmin=? [ F "MissionTimeUp"|"AllAreaSearched"|"Crash" ]
(11) R{"drop"}max=? [ F "deadlock" ]
(12) R{"drop"}min=? [ F "deadlock" ]
(13) R{"charge"}max=? [ F "deadlock" ]
(14) R{"charge"}min=? [ F "deadlock" ]
(15) R{"time"}max=? [ F "deadlock" ]
(16) R{"time"}min=? [ F "deadlock" ]
(17) R{"success"}max=? [ F "deadlock" ]/Pmin=? [ F "MissionSuccessful" ]
(18) R{"success"}min=? [ F "deadlock" ]/Pmax=? [ F "MissionSuccessful" ]

Type:        MDP
Modules:     Obj Move Robot T 
Variables:   NoOfObjs posx posy s1 retx rety b t c 

---------------------------------------------------------------------

Model checking: Pmax=? [ F "MissionSuccessful" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=1,Objy1=1,Objx2=5,Objy2=1

Building model...
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=1,Objy1=1,Objx2=5,Objy2=1

Warning: Guard for command 7 of module "Move" is never satisfied.

Warning: Guard for command 8 of module "Move" is never satisfied.

Warning: Guard for command 9 of module "Move" is never satisfied.

Warning: Guard for command 7 of module "Robot" is never satisfied.

Warning: Update 2 of command 3 of module "Robot" doesn't do anything ("(s1'=13)", line 249, column 44)

Warning: Update 2 of command 27 of module "T" doesn't do anything ("(c'=1)", line 372, column 108)

Warning: Guard for command 28 of module "T" is never satisfied.

Warning: Update 2 of command 9 of module "T" doesn't do anything ("(c'=1)", line 349, column 112)

Warning: Guard for command 10 of module "T" is never satisfied.

Warning: Update 2 of command 51 of module "T" doesn't do anything ("(c'=1)", line 403, column 113)

Warning: Guard for command 52 of module "T" is never satisfied.

Computing reachable states...

Reachability (BFS): 81 iterations in 1.01 seconds (average 0.012444, setup 0.00)

Time for model construction: 2.533 seconds.

Warning: Deadlocks detected and fixed in 1 states

Type:        MDP
States:      213429 (1 initial)
Transitions: 571074
Choices:     283158

Transition matrix: 171244 nodes (76 terminal), 571074 minterms, vars: 34r/34c/18nd

Prob0A: 43 iterations in 0.34 seconds (average 0.007814, setup 0.00)

Prob1E: 4 iterations in 0.05 seconds (average 0.013000, setup 0.00)

yes = 951, no = 179815, maybe = 32663

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=213429, nc=58578, nnz=169613, k=2] [2.2 MB]
Creating vector for yes... [1.6 MB]
Allocating iteration vectors... [2 x 1.6 MB]
TOTAL: [7.1 MB]

Starting iterations...

Iterative method: 87 iterations in 0.37 seconds (average 0.001563, setup 0.23)

Value in the initial state: 0.9811685963391393

Time for model checking: 0.798 seconds.

Result: 0.9811685963391393 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmin=? [ F "MissionSuccessful" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=1,Objy1=1,Objx2=5,Objy2=1

Prob0E: 45 iterations in 0.40 seconds (average 0.008978, setup 0.00)

Prob1A: 3 iterations in 0.05 seconds (average 0.016000, setup 0.00)

yes = 951, no = 184954, maybe = 27524

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=213429, nc=49151, nnz=141979, k=2] [1.9 MB]
Creating vector for yes... [1.6 MB]
Allocating iteration vectors... [2 x 1.6 MB]
TOTAL: [6.8 MB]

Starting iterations...

Iterative method: 82 iterations in 0.30 seconds (average 0.001366, setup 0.19)

Value in the initial state: 0.7405775030959273

Time for model checking: 0.852 seconds.

Result: 0.7405775030959273 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmax=? [ F "MissionTimeUp" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=1,Objy1=1,Objx2=5,Objy2=1

Prob0A: 60 iterations in 0.52 seconds (average 0.008600, setup 0.00)

Prob1E: 4 iterations in 0.04 seconds (average 0.009000, setup 0.00)

yes = 428, no = 133401, maybe = 79600

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=213429, nc=139500, nnz=389239, k=4] [4.8 MB]
Creating vector for yes... [1.6 MB]
Allocating iteration vectors... [2 x 1.6 MB]
TOTAL: [9.7 MB]

Starting iterations...

Iterative method: 90 iterations in 0.44 seconds (average 0.001867, setup 0.28)

Value in the initial state: 8.362797146009248E-4

Time for model checking: 1.138 seconds.

Result: 8.362797146009248E-4 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmin=? [ F "MissionTimeUp" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=1,Objy1=1,Objx2=5,Objy2=1

Prob0E: 68 iterations in 0.61 seconds (average 0.008941, setup 0.00)

Prob1A: 3 iterations in 0.05 seconds (average 0.016000, setup 0.00)

yes = 428, no = 133777, maybe = 79224

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=213429, nc=138824, nnz=387891, k=4] [4.8 MB]
Creating vector for yes... [1.6 MB]
Allocating iteration vectors... [2 x 1.6 MB]
TOTAL: [9.7 MB]

Starting iterations...

Iterative method: 105 iterations in 0.46 seconds (average 0.001905, setup 0.26)

Value in the initial state: 2.1672474351222196E-6

Time for model checking: 1.168 seconds.

Result: 2.1672474351222196E-6 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmax=? [ F "Crash" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=1,Objy1=1,Objx2=5,Objy2=1

Prob0A: 3 iterations in 0.10 seconds (average 0.034667, setup 0.00)

Prob1E: 169 iterations in 2.54 seconds (average 0.015053, setup 0.00)

yes = 104516, no = 2617, maybe = 106296

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=213429, nc=176025, nnz=463941, k=4] [5.7 MB]
Creating vector for yes... [1.6 MB]
Allocating iteration vectors... [2 x 1.6 MB]
TOTAL: [10.6 MB]

Starting iterations...

Iterative method: 86 iterations in 0.50 seconds (average 0.002047, setup 0.32)

Value in the initial state: 0.031035624566504355

Time for model checking: 3.689 seconds.

Result: 0.031035624566504355 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmin=? [ F "Crash" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=1,Objy1=1,Objx2=5,Objy2=1

Prob0E: 3 iterations in 0.06 seconds (average 0.021333, setup 0.00)

Prob1A: 42 iterations in 0.48 seconds (average 0.011429, setup 0.00)

yes = 104516, no = 2617, maybe = 106296

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=213429, nc=176025, nnz=463941, k=4] [5.7 MB]
Creating vector for yes... [1.6 MB]
Allocating iteration vectors... [2 x 1.6 MB]
TOTAL: [10.6 MB]

Starting iterations...

Iterative method: 83 iterations in 0.41 seconds (average 0.002120, setup 0.24)

Value in the initial state: 0.01673073114832668

Time for model checking: 1.111 seconds.

Result: 0.01673073114832668 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmax=? [ F "AllAreaSearched" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=1,Objy1=1,Objx2=5,Objy2=1

Prob0A: 55 iterations in 0.47 seconds (average 0.008582, setup 0.00)

Prob1E: 4 iterations in 0.04 seconds (average 0.011000, setup 0.00)

yes = 1237, no = 168206, maybe = 43986

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=213429, nc=66183, nnz=171112, k=2] [2.2 MB]
Creating vector for yes... [1.6 MB]
Allocating iteration vectors... [2 x 1.6 MB]
TOTAL: [7.1 MB]

Starting iterations...

Iterative method: 87 iterations in 0.30 seconds (average 0.001563, setup 0.16)

Value in the initial state: 0.2307498021899573

Time for model checking: 0.964 seconds.

Result: 0.2307498021899573 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmin=? [ F "AllAreaSearched" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=1,Objy1=1,Objx2=5,Objy2=1

Prob0E: 63 iterations in 0.46 seconds (average 0.007238, setup 0.00)

Prob1A: 3 iterations in 0.04 seconds (average 0.014667, setup 0.00)

yes = 1237, no = 173184, maybe = 39008

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=213429, nc=57428, nnz=146235, k=2] [1.9 MB]
Creating vector for yes... [1.6 MB]
Allocating iteration vectors... [2 x 1.6 MB]
TOTAL: [6.8 MB]

Starting iterations...

Iterative method: 92 iterations in 0.27 seconds (average 0.001478, setup 0.14)

Value in the initial state: 0.0017442343372834146

Time for model checking: 0.805 seconds.

Result: 0.0017442343372834146 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmax=? [ F "MissionTimeUp"|"AllAreaSearched"|"Crash" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=1,Objy1=1,Objx2=5,Objy2=1

Prob0A: 3 iterations in 0.08 seconds (average 0.028000, setup 0.00)

Prob1E: 176 iterations in 2.32 seconds (average 0.013159, setup 0.00)

yes = 184953, no = 952, maybe = 27524

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=213429, nc=49151, nnz=141979, k=2] [1.9 MB]
Creating vector for yes... [1.6 MB]
Allocating iteration vectors... [2 x 1.6 MB]
TOTAL: [6.8 MB]

Starting iterations...

Iterative method: 75 iterations in 0.30 seconds (average 0.001440, setup 0.20)

Value in the initial state: 0.25942236308409555

Time for model checking: 2.745 seconds.

Result: 0.25942236308409555 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmin=? [ F "MissionTimeUp"|"AllAreaSearched"|"Crash" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=1,Objy1=1,Objx2=5,Objy2=1

Prob0E: 3 iterations in 0.05 seconds (average 0.017333, setup 0.00)

Prob1A: 43 iterations in 0.42 seconds (average 0.009860, setup 0.00)

yes = 179814, no = 952, maybe = 32663

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=213429, nc=58578, nnz=169613, k=2] [2.2 MB]
Creating vector for yes... [1.6 MB]
Allocating iteration vectors... [2 x 1.6 MB]
TOTAL: [7.1 MB]

Starting iterations...

Iterative method: 86 iterations in 0.30 seconds (average 0.001488, setup 0.17)

Value in the initial state: 0.018830385136946085

Time for model checking: 0.805 seconds.

Result: 0.018830385136946085 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"drop"}max=? [ F "deadlock" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=1,Objy1=1,Objx2=5,Objy2=1

Prob0E: 4 iterations in 0.06 seconds (average 0.016000, setup 0.00)

Prob1A: 1 iterations in 0.01 seconds (average 0.008000, setup 0.00)

goal = 1, inf = 0, maybe = 213428

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=213429, nc=283157, nnz=571073, k=4] [8.4 MB]
Building sparse matrix (transition rewards)... [n=213429, nc=283157, nnz=0, k=4] [1.9 MB]
Creating vector for state rewards... [1.6 MB]
Creating vector for inf... [1.6 MB]
Allocating iteration vectors... [2 x 1.6 MB]
TOTAL: [16.8 MB]

Starting iterations...

Iterative method: 84 iterations in 0.66 seconds (average 0.003143, setup 0.40)

Value in the initial state: 0.21131020838633863

Time for model checking: 0.76 seconds.

Result: 0.21131020838633863 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"drop"}min=? [ F "deadlock" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=1,Objy1=1,Objx2=5,Objy2=1

Prob0A: 4 iterations in 0.04 seconds (average 0.010000, setup 0.00)

Prob1E: 5 iterations in 0.05 seconds (average 0.010400, setup 0.00)

Warning: PRISM hasn't checked for zero-reward loops.
Your minimum rewards may be too low...

goal = 1, inf = 0, maybe = 213428

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=213429, nc=283157, nnz=571073, k=4] [8.4 MB]
Building sparse matrix (transition rewards)... [n=213429, nc=283157, nnz=0, k=4] [1.9 MB]
Creating vector for state rewards... [1.6 MB]
Creating vector for inf... [1.6 MB]
Allocating iteration vectors... [2 x 1.6 MB]
TOTAL: [16.8 MB]

Starting iterations...

Iterative method: 75 iterations in 0.55 seconds (average 0.003147, setup 0.32)

Value in the initial state: 0.18695673608458932

Time for model checking: 0.658 seconds.

Result: 0.18695673608458932 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"charge"}max=? [ F "deadlock" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=1,Objy1=1,Objx2=5,Objy2=1

Prob0E: 4 iterations in 0.03 seconds (average 0.007000, setup 0.00)

Prob1A: 1 iterations in 0.00 seconds (average 0.004000, setup 0.00)

goal = 1, inf = 0, maybe = 213428

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=213429, nc=283157, nnz=571073, k=4] [8.4 MB]
Building sparse matrix (transition rewards)... [n=213429, nc=283157, nnz=6458, k=4] [2.0 MB]
Creating vector for state rewards... [1.6 MB]
Creating vector for inf... [1.6 MB]
Allocating iteration vectors... [2 x 1.6 MB]
TOTAL: [16.9 MB]

Starting iterations...

Iterative method: 80 iterations in 0.56 seconds (average 0.003200, setup 0.31)

Value in the initial state: 0.29320469884348405

Time for model checking: 0.631 seconds.

Result: 0.29320469884348405 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"charge"}min=? [ F "deadlock" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=1,Objy1=1,Objx2=5,Objy2=1

Prob0A: 4 iterations in 0.03 seconds (average 0.008000, setup 0.00)

Prob1E: 5 iterations in 0.05 seconds (average 0.010400, setup 0.00)

Warning: PRISM hasn't checked for zero-reward loops.
Your minimum rewards may be too low...

goal = 1, inf = 0, maybe = 213428

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=213429, nc=283157, nnz=571073, k=4] [8.4 MB]
Building sparse matrix (transition rewards)... [n=213429, nc=283157, nnz=6458, k=4] [2.0 MB]
Creating vector for state rewards... [1.6 MB]
Creating vector for inf... [1.6 MB]
Allocating iteration vectors... [2 x 1.6 MB]
TOTAL: [16.9 MB]

Starting iterations...

Iterative method: 89 iterations in 0.59 seconds (average 0.003146, setup 0.31)

Value in the initial state: 0.04582730037495737

Time for model checking: 0.71 seconds.

Result: 0.04582730037495737 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"time"}max=? [ F "deadlock" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=1,Objy1=1,Objx2=5,Objy2=1

Prob0E: 4 iterations in 0.03 seconds (average 0.008000, setup 0.00)

Prob1A: 1 iterations in 0.01 seconds (average 0.008000, setup 0.00)

goal = 1, inf = 0, maybe = 213428

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=213429, nc=283157, nnz=571073, k=4] [8.4 MB]
Building sparse matrix (transition rewards)... [n=213429, nc=283157, nnz=107131, k=4] [3.1 MB]
Creating vector for state rewards... [1.6 MB]
Creating vector for inf... [1.6 MB]
Allocating iteration vectors... [2 x 1.6 MB]
TOTAL: [18.1 MB]

Starting iterations...

Iterative method: 89 iterations in 0.62 seconds (average 0.003236, setup 0.34)

Value in the initial state: 171.15877116334835

Time for model checking: 0.698 seconds.

Result: 171.15877116334835 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"time"}min=? [ F "deadlock" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=1,Objy1=1,Objx2=5,Objy2=1

Prob0A: 4 iterations in 0.03 seconds (average 0.007000, setup 0.00)

Prob1E: 5 iterations in 0.04 seconds (average 0.008800, setup 0.00)

Warning: PRISM hasn't checked for zero-reward loops.
Your minimum rewards may be too low...

goal = 1, inf = 0, maybe = 213428

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=213429, nc=283157, nnz=571073, k=4] [8.4 MB]
Building sparse matrix (transition rewards)... [n=213429, nc=283157, nnz=107131, k=4] [3.1 MB]
Creating vector for state rewards... [1.6 MB]
Creating vector for inf... [1.6 MB]
Allocating iteration vectors... [2 x 1.6 MB]
TOTAL: [18.1 MB]

Starting iterations...

Iterative method: 94 iterations in 0.65 seconds (average 0.003277, setup 0.34)

Value in the initial state: 92.72445998091439

Time for model checking: 0.739 seconds.

Result: 92.72445998091439 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"success"}max=? [ F "deadlock" ]/Pmin=? [ F "MissionSuccessful" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=1,Objy1=1,Objx2=5,Objy2=1

Prob0E: 4 iterations in 0.03 seconds (average 0.007000, setup 0.00)

Prob1A: 1 iterations in 0.00 seconds (average 0.004000, setup 0.00)

goal = 1, inf = 0, maybe = 213428

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=213429, nc=283157, nnz=571073, k=4] [8.4 MB]
Building sparse matrix (transition rewards)... [n=213429, nc=283157, nnz=1874, k=4] [1.9 MB]
Creating vector for state rewards... [1.6 MB]
Creating vector for inf... [1.6 MB]
Allocating iteration vectors... [2 x 1.6 MB]
TOTAL: [16.9 MB]

Starting iterations...

Iterative method: 90 iterations in 0.59 seconds (average 0.003156, setup 0.30)

Prob0E: 45 iterations in 0.44 seconds (average 0.009689, setup 0.00)

Prob1A: 3 iterations in 0.04 seconds (average 0.013333, setup 0.00)

yes = 951, no = 184954, maybe = 27524

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=213429, nc=49151, nnz=141979, k=2] [1.9 MB]
Creating vector for yes... [1.6 MB]
Allocating iteration vectors... [2 x 1.6 MB]
TOTAL: [6.8 MB]

Starting iterations...

Iterative method: 82 iterations in 0.28 seconds (average 0.001415, setup 0.16)

Value in the initial state: 226.41203006018154

Time for model checking: 1.399 seconds.

Result: 226.41203006018154 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"success"}min=? [ F "deadlock" ]/Pmax=? [ F "MissionSuccessful" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=1,Objy1=1,Objx2=5,Objy2=1

Prob0A: 4 iterations in 0.11 seconds (average 0.028000, setup 0.00)

Prob1E: 5 iterations in 0.05 seconds (average 0.010400, setup 0.00)

Warning: PRISM hasn't checked for zero-reward loops.
Your minimum rewards may be too low...

goal = 1, inf = 0, maybe = 213428

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=213429, nc=283157, nnz=571073, k=4] [8.4 MB]
Building sparse matrix (transition rewards)... [n=213429, nc=283157, nnz=1874, k=4] [1.9 MB]
Creating vector for state rewards... [1.6 MB]
Creating vector for inf... [1.6 MB]
Allocating iteration vectors... [2 x 1.6 MB]
TOTAL: [16.9 MB]

Starting iterations...

Iterative method: 85 iterations in 0.62 seconds (average 0.003059, setup 0.36)

Prob0A: 43 iterations in 0.22 seconds (average 0.005209, setup 0.00)

Prob1E: 4 iterations in 0.03 seconds (average 0.008000, setup 0.00)

yes = 951, no = 179815, maybe = 32663

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=213429, nc=58578, nnz=169613, k=2] [2.2 MB]
Creating vector for yes... [1.6 MB]
Allocating iteration vectors... [2 x 1.6 MB]
TOTAL: [7.1 MB]

Starting iterations...

Iterative method: 87 iterations in 0.28 seconds (average 0.001425, setup 0.16)

Value in the initial state: 78.92764985730139

Time for model checking: 1.341 seconds.

Result: 78.92764985730139 (value in the initial state)

---------------------------------------------------------------------

Note: There were 16 warnings during computation.

