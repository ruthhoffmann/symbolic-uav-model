PRISM
=====

Version: 4.3
Date: Tue Aug 02 12:35:49 BST 2016
Hostname: eglinton
Memory limits: cudd=1g, java(heap)=981.5m
Command line: prism uav_sim_ft_mdp.prism -s uav_mdp.props -const 'Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=0,Objy1=2,Objx2=3,Objy2=1,'

Parsing model file "uav_sim_ft_mdp.prism"...

Parsing properties file "uav_mdp.props"...

18 properties:
(1) Pmax=? [ F "MissionSuccessful" ]
(2) Pmin=? [ F "MissionSuccessful" ]
(3) Pmax=? [ F "MissionTimeUp" ]
(4) Pmin=? [ F "MissionTimeUp" ]
(5) Pmax=? [ F "Crash" ]
(6) Pmin=? [ F "Crash" ]
(7) Pmax=? [ F "AllAreaSearched" ]
(8) Pmin=? [ F "AllAreaSearched" ]
(9) Pmax=? [ F "MissionTimeUp"|"AllAreaSearched"|"Crash" ]
(10) Pmin=? [ F "MissionTimeUp"|"AllAreaSearched"|"Crash" ]
(11) R{"drop"}max=? [ F "deadlock" ]
(12) R{"drop"}min=? [ F "deadlock" ]
(13) R{"charge"}max=? [ F "deadlock" ]
(14) R{"charge"}min=? [ F "deadlock" ]
(15) R{"time"}max=? [ F "deadlock" ]
(16) R{"time"}min=? [ F "deadlock" ]
(17) R{"success"}max=? [ F "deadlock" ]/Pmin=? [ F "MissionSuccessful" ]
(18) R{"success"}min=? [ F "deadlock" ]/Pmax=? [ F "MissionSuccessful" ]

Type:        MDP
Modules:     Obj Move Robot T 
Variables:   NoOfObjs posx posy s1 retx rety b t c 

---------------------------------------------------------------------

Model checking: Pmax=? [ F "MissionSuccessful" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=0,Objy1=2,Objx2=3,Objy2=1

Building model...
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=0,Objy1=2,Objx2=3,Objy2=1

Warning: Guard for command 7 of module "Move" is never satisfied.

Warning: Guard for command 8 of module "Move" is never satisfied.

Warning: Guard for command 9 of module "Move" is never satisfied.

Warning: Guard for command 7 of module "Robot" is never satisfied.

Warning: Update 2 of command 3 of module "Robot" doesn't do anything ("(s1'=13)", line 249, column 44)

Warning: Update 2 of command 27 of module "T" doesn't do anything ("(c'=1)", line 372, column 108)

Warning: Guard for command 28 of module "T" is never satisfied.

Warning: Update 2 of command 9 of module "T" doesn't do anything ("(c'=1)", line 349, column 112)

Warning: Guard for command 10 of module "T" is never satisfied.

Warning: Update 2 of command 51 of module "T" doesn't do anything ("(c'=1)", line 403, column 113)

Warning: Guard for command 52 of module "T" is never satisfied.

Computing reachable states...

Reachability (BFS): 80 iterations in 0.78 seconds (average 0.009700, setup 0.00)

Time for model construction: 2.433 seconds.

Warning: Deadlocks detected and fixed in 1 states

Type:        MDP
States:      193968 (1 initial)
Transitions: 517241
Choices:     257606

Transition matrix: 166537 nodes (76 terminal), 517241 minterms, vars: 34r/34c/18nd

Prob0A: 43 iterations in 0.34 seconds (average 0.007814, setup 0.00)

Prob1E: 4 iterations in 0.06 seconds (average 0.015000, setup 0.00)

yes = 953, no = 162840, maybe = 30175

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=193968, nc=54071, nnz=155102, k=2] [2.0 MB]
Creating vector for yes... [1.5 MB]
Allocating iteration vectors... [2 x 1.5 MB]
TOTAL: [6.5 MB]

Starting iterations...

Iterative method: 86 iterations in 0.33 seconds (average 0.001349, setup 0.22)

Value in the initial state: 0.9809772168537647

Time for model checking: 1.01 seconds.

Result: 0.9809772168537647 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmin=? [ F "MissionSuccessful" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=0,Objy1=2,Objx2=3,Objy2=1

Prob0E: 43 iterations in 0.38 seconds (average 0.008930, setup 0.00)

Prob1A: 3 iterations in 0.04 seconds (average 0.014667, setup 0.00)

yes = 953, no = 167161, maybe = 25854

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=193968, nc=46181, nnz=132171, k=2] [1.7 MB]
Creating vector for yes... [1.5 MB]
Allocating iteration vectors... [2 x 1.5 MB]
TOTAL: [6.2 MB]

Starting iterations...

Iterative method: 80 iterations in 0.28 seconds (average 0.001300, setup 0.18)

Value in the initial state: 0.753086258569056

Time for model checking: 0.925 seconds.

Result: 0.753086258569056 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmax=? [ F "MissionTimeUp" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=0,Objy1=2,Objx2=3,Objy2=1

Prob0A: 62 iterations in 0.51 seconds (average 0.008258, setup 0.00)

Prob1E: 4 iterations in 0.04 seconds (average 0.011000, setup 0.00)

yes = 376, no = 121810, maybe = 71782

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=193968, nc=125611, nnz=349014, k=4] [4.3 MB]
Creating vector for yes... [1.5 MB]
Allocating iteration vectors... [2 x 1.5 MB]
TOTAL: [8.7 MB]

Starting iterations...

Iterative method: 88 iterations in 0.40 seconds (average 0.001682, setup 0.25)

Value in the initial state: 5.661032516683372E-4

Time for model checking: 1.134 seconds.

Result: 5.661032516683372E-4 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmin=? [ F "MissionTimeUp" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=0,Objy1=2,Objx2=3,Objy2=1

Prob0E: 69 iterations in 0.62 seconds (average 0.008928, setup 0.00)

Prob1A: 3 iterations in 0.05 seconds (average 0.016000, setup 0.00)

yes = 376, no = 122167, maybe = 71425

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=193968, nc=124973, nnz=347705, k=4] [4.3 MB]
Creating vector for yes... [1.5 MB]
Allocating iteration vectors... [2 x 1.5 MB]
TOTAL: [8.7 MB]

Starting iterations...

Iterative method: 104 iterations in 0.42 seconds (average 0.001731, setup 0.24)

Value in the initial state: 1.2818516288046743E-6

Time for model checking: 1.173 seconds.

Result: 1.2818516288046743E-6 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmax=? [ F "Crash" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=0,Objy1=2,Objx2=3,Objy2=1

Prob0A: 3 iterations in 0.08 seconds (average 0.026667, setup 0.00)

Prob1E: 170 iterations in 2.35 seconds (average 0.013812, setup 0.00)

yes = 95036, no = 2552, maybe = 96380

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=193968, nc=160018, nnz=419653, k=4] [5.1 MB]
Creating vector for yes... [1.5 MB]
Allocating iteration vectors... [2 x 1.5 MB]
TOTAL: [9.6 MB]

Starting iterations...

Iterative method: 85 iterations in 0.45 seconds (average 0.001835, setup 0.30)

Value in the initial state: 0.030274683886049075

Time for model checking: 3.171 seconds.

Result: 0.030274683886049075 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmin=? [ F "Crash" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=0,Objy1=2,Objx2=3,Objy2=1

Prob0E: 3 iterations in 0.05 seconds (average 0.017333, setup 0.00)

Prob1A: 43 iterations in 0.38 seconds (average 0.008930, setup 0.00)

yes = 95036, no = 2552, maybe = 96380

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=193968, nc=160018, nnz=419653, k=4] [5.1 MB]
Creating vector for yes... [1.5 MB]
Allocating iteration vectors... [2 x 1.5 MB]
TOTAL: [9.6 MB]

Starting iterations...

Iterative method: 80 iterations in 0.36 seconds (average 0.001950, setup 0.20)

Value in the initial state: 0.016907753847211805

Time for model checking: 1.138 seconds.

Result: 0.016907753847211805 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmax=? [ F "AllAreaSearched" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=0,Objy1=2,Objx2=3,Objy2=1

Prob0A: 52 iterations in 0.38 seconds (average 0.007308, setup 0.00)

Prob1E: 4 iterations in 0.04 seconds (average 0.010000, setup 0.00)

yes = 1222, no = 149631, maybe = 43115

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=193968, nc=66247, nnz=173430, k=2] [2.2 MB]
Creating vector for yes... [1.5 MB]
Allocating iteration vectors... [2 x 1.5 MB]
TOTAL: [6.7 MB]

Starting iterations...

Iterative method: 85 iterations in 0.29 seconds (average 0.001412, setup 0.17)

Value in the initial state: 0.21914523263694224

Time for model checking: 0.935 seconds.

Result: 0.21914523263694224 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmin=? [ F "AllAreaSearched" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=0,Objy1=2,Objx2=3,Objy2=1

Prob0E: 62 iterations in 0.41 seconds (average 0.006645, setup 0.00)

Prob1A: 3 iterations in 0.03 seconds (average 0.010667, setup 0.00)

yes = 1222, no = 154747, maybe = 37999

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=193968, nc=57143, nnz=147350, k=2] [1.9 MB]
Creating vector for yes... [1.5 MB]
Allocating iteration vectors... [2 x 1.5 MB]
TOTAL: [6.4 MB]

Starting iterations...

Iterative method: 91 iterations in 0.24 seconds (average 0.001363, setup 0.12)

Value in the initial state: 0.0017586971430037866

Time for model checking: 0.703 seconds.

Result: 0.0017586971430037866 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmax=? [ F "MissionTimeUp"|"AllAreaSearched"|"Crash" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=0,Objy1=2,Objx2=3,Objy2=1

Prob0A: 3 iterations in 0.08 seconds (average 0.028000, setup 0.00)

Prob1E: 168 iterations in 2.12 seconds (average 0.012643, setup 0.00)

yes = 167160, no = 954, maybe = 25854

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=193968, nc=46181, nnz=132171, k=2] [1.7 MB]
Creating vector for yes... [1.5 MB]
Allocating iteration vectors... [2 x 1.5 MB]
TOTAL: [6.2 MB]

Starting iterations...

Iterative method: 73 iterations in 0.28 seconds (average 0.001260, setup 0.18)

Value in the initial state: 0.24691332849312342

Time for model checking: 3.008 seconds.

Result: 0.24691332849312342 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmin=? [ F "MissionTimeUp"|"AllAreaSearched"|"Crash" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=0,Objy1=2,Objx2=3,Objy2=1

Prob0E: 3 iterations in 0.04 seconds (average 0.013333, setup 0.00)

Prob1A: 43 iterations in 0.33 seconds (average 0.007628, setup 0.00)

yes = 162839, no = 954, maybe = 30175

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=193968, nc=54071, nnz=155102, k=2] [2.0 MB]
Creating vector for yes... [1.5 MB]
Allocating iteration vectors... [2 x 1.5 MB]
TOTAL: [6.5 MB]

Starting iterations...

Iterative method: 84 iterations in 0.25 seconds (average 0.001381, setup 0.14)

Value in the initial state: 0.01902172516183704

Time for model checking: 0.649 seconds.

Result: 0.01902172516183704 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"drop"}max=? [ F "deadlock" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=0,Objy1=2,Objx2=3,Objy2=1

Prob0E: 4 iterations in 0.05 seconds (average 0.012000, setup 0.00)

Prob1A: 1 iterations in 0.00 seconds (average 0.004000, setup 0.00)

goal = 1, inf = 0, maybe = 193967

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=193968, nc=257605, nnz=517240, k=4] [7.6 MB]
Building sparse matrix (transition rewards)... [n=193968, nc=257605, nnz=0, k=4] [1.7 MB]
Creating vector for state rewards... [1.5 MB]
Creating vector for inf... [1.5 MB]
Allocating iteration vectors... [2 x 1.5 MB]
TOTAL: [15.3 MB]

Starting iterations...

Iterative method: 83 iterations in 0.60 seconds (average 0.002843, setup 0.37)

Value in the initial state: 0.18240777262499852

Time for model checking: 0.827 seconds.

Result: 0.18240777262499852 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"drop"}min=? [ F "deadlock" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=0,Objy1=2,Objx2=3,Objy2=1

Prob0A: 4 iterations in 0.03 seconds (average 0.008000, setup 0.00)

Prob1E: 5 iterations in 0.04 seconds (average 0.008000, setup 0.00)

Warning: PRISM hasn't checked for zero-reward loops.
Your minimum rewards may be too low...

goal = 1, inf = 0, maybe = 193967

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=193968, nc=257605, nnz=517240, k=4] [7.6 MB]
Building sparse matrix (transition rewards)... [n=193968, nc=257605, nnz=0, k=4] [1.7 MB]
Creating vector for state rewards... [1.5 MB]
Creating vector for inf... [1.5 MB]
Allocating iteration vectors... [2 x 1.5 MB]
TOTAL: [15.3 MB]

Starting iterations...

Iterative method: 74 iterations in 0.49 seconds (average 0.002865, setup 0.28)

Value in the initial state: 0.15691899355797373

Time for model checking: 0.58 seconds.

Result: 0.15691899355797373 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"charge"}max=? [ F "deadlock" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=0,Objy1=2,Objx2=3,Objy2=1

Prob0E: 4 iterations in 0.02 seconds (average 0.004000, setup 0.00)

Prob1A: 1 iterations in 0.00 seconds (average 0.004000, setup 0.00)

goal = 1, inf = 0, maybe = 193967

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=193968, nc=257605, nnz=517240, k=4] [7.6 MB]
Building sparse matrix (transition rewards)... [n=193968, nc=257605, nnz=5786, k=4] [1.8 MB]
Creating vector for state rewards... [1.5 MB]
Creating vector for inf... [1.5 MB]
Allocating iteration vectors... [2 x 1.5 MB]
TOTAL: [15.4 MB]

Starting iterations...

Iterative method: 80 iterations in 0.50 seconds (average 0.002800, setup 0.28)

Value in the initial state: 0.2740237644464107

Time for model checking: 0.546 seconds.

Result: 0.2740237644464107 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"charge"}min=? [ F "deadlock" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=0,Objy1=2,Objx2=3,Objy2=1

Prob0A: 4 iterations in 0.02 seconds (average 0.006000, setup 0.00)

Prob1E: 5 iterations in 0.04 seconds (average 0.008000, setup 0.00)

Warning: PRISM hasn't checked for zero-reward loops.
Your minimum rewards may be too low...

goal = 1, inf = 0, maybe = 193967

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=193968, nc=257605, nnz=517240, k=4] [7.6 MB]
Building sparse matrix (transition rewards)... [n=193968, nc=257605, nnz=5786, k=4] [1.8 MB]
Creating vector for state rewards... [1.5 MB]
Creating vector for inf... [1.5 MB]
Allocating iteration vectors... [2 x 1.5 MB]
TOTAL: [15.4 MB]

Starting iterations...

Iterative method: 87 iterations in 0.53 seconds (average 0.002851, setup 0.28)

Value in the initial state: 0.04766301162522347

Time for model checking: 0.717 seconds.

Result: 0.04766301162522347 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"time"}max=? [ F "deadlock" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=0,Objy1=2,Objx2=3,Objy2=1

Prob0E: 4 iterations in 0.02 seconds (average 0.005000, setup 0.00)

Prob1A: 1 iterations in 0.01 seconds (average 0.008000, setup 0.00)

goal = 1, inf = 0, maybe = 193967

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=193968, nc=257605, nnz=517240, k=4] [7.6 MB]
Building sparse matrix (transition rewards)... [n=193968, nc=257605, nnz=97586, k=4] [2.8 MB]
Creating vector for state rewards... [1.5 MB]
Creating vector for inf... [1.5 MB]
Allocating iteration vectors... [2 x 1.5 MB]
TOTAL: [16.4 MB]

Starting iterations...

Iterative method: 88 iterations in 0.57 seconds (average 0.002909, setup 0.32)

Value in the initial state: 167.0342523638515

Time for model checking: 0.661 seconds.

Result: 167.0342523638515 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"time"}min=? [ F "deadlock" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=0,Objy1=2,Objx2=3,Objy2=1

Prob0A: 4 iterations in 0.02 seconds (average 0.006000, setup 0.00)

Prob1E: 5 iterations in 0.04 seconds (average 0.008000, setup 0.00)

Warning: PRISM hasn't checked for zero-reward loops.
Your minimum rewards may be too low...

goal = 1, inf = 0, maybe = 193967

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=193968, nc=257605, nnz=517240, k=4] [7.6 MB]
Building sparse matrix (transition rewards)... [n=193968, nc=257605, nnz=97586, k=4] [2.8 MB]
Creating vector for state rewards... [1.5 MB]
Creating vector for inf... [1.5 MB]
Allocating iteration vectors... [2 x 1.5 MB]
TOTAL: [16.4 MB]

Starting iterations...

Iterative method: 92 iterations in 0.58 seconds (average 0.002957, setup 0.31)

Value in the initial state: 93.7002851850023

Time for model checking: 0.656 seconds.

Result: 93.7002851850023 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"success"}max=? [ F "deadlock" ]/Pmin=? [ F "MissionSuccessful" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=0,Objy1=2,Objx2=3,Objy2=1

Prob0E: 4 iterations in 0.02 seconds (average 0.006000, setup 0.00)

Prob1A: 1 iterations in 0.00 seconds (average 0.004000, setup 0.00)

goal = 1, inf = 0, maybe = 193967

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=193968, nc=257605, nnz=517240, k=4] [7.6 MB]
Building sparse matrix (transition rewards)... [n=193968, nc=257605, nnz=1878, k=4] [1.7 MB]
Creating vector for state rewards... [1.5 MB]
Creating vector for inf... [1.5 MB]
Allocating iteration vectors... [2 x 1.5 MB]
TOTAL: [15.3 MB]

Starting iterations...

Iterative method: 88 iterations in 0.52 seconds (average 0.002773, setup 0.28)

Prob0E: 43 iterations in 0.28 seconds (average 0.006419, setup 0.00)

Prob1A: 3 iterations in 0.04 seconds (average 0.012000, setup 0.00)

yes = 953, no = 167161, maybe = 25854

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=193968, nc=46181, nnz=132171, k=2] [1.7 MB]
Creating vector for yes... [1.5 MB]
Allocating iteration vectors... [2 x 1.5 MB]
TOTAL: [6.2 MB]

Starting iterations...

Iterative method: 80 iterations in 0.20 seconds (average 0.001250, setup 0.10)

Value in the initial state: 217.3897994900286

Time for model checking: 1.127 seconds.

Result: 217.3897994900286 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"success"}min=? [ F "deadlock" ]/Pmax=? [ F "MissionSuccessful" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=0,Objy1=2,Objx2=3,Objy2=1

Prob0A: 4 iterations in 0.03 seconds (average 0.008000, setup 0.00)

Prob1E: 5 iterations in 0.04 seconds (average 0.008000, setup 0.00)

Warning: PRISM hasn't checked for zero-reward loops.
Your minimum rewards may be too low...

goal = 1, inf = 0, maybe = 193967

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=193968, nc=257605, nnz=517240, k=4] [7.6 MB]
Building sparse matrix (transition rewards)... [n=193968, nc=257605, nnz=1878, k=4] [1.7 MB]
Creating vector for state rewards... [1.5 MB]
Creating vector for inf... [1.5 MB]
Allocating iteration vectors... [2 x 1.5 MB]
TOTAL: [15.3 MB]

Starting iterations...

Iterative method: 85 iterations in 0.53 seconds (average 0.002871, setup 0.28)

Prob0A: 43 iterations in 0.28 seconds (average 0.006512, setup 0.00)

Prob1E: 4 iterations in 0.02 seconds (average 0.006000, setup 0.00)

yes = 953, no = 162840, maybe = 30175

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=193968, nc=54071, nnz=155102, k=2] [2.0 MB]
Creating vector for yes... [1.5 MB]
Allocating iteration vectors... [2 x 1.5 MB]
TOTAL: [6.5 MB]

Starting iterations...

Iterative method: 86 iterations in 0.25 seconds (average 0.001302, setup 0.14)

Value in the initial state: 78.36818113734745

Time for model checking: 1.193 seconds.

Result: 78.36818113734745 (value in the initial state)

---------------------------------------------------------------------

Note: There were 16 warnings during computation.

