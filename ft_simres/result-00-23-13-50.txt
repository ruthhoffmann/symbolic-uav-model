PRISM
=====

Version: 4.3
Date: Tue Aug 02 12:26:30 BST 2016
Hostname: eglinton
Memory limits: cudd=1g, java(heap)=981.5m
Command line: prism uav_sim_ft_mdp.prism -s uav_mdp.props -const 'Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=1,Objy1=3,Objx2=5,Objy2=0,'

Parsing model file "uav_sim_ft_mdp.prism"...

Parsing properties file "uav_mdp.props"...

18 properties:
(1) Pmax=? [ F "MissionSuccessful" ]
(2) Pmin=? [ F "MissionSuccessful" ]
(3) Pmax=? [ F "MissionTimeUp" ]
(4) Pmin=? [ F "MissionTimeUp" ]
(5) Pmax=? [ F "Crash" ]
(6) Pmin=? [ F "Crash" ]
(7) Pmax=? [ F "AllAreaSearched" ]
(8) Pmin=? [ F "AllAreaSearched" ]
(9) Pmax=? [ F "MissionTimeUp"|"AllAreaSearched"|"Crash" ]
(10) Pmin=? [ F "MissionTimeUp"|"AllAreaSearched"|"Crash" ]
(11) R{"drop"}max=? [ F "deadlock" ]
(12) R{"drop"}min=? [ F "deadlock" ]
(13) R{"charge"}max=? [ F "deadlock" ]
(14) R{"charge"}min=? [ F "deadlock" ]
(15) R{"time"}max=? [ F "deadlock" ]
(16) R{"time"}min=? [ F "deadlock" ]
(17) R{"success"}max=? [ F "deadlock" ]/Pmin=? [ F "MissionSuccessful" ]
(18) R{"success"}min=? [ F "deadlock" ]/Pmax=? [ F "MissionSuccessful" ]

Type:        MDP
Modules:     Obj Move Robot T 
Variables:   NoOfObjs posx posy s1 retx rety b t c 

---------------------------------------------------------------------

Model checking: Pmax=? [ F "MissionSuccessful" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=1,Objy1=3,Objx2=5,Objy2=0

Building model...
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=1,Objy1=3,Objx2=5,Objy2=0

Warning: Guard for command 7 of module "Move" is never satisfied.

Warning: Guard for command 8 of module "Move" is never satisfied.

Warning: Guard for command 9 of module "Move" is never satisfied.

Warning: Guard for command 7 of module "Robot" is never satisfied.

Warning: Update 2 of command 3 of module "Robot" doesn't do anything ("(s1'=13)", line 249, column 44)

Warning: Update 2 of command 27 of module "T" doesn't do anything ("(c'=1)", line 372, column 108)

Warning: Guard for command 28 of module "T" is never satisfied.

Warning: Update 2 of command 9 of module "T" doesn't do anything ("(c'=1)", line 349, column 112)

Warning: Guard for command 10 of module "T" is never satisfied.

Warning: Update 2 of command 51 of module "T" doesn't do anything ("(c'=1)", line 403, column 113)

Warning: Guard for command 52 of module "T" is never satisfied.

Computing reachable states...

Reachability (BFS): 81 iterations in 0.99 seconds (average 0.012198, setup 0.00)

Time for model construction: 2.229 seconds.

Warning: Deadlocks detected and fixed in 1 states

Type:        MDP
States:      198409 (1 initial)
Transitions: 511020
Choices:     259211

Transition matrix: 173482 nodes (76 terminal), 511020 minterms, vars: 34r/34c/18nd

Prob0A: 59 iterations in 0.51 seconds (average 0.008610, setup 0.00)

Prob1E: 4 iterations in 0.07 seconds (average 0.017000, setup 0.00)

yes = 948, no = 169188, maybe = 28273

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=198409, nc=47083, nnz=128911, k=2] [1.7 MB]
Creating vector for yes... [1.5 MB]
Allocating iteration vectors... [2 x 1.5 MB]
TOTAL: [6.3 MB]

Starting iterations...

Iterative method: 97 iterations in 0.34 seconds (average 0.001320, setup 0.22)

Value in the initial state: 0.9704041361344649

Time for model checking: 0.982 seconds.

Result: 0.9704041361344649 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmin=? [ F "MissionSuccessful" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=1,Objy1=3,Objx2=5,Objy2=0

Prob0E: 68 iterations in 0.50 seconds (average 0.007412, setup 0.00)

Prob1A: 3 iterations in 0.04 seconds (average 0.014667, setup 0.00)

yes = 948, no = 172668, maybe = 24793

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=198409, nc=41580, nnz=113533, k=2] [1.5 MB]
Creating vector for yes... [1.5 MB]
Allocating iteration vectors... [2 x 1.5 MB]
TOTAL: [6.1 MB]

Starting iterations...

Iterative method: 92 iterations in 0.28 seconds (average 0.001261, setup 0.16)

Value in the initial state: 0.9333495599985548

Time for model checking: 0.852 seconds.

Result: 0.9333495599985548 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmax=? [ F "MissionTimeUp" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=1,Objy1=3,Objx2=5,Objy2=0

Prob0A: 62 iterations in 0.59 seconds (average 0.009548, setup 0.00)

Prob1E: 4 iterations in 0.04 seconds (average 0.010000, setup 0.00)

yes = 420, no = 112921, maybe = 85068

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=198409, nc=135202, nnz=359587, k=4] [4.4 MB]
Creating vector for yes... [1.5 MB]
Allocating iteration vectors... [2 x 1.5 MB]
TOTAL: [9.0 MB]

Starting iterations...

Iterative method: 94 iterations in 0.42 seconds (average 0.001745, setup 0.26)

Value in the initial state: 9.566931141264847E-4

Time for model checking: 1.069 seconds.

Result: 9.566931141264847E-4 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmin=? [ F "MissionTimeUp" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=1,Objy1=3,Objx2=5,Objy2=0

Prob0E: 68 iterations in 0.78 seconds (average 0.011412, setup 0.00)

Prob1A: 3 iterations in 0.04 seconds (average 0.014667, setup 0.00)

yes = 420, no = 112998, maybe = 84991

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=198409, nc=135081, nnz=359224, k=4] [4.4 MB]
Creating vector for yes... [1.5 MB]
Allocating iteration vectors... [2 x 1.5 MB]
TOTAL: [9.0 MB]

Starting iterations...

Iterative method: 110 iterations in 0.44 seconds (average 0.001782, setup 0.24)

Value in the initial state: 7.954957139028757E-6

Time for model checking: 1.28 seconds.

Result: 7.954957139028757E-6 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmax=? [ F "Crash" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=1,Objy1=3,Objx2=5,Objy2=0

Prob0A: 3 iterations in 0.07 seconds (average 0.024000, setup 0.00)

Prob1E: 240 iterations in 3.11 seconds (average 0.012967, setup 0.00)

yes = 97319, no = 2669, maybe = 98421

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=198409, nc=159223, nnz=411032, k=4] [5.0 MB]
Creating vector for yes... [1.5 MB]
Allocating iteration vectors... [2 x 1.5 MB]
TOTAL: [9.6 MB]

Starting iterations...

Iterative method: 92 iterations in 0.48 seconds (average 0.001913, setup 0.30)

Value in the initial state: 0.06299322059642277

Time for model checking: 3.69 seconds.

Result: 0.06299322059642277 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmin=? [ F "Crash" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=1,Objy1=3,Objx2=5,Objy2=0

Prob0E: 3 iterations in 0.06 seconds (average 0.021333, setup 0.00)

Prob1A: 51 iterations in 0.62 seconds (average 0.012078, setup 0.00)

yes = 97319, no = 2669, maybe = 98421

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=198409, nc=159223, nnz=411032, k=4] [5.0 MB]
Creating vector for yes... [1.5 MB]
Allocating iteration vectors... [2 x 1.5 MB]
TOTAL: [9.6 MB]

Starting iterations...

Iterative method: 96 iterations in 0.42 seconds (average 0.001917, setup 0.24)

Value in the initial state: 0.02773913936010349

Time for model checking: 1.135 seconds.

Result: 0.02773913936010349 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmax=? [ F "AllAreaSearched" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=1,Objy1=3,Objx2=5,Objy2=0

Prob0A: 56 iterations in 0.44 seconds (average 0.007929, setup 0.00)

Prob1E: 4 iterations in 0.04 seconds (average 0.011000, setup 0.00)

yes = 1300, no = 152480, maybe = 44629

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=198409, nc=71231, nnz=187805, k=2] [2.4 MB]
Creating vector for yes... [1.5 MB]
Allocating iteration vectors... [2 x 1.5 MB]
TOTAL: [6.9 MB]

Starting iterations...

Iterative method: 94 iterations in 0.32 seconds (average 0.001532, setup 0.18)

Value in the initial state: 0.0032311977128703837

Time for model checking: 0.822 seconds.

Result: 0.0032311977128703837 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmin=? [ F "AllAreaSearched" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=1,Objy1=3,Objx2=5,Objy2=0

Prob0E: 68 iterations in 0.46 seconds (average 0.006706, setup 0.00)

Prob1A: 3 iterations in 0.04 seconds (average 0.012000, setup 0.00)

yes = 1300, no = 157487, maybe = 39622

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=198409, nc=63682, nnz=167030, k=2] [2.2 MB]
Creating vector for yes... [1.5 MB]
Allocating iteration vectors... [2 x 1.5 MB]
TOTAL: [6.7 MB]

Starting iterations...

Iterative method: 95 iterations in 0.27 seconds (average 0.001389, setup 0.14)

Value in the initial state: 0.0012535501451381122

Time for model checking: 0.779 seconds.

Result: 0.0012535501451381122 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmax=? [ F "MissionTimeUp"|"AllAreaSearched"|"Crash" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=1,Objy1=3,Objx2=5,Objy2=0

Prob0A: 3 iterations in 0.08 seconds (average 0.028000, setup 0.00)

Prob1E: 264 iterations in 3.42 seconds (average 0.012939, setup 0.00)

yes = 172667, no = 949, maybe = 24793

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=198409, nc=41580, nnz=113533, k=2] [1.5 MB]
Creating vector for yes... [1.5 MB]
Allocating iteration vectors... [2 x 1.5 MB]
TOTAL: [6.1 MB]

Starting iterations...

Iterative method: 89 iterations in 0.30 seconds (average 0.001303, setup 0.18)

Value in the initial state: 0.06665040777485733

Time for model checking: 3.82 seconds.

Result: 0.06665040777485733 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmin=? [ F "MissionTimeUp"|"AllAreaSearched"|"Crash" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=1,Objy1=3,Objx2=5,Objy2=0

Prob0E: 3 iterations in 0.07 seconds (average 0.024000, setup 0.00)

Prob1A: 59 iterations in 0.60 seconds (average 0.010102, setup 0.00)

yes = 169187, no = 949, maybe = 28273

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=198409, nc=47083, nnz=128911, k=2] [1.7 MB]
Creating vector for yes... [1.5 MB]
Allocating iteration vectors... [2 x 1.5 MB]
TOTAL: [6.3 MB]

Starting iterations...

Iterative method: 95 iterations in 0.28 seconds (average 0.001305, setup 0.15)

Value in the initial state: 0.02959559448886518

Time for model checking: 0.982 seconds.

Result: 0.02959559448886518 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"drop"}max=? [ F "deadlock" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=1,Objy1=3,Objx2=5,Objy2=0

Prob0E: 4 iterations in 0.08 seconds (average 0.021000, setup 0.00)

Prob1A: 1 iterations in 0.01 seconds (average 0.008000, setup 0.00)

goal = 1, inf = 0, maybe = 198408

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=198409, nc=259210, nnz=511019, k=4] [7.6 MB]
Building sparse matrix (transition rewards)... [n=198409, nc=259210, nnz=0, k=4] [1.7 MB]
Creating vector for state rewards... [1.5 MB]
Creating vector for inf... [1.5 MB]
Allocating iteration vectors... [2 x 1.5 MB]
TOTAL: [15.4 MB]

Starting iterations...

Iterative method: 93 iterations in 0.64 seconds (average 0.002839, setup 0.38)

Value in the initial state: 0.20757641614310607

Time for model checking: 0.758 seconds.

Result: 0.20757641614310607 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"drop"}min=? [ F "deadlock" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=1,Objy1=3,Objx2=5,Objy2=0

Prob0A: 4 iterations in 0.04 seconds (average 0.009000, setup 0.00)

Prob1E: 5 iterations in 0.04 seconds (average 0.008800, setup 0.00)

Warning: PRISM hasn't checked for zero-reward loops.
Your minimum rewards may be too low...

goal = 1, inf = 0, maybe = 198408

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=198409, nc=259210, nnz=511019, k=4] [7.6 MB]
Building sparse matrix (transition rewards)... [n=198409, nc=259210, nnz=0, k=4] [1.7 MB]
Creating vector for state rewards... [1.5 MB]
Creating vector for inf... [1.5 MB]
Allocating iteration vectors... [2 x 1.5 MB]
TOTAL: [15.4 MB]

Starting iterations...

Iterative method: 84 iterations in 0.52 seconds (average 0.002857, setup 0.28)

Value in the initial state: 0.16353402055896296

Time for model checking: 0.621 seconds.

Result: 0.16353402055896296 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"charge"}max=? [ F "deadlock" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=1,Objy1=3,Objx2=5,Objy2=0

Prob0E: 4 iterations in 0.02 seconds (average 0.006000, setup 0.00)

Prob1A: 1 iterations in 0.00 seconds (average 0.004000, setup 0.00)

goal = 1, inf = 0, maybe = 198408

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=198409, nc=259210, nnz=511019, k=4] [7.6 MB]
Building sparse matrix (transition rewards)... [n=198409, nc=259210, nnz=5402, k=4] [1.8 MB]
Creating vector for state rewards... [1.5 MB]
Creating vector for inf... [1.5 MB]
Allocating iteration vectors... [2 x 1.5 MB]
TOTAL: [15.5 MB]

Starting iterations...

Iterative method: 90 iterations in 0.55 seconds (average 0.002933, setup 0.28)

Value in the initial state: 0.9758187911647451

Time for model checking: 0.589 seconds.

Result: 0.9758187911647451 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"charge"}min=? [ F "deadlock" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=1,Objy1=3,Objx2=5,Objy2=0

Prob0A: 4 iterations in 0.02 seconds (average 0.006000, setup 0.00)

Prob1E: 5 iterations in 0.04 seconds (average 0.008000, setup 0.00)

Warning: PRISM hasn't checked for zero-reward loops.
Your minimum rewards may be too low...

goal = 1, inf = 0, maybe = 198408

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=198409, nc=259210, nnz=511019, k=4] [7.6 MB]
Building sparse matrix (transition rewards)... [n=198409, nc=259210, nnz=5402, k=4] [1.8 MB]
Creating vector for state rewards... [1.5 MB]
Creating vector for inf... [1.5 MB]
Allocating iteration vectors... [2 x 1.5 MB]
TOTAL: [15.5 MB]

Starting iterations...

Iterative method: 95 iterations in 0.56 seconds (average 0.002905, setup 0.29)

Value in the initial state: 0.21247403929224398

Time for model checking: 0.64 seconds.

Result: 0.21247403929224398 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"time"}max=? [ F "deadlock" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=1,Objy1=3,Objx2=5,Objy2=0

Prob0E: 4 iterations in 0.02 seconds (average 0.006000, setup 0.00)

Prob1A: 1 iterations in 0.01 seconds (average 0.008000, setup 0.00)

goal = 1, inf = 0, maybe = 198408

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=198409, nc=259210, nnz=511019, k=4] [7.6 MB]
Building sparse matrix (transition rewards)... [n=198409, nc=259210, nnz=99986, k=4] [2.9 MB]
Creating vector for state rewards... [1.5 MB]
Creating vector for inf... [1.5 MB]
Allocating iteration vectors... [2 x 1.5 MB]
TOTAL: [16.5 MB]

Starting iterations...

Iterative method: 94 iterations in 0.68 seconds (average 0.002894, setup 0.41)

Value in the initial state: 346.0739567449648

Time for model checking: 0.729 seconds.

Result: 346.0739567449648 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"time"}min=? [ F "deadlock" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=1,Objy1=3,Objx2=5,Objy2=0

Prob0A: 4 iterations in 0.02 seconds (average 0.006000, setup 0.00)

Prob1E: 5 iterations in 0.04 seconds (average 0.008800, setup 0.00)

Warning: PRISM hasn't checked for zero-reward loops.
Your minimum rewards may be too low...

goal = 1, inf = 0, maybe = 198408

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=198409, nc=259210, nnz=511019, k=4] [7.6 MB]
Building sparse matrix (transition rewards)... [n=198409, nc=259210, nnz=99986, k=4] [2.9 MB]
Creating vector for state rewards... [1.5 MB]
Creating vector for inf... [1.5 MB]
Allocating iteration vectors... [2 x 1.5 MB]
TOTAL: [16.5 MB]

Starting iterations...

Iterative method: 104 iterations in 0.62 seconds (average 0.002923, setup 0.32)

Value in the initial state: 153.13766185066459

Time for model checking: 0.7 seconds.

Result: 153.13766185066459 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"success"}max=? [ F "deadlock" ]/Pmin=? [ F "MissionSuccessful" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=1,Objy1=3,Objx2=5,Objy2=0

Prob0E: 4 iterations in 0.02 seconds (average 0.004000, setup 0.00)

Prob1A: 1 iterations in 0.00 seconds (average 0.004000, setup 0.00)

goal = 1, inf = 0, maybe = 198408

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=198409, nc=259210, nnz=511019, k=4] [7.6 MB]
Building sparse matrix (transition rewards)... [n=198409, nc=259210, nnz=1868, k=4] [1.8 MB]
Creating vector for state rewards... [1.5 MB]
Creating vector for inf... [1.5 MB]
Allocating iteration vectors... [2 x 1.5 MB]
TOTAL: [15.4 MB]

Starting iterations...

Iterative method: 97 iterations in 0.54 seconds (average 0.002763, setup 0.27)

Prob0E: 68 iterations in 0.44 seconds (average 0.006471, setup 0.00)

Prob1A: 3 iterations in 0.04 seconds (average 0.013333, setup 0.00)

yes = 948, no = 172668, maybe = 24793

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=198409, nc=41580, nnz=113533, k=2] [1.5 MB]
Creating vector for yes... [1.5 MB]
Allocating iteration vectors... [2 x 1.5 MB]
TOTAL: [6.1 MB]

Starting iterations...

Iterative method: 92 iterations in 0.25 seconds (average 0.001261, setup 0.13)

Value in the initial state: 362.29575429124566

Time for model checking: 1.332 seconds.

Result: 362.29575429124566 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"success"}min=? [ F "deadlock" ]/Pmax=? [ F "MissionSuccessful" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=1,Objy1=3,Objx2=5,Objy2=0

Prob0A: 4 iterations in 0.09 seconds (average 0.023000, setup 0.00)

Prob1E: 5 iterations in 0.04 seconds (average 0.008000, setup 0.00)

Warning: PRISM hasn't checked for zero-reward loops.
Your minimum rewards may be too low...

goal = 1, inf = 0, maybe = 198408

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=198409, nc=259210, nnz=511019, k=4] [7.6 MB]
Building sparse matrix (transition rewards)... [n=198409, nc=259210, nnz=1868, k=4] [1.8 MB]
Creating vector for state rewards... [1.5 MB]
Creating vector for inf... [1.5 MB]
Allocating iteration vectors... [2 x 1.5 MB]
TOTAL: [15.4 MB]

Starting iterations...

Iterative method: 96 iterations in 0.60 seconds (average 0.002792, setup 0.33)

Prob0A: 59 iterations in 0.25 seconds (average 0.004271, setup 0.00)

Prob1E: 4 iterations in 0.03 seconds (average 0.007000, setup 0.00)

yes = 948, no = 169188, maybe = 28273

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=198409, nc=47083, nnz=128911, k=2] [1.7 MB]
Creating vector for yes... [1.5 MB]
Allocating iteration vectors... [2 x 1.5 MB]
TOTAL: [6.3 MB]

Starting iterations...

Iterative method: 97 iterations in 0.25 seconds (average 0.001237, setup 0.13)

Value in the initial state: 155.03550863016994

Time for model checking: 1.269 seconds.

Result: 155.03550863016994 (value in the initial state)

---------------------------------------------------------------------

Note: There were 16 warnings during computation.

