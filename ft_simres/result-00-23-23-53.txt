PRISM
=====

Version: 4.3
Date: Tue Aug 02 12:28:18 BST 2016
Hostname: eglinton
Memory limits: cudd=1g, java(heap)=981.5m
Command line: prism uav_sim_ft_mdp.prism -s uav_mdp.props -const 'Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=2,Objy1=3,Objx2=5,Objy2=3,'

Parsing model file "uav_sim_ft_mdp.prism"...

Parsing properties file "uav_mdp.props"...

18 properties:
(1) Pmax=? [ F "MissionSuccessful" ]
(2) Pmin=? [ F "MissionSuccessful" ]
(3) Pmax=? [ F "MissionTimeUp" ]
(4) Pmin=? [ F "MissionTimeUp" ]
(5) Pmax=? [ F "Crash" ]
(6) Pmin=? [ F "Crash" ]
(7) Pmax=? [ F "AllAreaSearched" ]
(8) Pmin=? [ F "AllAreaSearched" ]
(9) Pmax=? [ F "MissionTimeUp"|"AllAreaSearched"|"Crash" ]
(10) Pmin=? [ F "MissionTimeUp"|"AllAreaSearched"|"Crash" ]
(11) R{"drop"}max=? [ F "deadlock" ]
(12) R{"drop"}min=? [ F "deadlock" ]
(13) R{"charge"}max=? [ F "deadlock" ]
(14) R{"charge"}min=? [ F "deadlock" ]
(15) R{"time"}max=? [ F "deadlock" ]
(16) R{"time"}min=? [ F "deadlock" ]
(17) R{"success"}max=? [ F "deadlock" ]/Pmin=? [ F "MissionSuccessful" ]
(18) R{"success"}min=? [ F "deadlock" ]/Pmax=? [ F "MissionSuccessful" ]

Type:        MDP
Modules:     Obj Move Robot T 
Variables:   NoOfObjs posx posy s1 retx rety b t c 

---------------------------------------------------------------------

Model checking: Pmax=? [ F "MissionSuccessful" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=2,Objy1=3,Objx2=5,Objy2=3

Building model...
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=2,Objy1=3,Objx2=5,Objy2=3

Warning: Guard for command 7 of module "Move" is never satisfied.

Warning: Guard for command 8 of module "Move" is never satisfied.

Warning: Guard for command 9 of module "Move" is never satisfied.

Warning: Guard for command 7 of module "Robot" is never satisfied.

Warning: Update 2 of command 3 of module "Robot" doesn't do anything ("(s1'=13)", line 249, column 44)

Warning: Update 2 of command 27 of module "T" doesn't do anything ("(c'=1)", line 372, column 108)

Warning: Guard for command 28 of module "T" is never satisfied.

Warning: Update 2 of command 9 of module "T" doesn't do anything ("(c'=1)", line 349, column 112)

Warning: Guard for command 10 of module "T" is never satisfied.

Warning: Update 2 of command 51 of module "T" doesn't do anything ("(c'=1)", line 403, column 113)

Warning: Guard for command 52 of module "T" is never satisfied.

Computing reachable states...

Reachability (BFS): 82 iterations in 0.44 seconds (average 0.005415, setup 0.00)

Time for model construction: 1.454 seconds.

Warning: Deadlocks detected and fixed in 1 states

Type:        MDP
States:      90220 (1 initial)
Transitions: 249135
Choices:     122129

Transition matrix: 149419 nodes (76 terminal), 249135 minterms, vars: 34r/34c/18nd

Prob0A: 67 iterations in 0.34 seconds (average 0.005075, setup 0.00)

Prob1E: 4 iterations in 0.04 seconds (average 0.011000, setup 0.00)

yes = 210, no = 83838, maybe = 6172

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=90220, nc=11250, nnz=33293, k=2] [489.2 KB]
Creating vector for yes... [704.8 KB]
Allocating iteration vectors... [2 x 704.8 KB]
TOTAL: [2.5 MB]

Starting iterations...

Iterative method: 98 iterations in 0.18 seconds (average 0.000571, setup 0.12)

Value in the initial state: 0.010942887635835593

Time for model checking: 0.588 seconds.

Result: 0.010942887635835593 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmin=? [ F "MissionSuccessful" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=2,Objy1=3,Objx2=5,Objy2=3

Prob0E: 22 iterations in 0.10 seconds (average 0.004364, setup 0.00)

Prob1A: 3 iterations in 0.02 seconds (average 0.006667, setup 0.00)

yes = 210, no = 84900, maybe = 5110

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=90220, nc=9352, nnz=27541, k=2] [420.0 KB]
Creating vector for yes... [704.8 KB]
Allocating iteration vectors... [2 x 704.8 KB]
TOTAL: [2.5 MB]

Starting iterations...

Iterative method: 42 iterations in 0.06 seconds (average 0.000571, setup 0.04)

Value in the initial state: 0.0

Time for model checking: 0.188 seconds.

Result: 0.0 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmax=? [ F "MissionTimeUp" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=2,Objy1=3,Objx2=5,Objy2=3

Prob0A: 71 iterations in 0.36 seconds (average 0.005127, setup 0.00)

Prob1E: 4 iterations in 0.03 seconds (average 0.008000, setup 0.00)

yes = 172, no = 59101, maybe = 30947

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=90220, nc=56018, nnz=160980, k=4] [2.0 MB]
Creating vector for yes... [704.8 KB]
Allocating iteration vectors... [2 x 704.8 KB]
TOTAL: [4.0 MB]

Starting iterations...

Iterative method: 94 iterations in 0.25 seconds (average 0.000809, setup 0.17)

Value in the initial state: 9.168015667082824E-5

Time for model checking: 0.654 seconds.

Result: 9.168015667082824E-5 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmin=? [ F "MissionTimeUp" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=2,Objy1=3,Objx2=5,Objy2=3

Prob0E: 78 iterations in 0.48 seconds (average 0.006103, setup 0.00)

Prob1A: 3 iterations in 0.03 seconds (average 0.010667, setup 0.00)

yes = 172, no = 59353, maybe = 30695

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=90220, nc=55566, nnz=160016, k=4] [2.0 MB]
Creating vector for yes... [704.8 KB]
Allocating iteration vectors... [2 x 704.8 KB]
TOTAL: [4.0 MB]

Starting iterations...

Iterative method: 109 iterations in 0.22 seconds (average 0.000771, setup 0.14)

Value in the initial state: 2.3453311881904166E-7

Time for model checking: 0.743 seconds.

Result: 2.3453311881904166E-7 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmax=? [ F "Crash" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=2,Objy1=3,Objx2=5,Objy2=3

Prob0A: 3 iterations in 0.05 seconds (average 0.016000, setup 0.00)

Prob1E: 176 iterations in 1.33 seconds (average 0.007545, setup 0.00)

yes = 44062, no = 1661, maybe = 44497

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=90220, nc=76406, nnz=203412, k=4] [2.5 MB]
Creating vector for yes... [704.8 KB]
Allocating iteration vectors... [2 x 704.8 KB]
TOTAL: [4.6 MB]

Starting iterations...

Iterative method: 89 iterations in 0.27 seconds (average 0.000854, setup 0.19)

Value in the initial state: 0.016936580309309904

Time for model checking: 1.652 seconds.

Result: 0.016936580309309904 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmin=? [ F "Crash" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=2,Objy1=3,Objx2=5,Objy2=3

Prob0E: 3 iterations in 0.04 seconds (average 0.014667, setup 0.00)

Prob1A: 46 iterations in 0.28 seconds (average 0.006000, setup 0.00)

yes = 44062, no = 1661, maybe = 44497

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=90220, nc=76406, nnz=203412, k=4] [2.5 MB]
Creating vector for yes... [704.8 KB]
Allocating iteration vectors... [2 x 704.8 KB]
TOTAL: [4.6 MB]

Starting iterations...

Iterative method: 83 iterations in 0.23 seconds (average 0.000819, setup 0.16)

Value in the initial state: 0.013124349331379287

Time for model checking: 0.567 seconds.

Result: 0.013124349331379287 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmax=? [ F "AllAreaSearched" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=2,Objy1=3,Objx2=5,Objy2=3

Prob0A: 46 iterations in 0.26 seconds (average 0.005739, setup 0.00)

Prob1E: 4 iterations in 0.03 seconds (average 0.008000, setup 0.00)

yes = 1278, no = 59769, maybe = 29173

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=90220, nc=48735, nnz=132344, k=2] [1.6 MB]
Creating vector for yes... [704.8 KB]
Allocating iteration vectors... [2 x 704.8 KB]
TOTAL: [3.7 MB]

Starting iterations...

Iterative method: 88 iterations in 0.21 seconds (average 0.000727, setup 0.15)

Value in the initial state: 0.9868734364351356

Time for model checking: 0.497 seconds.

Result: 0.9868734364351356 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmin=? [ F "AllAreaSearched" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=2,Objy1=3,Objx2=5,Objy2=3

Prob0E: 46 iterations in 0.26 seconds (average 0.005739, setup 0.00)

Prob1A: 3 iterations in 0.03 seconds (average 0.009333, setup 0.00)

yes = 1278, no = 62121, maybe = 26821

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=90220, nc=44278, nnz=118962, k=2] [1.5 MB]
Creating vector for yes... [704.8 KB]
Allocating iteration vectors... [2 x 704.8 KB]
TOTAL: [3.6 MB]

Starting iterations...

Iterative method: 89 iterations in 0.18 seconds (average 0.000629, setup 0.13)

Value in the initial state: 0.9722238974215577

Time for model checking: 0.492 seconds.

Result: 0.9722238974215577 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmax=? [ F "MissionTimeUp"|"AllAreaSearched"|"Crash" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=2,Objy1=3,Objx2=5,Objy2=3

Prob0A: 3 iterations in 0.06 seconds (average 0.018667, setup 0.00)

Prob1E: 84 iterations in 0.58 seconds (average 0.006857, setup 0.00)

yes = 84899, no = 211, maybe = 5110

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=90220, nc=9352, nnz=27541, k=2] [420.0 KB]
Creating vector for yes... [704.8 KB]
Allocating iteration vectors... [2 x 704.8 KB]
TOTAL: [2.5 MB]

Starting iterations...

Iterative method: 40 iterations in 0.07 seconds (average 0.000500, setup 0.05)

Value in the initial state: 1.0

Time for model checking: 0.719 seconds.

Result: 1.0 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmin=? [ F "MissionTimeUp"|"AllAreaSearched"|"Crash" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=2,Objy1=3,Objx2=5,Objy2=3

Prob0E: 3 iterations in 0.02 seconds (average 0.006667, setup 0.00)

Prob1A: 67 iterations in 0.31 seconds (average 0.004657, setup 0.00)

yes = 83837, no = 211, maybe = 6172

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=90220, nc=11250, nnz=33293, k=2] [489.2 KB]
Creating vector for yes... [704.8 KB]
Allocating iteration vectors... [2 x 704.8 KB]
TOTAL: [2.5 MB]

Starting iterations...

Iterative method: 73 iterations in 0.09 seconds (average 0.000493, setup 0.06)

Value in the initial state: 0.9890541208476996

Time for model checking: 0.447 seconds.

Result: 0.9890541208476996 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"drop"}max=? [ F "deadlock" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=2,Objy1=3,Objx2=5,Objy2=3

Prob0E: 4 iterations in 0.05 seconds (average 0.013000, setup 0.00)

Prob1A: 1 iterations in 0.00 seconds (average 0.004000, setup 0.00)

goal = 1, inf = 0, maybe = 90219

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=90220, nc=122128, nnz=249134, k=4] [3.7 MB]
Building sparse matrix (transition rewards)... [n=90220, nc=122128, nnz=0, k=4] [829.5 KB]
Creating vector for state rewards... [704.8 KB]
Creating vector for inf... [704.8 KB]
Allocating iteration vectors... [2 x 704.8 KB]
TOTAL: [7.2 MB]

Starting iterations...

Iterative method: 84 iterations in 0.34 seconds (average 0.001286, setup 0.23)

Value in the initial state: 0.11095833622026777

Time for model checking: 0.403 seconds.

Result: 0.11095833622026777 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"drop"}min=? [ F "deadlock" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=2,Objy1=3,Objx2=5,Objy2=3

Prob0A: 4 iterations in 0.03 seconds (average 0.008000, setup 0.00)

Prob1E: 5 iterations in 0.03 seconds (average 0.006400, setup 0.00)

Warning: PRISM hasn't checked for zero-reward loops.
Your minimum rewards may be too low...

goal = 1, inf = 0, maybe = 90219

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=90220, nc=122128, nnz=249134, k=4] [3.7 MB]
Building sparse matrix (transition rewards)... [n=90220, nc=122128, nnz=0, k=4] [829.5 KB]
Creating vector for state rewards... [704.8 KB]
Creating vector for inf... [704.8 KB]
Allocating iteration vectors... [2 x 704.8 KB]
TOTAL: [7.2 MB]

Starting iterations...

Iterative method: 76 iterations in 0.25 seconds (average 0.001316, setup 0.15)

Value in the initial state: 0.1091982126185615

Time for model checking: 0.319 seconds.

Result: 0.1091982126185615 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"charge"}max=? [ F "deadlock" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=2,Objy1=3,Objx2=5,Objy2=3

Prob0E: 4 iterations in 0.02 seconds (average 0.006000, setup 0.00)

Prob1A: 1 iterations in 0.00 seconds (average 0.000000, setup 0.00)

goal = 1, inf = 0, maybe = 90219

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=90220, nc=122128, nnz=249134, k=4] [3.7 MB]
Building sparse matrix (transition rewards)... [n=90220, nc=122128, nnz=2949, k=4] [864.0 KB]
Creating vector for state rewards... [704.8 KB]
Creating vector for inf... [704.8 KB]
Allocating iteration vectors... [2 x 704.8 KB]
TOTAL: [7.3 MB]

Starting iterations...

Iterative method: 89 iterations in 0.27 seconds (average 0.001303, setup 0.15)

Value in the initial state: 0.026850249011515784

Time for model checking: 0.297 seconds.

Result: 0.026850249011515784 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"charge"}min=? [ F "deadlock" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=2,Objy1=3,Objx2=5,Objy2=3

Prob0A: 4 iterations in 0.02 seconds (average 0.004000, setup 0.00)

Prob1E: 5 iterations in 0.03 seconds (average 0.005600, setup 0.00)

Warning: PRISM hasn't checked for zero-reward loops.
Your minimum rewards may be too low...

goal = 1, inf = 0, maybe = 90219

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=90220, nc=122128, nnz=249134, k=4] [3.7 MB]
Building sparse matrix (transition rewards)... [n=90220, nc=122128, nnz=2949, k=4] [864.0 KB]
Creating vector for state rewards... [704.8 KB]
Creating vector for inf... [704.8 KB]
Allocating iteration vectors... [2 x 704.8 KB]
TOTAL: [7.3 MB]

Starting iterations...

Iterative method: 98 iterations in 0.28 seconds (average 0.001347, setup 0.15)

Value in the initial state: 0.003487465177312746

Time for model checking: 0.334 seconds.

Result: 0.003487465177312746 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"time"}max=? [ F "deadlock" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=2,Objy1=3,Objx2=5,Objy2=3

Prob0E: 4 iterations in 0.02 seconds (average 0.004000, setup 0.00)

Prob1A: 1 iterations in 0.00 seconds (average 0.004000, setup 0.00)

goal = 1, inf = 0, maybe = 90219

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=90220, nc=122128, nnz=249134, k=4] [3.7 MB]
Building sparse matrix (transition rewards)... [n=90220, nc=122128, nnz=45721, k=4] [1.3 MB]
Creating vector for state rewards... [704.8 KB]
Creating vector for inf... [704.8 KB]
Allocating iteration vectors... [2 x 704.8 KB]
TOTAL: [7.7 MB]

Starting iterations...

Iterative method: 93 iterations in 0.30 seconds (average 0.001333, setup 0.17)

Value in the initial state: 93.89434964270073

Time for model checking: 0.321 seconds.

Result: 93.89434964270073 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"time"}min=? [ F "deadlock" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=2,Objy1=3,Objx2=5,Objy2=3

Prob0A: 4 iterations in 0.02 seconds (average 0.004000, setup 0.00)

Prob1E: 5 iterations in 0.03 seconds (average 0.005600, setup 0.00)

Warning: PRISM hasn't checked for zero-reward loops.
Your minimum rewards may be too low...

goal = 1, inf = 0, maybe = 90219

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=90220, nc=122128, nnz=249134, k=4] [3.7 MB]
Building sparse matrix (transition rewards)... [n=90220, nc=122128, nnz=45721, k=4] [1.3 MB]
Creating vector for state rewards... [704.8 KB]
Creating vector for inf... [704.8 KB]
Allocating iteration vectors... [2 x 704.8 KB]
TOTAL: [7.7 MB]

Starting iterations...

Iterative method: 95 iterations in 0.30 seconds (average 0.001347, setup 0.17)

Value in the initial state: 72.86080048835784

Time for model checking: 0.344 seconds.

Result: 72.86080048835784 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"success"}max=? [ F "deadlock" ]/Pmin=? [ F "MissionSuccessful" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=2,Objy1=3,Objx2=5,Objy2=3

Prob0E: 4 iterations in 0.02 seconds (average 0.004000, setup 0.00)

Prob1A: 1 iterations in 0.00 seconds (average 0.000000, setup 0.00)

goal = 1, inf = 0, maybe = 90219

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=90220, nc=122128, nnz=249134, k=4] [3.7 MB]
Building sparse matrix (transition rewards)... [n=90220, nc=122128, nnz=414, k=4] [834.3 KB]
Creating vector for state rewards... [704.8 KB]
Creating vector for inf... [704.8 KB]
Allocating iteration vectors... [2 x 704.8 KB]
TOTAL: [7.2 MB]

Starting iterations...

Iterative method: 95 iterations in 0.28 seconds (average 0.001305, setup 0.15)

Prob0E: 22 iterations in 0.05 seconds (average 0.002182, setup 0.00)

Prob1A: 3 iterations in 0.02 seconds (average 0.006667, setup 0.00)

yes = 210, no = 84900, maybe = 5110

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=90220, nc=9352, nnz=27541, k=2] [420.0 KB]
Creating vector for yes... [704.8 KB]
Allocating iteration vectors... [2 x 704.8 KB]
TOTAL: [2.5 MB]

Starting iterations...

Iterative method: 42 iterations in 0.06 seconds (average 0.000476, setup 0.04)

Value in the initial state: Infinity

Time for model checking: 0.444 seconds.

Result: Infinity (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"success"}min=? [ F "deadlock" ]/Pmax=? [ F "MissionSuccessful" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=2,Objy1=3,Objx2=5,Objy2=3

Prob0A: 4 iterations in 0.02 seconds (average 0.006000, setup 0.00)

Prob1E: 5 iterations in 0.03 seconds (average 0.006400, setup 0.00)

Warning: PRISM hasn't checked for zero-reward loops.
Your minimum rewards may be too low...

goal = 1, inf = 0, maybe = 90219

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=90220, nc=122128, nnz=249134, k=4] [3.7 MB]
Building sparse matrix (transition rewards)... [n=90220, nc=122128, nnz=414, k=4] [834.3 KB]
Creating vector for state rewards... [704.8 KB]
Creating vector for inf... [704.8 KB]
Allocating iteration vectors... [2 x 704.8 KB]
TOTAL: [7.2 MB]

Starting iterations...

Iterative method: 47 iterations in 0.24 seconds (average 0.001362, setup 0.18)

Prob0A: 67 iterations in 0.18 seconds (average 0.002627, setup 0.00)

Prob1E: 4 iterations in 0.02 seconds (average 0.006000, setup 0.00)

yes = 210, no = 83838, maybe = 6172

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=90220, nc=11250, nnz=33293, k=2] [489.2 KB]
Creating vector for yes... [704.8 KB]
Allocating iteration vectors... [2 x 704.8 KB]
TOTAL: [2.5 MB]

Starting iterations...

Iterative method: 98 iterations in 0.12 seconds (average 0.000531, setup 0.06)

Value in the initial state: 0.0

Time for model checking: 0.618 seconds.

Result: 0.0 (value in the initial state)

---------------------------------------------------------------------

Note: There were 16 warnings during computation.

