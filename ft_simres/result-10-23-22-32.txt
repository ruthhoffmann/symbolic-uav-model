PRISM
=====

Version: 4.3
Date: Tue Aug 02 12:39:06 BST 2016
Hostname: eglinton
Memory limits: cudd=1g, java(heap)=981.5m
Command line: prism uav_sim_ft_mdp.prism -s uav_mdp.props -const 'Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=2,Objy1=2,Objx2=3,Objy2=2,'

Parsing model file "uav_sim_ft_mdp.prism"...

Parsing properties file "uav_mdp.props"...

18 properties:
(1) Pmax=? [ F "MissionSuccessful" ]
(2) Pmin=? [ F "MissionSuccessful" ]
(3) Pmax=? [ F "MissionTimeUp" ]
(4) Pmin=? [ F "MissionTimeUp" ]
(5) Pmax=? [ F "Crash" ]
(6) Pmin=? [ F "Crash" ]
(7) Pmax=? [ F "AllAreaSearched" ]
(8) Pmin=? [ F "AllAreaSearched" ]
(9) Pmax=? [ F "MissionTimeUp"|"AllAreaSearched"|"Crash" ]
(10) Pmin=? [ F "MissionTimeUp"|"AllAreaSearched"|"Crash" ]
(11) R{"drop"}max=? [ F "deadlock" ]
(12) R{"drop"}min=? [ F "deadlock" ]
(13) R{"charge"}max=? [ F "deadlock" ]
(14) R{"charge"}min=? [ F "deadlock" ]
(15) R{"time"}max=? [ F "deadlock" ]
(16) R{"time"}min=? [ F "deadlock" ]
(17) R{"success"}max=? [ F "deadlock" ]/Pmin=? [ F "MissionSuccessful" ]
(18) R{"success"}min=? [ F "deadlock" ]/Pmax=? [ F "MissionSuccessful" ]

Type:        MDP
Modules:     Obj Move Robot T 
Variables:   NoOfObjs posx posy s1 retx rety b t c 

---------------------------------------------------------------------

Model checking: Pmax=? [ F "MissionSuccessful" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=2,Objy1=2,Objx2=3,Objy2=2

Building model...
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=2,Objy1=2,Objx2=3,Objy2=2

Warning: Guard for command 7 of module "Move" is never satisfied.

Warning: Guard for command 8 of module "Move" is never satisfied.

Warning: Guard for command 9 of module "Move" is never satisfied.

Warning: Guard for command 7 of module "Robot" is never satisfied.

Warning: Update 2 of command 3 of module "Robot" doesn't do anything ("(s1'=13)", line 249, column 44)

Warning: Update 2 of command 27 of module "T" doesn't do anything ("(c'=1)", line 372, column 108)

Warning: Guard for command 28 of module "T" is never satisfied.

Warning: Update 2 of command 9 of module "T" doesn't do anything ("(c'=1)", line 349, column 112)

Warning: Guard for command 10 of module "T" is never satisfied.

Warning: Update 2 of command 51 of module "T" doesn't do anything ("(c'=1)", line 403, column 113)

Warning: Guard for command 52 of module "T" is never satisfied.

Computing reachable states...

Reachability (BFS): 79 iterations in 0.61 seconds (average 0.007696, setup 0.00)

Time for model construction: 1.696 seconds.

Warning: Deadlocks detected and fixed in 1 states

Type:        MDP
States:      138078 (1 initial)
Transitions: 365343
Choices:     184066

Transition matrix: 153838 nodes (76 terminal), 365343 minterms, vars: 34r/34c/18nd

Prob0A: 44 iterations in 0.24 seconds (average 0.005455, setup 0.00)

Prob1E: 4 iterations in 0.04 seconds (average 0.010000, setup 0.00)

yes = 956, no = 114771, maybe = 22351

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=138078, nc=40859, nnz=115399, k=2] [1.5 MB]
Creating vector for yes... [1.1 MB]
Allocating iteration vectors... [2 x 1.1 MB]
TOTAL: [4.7 MB]

Starting iterations...

Iterative method: 83 iterations in 0.25 seconds (average 0.000916, setup 0.17)

Value in the initial state: 0.9815538546182523

Time for model checking: 0.547 seconds.

Result: 0.9815538546182523 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmin=? [ F "MissionSuccessful" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=2,Objy1=2,Objx2=3,Objy2=2

Prob0E: 44 iterations in 0.26 seconds (average 0.005818, setup 0.00)

Prob1A: 3 iterations in 0.04 seconds (average 0.012000, setup 0.00)

yes = 956, no = 117795, maybe = 19327

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=138078, nc=35245, nnz=98904, k=2] [1.3 MB]
Creating vector for yes... [1.1 MB]
Allocating iteration vectors... [2 x 1.1 MB]
TOTAL: [4.5 MB]

Starting iterations...

Iterative method: 80 iterations in 0.19 seconds (average 0.000900, setup 0.12)

Value in the initial state: 0.7833203413823283

Time for model checking: 0.506 seconds.

Result: 0.7833203413823283 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmax=? [ F "MissionTimeUp" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=2,Objy1=2,Objx2=3,Objy2=2

Prob0A: 67 iterations in 0.43 seconds (average 0.006388, setup 0.00)

Prob1E: 4 iterations in 0.04 seconds (average 0.009000, setup 0.00)

yes = 232, no = 91797, maybe = 46049

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=138078, nc=82309, nnz=229963, k=4] [2.8 MB]
Creating vector for yes... [1.1 MB]
Allocating iteration vectors... [2 x 1.1 MB]
TOTAL: [6.0 MB]

Starting iterations...

Iterative method: 85 iterations in 0.30 seconds (average 0.001129, setup 0.20)

Value in the initial state: 1.9902098659583872E-4

Time for model checking: 0.776 seconds.

Result: 1.9902098659583872E-4 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmin=? [ F "MissionTimeUp" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=2,Objy1=2,Objx2=3,Objy2=2

Prob0E: 74 iterations in 0.42 seconds (average 0.005676, setup 0.00)

Prob1A: 3 iterations in 0.03 seconds (average 0.010667, setup 0.00)

yes = 232, no = 92097, maybe = 45749

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=138078, nc=81785, nnz=228771, k=4] [2.8 MB]
Creating vector for yes... [1.1 MB]
Allocating iteration vectors... [2 x 1.1 MB]
TOTAL: [6.0 MB]

Starting iterations...

Iterative method: 100 iterations in 0.28 seconds (average 0.001120, setup 0.17)

Value in the initial state: 3.734355403470982E-7

Time for model checking: 0.753 seconds.

Result: 3.734355403470982E-7 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmax=? [ F "Crash" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=2,Objy1=2,Objx2=3,Objy2=2

Prob0A: 3 iterations in 0.06 seconds (average 0.018667, setup 0.00)

Prob1E: 168 iterations in 1.65 seconds (average 0.009833, setup 0.00)

yes = 67737, no = 2441, maybe = 67900

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=138078, nc=113888, nnz=295165, k=4] [3.6 MB]
Creating vector for yes... [1.1 MB]
Allocating iteration vectors... [2 x 1.1 MB]
TOTAL: [6.8 MB]

Starting iterations...

Iterative method: 82 iterations in 0.34 seconds (average 0.001268, setup 0.24)

Value in the initial state: 0.028505439970097766

Time for model checking: 2.064 seconds.

Result: 0.028505439970097766 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmin=? [ F "Crash" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=2,Objy1=2,Objx2=3,Objy2=2

Prob0E: 3 iterations in 0.05 seconds (average 0.016000, setup 0.00)

Prob1A: 44 iterations in 0.36 seconds (average 0.008182, setup 0.00)

yes = 67737, no = 2441, maybe = 67900

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=138078, nc=113888, nnz=295165, k=4] [3.6 MB]
Creating vector for yes... [1.1 MB]
Allocating iteration vectors... [2 x 1.1 MB]
TOTAL: [6.8 MB]

Starting iterations...

Iterative method: 77 iterations in 0.26 seconds (average 0.001351, setup 0.16)

Value in the initial state: 0.01638315339090667

Time for model checking: 0.688 seconds.

Result: 0.01638315339090667 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmax=? [ F "AllAreaSearched" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=2,Objy1=2,Objx2=3,Objy2=2

Prob0A: 50 iterations in 0.28 seconds (average 0.005680, setup 0.00)

Prob1E: 4 iterations in 0.03 seconds (average 0.008000, setup 0.00)

yes = 1252, no = 101750, maybe = 35076

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=138078, nc=55002, nnz=143127, k=2] [1.8 MB]
Creating vector for yes... [1.1 MB]
Allocating iteration vectors... [2 x 1.1 MB]
TOTAL: [5.0 MB]

Starting iterations...

Iterative method: 81 iterations in 0.22 seconds (average 0.000988, setup 0.14)

Value in the initial state: 0.19068433547274843

Time for model checking: 0.546 seconds.

Result: 0.19068433547274843 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmin=? [ F "AllAreaSearched" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=2,Objy1=2,Objx2=3,Objy2=2

Prob0E: 60 iterations in 0.34 seconds (average 0.005667, setup 0.00)

Prob1A: 3 iterations in 0.03 seconds (average 0.009333, setup 0.00)

yes = 1252, no = 105975, maybe = 30851

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=138078, nc=47215, nnz=120016, k=2] [1.6 MB]
Creating vector for yes... [1.1 MB]
Allocating iteration vectors... [2 x 1.1 MB]
TOTAL: [4.7 MB]

Starting iterations...

Iterative method: 88 iterations in 0.18 seconds (average 0.000955, setup 0.10)

Value in the initial state: 0.0017860920497887824

Time for model checking: 0.564 seconds.

Result: 0.0017860920497887824 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmax=? [ F "MissionTimeUp"|"AllAreaSearched"|"Crash" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=2,Objy1=2,Objx2=3,Objy2=2

Prob0A: 3 iterations in 0.08 seconds (average 0.026667, setup 0.00)

Prob1E: 172 iterations in 1.56 seconds (average 0.009093, setup 0.00)

yes = 117794, no = 957, maybe = 19327

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=138078, nc=35245, nnz=98904, k=2] [1.3 MB]
Creating vector for yes... [1.1 MB]
Allocating iteration vectors... [2 x 1.1 MB]
TOTAL: [4.5 MB]

Starting iterations...

Iterative method: 73 iterations in 0.20 seconds (average 0.000932, setup 0.13)

Value in the initial state: 0.2166794713719899

Time for model checking: 1.83 seconds.

Result: 0.2166794713719899 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmin=? [ F "MissionTimeUp"|"AllAreaSearched"|"Crash" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=2,Objy1=2,Objx2=3,Objy2=2

Prob0E: 3 iterations in 0.04 seconds (average 0.013333, setup 0.00)

Prob1A: 44 iterations in 0.26 seconds (average 0.005909, setup 0.00)

yes = 114770, no = 957, maybe = 22351

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=138078, nc=40859, nnz=115399, k=2] [1.5 MB]
Creating vector for yes... [1.1 MB]
Allocating iteration vectors... [2 x 1.1 MB]
TOTAL: [4.7 MB]

Starting iterations...

Iterative method: 81 iterations in 0.17 seconds (average 0.000938, setup 0.10)

Value in the initial state: 0.018445200914868707

Time for model checking: 0.475 seconds.

Result: 0.018445200914868707 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"drop"}max=? [ F "deadlock" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=2,Objy1=2,Objx2=3,Objy2=2

Prob0E: 4 iterations in 0.04 seconds (average 0.010000, setup 0.00)

Prob1A: 1 iterations in 0.00 seconds (average 0.004000, setup 0.00)

goal = 1, inf = 0, maybe = 138077

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=138078, nc=184065, nnz=365342, k=4] [5.4 MB]
Building sparse matrix (transition rewards)... [n=138078, nc=184065, nnz=0, k=4] [1.2 MB]
Creating vector for state rewards... [1.1 MB]
Creating vector for inf... [1.1 MB]
Allocating iteration vectors... [2 x 1.1 MB]
TOTAL: [10.9 MB]

Starting iterations...

Iterative method: 80 iterations in 0.43 seconds (average 0.001950, setup 0.27)

Value in the initial state: 0.11938417300764348

Time for model checking: 0.486 seconds.

Result: 0.11938417300764348 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"drop"}min=? [ F "deadlock" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=2,Objy1=2,Objx2=3,Objy2=2

Prob0A: 4 iterations in 0.02 seconds (average 0.006000, setup 0.00)

Prob1E: 5 iterations in 0.03 seconds (average 0.006400, setup 0.00)

Warning: PRISM hasn't checked for zero-reward loops.
Your minimum rewards may be too low...

goal = 1, inf = 0, maybe = 138077

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=138078, nc=184065, nnz=365342, k=4] [5.4 MB]
Building sparse matrix (transition rewards)... [n=138078, nc=184065, nnz=0, k=4] [1.2 MB]
Creating vector for state rewards... [1.1 MB]
Creating vector for inf... [1.1 MB]
Allocating iteration vectors... [2 x 1.1 MB]
TOTAL: [10.9 MB]

Starting iterations...

Iterative method: 73 iterations in 0.35 seconds (average 0.001973, setup 0.21)

Value in the initial state: 0.10772640537839212

Time for model checking: 0.412 seconds.

Result: 0.10772640537839212 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"charge"}max=? [ F "deadlock" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=2,Objy1=2,Objx2=3,Objy2=2

Prob0E: 4 iterations in 0.02 seconds (average 0.006000, setup 0.00)

Prob1A: 1 iterations in 0.00 seconds (average 0.004000, setup 0.00)

goal = 1, inf = 0, maybe = 138077

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=138078, nc=184065, nnz=365342, k=4] [5.4 MB]
Building sparse matrix (transition rewards)... [n=138078, nc=184065, nnz=3948, k=4] [1.3 MB]
Creating vector for state rewards... [1.1 MB]
Creating vector for inf... [1.1 MB]
Allocating iteration vectors... [2 x 1.1 MB]
TOTAL: [10.9 MB]

Starting iterations...

Iterative method: 79 iterations in 0.36 seconds (average 0.002025, setup 0.20)

Value in the initial state: 0.23425741389563615

Time for model checking: 0.392 seconds.

Result: 0.23425741389563615 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"charge"}min=? [ F "deadlock" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=2,Objy1=2,Objx2=3,Objy2=2

Prob0A: 4 iterations in 0.02 seconds (average 0.005000, setup 0.00)

Prob1E: 5 iterations in 0.03 seconds (average 0.006400, setup 0.00)

Warning: PRISM hasn't checked for zero-reward loops.
Your minimum rewards may be too low...

goal = 1, inf = 0, maybe = 138077

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=138078, nc=184065, nnz=365342, k=4] [5.4 MB]
Building sparse matrix (transition rewards)... [n=138078, nc=184065, nnz=3948, k=4] [1.3 MB]
Creating vector for state rewards... [1.1 MB]
Creating vector for inf... [1.1 MB]
Allocating iteration vectors... [2 x 1.1 MB]
TOTAL: [10.9 MB]

Starting iterations...

Iterative method: 84 iterations in 0.37 seconds (average 0.002000, setup 0.20)

Value in the initial state: 0.032653657778390514

Time for model checking: 0.432 seconds.

Result: 0.032653657778390514 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"time"}max=? [ F "deadlock" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=2,Objy1=2,Objx2=3,Objy2=2

Prob0E: 4 iterations in 0.02 seconds (average 0.005000, setup 0.00)

Prob1A: 1 iterations in 0.00 seconds (average 0.004000, setup 0.00)

goal = 1, inf = 0, maybe = 138077

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=138078, nc=184065, nnz=365342, k=4] [5.4 MB]
Building sparse matrix (transition rewards)... [n=138078, nc=184065, nnz=70176, k=4] [2.0 MB]
Creating vector for state rewards... [1.1 MB]
Creating vector for inf... [1.1 MB]
Allocating iteration vectors... [2 x 1.1 MB]
TOTAL: [11.7 MB]

Starting iterations...

Iterative method: 85 iterations in 0.40 seconds (average 0.002024, setup 0.22)

Value in the initial state: 157.39910616465664

Time for model checking: 0.431 seconds.

Result: 157.39910616465664 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"time"}min=? [ F "deadlock" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=2,Objy1=2,Objx2=3,Objy2=2

Prob0A: 4 iterations in 0.02 seconds (average 0.005000, setup 0.00)

Prob1E: 5 iterations in 0.03 seconds (average 0.006400, setup 0.00)

Warning: PRISM hasn't checked for zero-reward loops.
Your minimum rewards may be too low...

goal = 1, inf = 0, maybe = 138077

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=138078, nc=184065, nnz=365342, k=4] [5.4 MB]
Building sparse matrix (transition rewards)... [n=138078, nc=184065, nnz=70176, k=4] [2.0 MB]
Creating vector for state rewards... [1.1 MB]
Creating vector for inf... [1.1 MB]
Allocating iteration vectors... [2 x 1.1 MB]
TOTAL: [11.7 MB]

Starting iterations...

Iterative method: 88 iterations in 0.40 seconds (average 0.002045, setup 0.22)

Value in the initial state: 90.83673459332651

Time for model checking: 0.465 seconds.

Result: 90.83673459332651 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"success"}max=? [ F "deadlock" ]/Pmin=? [ F "MissionSuccessful" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=2,Objy1=2,Objx2=3,Objy2=2

Prob0E: 4 iterations in 0.02 seconds (average 0.005000, setup 0.00)

Prob1A: 1 iterations in 0.00 seconds (average 0.004000, setup 0.00)

goal = 1, inf = 0, maybe = 138077

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=138078, nc=184065, nnz=365342, k=4] [5.4 MB]
Building sparse matrix (transition rewards)... [n=138078, nc=184065, nnz=1887, k=4] [1.3 MB]
Creating vector for state rewards... [1.1 MB]
Creating vector for inf... [1.1 MB]
Allocating iteration vectors... [2 x 1.1 MB]
TOTAL: [10.9 MB]

Starting iterations...

Iterative method: 85 iterations in 0.37 seconds (average 0.001976, setup 0.20)

Prob0E: 44 iterations in 0.20 seconds (average 0.004636, setup 0.00)

Prob1A: 3 iterations in 0.02 seconds (average 0.008000, setup 0.00)

yes = 956, no = 117795, maybe = 19327

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=138078, nc=35245, nnz=98904, k=2] [1.3 MB]
Creating vector for yes... [1.1 MB]
Allocating iteration vectors... [2 x 1.1 MB]
TOTAL: [4.5 MB]

Starting iterations...

Iterative method: 80 iterations in 0.14 seconds (average 0.000850, setup 0.07)

Value in the initial state: 197.12374687757665

Time for model checking: 0.792 seconds.

Result: 197.12374687757665 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"success"}min=? [ F "deadlock" ]/Pmax=? [ F "MissionSuccessful" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=2,Objy1=2,Objx2=3,Objy2=2

Prob0A: 4 iterations in 0.04 seconds (average 0.010000, setup 0.00)

Prob1E: 5 iterations in 0.03 seconds (average 0.006400, setup 0.00)

Warning: PRISM hasn't checked for zero-reward loops.
Your minimum rewards may be too low...

goal = 1, inf = 0, maybe = 138077

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=138078, nc=184065, nnz=365342, k=4] [5.4 MB]
Building sparse matrix (transition rewards)... [n=138078, nc=184065, nnz=1887, k=4] [1.3 MB]
Creating vector for state rewards... [1.1 MB]
Creating vector for inf... [1.1 MB]
Allocating iteration vectors... [2 x 1.1 MB]
TOTAL: [10.9 MB]

Starting iterations...

Iterative method: 84 iterations in 0.36 seconds (average 0.001952, setup 0.20)

Prob0A: 44 iterations in 0.17 seconds (average 0.003909, setup 0.00)

Prob1E: 4 iterations in 0.02 seconds (average 0.005000, setup 0.00)

yes = 956, no = 114771, maybe = 22351

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=138078, nc=40859, nnz=115399, k=2] [1.5 MB]
Creating vector for yes... [1.1 MB]
Allocating iteration vectors... [2 x 1.1 MB]
TOTAL: [4.7 MB]

Starting iterations...

Iterative method: 83 iterations in 0.16 seconds (average 0.000916, setup 0.08)

Value in the initial state: 78.6536990712938

Time for model checking: 0.803 seconds.

Result: 78.6536990712938 (value in the initial state)

---------------------------------------------------------------------

Note: There were 16 warnings during computation.

