PRISM
=====

Version: 4.3
Date: Tue Aug 02 12:30:20 BST 2016
Hostname: eglinton
Memory limits: cudd=1g, java(heap)=981.5m
Command line: prism uav_sim_ft_mdp.prism -s uav_mdp.props -const 'Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=4,Objy1=0,Objx2=4,Objy2=1,'

Parsing model file "uav_sim_ft_mdp.prism"...

Parsing properties file "uav_mdp.props"...

18 properties:
(1) Pmax=? [ F "MissionSuccessful" ]
(2) Pmin=? [ F "MissionSuccessful" ]
(3) Pmax=? [ F "MissionTimeUp" ]
(4) Pmin=? [ F "MissionTimeUp" ]
(5) Pmax=? [ F "Crash" ]
(6) Pmin=? [ F "Crash" ]
(7) Pmax=? [ F "AllAreaSearched" ]
(8) Pmin=? [ F "AllAreaSearched" ]
(9) Pmax=? [ F "MissionTimeUp"|"AllAreaSearched"|"Crash" ]
(10) Pmin=? [ F "MissionTimeUp"|"AllAreaSearched"|"Crash" ]
(11) R{"drop"}max=? [ F "deadlock" ]
(12) R{"drop"}min=? [ F "deadlock" ]
(13) R{"charge"}max=? [ F "deadlock" ]
(14) R{"charge"}min=? [ F "deadlock" ]
(15) R{"time"}max=? [ F "deadlock" ]
(16) R{"time"}min=? [ F "deadlock" ]
(17) R{"success"}max=? [ F "deadlock" ]/Pmin=? [ F "MissionSuccessful" ]
(18) R{"success"}min=? [ F "deadlock" ]/Pmax=? [ F "MissionSuccessful" ]

Type:        MDP
Modules:     Obj Move Robot T 
Variables:   NoOfObjs posx posy s1 retx rety b t c 

---------------------------------------------------------------------

Model checking: Pmax=? [ F "MissionSuccessful" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=4,Objy1=0,Objx2=4,Objy2=1

Building model...
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=4,Objy1=0,Objx2=4,Objy2=1

Warning: Guard for command 7 of module "Move" is never satisfied.

Warning: Guard for command 8 of module "Move" is never satisfied.

Warning: Guard for command 9 of module "Move" is never satisfied.

Warning: Guard for command 7 of module "Robot" is never satisfied.

Warning: Update 2 of command 3 of module "Robot" doesn't do anything ("(s1'=13)", line 249, column 44)

Warning: Update 2 of command 27 of module "T" doesn't do anything ("(c'=1)", line 372, column 108)

Warning: Guard for command 28 of module "T" is never satisfied.

Warning: Update 2 of command 9 of module "T" doesn't do anything ("(c'=1)", line 349, column 112)

Warning: Guard for command 10 of module "T" is never satisfied.

Warning: Update 2 of command 51 of module "T" doesn't do anything ("(c'=1)", line 403, column 113)

Warning: Guard for command 52 of module "T" is never satisfied.

Computing reachable states...

Reachability (BFS): 81 iterations in 0.91 seconds (average 0.011259, setup 0.00)

Time for model construction: 2.328 seconds.

Warning: Deadlocks detected and fixed in 1 states

Type:        MDP
States:      224240 (1 initial)
Transitions: 589357
Choices:     294617

Transition matrix: 174221 nodes (76 terminal), 589357 minterms, vars: 34r/34c/18nd

Prob0A: 44 iterations in 0.32 seconds (average 0.007364, setup 0.00)

Prob1E: 4 iterations in 0.06 seconds (average 0.014000, setup 0.00)

yes = 954, no = 190171, maybe = 33115

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=224240, nc=59057, nnz=170618, k=2] [2.2 MB]
Creating vector for yes... [1.7 MB]
Allocating iteration vectors... [2 x 1.7 MB]
TOTAL: [7.4 MB]

Starting iterations...

Iterative method: 85 iterations in 0.33 seconds (average 0.001459, setup 0.21)

Value in the initial state: 0.9828577369554272

Time for model checking: 0.775 seconds.

Result: 0.9828577369554272 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmin=? [ F "MissionSuccessful" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=4,Objy1=0,Objx2=4,Objy2=1

Prob0E: 46 iterations in 0.50 seconds (average 0.010957, setup 0.00)

Prob1A: 3 iterations in 0.04 seconds (average 0.012000, setup 0.00)

yes = 954, no = 195418, maybe = 27868

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=224240, nc=49503, nnz=142623, k=2] [1.9 MB]
Creating vector for yes... [1.7 MB]
Allocating iteration vectors... [2 x 1.7 MB]
TOTAL: [7.0 MB]

Starting iterations...

Iterative method: 79 iterations in 0.27 seconds (average 0.001468, setup 0.16)

Value in the initial state: 0.824862051545703

Time for model checking: 0.851 seconds.

Result: 0.824862051545703 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmax=? [ F "MissionTimeUp" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=4,Objy1=0,Objx2=4,Objy2=1

Prob0A: 57 iterations in 0.40 seconds (average 0.007088, setup 0.00)

Prob1E: 4 iterations in 0.04 seconds (average 0.010000, setup 0.00)

yes = 460, no = 140889, maybe = 82891

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=224240, nc=143569, nnz=398289, k=4] [4.9 MB]
Creating vector for yes... [1.7 MB]
Allocating iteration vectors... [2 x 1.7 MB]
TOTAL: [10.0 MB]

Starting iterations...

Iterative method: 90 iterations in 0.44 seconds (average 0.001956, setup 0.26)

Value in the initial state: 6.404636839488869E-4

Time for model checking: 0.892 seconds.

Result: 6.404636839488869E-4 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmin=? [ F "MissionTimeUp" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=4,Objy1=0,Objx2=4,Objy2=1

Prob0E: 65 iterations in 0.58 seconds (average 0.008923, setup 0.00)

Prob1A: 3 iterations in 0.03 seconds (average 0.010667, setup 0.00)

yes = 460, no = 141265, maybe = 82515

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=224240, nc=142893, nnz=396941, k=4] [4.9 MB]
Creating vector for yes... [1.7 MB]
Allocating iteration vectors... [2 x 1.7 MB]
TOTAL: [10.0 MB]

Starting iterations...

Iterative method: 104 iterations in 0.43 seconds (average 0.001923, setup 0.23)

Value in the initial state: 1.4413232202549781E-6

Time for model checking: 1.071 seconds.

Result: 1.4413232202549781E-6 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmax=? [ F "Crash" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=4,Objy1=0,Objx2=4,Objy2=1

Prob0A: 3 iterations in 0.07 seconds (average 0.024000, setup 0.00)

Prob1E: 174 iterations in 2.12 seconds (average 0.012184, setup 0.00)

yes = 109872, no = 2638, maybe = 111730

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=224240, nc=182107, nnz=476847, k=4] [5.8 MB]
Creating vector for yes... [1.7 MB]
Allocating iteration vectors... [2 x 1.7 MB]
TOTAL: [11.0 MB]

Starting iterations...

Iterative method: 85 iterations in 0.48 seconds (average 0.002118, setup 0.30)

Value in the initial state: 0.030593688492283826

Time for model checking: 2.702 seconds.

Result: 0.030593688492283826 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmin=? [ F "Crash" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=4,Objy1=0,Objx2=4,Objy2=1

Prob0E: 3 iterations in 0.05 seconds (average 0.017333, setup 0.00)

Prob1A: 41 iterations in 0.44 seconds (average 0.010634, setup 0.00)

yes = 109872, no = 2638, maybe = 111730

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=224240, nc=182107, nnz=476847, k=4] [5.8 MB]
Creating vector for yes... [1.7 MB]
Allocating iteration vectors... [2 x 1.7 MB]
TOTAL: [11.0 MB]

Starting iterations...

Iterative method: 81 iterations in 0.41 seconds (average 0.002123, setup 0.24)

Value in the initial state: 0.015104371777330613

Time for model checking: 0.916 seconds.

Result: 0.015104371777330613 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmax=? [ F "AllAreaSearched" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=4,Objy1=0,Objx2=4,Objy2=1

Prob0A: 59 iterations in 0.48 seconds (average 0.008068, setup 0.00)

Prob1E: 4 iterations in 0.04 seconds (average 0.011000, setup 0.00)

yes = 1223, no = 177665, maybe = 45352

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=224240, nc=66605, nnz=170001, k=2] [2.2 MB]
Creating vector for yes... [1.7 MB]
Allocating iteration vectors... [2 x 1.7 MB]
TOTAL: [7.4 MB]

Starting iterations...

Iterative method: 88 iterations in 0.30 seconds (average 0.001591, setup 0.16)

Value in the initial state: 0.1483432370024232

Time for model checking: 0.849 seconds.

Result: 0.1483432370024232 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmin=? [ F "AllAreaSearched" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=4,Objy1=0,Objx2=4,Objy2=1

Prob0E: 63 iterations in 0.43 seconds (average 0.006857, setup 0.00)

Prob1A: 3 iterations in 0.04 seconds (average 0.014667, setup 0.00)

yes = 1223, no = 183026, maybe = 39991

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=224240, nc=57358, nnz=143838, k=2] [1.9 MB]
Creating vector for yes... [1.7 MB]
Allocating iteration vectors... [2 x 1.7 MB]
TOTAL: [7.0 MB]

Starting iterations...

Iterative method: 92 iterations in 0.26 seconds (average 0.001478, setup 0.12)

Value in the initial state: 0.0017423469530177642

Time for model checking: 0.759 seconds.

Result: 0.0017423469530177642 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmax=? [ F "MissionTimeUp"|"AllAreaSearched"|"Crash" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=4,Objy1=0,Objx2=4,Objy2=1

Prob0A: 3 iterations in 0.10 seconds (average 0.032000, setup 0.00)

Prob1E: 180 iterations in 1.98 seconds (average 0.010978, setup 0.00)

yes = 195417, no = 955, maybe = 27868

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=224240, nc=49503, nnz=142623, k=2] [1.9 MB]
Creating vector for yes... [1.7 MB]
Allocating iteration vectors... [2 x 1.7 MB]
TOTAL: [7.0 MB]

Starting iterations...

Iterative method: 73 iterations in 0.27 seconds (average 0.001425, setup 0.16)

Value in the initial state: 0.17513783257534746

Time for model checking: 2.352 seconds.

Result: 0.17513783257534746 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmin=? [ F "MissionTimeUp"|"AllAreaSearched"|"Crash" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=4,Objy1=0,Objx2=4,Objy2=1

Prob0E: 3 iterations in 0.06 seconds (average 0.020000, setup 0.00)

Prob1A: 44 iterations in 0.34 seconds (average 0.007818, setup 0.00)

yes = 190170, no = 955, maybe = 33115

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=224240, nc=59057, nnz=170618, k=2] [2.2 MB]
Creating vector for yes... [1.7 MB]
Allocating iteration vectors... [2 x 1.7 MB]
TOTAL: [7.4 MB]

Starting iterations...

Iterative method: 84 iterations in 0.28 seconds (average 0.001524, setup 0.15)

Value in the initial state: 0.017141477349731614

Time for model checking: 0.714 seconds.

Result: 0.017141477349731614 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"drop"}max=? [ F "deadlock" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=4,Objy1=0,Objx2=4,Objy2=1

Prob0E: 4 iterations in 0.06 seconds (average 0.016000, setup 0.00)

Prob1A: 1 iterations in 0.01 seconds (average 0.008000, setup 0.00)

goal = 1, inf = 0, maybe = 224239

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=224240, nc=294616, nnz=589356, k=4] [8.7 MB]
Building sparse matrix (transition rewards)... [n=224240, nc=294616, nnz=0, k=4] [2.0 MB]
Creating vector for state rewards... [1.7 MB]
Creating vector for inf... [1.7 MB]
Allocating iteration vectors... [2 x 1.7 MB]
TOTAL: [17.5 MB]

Starting iterations...

Iterative method: 81 iterations in 0.65 seconds (average 0.003210, setup 0.39)

Value in the initial state: 0.21131865670961802

Time for model checking: 0.736 seconds.

Result: 0.21131865670961802 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"drop"}min=? [ F "deadlock" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=4,Objy1=0,Objx2=4,Objy2=1

Prob0A: 4 iterations in 0.04 seconds (average 0.009000, setup 0.00)

Prob1E: 5 iterations in 0.04 seconds (average 0.008800, setup 0.00)

Warning: PRISM hasn't checked for zero-reward loops.
Your minimum rewards may be too low...

goal = 1, inf = 0, maybe = 224239

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=224240, nc=294616, nnz=589356, k=4] [8.7 MB]
Building sparse matrix (transition rewards)... [n=224240, nc=294616, nnz=0, k=4] [2.0 MB]
Creating vector for state rewards... [1.7 MB]
Creating vector for inf... [1.7 MB]
Allocating iteration vectors... [2 x 1.7 MB]
TOTAL: [17.5 MB]

Starting iterations...

Iterative method: 72 iterations in 0.54 seconds (average 0.003222, setup 0.31)

Value in the initial state: 0.18750176005532113

Time for model checking: 0.634 seconds.

Result: 0.18750176005532113 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"charge"}max=? [ F "deadlock" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=4,Objy1=0,Objx2=4,Objy2=1

Prob0E: 4 iterations in 0.02 seconds (average 0.006000, setup 0.00)

Prob1A: 1 iterations in 0.01 seconds (average 0.008000, setup 0.00)

goal = 1, inf = 0, maybe = 224239

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=224240, nc=294616, nnz=589356, k=4] [8.7 MB]
Building sparse matrix (transition rewards)... [n=224240, nc=294616, nnz=6458, k=4] [2.1 MB]
Creating vector for state rewards... [1.7 MB]
Creating vector for inf... [1.7 MB]
Allocating iteration vectors... [2 x 1.7 MB]
TOTAL: [17.6 MB]

Starting iterations...

Iterative method: 78 iterations in 0.56 seconds (average 0.003231, setup 0.31)

Value in the initial state: 0.2929985026028808

Time for model checking: 0.608 seconds.

Result: 0.2929985026028808 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"charge"}min=? [ F "deadlock" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=4,Objy1=0,Objx2=4,Objy2=1

Prob0A: 4 iterations in 0.02 seconds (average 0.006000, setup 0.00)

Prob1E: 5 iterations in 0.03 seconds (average 0.006400, setup 0.00)

Warning: PRISM hasn't checked for zero-reward loops.
Your minimum rewards may be too low...

goal = 1, inf = 0, maybe = 224239

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=224240, nc=294616, nnz=589356, k=4] [8.7 MB]
Building sparse matrix (transition rewards)... [n=224240, nc=294616, nnz=6458, k=4] [2.1 MB]
Creating vector for state rewards... [1.7 MB]
Creating vector for inf... [1.7 MB]
Allocating iteration vectors... [2 x 1.7 MB]
TOTAL: [17.6 MB]

Starting iterations...

Iterative method: 88 iterations in 0.58 seconds (average 0.003227, setup 0.30)

Value in the initial state: 0.02024799074252001

Time for model checking: 0.669 seconds.

Result: 0.02024799074252001 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"time"}max=? [ F "deadlock" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=4,Objy1=0,Objx2=4,Objy2=1

Prob0E: 4 iterations in 0.02 seconds (average 0.005000, setup 0.00)

Prob1A: 1 iterations in 0.00 seconds (average 0.004000, setup 0.00)

goal = 1, inf = 0, maybe = 224239

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=224240, nc=294616, nnz=589356, k=4] [8.7 MB]
Building sparse matrix (transition rewards)... [n=224240, nc=294616, nnz=112508, k=4] [3.3 MB]
Creating vector for state rewards... [1.7 MB]
Creating vector for inf... [1.7 MB]
Allocating iteration vectors... [2 x 1.7 MB]
TOTAL: [18.8 MB]

Starting iterations...

Iterative method: 88 iterations in 0.65 seconds (average 0.003364, setup 0.35)

Value in the initial state: 168.73421435929887

Time for model checking: 0.682 seconds.

Result: 168.73421435929887 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"time"}min=? [ F "deadlock" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=4,Objy1=0,Objx2=4,Objy2=1

Prob0A: 4 iterations in 0.02 seconds (average 0.006000, setup 0.00)

Prob1E: 5 iterations in 0.04 seconds (average 0.007200, setup 0.00)

Warning: PRISM hasn't checked for zero-reward loops.
Your minimum rewards may be too low...

goal = 1, inf = 0, maybe = 224239

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=224240, nc=294616, nnz=589356, k=4] [8.7 MB]
Building sparse matrix (transition rewards)... [n=224240, nc=294616, nnz=112508, k=4] [3.3 MB]
Creating vector for state rewards... [1.7 MB]
Creating vector for inf... [1.7 MB]
Allocating iteration vectors... [2 x 1.7 MB]
TOTAL: [18.8 MB]

Starting iterations...

Iterative method: 92 iterations in 0.66 seconds (average 0.003391, setup 0.34)

Value in the initial state: 83.79020330643904

Time for model checking: 0.725 seconds.

Result: 83.79020330643904 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"success"}max=? [ F "deadlock" ]/Pmin=? [ F "MissionSuccessful" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=4,Objy1=0,Objx2=4,Objy2=1

Prob0E: 4 iterations in 0.02 seconds (average 0.006000, setup 0.00)

Prob1A: 1 iterations in 0.00 seconds (average 0.004000, setup 0.00)

goal = 1, inf = 0, maybe = 224239

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=224240, nc=294616, nnz=589356, k=4] [8.7 MB]
Building sparse matrix (transition rewards)... [n=224240, nc=294616, nnz=1880, k=4] [2.0 MB]
Creating vector for state rewards... [1.7 MB]
Creating vector for inf... [1.7 MB]
Allocating iteration vectors... [2 x 1.7 MB]
TOTAL: [17.6 MB]

Starting iterations...

Iterative method: 87 iterations in 0.60 seconds (average 0.003310, setup 0.31)

Prob0E: 46 iterations in 0.30 seconds (average 0.006609, setup 0.00)

Prob1A: 3 iterations in 0.04 seconds (average 0.014667, setup 0.00)

yes = 954, no = 195418, maybe = 27868

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=224240, nc=49503, nnz=142623, k=2] [1.9 MB]
Creating vector for yes... [1.7 MB]
Allocating iteration vectors... [2 x 1.7 MB]
TOTAL: [7.0 MB]

Starting iterations...

Iterative method: 79 iterations in 0.23 seconds (average 0.001468, setup 0.11)

Value in the initial state: 200.44131940280909

Time for model checking: 1.232 seconds.

Result: 200.44131940280909 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"success"}min=? [ F "deadlock" ]/Pmax=? [ F "MissionSuccessful" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=4,Objy1=0,Objx2=4,Objy2=1

Prob0A: 4 iterations in 0.03 seconds (average 0.008000, setup 0.00)

Prob1E: 5 iterations in 0.04 seconds (average 0.008800, setup 0.00)

Warning: PRISM hasn't checked for zero-reward loops.
Your minimum rewards may be too low...

goal = 1, inf = 0, maybe = 224239

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=224240, nc=294616, nnz=589356, k=4] [8.7 MB]
Building sparse matrix (transition rewards)... [n=224240, nc=294616, nnz=1880, k=4] [2.0 MB]
Creating vector for state rewards... [1.7 MB]
Creating vector for inf... [1.7 MB]
Allocating iteration vectors... [2 x 1.7 MB]
TOTAL: [17.6 MB]

Starting iterations...

Iterative method: 84 iterations in 0.62 seconds (average 0.003238, setup 0.35)

Prob0A: 44 iterations in 0.28 seconds (average 0.006455, setup 0.00)

Prob1E: 4 iterations in 0.03 seconds (average 0.008000, setup 0.00)

yes = 954, no = 190171, maybe = 33115

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=224240, nc=59057, nnz=170618, k=2] [2.2 MB]
Creating vector for yes... [1.7 MB]
Allocating iteration vectors... [2 x 1.7 MB]
TOTAL: [7.4 MB]

Starting iterations...

Iterative method: 85 iterations in 0.28 seconds (average 0.001506, setup 0.16)

Value in the initial state: 76.13082783931478

Time for model checking: 1.324 seconds.

Result: 76.13082783931478 (value in the initial state)

---------------------------------------------------------------------

Note: There were 16 warnings during computation.

