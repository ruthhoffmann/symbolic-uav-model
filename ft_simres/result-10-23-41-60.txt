PRISM
=====

Version: 4.3
Date: Tue Aug 02 12:41:56 BST 2016
Hostname: eglinton
Memory limits: cudd=1g, java(heap)=981.5m
Command line: prism uav_sim_ft_mdp.prism -s uav_mdp.props -const 'Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=4,Objy1=1,Objx2=6,Objy2=0,'

Parsing model file "uav_sim_ft_mdp.prism"...

Parsing properties file "uav_mdp.props"...

18 properties:
(1) Pmax=? [ F "MissionSuccessful" ]
(2) Pmin=? [ F "MissionSuccessful" ]
(3) Pmax=? [ F "MissionTimeUp" ]
(4) Pmin=? [ F "MissionTimeUp" ]
(5) Pmax=? [ F "Crash" ]
(6) Pmin=? [ F "Crash" ]
(7) Pmax=? [ F "AllAreaSearched" ]
(8) Pmin=? [ F "AllAreaSearched" ]
(9) Pmax=? [ F "MissionTimeUp"|"AllAreaSearched"|"Crash" ]
(10) Pmin=? [ F "MissionTimeUp"|"AllAreaSearched"|"Crash" ]
(11) R{"drop"}max=? [ F "deadlock" ]
(12) R{"drop"}min=? [ F "deadlock" ]
(13) R{"charge"}max=? [ F "deadlock" ]
(14) R{"charge"}min=? [ F "deadlock" ]
(15) R{"time"}max=? [ F "deadlock" ]
(16) R{"time"}min=? [ F "deadlock" ]
(17) R{"success"}max=? [ F "deadlock" ]/Pmin=? [ F "MissionSuccessful" ]
(18) R{"success"}min=? [ F "deadlock" ]/Pmax=? [ F "MissionSuccessful" ]

Type:        MDP
Modules:     Obj Move Robot T 
Variables:   NoOfObjs posx posy s1 retx rety b t c 

---------------------------------------------------------------------

Model checking: Pmax=? [ F "MissionSuccessful" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=4,Objy1=1,Objx2=6,Objy2=0

Building model...
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=4,Objy1=1,Objx2=6,Objy2=0

Warning: Guard for command 7 of module "Move" is never satisfied.

Warning: Guard for command 8 of module "Move" is never satisfied.

Warning: Guard for command 9 of module "Move" is never satisfied.

Warning: Guard for command 7 of module "Robot" is never satisfied.

Warning: Update 2 of command 3 of module "Robot" doesn't do anything ("(s1'=13)", line 249, column 44)

Warning: Update 2 of command 27 of module "T" doesn't do anything ("(c'=1)", line 372, column 108)

Warning: Guard for command 28 of module "T" is never satisfied.

Warning: Update 2 of command 9 of module "T" doesn't do anything ("(c'=1)", line 349, column 112)

Warning: Guard for command 10 of module "T" is never satisfied.

Warning: Update 2 of command 51 of module "T" doesn't do anything ("(c'=1)", line 403, column 113)

Warning: Guard for command 52 of module "T" is never satisfied.

Computing reachable states...

Reachability (BFS): 79 iterations in 0.83 seconds (average 0.010481, setup 0.00)

Time for model construction: 1.868 seconds.

Warning: Deadlocks detected and fixed in 1 states

Type:        MDP
States:      230721 (1 initial)
Transitions: 620882
Choices:     306328

Transition matrix: 175940 nodes (76 terminal), 620882 minterms, vars: 34r/34c/18nd

Prob0A: 43 iterations in 0.34 seconds (average 0.008000, setup 0.00)

Prob1E: 4 iterations in 0.05 seconds (average 0.013000, setup 0.00)

yes = 950, no = 194873, maybe = 34898

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=230721, nc=62965, nnz=184171, k=2] [2.4 MB]
Creating vector for yes... [1.8 MB]
Allocating iteration vectors... [2 x 1.8 MB]
TOTAL: [7.7 MB]

Starting iterations...

Iterative method: 88 iterations in 0.35 seconds (average 0.001500, setup 0.22)

Value in the initial state: 0.9822410996808822

Time for model checking: 0.776 seconds.

Result: 0.9822410996808822 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmin=? [ F "MissionSuccessful" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=4,Objy1=1,Objx2=6,Objy2=0

Prob0E: 45 iterations in 0.42 seconds (average 0.009422, setup 0.00)

Prob1A: 3 iterations in 0.04 seconds (average 0.012000, setup 0.00)

yes = 950, no = 200785, maybe = 28986

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=230721, nc=52027, nnz=151843, k=2] [2.0 MB]
Creating vector for yes... [1.8 MB]
Allocating iteration vectors... [2 x 1.8 MB]
TOTAL: [7.3 MB]

Starting iterations...

Iterative method: 83 iterations in 0.27 seconds (average 0.001446, setup 0.15)

Value in the initial state: 0.8098815145257128

Time for model checking: 0.762 seconds.

Result: 0.8098815145257128 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmax=? [ F "MissionTimeUp" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=4,Objy1=1,Objx2=6,Objy2=0

Prob0A: 59 iterations in 0.43 seconds (average 0.007254, setup 0.00)

Prob1E: 4 iterations in 0.04 seconds (average 0.009000, setup 0.00)

yes = 480, no = 143663, maybe = 86578

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=230721, nc=152522, nnz=427435, k=4] [5.3 MB]
Creating vector for yes... [1.8 MB]
Allocating iteration vectors... [2 x 1.8 MB]
TOTAL: [10.5 MB]

Starting iterations...

Iterative method: 89 iterations in 0.44 seconds (average 0.002022, setup 0.26)

Value in the initial state: 0.00120266865967801

Time for model checking: 0.917 seconds.

Result: 0.00120266865967801 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmin=? [ F "MissionTimeUp" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=4,Objy1=1,Objx2=6,Objy2=0

Prob0E: 66 iterations in 0.65 seconds (average 0.009879, setup 0.00)

Prob1A: 3 iterations in 0.04 seconds (average 0.012000, setup 0.00)

yes = 480, no = 144039, maybe = 86202

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=230721, nc=151846, nnz=426087, k=4] [5.2 MB]
Creating vector for yes... [1.8 MB]
Allocating iteration vectors... [2 x 1.8 MB]
TOTAL: [10.5 MB]

Starting iterations...

Iterative method: 108 iterations in 0.47 seconds (average 0.002074, setup 0.25)

Value in the initial state: 3.2806320055303925E-6

Time for model checking: 1.172 seconds.

Result: 3.2806320055303925E-6 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmax=? [ F "Crash" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=4,Objy1=1,Objx2=6,Objy2=0

Prob0A: 3 iterations in 0.08 seconds (average 0.026667, setup 0.00)

Prob1E: 165 iterations in 2.23 seconds (average 0.013527, setup 0.00)

yes = 112906, no = 2641, maybe = 115174

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=230721, nc=190781, nnz=505335, k=4] [6.2 MB]
Creating vector for yes... [1.8 MB]
Allocating iteration vectors... [2 x 1.8 MB]
TOTAL: [11.5 MB]

Starting iterations...

Iterative method: 86 iterations in 0.50 seconds (average 0.002186, setup 0.31)

Value in the initial state: 0.03168435646117201

Time for model checking: 2.827 seconds.

Result: 0.03168435646117201 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmin=? [ F "Crash" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=4,Objy1=1,Objx2=6,Objy2=0

Prob0E: 3 iterations in 0.07 seconds (average 0.022667, setup 0.00)

Prob1A: 40 iterations in 0.48 seconds (average 0.012100, setup 0.00)

yes = 112906, no = 2641, maybe = 115174

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=230721, nc=190781, nnz=505335, k=4] [6.2 MB]
Creating vector for yes... [1.8 MB]
Allocating iteration vectors... [2 x 1.8 MB]
TOTAL: [11.5 MB]

Starting iterations...

Iterative method: 87 iterations in 0.44 seconds (average 0.002207, setup 0.25)

Value in the initial state: 0.01570408851251522

Time for model checking: 1.02 seconds.

Result: 0.01570408851251522 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmax=? [ F "AllAreaSearched" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=4,Objy1=1,Objx2=6,Objy2=0

Prob0A: 58 iterations in 0.46 seconds (average 0.007931, setup 0.00)

Prob1E: 4 iterations in 0.05 seconds (average 0.013000, setup 0.00)

yes = 1210, no = 184822, maybe = 44689

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=230721, nc=66019, nnz=168900, k=2] [2.2 MB]
Creating vector for yes... [1.8 MB]
Allocating iteration vectors... [2 x 1.8 MB]
TOTAL: [7.5 MB]

Starting iterations...

Iterative method: 92 iterations in 0.31 seconds (average 0.001565, setup 0.16)

Value in the initial state: 0.16231256150508205

Time for model checking: 0.838 seconds.

Result: 0.16231256150508205 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmin=? [ F "AllAreaSearched" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=4,Objy1=1,Objx2=6,Objy2=0

Prob0E: 64 iterations in 0.43 seconds (average 0.006687, setup 0.00)

Prob1A: 3 iterations in 0.04 seconds (average 0.013333, setup 0.00)

yes = 1210, no = 190219, maybe = 39292

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=230721, nc=56546, nnz=141849, k=2] [1.9 MB]
Creating vector for yes... [1.8 MB]
Allocating iteration vectors... [2 x 1.8 MB]
TOTAL: [7.2 MB]

Starting iterations...

Iterative method: 94 iterations in 0.27 seconds (average 0.001489, setup 0.13)

Value in the initial state: 0.0017273440489328847

Time for model checking: 0.752 seconds.

Result: 0.0017273440489328847 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmax=? [ F "MissionTimeUp"|"AllAreaSearched"|"Crash" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=4,Objy1=1,Objx2=6,Objy2=0

Prob0A: 3 iterations in 0.10 seconds (average 0.032000, setup 0.00)

Prob1E: 176 iterations in 1.87 seconds (average 0.010636, setup 0.00)

yes = 200784, no = 951, maybe = 28986

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=230721, nc=52027, nnz=151843, k=2] [2.0 MB]
Creating vector for yes... [1.8 MB]
Allocating iteration vectors... [2 x 1.8 MB]
TOTAL: [7.3 MB]

Starting iterations...

Iterative method: 77 iterations in 0.30 seconds (average 0.001506, setup 0.18)

Value in the initial state: 0.19011840289856308

Time for model checking: 2.266 seconds.

Result: 0.19011840289856308 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmin=? [ F "MissionTimeUp"|"AllAreaSearched"|"Crash" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=4,Objy1=1,Objx2=6,Objy2=0

Prob0E: 3 iterations in 0.06 seconds (average 0.020000, setup 0.00)

Prob1A: 43 iterations in 0.37 seconds (average 0.008651, setup 0.00)

yes = 194872, no = 951, maybe = 34898

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=230721, nc=62965, nnz=184171, k=2] [2.4 MB]
Creating vector for yes... [1.8 MB]
Allocating iteration vectors... [2 x 1.8 MB]
TOTAL: [7.7 MB]

Starting iterations...

Iterative method: 87 iterations in 0.30 seconds (average 0.001563, setup 0.16)

Value in the initial state: 0.01775777278505041

Time for model checking: 0.757 seconds.

Result: 0.01775777278505041 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"drop"}max=? [ F "deadlock" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=4,Objy1=1,Objx2=6,Objy2=0

Prob0E: 4 iterations in 0.06 seconds (average 0.014000, setup 0.00)

Prob1A: 1 iterations in 0.00 seconds (average 0.004000, setup 0.00)

goal = 1, inf = 0, maybe = 230720

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=230721, nc=306327, nnz=620881, k=4] [9.2 MB]
Building sparse matrix (transition rewards)... [n=230721, nc=306327, nnz=0, k=4] [2.0 MB]
Creating vector for state rewards... [1.8 MB]
Creating vector for inf... [1.8 MB]
Allocating iteration vectors... [2 x 1.8 MB]
TOTAL: [18.2 MB]

Starting iterations...

Iterative method: 85 iterations in 0.70 seconds (average 0.003388, setup 0.41)

Value in the initial state: 0.24035555568608172

Time for model checking: 0.769 seconds.

Result: 0.24035555568608172 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"drop"}min=? [ F "deadlock" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=4,Objy1=1,Objx2=6,Objy2=0

Prob0A: 4 iterations in 0.04 seconds (average 0.009000, setup 0.00)

Prob1E: 5 iterations in 0.04 seconds (average 0.007200, setup 0.00)

Warning: PRISM hasn't checked for zero-reward loops.
Your minimum rewards may be too low...

goal = 1, inf = 0, maybe = 230720

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=230721, nc=306327, nnz=620881, k=4] [9.2 MB]
Building sparse matrix (transition rewards)... [n=230721, nc=306327, nnz=0, k=4] [2.0 MB]
Creating vector for state rewards... [1.8 MB]
Creating vector for inf... [1.8 MB]
Allocating iteration vectors... [2 x 1.8 MB]
TOTAL: [18.2 MB]

Starting iterations...

Iterative method: 74 iterations in 0.58 seconds (average 0.003351, setup 0.33)

Value in the initial state: 0.21477326125444163

Time for model checking: 0.662 seconds.

Result: 0.21477326125444163 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"charge"}max=? [ F "deadlock" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=4,Objy1=1,Objx2=6,Objy2=0

Prob0E: 4 iterations in 0.02 seconds (average 0.006000, setup 0.00)

Prob1A: 1 iterations in 0.00 seconds (average 0.004000, setup 0.00)

goal = 1, inf = 0, maybe = 230720

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=230721, nc=306327, nnz=620881, k=4] [9.2 MB]
Building sparse matrix (transition rewards)... [n=230721, nc=306327, nnz=7130, k=4] [2.1 MB]
Creating vector for state rewards... [1.8 MB]
Creating vector for inf... [1.8 MB]
Allocating iteration vectors... [2 x 1.8 MB]
TOTAL: [18.3 MB]

Starting iterations...

Iterative method: 81 iterations in 0.60 seconds (average 0.003407, setup 0.33)

Value in the initial state: 0.31198311481370444

Time for model checking: 0.639 seconds.

Result: 0.31198311481370444 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"charge"}min=? [ F "deadlock" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=4,Objy1=1,Objx2=6,Objy2=0

Prob0A: 4 iterations in 0.02 seconds (average 0.005000, setup 0.00)

Prob1E: 5 iterations in 0.03 seconds (average 0.006400, setup 0.00)

Warning: PRISM hasn't checked for zero-reward loops.
Your minimum rewards may be too low...

goal = 1, inf = 0, maybe = 230720

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=230721, nc=306327, nnz=620881, k=4] [9.2 MB]
Building sparse matrix (transition rewards)... [n=230721, nc=306327, nnz=7130, k=4] [2.1 MB]
Creating vector for state rewards... [1.8 MB]
Creating vector for inf... [1.8 MB]
Allocating iteration vectors... [2 x 1.8 MB]
TOTAL: [18.3 MB]

Starting iterations...

Iterative method: 91 iterations in 0.64 seconds (average 0.003385, setup 0.33)

Value in the initial state: 0.026976793910678344

Time for model checking: 0.704 seconds.

Result: 0.026976793910678344 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"time"}max=? [ F "deadlock" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=4,Objy1=1,Objx2=6,Objy2=0

Prob0E: 4 iterations in 0.02 seconds (average 0.005000, setup 0.00)

Prob1A: 1 iterations in 0.00 seconds (average 0.004000, setup 0.00)

goal = 1, inf = 0, maybe = 230720

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=230721, nc=306327, nnz=620881, k=4] [9.2 MB]
Building sparse matrix (transition rewards)... [n=230721, nc=306327, nnz=115545, k=4] [3.4 MB]
Creating vector for state rewards... [1.8 MB]
Creating vector for inf... [1.8 MB]
Allocating iteration vectors... [2 x 1.8 MB]
TOTAL: [19.6 MB]

Starting iterations...

Iterative method: 89 iterations in 0.68 seconds (average 0.003416, setup 0.37)

Value in the initial state: 174.64489685559346

Time for model checking: 0.712 seconds.

Result: 174.64489685559346 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"time"}min=? [ F "deadlock" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=4,Objy1=1,Objx2=6,Objy2=0

Prob0A: 4 iterations in 0.02 seconds (average 0.005000, setup 0.00)

Prob1E: 5 iterations in 0.04 seconds (average 0.007200, setup 0.00)

Warning: PRISM hasn't checked for zero-reward loops.
Your minimum rewards may be too low...

goal = 1, inf = 0, maybe = 230720

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=230721, nc=306327, nnz=620881, k=4] [9.2 MB]
Building sparse matrix (transition rewards)... [n=230721, nc=306327, nnz=115545, k=4] [3.4 MB]
Creating vector for state rewards... [1.8 MB]
Creating vector for inf... [1.8 MB]
Allocating iteration vectors... [2 x 1.8 MB]
TOTAL: [19.6 MB]

Starting iterations...

Iterative method: 95 iterations in 0.68 seconds (average 0.003495, setup 0.35)

Value in the initial state: 87.08940435651282

Time for model checking: 0.763 seconds.

Result: 87.08940435651282 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"success"}max=? [ F "deadlock" ]/Pmin=? [ F "MissionSuccessful" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=4,Objy1=1,Objx2=6,Objy2=0

Prob0E: 4 iterations in 0.02 seconds (average 0.005000, setup 0.00)

Prob1A: 1 iterations in 0.00 seconds (average 0.004000, setup 0.00)

goal = 1, inf = 0, maybe = 230720

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=230721, nc=306327, nnz=620881, k=4] [9.2 MB]
Building sparse matrix (transition rewards)... [n=230721, nc=306327, nnz=1872, k=4] [2.1 MB]
Creating vector for state rewards... [1.8 MB]
Creating vector for inf... [1.8 MB]
Allocating iteration vectors... [2 x 1.8 MB]
TOTAL: [18.3 MB]

Starting iterations...

Iterative method: 91 iterations in 0.79 seconds (average 0.003429, setup 0.48)

Prob0E: 45 iterations in 0.33 seconds (average 0.007378, setup 0.00)

Prob1A: 3 iterations in 0.05 seconds (average 0.016000, setup 0.00)

yes = 950, no = 200785, maybe = 28986

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=230721, nc=52027, nnz=151843, k=2] [2.0 MB]
Creating vector for yes... [1.8 MB]
Allocating iteration vectors... [2 x 1.8 MB]
TOTAL: [7.3 MB]

Starting iterations...

Iterative method: 83 iterations in 0.27 seconds (average 0.001446, setup 0.15)

Value in the initial state: 211.12913114820734

Time for model checking: 1.503 seconds.

Result: 211.12913114820734 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"success"}min=? [ F "deadlock" ]/Pmax=? [ F "MissionSuccessful" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=4,Objy1=1,Objx2=6,Objy2=0

Prob0A: 4 iterations in 0.09 seconds (average 0.022000, setup 0.00)

Prob1E: 5 iterations in 0.06 seconds (average 0.011200, setup 0.00)

Warning: PRISM hasn't checked for zero-reward loops.
Your minimum rewards may be too low...

goal = 1, inf = 0, maybe = 230720

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=230721, nc=306327, nnz=620881, k=4] [9.2 MB]
Building sparse matrix (transition rewards)... [n=230721, nc=306327, nnz=1872, k=4] [2.1 MB]
Creating vector for state rewards... [1.8 MB]
Creating vector for inf... [1.8 MB]
Allocating iteration vectors... [2 x 1.8 MB]
TOTAL: [18.3 MB]

Starting iterations...

Iterative method: 86 iterations in 0.62 seconds (average 0.003302, setup 0.34)

Prob0A: 43 iterations in 0.24 seconds (average 0.005674, setup 0.00)

Prob1E: 4 iterations in 0.04 seconds (average 0.009000, setup 0.00)

yes = 950, no = 194873, maybe = 34898

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=230721, nc=62965, nnz=184171, k=2] [2.4 MB]
Creating vector for yes... [1.8 MB]
Allocating iteration vectors... [2 x 1.8 MB]
TOTAL: [7.7 MB]

Starting iterations...

Iterative method: 88 iterations in 0.31 seconds (average 0.001727, setup 0.16)

Value in the initial state: 77.53272212918442

Time for model checking: 1.369 seconds.

Result: 77.53272212918442 (value in the initial state)

---------------------------------------------------------------------

Note: There were 16 warnings during computation.

