PRISM
=====

Version: 4.3
Date: Tue Aug 02 12:36:49 BST 2016
Hostname: eglinton
Memory limits: cudd=1g, java(heap)=981.5m
Command line: prism uav_sim_ft_mdp.prism -s uav_mdp.props -const 'Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=1,Objy1=1,Objx2=4,Objy2=0,'

Parsing model file "uav_sim_ft_mdp.prism"...

Parsing properties file "uav_mdp.props"...

18 properties:
(1) Pmax=? [ F "MissionSuccessful" ]
(2) Pmin=? [ F "MissionSuccessful" ]
(3) Pmax=? [ F "MissionTimeUp" ]
(4) Pmin=? [ F "MissionTimeUp" ]
(5) Pmax=? [ F "Crash" ]
(6) Pmin=? [ F "Crash" ]
(7) Pmax=? [ F "AllAreaSearched" ]
(8) Pmin=? [ F "AllAreaSearched" ]
(9) Pmax=? [ F "MissionTimeUp"|"AllAreaSearched"|"Crash" ]
(10) Pmin=? [ F "MissionTimeUp"|"AllAreaSearched"|"Crash" ]
(11) R{"drop"}max=? [ F "deadlock" ]
(12) R{"drop"}min=? [ F "deadlock" ]
(13) R{"charge"}max=? [ F "deadlock" ]
(14) R{"charge"}min=? [ F "deadlock" ]
(15) R{"time"}max=? [ F "deadlock" ]
(16) R{"time"}min=? [ F "deadlock" ]
(17) R{"success"}max=? [ F "deadlock" ]/Pmin=? [ F "MissionSuccessful" ]
(18) R{"success"}min=? [ F "deadlock" ]/Pmax=? [ F "MissionSuccessful" ]

Type:        MDP
Modules:     Obj Move Robot T 
Variables:   NoOfObjs posx posy s1 retx rety b t c 

---------------------------------------------------------------------

Model checking: Pmax=? [ F "MissionSuccessful" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=1,Objy1=1,Objx2=4,Objy2=0

Building model...
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=1,Objy1=1,Objx2=4,Objy2=0

Warning: Guard for command 7 of module "Move" is never satisfied.

Warning: Guard for command 8 of module "Move" is never satisfied.

Warning: Guard for command 9 of module "Move" is never satisfied.

Warning: Guard for command 7 of module "Robot" is never satisfied.

Warning: Update 2 of command 3 of module "Robot" doesn't do anything ("(s1'=13)", line 249, column 44)

Warning: Update 2 of command 27 of module "T" doesn't do anything ("(c'=1)", line 372, column 108)

Warning: Guard for command 28 of module "T" is never satisfied.

Warning: Update 2 of command 9 of module "T" doesn't do anything ("(c'=1)", line 349, column 112)

Warning: Guard for command 10 of module "T" is never satisfied.

Warning: Update 2 of command 51 of module "T" doesn't do anything ("(c'=1)", line 403, column 113)

Warning: Guard for command 52 of module "T" is never satisfied.

Computing reachable states...

Reachability (BFS): 75 iterations in 1.00 seconds (average 0.013387, setup 0.00)

Time for model construction: 2.48 seconds.

Warning: Deadlocks detected and fixed in 1 states

Type:        MDP
States:      224743 (1 initial)
Transitions: 590252
Choices:     295078

Transition matrix: 174425 nodes (76 terminal), 590252 minterms, vars: 34r/34c/18nd

Prob0A: 47 iterations in 0.34 seconds (average 0.007149, setup 0.00)

Prob1E: 4 iterations in 0.05 seconds (average 0.013000, setup 0.00)

yes = 951, no = 190621, maybe = 33171

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=224743, nc=58257, nnz=167028, k=2] [2.2 MB]
Creating vector for yes... [1.7 MB]
Allocating iteration vectors... [2 x 1.7 MB]
TOTAL: [7.3 MB]

Starting iterations...

Iterative method: 88 iterations in 0.35 seconds (average 0.001591, setup 0.21)

Value in the initial state: 0.9811703891471605

Time for model checking: 0.791 seconds.

Result: 0.9811703891471605 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmin=? [ F "MissionSuccessful" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=1,Objy1=1,Objx2=4,Objy2=0

Prob0E: 49 iterations in 0.40 seconds (average 0.008245, setup 0.00)

Prob1A: 3 iterations in 0.05 seconds (average 0.016000, setup 0.00)

yes = 951, no = 195912, maybe = 27880

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=224743, nc=48798, nnz=139330, k=2] [1.9 MB]
Creating vector for yes... [1.7 MB]
Allocating iteration vectors... [2 x 1.7 MB]
TOTAL: [7.0 MB]

Starting iterations...

Iterative method: 81 iterations in 0.30 seconds (average 0.001432, setup 0.19)

Value in the initial state: 0.7412025438940771

Time for model checking: 0.793 seconds.

Result: 0.7412025438940771 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmax=? [ F "MissionTimeUp" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=1,Objy1=1,Objx2=4,Objy2=0

Prob0A: 57 iterations in 0.60 seconds (average 0.010596, setup 0.00)

Prob1E: 4 iterations in 0.04 seconds (average 0.011000, setup 0.00)

yes = 448, no = 138779, maybe = 85516

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=224743, nc=146126, nnz=403367, k=4] [5.0 MB]
Creating vector for yes... [1.7 MB]
Allocating iteration vectors... [2 x 1.7 MB]
TOTAL: [10.1 MB]

Starting iterations...

Iterative method: 90 iterations in 0.45 seconds (average 0.002000, setup 0.27)

Value in the initial state: 8.525705171960697E-4

Time for model checking: 1.12 seconds.

Result: 8.525705171960697E-4 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmin=? [ F "MissionTimeUp" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=1,Objy1=1,Objx2=4,Objy2=0

Prob0E: 65 iterations in 0.66 seconds (average 0.010092, setup 0.00)

Prob1A: 3 iterations in 0.06 seconds (average 0.021333, setup 0.00)

yes = 448, no = 139136, maybe = 85159

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=224743, nc=145488, nnz=402058, k=4] [5.0 MB]
Creating vector for yes... [1.7 MB]
Allocating iteration vectors... [2 x 1.7 MB]
TOTAL: [10.1 MB]

Starting iterations...

Iterative method: 105 iterations in 0.49 seconds (average 0.001943, setup 0.29)

Value in the initial state: 1.922714923296761E-6

Time for model checking: 1.249 seconds.

Result: 1.922714923296761E-6 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmax=? [ F "Crash" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=1,Objy1=1,Objx2=4,Objy2=0

Prob0A: 3 iterations in 0.10 seconds (average 0.032000, setup 0.00)

Prob1E: 184 iterations in 2.68 seconds (average 0.014543, setup 0.00)

yes = 110157, no = 2616, maybe = 111970

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=224743, nc=182305, nnz=477479, k=4] [5.9 MB]
Creating vector for yes... [1.7 MB]
Allocating iteration vectors... [2 x 1.7 MB]
TOTAL: [11.0 MB]

Starting iterations...

Iterative method: 85 iterations in 0.51 seconds (average 0.002165, setup 0.32)

Value in the initial state: 0.03103088237914644

Time for model checking: 3.317 seconds.

Result: 0.03103088237914644 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmin=? [ F "Crash" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=1,Objy1=1,Objx2=4,Objy2=0

Prob0E: 3 iterations in 0.06 seconds (average 0.021333, setup 0.00)

Prob1A: 44 iterations in 0.47 seconds (average 0.010636, setup 0.00)

yes = 110157, no = 2616, maybe = 111970

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=224743, nc=182305, nnz=477479, k=4] [5.9 MB]
Creating vector for yes... [1.7 MB]
Allocating iteration vectors... [2 x 1.7 MB]
TOTAL: [11.0 MB]

Starting iterations...

Iterative method: 83 iterations in 0.44 seconds (average 0.002265, setup 0.26)

Value in the initial state: 0.016731835042955306

Time for model checking: 1.156 seconds.

Result: 0.016731835042955306 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmax=? [ F "AllAreaSearched" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=1,Objy1=1,Objx2=4,Objy2=0

Prob0A: 59 iterations in 0.48 seconds (average 0.008136, setup 0.00)

Prob1E: 4 iterations in 0.05 seconds (average 0.013000, setup 0.00)

yes = 1216, no = 177914, maybe = 45613

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=224743, nc=68228, nnz=176270, k=2] [2.3 MB]
Creating vector for yes... [1.7 MB]
Allocating iteration vectors... [2 x 1.7 MB]
TOTAL: [7.4 MB]

Starting iterations...

Iterative method: 87 iterations in 0.32 seconds (average 0.001655, setup 0.18)

Value in the initial state: 0.2300756247354806

Time for model checking: 1.186 seconds.

Result: 0.2300756247354806 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmin=? [ F "AllAreaSearched" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=1,Objy1=1,Objx2=4,Objy2=0

Prob0E: 63 iterations in 0.48 seconds (average 0.007556, setup 0.00)

Prob1A: 3 iterations in 0.04 seconds (average 0.013333, setup 0.00)

yes = 1216, no = 183695, maybe = 39832

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=224743, nc=58561, nnz=149267, k=2] [2.0 MB]
Creating vector for yes... [1.7 MB]
Allocating iteration vectors... [2 x 1.7 MB]
TOTAL: [7.1 MB]

Starting iterations...

Iterative method: 92 iterations in 0.28 seconds (average 0.001565, setup 0.14)

Value in the initial state: 0.001746567111359793

Time for model checking: 0.84 seconds.

Result: 0.001746567111359793 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmax=? [ F "MissionTimeUp"|"AllAreaSearched"|"Crash" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=1,Objy1=1,Objx2=4,Objy2=0

Prob0A: 3 iterations in 0.08 seconds (average 0.025333, setup 0.00)

Prob1E: 192 iterations in 2.39 seconds (average 0.012458, setup 0.00)

yes = 195911, no = 952, maybe = 27880

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=224743, nc=48798, nnz=139330, k=2] [1.9 MB]
Creating vector for yes... [1.7 MB]
Allocating iteration vectors... [2 x 1.7 MB]
TOTAL: [7.0 MB]

Starting iterations...

Iterative method: 74 iterations in 0.31 seconds (average 0.001351, setup 0.21)

Value in the initial state: 0.25879728140514274

Time for model checking: 2.88 seconds.

Result: 0.25879728140514274 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmin=? [ F "MissionTimeUp"|"AllAreaSearched"|"Crash" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=1,Objy1=1,Objx2=4,Objy2=0

Prob0E: 3 iterations in 0.05 seconds (average 0.016000, setup 0.00)

Prob1A: 47 iterations in 0.45 seconds (average 0.009532, setup 0.00)

yes = 190620, no = 952, maybe = 33171

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=224743, nc=58257, nnz=167028, k=2] [2.2 MB]
Creating vector for yes... [1.7 MB]
Allocating iteration vectors... [2 x 1.7 MB]
TOTAL: [7.3 MB]

Starting iterations...

Iterative method: 86 iterations in 0.30 seconds (average 0.001581, setup 0.17)

Value in the initial state: 0.018829101075420032

Time for model checking: 0.839 seconds.

Result: 0.018829101075420032 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"drop"}max=? [ F "deadlock" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=1,Objy1=1,Objx2=4,Objy2=0

Prob0E: 4 iterations in 0.07 seconds (average 0.018000, setup 0.00)

Prob1A: 1 iterations in 0.01 seconds (average 0.008000, setup 0.00)

goal = 1, inf = 0, maybe = 224742

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=224743, nc=295077, nnz=590251, k=4] [8.7 MB]
Building sparse matrix (transition rewards)... [n=224743, nc=295077, nnz=0, k=4] [2.0 MB]
Creating vector for state rewards... [1.7 MB]
Creating vector for inf... [1.7 MB]
Allocating iteration vectors... [2 x 1.7 MB]
TOTAL: [17.6 MB]

Starting iterations...

Iterative method: 84 iterations in 0.70 seconds (average 0.003238, setup 0.42)

Value in the initial state: 0.21135583665087637

Time for model checking: 0.802 seconds.

Result: 0.21135583665087637 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"drop"}min=? [ F "deadlock" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=1,Objy1=1,Objx2=4,Objy2=0

Prob0A: 4 iterations in 0.05 seconds (average 0.012000, setup 0.00)

Prob1E: 5 iterations in 0.05 seconds (average 0.009600, setup 0.00)

Warning: PRISM hasn't checked for zero-reward loops.
Your minimum rewards may be too low...

goal = 1, inf = 0, maybe = 224742

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=224743, nc=295077, nnz=590251, k=4] [8.7 MB]
Building sparse matrix (transition rewards)... [n=224743, nc=295077, nnz=0, k=4] [2.0 MB]
Creating vector for state rewards... [1.7 MB]
Creating vector for inf... [1.7 MB]
Allocating iteration vectors... [2 x 1.7 MB]
TOTAL: [17.6 MB]

Starting iterations...

Iterative method: 75 iterations in 0.57 seconds (average 0.003253, setup 0.32)

Value in the initial state: 0.18711965858199459

Time for model checking: 0.682 seconds.

Result: 0.18711965858199459 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"charge"}max=? [ F "deadlock" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=1,Objy1=1,Objx2=4,Objy2=0

Prob0E: 4 iterations in 0.03 seconds (average 0.007000, setup 0.00)

Prob1A: 1 iterations in 0.00 seconds (average 0.004000, setup 0.00)

goal = 1, inf = 0, maybe = 224742

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=224743, nc=295077, nnz=590251, k=4] [8.7 MB]
Building sparse matrix (transition rewards)... [n=224743, nc=295077, nnz=6458, k=4] [2.1 MB]
Creating vector for state rewards... [1.7 MB]
Creating vector for inf... [1.7 MB]
Allocating iteration vectors... [2 x 1.7 MB]
TOTAL: [17.7 MB]

Starting iterations...

Iterative method: 81 iterations in 0.60 seconds (average 0.003309, setup 0.33)

Value in the initial state: 0.2931461322413408

Time for model checking: 0.642 seconds.

Result: 0.2931461322413408 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"charge"}min=? [ F "deadlock" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=1,Objy1=1,Objx2=4,Objy2=0

Prob0A: 4 iterations in 0.02 seconds (average 0.006000, setup 0.00)

Prob1E: 5 iterations in 0.05 seconds (average 0.009600, setup 0.00)

Warning: PRISM hasn't checked for zero-reward loops.
Your minimum rewards may be too low...

goal = 1, inf = 0, maybe = 224742

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=224743, nc=295077, nnz=590251, k=4] [8.7 MB]
Building sparse matrix (transition rewards)... [n=224743, nc=295077, nnz=6458, k=4] [2.1 MB]
Creating vector for state rewards... [1.7 MB]
Creating vector for inf... [1.7 MB]
Allocating iteration vectors... [2 x 1.7 MB]
TOTAL: [17.7 MB]

Starting iterations...

Iterative method: 88 iterations in 0.62 seconds (average 0.003318, setup 0.32)

Value in the initial state: 0.04537845463647499

Time for model checking: 0.707 seconds.

Result: 0.04537845463647499 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"time"}max=? [ F "deadlock" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=1,Objy1=1,Objx2=4,Objy2=0

Prob0E: 4 iterations in 0.02 seconds (average 0.006000, setup 0.00)

Prob1A: 1 iterations in 0.01 seconds (average 0.008000, setup 0.00)

goal = 1, inf = 0, maybe = 224742

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=224743, nc=295077, nnz=590251, k=4] [8.7 MB]
Building sparse matrix (transition rewards)... [n=224743, nc=295077, nnz=112771, k=4] [3.3 MB]
Creating vector for state rewards... [1.7 MB]
Creating vector for inf... [1.7 MB]
Allocating iteration vectors... [2 x 1.7 MB]
TOTAL: [18.9 MB]

Starting iterations...

Iterative method: 88 iterations in 0.66 seconds (average 0.003364, setup 0.37)

Value in the initial state: 171.13089882612965

Time for model checking: 0.714 seconds.

Result: 171.13089882612965 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"time"}min=? [ F "deadlock" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=1,Objy1=1,Objx2=4,Objy2=0

Prob0A: 4 iterations in 0.02 seconds (average 0.006000, setup 0.00)

Prob1E: 5 iterations in 0.05 seconds (average 0.009600, setup 0.00)

Warning: PRISM hasn't checked for zero-reward loops.
Your minimum rewards may be too low...

goal = 1, inf = 0, maybe = 224742

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=224743, nc=295077, nnz=590251, k=4] [8.7 MB]
Building sparse matrix (transition rewards)... [n=224743, nc=295077, nnz=112771, k=4] [3.3 MB]
Creating vector for state rewards... [1.7 MB]
Creating vector for inf... [1.7 MB]
Allocating iteration vectors... [2 x 1.7 MB]
TOTAL: [18.9 MB]

Starting iterations...

Iterative method: 94 iterations in 0.68 seconds (average 0.003404, setup 0.36)

Value in the initial state: 92.73058723513088

Time for model checking: 0.765 seconds.

Result: 92.73058723513088 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"success"}max=? [ F "deadlock" ]/Pmin=? [ F "MissionSuccessful" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=1,Objy1=1,Objx2=4,Objy2=0

Prob0E: 4 iterations in 0.03 seconds (average 0.007000, setup 0.00)

Prob1A: 1 iterations in 0.01 seconds (average 0.008000, setup 0.00)

goal = 1, inf = 0, maybe = 224742

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=224743, nc=295077, nnz=590251, k=4] [8.7 MB]
Building sparse matrix (transition rewards)... [n=224743, nc=295077, nnz=1874, k=4] [2.0 MB]
Creating vector for state rewards... [1.7 MB]
Creating vector for inf... [1.7 MB]
Allocating iteration vectors... [2 x 1.7 MB]
TOTAL: [17.6 MB]

Starting iterations...

Iterative method: 90 iterations in 0.62 seconds (average 0.003244, setup 0.32)

Prob0E: 49 iterations in 0.38 seconds (average 0.007755, setup 0.00)

Prob1A: 3 iterations in 0.04 seconds (average 0.013333, setup 0.00)

yes = 951, no = 195912, maybe = 27880

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=224743, nc=48798, nnz=139330, k=2] [1.9 MB]
Creating vector for yes... [1.7 MB]
Allocating iteration vectors... [2 x 1.7 MB]
TOTAL: [7.0 MB]

Starting iterations...

Iterative method: 81 iterations in 0.26 seconds (average 0.001432, setup 0.14)

Value in the initial state: 226.19980501848934

Time for model checking: 1.353 seconds.

Result: 226.19980501848934 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"success"}min=? [ F "deadlock" ]/Pmax=? [ F "MissionSuccessful" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=1,Objy1=1,Objx2=4,Objy2=0

Prob0A: 4 iterations in 0.04 seconds (average 0.010000, setup 0.00)

Prob1E: 5 iterations in 0.05 seconds (average 0.009600, setup 0.00)

Warning: PRISM hasn't checked for zero-reward loops.
Your minimum rewards may be too low...

goal = 1, inf = 0, maybe = 224742

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=224743, nc=295077, nnz=590251, k=4] [8.7 MB]
Building sparse matrix (transition rewards)... [n=224743, nc=295077, nnz=1874, k=4] [2.0 MB]
Creating vector for state rewards... [1.7 MB]
Creating vector for inf... [1.7 MB]
Allocating iteration vectors... [2 x 1.7 MB]
TOTAL: [17.6 MB]

Starting iterations...

Iterative method: 85 iterations in 0.62 seconds (average 0.003247, setup 0.35)

Prob0A: 47 iterations in 0.39 seconds (average 0.008340, setup 0.00)

Prob1E: 4 iterations in 0.04 seconds (average 0.011000, setup 0.00)

yes = 951, no = 190621, maybe = 33171

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=224743, nc=58257, nnz=167028, k=2] [2.2 MB]
Creating vector for yes... [1.7 MB]
Allocating iteration vectors... [2 x 1.7 MB]
TOTAL: [7.3 MB]

Starting iterations...

Iterative method: 88 iterations in 0.30 seconds (average 0.001500, setup 0.17)

Value in the initial state: 79.03911998119088

Time for model checking: 1.485 seconds.

Result: 79.03911998119088 (value in the initial state)

---------------------------------------------------------------------

Note: There were 16 warnings during computation.

