PRISM
=====

Version: 4.3
Date: Tue Aug 02 12:26:03 BST 2016
Hostname: eglinton
Memory limits: cudd=1g, java(heap)=981.5m
Command line: prism uav_sim_ft_mdp.prism -s uav_mdp.props -const 'Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=1,Objy1=2,Objx2=5,Objy2=1,'

Parsing model file "uav_sim_ft_mdp.prism"...

Parsing properties file "uav_mdp.props"...

18 properties:
(1) Pmax=? [ F "MissionSuccessful" ]
(2) Pmin=? [ F "MissionSuccessful" ]
(3) Pmax=? [ F "MissionTimeUp" ]
(4) Pmin=? [ F "MissionTimeUp" ]
(5) Pmax=? [ F "Crash" ]
(6) Pmin=? [ F "Crash" ]
(7) Pmax=? [ F "AllAreaSearched" ]
(8) Pmin=? [ F "AllAreaSearched" ]
(9) Pmax=? [ F "MissionTimeUp"|"AllAreaSearched"|"Crash" ]
(10) Pmin=? [ F "MissionTimeUp"|"AllAreaSearched"|"Crash" ]
(11) R{"drop"}max=? [ F "deadlock" ]
(12) R{"drop"}min=? [ F "deadlock" ]
(13) R{"charge"}max=? [ F "deadlock" ]
(14) R{"charge"}min=? [ F "deadlock" ]
(15) R{"time"}max=? [ F "deadlock" ]
(16) R{"time"}min=? [ F "deadlock" ]
(17) R{"success"}max=? [ F "deadlock" ]/Pmin=? [ F "MissionSuccessful" ]
(18) R{"success"}min=? [ F "deadlock" ]/Pmax=? [ F "MissionSuccessful" ]

Type:        MDP
Modules:     Obj Move Robot T 
Variables:   NoOfObjs posx posy s1 retx rety b t c 

---------------------------------------------------------------------

Model checking: Pmax=? [ F "MissionSuccessful" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=1,Objy1=2,Objx2=5,Objy2=1

Building model...
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=1,Objy1=2,Objx2=5,Objy2=1

Warning: Guard for command 7 of module "Move" is never satisfied.

Warning: Guard for command 8 of module "Move" is never satisfied.

Warning: Guard for command 9 of module "Move" is never satisfied.

Warning: Guard for command 7 of module "Robot" is never satisfied.

Warning: Update 2 of command 3 of module "Robot" doesn't do anything ("(s1'=13)", line 249, column 44)

Warning: Update 2 of command 27 of module "T" doesn't do anything ("(c'=1)", line 372, column 108)

Warning: Guard for command 28 of module "T" is never satisfied.

Warning: Update 2 of command 9 of module "T" doesn't do anything ("(c'=1)", line 349, column 112)

Warning: Guard for command 10 of module "T" is never satisfied.

Warning: Update 2 of command 51 of module "T" doesn't do anything ("(c'=1)", line 403, column 113)

Warning: Guard for command 52 of module "T" is never satisfied.

Computing reachable states...

Reachability (BFS): 81 iterations in 0.96 seconds (average 0.011852, setup 0.00)

Time for model construction: 2.114 seconds.

Warning: Deadlocks detected and fixed in 1 states

Type:        MDP
States:      193472 (1 initial)
Transitions: 504662
Choices:     254450

Transition matrix: 171079 nodes (76 terminal), 504662 minterms, vars: 34r/34c/18nd

Prob0A: 45 iterations in 0.32 seconds (average 0.007022, setup 0.00)

Prob1E: 4 iterations in 0.05 seconds (average 0.013000, setup 0.00)

yes = 959, no = 162933, maybe = 29580

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=193472, nc=51971, nnz=147211, k=2] [1.9 MB]
Creating vector for yes... [1.5 MB]
Allocating iteration vectors... [2 x 1.5 MB]
TOTAL: [6.3 MB]

Starting iterations...

Iterative method: 88 iterations in 0.33 seconds (average 0.001364, setup 0.21)

Value in the initial state: 0.9808358104837706

Time for model checking: 0.74 seconds.

Result: 0.9808358104837706 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmin=? [ F "MissionSuccessful" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=1,Objy1=2,Objx2=5,Objy2=1

Prob0E: 47 iterations in 0.39 seconds (average 0.008255, setup 0.00)

Prob1A: 3 iterations in 0.05 seconds (average 0.016000, setup 0.00)

yes = 959, no = 167853, maybe = 24660

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=193472, nc=43153, nnz=121442, k=2] [1.6 MB]
Creating vector for yes... [1.5 MB]
Allocating iteration vectors... [2 x 1.5 MB]
TOTAL: [6.0 MB]

Starting iterations...

Iterative method: 83 iterations in 0.28 seconds (average 0.001253, setup 0.18)

Value in the initial state: 0.7571761613473028

Time for model checking: 0.745 seconds.

Result: 0.7571761613473028 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmax=? [ F "MissionTimeUp" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=1,Objy1=2,Objx2=5,Objy2=1

Prob0A: 60 iterations in 0.58 seconds (average 0.009733, setup 0.00)

Prob1E: 4 iterations in 0.04 seconds (average 0.010000, setup 0.00)

yes = 400, no = 121273, maybe = 71799

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=193472, nc=122833, nnz=336863, k=4] [4.2 MB]
Creating vector for yes... [1.5 MB]
Allocating iteration vectors... [2 x 1.5 MB]
TOTAL: [8.6 MB]

Starting iterations...

Iterative method: 88 iterations in 0.39 seconds (average 0.001682, setup 0.24)

Value in the initial state: 6.332261203412501E-4

Time for model checking: 1.064 seconds.

Result: 6.332261203412501E-4 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmin=? [ F "MissionTimeUp" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=1,Objy1=2,Objx2=5,Objy2=1

Prob0E: 68 iterations in 0.61 seconds (average 0.009000, setup 0.00)

Prob1A: 3 iterations in 0.04 seconds (average 0.014667, setup 0.00)

yes = 400, no = 121611, maybe = 71461

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=193472, nc=122233, nnz=335593, k=4] [4.1 MB]
Creating vector for yes... [1.5 MB]
Allocating iteration vectors... [2 x 1.5 MB]
TOTAL: [8.6 MB]

Starting iterations...

Iterative method: 105 iterations in 0.42 seconds (average 0.001714, setup 0.24)

Value in the initial state: 1.756273360164157E-6

Time for model checking: 1.128 seconds.

Result: 1.756273360164157E-6 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmax=? [ F "Crash" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=1,Objy1=2,Objx2=5,Objy2=1

Prob0A: 3 iterations in 0.12 seconds (average 0.038667, setup 0.00)

Prob1E: 175 iterations in 2.51 seconds (average 0.014354, setup 0.00)

yes = 94858, no = 2613, maybe = 96001

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=193472, nc=156979, nnz=407191, k=4] [5.0 MB]
Creating vector for yes... [1.5 MB]
Allocating iteration vectors... [2 x 1.5 MB]
TOTAL: [9.4 MB]

Starting iterations...

Iterative method: 86 iterations in 0.45 seconds (average 0.001674, setup 0.30)

Value in the initial state: 0.030358832257846025

Time for model checking: 3.114 seconds.

Result: 0.030358832257846025 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmin=? [ F "Crash" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=1,Objy1=2,Objx2=5,Objy2=1

Prob0E: 3 iterations in 0.06 seconds (average 0.020000, setup 0.00)

Prob1A: 44 iterations in 0.45 seconds (average 0.010182, setup 0.00)

yes = 94858, no = 2613, maybe = 96001

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=193472, nc=156979, nnz=407191, k=4] [5.0 MB]
Creating vector for yes... [1.5 MB]
Allocating iteration vectors... [2 x 1.5 MB]
TOTAL: [9.4 MB]

Starting iterations...

Iterative method: 82 iterations in 0.37 seconds (average 0.001902, setup 0.21)

Value in the initial state: 0.017093592785020744

Time for model checking: 0.905 seconds.

Result: 0.017093592785020744 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmax=? [ F "AllAreaSearched" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=1,Objy1=2,Objx2=5,Objy2=1

Prob0A: 54 iterations in 0.42 seconds (average 0.007852, setup 0.00)

Prob1E: 4 iterations in 0.05 seconds (average 0.013000, setup 0.00)

yes = 1253, no = 152649, maybe = 39570

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=193472, nc=59298, nnz=150205, k=2] [2.0 MB]
Creating vector for yes... [1.5 MB]
Allocating iteration vectors... [2 x 1.5 MB]
TOTAL: [6.4 MB]

Starting iterations...

Iterative method: 84 iterations in 0.29 seconds (average 0.001381, setup 0.17)

Value in the initial state: 0.21503633474842654

Time for model checking: 0.781 seconds.

Result: 0.21503633474842654 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmin=? [ F "AllAreaSearched" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=1,Objy1=2,Objx2=5,Objy2=1

Prob0E: 62 iterations in 0.43 seconds (average 0.006968, setup 0.00)

Prob1A: 3 iterations in 0.04 seconds (average 0.013333, setup 0.00)

yes = 1253, no = 157249, maybe = 34970

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=193472, nc=51642, nnz=129122, k=2] [1.7 MB]
Creating vector for yes... [1.5 MB]
Allocating iteration vectors... [2 x 1.5 MB]
TOTAL: [6.1 MB]

Starting iterations...

Iterative method: 91 iterations in 0.26 seconds (average 0.001319, setup 0.14)

Value in the initial state: 0.001757778206031682

Time for model checking: 0.752 seconds.

Result: 0.001757778206031682 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmax=? [ F "MissionTimeUp"|"AllAreaSearched"|"Crash" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=1,Objy1=2,Objx2=5,Objy2=1

Prob0A: 3 iterations in 0.07 seconds (average 0.024000, setup 0.00)

Prob1E: 184 iterations in 2.37 seconds (average 0.012891, setup 0.00)

yes = 167852, no = 960, maybe = 24660

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=193472, nc=43153, nnz=121442, k=2] [1.6 MB]
Creating vector for yes... [1.5 MB]
Allocating iteration vectors... [2 x 1.5 MB]
TOTAL: [6.0 MB]

Starting iterations...

Iterative method: 76 iterations in 0.27 seconds (average 0.001263, setup 0.18)

Value in the initial state: 0.24282355798075234

Time for model checking: 2.767 seconds.

Result: 0.24282355798075234 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmin=? [ F "MissionTimeUp"|"AllAreaSearched"|"Crash" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=1,Objy1=2,Objx2=5,Objy2=1

Prob0E: 3 iterations in 0.05 seconds (average 0.016000, setup 0.00)

Prob1A: 45 iterations in 0.45 seconds (average 0.010044, setup 0.00)

yes = 162932, no = 960, maybe = 29580

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=193472, nc=51971, nnz=147211, k=2] [1.9 MB]
Creating vector for yes... [1.5 MB]
Allocating iteration vectors... [2 x 1.5 MB]
TOTAL: [6.3 MB]

Starting iterations...

Iterative method: 87 iterations in 0.28 seconds (average 0.001287, setup 0.16)

Value in the initial state: 0.01916330761780588

Time for model checking: 0.801 seconds.

Result: 0.01916330761780588 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"drop"}max=? [ F "deadlock" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=1,Objy1=2,Objx2=5,Objy2=1

Prob0E: 4 iterations in 0.07 seconds (average 0.017000, setup 0.00)

Prob1A: 1 iterations in 0.01 seconds (average 0.008000, setup 0.00)

goal = 1, inf = 0, maybe = 193471

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=193472, nc=254449, nnz=504661, k=4] [7.5 MB]
Building sparse matrix (transition rewards)... [n=193472, nc=254449, nnz=0, k=4] [1.7 MB]
Creating vector for state rewards... [1.5 MB]
Creating vector for inf... [1.5 MB]
Allocating iteration vectors... [2 x 1.5 MB]
TOTAL: [15.1 MB]

Starting iterations...

Iterative method: 85 iterations in 0.62 seconds (average 0.002729, setup 0.39)

Value in the initial state: 0.17638280870844528

Time for model checking: 0.726 seconds.

Result: 0.17638280870844528 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"drop"}min=? [ F "deadlock" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=1,Objy1=2,Objx2=5,Objy2=1

Prob0A: 4 iterations in 0.04 seconds (average 0.010000, setup 0.00)

Prob1E: 5 iterations in 0.05 seconds (average 0.010400, setup 0.00)

Warning: PRISM hasn't checked for zero-reward loops.
Your minimum rewards may be too low...

goal = 1, inf = 0, maybe = 193471

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=193472, nc=254449, nnz=504661, k=4] [7.5 MB]
Building sparse matrix (transition rewards)... [n=193472, nc=254449, nnz=0, k=4] [1.7 MB]
Creating vector for state rewards... [1.5 MB]
Creating vector for inf... [1.5 MB]
Allocating iteration vectors... [2 x 1.5 MB]
TOTAL: [15.1 MB]

Starting iterations...

Iterative method: 76 iterations in 0.49 seconds (average 0.002789, setup 0.28)

Value in the initial state: 0.16285248242318184

Time for model checking: 0.6 seconds.

Result: 0.16285248242318184 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"charge"}max=? [ F "deadlock" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=1,Objy1=2,Objx2=5,Objy2=1

Prob0E: 4 iterations in 0.02 seconds (average 0.006000, setup 0.00)

Prob1A: 1 iterations in 0.01 seconds (average 0.008000, setup 0.00)

goal = 1, inf = 0, maybe = 193471

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=193472, nc=254449, nnz=504661, k=4] [7.5 MB]
Building sparse matrix (transition rewards)... [n=193472, nc=254449, nnz=5402, k=4] [1.8 MB]
Creating vector for state rewards... [1.5 MB]
Creating vector for inf... [1.5 MB]
Allocating iteration vectors... [2 x 1.5 MB]
TOTAL: [15.2 MB]

Starting iterations...

Iterative method: 83 iterations in 0.52 seconds (average 0.002747, setup 0.29)

Value in the initial state: 0.2743707123490619

Time for model checking: 0.566 seconds.

Result: 0.2743707123490619 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"charge"}min=? [ F "deadlock" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=1,Objy1=2,Objx2=5,Objy2=1

Prob0A: 4 iterations in 0.02 seconds (average 0.006000, setup 0.00)

Prob1E: 5 iterations in 0.04 seconds (average 0.008800, setup 0.00)

Warning: PRISM hasn't checked for zero-reward loops.
Your minimum rewards may be too low...

goal = 1, inf = 0, maybe = 193471

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=193472, nc=254449, nnz=504661, k=4] [7.5 MB]
Building sparse matrix (transition rewards)... [n=193472, nc=254449, nnz=5402, k=4] [1.8 MB]
Creating vector for state rewards... [1.5 MB]
Creating vector for inf... [1.5 MB]
Allocating iteration vectors... [2 x 1.5 MB]
TOTAL: [15.2 MB]

Starting iterations...

Iterative method: 89 iterations in 0.54 seconds (average 0.002831, setup 0.29)

Value in the initial state: 0.04218427058168637

Time for model checking: 0.616 seconds.

Result: 0.04218427058168637 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"time"}max=? [ F "deadlock" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=1,Objy1=2,Objx2=5,Objy2=1

Prob0E: 4 iterations in 0.02 seconds (average 0.006000, setup 0.00)

Prob1A: 1 iterations in 0.00 seconds (average 0.004000, setup 0.00)

goal = 1, inf = 0, maybe = 193471

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=193472, nc=254449, nnz=504661, k=4] [7.5 MB]
Building sparse matrix (transition rewards)... [n=193472, nc=254449, nnz=97469, k=4] [2.8 MB]
Creating vector for state rewards... [1.5 MB]
Creating vector for inf... [1.5 MB]
Allocating iteration vectors... [2 x 1.5 MB]
TOTAL: [16.2 MB]

Starting iterations...

Iterative method: 88 iterations in 0.58 seconds (average 0.002864, setup 0.32)

Value in the initial state: 167.48905408009514

Time for model checking: 0.613 seconds.

Result: 167.48905408009514 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"time"}min=? [ F "deadlock" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=1,Objy1=2,Objx2=5,Objy2=1

Prob0A: 4 iterations in 0.02 seconds (average 0.006000, setup 0.00)

Prob1E: 5 iterations in 0.04 seconds (average 0.008800, setup 0.00)

Warning: PRISM hasn't checked for zero-reward loops.
Your minimum rewards may be too low...

goal = 1, inf = 0, maybe = 193471

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=193472, nc=254449, nnz=504661, k=4] [7.5 MB]
Building sparse matrix (transition rewards)... [n=193472, nc=254449, nnz=97469, k=4] [2.8 MB]
Creating vector for state rewards... [1.5 MB]
Creating vector for inf... [1.5 MB]
Allocating iteration vectors... [2 x 1.5 MB]
TOTAL: [16.2 MB]

Starting iterations...

Iterative method: 94 iterations in 0.58 seconds (average 0.002936, setup 0.30)

Value in the initial state: 94.73197539901624

Time for model checking: 0.672 seconds.

Result: 94.73197539901624 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"success"}max=? [ F "deadlock" ]/Pmin=? [ F "MissionSuccessful" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=1,Objy1=2,Objx2=5,Objy2=1

Prob0E: 4 iterations in 0.02 seconds (average 0.005000, setup 0.00)

Prob1A: 1 iterations in 0.00 seconds (average 0.004000, setup 0.00)

goal = 1, inf = 0, maybe = 193471

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=193472, nc=254449, nnz=504661, k=4] [7.5 MB]
Building sparse matrix (transition rewards)... [n=193472, nc=254449, nnz=1890, k=4] [1.7 MB]
Creating vector for state rewards... [1.5 MB]
Creating vector for inf... [1.5 MB]
Allocating iteration vectors... [2 x 1.5 MB]
TOTAL: [15.1 MB]

Starting iterations...

Iterative method: 91 iterations in 0.53 seconds (average 0.002769, setup 0.28)

Prob0E: 47 iterations in 0.36 seconds (average 0.007574, setup 0.00)

Prob1A: 3 iterations in 0.04 seconds (average 0.013333, setup 0.00)

yes = 959, no = 167853, maybe = 24660

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=193472, nc=43153, nnz=121442, k=2] [1.6 MB]
Creating vector for yes... [1.5 MB]
Allocating iteration vectors... [2 x 1.5 MB]
TOTAL: [6.0 MB]

Starting iterations...

Iterative method: 83 iterations in 0.24 seconds (average 0.001205, setup 0.14)

Value in the initial state: 216.80796595370205

Time for model checking: 1.223 seconds.

Result: 216.80796595370205 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"success"}min=? [ F "deadlock" ]/Pmax=? [ F "MissionSuccessful" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=1,Objy1=2,Objx2=5,Objy2=1

Prob0A: 4 iterations in 0.05 seconds (average 0.013000, setup 0.00)

Prob1E: 5 iterations in 0.04 seconds (average 0.008800, setup 0.00)

Warning: PRISM hasn't checked for zero-reward loops.
Your minimum rewards may be too low...

goal = 1, inf = 0, maybe = 193471

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=193472, nc=254449, nnz=504661, k=4] [7.5 MB]
Building sparse matrix (transition rewards)... [n=193472, nc=254449, nnz=1890, k=4] [1.7 MB]
Creating vector for state rewards... [1.5 MB]
Creating vector for inf... [1.5 MB]
Allocating iteration vectors... [2 x 1.5 MB]
TOTAL: [15.1 MB]

Starting iterations...

Iterative method: 86 iterations in 0.55 seconds (average 0.002791, setup 0.31)

Prob0A: 45 iterations in 0.30 seconds (average 0.006756, setup 0.00)

Prob1E: 4 iterations in 0.04 seconds (average 0.010000, setup 0.00)

yes = 959, no = 162933, maybe = 29580

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=193472, nc=51971, nnz=147211, k=2] [1.9 MB]
Creating vector for yes... [1.5 MB]
Allocating iteration vectors... [2 x 1.5 MB]
TOTAL: [6.3 MB]

Starting iterations...

Iterative method: 88 iterations in 0.27 seconds (average 0.001273, setup 0.16)

Value in the initial state: 79.75026593700701

Time for model checking: 1.254 seconds.

Result: 79.75026593700701 (value in the initial state)

---------------------------------------------------------------------

Note: There were 16 warnings during computation.

