PRISM
=====

Version: 4.3
Date: Tue Aug 02 12:38:09 BST 2016
Hostname: eglinton
Memory limits: cudd=1g, java(heap)=981.5m
Command line: prism uav_sim_ft_mdp.prism -s uav_mdp.props -const 'Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=2,Objy1=0,Objx2=4,Objy2=3,'

Parsing model file "uav_sim_ft_mdp.prism"...

Parsing properties file "uav_mdp.props"...

18 properties:
(1) Pmax=? [ F "MissionSuccessful" ]
(2) Pmin=? [ F "MissionSuccessful" ]
(3) Pmax=? [ F "MissionTimeUp" ]
(4) Pmin=? [ F "MissionTimeUp" ]
(5) Pmax=? [ F "Crash" ]
(6) Pmin=? [ F "Crash" ]
(7) Pmax=? [ F "AllAreaSearched" ]
(8) Pmin=? [ F "AllAreaSearched" ]
(9) Pmax=? [ F "MissionTimeUp"|"AllAreaSearched"|"Crash" ]
(10) Pmin=? [ F "MissionTimeUp"|"AllAreaSearched"|"Crash" ]
(11) R{"drop"}max=? [ F "deadlock" ]
(12) R{"drop"}min=? [ F "deadlock" ]
(13) R{"charge"}max=? [ F "deadlock" ]
(14) R{"charge"}min=? [ F "deadlock" ]
(15) R{"time"}max=? [ F "deadlock" ]
(16) R{"time"}min=? [ F "deadlock" ]
(17) R{"success"}max=? [ F "deadlock" ]/Pmin=? [ F "MissionSuccessful" ]
(18) R{"success"}min=? [ F "deadlock" ]/Pmax=? [ F "MissionSuccessful" ]

Type:        MDP
Modules:     Obj Move Robot T 
Variables:   NoOfObjs posx posy s1 retx rety b t c 

---------------------------------------------------------------------

Model checking: Pmax=? [ F "MissionSuccessful" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=2,Objy1=0,Objx2=4,Objy2=3

Building model...
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=2,Objy1=0,Objx2=4,Objy2=3

Warning: Guard for command 7 of module "Move" is never satisfied.

Warning: Guard for command 8 of module "Move" is never satisfied.

Warning: Guard for command 9 of module "Move" is never satisfied.

Warning: Guard for command 7 of module "Robot" is never satisfied.

Warning: Update 2 of command 3 of module "Robot" doesn't do anything ("(s1'=13)", line 249, column 44)

Warning: Update 2 of command 27 of module "T" doesn't do anything ("(c'=1)", line 372, column 108)

Warning: Guard for command 28 of module "T" is never satisfied.

Warning: Update 2 of command 9 of module "T" doesn't do anything ("(c'=1)", line 349, column 112)

Warning: Guard for command 10 of module "T" is never satisfied.

Warning: Update 2 of command 51 of module "T" doesn't do anything ("(c'=1)", line 403, column 113)

Warning: Guard for command 52 of module "T" is never satisfied.

Computing reachable states...

Reachability (BFS): 81 iterations in 1.02 seconds (average 0.012543, setup 0.00)

Time for model construction: 2.285 seconds.

Warning: Deadlocks detected and fixed in 1 states

Type:        MDP
States:      228011 (1 initial)
Transitions: 593397
Choices:     298184

Transition matrix: 176019 nodes (76 terminal), 593397 minterms, vars: 34r/34c/18nd

Prob0A: 60 iterations in 0.46 seconds (average 0.007600, setup 0.00)

Prob1E: 4 iterations in 0.06 seconds (average 0.015000, setup 0.00)

yes = 940, no = 195489, maybe = 31582

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=228011, nc=53211, nnz=148552, k=2] [2.0 MB]
Creating vector for yes... [1.7 MB]
Allocating iteration vectors... [2 x 1.7 MB]
TOTAL: [7.2 MB]

Starting iterations...

Iterative method: 97 iterations in 0.37 seconds (average 0.001526, setup 0.22)

Value in the initial state: 0.9778248131944449

Time for model checking: 0.925 seconds.

Result: 0.9778248131944449 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmin=? [ F "MissionSuccessful" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=2,Objy1=0,Objx2=4,Objy2=3

Prob0E: 66 iterations in 0.50 seconds (average 0.007636, setup 0.00)

Prob1A: 3 iterations in 0.07 seconds (average 0.024000, setup 0.00)

yes = 940, no = 199855, maybe = 27216

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=228011, nc=46115, nnz=128977, k=2] [1.7 MB]
Creating vector for yes... [1.7 MB]
Allocating iteration vectors... [2 x 1.7 MB]
TOTAL: [7.0 MB]

Starting iterations...

Iterative method: 89 iterations in 0.31 seconds (average 0.001483, setup 0.18)

Value in the initial state: 0.7507769794081138

Time for model checking: 0.913 seconds.

Result: 0.7507769794081138 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmax=? [ F "MissionTimeUp" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=2,Objy1=0,Objx2=4,Objy2=3

Prob0A: 63 iterations in 0.64 seconds (average 0.010222, setup 0.00)

Prob1E: 4 iterations in 0.05 seconds (average 0.013000, setup 0.00)

yes = 484, no = 129911, maybe = 97616

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=228011, nc=157807, nnz=424518, k=4] [5.2 MB]
Creating vector for yes... [1.7 MB]
Allocating iteration vectors... [2 x 1.7 MB]
TOTAL: [10.4 MB]

Starting iterations...

Iterative method: 92 iterations in 0.48 seconds (average 0.002087, setup 0.28)

Value in the initial state: 0.0023655305130760125

Time for model checking: 1.205 seconds.

Result: 0.0023655305130760125 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmin=? [ F "MissionTimeUp" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=2,Objy1=0,Objx2=4,Objy2=3

Prob0E: 69 iterations in 0.85 seconds (average 0.012348, setup 0.00)

Prob1A: 3 iterations in 0.05 seconds (average 0.017333, setup 0.00)

yes = 484, no = 130249, maybe = 97278

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=228011, nc=157207, nnz=423248, k=4] [5.2 MB]
Creating vector for yes... [1.7 MB]
Allocating iteration vectors... [2 x 1.7 MB]
TOTAL: [10.4 MB]

Starting iterations...

Iterative method: 108 iterations in 0.51 seconds (average 0.002111, setup 0.28)

Value in the initial state: 6.287270207428548E-6

Time for model checking: 1.448 seconds.

Result: 6.287270207428548E-6 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmax=? [ F "Crash" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=2,Objy1=0,Objx2=4,Objy2=3

Prob0A: 3 iterations in 0.08 seconds (average 0.026667, setup 0.00)

Prob1E: 232 iterations in 3.01 seconds (average 0.012983, setup 0.00)

yes = 111771, no = 2701, maybe = 113539

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=228011, nc=183712, nnz=478925, k=4] [5.9 MB]
Creating vector for yes... [1.7 MB]
Allocating iteration vectors... [2 x 1.7 MB]
TOTAL: [11.1 MB]

Starting iterations...

Iterative method: 91 iterations in 0.53 seconds (average 0.002066, setup 0.34)

Value in the initial state: 0.0628103774232601

Time for model checking: 3.677 seconds.

Result: 0.0628103774232601 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmin=? [ F "Crash" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=2,Objy1=0,Objx2=4,Objy2=3

Prob0E: 3 iterations in 0.06 seconds (average 0.020000, setup 0.00)

Prob1A: 50 iterations in 0.65 seconds (average 0.013040, setup 0.00)

yes = 111771, no = 2701, maybe = 113539

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=228011, nc=183712, nnz=478925, k=4] [5.9 MB]
Creating vector for yes... [1.7 MB]
Allocating iteration vectors... [2 x 1.7 MB]
TOTAL: [11.1 MB]

Starting iterations...

Iterative method: 96 iterations in 0.51 seconds (average 0.002250, setup 0.29)

Value in the initial state: 0.020192553573194768

Time for model checking: 1.253 seconds.

Result: 0.020192553573194768 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmax=? [ F "AllAreaSearched" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=2,Objy1=0,Objx2=4,Objy2=3

Prob0A: 61 iterations in 0.44 seconds (average 0.007148, setup 0.00)

Prob1E: 4 iterations in 0.04 seconds (average 0.011000, setup 0.00)

yes = 1276, no = 182477, maybe = 44258

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=228011, nc=71156, nnz=190713, k=2] [2.5 MB]
Creating vector for yes... [1.7 MB]
Allocating iteration vectors... [2 x 1.7 MB]
TOTAL: [7.7 MB]

Starting iterations...

Iterative method: 87 iterations in 0.32 seconds (average 0.001701, setup 0.17)

Value in the initial state: 0.2203016612593592

Time for model checking: 0.827 seconds.

Result: 0.2203016612593592 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmin=? [ F "AllAreaSearched" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=2,Objy1=0,Objx2=4,Objy2=3

Prob0E: 69 iterations in 0.48 seconds (average 0.007014, setup 0.00)

Prob1A: 3 iterations in 0.04 seconds (average 0.013333, setup 0.00)

yes = 1276, no = 189435, maybe = 37300

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=228011, nc=60451, nnz=161926, k=2] [2.1 MB]
Creating vector for yes... [1.7 MB]
Allocating iteration vectors... [2 x 1.7 MB]
TOTAL: [7.3 MB]

Starting iterations...

Iterative method: 88 iterations in 0.27 seconds (average 0.001545, setup 0.13)

Value in the initial state: 0.0012669614019673629

Time for model checking: 0.815 seconds.

Result: 0.0012669614019673629 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmax=? [ F "MissionTimeUp"|"AllAreaSearched"|"Crash" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=2,Objy1=0,Objx2=4,Objy2=3

Prob0A: 3 iterations in 0.10 seconds (average 0.033333, setup 0.00)

Prob1E: 256 iterations in 3.39 seconds (average 0.013234, setup 0.00)

yes = 199854, no = 941, maybe = 27216

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=228011, nc=46115, nnz=128977, k=2] [1.7 MB]
Creating vector for yes... [1.7 MB]
Allocating iteration vectors... [2 x 1.7 MB]
TOTAL: [7.0 MB]

Starting iterations...

Iterative method: 83 iterations in 0.30 seconds (average 0.001446, setup 0.18)

Value in the initial state: 0.24922268747279605

Time for model checking: 3.809 seconds.

Result: 0.24922268747279605 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmin=? [ F "MissionTimeUp"|"AllAreaSearched"|"Crash" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=2,Objy1=0,Objx2=4,Objy2=3

Prob0E: 3 iterations in 0.06 seconds (average 0.018667, setup 0.00)

Prob1A: 60 iterations in 0.52 seconds (average 0.008600, setup 0.00)

yes = 195488, no = 941, maybe = 31582

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=228011, nc=53211, nnz=148552, k=2] [2.0 MB]
Creating vector for yes... [1.7 MB]
Allocating iteration vectors... [2 x 1.7 MB]
TOTAL: [7.2 MB]

Starting iterations...

Iterative method: 95 iterations in 0.31 seconds (average 0.001558, setup 0.16)

Value in the initial state: 0.022174928710079495

Time for model checking: 0.898 seconds.

Result: 0.022174928710079495 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"drop"}max=? [ F "deadlock" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=2,Objy1=0,Objx2=4,Objy2=3

Prob0E: 4 iterations in 0.06 seconds (average 0.015000, setup 0.00)

Prob1A: 1 iterations in 0.01 seconds (average 0.008000, setup 0.00)

goal = 1, inf = 0, maybe = 228010

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=228011, nc=298183, nnz=593396, k=4] [8.8 MB]
Building sparse matrix (transition rewards)... [n=228011, nc=298183, nnz=0, k=4] [2.0 MB]
Creating vector for state rewards... [1.7 MB]
Creating vector for inf... [1.7 MB]
Allocating iteration vectors... [2 x 1.7 MB]
TOTAL: [17.8 MB]

Starting iterations...

Iterative method: 92 iterations in 0.72 seconds (average 0.003391, setup 0.40)

Value in the initial state: 0.25710083280507484

Time for model checking: 0.807 seconds.

Result: 0.25710083280507484 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"drop"}min=? [ F "deadlock" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=2,Objy1=0,Objx2=4,Objy2=3

Prob0A: 4 iterations in 0.04 seconds (average 0.010000, setup 0.00)

Prob1E: 5 iterations in 0.05 seconds (average 0.009600, setup 0.00)

Warning: PRISM hasn't checked for zero-reward loops.
Your minimum rewards may be too low...

goal = 1, inf = 0, maybe = 228010

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=228011, nc=298183, nnz=593396, k=4] [8.8 MB]
Building sparse matrix (transition rewards)... [n=228011, nc=298183, nnz=0, k=4] [2.0 MB]
Creating vector for state rewards... [1.7 MB]
Creating vector for inf... [1.7 MB]
Allocating iteration vectors... [2 x 1.7 MB]
TOTAL: [17.8 MB]

Starting iterations...

Iterative method: 85 iterations in 0.59 seconds (average 0.003294, setup 0.31)

Value in the initial state: 0.18475557200667356

Time for model checking: 0.703 seconds.

Result: 0.18475557200667356 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"charge"}max=? [ F "deadlock" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=2,Objy1=0,Objx2=4,Objy2=3

Prob0E: 4 iterations in 0.03 seconds (average 0.008000, setup 0.00)

Prob1A: 1 iterations in 0.00 seconds (average 0.004000, setup 0.00)

goal = 1, inf = 0, maybe = 228010

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=228011, nc=298183, nnz=593396, k=4] [8.8 MB]
Building sparse matrix (transition rewards)... [n=228011, nc=298183, nnz=6458, k=4] [2.1 MB]
Creating vector for state rewards... [1.7 MB]
Creating vector for inf... [1.7 MB]
Allocating iteration vectors... [2 x 1.7 MB]
TOTAL: [17.8 MB]

Starting iterations...

Iterative method: 88 iterations in 0.61 seconds (average 0.003364, setup 0.31)

Value in the initial state: 0.9783281475859362

Time for model checking: 0.668 seconds.

Result: 0.9783281475859362 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"charge"}min=? [ F "deadlock" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=2,Objy1=0,Objx2=4,Objy2=3

Prob0A: 4 iterations in 0.03 seconds (average 0.007000, setup 0.00)

Prob1E: 5 iterations in 0.05 seconds (average 0.010400, setup 0.00)

Warning: PRISM hasn't checked for zero-reward loops.
Your minimum rewards may be too low...

goal = 1, inf = 0, maybe = 228010

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=228011, nc=298183, nnz=593396, k=4] [8.8 MB]
Building sparse matrix (transition rewards)... [n=228011, nc=298183, nnz=6458, k=4] [2.1 MB]
Creating vector for state rewards... [1.7 MB]
Creating vector for inf... [1.7 MB]
Allocating iteration vectors... [2 x 1.7 MB]
TOTAL: [17.8 MB]

Starting iterations...

Iterative method: 97 iterations in 0.65 seconds (average 0.003381, setup 0.32)

Value in the initial state: 0.06618118457703577

Time for model checking: 0.739 seconds.

Result: 0.06618118457703577 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"time"}max=? [ F "deadlock" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=2,Objy1=0,Objx2=4,Objy2=3

Prob0E: 4 iterations in 0.03 seconds (average 0.008000, setup 0.00)

Prob1A: 1 iterations in 0.01 seconds (average 0.008000, setup 0.00)

goal = 1, inf = 0, maybe = 228010

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=228011, nc=298183, nnz=593396, k=4] [8.8 MB]
Building sparse matrix (transition rewards)... [n=228011, nc=298183, nnz=114470, k=4] [3.3 MB]
Creating vector for state rewards... [1.7 MB]
Creating vector for inf... [1.7 MB]
Allocating iteration vectors... [2 x 1.7 MB]
TOTAL: [19.1 MB]

Starting iterations...

Iterative method: 92 iterations in 0.68 seconds (average 0.003348, setup 0.37)

Value in the initial state: 344.9649078626312

Time for model checking: 0.737 seconds.

Result: 344.9649078626312 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"time"}min=? [ F "deadlock" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=2,Objy1=0,Objx2=4,Objy2=3

Prob0A: 4 iterations in 0.02 seconds (average 0.006000, setup 0.00)

Prob1E: 5 iterations in 0.05 seconds (average 0.009600, setup 0.00)

Warning: PRISM hasn't checked for zero-reward loops.
Your minimum rewards may be too low...

goal = 1, inf = 0, maybe = 228010

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=228011, nc=298183, nnz=593396, k=4] [8.8 MB]
Building sparse matrix (transition rewards)... [n=228011, nc=298183, nnz=114470, k=4] [3.3 MB]
Creating vector for state rewards... [1.7 MB]
Creating vector for inf... [1.7 MB]
Allocating iteration vectors... [2 x 1.7 MB]
TOTAL: [19.1 MB]

Starting iterations...

Iterative method: 103 iterations in 0.71 seconds (average 0.003417, setup 0.36)

Value in the initial state: 111.80998270639793

Time for model checking: 0.805 seconds.

Result: 111.80998270639793 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"success"}max=? [ F "deadlock" ]/Pmin=? [ F "MissionSuccessful" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=2,Objy1=0,Objx2=4,Objy2=3

Prob0E: 4 iterations in 0.03 seconds (average 0.007000, setup 0.00)

Prob1A: 1 iterations in 0.01 seconds (average 0.008000, setup 0.00)

goal = 1, inf = 0, maybe = 228010

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=228011, nc=298183, nnz=593396, k=4] [8.8 MB]
Building sparse matrix (transition rewards)... [n=228011, nc=298183, nnz=1852, k=4] [2.0 MB]
Creating vector for state rewards... [1.7 MB]
Creating vector for inf... [1.7 MB]
Allocating iteration vectors... [2 x 1.7 MB]
TOTAL: [17.8 MB]

Starting iterations...

Iterative method: 95 iterations in 0.64 seconds (average 0.003284, setup 0.32)

Prob0E: 66 iterations in 0.42 seconds (average 0.006424, setup 0.00)

Prob1A: 3 iterations in 0.06 seconds (average 0.018667, setup 0.00)

yes = 940, no = 199855, maybe = 27216

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=228011, nc=46115, nnz=128977, k=2] [1.7 MB]
Creating vector for yes... [1.7 MB]
Allocating iteration vectors... [2 x 1.7 MB]
TOTAL: [7.0 MB]

Starting iterations...

Iterative method: 89 iterations in 0.41 seconds (average 0.002831, setup 0.16)

Value in the initial state: 448.68997705543904

Time for model checking: 1.587 seconds.

Result: 448.68997705543904 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"success"}min=? [ F "deadlock" ]/Pmax=? [ F "MissionSuccessful" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=2,Objy1=0,Objx2=4,Objy2=3

Prob0A: 4 iterations in 0.05 seconds (average 0.012000, setup 0.00)

Prob1E: 5 iterations in 0.06 seconds (average 0.012000, setup 0.00)

Warning: PRISM hasn't checked for zero-reward loops.
Your minimum rewards may be too low...

goal = 1, inf = 0, maybe = 228010

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=228011, nc=298183, nnz=593396, k=4] [8.8 MB]
Building sparse matrix (transition rewards)... [n=228011, nc=298183, nnz=1852, k=4] [2.0 MB]
Creating vector for state rewards... [1.7 MB]
Creating vector for inf... [1.7 MB]
Allocating iteration vectors... [2 x 1.7 MB]
TOTAL: [17.8 MB]

Starting iterations...

Iterative method: 95 iterations in 0.67 seconds (average 0.003368, setup 0.35)

Prob0A: 60 iterations in 0.34 seconds (average 0.005600, setup 0.00)

Prob1E: 4 iterations in 0.04 seconds (average 0.009000, setup 0.00)

yes = 940, no = 195489, maybe = 31582

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=228011, nc=53211, nnz=148552, k=2] [2.0 MB]
Creating vector for yes... [1.7 MB]
Allocating iteration vectors... [2 x 1.7 MB]
TOTAL: [7.2 MB]

Starting iterations...

Iterative method: 97 iterations in 0.29 seconds (average 0.001402, setup 0.16)

Value in the initial state: 83.32170593037846

Time for model checking: 1.472 seconds.

Result: 83.32170593037846 (value in the initial state)

---------------------------------------------------------------------

Note: There were 16 warnings during computation.

