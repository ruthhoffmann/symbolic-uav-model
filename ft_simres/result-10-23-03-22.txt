PRISM
=====

Version: 4.3
Date: Tue Aug 02 12:36:19 BST 2016
Hostname: eglinton
Memory limits: cudd=1g, java(heap)=981.5m
Command line: prism uav_sim_ft_mdp.prism -s uav_mdp.props -const 'Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=0,Objy1=3,Objx2=2,Objy2=2,'

Parsing model file "uav_sim_ft_mdp.prism"...

Parsing properties file "uav_mdp.props"...

18 properties:
(1) Pmax=? [ F "MissionSuccessful" ]
(2) Pmin=? [ F "MissionSuccessful" ]
(3) Pmax=? [ F "MissionTimeUp" ]
(4) Pmin=? [ F "MissionTimeUp" ]
(5) Pmax=? [ F "Crash" ]
(6) Pmin=? [ F "Crash" ]
(7) Pmax=? [ F "AllAreaSearched" ]
(8) Pmin=? [ F "AllAreaSearched" ]
(9) Pmax=? [ F "MissionTimeUp"|"AllAreaSearched"|"Crash" ]
(10) Pmin=? [ F "MissionTimeUp"|"AllAreaSearched"|"Crash" ]
(11) R{"drop"}max=? [ F "deadlock" ]
(12) R{"drop"}min=? [ F "deadlock" ]
(13) R{"charge"}max=? [ F "deadlock" ]
(14) R{"charge"}min=? [ F "deadlock" ]
(15) R{"time"}max=? [ F "deadlock" ]
(16) R{"time"}min=? [ F "deadlock" ]
(17) R{"success"}max=? [ F "deadlock" ]/Pmin=? [ F "MissionSuccessful" ]
(18) R{"success"}min=? [ F "deadlock" ]/Pmax=? [ F "MissionSuccessful" ]

Type:        MDP
Modules:     Obj Move Robot T 
Variables:   NoOfObjs posx posy s1 retx rety b t c 

---------------------------------------------------------------------

Model checking: Pmax=? [ F "MissionSuccessful" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=0,Objy1=3,Objx2=2,Objy2=2

Building model...
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=0,Objy1=3,Objx2=2,Objy2=2

Warning: Guard for command 7 of module "Move" is never satisfied.

Warning: Guard for command 8 of module "Move" is never satisfied.

Warning: Guard for command 9 of module "Move" is never satisfied.

Warning: Guard for command 6 of module "Robot" is never satisfied.

Warning: Update 2 of command 3 of module "Robot" doesn't do anything ("(s1'=13)", line 249, column 44)

Warning: Update 2 of command 27 of module "T" doesn't do anything ("(c'=1)", line 372, column 108)

Warning: Guard for command 28 of module "T" is never satisfied.

Warning: Update 2 of command 9 of module "T" doesn't do anything ("(c'=1)", line 349, column 112)

Warning: Guard for command 10 of module "T" is never satisfied.

Warning: Update 2 of command 51 of module "T" doesn't do anything ("(c'=1)", line 403, column 113)

Warning: Guard for command 52 of module "T" is never satisfied.

Computing reachable states...

Reachability (BFS): 78 iterations in 0.58 seconds (average 0.007385, setup 0.00)

Time for model construction: 2.076 seconds.

Warning: Deadlocks detected and fixed in 1 states

Type:        MDP
States:      143216 (1 initial)
Transitions: 395515
Choices:     193364

Transition matrix: 175647 nodes (76 terminal), 395515 minterms, vars: 34r/34c/18nd

Prob0A: 55 iterations in 0.37 seconds (average 0.006764, setup 0.00)

Prob1E: 4 iterations in 0.05 seconds (average 0.013000, setup 0.00)

yes = 778, no = 111056, maybe = 31382

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=143216, nc=54692, nnz=152851, k=2] [1.9 MB]
Creating vector for yes... [1.1 MB]
Allocating iteration vectors... [2 x 1.1 MB]
TOTAL: [5.2 MB]

Starting iterations...

Iterative method: 92 iterations in 0.32 seconds (average 0.000957, setup 0.23)

Value in the initial state: 0.7497041664815982

Time for model checking: 0.79 seconds.

Result: 0.7497041664815982 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmin=? [ F "MissionSuccessful" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=0,Objy1=3,Objx2=2,Objy2=2

Prob0E: 71 iterations in 0.52 seconds (average 0.007380, setup 0.00)

Prob1A: 3 iterations in 0.05 seconds (average 0.016000, setup 0.00)

yes = 778, no = 117482, maybe = 24956

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=143216, nc=43033, nnz=119036, k=2] [1.5 MB]
Creating vector for yes... [1.1 MB]
Allocating iteration vectors... [2 x 1.1 MB]
TOTAL: [4.8 MB]

Starting iterations...

Iterative method: 89 iterations in 0.28 seconds (average 0.000944, setup 0.19)

Value in the initial state: 0.002730535552307069

Time for model checking: 0.872 seconds.

Result: 0.002730535552307069 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmax=? [ F "MissionTimeUp" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=0,Objy1=3,Objx2=2,Objy2=2

Prob0A: 67 iterations in 0.39 seconds (average 0.005851, setup 0.00)

Prob1E: 4 iterations in 0.04 seconds (average 0.010000, setup 0.00)

yes = 204, no = 123349, maybe = 19663

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=143216, nc=34412, nnz=93850, k=4] [1.2 MB]
Creating vector for yes... [1.1 MB]
Allocating iteration vectors... [2 x 1.1 MB]
TOTAL: [4.5 MB]

Starting iterations...

Iterative method: 85 iterations in 0.23 seconds (average 0.000894, setup 0.15)

Value in the initial state: 8.935792684162083E-6

Time for model checking: 0.785 seconds.

Result: 8.935792684162083E-6 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmin=? [ F "MissionTimeUp" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=0,Objy1=3,Objx2=2,Objy2=2

Prob0E: 74 iterations in 0.50 seconds (average 0.006703, setup 0.00)

Prob1A: 3 iterations in 0.04 seconds (average 0.013333, setup 0.00)

yes = 204, no = 123563, maybe = 19449

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=143216, nc=34036, nnz=92964, k=4] [1.2 MB]
Creating vector for yes... [1.1 MB]
Allocating iteration vectors... [2 x 1.1 MB]
TOTAL: [4.5 MB]

Starting iterations...

Iterative method: 102 iterations in 0.22 seconds (average 0.000902, setup 0.12)

Value in the initial state: 1.437433640146354E-8

Time for model checking: 0.855 seconds.

Result: 1.437433640146354E-8 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmax=? [ F "Crash" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=0,Objy1=3,Objx2=2,Objy2=2

Prob0A: 3 iterations in 0.10 seconds (average 0.034667, setup 0.00)

Prob1E: 196 iterations in 2.29 seconds (average 0.011694, setup 0.00)

yes = 70072, no = 1203, maybe = 71941

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=143216, nc=122089, nnz=324240, k=4] [4.0 MB]
Creating vector for yes... [1.1 MB]
Allocating iteration vectors... [2 x 1.1 MB]
TOTAL: [7.2 MB]

Starting iterations...

Iterative method: 87 iterations in 0.40 seconds (average 0.001425, setup 0.28)

Value in the initial state: 0.01958947769749737

Time for model checking: 3.272 seconds.

Result: 0.01958947769749737 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmin=? [ F "Crash" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=0,Objy1=3,Objx2=2,Objy2=2

Prob0E: 3 iterations in 0.07 seconds (average 0.022667, setup 0.00)

Prob1A: 49 iterations in 0.49 seconds (average 0.010041, setup 0.00)

yes = 70072, no = 1203, maybe = 71941

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=143216, nc=122089, nnz=324240, k=4] [4.0 MB]
Creating vector for yes... [1.1 MB]
Allocating iteration vectors... [2 x 1.1 MB]
TOTAL: [7.2 MB]

Starting iterations...

Iterative method: 87 iterations in 0.38 seconds (average 0.001333, setup 0.26)

Value in the initial state: 0.016531410470426226

Time for model checking: 1.175 seconds.

Result: 0.016531410470426226 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmax=? [ F "AllAreaSearched" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=0,Objy1=3,Objx2=2,Objy2=2

Prob0A: 49 iterations in 0.50 seconds (average 0.010204, setup 0.00)

Prob1E: 4 iterations in 0.04 seconds (average 0.010000, setup 0.00)

yes = 220, no = 85629, maybe = 57367

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=143216, nc=96468, nnz=263953, k=2] [3.2 MB]
Creating vector for yes... [1.1 MB]
Allocating iteration vectors... [2 x 1.1 MB]
TOTAL: [6.5 MB]

Starting iterations...

Iterative method: 88 iterations in 0.32 seconds (average 0.001273, setup 0.20)

Value in the initial state: 0.9796136913217637

Time for model checking: 0.938 seconds.

Result: 0.9796136913217637 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmin=? [ F "AllAreaSearched" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=0,Objy1=3,Objx2=2,Objy2=2

Prob0E: 53 iterations in 0.38 seconds (average 0.007094, setup 0.00)

Prob1A: 3 iterations in 0.03 seconds (average 0.009333, setup 0.00)

yes = 220, no = 86915, maybe = 56081

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=143216, nc=94065, nnz=256889, k=2] [3.2 MB]
Creating vector for yes... [1.1 MB]
Allocating iteration vectors... [2 x 1.1 MB]
TOTAL: [6.4 MB]

Starting iterations...

Iterative method: 94 iterations in 0.30 seconds (average 0.001277, setup 0.18)

Value in the initial state: 0.23200174737178142

Time for model checking: 0.718 seconds.

Result: 0.23200174737178142 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmax=? [ F "MissionTimeUp"|"AllAreaSearched"|"Crash" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=0,Objy1=3,Objx2=2,Objy2=2

Prob0A: 3 iterations in 0.07 seconds (average 0.024000, setup 0.00)

Prob1E: 276 iterations in 3.14 seconds (average 0.011391, setup 0.00)

yes = 117481, no = 779, maybe = 24956

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=143216, nc=43033, nnz=119036, k=2] [1.5 MB]
Creating vector for yes... [1.1 MB]
Allocating iteration vectors... [2 x 1.1 MB]
TOTAL: [4.8 MB]

Starting iterations...

Iterative method: 80 iterations in 0.26 seconds (average 0.000950, setup 0.18)

Value in the initial state: 0.9972681223397226

Time for model checking: 3.888 seconds.

Result: 0.9972681223397226 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmin=? [ F "MissionTimeUp"|"AllAreaSearched"|"Crash" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=0,Objy1=3,Objx2=2,Objy2=2

Prob0E: 3 iterations in 0.05 seconds (average 0.017333, setup 0.00)

Prob1A: 55 iterations in 0.42 seconds (average 0.007636, setup 0.00)

yes = 111055, no = 779, maybe = 31382

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=143216, nc=54692, nnz=152851, k=2] [1.9 MB]
Creating vector for yes... [1.1 MB]
Allocating iteration vectors... [2 x 1.1 MB]
TOTAL: [5.2 MB]

Starting iterations...

Iterative method: 85 iterations in 0.23 seconds (average 0.001035, setup 0.14)

Value in the initial state: 0.2502951589209388

Time for model checking: 0.748 seconds.

Result: 0.2502951589209388 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"drop"}max=? [ F "deadlock" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=0,Objy1=3,Objx2=2,Objy2=2

Prob0E: 4 iterations in 0.05 seconds (average 0.013000, setup 0.00)

Prob1A: 1 iterations in 0.01 seconds (average 0.008000, setup 0.00)

goal = 1, inf = 0, maybe = 143215

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=143216, nc=193363, nnz=395514, k=4] [5.8 MB]
Building sparse matrix (transition rewards)... [n=143216, nc=193363, nnz=0, k=4] [1.3 MB]
Creating vector for state rewards... [1.1 MB]
Creating vector for inf... [1.1 MB]
Allocating iteration vectors... [2 x 1.1 MB]
TOTAL: [11.5 MB]

Starting iterations...

Iterative method: 88 iterations in 0.50 seconds (average 0.002136, setup 0.32)

Value in the initial state: 0.13259987847993093

Time for model checking: 0.574 seconds.

Result: 0.13259987847993093 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"drop"}min=? [ F "deadlock" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=0,Objy1=3,Objx2=2,Objy2=2

Prob0A: 4 iterations in 0.03 seconds (average 0.008000, setup 0.00)

Prob1E: 5 iterations in 0.04 seconds (average 0.008800, setup 0.00)

Warning: PRISM hasn't checked for zero-reward loops.
Your minimum rewards may be too low...

goal = 1, inf = 0, maybe = 143215

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=143216, nc=193363, nnz=395514, k=4] [5.8 MB]
Building sparse matrix (transition rewards)... [n=143216, nc=193363, nnz=0, k=4] [1.3 MB]
Creating vector for state rewards... [1.1 MB]
Creating vector for inf... [1.1 MB]
Allocating iteration vectors... [2 x 1.1 MB]
TOTAL: [11.5 MB]

Starting iterations...

Iterative method: 83 iterations in 0.40 seconds (average 0.002120, setup 0.22)

Value in the initial state: 0.11631889514125951

Time for model checking: 0.49 seconds.

Result: 0.11631889514125951 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"charge"}max=? [ F "deadlock" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=0,Objy1=3,Objx2=2,Objy2=2

Prob0E: 4 iterations in 0.02 seconds (average 0.006000, setup 0.00)

Prob1A: 1 iterations in 0.00 seconds (average 0.004000, setup 0.00)

goal = 1, inf = 0, maybe = 143215

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=143216, nc=193363, nnz=395514, k=4] [5.8 MB]
Building sparse matrix (transition rewards)... [n=143216, nc=193363, nnz=5004, k=4] [1.3 MB]
Creating vector for state rewards... [1.1 MB]
Creating vector for inf... [1.1 MB]
Allocating iteration vectors... [2 x 1.1 MB]
TOTAL: [11.5 MB]

Starting iterations...

Iterative method: 86 iterations in 0.40 seconds (average 0.002093, setup 0.22)

Value in the initial state: 0.9828252826290581

Time for model checking: 0.448 seconds.

Result: 0.9828252826290581 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"charge"}min=? [ F "deadlock" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=0,Objy1=3,Objx2=2,Objy2=2

Prob0A: 4 iterations in 0.02 seconds (average 0.006000, setup 0.00)

Prob1E: 5 iterations in 0.04 seconds (average 0.008000, setup 0.00)

Warning: PRISM hasn't checked for zero-reward loops.
Your minimum rewards may be too low...

goal = 1, inf = 0, maybe = 143215

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=143216, nc=193363, nnz=395514, k=4] [5.8 MB]
Building sparse matrix (transition rewards)... [n=143216, nc=193363, nnz=5004, k=4] [1.3 MB]
Creating vector for state rewards... [1.1 MB]
Creating vector for inf... [1.1 MB]
Allocating iteration vectors... [2 x 1.1 MB]
TOTAL: [11.5 MB]

Starting iterations...

Iterative method: 92 iterations in 0.42 seconds (average 0.002130, setup 0.23)

Value in the initial state: 0.21212681700248734

Time for model checking: 0.5 seconds.

Result: 0.21212681700248734 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"time"}max=? [ F "deadlock" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=0,Objy1=3,Objx2=2,Objy2=2

Prob0E: 4 iterations in 0.02 seconds (average 0.006000, setup 0.00)

Prob1A: 1 iterations in 0.00 seconds (average 0.004000, setup 0.00)

goal = 1, inf = 0, maybe = 143215

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=143216, nc=193363, nnz=395514, k=4] [5.8 MB]
Building sparse matrix (transition rewards)... [n=143216, nc=193363, nnz=71273, k=4] [2.1 MB]
Creating vector for state rewards... [1.1 MB]
Creating vector for inf... [1.1 MB]
Allocating iteration vectors... [2 x 1.1 MB]
TOTAL: [12.3 MB]

Starting iterations...

Iterative method: 89 iterations in 0.44 seconds (average 0.002157, setup 0.25)

Value in the initial state: 108.70282329279549

Time for model checking: 0.485 seconds.

Result: 108.70282329279549 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"time"}min=? [ F "deadlock" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=0,Objy1=3,Objx2=2,Objy2=2

Prob0A: 4 iterations in 0.02 seconds (average 0.006000, setup 0.00)

Prob1E: 5 iterations in 0.05 seconds (average 0.009600, setup 0.00)

Warning: PRISM hasn't checked for zero-reward loops.
Your minimum rewards may be too low...

goal = 1, inf = 0, maybe = 143215

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=143216, nc=193363, nnz=395514, k=4] [5.8 MB]
Building sparse matrix (transition rewards)... [n=143216, nc=193363, nnz=71273, k=4] [2.1 MB]
Creating vector for state rewards... [1.1 MB]
Creating vector for inf... [1.1 MB]
Allocating iteration vectors... [2 x 1.1 MB]
TOTAL: [12.3 MB]

Starting iterations...

Iterative method: 98 iterations in 0.46 seconds (average 0.002204, setup 0.24)

Value in the initial state: 91.79886133114682

Time for model checking: 0.546 seconds.

Result: 91.79886133114682 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"success"}max=? [ F "deadlock" ]/Pmin=? [ F "MissionSuccessful" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=0,Objy1=3,Objx2=2,Objy2=2

Prob0E: 4 iterations in 0.03 seconds (average 0.007000, setup 0.00)

Prob1A: 1 iterations in 0.00 seconds (average 0.004000, setup 0.00)

goal = 1, inf = 0, maybe = 143215

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=143216, nc=193363, nnz=395514, k=4] [5.8 MB]
Building sparse matrix (transition rewards)... [n=143216, nc=193363, nnz=1539, k=4] [1.3 MB]
Creating vector for state rewards... [1.1 MB]
Creating vector for inf... [1.1 MB]
Allocating iteration vectors... [2 x 1.1 MB]
TOTAL: [11.5 MB]

Starting iterations...

Iterative method: 94 iterations in 0.41 seconds (average 0.002085, setup 0.22)

Prob0E: 71 iterations in 0.48 seconds (average 0.006761, setup 0.00)

Prob1A: 3 iterations in 0.04 seconds (average 0.014667, setup 0.00)

yes = 778, no = 117482, maybe = 24956

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=143216, nc=43033, nnz=119036, k=2] [1.5 MB]
Creating vector for yes... [1.1 MB]
Allocating iteration vectors... [2 x 1.1 MB]
TOTAL: [4.8 MB]

Starting iterations...

Iterative method: 89 iterations in 0.18 seconds (average 0.000989, setup 0.10)

Value in the initial state: 30606.67220374402

Time for model checking: 1.209 seconds.

Result: 30606.67220374402 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"success"}min=? [ F "deadlock" ]/Pmax=? [ F "MissionSuccessful" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=0,Objy1=3,Objx2=2,Objy2=2

Prob0A: 4 iterations in 0.04 seconds (average 0.010000, setup 0.00)

Prob1E: 5 iterations in 0.05 seconds (average 0.009600, setup 0.00)

Warning: PRISM hasn't checked for zero-reward loops.
Your minimum rewards may be too low...

goal = 1, inf = 0, maybe = 143215

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=143216, nc=193363, nnz=395514, k=4] [5.8 MB]
Building sparse matrix (transition rewards)... [n=143216, nc=193363, nnz=1539, k=4] [1.3 MB]
Creating vector for state rewards... [1.1 MB]
Creating vector for inf... [1.1 MB]
Allocating iteration vectors... [2 x 1.1 MB]
TOTAL: [11.5 MB]

Starting iterations...

Iterative method: 96 iterations in 0.44 seconds (average 0.002167, setup 0.23)

Prob0A: 55 iterations in 0.34 seconds (average 0.006255, setup 0.00)

Prob1E: 4 iterations in 0.02 seconds (average 0.005000, setup 0.00)

yes = 778, no = 111056, maybe = 31382

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=143216, nc=54692, nnz=152851, k=2] [1.9 MB]
Creating vector for yes... [1.1 MB]
Allocating iteration vectors... [2 x 1.1 MB]
TOTAL: [5.2 MB]

Starting iterations...

Iterative method: 92 iterations in 0.22 seconds (average 0.001043, setup 0.12)

Value in the initial state: 1.2769052854568608

Time for model checking: 1.132 seconds.

Result: 1.2769052854568608 (value in the initial state)

---------------------------------------------------------------------

Note: There were 16 warnings during computation.

