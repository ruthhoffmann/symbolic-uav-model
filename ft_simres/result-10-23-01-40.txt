PRISM
=====

Version: 4.3
Date: Tue Aug 02 12:35:17 BST 2016
Hostname: eglinton
Memory limits: cudd=1g, java(heap)=981.5m
Command line: prism uav_sim_ft_mdp.prism -s uav_mdp.props -const 'Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=0,Objy1=1,Objx2=4,Objy2=0,'

Parsing model file "uav_sim_ft_mdp.prism"...

Parsing properties file "uav_mdp.props"...

18 properties:
(1) Pmax=? [ F "MissionSuccessful" ]
(2) Pmin=? [ F "MissionSuccessful" ]
(3) Pmax=? [ F "MissionTimeUp" ]
(4) Pmin=? [ F "MissionTimeUp" ]
(5) Pmax=? [ F "Crash" ]
(6) Pmin=? [ F "Crash" ]
(7) Pmax=? [ F "AllAreaSearched" ]
(8) Pmin=? [ F "AllAreaSearched" ]
(9) Pmax=? [ F "MissionTimeUp"|"AllAreaSearched"|"Crash" ]
(10) Pmin=? [ F "MissionTimeUp"|"AllAreaSearched"|"Crash" ]
(11) R{"drop"}max=? [ F "deadlock" ]
(12) R{"drop"}min=? [ F "deadlock" ]
(13) R{"charge"}max=? [ F "deadlock" ]
(14) R{"charge"}min=? [ F "deadlock" ]
(15) R{"time"}max=? [ F "deadlock" ]
(16) R{"time"}min=? [ F "deadlock" ]
(17) R{"success"}max=? [ F "deadlock" ]/Pmin=? [ F "MissionSuccessful" ]
(18) R{"success"}min=? [ F "deadlock" ]/Pmax=? [ F "MissionSuccessful" ]

Type:        MDP
Modules:     Obj Move Robot T 
Variables:   NoOfObjs posx posy s1 retx rety b t c 

---------------------------------------------------------------------

Model checking: Pmax=? [ F "MissionSuccessful" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=0,Objy1=1,Objx2=4,Objy2=0

Building model...
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=0,Objy1=1,Objx2=4,Objy2=0

Warning: Guard for command 4 of module "Move" is never satisfied.

Warning: Guard for command 7 of module "Move" is never satisfied.

Warning: Guard for command 8 of module "Move" is never satisfied.

Warning: Guard for command 9 of module "Move" is never satisfied.

Warning: Guard for command 7 of module "Robot" is never satisfied.

Warning: Update 2 of command 3 of module "Robot" doesn't do anything ("(s1'=13)", line 249, column 44)

Warning: Update 2 of command 27 of module "T" doesn't do anything ("(c'=1)", line 372, column 108)

Warning: Guard for command 28 of module "T" is never satisfied.

Warning: Update 2 of command 9 of module "T" doesn't do anything ("(c'=1)", line 349, column 112)

Warning: Guard for command 10 of module "T" is never satisfied.

Warning: Update 2 of command 51 of module "T" doesn't do anything ("(c'=1)", line 403, column 113)

Warning: Guard for command 52 of module "T" is never satisfied.

Computing reachable states...

Reachability (BFS): 75 iterations in 0.98 seconds (average 0.013120, setup 0.00)

Time for model construction: 3.183 seconds.

Warning: Deadlocks detected and fixed in 1 states

Type:        MDP
States:      225689 (1 initial)
Transitions: 592232
Choices:     296315

Transition matrix: 176251 nodes (76 terminal), 592232 minterms, vars: 34r/34c/18nd

Prob0A: 48 iterations in 0.40 seconds (average 0.008250, setup 0.00)

Prob1E: 4 iterations in 0.07 seconds (average 0.017000, setup 0.00)

yes = 950, no = 191607, maybe = 33132

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=225689, nc=57915, nnz=165644, k=2] [2.2 MB]
Creating vector for yes... [1.7 MB]
Allocating iteration vectors... [2 x 1.7 MB]
TOTAL: [7.3 MB]

Starting iterations...

Iterative method: 88 iterations in 0.40 seconds (average 0.001591, setup 0.26)

Value in the initial state: 0.9805942652631188

Time for model checking: 1.253 seconds.

Result: 0.9805942652631188 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmin=? [ F "MissionSuccessful" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=0,Objy1=1,Objx2=4,Objy2=0

Prob0E: 50 iterations in 0.49 seconds (average 0.009840, setup 0.00)

Prob1A: 3 iterations in 0.06 seconds (average 0.020000, setup 0.00)

yes = 950, no = 196913, maybe = 27826

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=225689, nc=48483, nnz=138021, k=2] [1.8 MB]
Creating vector for yes... [1.7 MB]
Allocating iteration vectors... [2 x 1.7 MB]
TOTAL: [7.0 MB]

Starting iterations...

Iterative method: 81 iterations in 0.32 seconds (average 0.001531, setup 0.20)

Value in the initial state: 0.7413459728350856

Time for model checking: 1.239 seconds.

Result: 0.7413459728350856 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmax=? [ F "MissionTimeUp" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=0,Objy1=1,Objx2=4,Objy2=0

Prob0A: 57 iterations in 0.57 seconds (average 0.010035, setup 0.00)

Prob1E: 4 iterations in 0.05 seconds (average 0.012000, setup 0.00)

yes = 476, no = 138509, maybe = 86704

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=225689, nc=147513, nnz=406070, k=4] [5.0 MB]
Creating vector for yes... [1.7 MB]
Allocating iteration vectors... [2 x 1.7 MB]
TOTAL: [10.2 MB]

Starting iterations...

Iterative method: 90 iterations in 0.46 seconds (average 0.002000, setup 0.28)

Value in the initial state: 8.532099078680474E-4

Time for model checking: 1.401 seconds.

Result: 8.532099078680474E-4 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmin=? [ F "MissionTimeUp" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=0,Objy1=1,Objx2=4,Objy2=0

Prob0E: 65 iterations in 0.72 seconds (average 0.011077, setup 0.00)

Prob1A: 3 iterations in 0.06 seconds (average 0.018667, setup 0.00)

yes = 476, no = 138866, maybe = 86347

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=225689, nc=146875, nnz=404761, k=4] [5.0 MB]
Creating vector for yes... [1.7 MB]
Allocating iteration vectors... [2 x 1.7 MB]
TOTAL: [10.2 MB]

Starting iterations...

Iterative method: 105 iterations in 0.50 seconds (average 0.001981, setup 0.30)

Value in the initial state: 2.129030155282166E-6

Time for model checking: 1.891 seconds.

Result: 2.129030155282166E-6 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmax=? [ F "Crash" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=0,Objy1=1,Objx2=4,Objy2=0

Prob0A: 3 iterations in 0.10 seconds (average 0.032000, setup 0.00)

Prob1E: 188 iterations in 2.84 seconds (average 0.015128, setup 0.00)

yes = 110621, no = 2648, maybe = 112420

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=225689, nc=183046, nnz=478963, k=4] [5.9 MB]
Creating vector for yes... [1.7 MB]
Allocating iteration vectors... [2 x 1.7 MB]
TOTAL: [11.0 MB]

Starting iterations...

Iterative method: 85 iterations in 0.53 seconds (average 0.002165, setup 0.34)

Value in the initial state: 0.03117241554861006

Time for model checking: 4.395 seconds.

Result: 0.03117241554861006 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmin=? [ F "Crash" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=0,Objy1=1,Objx2=4,Objy2=0

Prob0E: 3 iterations in 0.07 seconds (average 0.022667, setup 0.00)

Prob1A: 45 iterations in 0.58 seconds (average 0.012889, setup 0.00)

yes = 110621, no = 2648, maybe = 112420

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=225689, nc=183046, nnz=478963, k=4] [5.9 MB]
Creating vector for yes... [1.7 MB]
Allocating iteration vectors... [2 x 1.7 MB]
TOTAL: [11.0 MB]

Starting iterations...

Iterative method: 84 iterations in 0.46 seconds (average 0.002238, setup 0.27)

Value in the initial state: 0.01728942784228288

Time for model checking: 1.83 seconds.

Result: 0.01728942784228288 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmax=? [ F "AllAreaSearched" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=0,Objy1=1,Objx2=4,Objy2=0

Prob0A: 59 iterations in 0.60 seconds (average 0.010169, setup 0.00)

Prob1E: 4 iterations in 0.04 seconds (average 0.011000, setup 0.00)

yes = 1221, no = 178742, maybe = 45726

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=225689, nc=68828, nnz=178401, k=2] [2.3 MB]
Creating vector for yes... [1.7 MB]
Allocating iteration vectors... [2 x 1.7 MB]
TOTAL: [7.5 MB]

Starting iterations...

Iterative method: 87 iterations in 0.32 seconds (average 0.001655, setup 0.17)

Value in the initial state: 0.22992910138013703

Time for model checking: 1.213 seconds.

Result: 0.22992910138013703 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmin=? [ F "AllAreaSearched" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=0,Objy1=1,Objx2=4,Objy2=0

Prob0E: 63 iterations in 0.38 seconds (average 0.005968, setup 0.00)

Prob1A: 3 iterations in 0.04 seconds (average 0.012000, setup 0.00)

yes = 1221, no = 184663, maybe = 39805

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=225689, nc=59021, nnz=151118, k=2] [2.0 MB]
Creating vector for yes... [1.7 MB]
Allocating iteration vectors... [2 x 1.7 MB]
TOTAL: [7.2 MB]

Starting iterations...

Iterative method: 93 iterations in 0.26 seconds (average 0.001591, setup 0.12)

Value in the initial state: 0.0017475951743421512

Time for model checking: 1.173 seconds.

Result: 0.0017475951743421512 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmax=? [ F "MissionTimeUp"|"AllAreaSearched"|"Crash" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=0,Objy1=1,Objx2=4,Objy2=0

Prob0A: 3 iterations in 0.09 seconds (average 0.029333, setup 0.00)

Prob1E: 196 iterations in 2.56 seconds (average 0.013061, setup 0.00)

yes = 196912, no = 951, maybe = 27826

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=225689, nc=48483, nnz=138021, k=2] [1.8 MB]
Creating vector for yes... [1.7 MB]
Allocating iteration vectors... [2 x 1.7 MB]
TOTAL: [7.0 MB]

Starting iterations...

Iterative method: 75 iterations in 0.32 seconds (average 0.001440, setup 0.21)

Value in the initial state: 0.25865374655576734

Time for model checking: 3.92 seconds.

Result: 0.25865374655576734 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmin=? [ F "MissionTimeUp"|"AllAreaSearched"|"Crash" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=0,Objy1=1,Objx2=4,Objy2=0

Prob0E: 3 iterations in 0.05 seconds (average 0.017333, setup 0.00)

Prob1A: 48 iterations in 0.52 seconds (average 0.010750, setup 0.00)

yes = 191606, no = 951, maybe = 33132

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=225689, nc=57915, nnz=165644, k=2] [2.2 MB]
Creating vector for yes... [1.7 MB]
Allocating iteration vectors... [2 x 1.7 MB]
TOTAL: [7.3 MB]

Starting iterations...

Iterative method: 87 iterations in 0.30 seconds (average 0.001563, setup 0.17)

Value in the initial state: 0.019404733520469654

Time for model checking: 0.952 seconds.

Result: 0.019404733520469654 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"drop"}max=? [ F "deadlock" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=0,Objy1=1,Objx2=4,Objy2=0

Prob0E: 4 iterations in 0.09 seconds (average 0.022000, setup 0.00)

Prob1A: 1 iterations in 0.01 seconds (average 0.008000, setup 0.00)

goal = 1, inf = 0, maybe = 225688

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=225689, nc=296314, nnz=592231, k=4] [8.8 MB]
Building sparse matrix (transition rewards)... [n=225689, nc=296314, nnz=0, k=4] [2.0 MB]
Creating vector for state rewards... [1.7 MB]
Creating vector for inf... [1.7 MB]
Allocating iteration vectors... [2 x 1.7 MB]
TOTAL: [17.6 MB]

Starting iterations...

Iterative method: 85 iterations in 0.75 seconds (average 0.003247, setup 0.48)

Value in the initial state: 0.21136582217041713

Time for model checking: 0.869 seconds.

Result: 0.21136582217041713 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"drop"}min=? [ F "deadlock" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=0,Objy1=1,Objx2=4,Objy2=0

Prob0A: 4 iterations in 0.05 seconds (average 0.012000, setup 0.00)

Prob1E: 5 iterations in 0.07 seconds (average 0.013600, setup 0.00)

Warning: PRISM hasn't checked for zero-reward loops.
Your minimum rewards may be too low...

goal = 1, inf = 0, maybe = 225688

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=225689, nc=296314, nnz=592231, k=4] [8.8 MB]
Building sparse matrix (transition rewards)... [n=225689, nc=296314, nnz=0, k=4] [2.0 MB]
Creating vector for state rewards... [1.7 MB]
Creating vector for inf... [1.7 MB]
Allocating iteration vectors... [2 x 1.7 MB]
TOTAL: [17.6 MB]

Starting iterations...

Iterative method: 75 iterations in 0.63 seconds (average 0.003360, setup 0.38)

Value in the initial state: 0.1843952092610228

Time for model checking: 0.764 seconds.

Result: 0.1843952092610228 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"charge"}max=? [ F "deadlock" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=0,Objy1=1,Objx2=4,Objy2=0

Prob0E: 4 iterations in 0.04 seconds (average 0.010000, setup 0.00)

Prob1A: 1 iterations in 0.00 seconds (average 0.004000, setup 0.00)

goal = 1, inf = 0, maybe = 225688

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=225689, nc=296314, nnz=592231, k=4] [8.8 MB]
Building sparse matrix (transition rewards)... [n=225689, nc=296314, nnz=6458, k=4] [2.1 MB]
Creating vector for state rewards... [1.7 MB]
Creating vector for inf... [1.7 MB]
Allocating iteration vectors... [2 x 1.7 MB]
TOTAL: [17.7 MB]

Starting iterations...

Iterative method: 81 iterations in 0.62 seconds (average 0.003309, setup 0.35)

Value in the initial state: 0.29296140887883565

Time for model checking: 0.677 seconds.

Result: 0.29296140887883565 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"charge"}min=? [ F "deadlock" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=0,Objy1=1,Objx2=4,Objy2=0

Prob0A: 4 iterations in 0.03 seconds (average 0.008000, setup 0.00)

Prob1E: 5 iterations in 0.06 seconds (average 0.012800, setup 0.00)

Warning: PRISM hasn't checked for zero-reward loops.
Your minimum rewards may be too low...

goal = 1, inf = 0, maybe = 225688

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=225689, nc=296314, nnz=592231, k=4] [8.8 MB]
Building sparse matrix (transition rewards)... [n=225689, nc=296314, nnz=6458, k=4] [2.1 MB]
Creating vector for state rewards... [1.7 MB]
Creating vector for inf... [1.7 MB]
Allocating iteration vectors... [2 x 1.7 MB]
TOTAL: [17.7 MB]

Starting iterations...

Iterative method: 88 iterations in 0.62 seconds (average 0.003318, setup 0.33)

Value in the initial state: 0.052306032793518704

Time for model checking: 0.739 seconds.

Result: 0.052306032793518704 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"time"}max=? [ F "deadlock" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=0,Objy1=1,Objx2=4,Objy2=0

Prob0E: 4 iterations in 0.03 seconds (average 0.008000, setup 0.00)

Prob1A: 1 iterations in 0.01 seconds (average 0.008000, setup 0.00)

goal = 1, inf = 0, maybe = 225688

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=225689, nc=296314, nnz=592231, k=4] [8.8 MB]
Building sparse matrix (transition rewards)... [n=225689, nc=296314, nnz=113267, k=4] [3.3 MB]
Creating vector for state rewards... [1.7 MB]
Creating vector for inf... [1.7 MB]
Allocating iteration vectors... [2 x 1.7 MB]
TOTAL: [18.9 MB]

Starting iterations...

Iterative method: 88 iterations in 0.66 seconds (average 0.003364, setup 0.36)

Value in the initial state: 171.91498816063216

Time for model checking: 0.732 seconds.

Result: 171.91498816063216 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"time"}min=? [ F "deadlock" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=0,Objy1=1,Objx2=4,Objy2=0

Prob0A: 4 iterations in 0.03 seconds (average 0.007000, setup 0.00)

Prob1E: 5 iterations in 0.06 seconds (average 0.012000, setup 0.00)

Warning: PRISM hasn't checked for zero-reward loops.
Your minimum rewards may be too low...

goal = 1, inf = 0, maybe = 225688

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=225689, nc=296314, nnz=592231, k=4] [8.8 MB]
Building sparse matrix (transition rewards)... [n=225689, nc=296314, nnz=113267, k=4] [3.3 MB]
Creating vector for state rewards... [1.7 MB]
Creating vector for inf... [1.7 MB]
Allocating iteration vectors... [2 x 1.7 MB]
TOTAL: [18.9 MB]

Starting iterations...

Iterative method: 94 iterations in 0.80 seconds (average 0.003447, setup 0.48)

Value in the initial state: 95.79283141136285

Time for model checking: 0.925 seconds.

Result: 95.79283141136285 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"success"}max=? [ F "deadlock" ]/Pmin=? [ F "MissionSuccessful" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=0,Objy1=1,Objx2=4,Objy2=0

Prob0E: 4 iterations in 0.03 seconds (average 0.008000, setup 0.00)

Prob1A: 1 iterations in 0.00 seconds (average 0.004000, setup 0.00)

goal = 1, inf = 0, maybe = 225688

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=225689, nc=296314, nnz=592231, k=4] [8.8 MB]
Building sparse matrix (transition rewards)... [n=225689, nc=296314, nnz=1872, k=4] [2.0 MB]
Creating vector for state rewards... [1.7 MB]
Creating vector for inf... [1.7 MB]
Allocating iteration vectors... [2 x 1.7 MB]
TOTAL: [17.7 MB]

Starting iterations...

Iterative method: 91 iterations in 0.60 seconds (average 0.003209, setup 0.31)

Prob0E: 50 iterations in 0.52 seconds (average 0.010400, setup 0.00)

Prob1A: 3 iterations in 0.05 seconds (average 0.017333, setup 0.00)

yes = 950, no = 196913, maybe = 27826

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=225689, nc=48483, nnz=138021, k=2] [1.8 MB]
Creating vector for yes... [1.7 MB]
Allocating iteration vectors... [2 x 1.7 MB]
TOTAL: [7.0 MB]

Starting iterations...

Iterative method: 81 iterations in 0.28 seconds (average 0.001432, setup 0.17)

Value in the initial state: 227.18064370935357

Time for model checking: 1.548 seconds.

Result: 227.18064370935357 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"success"}min=? [ F "deadlock" ]/Pmax=? [ F "MissionSuccessful" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=0,Objy1=1,Objx2=4,Objy2=0

Prob0A: 4 iterations in 0.10 seconds (average 0.024000, setup 0.00)

Prob1E: 5 iterations in 0.04 seconds (average 0.008800, setup 0.00)

Warning: PRISM hasn't checked for zero-reward loops.
Your minimum rewards may be too low...

goal = 1, inf = 0, maybe = 225688

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=225689, nc=296314, nnz=592231, k=4] [8.8 MB]
Building sparse matrix (transition rewards)... [n=225689, nc=296314, nnz=1872, k=4] [2.0 MB]
Creating vector for state rewards... [1.7 MB]
Creating vector for inf... [1.7 MB]
Allocating iteration vectors... [2 x 1.7 MB]
TOTAL: [17.7 MB]

Starting iterations...

Iterative method: 86 iterations in 0.81 seconds (average 0.004186, setup 0.45)

Prob0A: 48 iterations in 0.22 seconds (average 0.004500, setup 0.00)

Prob1E: 4 iterations in 0.04 seconds (average 0.009000, setup 0.00)

yes = 950, no = 191607, maybe = 33132

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=225689, nc=57915, nnz=165644, k=2] [2.2 MB]
Creating vector for yes... [1.7 MB]
Allocating iteration vectors... [2 x 1.7 MB]
TOTAL: [7.3 MB]

Starting iterations...

Iterative method: 88 iterations in 0.28 seconds (average 0.001545, setup 0.15)

Value in the initial state: 79.12074319633929

Time for model checking: 1.513 seconds.

Result: 79.12074319633929 (value in the initial state)

---------------------------------------------------------------------

Note: There were 17 warnings during computation.

