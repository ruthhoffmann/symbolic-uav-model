PRISM
=====

Version: 4.3
Date: Tue Aug 02 12:41:56 BST 2016
Hostname: eglinton
Memory limits: cudd=1g, java(heap)=981.5m
Command line: prism uav_sim_ft_mdp.prism -s uav_mdp.props -const 'Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=4,Objy1=1,Objx2=4,Objy2=3,'

Parsing model file "uav_sim_ft_mdp.prism"...

Parsing properties file "uav_mdp.props"...

18 properties:
(1) Pmax=? [ F "MissionSuccessful" ]
(2) Pmin=? [ F "MissionSuccessful" ]
(3) Pmax=? [ F "MissionTimeUp" ]
(4) Pmin=? [ F "MissionTimeUp" ]
(5) Pmax=? [ F "Crash" ]
(6) Pmin=? [ F "Crash" ]
(7) Pmax=? [ F "AllAreaSearched" ]
(8) Pmin=? [ F "AllAreaSearched" ]
(9) Pmax=? [ F "MissionTimeUp"|"AllAreaSearched"|"Crash" ]
(10) Pmin=? [ F "MissionTimeUp"|"AllAreaSearched"|"Crash" ]
(11) R{"drop"}max=? [ F "deadlock" ]
(12) R{"drop"}min=? [ F "deadlock" ]
(13) R{"charge"}max=? [ F "deadlock" ]
(14) R{"charge"}min=? [ F "deadlock" ]
(15) R{"time"}max=? [ F "deadlock" ]
(16) R{"time"}min=? [ F "deadlock" ]
(17) R{"success"}max=? [ F "deadlock" ]/Pmin=? [ F "MissionSuccessful" ]
(18) R{"success"}min=? [ F "deadlock" ]/Pmax=? [ F "MissionSuccessful" ]

Type:        MDP
Modules:     Obj Move Robot T 
Variables:   NoOfObjs posx posy s1 retx rety b t c 

---------------------------------------------------------------------

Model checking: Pmax=? [ F "MissionSuccessful" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=4,Objy1=1,Objx2=4,Objy2=3

Building model...
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=4,Objy1=1,Objx2=4,Objy2=3

Warning: Guard for command 7 of module "Move" is never satisfied.

Warning: Guard for command 8 of module "Move" is never satisfied.

Warning: Guard for command 9 of module "Move" is never satisfied.

Warning: Guard for command 7 of module "Robot" is never satisfied.

Warning: Update 2 of command 3 of module "Robot" doesn't do anything ("(s1'=13)", line 249, column 44)

Warning: Update 2 of command 27 of module "T" doesn't do anything ("(c'=1)", line 372, column 108)

Warning: Guard for command 28 of module "T" is never satisfied.

Warning: Update 2 of command 9 of module "T" doesn't do anything ("(c'=1)", line 349, column 112)

Warning: Guard for command 10 of module "T" is never satisfied.

Warning: Update 2 of command 51 of module "T" doesn't do anything ("(c'=1)", line 403, column 113)

Warning: Guard for command 52 of module "T" is never satisfied.

Computing reachable states...

Reachability (BFS): 80 iterations in 0.80 seconds (average 0.010000, setup 0.00)

Time for model construction: 1.807 seconds.

Warning: Deadlocks detected and fixed in 1 states

Type:        MDP
States:      196831 (1 initial)
Transitions: 520690
Choices:     260253

Transition matrix: 168805 nodes (76 terminal), 520690 minterms, vars: 34r/34c/18nd

Prob0A: 52 iterations in 0.37 seconds (average 0.007154, setup 0.00)

Prob1E: 4 iterations in 0.04 seconds (average 0.011000, setup 0.00)

yes = 944, no = 165406, maybe = 30481

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=196831, nc=52064, nnz=145726, k=2] [1.9 MB]
Creating vector for yes... [1.5 MB]
Allocating iteration vectors... [2 x 1.5 MB]
TOTAL: [6.4 MB]

Starting iterations...

Iterative method: 94 iterations in 0.31 seconds (average 0.001319, setup 0.19)

Value in the initial state: 0.9783993763643045

Time for model checking: 0.747 seconds.

Result: 0.9783993763643045 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmin=? [ F "MissionSuccessful" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=4,Objy1=1,Objx2=4,Objy2=3

Prob0E: 65 iterations in 0.41 seconds (average 0.006277, setup 0.00)

Prob1A: 3 iterations in 0.03 seconds (average 0.010667, setup 0.00)

yes = 944, no = 169849, maybe = 26038

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=196831, nc=44446, nnz=123848, k=2] [1.6 MB]
Creating vector for yes... [1.5 MB]
Allocating iteration vectors... [2 x 1.5 MB]
TOTAL: [6.2 MB]

Starting iterations...

Iterative method: 89 iterations in 0.25 seconds (average 0.001258, setup 0.14)

Value in the initial state: 0.7572806324485073

Time for model checking: 0.707 seconds.

Result: 0.7572806324485073 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmax=? [ F "MissionTimeUp" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=4,Objy1=1,Objx2=4,Objy2=3

Prob0A: 61 iterations in 0.40 seconds (average 0.006557, setup 0.00)

Prob1E: 4 iterations in 0.03 seconds (average 0.008000, setup 0.00)

yes = 372, no = 114658, maybe = 81801

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=196831, nc=135214, nnz=367082, k=4] [4.5 MB]
Creating vector for yes... [1.5 MB]
Allocating iteration vectors... [2 x 1.5 MB]
TOTAL: [9.0 MB]

Starting iterations...

Iterative method: 90 iterations in 0.39 seconds (average 0.001733, setup 0.24)

Value in the initial state: 0.0012616608706056583

Time for model checking: 0.827 seconds.

Result: 0.0012616608706056583 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmin=? [ F "MissionTimeUp" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=4,Objy1=1,Objx2=4,Objy2=3

Prob0E: 69 iterations in 0.60 seconds (average 0.008638, setup 0.00)

Prob1A: 3 iterations in 0.03 seconds (average 0.010667, setup 0.00)

yes = 372, no = 114996, maybe = 81463

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=196831, nc=134614, nnz=365812, k=4] [4.5 MB]
Creating vector for yes... [1.5 MB]
Allocating iteration vectors... [2 x 1.5 MB]
TOTAL: [9.0 MB]

Starting iterations...

Iterative method: 105 iterations in 0.39 seconds (average 0.001752, setup 0.21)

Value in the initial state: 4.887465292052315E-6

Time for model checking: 1.049 seconds.

Result: 4.887465292052315E-6 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmax=? [ F "Crash" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=4,Objy1=1,Objx2=4,Objy2=3

Prob0A: 3 iterations in 0.06 seconds (average 0.020000, setup 0.00)

Prob1E: 248 iterations in 2.30 seconds (average 0.009258, setup 0.00)

yes = 96475, no = 2595, maybe = 97761

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=196831, nc=161183, nnz=421620, k=4] [5.2 MB]
Creating vector for yes... [1.5 MB]
Allocating iteration vectors... [2 x 1.5 MB]
TOTAL: [9.7 MB]

Starting iterations...

Iterative method: 89 iterations in 0.43 seconds (average 0.001798, setup 0.27)

Value in the initial state: 0.06232312190613047

Time for model checking: 2.803 seconds.

Result: 0.06232312190613047 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmin=? [ F "Crash" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=4,Objy1=1,Objx2=4,Objy2=3

Prob0E: 3 iterations in 0.05 seconds (average 0.017333, setup 0.00)

Prob1A: 45 iterations in 0.39 seconds (average 0.008622, setup 0.00)

yes = 96475, no = 2595, maybe = 97761

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=196831, nc=161183, nnz=421620, k=4] [5.2 MB]
Creating vector for yes... [1.5 MB]
Allocating iteration vectors... [2 x 1.5 MB]
TOTAL: [9.7 MB]

Starting iterations...

Iterative method: 93 iterations in 0.38 seconds (average 0.001892, setup 0.20)

Value in the initial state: 0.01958518181495069

Time for model checking: 0.832 seconds.

Result: 0.01958518181495069 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmax=? [ F "AllAreaSearched" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=4,Objy1=1,Objx2=4,Objy2=3

Prob0A: 53 iterations in 0.35 seconds (average 0.006642, setup 0.00)

Prob1E: 4 iterations in 0.04 seconds (average 0.009000, setup 0.00)

yes = 1278, no = 152526, maybe = 43027

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=196831, nc=69710, nnz=187761, k=2] [2.4 MB]
Creating vector for yes... [1.5 MB]
Allocating iteration vectors... [2 x 1.5 MB]
TOTAL: [6.9 MB]

Starting iterations...

Iterative method: 85 iterations in 0.29 seconds (average 0.001412, setup 0.17)

Value in the initial state: 0.21499593455984392

Time for model checking: 0.693 seconds.

Result: 0.21499593455984392 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmin=? [ F "AllAreaSearched" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=4,Objy1=1,Objx2=4,Objy2=3

Prob0E: 68 iterations in 0.50 seconds (average 0.007294, setup 0.00)

Prob1A: 3 iterations in 0.03 seconds (average 0.010667, setup 0.00)

yes = 1278, no = 158786, maybe = 36767

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=196831, nc=59618, nnz=160069, k=2] [2.1 MB]
Creating vector for yes... [1.5 MB]
Allocating iteration vectors... [2 x 1.5 MB]
TOTAL: [6.6 MB]

Starting iterations...

Iterative method: 88 iterations in 0.24 seconds (average 0.001364, setup 0.12)

Value in the initial state: 0.0012483894712874116

Time for model checking: 0.784 seconds.

Result: 0.0012483894712874116 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmax=? [ F "MissionTimeUp"|"AllAreaSearched"|"Crash" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=4,Objy1=1,Objx2=4,Objy2=3

Prob0A: 3 iterations in 0.09 seconds (average 0.029333, setup 0.00)

Prob1E: 252 iterations in 2.20 seconds (average 0.008730, setup 0.00)

yes = 169848, no = 945, maybe = 26038

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=196831, nc=44446, nnz=123848, k=2] [1.6 MB]
Creating vector for yes... [1.5 MB]
Allocating iteration vectors... [2 x 1.5 MB]
TOTAL: [6.2 MB]

Starting iterations...

Iterative method: 83 iterations in 0.25 seconds (average 0.001253, setup 0.15)

Value in the initial state: 0.24271907163660905

Time for model checking: 2.541 seconds.

Result: 0.24271907163660905 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmin=? [ F "MissionTimeUp"|"AllAreaSearched"|"Crash" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=4,Objy1=1,Objx2=4,Objy2=3

Prob0E: 3 iterations in 0.04 seconds (average 0.013333, setup 0.00)

Prob1A: 52 iterations in 0.34 seconds (average 0.006615, setup 0.00)

yes = 165405, no = 945, maybe = 30481

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=196831, nc=52064, nnz=145726, k=2] [1.9 MB]
Creating vector for yes... [1.5 MB]
Allocating iteration vectors... [2 x 1.5 MB]
TOTAL: [6.4 MB]

Starting iterations...

Iterative method: 91 iterations in 0.25 seconds (average 0.001275, setup 0.14)

Value in the initial state: 0.021600141254202034

Time for model checking: 0.658 seconds.

Result: 0.021600141254202034 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"drop"}max=? [ F "deadlock" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=4,Objy1=1,Objx2=4,Objy2=3

Prob0E: 4 iterations in 0.05 seconds (average 0.012000, setup 0.00)

Prob1A: 1 iterations in 0.01 seconds (average 0.008000, setup 0.00)

goal = 1, inf = 0, maybe = 196830

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=196831, nc=260252, nnz=520689, k=4] [7.7 MB]
Building sparse matrix (transition rewards)... [n=196831, nc=260252, nnz=0, k=4] [1.7 MB]
Creating vector for state rewards... [1.5 MB]
Creating vector for inf... [1.5 MB]
Allocating iteration vectors... [2 x 1.5 MB]
TOTAL: [15.5 MB]

Starting iterations...

Iterative method: 89 iterations in 0.59 seconds (average 0.002787, setup 0.34)

Value in the initial state: 0.22992620392069787

Time for model checking: 0.659 seconds.

Result: 0.22992620392069787 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"drop"}min=? [ F "deadlock" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=4,Objy1=1,Objx2=4,Objy2=3

Prob0A: 4 iterations in 0.03 seconds (average 0.007000, setup 0.00)

Prob1E: 5 iterations in 0.03 seconds (average 0.005600, setup 0.00)

Warning: PRISM hasn't checked for zero-reward loops.
Your minimum rewards may be too low...

goal = 1, inf = 0, maybe = 196830

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=196831, nc=260252, nnz=520689, k=4] [7.7 MB]
Building sparse matrix (transition rewards)... [n=196831, nc=260252, nnz=0, k=4] [1.7 MB]
Creating vector for state rewards... [1.5 MB]
Creating vector for inf... [1.5 MB]
Allocating iteration vectors... [2 x 1.5 MB]
TOTAL: [15.5 MB]

Starting iterations...

Iterative method: 83 iterations in 0.51 seconds (average 0.002843, setup 0.28)

Value in the initial state: 0.1570870207004101

Time for model checking: 0.575 seconds.

Result: 0.1570870207004101 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"charge"}max=? [ F "deadlock" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=4,Objy1=1,Objx2=4,Objy2=3

Prob0E: 4 iterations in 0.02 seconds (average 0.005000, setup 0.00)

Prob1A: 1 iterations in 0.00 seconds (average 0.004000, setup 0.00)

goal = 1, inf = 0, maybe = 196830

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=196831, nc=260252, nnz=520689, k=4] [7.7 MB]
Building sparse matrix (transition rewards)... [n=196831, nc=260252, nnz=5786, k=4] [1.8 MB]
Creating vector for state rewards... [1.5 MB]
Creating vector for inf... [1.5 MB]
Allocating iteration vectors... [2 x 1.5 MB]
TOTAL: [15.5 MB]

Starting iterations...

Iterative method: 85 iterations in 0.52 seconds (average 0.002871, setup 0.27)

Value in the initial state: 0.9814508129882522

Time for model checking: 0.553 seconds.

Result: 0.9814508129882522 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"charge"}min=? [ F "deadlock" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=4,Objy1=1,Objx2=4,Objy2=3

Prob0A: 4 iterations in 0.02 seconds (average 0.004000, setup 0.00)

Prob1E: 5 iterations in 0.03 seconds (average 0.005600, setup 0.00)

Warning: PRISM hasn't checked for zero-reward loops.
Your minimum rewards may be too low...

goal = 1, inf = 0, maybe = 196830

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=196831, nc=260252, nnz=520689, k=4] [7.7 MB]
Building sparse matrix (transition rewards)... [n=196831, nc=260252, nnz=5786, k=4] [1.8 MB]
Creating vector for state rewards... [1.5 MB]
Creating vector for inf... [1.5 MB]
Allocating iteration vectors... [2 x 1.5 MB]
TOTAL: [15.5 MB]

Starting iterations...

Iterative method: 95 iterations in 0.55 seconds (average 0.002821, setup 0.28)

Value in the initial state: 0.06262257826336376

Time for model checking: 0.606 seconds.

Result: 0.06262257826336376 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"time"}max=? [ F "deadlock" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=4,Objy1=1,Objx2=4,Objy2=3

Prob0E: 4 iterations in 0.02 seconds (average 0.004000, setup 0.00)

Prob1A: 1 iterations in 0.00 seconds (average 0.004000, setup 0.00)

goal = 1, inf = 0, maybe = 196830

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=196831, nc=260252, nnz=520689, k=4] [7.7 MB]
Building sparse matrix (transition rewards)... [n=196831, nc=260252, nnz=99068, k=4] [2.9 MB]
Creating vector for state rewards... [1.5 MB]
Creating vector for inf... [1.5 MB]
Allocating iteration vectors... [2 x 1.5 MB]
TOTAL: [16.6 MB]

Starting iterations...

Iterative method: 91 iterations in 0.57 seconds (average 0.002857, setup 0.31)

Value in the initial state: 342.4527360579836

Time for model checking: 0.607 seconds.

Result: 342.4527360579836 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"time"}min=? [ F "deadlock" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=4,Objy1=1,Objx2=4,Objy2=3

Prob0A: 4 iterations in 0.02 seconds (average 0.005000, setup 0.00)

Prob1E: 5 iterations in 0.03 seconds (average 0.006400, setup 0.00)

Warning: PRISM hasn't checked for zero-reward loops.
Your minimum rewards may be too low...

goal = 1, inf = 0, maybe = 196830

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=196831, nc=260252, nnz=520689, k=4] [7.7 MB]
Building sparse matrix (transition rewards)... [n=196831, nc=260252, nnz=99068, k=4] [2.9 MB]
Creating vector for state rewards... [1.5 MB]
Creating vector for inf... [1.5 MB]
Allocating iteration vectors... [2 x 1.5 MB]
TOTAL: [16.6 MB]

Starting iterations...

Iterative method: 99 iterations in 0.60 seconds (average 0.002949, setup 0.31)

Value in the initial state: 108.47041104537706

Time for model checking: 0.66 seconds.

Result: 108.47041104537706 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"success"}max=? [ F "deadlock" ]/Pmin=? [ F "MissionSuccessful" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=4,Objy1=1,Objx2=4,Objy2=3

Prob0E: 4 iterations in 0.02 seconds (average 0.005000, setup 0.00)

Prob1A: 1 iterations in 0.00 seconds (average 0.004000, setup 0.00)

goal = 1, inf = 0, maybe = 196830

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=196831, nc=260252, nnz=520689, k=4] [7.7 MB]
Building sparse matrix (transition rewards)... [n=196831, nc=260252, nnz=1860, k=4] [1.8 MB]
Creating vector for state rewards... [1.5 MB]
Creating vector for inf... [1.5 MB]
Allocating iteration vectors... [2 x 1.5 MB]
TOTAL: [15.5 MB]

Starting iterations...

Iterative method: 92 iterations in 0.53 seconds (average 0.002826, setup 0.27)

Prob0E: 65 iterations in 0.35 seconds (average 0.005415, setup 0.00)

Prob1A: 3 iterations in 0.04 seconds (average 0.013333, setup 0.00)

yes = 944, no = 169849, maybe = 26038

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=196831, nc=44446, nnz=123848, k=2] [1.6 MB]
Creating vector for yes... [1.5 MB]
Allocating iteration vectors... [2 x 1.5 MB]
TOTAL: [6.2 MB]

Starting iterations...

Iterative method: 89 iterations in 0.23 seconds (average 0.001258, setup 0.12)

Value in the initial state: 441.8382559614069

Time for model checking: 1.22 seconds.

Result: 441.8382559614069 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"success"}min=? [ F "deadlock" ]/Pmax=? [ F "MissionSuccessful" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=4,Objy1=1,Objx2=4,Objy2=3

Prob0A: 4 iterations in 0.05 seconds (average 0.013000, setup 0.00)

Prob1E: 5 iterations in 0.04 seconds (average 0.007200, setup 0.00)

Warning: PRISM hasn't checked for zero-reward loops.
Your minimum rewards may be too low...

goal = 1, inf = 0, maybe = 196830

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=196831, nc=260252, nnz=520689, k=4] [7.7 MB]
Building sparse matrix (transition rewards)... [n=196831, nc=260252, nnz=1860, k=4] [1.8 MB]
Creating vector for state rewards... [1.5 MB]
Creating vector for inf... [1.5 MB]
Allocating iteration vectors... [2 x 1.5 MB]
TOTAL: [15.5 MB]

Starting iterations...

Iterative method: 93 iterations in 0.55 seconds (average 0.002796, setup 0.29)

Prob0A: 52 iterations in 0.28 seconds (average 0.005462, setup 0.00)

Prob1E: 4 iterations in 0.02 seconds (average 0.006000, setup 0.00)

yes = 944, no = 165406, maybe = 30481

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=196831, nc=52064, nnz=145726, k=2] [1.9 MB]
Creating vector for yes... [1.5 MB]
Allocating iteration vectors... [2 x 1.5 MB]
TOTAL: [6.4 MB]

Starting iterations...

Iterative method: 94 iterations in 0.24 seconds (average 0.001319, setup 0.12)

Value in the initial state: 80.68047878382941

Time for model checking: 1.189 seconds.

Result: 80.68047878382941 (value in the initial state)

---------------------------------------------------------------------

Note: There were 16 warnings during computation.

