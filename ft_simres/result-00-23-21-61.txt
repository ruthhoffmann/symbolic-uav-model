PRISM
=====

Version: 4.3
Date: Tue Aug 02 12:27:28 BST 2016
Hostname: eglinton
Memory limits: cudd=1g, java(heap)=981.5m
Command line: prism uav_sim_ft_mdp.prism -s uav_mdp.props -const 'Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=2,Objy1=1,Objx2=6,Objy2=1,'

Parsing model file "uav_sim_ft_mdp.prism"...

Parsing properties file "uav_mdp.props"...

18 properties:
(1) Pmax=? [ F "MissionSuccessful" ]
(2) Pmin=? [ F "MissionSuccessful" ]
(3) Pmax=? [ F "MissionTimeUp" ]
(4) Pmin=? [ F "MissionTimeUp" ]
(5) Pmax=? [ F "Crash" ]
(6) Pmin=? [ F "Crash" ]
(7) Pmax=? [ F "AllAreaSearched" ]
(8) Pmin=? [ F "AllAreaSearched" ]
(9) Pmax=? [ F "MissionTimeUp"|"AllAreaSearched"|"Crash" ]
(10) Pmin=? [ F "MissionTimeUp"|"AllAreaSearched"|"Crash" ]
(11) R{"drop"}max=? [ F "deadlock" ]
(12) R{"drop"}min=? [ F "deadlock" ]
(13) R{"charge"}max=? [ F "deadlock" ]
(14) R{"charge"}min=? [ F "deadlock" ]
(15) R{"time"}max=? [ F "deadlock" ]
(16) R{"time"}min=? [ F "deadlock" ]
(17) R{"success"}max=? [ F "deadlock" ]/Pmin=? [ F "MissionSuccessful" ]
(18) R{"success"}min=? [ F "deadlock" ]/Pmax=? [ F "MissionSuccessful" ]

Type:        MDP
Modules:     Obj Move Robot T 
Variables:   NoOfObjs posx posy s1 retx rety b t c 

---------------------------------------------------------------------

Model checking: Pmax=? [ F "MissionSuccessful" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=2,Objy1=1,Objx2=6,Objy2=1

Building model...
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=2,Objy1=1,Objx2=6,Objy2=1

Warning: Guard for command 7 of module "Move" is never satisfied.

Warning: Guard for command 8 of module "Move" is never satisfied.

Warning: Guard for command 9 of module "Move" is never satisfied.

Warning: Guard for command 7 of module "Robot" is never satisfied.

Warning: Update 2 of command 3 of module "Robot" doesn't do anything ("(s1'=13)", line 249, column 44)

Warning: Update 2 of command 27 of module "T" doesn't do anything ("(c'=1)", line 372, column 108)

Warning: Guard for command 28 of module "T" is never satisfied.

Warning: Update 2 of command 9 of module "T" doesn't do anything ("(c'=1)", line 349, column 112)

Warning: Guard for command 10 of module "T" is never satisfied.

Warning: Update 2 of command 51 of module "T" doesn't do anything ("(c'=1)", line 403, column 113)

Warning: Guard for command 52 of module "T" is never satisfied.

Computing reachable states...

Reachability (BFS): 82 iterations in 0.91 seconds (average 0.011122, setup 0.00)

Time for model construction: 2.146 seconds.

Warning: Deadlocks detected and fixed in 1 states

Type:        MDP
States:      229289 (1 initial)
Transitions: 618660
Choices:     304824

Transition matrix: 175080 nodes (76 terminal), 618660 minterms, vars: 34r/34c/18nd

Prob0A: 44 iterations in 0.38 seconds (average 0.008636, setup 0.00)

Prob1E: 4 iterations in 0.06 seconds (average 0.014000, setup 0.00)

yes = 948, no = 193448, maybe = 34893

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=229289, nc=62607, nnz=182680, k=2] [2.4 MB]
Creating vector for yes... [1.7 MB]
Allocating iteration vectors... [2 x 1.7 MB]
TOTAL: [7.6 MB]

Starting iterations...

Iterative method: 90 iterations in 0.39 seconds (average 0.001600, setup 0.24)

Value in the initial state: 0.9807527727866875

Time for model checking: 0.858 seconds.

Result: 0.9807527727866875 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmin=? [ F "MissionSuccessful" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=2,Objy1=1,Objx2=6,Objy2=1

Prob0E: 46 iterations in 0.48 seconds (average 0.010348, setup 0.00)

Prob1A: 3 iterations in 0.04 seconds (average 0.013333, setup 0.00)

yes = 948, no = 199347, maybe = 28994

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=229289, nc=51753, nnz=150584, k=2] [2.0 MB]
Creating vector for yes... [1.7 MB]
Allocating iteration vectors... [2 x 1.7 MB]
TOTAL: [7.2 MB]

Starting iterations...

Iterative method: 85 iterations in 0.30 seconds (average 0.001506, setup 0.17)

Value in the initial state: 0.7285958140514174

Time for model checking: 0.837 seconds.

Result: 0.7285958140514174 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmax=? [ F "MissionTimeUp" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=2,Objy1=1,Objx2=6,Objy2=1

Prob0A: 60 iterations in 0.50 seconds (average 0.008267, setup 0.00)

Prob1E: 4 iterations in 0.04 seconds (average 0.011000, setup 0.00)

yes = 468, no = 142021, maybe = 86800

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=229289, nc=152485, nnz=427294, k=4] [5.3 MB]
Creating vector for yes... [1.7 MB]
Allocating iteration vectors... [2 x 1.7 MB]
TOTAL: [10.5 MB]

Starting iterations...

Iterative method: 90 iterations in 0.48 seconds (average 0.002044, setup 0.30)

Value in the initial state: 0.001236101503622506

Time for model checking: 1.043 seconds.

Result: 0.001236101503622506 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmin=? [ F "MissionTimeUp" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=2,Objy1=1,Objx2=6,Objy2=1

Prob0E: 67 iterations in 0.74 seconds (average 0.011104, setup 0.00)

Prob1A: 3 iterations in 0.05 seconds (average 0.016000, setup 0.00)

yes = 468, no = 142416, maybe = 86405

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=229289, nc=151771, nnz=425907, k=4] [5.2 MB]
Creating vector for yes... [1.7 MB]
Allocating iteration vectors... [2 x 1.7 MB]
TOTAL: [10.5 MB]

Starting iterations...

Iterative method: 107 iterations in 0.49 seconds (average 0.002093, setup 0.27)

Value in the initial state: 4.032808483365383E-6

Time for model checking: 1.314 seconds.

Result: 4.032808483365383E-6 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmax=? [ F "Crash" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=2,Objy1=1,Objx2=6,Objy2=1

Prob0A: 3 iterations in 0.08 seconds (average 0.025333, setup 0.00)

Prob1E: 169 iterations in 2.36 seconds (average 0.013964, setup 0.00)

yes = 112223, no = 2646, maybe = 114420

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=229289, nc=189955, nnz=503791, k=4] [6.2 MB]
Creating vector for yes... [1.7 MB]
Allocating iteration vectors... [2 x 1.7 MB]
TOTAL: [11.4 MB]

Starting iterations...

Iterative method: 87 iterations in 0.54 seconds (average 0.002253, setup 0.34)

Value in the initial state: 0.03192646671005073

Time for model checking: 2.999 seconds.

Result: 0.03192646671005073 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmin=? [ F "Crash" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=2,Objy1=1,Objx2=6,Objy2=1

Prob0E: 3 iterations in 0.06 seconds (average 0.020000, setup 0.00)

Prob1A: 42 iterations in 0.52 seconds (average 0.012381, setup 0.00)

yes = 112223, no = 2646, maybe = 114420

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=229289, nc=189955, nnz=503791, k=4] [6.2 MB]
Creating vector for yes... [1.7 MB]
Allocating iteration vectors... [2 x 1.7 MB]
TOTAL: [11.4 MB]

Starting iterations...

Iterative method: 89 iterations in 0.45 seconds (average 0.002157, setup 0.26)

Value in the initial state: 0.01712509680004086

Time for model checking: 1.059 seconds.

Result: 0.01712509680004086 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmax=? [ F "AllAreaSearched" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=2,Objy1=1,Objx2=6,Objy2=1

Prob0A: 57 iterations in 0.46 seconds (average 0.008000, setup 0.00)

Prob1E: 4 iterations in 0.05 seconds (average 0.012000, setup 0.00)

yes = 1229, no = 183112, maybe = 44948

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=229289, nc=67181, nnz=173177, k=2] [2.3 MB]
Creating vector for yes... [1.7 MB]
Allocating iteration vectors... [2 x 1.7 MB]
TOTAL: [7.5 MB]

Starting iterations...

Iterative method: 89 iterations in 0.32 seconds (average 0.001708, setup 0.17)

Value in the initial state: 0.24182648448253402

Time for model checking: 0.836 seconds.

Result: 0.24182648448253402 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmin=? [ F "AllAreaSearched" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=2,Objy1=1,Objx2=6,Objy2=1

Prob0E: 64 iterations in 0.38 seconds (average 0.005875, setup 0.00)

Prob1A: 3 iterations in 0.03 seconds (average 0.010667, setup 0.00)

yes = 1229, no = 188639, maybe = 39421

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=229289, nc=57588, nnz=145886, k=2] [1.9 MB]
Creating vector for yes... [1.7 MB]
Allocating iteration vectors... [2 x 1.7 MB]
TOTAL: [7.2 MB]

Starting iterations...

Iterative method: 94 iterations in 0.26 seconds (average 0.001574, setup 0.11)

Value in the initial state: 0.00173028279556934

Time for model checking: 0.688 seconds.

Result: 0.00173028279556934 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmax=? [ F "MissionTimeUp"|"AllAreaSearched"|"Crash" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=2,Objy1=1,Objx2=6,Objy2=1

Prob0A: 3 iterations in 0.11 seconds (average 0.036000, setup 0.00)

Prob1E: 180 iterations in 2.35 seconds (average 0.013067, setup 0.00)

yes = 199346, no = 949, maybe = 28994

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=229289, nc=51753, nnz=150584, k=2] [2.0 MB]
Creating vector for yes... [1.7 MB]
Allocating iteration vectors... [2 x 1.7 MB]
TOTAL: [7.2 MB]

Starting iterations...

Iterative method: 77 iterations in 0.30 seconds (average 0.001506, setup 0.19)

Value in the initial state: 0.27140392459201235

Time for model checking: 2.773 seconds.

Result: 0.27140392459201235 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmin=? [ F "MissionTimeUp"|"AllAreaSearched"|"Crash" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=2,Objy1=1,Objx2=6,Objy2=1

Prob0E: 3 iterations in 0.06 seconds (average 0.021333, setup 0.00)

Prob1A: 44 iterations in 0.43 seconds (average 0.009818, setup 0.00)

yes = 193447, no = 949, maybe = 34893

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=229289, nc=62607, nnz=182680, k=2] [2.4 MB]
Creating vector for yes... [1.7 MB]
Allocating iteration vectors... [2 x 1.7 MB]
TOTAL: [7.6 MB]

Starting iterations...

Iterative method: 89 iterations in 0.39 seconds (average 0.001618, setup 0.24)

Value in the initial state: 0.01924663554480362

Time for model checking: 0.893 seconds.

Result: 0.01924663554480362 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"drop"}max=? [ F "deadlock" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=2,Objy1=1,Objx2=6,Objy2=1

Prob0E: 4 iterations in 0.09 seconds (average 0.023000, setup 0.00)

Prob1A: 1 iterations in 0.01 seconds (average 0.012000, setup 0.00)

goal = 1, inf = 0, maybe = 229288

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=229289, nc=304823, nnz=618659, k=4] [9.1 MB]
Building sparse matrix (transition rewards)... [n=229289, nc=304823, nnz=0, k=4] [2.0 MB]
Creating vector for state rewards... [1.7 MB]
Creating vector for inf... [1.7 MB]
Allocating iteration vectors... [2 x 1.7 MB]
TOTAL: [18.2 MB]

Starting iterations...

Iterative method: 87 iterations in 0.70 seconds (average 0.003310, setup 0.41)

Value in the initial state: 0.240365267836235

Time for model checking: 0.817 seconds.

Result: 0.240365267836235 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"drop"}min=? [ F "deadlock" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=2,Objy1=1,Objx2=6,Objy2=1

Prob0A: 4 iterations in 0.03 seconds (average 0.007000, setup 0.00)

Prob1E: 5 iterations in 0.04 seconds (average 0.008000, setup 0.00)

Warning: PRISM hasn't checked for zero-reward loops.
Your minimum rewards may be too low...

goal = 1, inf = 0, maybe = 229288

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=229289, nc=304823, nnz=618659, k=4] [9.1 MB]
Building sparse matrix (transition rewards)... [n=229289, nc=304823, nnz=0, k=4] [2.0 MB]
Creating vector for state rewards... [1.7 MB]
Creating vector for inf... [1.7 MB]
Allocating iteration vectors... [2 x 1.7 MB]
TOTAL: [18.2 MB]

Starting iterations...

Iterative method: 76 iterations in 0.59 seconds (average 0.003368, setup 0.33)

Value in the initial state: 0.2143178995370823

Time for model checking: 0.661 seconds.

Result: 0.2143178995370823 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"charge"}max=? [ F "deadlock" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=2,Objy1=1,Objx2=6,Objy2=1

Prob0E: 4 iterations in 0.02 seconds (average 0.006000, setup 0.00)

Prob1A: 1 iterations in 0.00 seconds (average 0.004000, setup 0.00)

goal = 1, inf = 0, maybe = 229288

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=229289, nc=304823, nnz=618659, k=4] [9.1 MB]
Building sparse matrix (transition rewards)... [n=229289, nc=304823, nnz=7130, k=4] [2.1 MB]
Creating vector for state rewards... [1.7 MB]
Creating vector for inf... [1.7 MB]
Allocating iteration vectors... [2 x 1.7 MB]
TOTAL: [18.2 MB]

Starting iterations...

Iterative method: 82 iterations in 0.61 seconds (average 0.003366, setup 0.34)

Value in the initial state: 0.3120167748325073

Time for model checking: 0.653 seconds.

Result: 0.3120167748325073 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"charge"}min=? [ F "deadlock" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=2,Objy1=1,Objx2=6,Objy2=1

Prob0A: 4 iterations in 0.02 seconds (average 0.006000, setup 0.00)

Prob1E: 5 iterations in 0.04 seconds (average 0.008000, setup 0.00)

Warning: PRISM hasn't checked for zero-reward loops.
Your minimum rewards may be too low...

goal = 1, inf = 0, maybe = 229288

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=229289, nc=304823, nnz=618659, k=4] [9.1 MB]
Building sparse matrix (transition rewards)... [n=229289, nc=304823, nnz=7130, k=4] [2.1 MB]
Creating vector for state rewards... [1.7 MB]
Creating vector for inf... [1.7 MB]
Allocating iteration vectors... [2 x 1.7 MB]
TOTAL: [18.2 MB]

Starting iterations...

Iterative method: 91 iterations in 0.64 seconds (average 0.003385, setup 0.34)

Value in the initial state: 0.051659124582533

Time for model checking: 0.716 seconds.

Result: 0.051659124582533 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"time"}max=? [ F "deadlock" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=2,Objy1=1,Objx2=6,Objy2=1

Prob0E: 4 iterations in 0.02 seconds (average 0.005000, setup 0.00)

Prob1A: 1 iterations in 0.00 seconds (average 0.004000, setup 0.00)

goal = 1, inf = 0, maybe = 229288

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=229289, nc=304823, nnz=618659, k=4] [9.1 MB]
Building sparse matrix (transition rewards)... [n=229289, nc=304823, nnz=114867, k=4] [3.4 MB]
Creating vector for state rewards... [1.7 MB]
Creating vector for inf... [1.7 MB]
Allocating iteration vectors... [2 x 1.7 MB]
TOTAL: [19.5 MB]

Starting iterations...

Iterative method: 90 iterations in 0.67 seconds (average 0.003378, setup 0.37)

Value in the initial state: 175.98631852580368

Time for model checking: 0.711 seconds.

Result: 175.98631852580368 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"time"}min=? [ F "deadlock" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=2,Objy1=1,Objx2=6,Objy2=1

Prob0A: 4 iterations in 0.02 seconds (average 0.004000, setup 0.00)

Prob1E: 5 iterations in 0.04 seconds (average 0.008000, setup 0.00)

Warning: PRISM hasn't checked for zero-reward loops.
Your minimum rewards may be too low...

goal = 1, inf = 0, maybe = 229288

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=229289, nc=304823, nnz=618659, k=4] [9.1 MB]
Building sparse matrix (transition rewards)... [n=229289, nc=304823, nnz=114867, k=4] [3.4 MB]
Creating vector for state rewards... [1.7 MB]
Creating vector for inf... [1.7 MB]
Allocating iteration vectors... [2 x 1.7 MB]
TOTAL: [19.5 MB]

Starting iterations...

Iterative method: 97 iterations in 0.71 seconds (average 0.003588, setup 0.36)

Value in the initial state: 94.88562836145059

Time for model checking: 0.796 seconds.

Result: 94.88562836145059 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"success"}max=? [ F "deadlock" ]/Pmin=? [ F "MissionSuccessful" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=2,Objy1=1,Objx2=6,Objy2=1

Prob0E: 4 iterations in 0.02 seconds (average 0.006000, setup 0.00)

Prob1A: 1 iterations in 0.00 seconds (average 0.004000, setup 0.00)

goal = 1, inf = 0, maybe = 229288

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=229289, nc=304823, nnz=618659, k=4] [9.1 MB]
Building sparse matrix (transition rewards)... [n=229289, nc=304823, nnz=1868, k=4] [2.1 MB]
Creating vector for state rewards... [1.7 MB]
Creating vector for inf... [1.7 MB]
Allocating iteration vectors... [2 x 1.7 MB]
TOTAL: [18.2 MB]

Starting iterations...

Iterative method: 93 iterations in 0.77 seconds (average 0.003441, setup 0.45)

Prob0E: 46 iterations in 0.29 seconds (average 0.006348, setup 0.00)

Prob1A: 3 iterations in 0.04 seconds (average 0.014667, setup 0.00)

yes = 948, no = 199347, maybe = 28994

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=229289, nc=51753, nnz=150584, k=2] [2.0 MB]
Creating vector for yes... [1.7 MB]
Allocating iteration vectors... [2 x 1.7 MB]
TOTAL: [7.2 MB]

Starting iterations...

Iterative method: 85 iterations in 0.29 seconds (average 0.001506, setup 0.16)

Value in the initial state: 236.48136245218228

Time for model checking: 1.459 seconds.

Result: 236.48136245218228 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"success"}min=? [ F "deadlock" ]/Pmax=? [ F "MissionSuccessful" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=2,Objy1=1,Objx2=6,Objy2=1

Prob0A: 4 iterations in 0.06 seconds (average 0.014000, setup 0.00)

Prob1E: 5 iterations in 0.04 seconds (average 0.008800, setup 0.00)

Warning: PRISM hasn't checked for zero-reward loops.
Your minimum rewards may be too low...

goal = 1, inf = 0, maybe = 229288

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=229289, nc=304823, nnz=618659, k=4] [9.1 MB]
Building sparse matrix (transition rewards)... [n=229289, nc=304823, nnz=1868, k=4] [2.1 MB]
Creating vector for state rewards... [1.7 MB]
Creating vector for inf... [1.7 MB]
Allocating iteration vectors... [2 x 1.7 MB]
TOTAL: [18.2 MB]

Starting iterations...

Iterative method: 87 iterations in 0.64 seconds (average 0.003310, setup 0.36)

Prob0A: 44 iterations in 0.24 seconds (average 0.005455, setup 0.00)

Prob1E: 4 iterations in 0.03 seconds (average 0.008000, setup 0.00)

yes = 948, no = 193448, maybe = 34893

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=229289, nc=62607, nnz=182680, k=2] [2.4 MB]
Creating vector for yes... [1.7 MB]
Allocating iteration vectors... [2 x 1.7 MB]
TOTAL: [7.6 MB]

Starting iterations...

Iterative method: 90 iterations in 0.30 seconds (average 0.001467, setup 0.16)

Value in the initial state: 79.62978516571391

Time for model checking: 1.331 seconds.

Result: 79.62978516571391 (value in the initial state)

---------------------------------------------------------------------

Note: There were 16 warnings during computation.

