PRISM
=====

Version: 4.3
Date: Tue Aug 02 12:35:49 BST 2016
Hostname: eglinton
Memory limits: cudd=1g, java(heap)=981.5m
Command line: prism uav_sim_ft_mdp.prism -s uav_mdp.props -const 'Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=0,Objy1=2,Objx2=6,Objy2=2,'

Parsing model file "uav_sim_ft_mdp.prism"...

Parsing properties file "uav_mdp.props"...

18 properties:
(1) Pmax=? [ F "MissionSuccessful" ]
(2) Pmin=? [ F "MissionSuccessful" ]
(3) Pmax=? [ F "MissionTimeUp" ]
(4) Pmin=? [ F "MissionTimeUp" ]
(5) Pmax=? [ F "Crash" ]
(6) Pmin=? [ F "Crash" ]
(7) Pmax=? [ F "AllAreaSearched" ]
(8) Pmin=? [ F "AllAreaSearched" ]
(9) Pmax=? [ F "MissionTimeUp"|"AllAreaSearched"|"Crash" ]
(10) Pmin=? [ F "MissionTimeUp"|"AllAreaSearched"|"Crash" ]
(11) R{"drop"}max=? [ F "deadlock" ]
(12) R{"drop"}min=? [ F "deadlock" ]
(13) R{"charge"}max=? [ F "deadlock" ]
(14) R{"charge"}min=? [ F "deadlock" ]
(15) R{"time"}max=? [ F "deadlock" ]
(16) R{"time"}min=? [ F "deadlock" ]
(17) R{"success"}max=? [ F "deadlock" ]/Pmin=? [ F "MissionSuccessful" ]
(18) R{"success"}min=? [ F "deadlock" ]/Pmax=? [ F "MissionSuccessful" ]

Type:        MDP
Modules:     Obj Move Robot T 
Variables:   NoOfObjs posx posy s1 retx rety b t c 

---------------------------------------------------------------------

Model checking: Pmax=? [ F "MissionSuccessful" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=0,Objy1=2,Objx2=6,Objy2=2

Building model...
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=0,Objy1=2,Objx2=6,Objy2=2

Warning: Guard for command 7 of module "Move" is never satisfied.

Warning: Guard for command 8 of module "Move" is never satisfied.

Warning: Guard for command 9 of module "Move" is never satisfied.

Warning: Guard for command 7 of module "Robot" is never satisfied.

Warning: Update 2 of command 3 of module "Robot" doesn't do anything ("(s1'=13)", line 249, column 44)

Warning: Update 2 of command 27 of module "T" doesn't do anything ("(c'=1)", line 372, column 108)

Warning: Guard for command 28 of module "T" is never satisfied.

Warning: Update 2 of command 9 of module "T" doesn't do anything ("(c'=1)", line 349, column 112)

Warning: Guard for command 10 of module "T" is never satisfied.

Warning: Update 2 of command 51 of module "T" doesn't do anything ("(c'=1)", line 403, column 113)

Warning: Guard for command 52 of module "T" is never satisfied.

Computing reachable states...

Reachability (BFS): 82 iterations in 0.89 seconds (average 0.010878, setup 0.00)

Time for model construction: 3.34 seconds.

Warning: Deadlocks detected and fixed in 1 states

Type:        MDP
States:      223436 (1 initial)
Transitions: 631470
Choices:     303593

Transition matrix: 168081 nodes (76 terminal), 631470 minterms, vars: 34r/34c/18nd

Prob0A: 51 iterations in 0.52 seconds (average 0.010275, setup 0.00)

Prob1E: 4 iterations in 0.06 seconds (average 0.015000, setup 0.00)

yes = 925, no = 186162, maybe = 36349

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=223436, nc=64739, nnz=189043, k=2] [2.4 MB]
Creating vector for yes... [1.7 MB]
Allocating iteration vectors... [2 x 1.7 MB]
TOTAL: [7.6 MB]

Starting iterations...

Iterative method: 98 iterations in 0.39 seconds (average 0.001633, setup 0.23)

Value in the initial state: 0.9780234072255103

Time for model checking: 1.286 seconds.

Result: 0.9780234072255103 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmin=? [ F "MissionSuccessful" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=0,Objy1=2,Objx2=6,Objy2=2

Prob0E: 65 iterations in 0.71 seconds (average 0.010892, setup 0.00)

Prob1A: 3 iterations in 0.05 seconds (average 0.017333, setup 0.00)

yes = 925, no = 191005, maybe = 31506

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=223436, nc=55905, nnz=163306, k=2] [2.1 MB]
Creating vector for yes... [1.7 MB]
Allocating iteration vectors... [2 x 1.7 MB]
TOTAL: [7.2 MB]

Starting iterations...

Iterative method: 91 iterations in 0.35 seconds (average 0.001495, setup 0.22)

Value in the initial state: 0.724628241410406

Time for model checking: 1.498 seconds.

Result: 0.724628241410406 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmax=? [ F "MissionTimeUp" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=0,Objy1=2,Objx2=6,Objy2=2

Prob0A: 65 iterations in 0.59 seconds (average 0.009108, setup 0.00)

Prob1E: 4 iterations in 0.05 seconds (average 0.012000, setup 0.00)

yes = 384, no = 130356, maybe = 92696

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=223436, nc=162887, nnz=459648, k=4] [5.6 MB]
Creating vector for yes... [1.7 MB]
Allocating iteration vectors... [2 x 1.7 MB]
TOTAL: [10.7 MB]

Starting iterations...

Iterative method: 93 iterations in 0.52 seconds (average 0.001978, setup 0.33)

Value in the initial state: 0.0032557831780079148

Time for model checking: 1.356 seconds.

Result: 0.0032557831780079148 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmin=? [ F "MissionTimeUp" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=0,Objy1=2,Objx2=6,Objy2=2

Prob0E: 72 iterations in 0.83 seconds (average 0.011500, setup 0.00)

Prob1A: 3 iterations in 0.04 seconds (average 0.014667, setup 0.00)

yes = 384, no = 130789, maybe = 92263

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=223436, nc=162097, nnz=458183, k=4] [5.6 MB]
Creating vector for yes... [1.7 MB]
Allocating iteration vectors... [2 x 1.7 MB]
TOTAL: [10.7 MB]

Starting iterations...

Iterative method: 110 iterations in 0.51 seconds (average 0.002109, setup 0.28)

Value in the initial state: 1.4300505146033154E-5

Time for model checking: 1.472 seconds.

Result: 1.4300505146033154E-5 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmax=? [ F "Crash" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=0,Objy1=2,Objx2=6,Objy2=2

Prob0A: 3 iterations in 0.09 seconds (average 0.030667, setup 0.00)

Prob1E: 244 iterations in 3.16 seconds (average 0.012951, setup 0.00)

yes = 109206, no = 2545, maybe = 111685

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=223436, nc=191842, nnz=519719, k=4] [6.3 MB]
Creating vector for yes... [1.7 MB]
Allocating iteration vectors... [2 x 1.7 MB]
TOTAL: [11.5 MB]

Starting iterations...

Iterative method: 91 iterations in 0.54 seconds (average 0.002198, setup 0.34)

Value in the initial state: 0.0626412404019849

Time for model checking: 4.37 seconds.

Result: 0.0626412404019849 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmin=? [ F "Crash" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=0,Objy1=2,Objx2=6,Objy2=2

Prob0E: 3 iterations in 0.06 seconds (average 0.020000, setup 0.00)

Prob1A: 47 iterations in 0.48 seconds (average 0.010298, setup 0.00)

yes = 109206, no = 2545, maybe = 111685

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=223436, nc=191842, nnz=519719, k=4] [6.3 MB]
Creating vector for yes... [1.7 MB]
Allocating iteration vectors... [2 x 1.7 MB]
TOTAL: [11.5 MB]

Starting iterations...

Iterative method: 97 iterations in 0.46 seconds (average 0.002227, setup 0.24)

Value in the initial state: 0.019944238889114764

Time for model checking: 1.029 seconds.

Result: 0.019944238889114764 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmax=? [ F "AllAreaSearched" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=0,Objy1=2,Objx2=6,Objy2=2

Prob0A: 50 iterations in 0.40 seconds (average 0.008080, setup 0.00)

Prob1E: 4 iterations in 0.04 seconds (average 0.011000, setup 0.00)

yes = 1235, no = 170251, maybe = 51950

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=223436, nc=86159, nnz=241526, k=2] [3.1 MB]
Creating vector for yes... [1.7 MB]
Allocating iteration vectors... [2 x 1.7 MB]
TOTAL: [8.2 MB]

Starting iterations...

Iterative method: 90 iterations in 0.36 seconds (average 0.001733, setup 0.20)

Value in the initial state: 0.24575098035139975

Time for model checking: 0.837 seconds.

Result: 0.24575098035139975 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmin=? [ F "AllAreaSearched" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=0,Objy1=2,Objx2=6,Objy2=2

Prob0E: 64 iterations in 0.48 seconds (average 0.007500, setup 0.00)

Prob1A: 3 iterations in 0.04 seconds (average 0.013333, setup 0.00)

yes = 1235, no = 177540, maybe = 44661

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=223436, nc=73089, nnz=203373, k=2] [2.6 MB]
Creating vector for yes... [1.7 MB]
Allocating iteration vectors... [2 x 1.7 MB]
TOTAL: [7.7 MB]

Starting iterations...

Iterative method: 91 iterations in 0.30 seconds (average 0.001670, setup 0.14)

Value in the initial state: 0.001270628362617555

Time for model checking: 0.847 seconds.

Result: 0.001270628362617555 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmax=? [ F "MissionTimeUp"|"AllAreaSearched"|"Crash" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=0,Objy1=2,Objx2=6,Objy2=2

Prob0A: 3 iterations in 0.07 seconds (average 0.022667, setup 0.00)

Prob1E: 252 iterations in 3.08 seconds (average 0.012222, setup 0.00)

yes = 191004, no = 926, maybe = 31506

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=223436, nc=55905, nnz=163306, k=2] [2.1 MB]
Creating vector for yes... [1.7 MB]
Allocating iteration vectors... [2 x 1.7 MB]
TOTAL: [7.2 MB]

Starting iterations...

Iterative method: 84 iterations in 0.32 seconds (average 0.001476, setup 0.20)

Value in the initial state: 0.2753711341614991

Time for model checking: 4.493 seconds.

Result: 0.2753711341614991 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmin=? [ F "MissionTimeUp"|"AllAreaSearched"|"Crash" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=0,Objy1=2,Objx2=6,Objy2=2

Prob0E: 3 iterations in 0.05 seconds (average 0.016000, setup 0.00)

Prob1A: 51 iterations in 0.48 seconds (average 0.009412, setup 0.00)

yes = 186161, no = 926, maybe = 36349

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=223436, nc=64739, nnz=189043, k=2] [2.4 MB]
Creating vector for yes... [1.7 MB]
Allocating iteration vectors... [2 x 1.7 MB]
TOTAL: [7.6 MB]

Starting iterations...

Iterative method: 94 iterations in 0.30 seconds (average 0.001532, setup 0.16)

Value in the initial state: 0.02197625074289455

Time for model checking: 0.983 seconds.

Result: 0.02197625074289455 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"drop"}max=? [ F "deadlock" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=0,Objy1=2,Objx2=6,Objy2=2

Prob0E: 4 iterations in 0.08 seconds (average 0.021000, setup 0.00)

Prob1A: 1 iterations in 0.01 seconds (average 0.008000, setup 0.00)

goal = 1, inf = 0, maybe = 223435

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=223436, nc=303592, nnz=631469, k=4] [9.2 MB]
Building sparse matrix (transition rewards)... [n=223436, nc=303592, nnz=0, k=4] [2.0 MB]
Creating vector for state rewards... [1.7 MB]
Creating vector for inf... [1.7 MB]
Allocating iteration vectors... [2 x 1.7 MB]
TOTAL: [18.1 MB]

Starting iterations...

Iterative method: 93 iterations in 0.76 seconds (average 0.003398, setup 0.44)

Value in the initial state: 0.3255322879293649

Time for model checking: 0.945 seconds.

Result: 0.3255322879293649 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"drop"}min=? [ F "deadlock" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=0,Objy1=2,Objx2=6,Objy2=2

Prob0A: 4 iterations in 0.04 seconds (average 0.009000, setup 0.00)

Prob1E: 5 iterations in 0.04 seconds (average 0.008000, setup 0.00)

Warning: PRISM hasn't checked for zero-reward loops.
Your minimum rewards may be too low...

goal = 1, inf = 0, maybe = 223435

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=223436, nc=303592, nnz=631469, k=4] [9.2 MB]
Building sparse matrix (transition rewards)... [n=223436, nc=303592, nnz=0, k=4] [2.0 MB]
Creating vector for state rewards... [1.7 MB]
Creating vector for inf... [1.7 MB]
Allocating iteration vectors... [2 x 1.7 MB]
TOTAL: [18.1 MB]

Starting iterations...

Iterative method: 84 iterations in 0.60 seconds (average 0.003381, setup 0.32)

Value in the initial state: 0.19860507714412326

Time for model checking: 0.693 seconds.

Result: 0.19860507714412326 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"charge"}max=? [ F "deadlock" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=0,Objy1=2,Objx2=6,Objy2=2

Prob0E: 4 iterations in 0.02 seconds (average 0.006000, setup 0.00)

Prob1A: 1 iterations in 0.00 seconds (average 0.004000, setup 0.00)

goal = 1, inf = 0, maybe = 223435

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=223436, nc=303592, nnz=631469, k=4] [9.2 MB]
Building sparse matrix (transition rewards)... [n=223436, nc=303592, nnz=7898, k=4] [2.1 MB]
Creating vector for state rewards... [1.7 MB]
Creating vector for inf... [1.7 MB]
Allocating iteration vectors... [2 x 1.7 MB]
TOTAL: [18.2 MB]

Starting iterations...

Iterative method: 90 iterations in 0.63 seconds (average 0.003378, setup 0.32)

Value in the initial state: 0.987214272459113

Time for model checking: 0.666 seconds.

Result: 0.987214272459113 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"charge"}min=? [ F "deadlock" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=0,Objy1=2,Objx2=6,Objy2=2

Prob0A: 4 iterations in 0.02 seconds (average 0.005000, setup 0.00)

Prob1E: 5 iterations in 0.04 seconds (average 0.008000, setup 0.00)

Warning: PRISM hasn't checked for zero-reward loops.
Your minimum rewards may be too low...

goal = 1, inf = 0, maybe = 223435

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=223436, nc=303592, nnz=631469, k=4] [9.2 MB]
Building sparse matrix (transition rewards)... [n=223436, nc=303592, nnz=7898, k=4] [2.1 MB]
Creating vector for state rewards... [1.7 MB]
Creating vector for inf... [1.7 MB]
Allocating iteration vectors... [2 x 1.7 MB]
TOTAL: [18.2 MB]

Starting iterations...

Iterative method: 98 iterations in 0.65 seconds (average 0.003388, setup 0.32)

Value in the initial state: 0.08363053853119669

Time for model checking: 0.732 seconds.

Result: 0.08363053853119669 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"time"}max=? [ F "deadlock" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=0,Objy1=2,Objx2=6,Objy2=2

Prob0E: 4 iterations in 0.04 seconds (average 0.011000, setup 0.00)

Prob1A: 1 iterations in 0.01 seconds (average 0.008000, setup 0.00)

goal = 1, inf = 0, maybe = 223435

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=223436, nc=303592, nnz=631469, k=4] [9.2 MB]
Building sparse matrix (transition rewards)... [n=223436, nc=303592, nnz=111749, k=4] [3.3 MB]
Creating vector for state rewards... [1.7 MB]
Creating vector for inf... [1.7 MB]
Allocating iteration vectors... [2 x 1.7 MB]
TOTAL: [19.3 MB]

Starting iterations...

Iterative method: 94 iterations in 0.79 seconds (average 0.003447, setup 0.47)

Value in the initial state: 344.0047057238828

Time for model checking: 0.865 seconds.

Result: 344.0047057238828 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"time"}min=? [ F "deadlock" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=0,Objy1=2,Objx2=6,Objy2=2

Prob0A: 4 iterations in 0.02 seconds (average 0.006000, setup 0.00)

Prob1E: 5 iterations in 0.04 seconds (average 0.008800, setup 0.00)

Warning: PRISM hasn't checked for zero-reward loops.
Your minimum rewards may be too low...

goal = 1, inf = 0, maybe = 223435

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=223436, nc=303592, nnz=631469, k=4] [9.2 MB]
Building sparse matrix (transition rewards)... [n=223436, nc=303592, nnz=111749, k=4] [3.3 MB]
Creating vector for state rewards... [1.7 MB]
Creating vector for inf... [1.7 MB]
Allocating iteration vectors... [2 x 1.7 MB]
TOTAL: [19.3 MB]

Starting iterations...

Iterative method: 102 iterations in 0.73 seconds (average 0.003529, setup 0.37)

Value in the initial state: 110.43166654633531

Time for model checking: 0.813 seconds.

Result: 110.43166654633531 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"success"}max=? [ F "deadlock" ]/Pmin=? [ F "MissionSuccessful" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=0,Objy1=2,Objx2=6,Objy2=2

Prob0E: 4 iterations in 0.04 seconds (average 0.011000, setup 0.00)

Prob1A: 1 iterations in 0.01 seconds (average 0.008000, setup 0.00)

goal = 1, inf = 0, maybe = 223435

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=223436, nc=303592, nnz=631469, k=4] [9.2 MB]
Building sparse matrix (transition rewards)... [n=223436, nc=303592, nnz=1822, k=4] [2.0 MB]
Creating vector for state rewards... [1.7 MB]
Creating vector for inf... [1.7 MB]
Allocating iteration vectors... [2 x 1.7 MB]
TOTAL: [18.1 MB]

Starting iterations...

Iterative method: 94 iterations in 0.70 seconds (average 0.003447, setup 0.37)

Prob0E: 65 iterations in 0.40 seconds (average 0.006092, setup 0.00)

Prob1A: 3 iterations in 0.06 seconds (average 0.020000, setup 0.00)

yes = 925, no = 191005, maybe = 31506

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=223436, nc=55905, nnz=163306, k=2] [2.1 MB]
Creating vector for yes... [1.7 MB]
Allocating iteration vectors... [2 x 1.7 MB]
TOTAL: [7.2 MB]

Starting iterations...

Iterative method: 91 iterations in 0.28 seconds (average 0.001319, setup 0.16)

Value in the initial state: 462.9764355765728

Time for model checking: 1.508 seconds.

Result: 462.9764355765728 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"success"}min=? [ F "deadlock" ]/Pmax=? [ F "MissionSuccessful" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=0,Objy1=2,Objx2=6,Objy2=2

Prob0A: 4 iterations in 0.06 seconds (average 0.014000, setup 0.00)

Prob1E: 5 iterations in 0.03 seconds (average 0.006400, setup 0.00)

Warning: PRISM hasn't checked for zero-reward loops.
Your minimum rewards may be too low...

goal = 1, inf = 0, maybe = 223435

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=223436, nc=303592, nnz=631469, k=4] [9.2 MB]
Building sparse matrix (transition rewards)... [n=223436, nc=303592, nnz=1822, k=4] [2.0 MB]
Creating vector for state rewards... [1.7 MB]
Creating vector for inf... [1.7 MB]
Allocating iteration vectors... [2 x 1.7 MB]
TOTAL: [18.1 MB]

Starting iterations...

Iterative method: 96 iterations in 0.61 seconds (average 0.003000, setup 0.32)

Prob0A: 51 iterations in 0.26 seconds (average 0.005020, setup 0.00)

Prob1E: 4 iterations in 0.02 seconds (average 0.006000, setup 0.00)

yes = 925, no = 186162, maybe = 36349

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=223436, nc=64739, nnz=189043, k=2] [2.4 MB]
Creating vector for yes... [1.7 MB]
Allocating iteration vectors... [2 x 1.7 MB]
TOTAL: [7.6 MB]

Starting iterations...

Iterative method: 98 iterations in 0.25 seconds (average 0.001347, setup 0.12)

Value in the initial state: 79.06161348308271

Time for model checking: 1.244 seconds.

Result: 79.06161348308271 (value in the initial state)

---------------------------------------------------------------------

Note: There were 16 warnings during computation.

