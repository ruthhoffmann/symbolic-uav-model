PRISM
=====

Version: 4.3
Date: Tue Aug 02 12:36:49 BST 2016
Hostname: eglinton
Memory limits: cudd=1g, java(heap)=981.5m
Command line: prism uav_sim_ft_mdp.prism -s uav_mdp.props -const 'Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=1,Objy1=1,Objx2=6,Objy2=1,'

Parsing model file "uav_sim_ft_mdp.prism"...

Parsing properties file "uav_mdp.props"...

18 properties:
(1) Pmax=? [ F "MissionSuccessful" ]
(2) Pmin=? [ F "MissionSuccessful" ]
(3) Pmax=? [ F "MissionTimeUp" ]
(4) Pmin=? [ F "MissionTimeUp" ]
(5) Pmax=? [ F "Crash" ]
(6) Pmin=? [ F "Crash" ]
(7) Pmax=? [ F "AllAreaSearched" ]
(8) Pmin=? [ F "AllAreaSearched" ]
(9) Pmax=? [ F "MissionTimeUp"|"AllAreaSearched"|"Crash" ]
(10) Pmin=? [ F "MissionTimeUp"|"AllAreaSearched"|"Crash" ]
(11) R{"drop"}max=? [ F "deadlock" ]
(12) R{"drop"}min=? [ F "deadlock" ]
(13) R{"charge"}max=? [ F "deadlock" ]
(14) R{"charge"}min=? [ F "deadlock" ]
(15) R{"time"}max=? [ F "deadlock" ]
(16) R{"time"}min=? [ F "deadlock" ]
(17) R{"success"}max=? [ F "deadlock" ]/Pmin=? [ F "MissionSuccessful" ]
(18) R{"success"}min=? [ F "deadlock" ]/Pmax=? [ F "MissionSuccessful" ]

Type:        MDP
Modules:     Obj Move Robot T 
Variables:   NoOfObjs posx posy s1 retx rety b t c 

---------------------------------------------------------------------

Model checking: Pmax=? [ F "MissionSuccessful" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=1,Objy1=1,Objx2=6,Objy2=1

Building model...
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=1,Objy1=1,Objx2=6,Objy2=1

Warning: Guard for command 7 of module "Move" is never satisfied.

Warning: Guard for command 8 of module "Move" is never satisfied.

Warning: Guard for command 9 of module "Move" is never satisfied.

Warning: Guard for command 7 of module "Robot" is never satisfied.

Warning: Update 2 of command 3 of module "Robot" doesn't do anything ("(s1'=13)", line 249, column 44)

Warning: Update 2 of command 27 of module "T" doesn't do anything ("(c'=1)", line 372, column 108)

Warning: Guard for command 28 of module "T" is never satisfied.

Warning: Update 2 of command 9 of module "T" doesn't do anything ("(c'=1)", line 349, column 112)

Warning: Guard for command 10 of module "T" is never satisfied.

Warning: Update 2 of command 51 of module "T" doesn't do anything ("(c'=1)", line 403, column 113)

Warning: Guard for command 52 of module "T" is never satisfied.

Computing reachable states...

Reachability (BFS): 82 iterations in 0.88 seconds (average 0.010683, setup 0.00)

Time for model construction: 2.2 seconds.

Warning: Deadlocks detected and fixed in 1 states

Type:        MDP
States:      229702 (1 initial)
Transitions: 619690
Choices:     305314

Transition matrix: 175633 nodes (76 terminal), 619690 minterms, vars: 34r/34c/18nd

Prob0A: 45 iterations in 0.35 seconds (average 0.007822, setup 0.00)

Prob1E: 4 iterations in 0.05 seconds (average 0.013000, setup 0.00)

yes = 947, no = 193819, maybe = 34936

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=229702, nc=62352, nnz=181511, k=2] [2.4 MB]
Creating vector for yes... [1.8 MB]
Allocating iteration vectors... [2 x 1.8 MB]
TOTAL: [7.6 MB]

Starting iterations...

Iterative method: 91 iterations in 0.38 seconds (average 0.001670, setup 0.23)

Value in the initial state: 0.9801909503733195

Time for model checking: 1.047 seconds.

Result: 0.9801909503733195 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmin=? [ F "MissionSuccessful" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=1,Objy1=1,Objx2=6,Objy2=1

Prob0E: 47 iterations in 0.44 seconds (average 0.009277, setup 0.00)

Prob1A: 3 iterations in 0.05 seconds (average 0.017333, setup 0.00)

yes = 947, no = 199727, maybe = 29028

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=229702, nc=51541, nnz=149522, k=2] [2.0 MB]
Creating vector for yes... [1.8 MB]
Allocating iteration vectors... [2 x 1.8 MB]
TOTAL: [7.2 MB]

Starting iterations...

Iterative method: 86 iterations in 0.33 seconds (average 0.001581, setup 0.20)

Value in the initial state: 0.7287392571634999

Time for model checking: 0.85 seconds.

Result: 0.7287392571634999 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmax=? [ F "MissionTimeUp" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=1,Objy1=1,Objx2=6,Objy2=1

Prob0A: 60 iterations in 0.57 seconds (average 0.009467, setup 0.00)

Prob1E: 4 iterations in 0.05 seconds (average 0.012000, setup 0.00)

yes = 468, no = 141463, maybe = 87771

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=229702, nc=153490, nnz=429428, k=4] [5.3 MB]
Creating vector for yes... [1.8 MB]
Allocating iteration vectors... [2 x 1.8 MB]
TOTAL: [10.5 MB]

Starting iterations...

Iterative method: 90 iterations in 0.49 seconds (average 0.002089, setup 0.30)

Value in the initial state: 0.0012355599554368457

Time for model checking: 1.48 seconds.

Result: 0.0012355599554368457 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmin=? [ F "MissionTimeUp" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=1,Objy1=1,Objx2=6,Objy2=1

Prob0E: 67 iterations in 0.70 seconds (average 0.010507, setup 0.00)

Prob1A: 3 iterations in 0.06 seconds (average 0.021333, setup 0.00)

yes = 468, no = 141858, maybe = 87376

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=229702, nc=152776, nnz=428041, k=4] [5.3 MB]
Creating vector for yes... [1.8 MB]
Allocating iteration vectors... [2 x 1.8 MB]
TOTAL: [10.5 MB]

Starting iterations...

Iterative method: 107 iterations in 0.52 seconds (average 0.002093, setup 0.30)

Value in the initial state: 4.423483455558883E-6

Time for model checking: 1.548 seconds.

Result: 4.423483455558883E-6 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmax=? [ F "Crash" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=1,Objy1=1,Objx2=6,Objy2=1

Prob0A: 3 iterations in 0.10 seconds (average 0.032000, setup 0.00)

Prob1E: 173 iterations in 2.64 seconds (average 0.015283, setup 0.00)

yes = 112426, no = 2651, maybe = 114625

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=229702, nc=190237, nnz=504613, k=4] [6.2 MB]
Creating vector for yes... [1.8 MB]
Allocating iteration vectors... [2 x 1.8 MB]
TOTAL: [11.4 MB]

Starting iterations...

Iterative method: 88 iterations in 0.53 seconds (average 0.002182, setup 0.34)

Value in the initial state: 0.0320646811106589

Time for model checking: 3.623 seconds.

Result: 0.0320646811106589 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmin=? [ F "Crash" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=1,Objy1=1,Objx2=6,Objy2=1

Prob0E: 3 iterations in 0.07 seconds (average 0.022667, setup 0.00)

Prob1A: 43 iterations in 0.54 seconds (average 0.012558, setup 0.00)

yes = 112426, no = 2651, maybe = 114625

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=229702, nc=190237, nnz=504613, k=4] [6.2 MB]
Creating vector for yes... [1.8 MB]
Allocating iteration vectors... [2 x 1.8 MB]
TOTAL: [11.4 MB]

Starting iterations...

Iterative method: 90 iterations in 0.48 seconds (average 0.002267, setup 0.28)

Value in the initial state: 0.017671487650729803

Time for model checking: 1.46 seconds.

Result: 0.017671487650729803 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmax=? [ F "AllAreaSearched" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=1,Objy1=1,Objx2=6,Objy2=1

Prob0A: 57 iterations in 0.48 seconds (average 0.008351, setup 0.00)

Prob1E: 4 iterations in 0.05 seconds (average 0.012000, setup 0.00)

yes = 1235, no = 183350, maybe = 45117

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=229702, nc=67861, nnz=175724, k=2] [2.3 MB]
Creating vector for yes... [1.8 MB]
Allocating iteration vectors... [2 x 1.8 MB]
TOTAL: [7.6 MB]

Starting iterations...

Iterative method: 89 iterations in 0.31 seconds (average 0.001618, setup 0.17)

Value in the initial state: 0.24168713776120526

Time for model checking: 0.875 seconds.

Result: 0.24168713776120526 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmin=? [ F "AllAreaSearched" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=1,Objy1=1,Objx2=6,Objy2=1

Prob0E: 64 iterations in 0.41 seconds (average 0.006375, setup 0.00)

Prob1A: 3 iterations in 0.03 seconds (average 0.010667, setup 0.00)

yes = 1235, no = 189017, maybe = 39450

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=229702, nc=58128, nnz=148153, k=2] [2.0 MB]
Creating vector for yes... [1.8 MB]
Allocating iteration vectors... [2 x 1.8 MB]
TOTAL: [7.2 MB]

Starting iterations...

Iterative method: 94 iterations in 0.26 seconds (average 0.001532, setup 0.12)

Value in the initial state: 0.0017325618789971377

Time for model checking: 0.735 seconds.

Result: 0.0017325618789971377 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmax=? [ F "MissionTimeUp"|"AllAreaSearched"|"Crash" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=1,Objy1=1,Objx2=6,Objy2=1

Prob0A: 3 iterations in 0.09 seconds (average 0.029333, setup 0.00)

Prob1E: 184 iterations in 2.66 seconds (average 0.014478, setup 0.00)

yes = 199726, no = 948, maybe = 29028

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=229702, nc=51541, nnz=149522, k=2] [2.0 MB]
Creating vector for yes... [1.8 MB]
Allocating iteration vectors... [2 x 1.8 MB]
TOTAL: [7.2 MB]

Starting iterations...

Iterative method: 78 iterations in 0.33 seconds (average 0.001590, setup 0.21)

Value in the initial state: 0.2712604858809572

Time for model checking: 3.416 seconds.

Result: 0.2712604858809572 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmin=? [ F "MissionTimeUp"|"AllAreaSearched"|"Crash" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=1,Objy1=1,Objx2=6,Objy2=1

Prob0E: 3 iterations in 0.07 seconds (average 0.022667, setup 0.00)

Prob1A: 45 iterations in 0.50 seconds (average 0.011200, setup 0.00)

yes = 193818, no = 948, maybe = 34936

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=229702, nc=62352, nnz=181511, k=2] [2.4 MB]
Creating vector for yes... [1.8 MB]
Allocating iteration vectors... [2 x 1.8 MB]
TOTAL: [7.6 MB]

Starting iterations...

Iterative method: 90 iterations in 0.36 seconds (average 0.001511, setup 0.22)

Value in the initial state: 0.019808588899366295

Time for model checking: 1.085 seconds.

Result: 0.019808588899366295 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"drop"}max=? [ F "deadlock" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=1,Objy1=1,Objx2=6,Objy2=1

Prob0E: 4 iterations in 0.09 seconds (average 0.022000, setup 0.00)

Prob1A: 1 iterations in 0.01 seconds (average 0.008000, setup 0.00)

goal = 1, inf = 0, maybe = 229701

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=229702, nc=305313, nnz=619689, k=4] [9.1 MB]
Building sparse matrix (transition rewards)... [n=229702, nc=305313, nnz=0, k=4] [2.0 MB]
Creating vector for state rewards... [1.8 MB]
Creating vector for inf... [1.8 MB]
Allocating iteration vectors... [2 x 1.8 MB]
TOTAL: [18.2 MB]

Starting iterations...

Iterative method: 88 iterations in 0.74 seconds (average 0.003409, setup 0.44)

Value in the initial state: 0.24037254457648916

Time for model checking: 1.056 seconds.

Result: 0.24037254457648916 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"drop"}min=? [ F "deadlock" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=1,Objy1=1,Objx2=6,Objy2=1

Prob0A: 4 iterations in 0.03 seconds (average 0.008000, setup 0.00)

Prob1E: 5 iterations in 0.05 seconds (average 0.009600, setup 0.00)

Warning: PRISM hasn't checked for zero-reward loops.
Your minimum rewards may be too low...

goal = 1, inf = 0, maybe = 229701

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=229702, nc=305313, nnz=619689, k=4] [9.1 MB]
Building sparse matrix (transition rewards)... [n=229702, nc=305313, nnz=0, k=4] [2.0 MB]
Creating vector for state rewards... [1.8 MB]
Creating vector for inf... [1.8 MB]
Allocating iteration vectors... [2 x 1.8 MB]
TOTAL: [18.2 MB]

Starting iterations...

Iterative method: 77 iterations in 0.60 seconds (average 0.003429, setup 0.34)

Value in the initial state: 0.21167152811783418

Time for model checking: 0.693 seconds.

Result: 0.21167152811783418 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"charge"}max=? [ F "deadlock" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=1,Objy1=1,Objx2=6,Objy2=1

Prob0E: 4 iterations in 0.03 seconds (average 0.007000, setup 0.00)

Prob1A: 1 iterations in 0.00 seconds (average 0.004000, setup 0.00)

goal = 1, inf = 0, maybe = 229701

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=229702, nc=305313, nnz=619689, k=4] [9.1 MB]
Building sparse matrix (transition rewards)... [n=229702, nc=305313, nnz=7130, k=4] [2.1 MB]
Creating vector for state rewards... [1.8 MB]
Creating vector for inf... [1.8 MB]
Allocating iteration vectors... [2 x 1.8 MB]
TOTAL: [18.3 MB]

Starting iterations...

Iterative method: 84 iterations in 0.62 seconds (average 0.003429, setup 0.34)

Value in the initial state: 0.31248168150917016

Time for model checking: 0.674 seconds.

Result: 0.31248168150917016 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"charge"}min=? [ F "deadlock" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=1,Objy1=1,Objx2=6,Objy2=1

Prob0A: 4 iterations in 0.03 seconds (average 0.007000, setup 0.00)

Prob1E: 5 iterations in 0.05 seconds (average 0.010400, setup 0.00)

Warning: PRISM hasn't checked for zero-reward loops.
Your minimum rewards may be too low...

goal = 1, inf = 0, maybe = 229701

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=229702, nc=305313, nnz=619689, k=4] [9.1 MB]
Building sparse matrix (transition rewards)... [n=229702, nc=305313, nnz=7130, k=4] [2.1 MB]
Creating vector for state rewards... [1.8 MB]
Creating vector for inf... [1.8 MB]
Allocating iteration vectors... [2 x 1.8 MB]
TOTAL: [18.3 MB]

Starting iterations...

Iterative method: 91 iterations in 0.65 seconds (average 0.003429, setup 0.34)

Value in the initial state: 0.05905403564611265

Time for model checking: 0.744 seconds.

Result: 0.05905403564611265 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"time"}max=? [ F "deadlock" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=1,Objy1=1,Objx2=6,Objy2=1

Prob0E: 4 iterations in 0.03 seconds (average 0.007000, setup 0.00)

Prob1A: 1 iterations in 0.01 seconds (average 0.008000, setup 0.00)

goal = 1, inf = 0, maybe = 229701

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=229702, nc=305313, nnz=619689, k=4] [9.1 MB]
Building sparse matrix (transition rewards)... [n=229702, nc=305313, nnz=115075, k=4] [3.4 MB]
Creating vector for state rewards... [1.8 MB]
Creating vector for inf... [1.8 MB]
Allocating iteration vectors... [2 x 1.8 MB]
TOTAL: [19.5 MB]

Starting iterations...

Iterative method: 91 iterations in 0.70 seconds (average 0.003429, setup 0.38)

Value in the initial state: 176.75031005393546

Time for model checking: 0.741 seconds.

Result: 176.75031005393546 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"time"}min=? [ F "deadlock" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=1,Objy1=1,Objx2=6,Objy2=1

Prob0A: 4 iterations in 0.02 seconds (average 0.006000, setup 0.00)

Prob1E: 5 iterations in 0.05 seconds (average 0.009600, setup 0.00)

Warning: PRISM hasn't checked for zero-reward loops.
Your minimum rewards may be too low...

goal = 1, inf = 0, maybe = 229701

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=229702, nc=305313, nnz=619689, k=4] [9.1 MB]
Building sparse matrix (transition rewards)... [n=229702, nc=305313, nnz=115075, k=4] [3.4 MB]
Creating vector for state rewards... [1.8 MB]
Creating vector for inf... [1.8 MB]
Allocating iteration vectors... [2 x 1.8 MB]
TOTAL: [19.5 MB]

Starting iterations...

Iterative method: 97 iterations in 0.70 seconds (average 0.003505, setup 0.36)

Value in the initial state: 97.88657236348597

Time for model checking: 0.787 seconds.

Result: 97.88657236348597 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"success"}max=? [ F "deadlock" ]/Pmin=? [ F "MissionSuccessful" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=1,Objy1=1,Objx2=6,Objy2=1

Prob0E: 4 iterations in 0.02 seconds (average 0.006000, setup 0.00)

Prob1A: 1 iterations in 0.00 seconds (average 0.004000, setup 0.00)

goal = 1, inf = 0, maybe = 229701

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=229702, nc=305313, nnz=619689, k=4] [9.1 MB]
Building sparse matrix (transition rewards)... [n=229702, nc=305313, nnz=1866, k=4] [2.1 MB]
Creating vector for state rewards... [1.8 MB]
Creating vector for inf... [1.8 MB]
Allocating iteration vectors... [2 x 1.8 MB]
TOTAL: [18.2 MB]

Starting iterations...

Iterative method: 93 iterations in 0.64 seconds (average 0.003355, setup 0.33)

Prob0E: 47 iterations in 0.27 seconds (average 0.005787, setup 0.00)

Prob1A: 3 iterations in 0.04 seconds (average 0.013333, setup 0.00)

yes = 947, no = 199727, maybe = 29028

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=229702, nc=51541, nnz=149522, k=2] [2.0 MB]
Creating vector for yes... [1.8 MB]
Allocating iteration vectors... [2 x 1.8 MB]
TOTAL: [7.2 MB]

Starting iterations...

Iterative method: 86 iterations in 0.28 seconds (average 0.001442, setup 0.16)

Value in the initial state: 237.45551542651415

Time for model checking: 1.304 seconds.

Result: 237.45551542651415 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"success"}min=? [ F "deadlock" ]/Pmax=? [ F "MissionSuccessful" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=1,Objy1=1,Objx2=6,Objy2=1

Prob0A: 4 iterations in 0.07 seconds (average 0.018000, setup 0.00)

Prob1E: 5 iterations in 0.04 seconds (average 0.008800, setup 0.00)

Warning: PRISM hasn't checked for zero-reward loops.
Your minimum rewards may be too low...

goal = 1, inf = 0, maybe = 229701

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=229702, nc=305313, nnz=619689, k=4] [9.1 MB]
Building sparse matrix (transition rewards)... [n=229702, nc=305313, nnz=1866, k=4] [2.1 MB]
Creating vector for state rewards... [1.8 MB]
Creating vector for inf... [1.8 MB]
Allocating iteration vectors... [2 x 1.8 MB]
TOTAL: [18.2 MB]

Starting iterations...

Iterative method: 88 iterations in 0.64 seconds (average 0.003273, setup 0.36)

Prob0A: 45 iterations in 0.22 seconds (average 0.004800, setup 0.00)

Prob1E: 4 iterations in 0.02 seconds (average 0.006000, setup 0.00)

yes = 947, no = 193819, maybe = 34936

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=229702, nc=62352, nnz=181511, k=2] [2.4 MB]
Creating vector for yes... [1.8 MB]
Allocating iteration vectors... [2 x 1.8 MB]
TOTAL: [7.6 MB]

Starting iterations...

Iterative method: 91 iterations in 0.26 seconds (average 0.001407, setup 0.14)

Value in the initial state: 79.7349088494252

Time for model checking: 1.265 seconds.

Result: 79.7349088494252 (value in the initial state)

---------------------------------------------------------------------

Note: There were 16 warnings during computation.

