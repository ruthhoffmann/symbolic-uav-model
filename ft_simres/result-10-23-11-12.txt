PRISM
=====

Version: 4.3
Date: Tue Aug 02 12:36:49 BST 2016
Hostname: eglinton
Memory limits: cudd=1g, java(heap)=981.5m
Command line: prism uav_sim_ft_mdp.prism -s uav_mdp.props -const 'Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=1,Objy1=1,Objx2=1,Objy2=2,'

Parsing model file "uav_sim_ft_mdp.prism"...

Parsing properties file "uav_mdp.props"...

18 properties:
(1) Pmax=? [ F "MissionSuccessful" ]
(2) Pmin=? [ F "MissionSuccessful" ]
(3) Pmax=? [ F "MissionTimeUp" ]
(4) Pmin=? [ F "MissionTimeUp" ]
(5) Pmax=? [ F "Crash" ]
(6) Pmin=? [ F "Crash" ]
(7) Pmax=? [ F "AllAreaSearched" ]
(8) Pmin=? [ F "AllAreaSearched" ]
(9) Pmax=? [ F "MissionTimeUp"|"AllAreaSearched"|"Crash" ]
(10) Pmin=? [ F "MissionTimeUp"|"AllAreaSearched"|"Crash" ]
(11) R{"drop"}max=? [ F "deadlock" ]
(12) R{"drop"}min=? [ F "deadlock" ]
(13) R{"charge"}max=? [ F "deadlock" ]
(14) R{"charge"}min=? [ F "deadlock" ]
(15) R{"time"}max=? [ F "deadlock" ]
(16) R{"time"}min=? [ F "deadlock" ]
(17) R{"success"}max=? [ F "deadlock" ]/Pmin=? [ F "MissionSuccessful" ]
(18) R{"success"}min=? [ F "deadlock" ]/Pmax=? [ F "MissionSuccessful" ]

Type:        MDP
Modules:     Obj Move Robot T 
Variables:   NoOfObjs posx posy s1 retx rety b t c 

---------------------------------------------------------------------

Model checking: Pmax=? [ F "MissionSuccessful" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=1,Objy1=1,Objx2=1,Objy2=2

Building model...
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=1,Objy1=1,Objx2=1,Objy2=2

Warning: Guard for command 7 of module "Move" is never satisfied.

Warning: Guard for command 8 of module "Move" is never satisfied.

Warning: Guard for command 9 of module "Move" is never satisfied.

Warning: Guard for command 7 of module "Robot" is never satisfied.

Warning: Update 2 of command 3 of module "Robot" doesn't do anything ("(s1'=13)", line 249, column 44)

Warning: Update 2 of command 27 of module "T" doesn't do anything ("(c'=1)", line 372, column 108)

Warning: Guard for command 28 of module "T" is never satisfied.

Warning: Update 2 of command 9 of module "T" doesn't do anything ("(c'=1)", line 349, column 112)

Warning: Guard for command 10 of module "T" is never satisfied.

Warning: Update 2 of command 51 of module "T" doesn't do anything ("(c'=1)", line 403, column 113)

Warning: Guard for command 52 of module "T" is never satisfied.

Computing reachable states...

Reachability (BFS): 79 iterations in 0.75 seconds (average 0.009468, setup 0.00)

Time for model construction: 2.281 seconds.

Warning: Deadlocks detected and fixed in 1 states

Type:        MDP
States:      167324 (1 initial)
Transitions: 438798
Choices:     221515

Transition matrix: 162648 nodes (76 terminal), 438798 minterms, vars: 34r/34c/18nd

Prob0A: 43 iterations in 0.30 seconds (average 0.007070, setup 0.00)

Prob1E: 4 iterations in 0.05 seconds (average 0.013000, setup 0.00)

yes = 963, no = 139931, maybe = 26430

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=167324, nc=47583, nnz=134732, k=2] [1.7 MB]
Creating vector for yes... [1.3 MB]
Allocating iteration vectors... [2 x 1.3 MB]
TOTAL: [5.6 MB]

Starting iterations...

Iterative method: 84 iterations in 0.28 seconds (average 0.001143, setup 0.19)

Value in the initial state: 0.9813513650079588

Time for model checking: 0.677 seconds.

Result: 0.9813513650079588 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmin=? [ F "MissionSuccessful" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=1,Objy1=1,Objx2=1,Objy2=2

Prob0E: 43 iterations in 0.36 seconds (average 0.008279, setup 0.00)

Prob1A: 3 iterations in 0.05 seconds (average 0.016000, setup 0.00)

yes = 963, no = 143940, maybe = 22421

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=167324, nc=40227, nnz=113282, k=2] [1.5 MB]
Creating vector for yes... [1.3 MB]
Allocating iteration vectors... [2 x 1.3 MB]
TOTAL: [5.3 MB]

Starting iterations...

Iterative method: 82 iterations in 0.25 seconds (average 0.001073, setup 0.16)

Value in the initial state: 0.7696426112239265

Time for model checking: 0.79 seconds.

Result: 0.7696426112239265 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmax=? [ F "MissionTimeUp" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=1,Objy1=1,Objx2=1,Objy2=2

Prob0A: 63 iterations in 0.48 seconds (average 0.007683, setup 0.00)

Prob1E: 4 iterations in 0.04 seconds (average 0.010000, setup 0.00)

yes = 308, no = 108179, maybe = 58837

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=167324, nc=103224, nnz=284796, k=4] [3.5 MB]
Creating vector for yes... [1.3 MB]
Allocating iteration vectors... [2 x 1.3 MB]
TOTAL: [7.3 MB]

Starting iterations...

Iterative method: 89 iterations in 0.35 seconds (average 0.001438, setup 0.22)

Value in the initial state: 2.4530583549930003E-4

Time for model checking: 0.962 seconds.

Result: 2.4530583549930003E-4 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmin=? [ F "MissionTimeUp" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=1,Objy1=1,Objx2=1,Objy2=2

Prob0E: 71 iterations in 0.56 seconds (average 0.007831, setup 0.00)

Prob1A: 3 iterations in 0.06 seconds (average 0.018667, setup 0.00)

yes = 308, no = 108498, maybe = 58518

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=167324, nc=102662, nnz=283565, k=4] [3.5 MB]
Creating vector for yes... [1.3 MB]
Allocating iteration vectors... [2 x 1.3 MB]
TOTAL: [7.3 MB]

Starting iterations...

Iterative method: 103 iterations in 0.36 seconds (average 0.001437, setup 0.22)

Value in the initial state: 8.128406147565196E-7

Time for model checking: 1.094 seconds.

Result: 8.128406147565196E-7 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmax=? [ F "Crash" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=1,Objy1=1,Objx2=1,Objy2=2

Prob0A: 3 iterations in 0.08 seconds (average 0.025333, setup 0.00)

Prob1E: 164 iterations in 2.24 seconds (average 0.013683, setup 0.00)

yes = 82031, no = 2512, maybe = 82781

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=167324, nc=136972, nnz=354255, k=4] [4.3 MB]
Creating vector for yes... [1.3 MB]
Allocating iteration vectors... [2 x 1.3 MB]
TOTAL: [8.2 MB]

Starting iterations...

Iterative method: 84 iterations in 0.40 seconds (average 0.001571, setup 0.26)

Value in the initial state: 0.029304747782892455

Time for model checking: 3.21 seconds.

Result: 0.029304747782892455 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmin=? [ F "Crash" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=1,Objy1=1,Objx2=1,Objy2=2

Prob0E: 3 iterations in 0.05 seconds (average 0.016000, setup 0.00)

Prob1A: 43 iterations in 0.32 seconds (average 0.007535, setup 0.00)

yes = 82031, no = 2512, maybe = 82781

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=167324, nc=136972, nnz=354255, k=4] [4.3 MB]
Creating vector for yes... [1.3 MB]
Allocating iteration vectors... [2 x 1.3 MB]
TOTAL: [8.2 MB]

Starting iterations...

Iterative method: 78 iterations in 0.29 seconds (average 0.001590, setup 0.17)

Value in the initial state: 0.01655712041879776

Time for model checking: 0.702 seconds.

Result: 0.01655712041879776 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmax=? [ F "AllAreaSearched" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=1,Objy1=1,Objx2=1,Objy2=2

Prob0A: 49 iterations in 0.31 seconds (average 0.006367, setup 0.00)

Prob1E: 4 iterations in 0.04 seconds (average 0.009000, setup 0.00)

yes = 1240, no = 127758, maybe = 38326

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=167324, nc=58421, nnz=149628, k=2] [1.9 MB]
Creating vector for yes... [1.3 MB]
Allocating iteration vectors... [2 x 1.3 MB]
TOTAL: [5.8 MB]

Starting iterations...

Iterative method: 83 iterations in 0.26 seconds (average 0.001205, setup 0.16)

Value in the initial state: 0.20347653993265827

Time for model checking: 0.719 seconds.

Result: 0.20347653993265827 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmin=? [ F "AllAreaSearched" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=1,Objy1=1,Objx2=1,Objy2=2

Prob0E: 61 iterations in 0.38 seconds (average 0.006295, setup 0.00)

Prob1A: 3 iterations in 0.03 seconds (average 0.009333, setup 0.00)

yes = 1240, no = 131773, maybe = 34311

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=167324, nc=51223, nnz=128510, k=2] [1.7 MB]
Creating vector for yes... [1.3 MB]
Allocating iteration vectors... [2 x 1.3 MB]
TOTAL: [5.5 MB]

Starting iterations...

Iterative method: 89 iterations in 0.20 seconds (average 0.001213, setup 0.10)

Value in the initial state: 0.0017691257632456788

Time for model checking: 0.696 seconds.

Result: 0.0017691257632456788 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmax=? [ F "MissionTimeUp"|"AllAreaSearched"|"Crash" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=1,Objy1=1,Objx2=1,Objy2=2

Prob0A: 3 iterations in 0.06 seconds (average 0.021333, setup 0.00)

Prob1E: 168 iterations in 1.96 seconds (average 0.011643, setup 0.00)

yes = 143939, no = 964, maybe = 22421

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=167324, nc=40227, nnz=113282, k=2] [1.5 MB]
Creating vector for yes... [1.3 MB]
Allocating iteration vectors... [2 x 1.3 MB]
TOTAL: [5.3 MB]

Starting iterations...

Iterative method: 73 iterations in 0.25 seconds (average 0.001096, setup 0.17)

Value in the initial state: 0.23035706013862162

Time for model checking: 2.384 seconds.

Result: 0.23035706013862162 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmin=? [ F "MissionTimeUp"|"AllAreaSearched"|"Crash" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=1,Objy1=1,Objx2=1,Objy2=2

Prob0E: 3 iterations in 0.04 seconds (average 0.012000, setup 0.00)

Prob1A: 43 iterations in 0.36 seconds (average 0.008465, setup 0.00)

yes = 139930, no = 964, maybe = 26430

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=167324, nc=47583, nnz=134732, k=2] [1.7 MB]
Creating vector for yes... [1.3 MB]
Allocating iteration vectors... [2 x 1.3 MB]
TOTAL: [5.6 MB]

Starting iterations...

Iterative method: 83 iterations in 0.22 seconds (average 0.001157, setup 0.13)

Value in the initial state: 0.018647354676133026

Time for model checking: 0.725 seconds.

Result: 0.018647354676133026 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"drop"}max=? [ F "deadlock" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=1,Objy1=1,Objx2=1,Objy2=2

Prob0E: 4 iterations in 0.05 seconds (average 0.013000, setup 0.00)

Prob1A: 1 iterations in 0.01 seconds (average 0.008000, setup 0.00)

goal = 1, inf = 0, maybe = 167323

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=167324, nc=221514, nnz=438797, k=4] [6.5 MB]
Building sparse matrix (transition rewards)... [n=167324, nc=221514, nnz=0, k=4] [1.5 MB]
Creating vector for state rewards... [1.3 MB]
Creating vector for inf... [1.3 MB]
Allocating iteration vectors... [2 x 1.3 MB]
TOTAL: [13.1 MB]

Starting iterations...

Iterative method: 81 iterations in 0.52 seconds (average 0.002420, setup 0.32)

Value in the initial state: 0.14780939073585858

Time for model checking: 0.698 seconds.

Result: 0.14780939073585858 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"drop"}min=? [ F "deadlock" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=1,Objy1=1,Objx2=1,Objy2=2

Prob0A: 4 iterations in 0.03 seconds (average 0.008000, setup 0.00)

Prob1E: 5 iterations in 0.04 seconds (average 0.008000, setup 0.00)

Warning: PRISM hasn't checked for zero-reward loops.
Your minimum rewards may be too low...

goal = 1, inf = 0, maybe = 167323

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=167324, nc=221514, nnz=438797, k=4] [6.5 MB]
Building sparse matrix (transition rewards)... [n=167324, nc=221514, nnz=0, k=4] [1.5 MB]
Creating vector for state rewards... [1.3 MB]
Creating vector for inf... [1.3 MB]
Allocating iteration vectors... [2 x 1.3 MB]
TOTAL: [13.1 MB]

Starting iterations...

Iterative method: 73 iterations in 0.42 seconds (average 0.002466, setup 0.24)

Value in the initial state: 0.135194476205013

Time for model checking: 0.591 seconds.

Result: 0.135194476205013 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"charge"}max=? [ F "deadlock" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=1,Objy1=1,Objx2=1,Objy2=2

Prob0E: 4 iterations in 0.02 seconds (average 0.006000, setup 0.00)

Prob1A: 1 iterations in 0.00 seconds (average 0.004000, setup 0.00)

goal = 1, inf = 0, maybe = 167323

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=167324, nc=221514, nnz=438797, k=4] [6.5 MB]
Building sparse matrix (transition rewards)... [n=167324, nc=221514, nnz=4730, k=4] [1.5 MB]
Creating vector for state rewards... [1.3 MB]
Creating vector for inf... [1.3 MB]
Allocating iteration vectors... [2 x 1.3 MB]
TOTAL: [13.1 MB]

Starting iterations...

Iterative method: 78 iterations in 0.44 seconds (average 0.002462, setup 0.24)

Value in the initial state: 0.25425859812631674

Time for model checking: 0.551 seconds.

Result: 0.25425859812631674 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"charge"}min=? [ F "deadlock" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=1,Objy1=1,Objx2=1,Objy2=2

Prob0A: 4 iterations in 0.02 seconds (average 0.005000, setup 0.00)

Prob1E: 5 iterations in 0.04 seconds (average 0.008800, setup 0.00)

Warning: PRISM hasn't checked for zero-reward loops.
Your minimum rewards may be too low...

goal = 1, inf = 0, maybe = 167323

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=167324, nc=221514, nnz=438797, k=4] [6.5 MB]
Building sparse matrix (transition rewards)... [n=167324, nc=221514, nnz=4730, k=4] [1.5 MB]
Creating vector for state rewards... [1.3 MB]
Creating vector for inf... [1.3 MB]
Allocating iteration vectors... [2 x 1.3 MB]
TOTAL: [13.1 MB]

Starting iterations...

Iterative method: 86 iterations in 0.46 seconds (average 0.002465, setup 0.25)

Value in the initial state: 0.03852798979938022

Time for model checking: 0.593 seconds.

Result: 0.03852798979938022 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"time"}max=? [ F "deadlock" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=1,Objy1=1,Objx2=1,Objy2=2

Prob0E: 4 iterations in 0.02 seconds (average 0.006000, setup 0.00)

Prob1A: 1 iterations in 0.00 seconds (average 0.004000, setup 0.00)

goal = 1, inf = 0, maybe = 167323

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=167324, nc=221514, nnz=438797, k=4] [6.5 MB]
Building sparse matrix (transition rewards)... [n=167324, nc=221514, nnz=84541, k=4] [2.5 MB]
Creating vector for state rewards... [1.3 MB]
Creating vector for inf... [1.3 MB]
Allocating iteration vectors... [2 x 1.3 MB]
TOTAL: [14.1 MB]

Starting iterations...

Iterative method: 87 iterations in 0.49 seconds (average 0.002529, setup 0.27)

Value in the initial state: 161.75667896038846

Time for model checking: 0.567 seconds.

Result: 161.75667896038846 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"time"}min=? [ F "deadlock" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=1,Objy1=1,Objx2=1,Objy2=2

Prob0A: 4 iterations in 0.02 seconds (average 0.006000, setup 0.00)

Prob1E: 5 iterations in 0.04 seconds (average 0.008800, setup 0.00)

Warning: PRISM hasn't checked for zero-reward loops.
Your minimum rewards may be too low...

goal = 1, inf = 0, maybe = 167323

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=167324, nc=221514, nnz=438797, k=4] [6.5 MB]
Building sparse matrix (transition rewards)... [n=167324, nc=221514, nnz=84541, k=4] [2.5 MB]
Creating vector for state rewards... [1.3 MB]
Creating vector for inf... [1.3 MB]
Allocating iteration vectors... [2 x 1.3 MB]
TOTAL: [14.1 MB]

Starting iterations...

Iterative method: 90 iterations in 0.49 seconds (average 0.002400, setup 0.27)

Value in the initial state: 91.77821152247563

Time for model checking: 0.647 seconds.

Result: 91.77821152247563 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"success"}max=? [ F "deadlock" ]/Pmin=? [ F "MissionSuccessful" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=1,Objy1=1,Objx2=1,Objy2=2

Prob0E: 4 iterations in 0.02 seconds (average 0.006000, setup 0.00)

Prob1A: 1 iterations in 0.01 seconds (average 0.008000, setup 0.00)

goal = 1, inf = 0, maybe = 167323

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=167324, nc=221514, nnz=438797, k=4] [6.5 MB]
Building sparse matrix (transition rewards)... [n=167324, nc=221514, nnz=1898, k=4] [1.5 MB]
Creating vector for state rewards... [1.3 MB]
Creating vector for inf... [1.3 MB]
Allocating iteration vectors... [2 x 1.3 MB]
TOTAL: [13.1 MB]

Starting iterations...

Iterative method: 88 iterations in 0.46 seconds (average 0.002409, setup 0.25)

Prob0E: 43 iterations in 0.36 seconds (average 0.008372, setup 0.00)

Prob1A: 3 iterations in 0.04 seconds (average 0.012000, setup 0.00)

yes = 963, no = 143940, maybe = 22421

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=167324, nc=40227, nnz=113282, k=2] [1.5 MB]
Creating vector for yes... [1.3 MB]
Allocating iteration vectors... [2 x 1.3 MB]
TOTAL: [5.3 MB]

Starting iterations...

Iterative method: 82 iterations in 0.22 seconds (average 0.001073, setup 0.14)

Value in the initial state: 206.11986841700923

Time for model checking: 1.222 seconds.

Result: 206.11986841700923 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"success"}min=? [ F "deadlock" ]/Pmax=? [ F "MissionSuccessful" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=1,Objy1=1,Objx2=1,Objy2=2

Prob0A: 4 iterations in 0.08 seconds (average 0.021000, setup 0.00)

Prob1E: 5 iterations in 0.06 seconds (average 0.011200, setup 0.00)

Warning: PRISM hasn't checked for zero-reward loops.
Your minimum rewards may be too low...

goal = 1, inf = 0, maybe = 167323

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=167324, nc=221514, nnz=438797, k=4] [6.5 MB]
Building sparse matrix (transition rewards)... [n=167324, nc=221514, nnz=1898, k=4] [1.5 MB]
Creating vector for state rewards... [1.3 MB]
Creating vector for inf... [1.3 MB]
Allocating iteration vectors... [2 x 1.3 MB]
TOTAL: [13.1 MB]

Starting iterations...

Iterative method: 84 iterations in 0.49 seconds (average 0.002381, setup 0.29)

Prob0A: 43 iterations in 0.21 seconds (average 0.004837, setup 0.00)

Prob1E: 4 iterations in 0.03 seconds (average 0.007000, setup 0.00)

yes = 963, no = 139931, maybe = 26430

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=167324, nc=47583, nnz=134732, k=2] [1.7 MB]
Creating vector for yes... [1.3 MB]
Allocating iteration vectors... [2 x 1.3 MB]
TOTAL: [5.6 MB]

Starting iterations...

Iterative method: 84 iterations in 0.24 seconds (average 0.001143, setup 0.15)

Value in the initial state: 79.1027664794552

Time for model checking: 1.195 seconds.

Result: 79.1027664794552 (value in the initial state)

---------------------------------------------------------------------

Note: There were 16 warnings during computation.

