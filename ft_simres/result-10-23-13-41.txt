PRISM
=====

Version: 4.3
Date: Tue Aug 02 12:37:42 BST 2016
Hostname: eglinton
Memory limits: cudd=1g, java(heap)=981.5m
Command line: prism uav_sim_ft_mdp.prism -s uav_mdp.props -const 'Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=1,Objy1=3,Objx2=4,Objy2=1,'

Parsing model file "uav_sim_ft_mdp.prism"...

Parsing properties file "uav_mdp.props"...

18 properties:
(1) Pmax=? [ F "MissionSuccessful" ]
(2) Pmin=? [ F "MissionSuccessful" ]
(3) Pmax=? [ F "MissionTimeUp" ]
(4) Pmin=? [ F "MissionTimeUp" ]
(5) Pmax=? [ F "Crash" ]
(6) Pmin=? [ F "Crash" ]
(7) Pmax=? [ F "AllAreaSearched" ]
(8) Pmin=? [ F "AllAreaSearched" ]
(9) Pmax=? [ F "MissionTimeUp"|"AllAreaSearched"|"Crash" ]
(10) Pmin=? [ F "MissionTimeUp"|"AllAreaSearched"|"Crash" ]
(11) R{"drop"}max=? [ F "deadlock" ]
(12) R{"drop"}min=? [ F "deadlock" ]
(13) R{"charge"}max=? [ F "deadlock" ]
(14) R{"charge"}min=? [ F "deadlock" ]
(15) R{"time"}max=? [ F "deadlock" ]
(16) R{"time"}min=? [ F "deadlock" ]
(17) R{"success"}max=? [ F "deadlock" ]/Pmin=? [ F "MissionSuccessful" ]
(18) R{"success"}min=? [ F "deadlock" ]/Pmax=? [ F "MissionSuccessful" ]

Type:        MDP
Modules:     Obj Move Robot T 
Variables:   NoOfObjs posx posy s1 retx rety b t c 

---------------------------------------------------------------------

Model checking: Pmax=? [ F "MissionSuccessful" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=1,Objy1=3,Objx2=4,Objy2=1

Building model...
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=1,Objy1=3,Objx2=4,Objy2=1

Warning: Guard for command 7 of module "Move" is never satisfied.

Warning: Guard for command 8 of module "Move" is never satisfied.

Warning: Guard for command 9 of module "Move" is never satisfied.

Warning: Guard for command 7 of module "Robot" is never satisfied.

Warning: Update 2 of command 3 of module "Robot" doesn't do anything ("(s1'=13)", line 249, column 44)

Warning: Update 2 of command 27 of module "T" doesn't do anything ("(c'=1)", line 372, column 108)

Warning: Guard for command 28 of module "T" is never satisfied.

Warning: Update 2 of command 9 of module "T" doesn't do anything ("(c'=1)", line 349, column 112)

Warning: Guard for command 10 of module "T" is never satisfied.

Warning: Update 2 of command 51 of module "T" doesn't do anything ("(c'=1)", line 403, column 113)

Warning: Guard for command 52 of module "T" is never satisfied.

Computing reachable states...

Reachability (BFS): 82 iterations in 0.91 seconds (average 0.011073, setup 0.00)

Time for model construction: 2.081 seconds.

Warning: Deadlocks detected and fixed in 1 states

Type:        MDP
States:      175067 (1 initial)
Transitions: 451026
Choices:     229567

Transition matrix: 167619 nodes (76 terminal), 451026 minterms, vars: 34r/34c/18nd

Prob0A: 54 iterations in 0.37 seconds (average 0.006815, setup 0.00)

Prob1E: 4 iterations in 0.05 seconds (average 0.012000, setup 0.00)

yes = 952, no = 146900, maybe = 27215

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=175067, nc=45536, nnz=124223, k=2] [1.6 MB]
Creating vector for yes... [1.3 MB]
Allocating iteration vectors... [2 x 1.3 MB]
TOTAL: [5.6 MB]

Starting iterations...

Iterative method: 94 iterations in 0.29 seconds (average 0.001149, setup 0.18)

Value in the initial state: 0.9755108455312244

Time for model checking: 0.76 seconds.

Result: 0.9755108455312244 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmin=? [ F "MissionSuccessful" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=1,Objy1=3,Objx2=4,Objy2=1

Prob0E: 67 iterations in 0.48 seconds (average 0.007224, setup 0.00)

Prob1A: 3 iterations in 0.05 seconds (average 0.016000, setup 0.00)

yes = 952, no = 150921, maybe = 23194

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=175067, nc=38865, nnz=105073, k=2] [1.4 MB]
Creating vector for yes... [1.3 MB]
Allocating iteration vectors... [2 x 1.3 MB]
TOTAL: [5.4 MB]

Starting iterations...

Iterative method: 88 iterations in 0.27 seconds (average 0.001136, setup 0.17)

Value in the initial state: 0.9336965850395794

Time for model checking: 0.827 seconds.

Result: 0.9336965850395794 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmax=? [ F "MissionTimeUp" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=1,Objy1=3,Objx2=4,Objy2=1

Prob0A: 61 iterations in 0.58 seconds (average 0.009574, setup 0.00)

Prob1E: 4 iterations in 0.04 seconds (average 0.009000, setup 0.00)

yes = 344, no = 101545, maybe = 73178

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=175067, nc=116983, nnz=310957, k=4] [3.8 MB]
Creating vector for yes... [1.3 MB]
Allocating iteration vectors... [2 x 1.3 MB]
TOTAL: [7.8 MB]

Starting iterations...

Iterative method: 92 iterations in 0.37 seconds (average 0.001522, setup 0.23)

Value in the initial state: 7.81421888613304E-4

Time for model checking: 1.021 seconds.

Result: 7.81421888613304E-4 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmin=? [ F "MissionTimeUp" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=1,Objy1=3,Objx2=4,Objy2=1

Prob0E: 69 iterations in 0.66 seconds (average 0.009623, setup 0.00)

Prob1A: 3 iterations in 0.05 seconds (average 0.016000, setup 0.00)

yes = 344, no = 101622, maybe = 73101

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=175067, nc=116862, nnz=310594, k=4] [3.8 MB]
Creating vector for yes... [1.3 MB]
Allocating iteration vectors... [2 x 1.3 MB]
TOTAL: [7.8 MB]

Starting iterations...

Iterative method: 106 iterations in 0.39 seconds (average 0.001547, setup 0.23)

Value in the initial state: 4.450410829694974E-6

Time for model checking: 1.134 seconds.

Result: 4.450410829694974E-6 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmax=? [ F "Crash" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=1,Objy1=3,Objx2=4,Objy2=1

Prob0A: 3 iterations in 0.08 seconds (average 0.025333, setup 0.00)

Prob1E: 252 iterations in 2.80 seconds (average 0.011111, setup 0.00)

yes = 85906, no = 2599, maybe = 86562

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=175067, nc=141062, nnz=362521, k=4] [4.5 MB]
Creating vector for yes... [1.3 MB]
Allocating iteration vectors... [2 x 1.3 MB]
TOTAL: [8.5 MB]

Starting iterations...

Iterative method: 90 iterations in 0.42 seconds (average 0.001644, setup 0.27)

Value in the initial state: 0.06287492522186781

Time for model checking: 3.319 seconds.

Result: 0.06287492522186781 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmin=? [ F "Crash" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=1,Objy1=3,Objx2=4,Objy2=1

Prob0E: 3 iterations in 0.05 seconds (average 0.017333, setup 0.00)

Prob1A: 53 iterations in 0.45 seconds (average 0.008528, setup 0.00)

yes = 85906, no = 2599, maybe = 86562

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=175067, nc=141062, nnz=362521, k=4] [4.5 MB]
Creating vector for yes... [1.3 MB]
Allocating iteration vectors... [2 x 1.3 MB]
TOTAL: [8.5 MB]

Starting iterations...

Iterative method: 93 iterations in 0.35 seconds (average 0.001720, setup 0.19)

Value in the initial state: 0.022532024273337618

Time for model checking: 0.865 seconds.

Result: 0.022532024273337618 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmax=? [ F "AllAreaSearched" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=1,Objy1=3,Objx2=4,Objy2=1

Prob0A: 53 iterations in 0.37 seconds (average 0.007019, setup 0.00)

Prob1E: 4 iterations in 0.04 seconds (average 0.011000, setup 0.00)

yes = 1302, no = 129804, maybe = 43961

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=175067, nc=71038, nnz=188478, k=2] [2.4 MB]
Creating vector for yes... [1.3 MB]
Allocating iteration vectors... [2 x 1.3 MB]
TOTAL: [6.4 MB]

Starting iterations...

Iterative method: 91 iterations in 0.32 seconds (average 0.001319, setup 0.20)

Value in the initial state: 0.0030609129883736497

Time for model checking: 0.751 seconds.

Result: 0.0030609129883736497 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmin=? [ F "AllAreaSearched" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=1,Objy1=3,Objx2=4,Objy2=1

Prob0E: 67 iterations in 0.45 seconds (average 0.006687, setup 0.00)

Prob1A: 3 iterations in 0.04 seconds (average 0.012000, setup 0.00)

yes = 1302, no = 135450, maybe = 38315

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=175067, nc=61928, nnz=162700, k=2] [2.1 MB]
Creating vector for yes... [1.3 MB]
Allocating iteration vectors... [2 x 1.3 MB]
TOTAL: [6.1 MB]

Starting iterations...

Iterative method: 90 iterations in 0.26 seconds (average 0.001289, setup 0.15)

Value in the initial state: 0.0012350166529290496

Time for model checking: 0.761 seconds.

Result: 0.0012350166529290496 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmax=? [ F "MissionTimeUp"|"AllAreaSearched"|"Crash" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=1,Objy1=3,Objx2=4,Objy2=1

Prob0A: 3 iterations in 0.06 seconds (average 0.021333, setup 0.00)

Prob1E: 260 iterations in 2.81 seconds (average 0.010800, setup 0.00)

yes = 150920, no = 953, maybe = 23194

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=175067, nc=38865, nnz=105073, k=2] [1.4 MB]
Creating vector for yes... [1.3 MB]
Allocating iteration vectors... [2 x 1.3 MB]
TOTAL: [5.4 MB]

Starting iterations...

Iterative method: 85 iterations in 0.25 seconds (average 0.001129, setup 0.15)

Value in the initial state: 0.06630339667702517

Time for model checking: 3.148 seconds.

Result: 0.06630339667702517 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmin=? [ F "MissionTimeUp"|"AllAreaSearched"|"Crash" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=1,Objy1=3,Objx2=4,Objy2=1

Prob0E: 3 iterations in 0.04 seconds (average 0.014667, setup 0.00)

Prob1A: 54 iterations in 0.44 seconds (average 0.008074, setup 0.00)

yes = 146899, no = 953, maybe = 27215

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=175067, nc=45536, nnz=124223, k=2] [1.6 MB]
Creating vector for yes... [1.3 MB]
Allocating iteration vectors... [2 x 1.3 MB]
TOTAL: [5.6 MB]

Starting iterations...

Iterative method: 92 iterations in 0.25 seconds (average 0.001217, setup 0.14)

Value in the initial state: 0.02448831249354569

Time for model checking: 0.748 seconds.

Result: 0.02448831249354569 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"drop"}max=? [ F "deadlock" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=1,Objy1=3,Objx2=4,Objy2=1

Prob0E: 4 iterations in 0.06 seconds (average 0.015000, setup 0.00)

Prob1A: 1 iterations in 0.01 seconds (average 0.008000, setup 0.00)

goal = 1, inf = 0, maybe = 175066

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=175067, nc=229566, nnz=451025, k=4] [6.7 MB]
Building sparse matrix (transition rewards)... [n=175067, nc=229566, nnz=0, k=4] [1.5 MB]
Creating vector for state rewards... [1.3 MB]
Creating vector for inf... [1.3 MB]
Allocating iteration vectors... [2 x 1.3 MB]
TOTAL: [13.6 MB]

Starting iterations...

Iterative method: 90 iterations in 0.56 seconds (average 0.002489, setup 0.34)

Value in the initial state: 0.18015764096057593

Time for model checking: 0.65 seconds.

Result: 0.18015764096057593 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"drop"}min=? [ F "deadlock" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=1,Objy1=3,Objx2=4,Objy2=1

Prob0A: 4 iterations in 0.02 seconds (average 0.005000, setup 0.00)

Prob1E: 5 iterations in 0.04 seconds (average 0.007200, setup 0.00)

Warning: PRISM hasn't checked for zero-reward loops.
Your minimum rewards may be too low...

goal = 1, inf = 0, maybe = 175066

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=175067, nc=229566, nnz=451025, k=4] [6.7 MB]
Building sparse matrix (transition rewards)... [n=175067, nc=229566, nnz=0, k=4] [1.5 MB]
Creating vector for state rewards... [1.3 MB]
Creating vector for inf... [1.3 MB]
Allocating iteration vectors... [2 x 1.3 MB]
TOTAL: [13.6 MB]

Starting iterations...

Iterative method: 82 iterations in 0.46 seconds (average 0.002537, setup 0.25)

Value in the initial state: 0.13585375031927488

Time for model checking: 0.536 seconds.

Result: 0.13585375031927488 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"charge"}max=? [ F "deadlock" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=1,Objy1=3,Objx2=4,Objy2=1

Prob0E: 4 iterations in 0.02 seconds (average 0.006000, setup 0.00)

Prob1A: 1 iterations in 0.00 seconds (average 0.004000, setup 0.00)

goal = 1, inf = 0, maybe = 175066

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=175067, nc=229566, nnz=451025, k=4] [6.7 MB]
Building sparse matrix (transition rewards)... [n=175067, nc=229566, nnz=4730, k=4] [1.6 MB]
Creating vector for state rewards... [1.3 MB]
Creating vector for inf... [1.3 MB]
Allocating iteration vectors... [2 x 1.3 MB]
TOTAL: [13.6 MB]

Starting iterations...

Iterative method: 87 iterations in 0.47 seconds (average 0.002483, setup 0.25)

Value in the initial state: 0.9791979227261383

Time for model checking: 0.515 seconds.

Result: 0.9791979227261383 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"charge"}min=? [ F "deadlock" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=1,Objy1=3,Objx2=4,Objy2=1

Prob0A: 4 iterations in 0.02 seconds (average 0.005000, setup 0.00)

Prob1E: 5 iterations in 0.04 seconds (average 0.007200, setup 0.00)

Warning: PRISM hasn't checked for zero-reward loops.
Your minimum rewards may be too low...

goal = 1, inf = 0, maybe = 175066

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=175067, nc=229566, nnz=451025, k=4] [6.7 MB]
Building sparse matrix (transition rewards)... [n=175067, nc=229566, nnz=4730, k=4] [1.6 MB]
Creating vector for state rewards... [1.3 MB]
Creating vector for inf... [1.3 MB]
Allocating iteration vectors... [2 x 1.3 MB]
TOTAL: [13.6 MB]

Starting iterations...

Iterative method: 94 iterations in 0.49 seconds (average 0.002553, setup 0.25)

Value in the initial state: 0.12193807066247939

Time for model checking: 0.557 seconds.

Result: 0.12193807066247939 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"time"}max=? [ F "deadlock" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=1,Objy1=3,Objx2=4,Objy2=1

Prob0E: 4 iterations in 0.02 seconds (average 0.005000, setup 0.00)

Prob1A: 1 iterations in 0.00 seconds (average 0.004000, setup 0.00)

goal = 1, inf = 0, maybe = 175066

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=175067, nc=229566, nnz=451025, k=4] [6.7 MB]
Building sparse matrix (transition rewards)... [n=175067, nc=229566, nnz=88503, k=4] [2.6 MB]
Creating vector for state rewards... [1.3 MB]
Creating vector for inf... [1.3 MB]
Allocating iteration vectors... [2 x 1.3 MB]
TOTAL: [14.6 MB]

Starting iterations...

Iterative method: 92 iterations in 0.52 seconds (average 0.002565, setup 0.28)

Value in the initial state: 345.4484395501514

Time for model checking: 0.56 seconds.

Result: 345.4484395501514 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"time"}min=? [ F "deadlock" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=1,Objy1=3,Objx2=4,Objy2=1

Prob0A: 4 iterations in 0.02 seconds (average 0.005000, setup 0.00)

Prob1E: 5 iterations in 0.04 seconds (average 0.007200, setup 0.00)

Warning: PRISM hasn't checked for zero-reward loops.
Your minimum rewards may be too low...

goal = 1, inf = 0, maybe = 175066

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=175067, nc=229566, nnz=451025, k=4] [6.7 MB]
Building sparse matrix (transition rewards)... [n=175067, nc=229566, nnz=88503, k=4] [2.6 MB]
Creating vector for state rewards... [1.3 MB]
Creating vector for inf... [1.3 MB]
Allocating iteration vectors... [2 x 1.3 MB]
TOTAL: [14.6 MB]

Starting iterations...

Iterative method: 100 iterations in 0.54 seconds (average 0.002640, setup 0.27)

Value in the initial state: 124.6274818310102

Time for model checking: 0.607 seconds.

Result: 124.6274818310102 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"success"}max=? [ F "deadlock" ]/Pmin=? [ F "MissionSuccessful" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=1,Objy1=3,Objx2=4,Objy2=1

Prob0E: 4 iterations in 0.02 seconds (average 0.005000, setup 0.00)

Prob1A: 1 iterations in 0.00 seconds (average 0.004000, setup 0.00)

goal = 1, inf = 0, maybe = 175066

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=175067, nc=229566, nnz=451025, k=4] [6.7 MB]
Building sparse matrix (transition rewards)... [n=175067, nc=229566, nnz=1876, k=4] [1.6 MB]
Creating vector for state rewards... [1.3 MB]
Creating vector for inf... [1.3 MB]
Allocating iteration vectors... [2 x 1.3 MB]
TOTAL: [13.6 MB]

Starting iterations...

Iterative method: 95 iterations in 0.48 seconds (average 0.002484, setup 0.25)

Prob0E: 67 iterations in 0.38 seconds (average 0.005672, setup 0.00)

Prob1A: 3 iterations in 0.04 seconds (average 0.013333, setup 0.00)

yes = 952, no = 150921, maybe = 23194

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=175067, nc=38865, nnz=105073, k=2] [1.4 MB]
Creating vector for yes... [1.3 MB]
Allocating iteration vectors... [2 x 1.3 MB]
TOTAL: [5.4 MB]

Starting iterations...

Iterative method: 88 iterations in 0.22 seconds (average 0.001136, setup 0.12)

Value in the initial state: 361.57291484842926

Time for model checking: 1.177 seconds.

Result: 361.57291484842926 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"success"}min=? [ F "deadlock" ]/Pmax=? [ F "MissionSuccessful" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=1,Objy1=3,Objx2=4,Objy2=1

Prob0A: 4 iterations in 0.06 seconds (average 0.015000, setup 0.00)

Prob1E: 5 iterations in 0.06 seconds (average 0.012000, setup 0.00)

Warning: PRISM hasn't checked for zero-reward loops.
Your minimum rewards may be too low...

goal = 1, inf = 0, maybe = 175066

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=175067, nc=229566, nnz=451025, k=4] [6.7 MB]
Building sparse matrix (transition rewards)... [n=175067, nc=229566, nnz=1876, k=4] [1.6 MB]
Creating vector for state rewards... [1.3 MB]
Creating vector for inf... [1.3 MB]
Allocating iteration vectors... [2 x 1.3 MB]
TOTAL: [13.6 MB]

Starting iterations...

Iterative method: 93 iterations in 0.50 seconds (average 0.002538, setup 0.27)

Prob0A: 54 iterations in 0.32 seconds (average 0.005926, setup 0.00)

Prob1E: 4 iterations in 0.03 seconds (average 0.008000, setup 0.00)

yes = 952, no = 146900, maybe = 27215

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=175067, nc=45536, nnz=124223, k=2] [1.6 MB]
Creating vector for yes... [1.3 MB]
Allocating iteration vectors... [2 x 1.3 MB]
TOTAL: [5.6 MB]

Starting iterations...

Iterative method: 94 iterations in 0.22 seconds (average 0.001149, setup 0.11)

Value in the initial state: 125.82414000866666

Time for model checking: 1.185 seconds.

Result: 125.82414000866666 (value in the initial state)

---------------------------------------------------------------------

Note: There were 16 warnings during computation.

