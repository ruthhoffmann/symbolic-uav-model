PRISM
=====

Version: 4.3
Date: Tue Aug 02 12:33:34 BST 2016
Hostname: eglinton
Memory limits: cudd=1g, java(heap)=981.5m
Command line: prism uav_sim_ft_mdp.prism -s uav_mdp.props -const 'Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=6,Objy1=0,Objx2=6,Objy2=1,'

Parsing model file "uav_sim_ft_mdp.prism"...

Parsing properties file "uav_mdp.props"...

18 properties:
(1) Pmax=? [ F "MissionSuccessful" ]
(2) Pmin=? [ F "MissionSuccessful" ]
(3) Pmax=? [ F "MissionTimeUp" ]
(4) Pmin=? [ F "MissionTimeUp" ]
(5) Pmax=? [ F "Crash" ]
(6) Pmin=? [ F "Crash" ]
(7) Pmax=? [ F "AllAreaSearched" ]
(8) Pmin=? [ F "AllAreaSearched" ]
(9) Pmax=? [ F "MissionTimeUp"|"AllAreaSearched"|"Crash" ]
(10) Pmin=? [ F "MissionTimeUp"|"AllAreaSearched"|"Crash" ]
(11) R{"drop"}max=? [ F "deadlock" ]
(12) R{"drop"}min=? [ F "deadlock" ]
(13) R{"charge"}max=? [ F "deadlock" ]
(14) R{"charge"}min=? [ F "deadlock" ]
(15) R{"time"}max=? [ F "deadlock" ]
(16) R{"time"}min=? [ F "deadlock" ]
(17) R{"success"}max=? [ F "deadlock" ]/Pmin=? [ F "MissionSuccessful" ]
(18) R{"success"}min=? [ F "deadlock" ]/Pmax=? [ F "MissionSuccessful" ]

Type:        MDP
Modules:     Obj Move Robot T 
Variables:   NoOfObjs posx posy s1 retx rety b t c 

---------------------------------------------------------------------

Model checking: Pmax=? [ F "MissionSuccessful" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=6,Objy1=0,Objx2=6,Objy2=1

Building model...
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=6,Objy1=0,Objx2=6,Objy2=1

Warning: Guard for command 7 of module "Move" is never satisfied.

Warning: Guard for command 8 of module "Move" is never satisfied.

Warning: Guard for command 9 of module "Move" is never satisfied.

Warning: Guard for command 7 of module "Robot" is never satisfied.

Warning: Update 2 of command 3 of module "Robot" doesn't do anything ("(s1'=13)", line 249, column 44)

Warning: Update 2 of command 27 of module "T" doesn't do anything ("(c'=1)", line 372, column 108)

Warning: Guard for command 28 of module "T" is never satisfied.

Warning: Update 2 of command 9 of module "T" doesn't do anything ("(c'=1)", line 349, column 112)

Warning: Guard for command 10 of module "T" is never satisfied.

Warning: Update 2 of command 51 of module "T" doesn't do anything ("(c'=1)", line 403, column 113)

Warning: Guard for command 52 of module "T" is never satisfied.

Computing reachable states...

Reachability (BFS): 84 iterations in 0.73 seconds (average 0.008714, setup 0.00)

Time for model construction: 1.561 seconds.

Warning: Deadlocks detected and fixed in 1 states

Type:        MDP
States:      261925 (1 initial)
Transitions: 732975
Choices:     352129

Transition matrix: 174279 nodes (76 terminal), 732975 minterms, vars: 34r/34c/18nd

Prob0A: 43 iterations in 0.23 seconds (average 0.005395, setup 0.00)

Prob1E: 4 iterations in 0.04 seconds (average 0.011000, setup 0.00)

yes = 921, no = 220814, maybe = 40190

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=261925, nc=73258, nnz=219199, k=2] [2.8 MB]
Creating vector for yes... [2.0 MB]
Allocating iteration vectors... [2 x 2.0 MB]
TOTAL: [8.8 MB]

Starting iterations...

Iterative method: 91 iterations in 0.54 seconds (average 0.002154, setup 0.35)

Value in the initial state: 0.9816570163425646

Time for model checking: 0.884 seconds.

Result: 0.9816570163425646 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmin=? [ F "MissionSuccessful" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=6,Objy1=0,Objx2=6,Objy2=1

Prob0E: 45 iterations in 0.38 seconds (average 0.008356, setup 0.00)

Prob1A: 3 iterations in 0.05 seconds (average 0.016000, setup 0.00)

yes = 921, no = 227208, maybe = 33796

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=261925, nc=61265, nnz=183501, k=2] [2.4 MB]
Creating vector for yes... [2.0 MB]
Allocating iteration vectors... [2 x 2.0 MB]
TOTAL: [8.4 MB]

Starting iterations...

Iterative method: 82 iterations in 0.30 seconds (average 0.001756, setup 0.16)

Value in the initial state: 0.7708971236307325

Time for model checking: 0.768 seconds.

Result: 0.7708971236307325 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmax=? [ F "MissionTimeUp" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=6,Objy1=0,Objx2=6,Objy2=1

Prob0A: 59 iterations in 0.42 seconds (average 0.007119, setup 0.00)

Prob1E: 4 iterations in 0.06 seconds (average 0.014000, setup 0.00)

yes = 476, no = 158699, maybe = 102750

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=261925, nc=183623, nnz=525506, k=4] [6.4 MB]
Creating vector for yes... [2.0 MB]
Allocating iteration vectors... [2 x 2.0 MB]
TOTAL: [12.4 MB]

Starting iterations...

Iterative method: 95 iterations in 0.61 seconds (average 0.002358, setup 0.39)

Value in the initial state: 0.0019296285068528382

Time for model checking: 1.132 seconds.

Result: 0.0019296285068528382 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmin=? [ F "MissionTimeUp" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=6,Objy1=0,Objx2=6,Objy2=1

Prob0E: 66 iterations in 0.82 seconds (average 0.012485, setup 0.00)

Prob1A: 3 iterations in 0.06 seconds (average 0.020000, setup 0.00)

yes = 476, no = 159170, maybe = 102279

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=261925, nc=182757, nnz=523963, k=4] [6.4 MB]
Creating vector for yes... [2.0 MB]
Allocating iteration vectors... [2 x 2.0 MB]
TOTAL: [12.4 MB]

Starting iterations...

Iterative method: 108 iterations in 0.76 seconds (average 0.002704, setup 0.47)

Value in the initial state: 7.41604974765334E-6

Time for model checking: 1.676 seconds.

Result: 7.41604974765334E-6 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmax=? [ F "Crash" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=6,Objy1=0,Objx2=6,Objy2=1

Prob0A: 3 iterations in 0.07 seconds (average 0.022667, setup 0.00)

Prob1E: 170 iterations in 2.33 seconds (average 0.013694, setup 0.00)

yes = 127947, no = 2549, maybe = 131429

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=261925, nc=221633, nnz=602479, k=4] [7.4 MB]
Creating vector for yes... [2.0 MB]
Allocating iteration vectors... [2 x 2.0 MB]
TOTAL: [13.4 MB]

Starting iterations...

Iterative method: 93 iterations in 0.80 seconds (average 0.003312, setup 0.49)

Value in the initial state: 0.03353602539090461

Time for model checking: 3.239 seconds.

Result: 0.03353602539090461 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmin=? [ F "Crash" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=6,Objy1=0,Objx2=6,Objy2=1

Prob0E: 3 iterations in 0.10 seconds (average 0.032000, setup 0.00)

Prob1A: 40 iterations in 0.64 seconds (average 0.016100, setup 0.00)

yes = 127947, no = 2549, maybe = 131429

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=261925, nc=221633, nnz=602479, k=4] [7.4 MB]
Creating vector for yes... [2.0 MB]
Allocating iteration vectors... [2 x 2.0 MB]
TOTAL: [13.4 MB]

Starting iterations...

Iterative method: 91 iterations in 0.56 seconds (average 0.002549, setup 0.33)

Value in the initial state: 0.016236331953717105

Time for model checking: 1.339 seconds.

Result: 0.016236331953717105 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmax=? [ F "AllAreaSearched" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=6,Objy1=0,Objx2=6,Objy2=1

Prob0A: 60 iterations in 0.27 seconds (average 0.004533, setup 0.00)

Prob1E: 4 iterations in 0.03 seconds (average 0.008000, setup 0.00)

yes = 1151, no = 212001, maybe = 48773

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=261925, nc=73395, nnz=194262, k=2] [2.5 MB]
Creating vector for yes... [2.0 MB]
Allocating iteration vectors... [2 x 2.0 MB]
TOTAL: [8.5 MB]

Starting iterations...

Iterative method: 94 iterations in 0.29 seconds (average 0.001617, setup 0.14)

Value in the initial state: 0.2017561390979269

Time for model checking: 0.61 seconds.

Result: 0.2017561390979269 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmin=? [ F "AllAreaSearched" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=6,Objy1=0,Objx2=6,Objy2=1

Prob0E: 66 iterations in 0.27 seconds (average 0.004061, setup 0.00)

Prob1A: 3 iterations in 0.02 seconds (average 0.008000, setup 0.00)

yes = 1151, no = 218460, maybe = 42314

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=261925, nc=61573, nnz=159249, k=2] [2.1 MB]
Creating vector for yes... [2.0 MB]
Allocating iteration vectors... [2 x 2.0 MB]
TOTAL: [8.1 MB]

Starting iterations...

Iterative method: 98 iterations in 0.26 seconds (average 0.001592, setup 0.10)

Value in the initial state: 0.001701029703046316

Time for model checking: 0.551 seconds.

Result: 0.001701029703046316 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmax=? [ F "MissionTimeUp"|"AllAreaSearched"|"Crash" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=6,Objy1=0,Objx2=6,Objy2=1

Prob0A: 3 iterations in 0.08 seconds (average 0.028000, setup 0.00)

Prob1E: 176 iterations in 2.20 seconds (average 0.012477, setup 0.00)

yes = 227207, no = 922, maybe = 33796

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=261925, nc=61265, nnz=183501, k=2] [2.4 MB]
Creating vector for yes... [2.0 MB]
Allocating iteration vectors... [2 x 2.0 MB]
TOTAL: [8.4 MB]

Starting iterations...

Iterative method: 77 iterations in 0.28 seconds (average 0.001558, setup 0.16)

Value in the initial state: 0.22910254863109616

Time for model checking: 2.553 seconds.

Result: 0.22910254863109616 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmin=? [ F "MissionTimeUp"|"AllAreaSearched"|"Crash" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=6,Objy1=0,Objx2=6,Objy2=1

Prob0E: 3 iterations in 0.03 seconds (average 0.010667, setup 0.00)

Prob1A: 43 iterations in 0.23 seconds (average 0.005302, setup 0.00)

yes = 220813, no = 922, maybe = 40190

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=261925, nc=73258, nnz=219199, k=2] [2.8 MB]
Creating vector for yes... [2.0 MB]
Allocating iteration vectors... [2 x 2.0 MB]
TOTAL: [8.8 MB]

Starting iterations...

Iterative method: 88 iterations in 0.29 seconds (average 0.001636, setup 0.14)

Value in the initial state: 0.018342569375258938

Time for model checking: 0.57 seconds.

Result: 0.018342569375258938 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"drop"}max=? [ F "deadlock" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=6,Objy1=0,Objx2=6,Objy2=1

Prob0E: 4 iterations in 0.04 seconds (average 0.011000, setup 0.00)

Prob1A: 1 iterations in 0.00 seconds (average 0.004000, setup 0.00)

goal = 1, inf = 0, maybe = 261924

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=261925, nc=352128, nnz=732974, k=4] [10.7 MB]
Building sparse matrix (transition rewards)... [n=261925, nc=352128, nnz=0, k=4] [2.3 MB]
Creating vector for state rewards... [2.0 MB]
Creating vector for inf... [2.0 MB]
Allocating iteration vectors... [2 x 2.0 MB]
TOTAL: [21.1 MB]

Starting iterations...

Iterative method: 86 iterations in 0.94 seconds (average 0.004884, setup 0.52)

Value in the initial state: 0.312599786835098

Time for model checking: 1.021 seconds.

Result: 0.312599786835098 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"drop"}min=? [ F "deadlock" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=6,Objy1=0,Objx2=6,Objy2=1

Prob0A: 4 iterations in 0.06 seconds (average 0.014000, setup 0.00)

Prob1E: 5 iterations in 0.06 seconds (average 0.011200, setup 0.00)

Warning: PRISM hasn't checked for zero-reward loops.
Your minimum rewards may be too low...

goal = 1, inf = 0, maybe = 261924

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=261925, nc=352128, nnz=732974, k=4] [10.7 MB]
Building sparse matrix (transition rewards)... [n=261925, nc=352128, nnz=0, k=4] [2.3 MB]
Creating vector for state rewards... [2.0 MB]
Creating vector for inf... [2.0 MB]
Allocating iteration vectors... [2 x 2.0 MB]
TOTAL: [21.1 MB]

Starting iterations...

Iterative method: 75 iterations in 0.68 seconds (average 0.003893, setup 0.39)

Value in the initial state: 0.26307741517256805

Time for model checking: 0.814 seconds.

Result: 0.26307741517256805 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"charge"}max=? [ F "deadlock" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=6,Objy1=0,Objx2=6,Objy2=1

Prob0E: 4 iterations in 0.02 seconds (average 0.006000, setup 0.00)

Prob1A: 1 iterations in 0.00 seconds (average 0.004000, setup 0.00)

goal = 1, inf = 0, maybe = 261924

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=261925, nc=352128, nnz=732974, k=4] [10.7 MB]
Building sparse matrix (transition rewards)... [n=261925, nc=352128, nnz=9132, k=4] [2.4 MB]
Creating vector for state rewards... [2.0 MB]
Creating vector for inf... [2.0 MB]
Allocating iteration vectors... [2 x 2.0 MB]
TOTAL: [21.2 MB]

Starting iterations...

Iterative method: 82 iterations in 0.94 seconds (average 0.005561, setup 0.48)

Value in the initial state: 0.3481318091277871

Time for model checking: 0.981 seconds.

Result: 0.3481318091277871 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"charge"}min=? [ F "deadlock" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=6,Objy1=0,Objx2=6,Objy2=1

Prob0A: 4 iterations in 0.03 seconds (average 0.008000, setup 0.00)

Prob1E: 5 iterations in 0.05 seconds (average 0.009600, setup 0.00)

Warning: PRISM hasn't checked for zero-reward loops.
Your minimum rewards may be too low...

goal = 1, inf = 0, maybe = 261924

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=261925, nc=352128, nnz=732974, k=4] [10.7 MB]
Building sparse matrix (transition rewards)... [n=261925, nc=352128, nnz=9132, k=4] [2.4 MB]
Creating vector for state rewards... [2.0 MB]
Creating vector for inf... [2.0 MB]
Allocating iteration vectors... [2 x 2.0 MB]
TOTAL: [21.2 MB]

Starting iterations...

Iterative method: 93 iterations in 1.06 seconds (average 0.004817, setup 0.62)

Value in the initial state: 0.0394808220896906

Time for model checking: 1.165 seconds.

Result: 0.0394808220896906 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"time"}max=? [ F "deadlock" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=6,Objy1=0,Objx2=6,Objy2=1

Prob0E: 4 iterations in 0.04 seconds (average 0.011000, setup 0.00)

Prob1A: 1 iterations in 0.00 seconds (average 0.004000, setup 0.00)

goal = 1, inf = 0, maybe = 261924

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=261925, nc=352128, nnz=732974, k=4] [10.7 MB]
Building sparse matrix (transition rewards)... [n=261925, nc=352128, nnz=130494, k=4] [3.8 MB]
Creating vector for state rewards... [2.0 MB]
Creating vector for inf... [2.0 MB]
Allocating iteration vectors... [2 x 2.0 MB]
TOTAL: [22.6 MB]

Starting iterations...

Iterative method: 95 iterations in 1.15 seconds (average 0.004926, setup 0.68)

Value in the initial state: 184.70849199539768

Time for model checking: 1.23 seconds.

Result: 184.70849199539768 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"time"}min=? [ F "deadlock" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=6,Objy1=0,Objx2=6,Objy2=1

Prob0A: 4 iterations in 0.04 seconds (average 0.009000, setup 0.00)

Prob1E: 5 iterations in 0.05 seconds (average 0.010400, setup 0.00)

Warning: PRISM hasn't checked for zero-reward loops.
Your minimum rewards may be too low...

goal = 1, inf = 0, maybe = 261924

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=261925, nc=352128, nnz=732974, k=4] [10.7 MB]
Building sparse matrix (transition rewards)... [n=261925, nc=352128, nnz=130494, k=4] [3.8 MB]
Creating vector for state rewards... [2.0 MB]
Creating vector for inf... [2.0 MB]
Allocating iteration vectors... [2 x 2.0 MB]
TOTAL: [22.6 MB]

Starting iterations...

Iterative method: 98 iterations in 1.03 seconds (average 0.005388, setup 0.50)

Value in the initial state: 90.00891383246338

Time for model checking: 1.15 seconds.

Result: 90.00891383246338 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"success"}max=? [ F "deadlock" ]/Pmin=? [ F "MissionSuccessful" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=6,Objy1=0,Objx2=6,Objy2=1

Prob0E: 4 iterations in 0.04 seconds (average 0.011000, setup 0.00)

Prob1A: 1 iterations in 0.01 seconds (average 0.008000, setup 0.00)

goal = 1, inf = 0, maybe = 261924

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=261925, nc=352128, nnz=732974, k=4] [10.7 MB]
Building sparse matrix (transition rewards)... [n=261925, nc=352128, nnz=1817, k=4] [2.4 MB]
Creating vector for state rewards... [2.0 MB]
Creating vector for inf... [2.0 MB]
Allocating iteration vectors... [2 x 2.0 MB]
TOTAL: [21.1 MB]

Starting iterations...

Iterative method: 91 iterations in 1.09 seconds (average 0.004923, setup 0.64)

Prob0E: 45 iterations in 0.25 seconds (average 0.005511, setup 0.00)

Prob1A: 3 iterations in 0.04 seconds (average 0.012000, setup 0.00)

yes = 921, no = 227208, maybe = 33796

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=261925, nc=61265, nnz=183501, k=2] [2.4 MB]
Creating vector for yes... [2.0 MB]
Allocating iteration vectors... [2 x 2.0 MB]
TOTAL: [8.4 MB]

Starting iterations...

Iterative method: 82 iterations in 0.29 seconds (average 0.001756, setup 0.15)

Value in the initial state: 234.2219801979793

Time for model checking: 1.814 seconds.

Result: 234.2219801979793 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"success"}min=? [ F "deadlock" ]/Pmax=? [ F "MissionSuccessful" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=6,Objy1=0,Objx2=6,Objy2=1

Prob0A: 4 iterations in 0.06 seconds (average 0.014000, setup 0.00)

Prob1E: 5 iterations in 0.05 seconds (average 0.009600, setup 0.00)

Warning: PRISM hasn't checked for zero-reward loops.
Your minimum rewards may be too low...

goal = 1, inf = 0, maybe = 261924

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=261925, nc=352128, nnz=732974, k=4] [10.7 MB]
Building sparse matrix (transition rewards)... [n=261925, nc=352128, nnz=1817, k=4] [2.4 MB]
Creating vector for state rewards... [2.0 MB]
Creating vector for inf... [2.0 MB]
Allocating iteration vectors... [2 x 2.0 MB]
TOTAL: [21.1 MB]

Starting iterations...

Iterative method: 90 iterations in 0.76 seconds (average 0.003867, setup 0.41)

Prob0A: 43 iterations in 0.29 seconds (average 0.006698, setup 0.00)

Prob1E: 4 iterations in 0.03 seconds (average 0.008000, setup 0.00)

yes = 921, no = 220814, maybe = 40190

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=261925, nc=73258, nnz=219199, k=2] [2.8 MB]
Creating vector for yes... [2.0 MB]
Allocating iteration vectors... [2 x 2.0 MB]
TOTAL: [8.8 MB]

Starting iterations...

Iterative method: 91 iterations in 0.34 seconds (average 0.001890, setup 0.17)

Value in the initial state: 77.09546079939345

Time for model checking: 1.576 seconds.

Result: 77.09546079939345 (value in the initial state)

---------------------------------------------------------------------

Note: There were 16 warnings during computation.

