PRISM
=====

Version: 4.3
Date: Tue Aug 02 12:38:41 BST 2016
Hostname: eglinton
Memory limits: cudd=1g, java(heap)=981.5m
Command line: prism uav_sim_ft_mdp.prism -s uav_mdp.props -const 'Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=2,Objy1=1,Objx2=5,Objy2=1,'

Parsing model file "uav_sim_ft_mdp.prism"...

Parsing properties file "uav_mdp.props"...

18 properties:
(1) Pmax=? [ F "MissionSuccessful" ]
(2) Pmin=? [ F "MissionSuccessful" ]
(3) Pmax=? [ F "MissionTimeUp" ]
(4) Pmin=? [ F "MissionTimeUp" ]
(5) Pmax=? [ F "Crash" ]
(6) Pmin=? [ F "Crash" ]
(7) Pmax=? [ F "AllAreaSearched" ]
(8) Pmin=? [ F "AllAreaSearched" ]
(9) Pmax=? [ F "MissionTimeUp"|"AllAreaSearched"|"Crash" ]
(10) Pmin=? [ F "MissionTimeUp"|"AllAreaSearched"|"Crash" ]
(11) R{"drop"}max=? [ F "deadlock" ]
(12) R{"drop"}min=? [ F "deadlock" ]
(13) R{"charge"}max=? [ F "deadlock" ]
(14) R{"charge"}min=? [ F "deadlock" ]
(15) R{"time"}max=? [ F "deadlock" ]
(16) R{"time"}min=? [ F "deadlock" ]
(17) R{"success"}max=? [ F "deadlock" ]/Pmin=? [ F "MissionSuccessful" ]
(18) R{"success"}min=? [ F "deadlock" ]/Pmax=? [ F "MissionSuccessful" ]

Type:        MDP
Modules:     Obj Move Robot T 
Variables:   NoOfObjs posx posy s1 retx rety b t c 

---------------------------------------------------------------------

Model checking: Pmax=? [ F "MissionSuccessful" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=2,Objy1=1,Objx2=5,Objy2=1

Building model...
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=2,Objy1=1,Objx2=5,Objy2=1

Warning: Guard for command 7 of module "Move" is never satisfied.

Warning: Guard for command 8 of module "Move" is never satisfied.

Warning: Guard for command 9 of module "Move" is never satisfied.

Warning: Guard for command 7 of module "Robot" is never satisfied.

Warning: Update 2 of command 3 of module "Robot" doesn't do anything ("(s1'=13)", line 249, column 44)

Warning: Update 2 of command 27 of module "T" doesn't do anything ("(c'=1)", line 372, column 108)

Warning: Guard for command 28 of module "T" is never satisfied.

Warning: Update 2 of command 9 of module "T" doesn't do anything ("(c'=1)", line 349, column 112)

Warning: Guard for command 10 of module "T" is never satisfied.

Warning: Update 2 of command 51 of module "T" doesn't do anything ("(c'=1)", line 403, column 113)

Warning: Guard for command 52 of module "T" is never satisfied.

Computing reachable states...

Reachability (BFS): 81 iterations in 1.01 seconds (average 0.012494, setup 0.00)

Time for model construction: 2.257 seconds.

Warning: Deadlocks detected and fixed in 1 states

Type:        MDP
States:      212787 (1 initial)
Transitions: 569536
Choices:     282390

Transition matrix: 170856 nodes (76 terminal), 569536 minterms, vars: 34r/34c/18nd

Prob0A: 42 iterations in 0.37 seconds (average 0.008762, setup 0.00)

Prob1E: 4 iterations in 0.06 seconds (average 0.015000, setup 0.00)

yes = 952, no = 179336, maybe = 32499

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=212787, nc=58631, nnz=170179, k=2] [2.2 MB]
Creating vector for yes... [1.6 MB]
Allocating iteration vectors... [2 x 1.6 MB]
TOTAL: [7.1 MB]

Starting iterations...

Iterative method: 87 iterations in 0.35 seconds (average 0.001471, setup 0.22)

Value in the initial state: 0.9819788090946595

Time for model checking: 0.8 seconds.

Result: 0.9819788090946595 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmin=? [ F "MissionSuccessful" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=2,Objy1=1,Objx2=5,Objy2=1

Prob0E: 44 iterations in 0.46 seconds (average 0.010545, setup 0.00)

Prob1A: 3 iterations in 0.04 seconds (average 0.013333, setup 0.00)

yes = 952, no = 184444, maybe = 27391

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=212787, nc=49203, nnz=142522, k=2] [1.9 MB]
Creating vector for yes... [1.6 MB]
Allocating iteration vectors... [2 x 1.6 MB]
TOTAL: [6.8 MB]

Starting iterations...

Iterative method: 81 iterations in 0.26 seconds (average 0.001481, setup 0.14)

Value in the initial state: 0.7405761859432309

Time for model checking: 0.79 seconds.

Result: 0.7405761859432309 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmax=? [ F "MissionTimeUp" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=2,Objy1=1,Objx2=5,Objy2=1

Prob0A: 60 iterations in 0.42 seconds (average 0.007000, setup 0.00)

Prob1E: 4 iterations in 0.04 seconds (average 0.009000, setup 0.00)

yes = 428, no = 133794, maybe = 78565

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=212787, nc=138403, nnz=386809, k=4] [4.8 MB]
Creating vector for yes... [1.6 MB]
Allocating iteration vectors... [2 x 1.6 MB]
TOTAL: [9.6 MB]

Starting iterations...

Iterative method: 89 iterations in 0.41 seconds (average 0.001798, setup 0.25)

Value in the initial state: 8.372215122759382E-4

Time for model checking: 0.886 seconds.

Result: 8.372215122759382E-4 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmin=? [ F "MissionTimeUp" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=2,Objy1=1,Objx2=5,Objy2=1

Prob0E: 68 iterations in 0.64 seconds (average 0.009471, setup 0.00)

Prob1A: 3 iterations in 0.04 seconds (average 0.013333, setup 0.00)

yes = 428, no = 134170, maybe = 78189

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=212787, nc=137727, nnz=385461, k=4] [4.7 MB]
Creating vector for yes... [1.6 MB]
Allocating iteration vectors... [2 x 1.6 MB]
TOTAL: [9.6 MB]

Starting iterations...

Iterative method: 105 iterations in 0.42 seconds (average 0.001867, setup 0.22)

Value in the initial state: 1.9172540738018577E-6

Time for model checking: 1.123 seconds.

Result: 1.9172540738018577E-6 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmax=? [ F "Crash" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=2,Objy1=1,Objx2=5,Objy2=1

Prob0A: 3 iterations in 0.08 seconds (average 0.026667, setup 0.00)

Prob1E: 165 iterations in 2.46 seconds (average 0.014933, setup 0.00)

yes = 104167, no = 2612, maybe = 106008

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=212787, nc=175611, nnz=462757, k=4] [5.7 MB]
Creating vector for yes... [1.6 MB]
Allocating iteration vectors... [2 x 1.6 MB]
TOTAL: [10.5 MB]

Starting iterations...

Iterative method: 86 iterations in 0.48 seconds (average 0.002047, setup 0.30)

Value in the initial state: 0.030917874964134683

Time for model checking: 3.053 seconds.

Result: 0.030917874964134683 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmin=? [ F "Crash" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=2,Objy1=1,Objx2=5,Objy2=1

Prob0E: 3 iterations in 0.06 seconds (average 0.021333, setup 0.00)

Prob1A: 41 iterations in 0.48 seconds (average 0.011610, setup 0.00)

yes = 104167, no = 2612, maybe = 106008

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=212787, nc=175611, nnz=462757, k=4] [5.7 MB]
Creating vector for yes... [1.6 MB]
Allocating iteration vectors... [2 x 1.6 MB]
TOTAL: [10.5 MB]

Starting iterations...

Iterative method: 82 iterations in 0.40 seconds (average 0.002146, setup 0.22)

Value in the initial state: 0.015971121811675714

Time for model checking: 0.951 seconds.

Result: 0.015971121811675714 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmax=? [ F "AllAreaSearched" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=2,Objy1=1,Objx2=5,Objy2=1

Prob0A: 55 iterations in 0.40 seconds (average 0.007345, setup 0.00)

Prob1E: 4 iterations in 0.06 seconds (average 0.014000, setup 0.00)

yes = 1231, no = 167805, maybe = 43751

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=212787, nc=65395, nnz=168244, k=2] [2.2 MB]
Creating vector for yes... [1.6 MB]
Allocating iteration vectors... [2 x 1.6 MB]
TOTAL: [7.1 MB]

Starting iterations...

Iterative method: 87 iterations in 0.31 seconds (average 0.001471, setup 0.18)

Value in the initial state: 0.23074938766338104

Time for model checking: 0.782 seconds.

Result: 0.23074938766338104 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmin=? [ F "AllAreaSearched" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=2,Objy1=1,Objx2=5,Objy2=1

Prob0E: 63 iterations in 0.46 seconds (average 0.007238, setup 0.00)

Prob1A: 3 iterations in 0.04 seconds (average 0.012000, setup 0.00)

yes = 1231, no = 172653, maybe = 38903

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=212787, nc=56770, nnz=143627, k=2] [1.9 MB]
Creating vector for yes... [1.6 MB]
Allocating iteration vectors... [2 x 1.6 MB]
TOTAL: [6.8 MB]

Starting iterations...

Iterative method: 92 iterations in 0.24 seconds (average 0.001435, setup 0.11)

Value in the initial state: 0.0017427774180146245

Time for model checking: 0.76 seconds.

Result: 0.0017427774180146245 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmax=? [ F "MissionTimeUp"|"AllAreaSearched"|"Crash" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=2,Objy1=1,Objx2=5,Objy2=1

Prob0A: 3 iterations in 0.12 seconds (average 0.038667, setup 0.00)

Prob1E: 172 iterations in 2.48 seconds (average 0.014442, setup 0.00)

yes = 184443, no = 953, maybe = 27391

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=212787, nc=49203, nnz=142522, k=2] [1.9 MB]
Creating vector for yes... [1.6 MB]
Allocating iteration vectors... [2 x 1.6 MB]
TOTAL: [6.8 MB]

Starting iterations...

Iterative method: 74 iterations in 0.28 seconds (average 0.001405, setup 0.18)

Value in the initial state: 0.25942364513858285

Time for model checking: 2.88 seconds.

Result: 0.25942364513858285 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmin=? [ F "MissionTimeUp"|"AllAreaSearched"|"Crash" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=2,Objy1=1,Objx2=5,Objy2=1

Prob0E: 3 iterations in 0.06 seconds (average 0.020000, setup 0.00)

Prob1A: 42 iterations in 0.36 seconds (average 0.008667, setup 0.00)

yes = 179335, no = 953, maybe = 32499

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=212787, nc=58631, nnz=170179, k=2] [2.2 MB]
Creating vector for yes... [1.6 MB]
Allocating iteration vectors... [2 x 1.6 MB]
TOTAL: [7.1 MB]

Starting iterations...

Iterative method: 85 iterations in 0.27 seconds (average 0.001459, setup 0.15)

Value in the initial state: 0.018020403680592812

Time for model checking: 0.723 seconds.

Result: 0.018020403680592812 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"drop"}max=? [ F "deadlock" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=2,Objy1=1,Objx2=5,Objy2=1

Prob0E: 4 iterations in 0.05 seconds (average 0.012000, setup 0.00)

Prob1A: 1 iterations in 0.01 seconds (average 0.008000, setup 0.00)

goal = 1, inf = 0, maybe = 212786

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=212787, nc=282389, nnz=569535, k=4] [8.4 MB]
Building sparse matrix (transition rewards)... [n=212787, nc=282389, nnz=0, k=4] [1.9 MB]
Creating vector for state rewards... [1.6 MB]
Creating vector for inf... [1.6 MB]
Allocating iteration vectors... [2 x 1.6 MB]
TOTAL: [16.8 MB]

Starting iterations...

Iterative method: 83 iterations in 0.62 seconds (average 0.002940, setup 0.38)

Value in the initial state: 0.21130059635636503

Time for model checking: 0.703 seconds.

Result: 0.21130059635636503 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"drop"}min=? [ F "deadlock" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=2,Objy1=1,Objx2=5,Objy2=1

Prob0A: 4 iterations in 0.04 seconds (average 0.009000, setup 0.00)

Prob1E: 5 iterations in 0.05 seconds (average 0.009600, setup 0.00)

Warning: PRISM hasn't checked for zero-reward loops.
Your minimum rewards may be too low...

goal = 1, inf = 0, maybe = 212786

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=212787, nc=282389, nnz=569535, k=4] [8.4 MB]
Building sparse matrix (transition rewards)... [n=212787, nc=282389, nnz=0, k=4] [1.9 MB]
Creating vector for state rewards... [1.6 MB]
Creating vector for inf... [1.6 MB]
Allocating iteration vectors... [2 x 1.6 MB]
TOTAL: [16.8 MB]

Starting iterations...

Iterative method: 74 iterations in 0.54 seconds (average 0.003081, setup 0.31)

Value in the initial state: 0.187335271829551

Time for model checking: 0.642 seconds.

Result: 0.187335271829551 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"charge"}max=? [ F "deadlock" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=2,Objy1=1,Objx2=5,Objy2=1

Prob0E: 4 iterations in 0.03 seconds (average 0.007000, setup 0.00)

Prob1A: 1 iterations in 0.00 seconds (average 0.004000, setup 0.00)

goal = 1, inf = 0, maybe = 212786

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=212787, nc=282389, nnz=569535, k=4] [8.4 MB]
Building sparse matrix (transition rewards)... [n=212787, nc=282389, nnz=6458, k=4] [2.0 MB]
Creating vector for state rewards... [1.6 MB]
Creating vector for inf... [1.6 MB]
Allocating iteration vectors... [2 x 1.6 MB]
TOTAL: [16.9 MB]

Starting iterations...

Iterative method: 79 iterations in 0.56 seconds (average 0.003089, setup 0.32)

Value in the initial state: 0.29320542935464006

Time for model checking: 0.605 seconds.

Result: 0.29320542935464006 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"charge"}min=? [ F "deadlock" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=2,Objy1=1,Objx2=5,Objy2=1

Prob0A: 4 iterations in 0.03 seconds (average 0.007000, setup 0.00)

Prob1E: 5 iterations in 0.05 seconds (average 0.009600, setup 0.00)

Warning: PRISM hasn't checked for zero-reward loops.
Your minimum rewards may be too low...

goal = 1, inf = 0, maybe = 212786

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=212787, nc=282389, nnz=569535, k=4] [8.4 MB]
Building sparse matrix (transition rewards)... [n=212787, nc=282389, nnz=6458, k=4] [2.0 MB]
Creating vector for state rewards... [1.6 MB]
Creating vector for inf... [1.6 MB]
Allocating iteration vectors... [2 x 1.6 MB]
TOTAL: [16.9 MB]

Starting iterations...

Iterative method: 88 iterations in 0.59 seconds (average 0.003182, setup 0.31)

Value in the initial state: 0.032675657968414394

Time for model checking: 0.682 seconds.

Result: 0.032675657968414394 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"time"}max=? [ F "deadlock" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=2,Objy1=1,Objx2=5,Objy2=1

Prob0E: 4 iterations in 0.02 seconds (average 0.006000, setup 0.00)

Prob1A: 1 iterations in 0.01 seconds (average 0.008000, setup 0.00)

goal = 1, inf = 0, maybe = 212786

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=212787, nc=282389, nnz=569535, k=4] [8.4 MB]
Building sparse matrix (transition rewards)... [n=212787, nc=282389, nnz=106777, k=4] [3.1 MB]
Creating vector for state rewards... [1.6 MB]
Creating vector for inf... [1.6 MB]
Allocating iteration vectors... [2 x 1.6 MB]
TOTAL: [18.0 MB]

Starting iterations...

Iterative method: 89 iterations in 0.64 seconds (average 0.003236, setup 0.35)

Value in the initial state: 170.50309073285968

Time for model checking: 0.679 seconds.

Result: 170.50309073285968 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"time"}min=? [ F "deadlock" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=2,Objy1=1,Objx2=5,Objy2=1

Prob0A: 4 iterations in 0.02 seconds (average 0.006000, setup 0.00)

Prob1E: 5 iterations in 0.05 seconds (average 0.009600, setup 0.00)

Warning: PRISM hasn't checked for zero-reward loops.
Your minimum rewards may be too low...

goal = 1, inf = 0, maybe = 212786

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=212787, nc=282389, nnz=569535, k=4] [8.4 MB]
Building sparse matrix (transition rewards)... [n=212787, nc=282389, nnz=106777, k=4] [3.1 MB]
Creating vector for state rewards... [1.6 MB]
Creating vector for inf... [1.6 MB]
Allocating iteration vectors... [2 x 1.6 MB]
TOTAL: [18.0 MB]

Starting iterations...

Iterative method: 93 iterations in 0.64 seconds (average 0.003269, setup 0.34)

Value in the initial state: 88.55791652495468

Time for model checking: 0.728 seconds.

Result: 88.55791652495468 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"success"}max=? [ F "deadlock" ]/Pmin=? [ F "MissionSuccessful" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=2,Objy1=1,Objx2=5,Objy2=1

Prob0E: 4 iterations in 0.02 seconds (average 0.006000, setup 0.00)

Prob1A: 1 iterations in 0.00 seconds (average 0.004000, setup 0.00)

goal = 1, inf = 0, maybe = 212786

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=212787, nc=282389, nnz=569535, k=4] [8.4 MB]
Building sparse matrix (transition rewards)... [n=212787, nc=282389, nnz=1876, k=4] [1.9 MB]
Creating vector for state rewards... [1.6 MB]
Creating vector for inf... [1.6 MB]
Allocating iteration vectors... [2 x 1.6 MB]
TOTAL: [16.8 MB]

Starting iterations...

Iterative method: 89 iterations in 0.58 seconds (average 0.003101, setup 0.31)

Prob0E: 44 iterations in 0.28 seconds (average 0.006455, setup 0.00)

Prob1A: 3 iterations in 0.04 seconds (average 0.013333, setup 0.00)

yes = 952, no = 184444, maybe = 27391

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=212787, nc=49203, nnz=142522, k=2] [1.9 MB]
Creating vector for yes... [1.6 MB]
Allocating iteration vectors... [2 x 1.6 MB]
TOTAL: [6.8 MB]

Starting iterations...

Iterative method: 81 iterations in 0.23 seconds (average 0.001383, setup 0.12)

Value in the initial state: 225.54423913292828

Time for model checking: 1.2 seconds.

Result: 225.54423913292828 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"success"}min=? [ F "deadlock" ]/Pmax=? [ F "MissionSuccessful" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=2,Objy1=1,Objx2=5,Objy2=1

Prob0A: 4 iterations in 0.06 seconds (average 0.015000, setup 0.00)

Prob1E: 5 iterations in 0.05 seconds (average 0.010400, setup 0.00)

Warning: PRISM hasn't checked for zero-reward loops.
Your minimum rewards may be too low...

goal = 1, inf = 0, maybe = 212786

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=212787, nc=282389, nnz=569535, k=4] [8.4 MB]
Building sparse matrix (transition rewards)... [n=212787, nc=282389, nnz=1876, k=4] [1.9 MB]
Creating vector for state rewards... [1.6 MB]
Creating vector for inf... [1.6 MB]
Allocating iteration vectors... [2 x 1.6 MB]
TOTAL: [16.8 MB]

Starting iterations...

Iterative method: 85 iterations in 0.59 seconds (average 0.003106, setup 0.33)

Prob0A: 42 iterations in 0.26 seconds (average 0.006190, setup 0.00)

Prob1E: 4 iterations in 0.04 seconds (average 0.009000, setup 0.00)

yes = 952, no = 179336, maybe = 32499

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=212787, nc=58631, nnz=170179, k=2] [2.2 MB]
Creating vector for yes... [1.6 MB]
Allocating iteration vectors... [2 x 1.6 MB]
TOTAL: [7.1 MB]

Starting iterations...

Iterative method: 87 iterations in 0.25 seconds (average 0.001425, setup 0.13)

Value in the initial state: 78.3306068637117

Time for model checking: 1.248 seconds.

Result: 78.3306068637117 (value in the initial state)

---------------------------------------------------------------------

Note: There were 16 warnings during computation.

