PRISM
=====

Version: 4.3
Date: Tue Aug 02 12:39:30 BST 2016
Hostname: eglinton
Memory limits: cudd=1g, java(heap)=981.5m
Command line: prism uav_sim_ft_mdp.prism -s uav_mdp.props -const 'Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=2,Objy1=3,Objx2=6,Objy2=3,'

Parsing model file "uav_sim_ft_mdp.prism"...

Parsing properties file "uav_mdp.props"...

18 properties:
(1) Pmax=? [ F "MissionSuccessful" ]
(2) Pmin=? [ F "MissionSuccessful" ]
(3) Pmax=? [ F "MissionTimeUp" ]
(4) Pmin=? [ F "MissionTimeUp" ]
(5) Pmax=? [ F "Crash" ]
(6) Pmin=? [ F "Crash" ]
(7) Pmax=? [ F "AllAreaSearched" ]
(8) Pmin=? [ F "AllAreaSearched" ]
(9) Pmax=? [ F "MissionTimeUp"|"AllAreaSearched"|"Crash" ]
(10) Pmin=? [ F "MissionTimeUp"|"AllAreaSearched"|"Crash" ]
(11) R{"drop"}max=? [ F "deadlock" ]
(12) R{"drop"}min=? [ F "deadlock" ]
(13) R{"charge"}max=? [ F "deadlock" ]
(14) R{"charge"}min=? [ F "deadlock" ]
(15) R{"time"}max=? [ F "deadlock" ]
(16) R{"time"}min=? [ F "deadlock" ]
(17) R{"success"}max=? [ F "deadlock" ]/Pmin=? [ F "MissionSuccessful" ]
(18) R{"success"}min=? [ F "deadlock" ]/Pmax=? [ F "MissionSuccessful" ]

Type:        MDP
Modules:     Obj Move Robot T 
Variables:   NoOfObjs posx posy s1 retx rety b t c 

---------------------------------------------------------------------

Model checking: Pmax=? [ F "MissionSuccessful" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=2,Objy1=3,Objx2=6,Objy2=3

Building model...
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=2,Objy1=3,Objx2=6,Objy2=3

Warning: Guard for command 7 of module "Move" is never satisfied.

Warning: Guard for command 8 of module "Move" is never satisfied.

Warning: Guard for command 9 of module "Move" is never satisfied.

Warning: Guard for command 7 of module "Robot" is never satisfied.

Warning: Update 2 of command 3 of module "Robot" doesn't do anything ("(s1'=13)", line 249, column 44)

Warning: Update 2 of command 27 of module "T" doesn't do anything ("(c'=1)", line 372, column 108)

Warning: Guard for command 28 of module "T" is never satisfied.

Warning: Update 2 of command 9 of module "T" doesn't do anything ("(c'=1)", line 349, column 112)

Warning: Guard for command 10 of module "T" is never satisfied.

Warning: Update 2 of command 51 of module "T" doesn't do anything ("(c'=1)", line 403, column 113)

Warning: Guard for command 52 of module "T" is never satisfied.

Computing reachable states...

Reachability (BFS): 83 iterations in 0.46 seconds (average 0.005494, setup 0.00)

Time for model construction: 1.572 seconds.

Warning: Deadlocks detected and fixed in 1 states

Type:        MDP
States:      105621 (1 initial)
Transitions: 295549
Choices:     143189

Transition matrix: 153212 nodes (76 terminal), 295549 minterms, vars: 34r/34c/18nd

Prob0A: 68 iterations in 0.24 seconds (average 0.003471, setup 0.00)

Prob1E: 4 iterations in 0.04 seconds (average 0.009000, setup 0.00)

yes = 207, no = 98190, maybe = 7224

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=105621, nc=13171, nnz=39476, k=2] [578.6 KB]
Creating vector for yes... [825.2 KB]
Allocating iteration vectors... [2 x 825.2 KB]
TOTAL: [3.0 MB]

Starting iterations...

Iterative method: 100 iterations in 0.16 seconds (average 0.000600, setup 0.10)

Value in the initial state: 0.014124027461945592

Time for model checking: 0.447 seconds.

Result: 0.014124027461945592 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmin=? [ F "MissionSuccessful" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=2,Objy1=3,Objx2=6,Objy2=3

Prob0E: 23 iterations in 0.06 seconds (average 0.002783, setup 0.00)

Prob1A: 3 iterations in 0.02 seconds (average 0.006667, setup 0.00)

yes = 207, no = 99467, maybe = 5947

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=105621, nc=10867, nnz=32446, k=2] [494.0 KB]
Creating vector for yes... [825.2 KB]
Allocating iteration vectors... [2 x 825.2 KB]
TOTAL: [2.9 MB]

Starting iterations...

Iterative method: 43 iterations in 0.06 seconds (average 0.000558, setup 0.04)

Value in the initial state: 0.0

Time for model checking: 0.148 seconds.

Result: 0.0 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmax=? [ F "MissionTimeUp" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=2,Objy1=3,Objx2=6,Objy2=3

Prob0A: 71 iterations in 0.42 seconds (average 0.005915, setup 0.00)

Prob1E: 4 iterations in 0.03 seconds (average 0.007000, setup 0.00)

yes = 212, no = 67438, maybe = 37971

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=105621, nc=68799, nnz=198431, k=4] [2.4 MB]
Creating vector for yes... [825.2 KB]
Allocating iteration vectors... [2 x 825.2 KB]
TOTAL: [4.9 MB]

Starting iterations...

Iterative method: 96 iterations in 0.28 seconds (average 0.000917, setup 0.20)

Value in the initial state: 2.0320202658893967E-4

Time for model checking: 0.749 seconds.

Result: 2.0320202658893967E-4 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmin=? [ F "MissionTimeUp" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=2,Objy1=3,Objx2=6,Objy2=3

Prob0E: 78 iterations in 0.48 seconds (average 0.006103, setup 0.00)

Prob1A: 3 iterations in 0.03 seconds (average 0.010667, setup 0.00)

yes = 212, no = 67709, maybe = 37700

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=105621, nc=68309, nnz=197428, k=4] [2.4 MB]
Creating vector for yes... [825.2 KB]
Allocating iteration vectors... [2 x 825.2 KB]
TOTAL: [4.8 MB]

Starting iterations...

Iterative method: 112 iterations in 0.26 seconds (average 0.000929, setup 0.16)

Value in the initial state: 7.427636791594283E-7

Time for model checking: 0.788 seconds.

Result: 7.427636791594283E-7 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmax=? [ F "Crash" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=2,Objy1=3,Objx2=6,Objy2=3

Prob0A: 3 iterations in 0.06 seconds (average 0.021333, setup 0.00)

Prob1E: 180 iterations in 1.63 seconds (average 0.009044, setup 0.00)

yes = 51540, no = 1669, maybe = 52412

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=105621, nc=89980, nnz=242340, k=4] [3.0 MB]
Creating vector for yes... [825.2 KB]
Allocating iteration vectors... [2 x 825.2 KB]
TOTAL: [5.4 MB]

Starting iterations...

Iterative method: 92 iterations in 0.31 seconds (average 0.001000, setup 0.22)

Value in the initial state: 0.017594557497967008

Time for model checking: 2.017 seconds.

Result: 0.017594557497967008 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmin=? [ F "Crash" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=2,Objy1=3,Objx2=6,Objy2=3

Prob0E: 3 iterations in 0.06 seconds (average 0.018667, setup 0.00)

Prob1A: 47 iterations in 0.35 seconds (average 0.007404, setup 0.00)

yes = 51540, no = 1669, maybe = 52412

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=105621, nc=89980, nnz=242340, k=4] [3.0 MB]
Creating vector for yes... [825.2 KB]
Allocating iteration vectors... [2 x 825.2 KB]
TOTAL: [5.4 MB]

Starting iterations...

Iterative method: 87 iterations in 0.28 seconds (average 0.001057, setup 0.19)

Value in the initial state: 0.013515503045693284

Time for model checking: 0.697 seconds.

Result: 0.013515503045693284 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmax=? [ F "AllAreaSearched" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=2,Objy1=3,Objx2=6,Objy2=3

Prob0A: 47 iterations in 0.22 seconds (average 0.004681, setup 0.00)

Prob1E: 4 iterations in 0.04 seconds (average 0.009000, setup 0.00)

yes = 1249, no = 71352, maybe = 33020

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=105621, nc=54878, nnz=150912, k=2] [1.9 MB]
Creating vector for yes... [825.2 KB]
Allocating iteration vectors... [2 x 825.2 KB]
TOTAL: [4.3 MB]

Starting iterations...

Iterative method: 93 iterations in 0.23 seconds (average 0.000817, setup 0.15)

Value in the initial state: 0.9864819908259038

Time for model checking: 0.497 seconds.

Result: 0.9864819908259038 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmin=? [ F "AllAreaSearched" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=2,Objy1=3,Objx2=6,Objy2=3

Prob0E: 47 iterations in 0.26 seconds (average 0.005617, setup 0.00)

Prob1A: 3 iterations in 0.03 seconds (average 0.010667, setup 0.00)

yes = 1249, no = 74237, maybe = 30135

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=105621, nc=49394, nnz=134371, k=2] [1.7 MB]
Creating vector for yes... [825.2 KB]
Allocating iteration vectors... [2 x 825.2 KB]
TOTAL: [4.1 MB]

Starting iterations...

Iterative method: 92 iterations in 0.20 seconds (average 0.000783, setup 0.13)

Value in the initial state: 0.9683647346673169

Time for model checking: 0.525 seconds.

Result: 0.9683647346673169 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmax=? [ F "MissionTimeUp"|"AllAreaSearched"|"Crash" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=2,Objy1=3,Objx2=6,Objy2=3

Prob0A: 3 iterations in 0.10 seconds (average 0.032000, setup 0.00)

Prob1E: 88 iterations in 0.69 seconds (average 0.007818, setup 0.00)

yes = 99466, no = 208, maybe = 5947

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=105621, nc=10867, nnz=32446, k=2] [494.0 KB]
Creating vector for yes... [825.2 KB]
Allocating iteration vectors... [2 x 825.2 KB]
TOTAL: [2.9 MB]

Starting iterations...

Iterative method: 42 iterations in 0.08 seconds (average 0.000571, setup 0.06)

Value in the initial state: 1.0

Time for model checking: 0.855 seconds.

Result: 1.0 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmin=? [ F "MissionTimeUp"|"AllAreaSearched"|"Crash" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=2,Objy1=3,Objx2=6,Objy2=3

Prob0E: 3 iterations in 0.04 seconds (average 0.012000, setup 0.00)

Prob1A: 68 iterations in 0.36 seconds (average 0.005235, setup 0.00)

yes = 98189, no = 208, maybe = 7224

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=105621, nc=13171, nnz=39476, k=2] [578.6 KB]
Creating vector for yes... [825.2 KB]
Allocating iteration vectors... [2 x 825.2 KB]
TOTAL: [3.0 MB]

Starting iterations...

Iterative method: 75 iterations in 0.12 seconds (average 0.000587, setup 0.08)

Value in the initial state: 0.985870596907319

Time for model checking: 0.519 seconds.

Result: 0.985870596907319 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"drop"}max=? [ F "deadlock" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=2,Objy1=3,Objx2=6,Objy2=3

Prob0E: 4 iterations in 0.06 seconds (average 0.014000, setup 0.00)

Prob1A: 1 iterations in 0.00 seconds (average 0.004000, setup 0.00)

goal = 1, inf = 0, maybe = 105620

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=105621, nc=143188, nnz=295548, k=4] [4.3 MB]
Building sparse matrix (transition rewards)... [n=105621, nc=143188, nnz=0, k=4] [971.9 KB]
Creating vector for state rewards... [825.2 KB]
Creating vector for inf... [825.2 KB]
Allocating iteration vectors... [2 x 825.2 KB]
TOTAL: [8.5 MB]

Starting iterations...

Iterative method: 88 iterations in 0.40 seconds (average 0.001455, setup 0.27)

Value in the initial state: 0.13978200497610532

Time for model checking: 0.478 seconds.

Result: 0.13978200497610532 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"drop"}min=? [ F "deadlock" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=2,Objy1=3,Objx2=6,Objy2=3

Prob0A: 4 iterations in 0.04 seconds (average 0.009000, setup 0.00)

Prob1E: 5 iterations in 0.04 seconds (average 0.007200, setup 0.00)

Warning: PRISM hasn't checked for zero-reward loops.
Your minimum rewards may be too low...

goal = 1, inf = 0, maybe = 105620

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=105621, nc=143188, nnz=295548, k=4] [4.3 MB]
Building sparse matrix (transition rewards)... [n=105621, nc=143188, nnz=0, k=4] [971.9 KB]
Creating vector for state rewards... [825.2 KB]
Creating vector for inf... [825.2 KB]
Allocating iteration vectors... [2 x 825.2 KB]
TOTAL: [8.5 MB]

Starting iterations...

Iterative method: 78 iterations in 0.38 seconds (average 0.002205, setup 0.20)

Value in the initial state: 0.13676385330506485

Time for model checking: 0.452 seconds.

Result: 0.13676385330506485 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"charge"}max=? [ F "deadlock" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=2,Objy1=3,Objx2=6,Objy2=3

Prob0E: 4 iterations in 0.02 seconds (average 0.005000, setup 0.00)

Prob1A: 1 iterations in 0.00 seconds (average 0.004000, setup 0.00)

goal = 1, inf = 0, maybe = 105620

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=105621, nc=143188, nnz=295548, k=4] [4.3 MB]
Building sparse matrix (transition rewards)... [n=105621, nc=143188, nnz=3621, k=4] [1014.3 KB]
Creating vector for state rewards... [825.2 KB]
Creating vector for inf... [825.2 KB]
Allocating iteration vectors... [2 x 825.2 KB]
TOTAL: [8.5 MB]

Starting iterations...

Iterative method: 91 iterations in 0.31 seconds (average 0.001538, setup 0.17)

Value in the initial state: 0.03645270479877296

Time for model checking: 0.347 seconds.

Result: 0.03645270479877296 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"charge"}min=? [ F "deadlock" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=2,Objy1=3,Objx2=6,Objy2=3

Prob0A: 4 iterations in 0.02 seconds (average 0.004000, setup 0.00)

Prob1E: 5 iterations in 0.03 seconds (average 0.005600, setup 0.00)

Warning: PRISM hasn't checked for zero-reward loops.
Your minimum rewards may be too low...

goal = 1, inf = 0, maybe = 105620

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=105621, nc=143188, nnz=295548, k=4] [4.3 MB]
Building sparse matrix (transition rewards)... [n=105621, nc=143188, nnz=3621, k=4] [1014.3 KB]
Creating vector for state rewards... [825.2 KB]
Creating vector for inf... [825.2 KB]
Allocating iteration vectors... [2 x 825.2 KB]
TOTAL: [8.5 MB]

Starting iterations...

Iterative method: 102 iterations in 0.33 seconds (average 0.001569, setup 0.17)

Value in the initial state: 0.004994724636844288

Time for model checking: 0.387 seconds.

Result: 0.004994724636844288 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"time"}max=? [ F "deadlock" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=2,Objy1=3,Objx2=6,Objy2=3

Prob0E: 4 iterations in 0.02 seconds (average 0.004000, setup 0.00)

Prob1A: 1 iterations in 0.00 seconds (average 0.004000, setup 0.00)

goal = 1, inf = 0, maybe = 105620

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=105621, nc=143188, nnz=295548, k=4] [4.3 MB]
Building sparse matrix (transition rewards)... [n=105621, nc=143188, nnz=53207, k=4] [1.6 MB]
Creating vector for state rewards... [825.2 KB]
Creating vector for inf... [825.2 KB]
Allocating iteration vectors... [2 x 825.2 KB]
TOTAL: [9.1 MB]

Starting iterations...

Iterative method: 95 iterations in 0.34 seconds (average 0.001558, setup 0.19)

Value in the initial state: 97.50095191441383

Time for model checking: 0.374 seconds.

Result: 97.50095191441383 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"time"}min=? [ F "deadlock" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=2,Objy1=3,Objx2=6,Objy2=3

Prob0A: 4 iterations in 0.02 seconds (average 0.004000, setup 0.00)

Prob1E: 5 iterations in 0.03 seconds (average 0.005600, setup 0.00)

Warning: PRISM hasn't checked for zero-reward loops.
Your minimum rewards may be too low...

goal = 1, inf = 0, maybe = 105620

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=105621, nc=143188, nnz=295548, k=4] [4.3 MB]
Building sparse matrix (transition rewards)... [n=105621, nc=143188, nnz=53207, k=4] [1.6 MB]
Creating vector for state rewards... [825.2 KB]
Creating vector for inf... [825.2 KB]
Allocating iteration vectors... [2 x 825.2 KB]
TOTAL: [9.1 MB]

Starting iterations...

Iterative method: 101 iterations in 0.35 seconds (average 0.001624, setup 0.19)

Value in the initial state: 75.02093281595525

Time for model checking: 0.405 seconds.

Result: 75.02093281595525 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"success"}max=? [ F "deadlock" ]/Pmin=? [ F "MissionSuccessful" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=2,Objy1=3,Objx2=6,Objy2=3

Prob0E: 4 iterations in 0.02 seconds (average 0.004000, setup 0.00)

Prob1A: 1 iterations in 0.00 seconds (average 0.004000, setup 0.00)

goal = 1, inf = 0, maybe = 105620

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=105621, nc=143188, nnz=295548, k=4] [4.3 MB]
Building sparse matrix (transition rewards)... [n=105621, nc=143188, nnz=408, k=4] [976.7 KB]
Creating vector for state rewards... [825.2 KB]
Creating vector for inf... [825.2 KB]
Allocating iteration vectors... [2 x 825.2 KB]
TOTAL: [8.5 MB]

Starting iterations...

Iterative method: 97 iterations in 0.32 seconds (average 0.001526, setup 0.18)

Prob0E: 23 iterations in 0.06 seconds (average 0.002435, setup 0.00)

Prob1A: 3 iterations in 0.02 seconds (average 0.005333, setup 0.00)

yes = 207, no = 99467, maybe = 5947

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=105621, nc=10867, nnz=32446, k=2] [494.0 KB]
Creating vector for yes... [825.2 KB]
Allocating iteration vectors... [2 x 825.2 KB]
TOTAL: [2.9 MB]

Starting iterations...

Iterative method: 43 iterations in 0.06 seconds (average 0.000558, setup 0.03)

Value in the initial state: Infinity

Time for model checking: 0.492 seconds.

Result: Infinity (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"success"}min=? [ F "deadlock" ]/Pmax=? [ F "MissionSuccessful" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=2,Objy1=3,Objx2=6,Objy2=3

Prob0A: 4 iterations in 0.02 seconds (average 0.006000, setup 0.00)

Prob1E: 5 iterations in 0.03 seconds (average 0.005600, setup 0.00)

Warning: PRISM hasn't checked for zero-reward loops.
Your minimum rewards may be too low...

goal = 1, inf = 0, maybe = 105620

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=105621, nc=143188, nnz=295548, k=4] [4.3 MB]
Building sparse matrix (transition rewards)... [n=105621, nc=143188, nnz=408, k=4] [976.7 KB]
Creating vector for state rewards... [825.2 KB]
Creating vector for inf... [825.2 KB]
Allocating iteration vectors... [2 x 825.2 KB]
TOTAL: [8.5 MB]

Starting iterations...

Iterative method: 48 iterations in 0.26 seconds (average 0.001500, setup 0.19)

Prob0A: 68 iterations in 0.18 seconds (average 0.002647, setup 0.00)

Prob1E: 4 iterations in 0.02 seconds (average 0.006000, setup 0.00)

yes = 207, no = 98190, maybe = 7224

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=105621, nc=13171, nnz=39476, k=2] [578.6 KB]
Creating vector for yes... [825.2 KB]
Allocating iteration vectors... [2 x 825.2 KB]
TOTAL: [3.0 MB]

Starting iterations...

Iterative method: 100 iterations in 0.14 seconds (average 0.000600, setup 0.08)

Value in the initial state: 0.0

Time for model checking: 0.656 seconds.

Result: 0.0 (value in the initial state)

---------------------------------------------------------------------

Note: There were 16 warnings during computation.

