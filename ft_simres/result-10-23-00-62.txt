PRISM
=====

Version: 4.3
Date: Tue Aug 02 12:34:39 BST 2016
Hostname: eglinton
Memory limits: cudd=1g, java(heap)=981.5m
Command line: prism uav_sim_ft_mdp.prism -s uav_mdp.props -const 'Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=0,Objy1=0,Objx2=6,Objy2=2,'

Parsing model file "uav_sim_ft_mdp.prism"...

Parsing properties file "uav_mdp.props"...

18 properties:
(1) Pmax=? [ F "MissionSuccessful" ]
(2) Pmin=? [ F "MissionSuccessful" ]
(3) Pmax=? [ F "MissionTimeUp" ]
(4) Pmin=? [ F "MissionTimeUp" ]
(5) Pmax=? [ F "Crash" ]
(6) Pmin=? [ F "Crash" ]
(7) Pmax=? [ F "AllAreaSearched" ]
(8) Pmin=? [ F "AllAreaSearched" ]
(9) Pmax=? [ F "MissionTimeUp"|"AllAreaSearched"|"Crash" ]
(10) Pmin=? [ F "MissionTimeUp"|"AllAreaSearched"|"Crash" ]
(11) R{"drop"}max=? [ F "deadlock" ]
(12) R{"drop"}min=? [ F "deadlock" ]
(13) R{"charge"}max=? [ F "deadlock" ]
(14) R{"charge"}min=? [ F "deadlock" ]
(15) R{"time"}max=? [ F "deadlock" ]
(16) R{"time"}min=? [ F "deadlock" ]
(17) R{"success"}max=? [ F "deadlock" ]/Pmin=? [ F "MissionSuccessful" ]
(18) R{"success"}min=? [ F "deadlock" ]/Pmax=? [ F "MissionSuccessful" ]

Type:        MDP
Modules:     Obj Move Robot T 
Variables:   NoOfObjs posx posy s1 retx rety b t c 

---------------------------------------------------------------------

Model checking: Pmax=? [ F "MissionSuccessful" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=0,Objy1=0,Objx2=6,Objy2=2

Building model...
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=0,Objy1=0,Objx2=6,Objy2=2

Warning: Guard for command 7 of module "Move" is never satisfied.

Warning: Guard for command 8 of module "Move" is never satisfied.

Warning: Guard for command 9 of module "Move" is never satisfied.

Warning: Guard for command 7 of module "Robot" is never satisfied.

Warning: Update 2 of command 3 of module "Robot" doesn't do anything ("(s1'=13)", line 249, column 44)

Warning: Update 2 of command 27 of module "T" doesn't do anything ("(c'=1)", line 372, column 108)

Warning: Guard for command 28 of module "T" is never satisfied.

Warning: Update 2 of command 9 of module "T" doesn't do anything ("(c'=1)", line 349, column 112)

Warning: Guard for command 10 of module "T" is never satisfied.

Warning: Update 2 of command 51 of module "T" doesn't do anything ("(c'=1)", line 403, column 113)

Warning: Guard for command 52 of module "T" is never satisfied.

Computing reachable states...

Reachability (BFS): 80 iterations in 1.20 seconds (average 0.015000, setup 0.00)

Time for model construction: 3.343 seconds.

Warning: Deadlocks detected and fixed in 1 states

Type:        MDP
States:      274445 (1 initial)
Transitions: 736141
Choices:     362414

Transition matrix: 180162 nodes (76 terminal), 736141 minterms, vars: 34r/34c/18nd

Prob0A: 61 iterations in 0.58 seconds (average 0.009443, setup 0.00)

Prob1E: 4 iterations in 0.07 seconds (average 0.017000, setup 0.00)

yes = 921, no = 235860, maybe = 37664

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=274445, nc=64637, nnz=185653, k=2] [2.4 MB]
Creating vector for yes... [2.1 MB]
Allocating iteration vectors... [2 x 2.1 MB]
TOTAL: [8.7 MB]

Starting iterations...

Iterative method: 99 iterations in 0.46 seconds (average 0.002020, setup 0.26)

Value in the initial state: 0.9769745586116501

Time for model checking: 1.744 seconds.

Result: 0.9769745586116501 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmin=? [ F "MissionSuccessful" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=0,Objy1=0,Objx2=6,Objy2=2

Prob0E: 66 iterations in 0.63 seconds (average 0.009576, setup 0.00)

Prob1A: 3 iterations in 0.07 seconds (average 0.022667, setup 0.00)

yes = 921, no = 240665, maybe = 32859

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=274445, nc=56656, nnz=163591, k=2] [2.2 MB]
Creating vector for yes... [2.1 MB]
Allocating iteration vectors... [2 x 2.1 MB]
TOTAL: [8.5 MB]

Starting iterations...

Iterative method: 92 iterations in 0.38 seconds (average 0.001826, setup 0.22)

Value in the initial state: 0.7144504607197556

Time for model checking: 1.421 seconds.

Result: 0.7144504607197556 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmax=? [ F "MissionTimeUp" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=0,Objy1=0,Objx2=6,Objy2=2

Prob0A: 63 iterations in 0.68 seconds (average 0.010794, setup 0.00)

Prob1E: 4 iterations in 0.06 seconds (average 0.014000, setup 0.00)

yes = 580, no = 155379, maybe = 118486

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=274445, nc=196578, nnz=539389, k=4] [6.6 MB]
Creating vector for yes... [2.1 MB]
Allocating iteration vectors... [2 x 2.1 MB]
TOTAL: [12.9 MB]

Starting iterations...

Iterative method: 94 iterations in 0.59 seconds (average 0.002596, setup 0.35)

Value in the initial state: 0.0036184949937953146

Time for model checking: 1.837 seconds.

Result: 0.0036184949937953146 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmin=? [ F "MissionTimeUp" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=0,Objy1=0,Objx2=6,Objy2=2

Prob0E: 69 iterations in 0.93 seconds (average 0.013507, setup 0.00)

Prob1A: 3 iterations in 0.06 seconds (average 0.020000, setup 0.00)

yes = 580, no = 155793, maybe = 118072

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=274445, nc=195826, nnz=537963, k=4] [6.6 MB]
Creating vector for yes... [2.1 MB]
Allocating iteration vectors... [2 x 2.1 MB]
TOTAL: [12.9 MB]

Starting iterations...

Iterative method: 110 iterations in 0.62 seconds (average 0.002582, setup 0.34)

Value in the initial state: 1.789523146041549E-5

Time for model checking: 2.097 seconds.

Result: 1.789523146041549E-5 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmax=? [ F "Crash" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=0,Objy1=0,Objx2=6,Objy2=2

Prob0A: 3 iterations in 0.10 seconds (average 0.032000, setup 0.00)

Prob1E: 224 iterations in 3.41 seconds (average 0.015232, setup 0.00)

yes = 134351, no = 2728, maybe = 137366

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=274445, nc=225335, nnz=599062, k=4] [7.3 MB]
Creating vector for yes... [2.1 MB]
Allocating iteration vectors... [2 x 2.1 MB]
TOTAL: [13.6 MB]

Starting iterations...

Iterative method: 92 iterations in 0.65 seconds (average 0.002652, setup 0.40)

Value in the initial state: 0.06302068069879181

Time for model checking: 5.032 seconds.

Result: 0.06302068069879181 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmin=? [ F "Crash" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=0,Objy1=0,Objx2=6,Objy2=2

Prob0E: 3 iterations in 0.14 seconds (average 0.048000, setup 0.00)

Prob1A: 54 iterations in 0.67 seconds (average 0.012444, setup 0.00)

yes = 134351, no = 2728, maybe = 137366

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=274445, nc=225335, nnz=599062, k=4] [7.3 MB]
Creating vector for yes... [2.1 MB]
Allocating iteration vectors... [2 x 2.1 MB]
TOTAL: [13.6 MB]

Starting iterations...

Iterative method: 98 iterations in 0.61 seconds (average 0.002694, setup 0.34)

Value in the initial state: 0.02092963248081785

Time for model checking: 1.758 seconds.

Result: 0.02092963248081785 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmax=? [ F "AllAreaSearched" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=0,Objy1=0,Objx2=6,Objy2=2

Prob0A: 65 iterations in 0.55 seconds (average 0.008492, setup 0.00)

Prob1E: 4 iterations in 0.05 seconds (average 0.012000, setup 0.00)

yes = 1226, no = 219542, maybe = 53677

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=274445, nc=87351, nnz=241509, k=2] [3.1 MB]
Creating vector for yes... [2.1 MB]
Allocating iteration vectors... [2 x 2.1 MB]
TOTAL: [9.4 MB]

Starting iterations...

Iterative method: 91 iterations in 0.39 seconds (average 0.002022, setup 0.21)

Value in the initial state: 0.25514912840271486

Time for model checking: 1.545 seconds.

Result: 0.25514912840271486 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmin=? [ F "AllAreaSearched" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=0,Objy1=0,Objx2=6,Objy2=2

Prob0E: 72 iterations in 0.59 seconds (average 0.008167, setup 0.00)

Prob1A: 3 iterations in 0.05 seconds (average 0.017333, setup 0.00)

yes = 1226, no = 225832, maybe = 47387

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=274445, nc=76228, nnz=209382, k=2] [2.7 MB]
Creating vector for yes... [2.1 MB]
Allocating iteration vectors... [2 x 2.1 MB]
TOTAL: [9.0 MB]

Starting iterations...

Iterative method: 91 iterations in 0.36 seconds (average 0.002022, setup 0.17)

Value in the initial state: 0.001292168505492628

Time for model checking: 1.144 seconds.

Result: 0.001292168505492628 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmax=? [ F "MissionTimeUp"|"AllAreaSearched"|"Crash" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=0,Objy1=0,Objx2=6,Objy2=2

Prob0A: 3 iterations in 0.10 seconds (average 0.032000, setup 0.00)

Prob1E: 256 iterations in 3.90 seconds (average 0.015250, setup 0.00)

yes = 240664, no = 922, maybe = 32859

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=274445, nc=56656, nnz=163591, k=2] [2.2 MB]
Creating vector for yes... [2.1 MB]
Allocating iteration vectors... [2 x 2.1 MB]
TOTAL: [8.5 MB]

Starting iterations...

Iterative method: 85 iterations in 0.36 seconds (average 0.001882, setup 0.20)

Value in the initial state: 0.28554890020032064

Time for model checking: 5.591 seconds.

Result: 0.28554890020032064 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmin=? [ F "MissionTimeUp"|"AllAreaSearched"|"Crash" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=0,Objy1=0,Objx2=6,Objy2=2

Prob0E: 3 iterations in 0.07 seconds (average 0.024000, setup 0.00)

Prob1A: 61 iterations in 0.70 seconds (average 0.011541, setup 0.00)

yes = 235859, no = 922, maybe = 37664

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=274445, nc=64637, nnz=185653, k=2] [2.4 MB]
Creating vector for yes... [2.1 MB]
Allocating iteration vectors... [2 x 2.1 MB]
TOTAL: [8.7 MB]

Starting iterations...

Iterative method: 96 iterations in 0.37 seconds (average 0.001792, setup 0.20)

Value in the initial state: 0.023025090031639668

Time for model checking: 1.219 seconds.

Result: 0.023025090031639668 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"drop"}max=? [ F "deadlock" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=0,Objy1=0,Objx2=6,Objy2=2

Prob0E: 4 iterations in 0.09 seconds (average 0.022000, setup 0.00)

Prob1A: 1 iterations in 0.01 seconds (average 0.008000, setup 0.00)

goal = 1, inf = 0, maybe = 274444

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=274445, nc=362413, nnz=736140, k=4] [10.9 MB]
Building sparse matrix (transition rewards)... [n=274445, nc=362413, nnz=0, k=4] [2.4 MB]
Creating vector for state rewards... [2.1 MB]
Creating vector for inf... [2.1 MB]
Allocating iteration vectors... [2 x 2.1 MB]
TOTAL: [21.7 MB]

Starting iterations...

Iterative method: 94 iterations in 0.89 seconds (average 0.004043, setup 0.51)

Value in the initial state: 0.3522859769303436

Time for model checking: 1.233 seconds.

Result: 0.3522859769303436 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"drop"}min=? [ F "deadlock" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=0,Objy1=0,Objx2=6,Objy2=2

Prob0A: 4 iterations in 0.05 seconds (average 0.012000, setup 0.00)

Prob1E: 5 iterations in 0.06 seconds (average 0.012000, setup 0.00)

Warning: PRISM hasn't checked for zero-reward loops.
Your minimum rewards may be too low...

goal = 1, inf = 0, maybe = 274444

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=274445, nc=362413, nnz=736140, k=4] [10.9 MB]
Building sparse matrix (transition rewards)... [n=274445, nc=362413, nnz=0, k=4] [2.4 MB]
Creating vector for state rewards... [2.1 MB]
Creating vector for inf... [2.1 MB]
Allocating iteration vectors... [2 x 2.1 MB]
TOTAL: [21.7 MB]

Starting iterations...

Iterative method: 85 iterations in 0.73 seconds (average 0.004047, setup 0.39)

Value in the initial state: 0.22582752249631213

Time for model checking: 0.892 seconds.

Result: 0.22582752249631213 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"charge"}max=? [ F "deadlock" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=0,Objy1=0,Objx2=6,Objy2=2

Prob0E: 4 iterations in 0.03 seconds (average 0.008000, setup 0.00)

Prob1A: 1 iterations in 0.01 seconds (average 0.008000, setup 0.00)

goal = 1, inf = 0, maybe = 274444

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=274445, nc=362413, nnz=736140, k=4] [10.9 MB]
Building sparse matrix (transition rewards)... [n=274445, nc=362413, nnz=8570, k=4] [2.5 MB]
Creating vector for state rewards... [2.1 MB]
Creating vector for inf... [2.1 MB]
Allocating iteration vectors... [2 x 2.1 MB]
TOTAL: [21.8 MB]

Starting iterations...

Iterative method: 90 iterations in 0.76 seconds (average 0.004089, setup 0.40)

Value in the initial state: 0.9815128878693267

Time for model checking: 0.965 seconds.

Result: 0.9815128878693267 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"charge"}min=? [ F "deadlock" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=0,Objy1=0,Objx2=6,Objy2=2

Prob0A: 4 iterations in 0.03 seconds (average 0.007000, setup 0.00)

Prob1E: 5 iterations in 0.06 seconds (average 0.012000, setup 0.00)

Warning: PRISM hasn't checked for zero-reward loops.
Your minimum rewards may be too low...

goal = 1, inf = 0, maybe = 274444

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=274445, nc=362413, nnz=736140, k=4] [10.9 MB]
Building sparse matrix (transition rewards)... [n=274445, nc=362413, nnz=8570, k=4] [2.5 MB]
Creating vector for state rewards... [2.1 MB]
Creating vector for inf... [2.1 MB]
Allocating iteration vectors... [2 x 2.1 MB]
TOTAL: [21.8 MB]

Starting iterations...

Iterative method: 99 iterations in 0.80 seconds (average 0.004081, setup 0.40)

Value in the initial state: 0.09828744165936071

Time for model checking: 0.904 seconds.

Result: 0.09828744165936071 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"time"}max=? [ F "deadlock" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=0,Objy1=0,Objx2=6,Objy2=2

Prob0E: 4 iterations in 0.03 seconds (average 0.007000, setup 0.00)

Prob1A: 1 iterations in 0.01 seconds (average 0.008000, setup 0.00)

goal = 1, inf = 0, maybe = 274444

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=274445, nc=362413, nnz=736140, k=4] [10.9 MB]
Building sparse matrix (transition rewards)... [n=274445, nc=362413, nnz=137077, k=4] [4.0 MB]
Creating vector for state rewards... [2.1 MB]
Creating vector for inf... [2.1 MB]
Allocating iteration vectors... [2 x 2.1 MB]
TOTAL: [23.2 MB]

Starting iterations...

Iterative method: 95 iterations in 0.92 seconds (average 0.005053, setup 0.44)

Value in the initial state: 346.03896019280864

Time for model checking: 0.969 seconds.

Result: 346.03896019280864 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"time"}min=? [ F "deadlock" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=0,Objy1=0,Objx2=6,Objy2=2

Prob0A: 4 iterations in 0.03 seconds (average 0.007000, setup 0.00)

Prob1E: 5 iterations in 0.06 seconds (average 0.011200, setup 0.00)

Warning: PRISM hasn't checked for zero-reward loops.
Your minimum rewards may be too low...

goal = 1, inf = 0, maybe = 274444

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=274445, nc=362413, nnz=736140, k=4] [10.9 MB]
Building sparse matrix (transition rewards)... [n=274445, nc=362413, nnz=137077, k=4] [4.0 MB]
Creating vector for state rewards... [2.1 MB]
Creating vector for inf... [2.1 MB]
Allocating iteration vectors... [2 x 2.1 MB]
TOTAL: [23.2 MB]

Starting iterations...

Iterative method: 104 iterations in 0.87 seconds (average 0.004346, setup 0.42)

Value in the initial state: 115.8338140258219

Time for model checking: 0.974 seconds.

Result: 115.8338140258219 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"success"}max=? [ F "deadlock" ]/Pmin=? [ F "MissionSuccessful" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=0,Objy1=0,Objx2=6,Objy2=2

Prob0E: 4 iterations in 0.02 seconds (average 0.006000, setup 0.00)

Prob1A: 1 iterations in 0.00 seconds (average 0.004000, setup 0.00)

goal = 1, inf = 0, maybe = 274444

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=274445, nc=362413, nnz=736140, k=4] [10.9 MB]
Building sparse matrix (transition rewards)... [n=274445, nc=362413, nnz=1814, k=4] [2.5 MB]
Creating vector for state rewards... [2.1 MB]
Creating vector for inf... [2.1 MB]
Allocating iteration vectors... [2 x 2.1 MB]
TOTAL: [21.7 MB]

Starting iterations...

Iterative method: 95 iterations in 0.85 seconds (average 0.004968, setup 0.38)

Prob0E: 66 iterations in 0.43 seconds (average 0.006485, setup 0.00)

Prob1A: 3 iterations in 0.05 seconds (average 0.016000, setup 0.00)

yes = 921, no = 240665, maybe = 32859

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=274445, nc=56656, nnz=163591, k=2] [2.2 MB]
Creating vector for yes... [2.1 MB]
Allocating iteration vectors... [2 x 2.1 MB]
TOTAL: [8.5 MB]

Starting iterations...

Iterative method: 92 iterations in 0.32 seconds (average 0.001739, setup 0.16)

Value in the initial state: 472.2735646554529

Time for model checking: 1.725 seconds.

Result: 472.2735646554529 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"success"}min=? [ F "deadlock" ]/Pmax=? [ F "MissionSuccessful" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=0,Objy1=0,Objx2=6,Objy2=2

Prob0A: 4 iterations in 0.10 seconds (average 0.024000, setup 0.00)

Prob1E: 5 iterations in 0.04 seconds (average 0.008800, setup 0.00)

Warning: PRISM hasn't checked for zero-reward loops.
Your minimum rewards may be too low...

goal = 1, inf = 0, maybe = 274444

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=274445, nc=362413, nnz=736140, k=4] [10.9 MB]
Building sparse matrix (transition rewards)... [n=274445, nc=362413, nnz=1814, k=4] [2.5 MB]
Creating vector for state rewards... [2.1 MB]
Creating vector for inf... [2.1 MB]
Allocating iteration vectors... [2 x 2.1 MB]
TOTAL: [21.7 MB]

Starting iterations...

Iterative method: 97 iterations in 0.86 seconds (average 0.004990, setup 0.38)

Prob0A: 61 iterations in 0.34 seconds (average 0.005574, setup 0.00)

Prob1E: 4 iterations in 0.04 seconds (average 0.009000, setup 0.00)

yes = 921, no = 235860, maybe = 37664

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=274445, nc=64637, nnz=185653, k=2] [2.4 MB]
Creating vector for yes... [2.1 MB]
Allocating iteration vectors... [2 x 2.1 MB]
TOTAL: [8.7 MB]

Starting iterations...

Iterative method: 99 iterations in 0.34 seconds (average 0.001818, setup 0.16)

Value in the initial state: 80.24353612121097

Time for model checking: 1.748 seconds.

Result: 80.24353612121097 (value in the initial state)

---------------------------------------------------------------------

Note: There were 16 warnings during computation.

