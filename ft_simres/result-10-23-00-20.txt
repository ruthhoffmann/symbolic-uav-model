PRISM
=====

Version: 4.3
Date: Tue Aug 02 12:34:39 BST 2016
Hostname: eglinton
Memory limits: cudd=1g, java(heap)=981.5m
Command line: prism uav_sim_ft_mdp.prism -s uav_mdp.props -const 'Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=0,Objy1=0,Objx2=2,Objy2=0,'

Parsing model file "uav_sim_ft_mdp.prism"...

Parsing properties file "uav_mdp.props"...

18 properties:
(1) Pmax=? [ F "MissionSuccessful" ]
(2) Pmin=? [ F "MissionSuccessful" ]
(3) Pmax=? [ F "MissionTimeUp" ]
(4) Pmin=? [ F "MissionTimeUp" ]
(5) Pmax=? [ F "Crash" ]
(6) Pmin=? [ F "Crash" ]
(7) Pmax=? [ F "AllAreaSearched" ]
(8) Pmin=? [ F "AllAreaSearched" ]
(9) Pmax=? [ F "MissionTimeUp"|"AllAreaSearched"|"Crash" ]
(10) Pmin=? [ F "MissionTimeUp"|"AllAreaSearched"|"Crash" ]
(11) R{"drop"}max=? [ F "deadlock" ]
(12) R{"drop"}min=? [ F "deadlock" ]
(13) R{"charge"}max=? [ F "deadlock" ]
(14) R{"charge"}min=? [ F "deadlock" ]
(15) R{"time"}max=? [ F "deadlock" ]
(16) R{"time"}min=? [ F "deadlock" ]
(17) R{"success"}max=? [ F "deadlock" ]/Pmin=? [ F "MissionSuccessful" ]
(18) R{"success"}min=? [ F "deadlock" ]/Pmax=? [ F "MissionSuccessful" ]

Type:        MDP
Modules:     Obj Move Robot T 
Variables:   NoOfObjs posx posy s1 retx rety b t c 

---------------------------------------------------------------------

Model checking: Pmax=? [ F "MissionSuccessful" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=0,Objy1=0,Objx2=2,Objy2=0

Building model...
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=0,Objy1=0,Objx2=2,Objy2=0

Warning: Guard for command 7 of module "Move" is never satisfied.

Warning: Guard for command 8 of module "Move" is never satisfied.

Warning: Guard for command 9 of module "Move" is never satisfied.

Warning: Guard for command 7 of module "Robot" is never satisfied.

Warning: Update 2 of command 3 of module "Robot" doesn't do anything ("(s1'=13)", line 249, column 44)

Warning: Update 2 of command 27 of module "T" doesn't do anything ("(c'=1)", line 372, column 108)

Warning: Guard for command 28 of module "T" is never satisfied.

Warning: Update 2 of command 9 of module "T" doesn't do anything ("(c'=1)", line 349, column 112)

Warning: Guard for command 10 of module "T" is never satisfied.

Warning: Update 2 of command 51 of module "T" doesn't do anything ("(c'=1)", line 403, column 113)

Warning: Guard for command 52 of module "T" is never satisfied.

Computing reachable states...

Reachability (BFS): 83 iterations in 1.17 seconds (average 0.014120, setup 0.00)

Time for model construction: 3.123 seconds.

Warning: Deadlocks detected and fixed in 1 states

Type:        MDP
States:      251546 (1 initial)
Transitions: 666695
Choices:     330896

Transition matrix: 174211 nodes (76 terminal), 666695 minterms, vars: 34r/34c/18nd

Prob0A: 42 iterations in 0.32 seconds (average 0.007714, setup 0.00)

Prob1E: 4 iterations in 0.06 seconds (average 0.014000, setup 0.00)

yes = 950, no = 214403, maybe = 36193

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=251546, nc=65765, nnz=193284, k=2] [2.5 MB]
Creating vector for yes... [1.9 MB]
Allocating iteration vectors... [2 x 1.9 MB]
TOTAL: [8.3 MB]

Starting iterations...

Iterative method: 81 iterations in 0.39 seconds (average 0.001728, setup 0.25)

Value in the initial state: 0.9841362433558185

Time for model checking: 1.036 seconds.

Result: 0.9841362433558185 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmin=? [ F "MissionSuccessful" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=0,Objy1=0,Objx2=2,Objy2=0

Prob0E: 44 iterations in 0.39 seconds (average 0.008909, setup 0.00)

Prob1A: 3 iterations in 0.05 seconds (average 0.016000, setup 0.00)

yes = 950, no = 220013, maybe = 30583

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=251546, nc=55328, nnz=162528, k=2] [2.2 MB]
Creating vector for yes... [1.9 MB]
Allocating iteration vectors... [2 x 1.9 MB]
TOTAL: [7.9 MB]

Starting iterations...

Iterative method: 75 iterations in 0.32 seconds (average 0.001653, setup 0.20)

Value in the initial state: 0.9029547688236245

Time for model checking: 1.022 seconds.

Result: 0.9029547688236245 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmax=? [ F "MissionTimeUp" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=0,Objy1=0,Objx2=2,Objy2=0

Prob0A: 54 iterations in 0.54 seconds (average 0.010000, setup 0.00)

Prob1E: 4 iterations in 0.04 seconds (average 0.010000, setup 0.00)

yes = 524, no = 157607, maybe = 93415

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=251546, nc=163471, nnz=456879, k=4] [5.6 MB]
Creating vector for yes... [1.9 MB]
Allocating iteration vectors... [2 x 1.9 MB]
TOTAL: [11.4 MB]

Starting iterations...

Iterative method: 90 iterations in 0.50 seconds (average 0.002267, setup 0.29)

Value in the initial state: 5.927835718247149E-4

Time for model checking: 1.443 seconds.

Result: 5.927835718247149E-4 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmin=? [ F "MissionTimeUp" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=0,Objy1=0,Objx2=2,Objy2=0

Prob0E: 62 iterations in 0.66 seconds (average 0.010710, setup 0.00)

Prob1A: 3 iterations in 0.06 seconds (average 0.020000, setup 0.00)

yes = 524, no = 158021, maybe = 93001

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=251546, nc=162719, nnz=455453, k=4] [5.6 MB]
Creating vector for yes... [1.9 MB]
Allocating iteration vectors... [2 x 1.9 MB]
TOTAL: [11.4 MB]

Starting iterations...

Iterative method: 106 iterations in 0.53 seconds (average 0.002226, setup 0.30)

Value in the initial state: 1.2219077545021426E-6

Time for model checking: 1.587 seconds.

Result: 1.2219077545021426E-6 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmax=? [ F "Crash" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=0,Objy1=0,Objx2=2,Objy2=0

Prob0A: 3 iterations in 0.10 seconds (average 0.032000, setup 0.00)

Prob1E: 185 iterations in 2.84 seconds (average 0.015351, setup 0.00)

yes = 123119, no = 2623, maybe = 125804

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=251546, nc=205154, nnz=540953, k=4] [6.6 MB]
Creating vector for yes... [1.9 MB]
Allocating iteration vectors... [2 x 1.9 MB]
TOTAL: [12.4 MB]

Starting iterations...

Iterative method: 86 iterations in 0.57 seconds (average 0.002419, setup 0.36)

Value in the initial state: 0.02629057546139794

Time for model checking: 4.195 seconds.

Result: 0.02629057546139794 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmin=? [ F "Crash" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=0,Objy1=0,Objx2=2,Objy2=0

Prob0E: 3 iterations in 0.07 seconds (average 0.022667, setup 0.00)

Prob1A: 39 iterations in 0.54 seconds (average 0.013949, setup 0.00)

yes = 123119, no = 2623, maybe = 125804

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=251546, nc=205154, nnz=540953, k=4] [6.6 MB]
Creating vector for yes... [1.9 MB]
Allocating iteration vectors... [2 x 1.9 MB]
TOTAL: [12.4 MB]

Starting iterations...

Iterative method: 84 iterations in 0.50 seconds (average 0.002571, setup 0.28)

Value in the initial state: 0.013834342543448964

Time for model checking: 1.613 seconds.

Result: 0.013834342543448964 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmax=? [ F "AllAreaSearched" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=0,Objy1=0,Objx2=2,Objy2=0

Prob0A: 64 iterations in 0.54 seconds (average 0.008500, setup 0.00)

Prob1E: 4 iterations in 0.05 seconds (average 0.013000, setup 0.00)

yes = 1148, no = 203600, maybe = 46798

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=251546, nc=66776, nnz=168322, k=2] [2.2 MB]
Creating vector for yes... [1.9 MB]
Allocating iteration vectors... [2 x 1.9 MB]
TOTAL: [8.0 MB]

Starting iterations...

Iterative method: 92 iterations in 0.35 seconds (average 0.001913, setup 0.17)

Value in the initial state: 0.077086515072465

Time for model checking: 1.247 seconds.

Result: 0.077086515072465 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmin=? [ F "AllAreaSearched" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=0,Objy1=0,Objx2=2,Objy2=0

Prob0E: 68 iterations in 0.48 seconds (average 0.007000, setup 0.00)

Prob1A: 3 iterations in 0.04 seconds (average 0.012000, setup 0.00)

yes = 1148, no = 208563, maybe = 41835

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=251546, nc=57612, nnz=141550, k=2] [1.9 MB]
Creating vector for yes... [1.9 MB]
Allocating iteration vectors... [2 x 1.9 MB]
TOTAL: [7.7 MB]

Starting iterations...

Iterative method: 92 iterations in 0.29 seconds (average 0.001826, setup 0.12)

Value in the initial state: 0.0018098103055416764

Time for model checking: 1.254 seconds.

Result: 0.0018098103055416764 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmax=? [ F "MissionTimeUp"|"AllAreaSearched"|"Crash" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=0,Objy1=0,Objx2=2,Objy2=0

Prob0A: 3 iterations in 0.09 seconds (average 0.030667, setup 0.00)

Prob1E: 172 iterations in 2.45 seconds (average 0.014256, setup 0.00)

yes = 220012, no = 951, maybe = 30583

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=251546, nc=55328, nnz=162528, k=2] [2.2 MB]
Creating vector for yes... [1.9 MB]
Allocating iteration vectors... [2 x 1.9 MB]
TOTAL: [7.9 MB]

Starting iterations...

Iterative method: 70 iterations in 0.33 seconds (average 0.001657, setup 0.21)

Value in the initial state: 0.09704479987138283

Time for model checking: 3.808 seconds.

Result: 0.09704479987138283 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmin=? [ F "MissionTimeUp"|"AllAreaSearched"|"Crash" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=0,Objy1=0,Objx2=2,Objy2=0

Prob0E: 3 iterations in 0.05 seconds (average 0.016000, setup 0.00)

Prob1A: 42 iterations in 0.42 seconds (average 0.010000, setup 0.00)

yes = 214402, no = 951, maybe = 36193

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=251546, nc=65765, nnz=193284, k=2] [2.5 MB]
Creating vector for yes... [1.9 MB]
Allocating iteration vectors... [2 x 1.9 MB]
TOTAL: [8.3 MB]

Starting iterations...

Iterative method: 80 iterations in 0.32 seconds (average 0.001750, setup 0.18)

Value in the initial state: 0.015862423535826696

Time for model checking: 1.265 seconds.

Result: 0.015862423535826696 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"drop"}max=? [ F "deadlock" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=0,Objy1=0,Objx2=2,Objy2=0

Prob0E: 4 iterations in 0.08 seconds (average 0.020000, setup 0.00)

Prob1A: 1 iterations in 0.01 seconds (average 0.008000, setup 0.00)

goal = 1, inf = 0, maybe = 251545

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=251546, nc=330895, nnz=666694, k=4] [9.9 MB]
Building sparse matrix (transition rewards)... [n=251546, nc=330895, nnz=0, k=4] [2.2 MB]
Creating vector for state rewards... [1.9 MB]
Creating vector for inf... [1.9 MB]
Allocating iteration vectors... [2 x 1.9 MB]
TOTAL: [19.7 MB]

Starting iterations...

Iterative method: 77 iterations in 0.83 seconds (average 0.003740, setup 0.54)

Value in the initial state: 0.23831171152301994

Time for model checking: 1.603 seconds.

Result: 0.23831171152301994 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"drop"}min=? [ F "deadlock" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=0,Objy1=0,Objx2=2,Objy2=0

Prob0A: 4 iterations in 0.09 seconds (average 0.022000, setup 0.00)

Prob1E: 5 iterations in 0.06 seconds (average 0.011200, setup 0.00)

Warning: PRISM hasn't checked for zero-reward loops.
Your minimum rewards may be too low...

goal = 1, inf = 0, maybe = 251545

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=251546, nc=330895, nnz=666694, k=4] [9.9 MB]
Building sparse matrix (transition rewards)... [n=251546, nc=330895, nnz=0, k=4] [2.2 MB]
Creating vector for state rewards... [1.9 MB]
Creating vector for inf... [1.9 MB]
Allocating iteration vectors... [2 x 1.9 MB]
TOTAL: [19.7 MB]

Starting iterations...

Iterative method: 68 iterations in 0.61 seconds (average 0.003647, setup 0.36)

Value in the initial state: 0.21507434248061197

Time for model checking: 1.027 seconds.

Result: 0.21507434248061197 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"charge"}max=? [ F "deadlock" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=0,Objy1=0,Objx2=2,Objy2=0

Prob0E: 4 iterations in 0.02 seconds (average 0.006000, setup 0.00)

Prob1A: 1 iterations in 0.00 seconds (average 0.004000, setup 0.00)

goal = 1, inf = 0, maybe = 251545

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=251546, nc=330895, nnz=666694, k=4] [9.9 MB]
Building sparse matrix (transition rewards)... [n=251546, nc=330895, nnz=7514, k=4] [2.3 MB]
Creating vector for state rewards... [1.9 MB]
Creating vector for inf... [1.9 MB]
Allocating iteration vectors... [2 x 1.9 MB]
TOTAL: [19.8 MB]

Starting iterations...

Iterative method: 73 iterations in 0.64 seconds (average 0.003726, setup 0.36)

Value in the initial state: 0.21614731470902673

Time for model checking: 1.056 seconds.

Result: 0.21614731470902673 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"charge"}min=? [ F "deadlock" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=0,Objy1=0,Objx2=2,Objy2=0

Prob0A: 4 iterations in 0.02 seconds (average 0.006000, setup 0.00)

Prob1E: 5 iterations in 0.04 seconds (average 0.008800, setup 0.00)

Warning: PRISM hasn't checked for zero-reward loops.
Your minimum rewards may be too low...

goal = 1, inf = 0, maybe = 251545

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=251546, nc=330895, nnz=666694, k=4] [9.9 MB]
Building sparse matrix (transition rewards)... [n=251546, nc=330895, nnz=7514, k=4] [2.3 MB]
Creating vector for state rewards... [1.9 MB]
Creating vector for inf... [1.9 MB]
Allocating iteration vectors... [2 x 1.9 MB]
TOTAL: [19.8 MB]

Starting iterations...

Iterative method: 85 iterations in 0.68 seconds (average 0.003765, setup 0.36)

Value in the initial state: 0.015385303638172703

Time for model checking: 0.893 seconds.

Result: 0.015385303638172703 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"time"}max=? [ F "deadlock" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=0,Objy1=0,Objx2=2,Objy2=0

Prob0E: 4 iterations in 0.02 seconds (average 0.005000, setup 0.00)

Prob1A: 1 iterations in 0.01 seconds (average 0.008000, setup 0.00)

goal = 1, inf = 0, maybe = 251545

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=251546, nc=330895, nnz=666694, k=4] [9.9 MB]
Building sparse matrix (transition rewards)... [n=251546, nc=330895, nnz=125740, k=4] [3.7 MB]
Creating vector for state rewards... [1.9 MB]
Creating vector for inf... [1.9 MB]
Allocating iteration vectors... [2 x 1.9 MB]
TOTAL: [21.2 MB]

Starting iterations...

Iterative method: 91 iterations in 0.75 seconds (average 0.003824, setup 0.40)

Value in the initial state: 145.1472257954126

Time for model checking: 0.9 seconds.

Result: 145.1472257954126 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"time"}min=? [ F "deadlock" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=0,Objy1=0,Objx2=2,Objy2=0

Prob0A: 4 iterations in 0.03 seconds (average 0.007000, setup 0.00)

Prob1E: 5 iterations in 0.05 seconds (average 0.009600, setup 0.00)

Warning: PRISM hasn't checked for zero-reward loops.
Your minimum rewards may be too low...

goal = 1, inf = 0, maybe = 251545

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=251546, nc=330895, nnz=666694, k=4] [9.9 MB]
Building sparse matrix (transition rewards)... [n=251546, nc=330895, nnz=125740, k=4] [3.7 MB]
Creating vector for state rewards... [1.9 MB]
Creating vector for inf... [1.9 MB]
Allocating iteration vectors... [2 x 1.9 MB]
TOTAL: [21.2 MB]

Starting iterations...

Iterative method: 93 iterations in 0.76 seconds (average 0.003828, setup 0.40)

Value in the initial state: 76.76165705592389

Time for model checking: 0.899 seconds.

Result: 76.76165705592389 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"success"}max=? [ F "deadlock" ]/Pmin=? [ F "MissionSuccessful" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=0,Objy1=0,Objx2=2,Objy2=0

Prob0E: 4 iterations in 0.03 seconds (average 0.007000, setup 0.00)

Prob1A: 1 iterations in 0.00 seconds (average 0.004000, setup 0.00)

goal = 1, inf = 0, maybe = 251545

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=251546, nc=330895, nnz=666694, k=4] [9.9 MB]
Building sparse matrix (transition rewards)... [n=251546, nc=330895, nnz=1872, k=4] [2.2 MB]
Creating vector for state rewards... [1.9 MB]
Creating vector for inf... [1.9 MB]
Allocating iteration vectors... [2 x 1.9 MB]
TOTAL: [19.8 MB]

Starting iterations...

Iterative method: 83 iterations in 0.67 seconds (average 0.003663, setup 0.36)

Prob0E: 44 iterations in 0.31 seconds (average 0.007000, setup 0.00)

Prob1A: 3 iterations in 0.05 seconds (average 0.016000, setup 0.00)

yes = 950, no = 220013, maybe = 30583

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=251546, nc=55328, nnz=162528, k=2] [2.2 MB]
Creating vector for yes... [1.9 MB]
Allocating iteration vectors... [2 x 1.9 MB]
TOTAL: [7.9 MB]

Starting iterations...

Iterative method: 75 iterations in 0.31 seconds (average 0.001600, setup 0.19)

Value in the initial state: 157.60517325062682

Time for model checking: 1.683 seconds.

Result: 157.60517325062682 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"success"}min=? [ F "deadlock" ]/Pmax=? [ F "MissionSuccessful" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=0,Objy1=0,Objx2=2,Objy2=0

Prob0A: 4 iterations in 0.04 seconds (average 0.009000, setup 0.00)

Prob1E: 5 iterations in 0.04 seconds (average 0.008800, setup 0.00)

Warning: PRISM hasn't checked for zero-reward loops.
Your minimum rewards may be too low...

goal = 1, inf = 0, maybe = 251545

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=251546, nc=330895, nnz=666694, k=4] [9.9 MB]
Building sparse matrix (transition rewards)... [n=251546, nc=330895, nnz=1872, k=4] [2.2 MB]
Creating vector for state rewards... [1.9 MB]
Creating vector for inf... [1.9 MB]
Allocating iteration vectors... [2 x 1.9 MB]
TOTAL: [19.8 MB]

Starting iterations...

Iterative method: 81 iterations in 0.72 seconds (average 0.003753, setup 0.42)

Prob0A: 42 iterations in 0.25 seconds (average 0.006000, setup 0.00)

Prob1E: 4 iterations in 0.03 seconds (average 0.008000, setup 0.00)

yes = 950, no = 214403, maybe = 36193

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=251546, nc=65765, nnz=193284, k=2] [2.5 MB]
Creating vector for yes... [1.9 MB]
Allocating iteration vectors... [2 x 1.9 MB]
TOTAL: [8.3 MB]

Starting iterations...

Iterative method: 81 iterations in 0.32 seconds (average 0.001778, setup 0.18)

Value in the initial state: 71.88043958651367

Time for model checking: 1.51 seconds.

Result: 71.88043958651367 (value in the initial state)

---------------------------------------------------------------------

Note: There were 16 warnings during computation.

