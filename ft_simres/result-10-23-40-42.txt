PRISM
=====

Version: 4.3
Date: Tue Aug 02 12:41:28 BST 2016
Hostname: eglinton
Memory limits: cudd=1g, java(heap)=981.5m
Command line: prism uav_sim_ft_mdp.prism -s uav_mdp.props -const 'Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=4,Objy1=0,Objx2=4,Objy2=2,'

Parsing model file "uav_sim_ft_mdp.prism"...

Parsing properties file "uav_mdp.props"...

18 properties:
(1) Pmax=? [ F "MissionSuccessful" ]
(2) Pmin=? [ F "MissionSuccessful" ]
(3) Pmax=? [ F "MissionTimeUp" ]
(4) Pmin=? [ F "MissionTimeUp" ]
(5) Pmax=? [ F "Crash" ]
(6) Pmin=? [ F "Crash" ]
(7) Pmax=? [ F "AllAreaSearched" ]
(8) Pmin=? [ F "AllAreaSearched" ]
(9) Pmax=? [ F "MissionTimeUp"|"AllAreaSearched"|"Crash" ]
(10) Pmin=? [ F "MissionTimeUp"|"AllAreaSearched"|"Crash" ]
(11) R{"drop"}max=? [ F "deadlock" ]
(12) R{"drop"}min=? [ F "deadlock" ]
(13) R{"charge"}max=? [ F "deadlock" ]
(14) R{"charge"}min=? [ F "deadlock" ]
(15) R{"time"}max=? [ F "deadlock" ]
(16) R{"time"}min=? [ F "deadlock" ]
(17) R{"success"}max=? [ F "deadlock" ]/Pmin=? [ F "MissionSuccessful" ]
(18) R{"success"}min=? [ F "deadlock" ]/Pmax=? [ F "MissionSuccessful" ]

Type:        MDP
Modules:     Obj Move Robot T 
Variables:   NoOfObjs posx posy s1 retx rety b t c 

---------------------------------------------------------------------

Model checking: Pmax=? [ F "MissionSuccessful" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=4,Objy1=0,Objx2=4,Objy2=2

Building model...
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=4,Objy1=0,Objx2=4,Objy2=2

Warning: Guard for command 7 of module "Move" is never satisfied.

Warning: Guard for command 8 of module "Move" is never satisfied.

Warning: Guard for command 9 of module "Move" is never satisfied.

Warning: Guard for command 7 of module "Robot" is never satisfied.

Warning: Update 2 of command 3 of module "Robot" doesn't do anything ("(s1'=13)", line 249, column 44)

Warning: Update 2 of command 27 of module "T" doesn't do anything ("(c'=1)", line 372, column 108)

Warning: Guard for command 28 of module "T" is never satisfied.

Warning: Update 2 of command 9 of module "T" doesn't do anything ("(c'=1)", line 349, column 112)

Warning: Guard for command 10 of module "T" is never satisfied.

Warning: Update 2 of command 51 of module "T" doesn't do anything ("(c'=1)", line 403, column 113)

Warning: Guard for command 52 of module "T" is never satisfied.

Computing reachable states...

Reachability (BFS): 76 iterations in 0.86 seconds (average 0.011316, setup 0.00)

Time for model construction: 1.892 seconds.

Warning: Deadlocks detected and fixed in 1 states

Type:        MDP
States:      224892 (1 initial)
Transitions: 589489
Choices:     295204

Transition matrix: 174396 nodes (76 terminal), 589489 minterms, vars: 34r/34c/18nd

Prob0A: 53 iterations in 0.36 seconds (average 0.006717, setup 0.00)

Prob1E: 4 iterations in 0.04 seconds (average 0.011000, setup 0.00)

yes = 945, no = 191289, maybe = 32658

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=224892, nc=56012, nnz=158492, k=2] [2.1 MB]
Creating vector for yes... [1.7 MB]
Allocating iteration vectors... [2 x 1.7 MB]
TOTAL: [7.2 MB]

Starting iterations...

Iterative method: 92 iterations in 0.35 seconds (average 0.001565, setup 0.20)

Value in the initial state: 0.979273936927742

Time for model checking: 0.778 seconds.

Result: 0.979273936927742 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmin=? [ F "MissionSuccessful" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=4,Objy1=0,Objx2=4,Objy2=2

Prob0E: 55 iterations in 0.40 seconds (average 0.007345, setup 0.00)

Prob1A: 3 iterations in 0.04 seconds (average 0.014667, setup 0.00)

yes = 945, no = 196475, maybe = 27472

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=224892, nc=47069, nnz=132348, k=2] [1.8 MB]
Creating vector for yes... [1.7 MB]
Allocating iteration vectors... [2 x 1.7 MB]
TOTAL: [6.9 MB]

Starting iterations...

Iterative method: 88 iterations in 0.30 seconds (average 0.001455, setup 0.17)

Value in the initial state: 0.7418407988453166

Time for model checking: 0.762 seconds.

Result: 0.7418407988453166 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmax=? [ F "MissionTimeUp" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=4,Objy1=0,Objx2=4,Objy2=2

Prob0A: 57 iterations in 0.51 seconds (average 0.008982, setup 0.00)

Prob1E: 4 iterations in 0.04 seconds (average 0.009000, setup 0.00)

yes = 476, no = 133481, maybe = 90935

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=224892, nc=151354, nnz=412485, k=4] [5.1 MB]
Creating vector for yes... [1.7 MB]
Allocating iteration vectors... [2 x 1.7 MB]
TOTAL: [10.2 MB]

Starting iterations...

Iterative method: 90 iterations in 0.44 seconds (average 0.001956, setup 0.26)

Value in the initial state: 8.661409654188173E-4

Time for model checking: 1.007 seconds.

Result: 8.661409654188173E-4 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmin=? [ F "MissionTimeUp" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=4,Objy1=0,Objx2=4,Objy2=2

Prob0E: 65 iterations in 0.66 seconds (average 0.010215, setup 0.00)

Prob1A: 3 iterations in 0.04 seconds (average 0.013333, setup 0.00)

yes = 476, no = 133819, maybe = 90597

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=224892, nc=150754, nnz=411215, k=4] [5.1 MB]
Creating vector for yes... [1.7 MB]
Allocating iteration vectors... [2 x 1.7 MB]
TOTAL: [10.2 MB]

Starting iterations...

Iterative method: 105 iterations in 0.47 seconds (average 0.002019, setup 0.26)

Value in the initial state: 5.162062421215573E-6

Time for model checking: 1.206 seconds.

Result: 5.162062421215573E-6 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmax=? [ F "Crash" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=4,Objy1=0,Objx2=4,Objy2=2

Prob0A: 3 iterations in 0.08 seconds (average 0.025333, setup 0.00)

Prob1E: 200 iterations in 2.41 seconds (average 0.012060, setup 0.00)

yes = 110219, no = 2668, maybe = 112005

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=224892, nc=182317, nnz=476602, k=4] [5.8 MB]
Creating vector for yes... [1.7 MB]
Allocating iteration vectors... [2 x 1.7 MB]
TOTAL: [11.0 MB]

Starting iterations...

Iterative method: 88 iterations in 0.48 seconds (average 0.002045, setup 0.30)

Value in the initial state: 0.03182065702778477

Time for model checking: 2.999 seconds.

Result: 0.03182065702778477 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmin=? [ F "Crash" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=4,Objy1=0,Objx2=4,Objy2=2

Prob0E: 3 iterations in 0.05 seconds (average 0.017333, setup 0.00)

Prob1A: 48 iterations in 0.50 seconds (average 0.010500, setup 0.00)

yes = 110219, no = 2668, maybe = 112005

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=224892, nc=182317, nnz=476602, k=4] [5.8 MB]
Creating vector for yes... [1.7 MB]
Allocating iteration vectors... [2 x 1.7 MB]
TOTAL: [11.0 MB]

Starting iterations...

Iterative method: 92 iterations in 0.43 seconds (average 0.002130, setup 0.23)

Value in the initial state: 0.018751130318984566

Time for model checking: 1.012 seconds.

Result: 0.018751130318984566 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmax=? [ F "AllAreaSearched" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=4,Objy1=0,Objx2=4,Objy2=2

Prob0A: 59 iterations in 0.44 seconds (average 0.007390, setup 0.00)

Prob1E: 4 iterations in 0.04 seconds (average 0.010000, setup 0.00)

yes = 1246, no = 178491, maybe = 45155

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=224892, nc=70137, nnz=184749, k=2] [2.4 MB]
Creating vector for yes... [1.7 MB]
Allocating iteration vectors... [2 x 1.7 MB]
TOTAL: [7.5 MB]

Starting iterations...

Iterative method: 87 iterations in 0.30 seconds (average 0.001563, setup 0.16)

Value in the initial state: 0.22963201036833603

Time for model checking: 0.785 seconds.

Result: 0.22963201036833603 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmin=? [ F "AllAreaSearched" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=4,Objy1=0,Objx2=4,Objy2=2

Prob0E: 63 iterations in 0.34 seconds (average 0.005397, setup 0.00)

Prob1A: 3 iterations in 0.03 seconds (average 0.009333, setup 0.00)

yes = 1246, no = 185112, maybe = 38534

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=224892, nc=59630, nnz=156066, k=2] [2.1 MB]
Creating vector for yes... [1.7 MB]
Allocating iteration vectors... [2 x 1.7 MB]
TOTAL: [7.2 MB]

Starting iterations...

Iterative method: 92 iterations in 0.26 seconds (average 0.001522, setup 0.12)

Value in the initial state: 0.0017592695686994805

Time for model checking: 0.634 seconds.

Result: 0.0017592695686994805 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmax=? [ F "MissionTimeUp"|"AllAreaSearched"|"Crash" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=4,Objy1=0,Objx2=4,Objy2=2

Prob0A: 3 iterations in 0.10 seconds (average 0.032000, setup 0.00)

Prob1E: 216 iterations in 2.26 seconds (average 0.010444, setup 0.00)

yes = 196474, no = 946, maybe = 27472

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=224892, nc=47069, nnz=132348, k=2] [1.8 MB]
Creating vector for yes... [1.7 MB]
Allocating iteration vectors... [2 x 1.7 MB]
TOTAL: [6.9 MB]

Starting iterations...

Iterative method: 81 iterations in 0.29 seconds (average 0.001432, setup 0.17)

Value in the initial state: 0.2581589566275233

Time for model checking: 2.645 seconds.

Result: 0.2581589566275233 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmin=? [ F "MissionTimeUp"|"AllAreaSearched"|"Crash" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=4,Objy1=0,Objx2=4,Objy2=2

Prob0E: 3 iterations in 0.06 seconds (average 0.020000, setup 0.00)

Prob1A: 53 iterations in 0.42 seconds (average 0.007849, setup 0.00)

yes = 191288, no = 946, maybe = 32658

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=224892, nc=56012, nnz=158492, k=2] [2.1 MB]
Creating vector for yes... [1.7 MB]
Allocating iteration vectors... [2 x 1.7 MB]
TOTAL: [7.2 MB]

Starting iterations...

Iterative method: 91 iterations in 0.29 seconds (average 0.001495, setup 0.15)

Value in the initial state: 0.020725054147358334

Time for model checking: 0.79 seconds.

Result: 0.020725054147358334 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"drop"}max=? [ F "deadlock" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=4,Objy1=0,Objx2=4,Objy2=2

Prob0E: 4 iterations in 0.06 seconds (average 0.015000, setup 0.00)

Prob1A: 1 iterations in 0.00 seconds (average 0.004000, setup 0.00)

goal = 1, inf = 0, maybe = 224891

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=224892, nc=295203, nnz=589488, k=4] [8.7 MB]
Building sparse matrix (transition rewards)... [n=224892, nc=295203, nnz=0, k=4] [2.0 MB]
Creating vector for state rewards... [1.7 MB]
Creating vector for inf... [1.7 MB]
Allocating iteration vectors... [2 x 1.7 MB]
TOTAL: [17.6 MB]

Starting iterations...

Iterative method: 89 iterations in 0.68 seconds (average 0.003236, setup 0.40)

Value in the initial state: 0.21056912864364746

Time for model checking: 0.767 seconds.

Result: 0.21056912864364746 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"drop"}min=? [ F "deadlock" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=4,Objy1=0,Objx2=4,Objy2=2

Prob0A: 4 iterations in 0.03 seconds (average 0.008000, setup 0.00)

Prob1E: 5 iterations in 0.04 seconds (average 0.008000, setup 0.00)

Warning: PRISM hasn't checked for zero-reward loops.
Your minimum rewards may be too low...

goal = 1, inf = 0, maybe = 224891

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=224892, nc=295203, nnz=589488, k=4] [8.7 MB]
Building sparse matrix (transition rewards)... [n=224892, nc=295203, nnz=0, k=4] [2.0 MB]
Creating vector for state rewards... [1.7 MB]
Creating vector for inf... [1.7 MB]
Allocating iteration vectors... [2 x 1.7 MB]
TOTAL: [17.6 MB]

Starting iterations...

Iterative method: 80 iterations in 0.56 seconds (average 0.003250, setup 0.30)

Value in the initial state: 0.18449321814721428

Time for model checking: 0.65 seconds.

Result: 0.18449321814721428 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"charge"}max=? [ F "deadlock" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=4,Objy1=0,Objx2=4,Objy2=2

Prob0E: 4 iterations in 0.02 seconds (average 0.006000, setup 0.00)

Prob1A: 1 iterations in 0.00 seconds (average 0.004000, setup 0.00)

goal = 1, inf = 0, maybe = 224891

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=224892, nc=295203, nnz=589488, k=4] [8.7 MB]
Building sparse matrix (transition rewards)... [n=224892, nc=295203, nnz=6458, k=4] [2.1 MB]
Creating vector for state rewards... [1.7 MB]
Creating vector for inf... [1.7 MB]
Allocating iteration vectors... [2 x 1.7 MB]
TOTAL: [17.7 MB]

Starting iterations...

Iterative method: 87 iterations in 0.59 seconds (average 0.003218, setup 0.31)

Value in the initial state: 0.291446524133226

Time for model checking: 0.632 seconds.

Result: 0.291446524133226 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"charge"}min=? [ F "deadlock" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=4,Objy1=0,Objx2=4,Objy2=2

Prob0A: 4 iterations in 0.02 seconds (average 0.005000, setup 0.00)

Prob1E: 5 iterations in 0.04 seconds (average 0.008800, setup 0.00)

Warning: PRISM hasn't checked for zero-reward loops.
Your minimum rewards may be too low...

goal = 1, inf = 0, maybe = 224891

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=224892, nc=295203, nnz=589488, k=4] [8.7 MB]
Building sparse matrix (transition rewards)... [n=224892, nc=295203, nnz=6458, k=4] [2.1 MB]
Creating vector for state rewards... [1.7 MB]
Creating vector for inf... [1.7 MB]
Allocating iteration vectors... [2 x 1.7 MB]
TOTAL: [17.7 MB]

Starting iterations...

Iterative method: 92 iterations in 0.61 seconds (average 0.003261, setup 0.31)

Value in the initial state: 0.057534728601459764

Time for model checking: 0.693 seconds.

Result: 0.057534728601459764 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"time"}max=? [ F "deadlock" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=4,Objy1=0,Objx2=4,Objy2=2

Prob0E: 4 iterations in 0.02 seconds (average 0.005000, setup 0.00)

Prob1A: 1 iterations in 0.00 seconds (average 0.004000, setup 0.00)

goal = 1, inf = 0, maybe = 224891

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=224892, nc=295203, nnz=589488, k=4] [8.7 MB]
Building sparse matrix (transition rewards)... [n=224892, nc=295203, nnz=112885, k=4] [3.3 MB]
Creating vector for state rewards... [1.7 MB]
Creating vector for inf... [1.7 MB]
Allocating iteration vectors... [2 x 1.7 MB]
TOTAL: [18.9 MB]

Starting iterations...

Iterative method: 90 iterations in 0.65 seconds (average 0.003244, setup 0.36)

Value in the initial state: 175.5252848417512

Time for model checking: 0.689 seconds.

Result: 175.5252848417512 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"time"}min=? [ F "deadlock" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=4,Objy1=0,Objx2=4,Objy2=2

Prob0A: 4 iterations in 0.02 seconds (average 0.005000, setup 0.00)

Prob1E: 5 iterations in 0.04 seconds (average 0.008000, setup 0.00)

Warning: PRISM hasn't checked for zero-reward loops.
Your minimum rewards may be too low...

goal = 1, inf = 0, maybe = 224891

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=224892, nc=295203, nnz=589488, k=4] [8.7 MB]
Building sparse matrix (transition rewards)... [n=224892, nc=295203, nnz=112885, k=4] [3.3 MB]
Creating vector for state rewards... [1.7 MB]
Creating vector for inf... [1.7 MB]
Allocating iteration vectors... [2 x 1.7 MB]
TOTAL: [18.9 MB]

Starting iterations...

Iterative method: 99 iterations in 0.67 seconds (average 0.003273, setup 0.35)

Value in the initial state: 103.86228095604335

Time for model checking: 0.754 seconds.

Result: 103.86228095604335 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"success"}max=? [ F "deadlock" ]/Pmin=? [ F "MissionSuccessful" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=4,Objy1=0,Objx2=4,Objy2=2

Prob0E: 4 iterations in 0.02 seconds (average 0.006000, setup 0.00)

Prob1A: 1 iterations in 0.00 seconds (average 0.004000, setup 0.00)

goal = 1, inf = 0, maybe = 224891

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=224892, nc=295203, nnz=589488, k=4] [8.7 MB]
Building sparse matrix (transition rewards)... [n=224892, nc=295203, nnz=1862, k=4] [2.0 MB]
Creating vector for state rewards... [1.7 MB]
Creating vector for inf... [1.7 MB]
Allocating iteration vectors... [2 x 1.7 MB]
TOTAL: [17.6 MB]

Starting iterations...

Iterative method: 93 iterations in 0.61 seconds (average 0.003140, setup 0.32)

Prob0E: 55 iterations in 0.44 seconds (average 0.008073, setup 0.00)

Prob1A: 3 iterations in 0.04 seconds (average 0.013333, setup 0.00)

yes = 945, no = 196475, maybe = 27472

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=224892, nc=47069, nnz=132348, k=2] [1.8 MB]
Creating vector for yes... [1.7 MB]
Allocating iteration vectors... [2 x 1.7 MB]
TOTAL: [6.9 MB]

Starting iterations...

Iterative method: 88 iterations in 0.26 seconds (average 0.001409, setup 0.14)

Value in the initial state: 231.7420296009797

Time for model checking: 1.416 seconds.

Result: 231.7420296009797 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"success"}min=? [ F "deadlock" ]/Pmax=? [ F "MissionSuccessful" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=4,Objy1=0,Objx2=4,Objy2=2

Prob0A: 4 iterations in 0.10 seconds (average 0.025000, setup 0.00)

Prob1E: 5 iterations in 0.05 seconds (average 0.009600, setup 0.00)

Warning: PRISM hasn't checked for zero-reward loops.
Your minimum rewards may be too low...

goal = 1, inf = 0, maybe = 224891

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=224892, nc=295203, nnz=589488, k=4] [8.7 MB]
Building sparse matrix (transition rewards)... [n=224892, nc=295203, nnz=1862, k=4] [2.0 MB]
Creating vector for state rewards... [1.7 MB]
Creating vector for inf... [1.7 MB]
Allocating iteration vectors... [2 x 1.7 MB]
TOTAL: [17.6 MB]

Starting iterations...

Iterative method: 91 iterations in 0.67 seconds (average 0.003253, setup 0.37)

Prob0A: 53 iterations in 0.32 seconds (average 0.006038, setup 0.00)

Prob1E: 4 iterations in 0.04 seconds (average 0.009000, setup 0.00)

yes = 945, no = 191289, maybe = 32658

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=224892, nc=56012, nnz=158492, k=2] [2.1 MB]
Creating vector for yes... [1.7 MB]
Allocating iteration vectors... [2 x 1.7 MB]
TOTAL: [7.2 MB]

Starting iterations...

Iterative method: 92 iterations in 0.29 seconds (average 0.001478, setup 0.16)

Value in the initial state: 80.07838299528288

Time for model checking: 1.495 seconds.

Result: 80.07838299528288 (value in the initial state)

---------------------------------------------------------------------

Note: There were 16 warnings during computation.

