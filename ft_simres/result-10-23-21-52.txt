PRISM
=====

Version: 4.3
Date: Tue Aug 02 12:38:41 BST 2016
Hostname: eglinton
Memory limits: cudd=1g, java(heap)=981.5m
Command line: prism uav_sim_ft_mdp.prism -s uav_mdp.props -const 'Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=2,Objy1=1,Objx2=5,Objy2=2,'

Parsing model file "uav_sim_ft_mdp.prism"...

Parsing properties file "uav_mdp.props"...

18 properties:
(1) Pmax=? [ F "MissionSuccessful" ]
(2) Pmin=? [ F "MissionSuccessful" ]
(3) Pmax=? [ F "MissionTimeUp" ]
(4) Pmin=? [ F "MissionTimeUp" ]
(5) Pmax=? [ F "Crash" ]
(6) Pmin=? [ F "Crash" ]
(7) Pmax=? [ F "AllAreaSearched" ]
(8) Pmin=? [ F "AllAreaSearched" ]
(9) Pmax=? [ F "MissionTimeUp"|"AllAreaSearched"|"Crash" ]
(10) Pmin=? [ F "MissionTimeUp"|"AllAreaSearched"|"Crash" ]
(11) R{"drop"}max=? [ F "deadlock" ]
(12) R{"drop"}min=? [ F "deadlock" ]
(13) R{"charge"}max=? [ F "deadlock" ]
(14) R{"charge"}min=? [ F "deadlock" ]
(15) R{"time"}max=? [ F "deadlock" ]
(16) R{"time"}min=? [ F "deadlock" ]
(17) R{"success"}max=? [ F "deadlock" ]/Pmin=? [ F "MissionSuccessful" ]
(18) R{"success"}min=? [ F "deadlock" ]/Pmax=? [ F "MissionSuccessful" ]

Type:        MDP
Modules:     Obj Move Robot T 
Variables:   NoOfObjs posx posy s1 retx rety b t c 

---------------------------------------------------------------------

Model checking: Pmax=? [ F "MissionSuccessful" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=2,Objy1=1,Objx2=5,Objy2=2

Building model...
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=2,Objy1=1,Objx2=5,Objy2=2

Warning: Guard for command 7 of module "Move" is never satisfied.

Warning: Guard for command 8 of module "Move" is never satisfied.

Warning: Guard for command 9 of module "Move" is never satisfied.

Warning: Guard for command 7 of module "Robot" is never satisfied.

Warning: Update 2 of command 3 of module "Robot" doesn't do anything ("(s1'=13)", line 249, column 44)

Warning: Update 2 of command 27 of module "T" doesn't do anything ("(c'=1)", line 372, column 108)

Warning: Guard for command 28 of module "T" is never satisfied.

Warning: Update 2 of command 9 of module "T" doesn't do anything ("(c'=1)", line 349, column 112)

Warning: Guard for command 10 of module "T" is never satisfied.

Warning: Update 2 of command 51 of module "T" doesn't do anything ("(c'=1)", line 403, column 113)

Warning: Guard for command 52 of module "T" is never satisfied.

Computing reachable states...

Reachability (BFS): 77 iterations in 0.79 seconds (average 0.010234, setup 0.00)

Time for model construction: 1.942 seconds.

Warning: Deadlocks detected and fixed in 1 states

Type:        MDP
States:      213071 (1 initial)
Transitions: 582679
Choices:     285443

Transition matrix: 170434 nodes (76 terminal), 582679 minterms, vars: 34r/34c/18nd

Prob0A: 49 iterations in 0.40 seconds (average 0.008163, setup 0.00)

Prob1E: 4 iterations in 0.06 seconds (average 0.015000, setup 0.00)

yes = 937, no = 178395, maybe = 33739

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=213071, nc=59382, nnz=170911, k=2] [2.2 MB]
Creating vector for yes... [1.6 MB]
Allocating iteration vectors... [2 x 1.6 MB]
TOTAL: [7.1 MB]

Starting iterations...

Iterative method: 94 iterations in 0.37 seconds (average 0.001489, setup 0.23)

Value in the initial state: 0.978833758103042

Time for model checking: 0.862 seconds.

Result: 0.978833758103042 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmin=? [ F "MissionSuccessful" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=2,Objy1=1,Objx2=5,Objy2=2

Prob0E: 49 iterations in 0.44 seconds (average 0.009061, setup 0.00)

Prob1A: 3 iterations in 0.06 seconds (average 0.020000, setup 0.00)

yes = 937, no = 183097, maybe = 29037

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=213071, nc=50963, nnz=146467, k=2] [1.9 MB]
Creating vector for yes... [1.6 MB]
Allocating iteration vectors... [2 x 1.6 MB]
TOTAL: [6.8 MB]

Starting iterations...

Iterative method: 87 iterations in 0.32 seconds (average 0.001379, setup 0.20)

Value in the initial state: 0.7373952813252735

Time for model checking: 0.842 seconds.

Result: 0.7373952813252735 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmax=? [ F "MissionTimeUp" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=2,Objy1=1,Objx2=5,Objy2=2

Prob0A: 63 iterations in 0.56 seconds (average 0.008825, setup 0.00)

Prob1E: 4 iterations in 0.04 seconds (average 0.011000, setup 0.00)

yes = 392, no = 126582, maybe = 86097

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=213071, nc=148516, nnz=413488, k=4] [5.1 MB]
Creating vector for yes... [1.6 MB]
Allocating iteration vectors... [2 x 1.6 MB]
TOTAL: [10.0 MB]

Starting iterations...

Iterative method: 90 iterations in 0.45 seconds (average 0.001911, setup 0.28)

Value in the initial state: 0.0011718424290927396

Time for model checking: 1.063 seconds.

Result: 0.0011718424290927396 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmin=? [ F "MissionTimeUp" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=2,Objy1=1,Objx2=5,Objy2=2

Prob0E: 70 iterations in 0.72 seconds (average 0.010286, setup 0.00)

Prob1A: 3 iterations in 0.05 seconds (average 0.016000, setup 0.00)

yes = 392, no = 126958, maybe = 85721

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=213071, nc=147840, nnz=412140, k=4] [5.1 MB]
Creating vector for yes... [1.6 MB]
Allocating iteration vectors... [2 x 1.6 MB]
TOTAL: [9.9 MB]

Starting iterations...

Iterative method: 105 iterations in 0.47 seconds (average 0.001905, setup 0.27)

Value in the initial state: 5.668860996337255E-6

Time for model checking: 1.258 seconds.

Result: 5.668860996337255E-6 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmax=? [ F "Crash" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=2,Objy1=1,Objx2=5,Objy2=2

Prob0A: 3 iterations in 0.11 seconds (average 0.037333, setup 0.00)

Prob1E: 188 iterations in 2.60 seconds (average 0.013830, setup 0.00)

yes = 104313, no = 2568, maybe = 106190

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=213071, nc=178562, nnz=475798, k=4] [5.8 MB]
Creating vector for yes... [1.6 MB]
Allocating iteration vectors... [2 x 1.6 MB]
TOTAL: [10.7 MB]

Starting iterations...

Iterative method: 87 iterations in 0.49 seconds (average 0.002069, setup 0.31)

Value in the initial state: 0.032024330555171225

Time for model checking: 3.206 seconds.

Result: 0.032024330555171225 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmin=? [ F "Crash" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=2,Objy1=1,Objx2=5,Objy2=2

Prob0E: 3 iterations in 0.06 seconds (average 0.018667, setup 0.00)

Prob1A: 46 iterations in 0.48 seconds (average 0.010522, setup 0.00)

yes = 104313, no = 2568, maybe = 106190

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=213071, nc=178562, nnz=475798, k=4] [5.8 MB]
Creating vector for yes... [1.6 MB]
Allocating iteration vectors... [2 x 1.6 MB]
TOTAL: [10.7 MB]

Starting iterations...

Iterative method: 93 iterations in 0.41 seconds (average 0.001978, setup 0.23)

Value in the initial state: 0.019136271523950036

Time for model checking: 0.99 seconds.

Result: 0.019136271523950036 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmax=? [ F "AllAreaSearched" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=2,Objy1=1,Objx2=5,Objy2=2

Prob0A: 52 iterations in 0.42 seconds (average 0.008077, setup 0.00)

Prob1E: 4 iterations in 0.04 seconds (average 0.011000, setup 0.00)

yes = 1238, no = 163590, maybe = 48243

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=213071, nc=77851, nnz=212749, k=2] [2.7 MB]
Creating vector for yes... [1.6 MB]
Allocating iteration vectors... [2 x 1.6 MB]
TOTAL: [7.6 MB]

Starting iterations...

Iterative method: 88 iterations in 0.34 seconds (average 0.001636, setup 0.20)

Value in the initial state: 0.23396515925139855

Time for model checking: 0.823 seconds.

Result: 0.23396515925139855 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmin=? [ F "AllAreaSearched" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=2,Objy1=1,Objx2=5,Objy2=2

Prob0E: 63 iterations in 0.48 seconds (average 0.007556, setup 0.00)

Prob1A: 3 iterations in 0.04 seconds (average 0.012000, setup 0.00)

yes = 1238, no = 170046, maybe = 41787

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=213071, nc=66647, nnz=180769, k=2] [2.3 MB]
Creating vector for yes... [1.6 MB]
Allocating iteration vectors... [2 x 1.6 MB]
TOTAL: [7.2 MB]

Starting iterations...

Iterative method: 91 iterations in 0.29 seconds (average 0.001582, setup 0.14)

Value in the initial state: 0.00176240279344598

Time for model checking: 0.801 seconds.

Result: 0.00176240279344598 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmax=? [ F "MissionTimeUp"|"AllAreaSearched"|"Crash" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=2,Objy1=1,Objx2=5,Objy2=2

Prob0A: 3 iterations in 0.07 seconds (average 0.024000, setup 0.00)

Prob1E: 192 iterations in 2.18 seconds (average 0.011375, setup 0.00)

yes = 183096, no = 938, maybe = 29037

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=213071, nc=50963, nnz=146467, k=2] [1.9 MB]
Creating vector for yes... [1.6 MB]
Allocating iteration vectors... [2 x 1.6 MB]
TOTAL: [6.8 MB]

Starting iterations...

Iterative method: 80 iterations in 0.30 seconds (average 0.001350, setup 0.19)

Value in the initial state: 0.2626040474923646

Time for model checking: 2.584 seconds.

Result: 0.2626040474923646 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmin=? [ F "MissionTimeUp"|"AllAreaSearched"|"Crash" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=2,Objy1=1,Objx2=5,Objy2=2

Prob0E: 3 iterations in 0.04 seconds (average 0.013333, setup 0.00)

Prob1A: 49 iterations in 0.43 seconds (average 0.008735, setup 0.00)

yes = 178394, no = 938, maybe = 33739

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=213071, nc=59382, nnz=170911, k=2] [2.2 MB]
Creating vector for yes... [1.6 MB]
Allocating iteration vectors... [2 x 1.6 MB]
TOTAL: [7.1 MB]

Starting iterations...

Iterative method: 91 iterations in 0.28 seconds (average 0.001451, setup 0.15)

Value in the initial state: 0.021165678122490653

Time for model checking: 0.773 seconds.

Result: 0.021165678122490653 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"drop"}max=? [ F "deadlock" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=2,Objy1=1,Objx2=5,Objy2=2

Prob0E: 4 iterations in 0.07 seconds (average 0.017000, setup 0.00)

Prob1A: 1 iterations in 0.00 seconds (average 0.004000, setup 0.00)

goal = 1, inf = 0, maybe = 213070

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=213071, nc=285442, nnz=582678, k=4] [8.6 MB]
Building sparse matrix (transition rewards)... [n=213071, nc=285442, nnz=0, k=4] [1.9 MB]
Creating vector for state rewards... [1.6 MB]
Creating vector for inf... [1.6 MB]
Allocating iteration vectors... [2 x 1.6 MB]
TOTAL: [17.0 MB]

Starting iterations...

Iterative method: 91 iterations in 0.69 seconds (average 0.003165, setup 0.40)

Value in the initial state: 0.2165865737393313

Time for model checking: 0.78 seconds.

Result: 0.2165865737393313 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"drop"}min=? [ F "deadlock" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=2,Objy1=1,Objx2=5,Objy2=2

Prob0A: 4 iterations in 0.03 seconds (average 0.008000, setup 0.00)

Prob1E: 5 iterations in 0.05 seconds (average 0.009600, setup 0.00)

Warning: PRISM hasn't checked for zero-reward loops.
Your minimum rewards may be too low...

goal = 1, inf = 0, maybe = 213070

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=213071, nc=285442, nnz=582678, k=4] [8.6 MB]
Building sparse matrix (transition rewards)... [n=213071, nc=285442, nnz=0, k=4] [1.9 MB]
Creating vector for state rewards... [1.6 MB]
Creating vector for inf... [1.6 MB]
Allocating iteration vectors... [2 x 1.6 MB]
TOTAL: [17.0 MB]

Starting iterations...

Iterative method: 81 iterations in 0.55 seconds (average 0.003111, setup 0.30)

Value in the initial state: 0.17796759187413572

Time for model checking: 0.652 seconds.

Result: 0.17796759187413572 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"charge"}max=? [ F "deadlock" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=2,Objy1=1,Objx2=5,Objy2=2

Prob0E: 4 iterations in 0.03 seconds (average 0.007000, setup 0.00)

Prob1A: 1 iterations in 0.00 seconds (average 0.004000, setup 0.00)

goal = 1, inf = 0, maybe = 213070

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=213071, nc=285442, nnz=582678, k=4] [8.6 MB]
Building sparse matrix (transition rewards)... [n=213071, nc=285442, nnz=6842, k=4] [2.0 MB]
Creating vector for state rewards... [1.6 MB]
Creating vector for inf... [1.6 MB]
Allocating iteration vectors... [2 x 1.6 MB]
TOTAL: [17.1 MB]

Starting iterations...

Iterative method: 86 iterations in 0.58 seconds (average 0.003163, setup 0.30)

Value in the initial state: 0.2939818655765795

Time for model checking: 0.623 seconds.

Result: 0.2939818655765795 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"charge"}min=? [ F "deadlock" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=2,Objy1=1,Objx2=5,Objy2=2

Prob0A: 4 iterations in 0.02 seconds (average 0.006000, setup 0.00)

Prob1E: 5 iterations in 0.05 seconds (average 0.009600, setup 0.00)

Warning: PRISM hasn't checked for zero-reward loops.
Your minimum rewards may be too low...

goal = 1, inf = 0, maybe = 213070

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=213071, nc=285442, nnz=582678, k=4] [8.6 MB]
Building sparse matrix (transition rewards)... [n=213071, nc=285442, nnz=6842, k=4] [2.0 MB]
Creating vector for state rewards... [1.6 MB]
Creating vector for inf... [1.6 MB]
Allocating iteration vectors... [2 x 1.6 MB]
TOTAL: [17.1 MB]

Starting iterations...

Iterative method: 94 iterations in 0.61 seconds (average 0.003191, setup 0.31)

Value in the initial state: 0.06709291623893625

Time for model checking: 0.691 seconds.

Result: 0.06709291623893625 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"time"}max=? [ F "deadlock" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=2,Objy1=1,Objx2=5,Objy2=2

Prob0E: 4 iterations in 0.02 seconds (average 0.006000, setup 0.00)

Prob1A: 1 iterations in 0.00 seconds (average 0.004000, setup 0.00)

goal = 1, inf = 0, maybe = 213070

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=213071, nc=285442, nnz=582678, k=4] [8.6 MB]
Building sparse matrix (transition rewards)... [n=213071, nc=285442, nnz=106879, k=4] [3.1 MB]
Creating vector for state rewards... [1.6 MB]
Creating vector for inf... [1.6 MB]
Allocating iteration vectors... [2 x 1.6 MB]
TOTAL: [18.2 MB]

Starting iterations...

Iterative method: 89 iterations in 0.62 seconds (average 0.003236, setup 0.34)

Value in the initial state: 176.60378830864101

Time for model checking: 0.671 seconds.

Result: 176.60378830864101 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"time"}min=? [ F "deadlock" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=2,Objy1=1,Objx2=5,Objy2=2

Prob0A: 4 iterations in 0.02 seconds (average 0.005000, setup 0.00)

Prob1E: 5 iterations in 0.04 seconds (average 0.008800, setup 0.00)

Warning: PRISM hasn't checked for zero-reward loops.
Your minimum rewards may be too low...

goal = 1, inf = 0, maybe = 213070

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=213071, nc=285442, nnz=582678, k=4] [8.6 MB]
Building sparse matrix (transition rewards)... [n=213071, nc=285442, nnz=106879, k=4] [3.1 MB]
Creating vector for state rewards... [1.6 MB]
Creating vector for inf... [1.6 MB]
Allocating iteration vectors... [2 x 1.6 MB]
TOTAL: [18.2 MB]

Starting iterations...

Iterative method: 99 iterations in 0.66 seconds (average 0.003192, setup 0.34)

Value in the initial state: 105.98043543539579

Time for model checking: 0.739 seconds.

Result: 105.98043543539579 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"success"}max=? [ F "deadlock" ]/Pmin=? [ F "MissionSuccessful" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=2,Objy1=1,Objx2=5,Objy2=2

Prob0E: 4 iterations in 0.02 seconds (average 0.006000, setup 0.00)

Prob1A: 1 iterations in 0.00 seconds (average 0.004000, setup 0.00)

goal = 1, inf = 0, maybe = 213070

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=213071, nc=285442, nnz=582678, k=4] [8.6 MB]
Building sparse matrix (transition rewards)... [n=213071, nc=285442, nnz=1846, k=4] [1.9 MB]
Creating vector for state rewards... [1.6 MB]
Creating vector for inf... [1.6 MB]
Allocating iteration vectors... [2 x 1.6 MB]
TOTAL: [17.0 MB]

Starting iterations...

Iterative method: 93 iterations in 0.60 seconds (average 0.003097, setup 0.32)

Prob0E: 49 iterations in 0.42 seconds (average 0.008653, setup 0.00)

Prob1A: 3 iterations in 0.04 seconds (average 0.012000, setup 0.00)

yes = 937, no = 183097, maybe = 29037

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=213071, nc=50963, nnz=146467, k=2] [1.9 MB]
Creating vector for yes... [1.6 MB]
Allocating iteration vectors... [2 x 1.6 MB]
TOTAL: [6.8 MB]

Starting iterations...

Iterative method: 87 iterations in 0.27 seconds (average 0.001379, setup 0.15)

Value in the initial state: 234.42905687404243

Time for model checking: 1.395 seconds.

Result: 234.42905687404243 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"success"}min=? [ F "deadlock" ]/Pmax=? [ F "MissionSuccessful" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=2,Objy1=1,Objx2=5,Objy2=2

Prob0A: 4 iterations in 0.06 seconds (average 0.016000, setup 0.00)

Prob1E: 5 iterations in 0.04 seconds (average 0.008800, setup 0.00)

Warning: PRISM hasn't checked for zero-reward loops.
Your minimum rewards may be too low...

goal = 1, inf = 0, maybe = 213070

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=213071, nc=285442, nnz=582678, k=4] [8.6 MB]
Building sparse matrix (transition rewards)... [n=213071, nc=285442, nnz=1846, k=4] [1.9 MB]
Creating vector for state rewards... [1.6 MB]
Creating vector for inf... [1.6 MB]
Allocating iteration vectors... [2 x 1.6 MB]
TOTAL: [17.0 MB]

Starting iterations...

Iterative method: 92 iterations in 0.64 seconds (average 0.003130, setup 0.36)

Prob0A: 49 iterations in 0.24 seconds (average 0.004898, setup 0.00)

Prob1E: 4 iterations in 0.03 seconds (average 0.008000, setup 0.00)

yes = 937, no = 178395, maybe = 33739

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=213071, nc=59382, nnz=170911, k=2] [2.2 MB]
Creating vector for yes... [1.6 MB]
Allocating iteration vectors... [2 x 1.6 MB]
TOTAL: [7.1 MB]

Starting iterations...

Iterative method: 94 iterations in 0.29 seconds (average 0.001404, setup 0.16)

Value in the initial state: 78.65335826279824

Time for model checking: 1.349 seconds.

Result: 78.65335826279824 (value in the initial state)

---------------------------------------------------------------------

Note: There were 16 warnings during computation.

