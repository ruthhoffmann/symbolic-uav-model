PRISM
=====

Version: 4.3
Date: Tue Aug 02 12:23:25 BST 2016
Hostname: eglinton
Memory limits: cudd=1g, java(heap)=981.5m
Command line: prism uav_sim_ft_mdp.prism -s uav_mdp.props -const 'Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=0,Objy1=1,Objx2=1,Objy2=3,'

Parsing model file "uav_sim_ft_mdp.prism"...

Parsing properties file "uav_mdp.props"...

18 properties:
(1) Pmax=? [ F "MissionSuccessful" ]
(2) Pmin=? [ F "MissionSuccessful" ]
(3) Pmax=? [ F "MissionTimeUp" ]
(4) Pmin=? [ F "MissionTimeUp" ]
(5) Pmax=? [ F "Crash" ]
(6) Pmin=? [ F "Crash" ]
(7) Pmax=? [ F "AllAreaSearched" ]
(8) Pmin=? [ F "AllAreaSearched" ]
(9) Pmax=? [ F "MissionTimeUp"|"AllAreaSearched"|"Crash" ]
(10) Pmin=? [ F "MissionTimeUp"|"AllAreaSearched"|"Crash" ]
(11) R{"drop"}max=? [ F "deadlock" ]
(12) R{"drop"}min=? [ F "deadlock" ]
(13) R{"charge"}max=? [ F "deadlock" ]
(14) R{"charge"}min=? [ F "deadlock" ]
(15) R{"time"}max=? [ F "deadlock" ]
(16) R{"time"}min=? [ F "deadlock" ]
(17) R{"success"}max=? [ F "deadlock" ]/Pmin=? [ F "MissionSuccessful" ]
(18) R{"success"}min=? [ F "deadlock" ]/Pmax=? [ F "MissionSuccessful" ]

Type:        MDP
Modules:     Obj Move Robot T 
Variables:   NoOfObjs posx posy s1 retx rety b t c 

---------------------------------------------------------------------

Model checking: Pmax=? [ F "MissionSuccessful" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=0,Objy1=1,Objx2=1,Objy2=3

Building model...
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=0,Objy1=1,Objx2=1,Objy2=3

Warning: Guard for command 4 of module "Move" is never satisfied.

Warning: Guard for command 7 of module "Move" is never satisfied.

Warning: Guard for command 8 of module "Move" is never satisfied.

Warning: Guard for command 9 of module "Move" is never satisfied.

Warning: Guard for command 7 of module "Robot" is never satisfied.

Warning: Update 2 of command 3 of module "Robot" doesn't do anything ("(s1'=13)", line 249, column 44)

Warning: Update 2 of command 27 of module "T" doesn't do anything ("(c'=1)", line 372, column 108)

Warning: Guard for command 28 of module "T" is never satisfied.

Warning: Update 2 of command 9 of module "T" doesn't do anything ("(c'=1)", line 349, column 112)

Warning: Guard for command 10 of module "T" is never satisfied.

Warning: Update 2 of command 51 of module "T" doesn't do anything ("(c'=1)", line 403, column 113)

Warning: Guard for command 52 of module "T" is never satisfied.

Computing reachable states...

Reachability (BFS): 81 iterations in 0.81 seconds (average 0.009975, setup 0.00)

Time for model construction: 2.275 seconds.

Warning: Deadlocks detected and fixed in 1 states

Type:        MDP
States:      164040 (1 initial)
Transitions: 432701
Choices:     217821

Transition matrix: 163309 nodes (76 terminal), 432701 minterms, vars: 34r/34c/18nd

Prob0A: 54 iterations in 0.40 seconds (average 0.007481, setup 0.00)

Prob1E: 4 iterations in 0.05 seconds (average 0.013000, setup 0.00)

yes = 952, no = 136115, maybe = 26973

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=164040, nc=45792, nnz=125859, k=2] [1.6 MB]
Creating vector for yes... [1.3 MB]
Allocating iteration vectors... [2 x 1.3 MB]
TOTAL: [5.4 MB]

Starting iterations...

Iterative method: 94 iterations in 0.30 seconds (average 0.001106, setup 0.19)

Value in the initial state: 0.9755108953315695

Time for model checking: 0.975 seconds.

Result: 0.9755108953315695 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmin=? [ F "MissionSuccessful" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=0,Objy1=1,Objx2=1,Objy2=3

Prob0E: 67 iterations in 0.53 seconds (average 0.007881, setup 0.00)

Prob1A: 3 iterations in 0.05 seconds (average 0.016000, setup 0.00)

yes = 952, no = 140232, maybe = 22856

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=164040, nc=38735, nnz=105467, k=2] [1.4 MB]
Creating vector for yes... [1.3 MB]
Allocating iteration vectors... [2 x 1.3 MB]
TOTAL: [5.2 MB]

Starting iterations...

Iterative method: 86 iterations in 0.26 seconds (average 0.001070, setup 0.17)

Value in the initial state: 0.932991201624076

Time for model checking: 1.216 seconds.

Result: 0.932991201624076 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmax=? [ F "MissionTimeUp" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=0,Objy1=1,Objx2=1,Objy2=3

Prob0A: 64 iterations in 0.53 seconds (average 0.008313, setup 0.00)

Prob1E: 4 iterations in 0.04 seconds (average 0.009000, setup 0.00)

yes = 296, no = 96021, maybe = 67723

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=164040, nc=110837, nnz=298351, k=4] [3.7 MB]
Creating vector for yes... [1.3 MB]
Allocating iteration vectors... [2 x 1.3 MB]
TOTAL: [7.4 MB]

Starting iterations...

Iterative method: 91 iterations in 0.36 seconds (average 0.001407, setup 0.24)

Value in the initial state: 0.0010491803361817346

Time for model checking: 1.47 seconds.

Result: 0.0010491803361817346 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmin=? [ F "MissionTimeUp" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=0,Objy1=1,Objx2=1,Objy2=3

Prob0E: 71 iterations in 0.60 seconds (average 0.008451, setup 0.00)

Prob1A: 3 iterations in 0.04 seconds (average 0.013333, setup 0.00)

yes = 296, no = 96098, maybe = 67646

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=164040, nc=110716, nnz=297988, k=4] [3.7 MB]
Creating vector for yes... [1.3 MB]
Allocating iteration vectors... [2 x 1.3 MB]
TOTAL: [7.4 MB]

Starting iterations...

Iterative method: 106 iterations in 0.38 seconds (average 0.001509, setup 0.22)

Value in the initial state: 4.122011190381232E-6

Time for model checking: 1.044 seconds.

Result: 4.122011190381232E-6 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmax=? [ F "Crash" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=0,Objy1=1,Objx2=1,Objy2=3

Prob0A: 3 iterations in 0.07 seconds (average 0.022667, setup 0.00)

Prob1E: 256 iterations in 2.82 seconds (average 0.011031, setup 0.00)

yes = 80424, no = 2551, maybe = 81065

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=164040, nc=134846, nnz=349726, k=4] [4.3 MB]
Creating vector for yes... [1.3 MB]
Allocating iteration vectors... [2 x 1.3 MB]
TOTAL: [8.0 MB]

Starting iterations...

Iterative method: 90 iterations in 0.40 seconds (average 0.001511, setup 0.26)

Value in the initial state: 0.06288489149317712

Time for model checking: 4.848 seconds.

Result: 0.06288489149317712 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmin=? [ F "Crash" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=0,Objy1=1,Objx2=1,Objy2=3

Prob0E: 3 iterations in 0.05 seconds (average 0.016000, setup 0.00)

Prob1A: 53 iterations in 0.39 seconds (average 0.007396, setup 0.00)

yes = 80424, no = 2551, maybe = 81065

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=164040, nc=134846, nnz=349726, k=4] [4.3 MB]
Creating vector for yes... [1.3 MB]
Allocating iteration vectors... [2 x 1.3 MB]
TOTAL: [8.0 MB]

Starting iterations...

Iterative method: 94 iterations in 0.32 seconds (average 0.001574, setup 0.17)

Value in the initial state: 0.02253163905682515

Time for model checking: 1.112 seconds.

Result: 0.02253163905682515 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmax=? [ F "AllAreaSearched" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=0,Objy1=1,Objx2=1,Objy2=3

Prob0A: 53 iterations in 0.32 seconds (average 0.006038, setup 0.00)

Prob1E: 4 iterations in 0.04 seconds (average 0.009000, setup 0.00)

yes = 1302, no = 119863, maybe = 42875

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=164040, nc=70484, nnz=188603, k=2] [2.4 MB]
Creating vector for yes... [1.3 MB]
Allocating iteration vectors... [2 x 1.3 MB]
TOTAL: [6.1 MB]

Starting iterations...

Iterative method: 91 iterations in 0.28 seconds (average 0.001275, setup 0.17)

Value in the initial state: 0.003675722581325617

Time for model checking: 0.666 seconds.

Result: 0.003675722581325617 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmin=? [ F "AllAreaSearched" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=0,Objy1=1,Objx2=1,Objy2=3

Prob0E: 67 iterations in 0.46 seconds (average 0.006925, setup 0.00)

Prob1A: 3 iterations in 0.03 seconds (average 0.010667, setup 0.00)

yes = 1302, no = 125356, maybe = 37382

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=164040, nc=61306, nnz=162455, k=2] [2.1 MB]
Creating vector for yes... [1.3 MB]
Allocating iteration vectors... [2 x 1.3 MB]
TOTAL: [5.8 MB]

Starting iterations...

Iterative method: 89 iterations in 0.23 seconds (average 0.001169, setup 0.12)

Value in the initial state: 0.0012338861320745922

Time for model checking: 0.91 seconds.

Result: 0.0012338861320745922 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmax=? [ F "MissionTimeUp"|"AllAreaSearched"|"Crash" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=0,Objy1=1,Objx2=1,Objy2=3

Prob0A: 3 iterations in 0.06 seconds (average 0.020000, setup 0.00)

Prob1E: 260 iterations in 3.00 seconds (average 0.011554, setup 0.00)

yes = 140231, no = 953, maybe = 22856

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=164040, nc=38735, nnz=105467, k=2] [1.4 MB]
Creating vector for yes... [1.3 MB]
Allocating iteration vectors... [2 x 1.3 MB]
TOTAL: [5.2 MB]

Starting iterations...

Iterative method: 84 iterations in 0.25 seconds (average 0.001000, setup 0.16)

Value in the initial state: 0.06700874362625991

Time for model checking: 4.304 seconds.

Result: 0.06700874362625991 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmin=? [ F "MissionTimeUp"|"AllAreaSearched"|"Crash" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=0,Objy1=1,Objx2=1,Objy2=3

Prob0E: 3 iterations in 0.04 seconds (average 0.013333, setup 0.00)

Prob1A: 54 iterations in 0.48 seconds (average 0.008815, setup 0.00)

yes = 136114, no = 953, maybe = 26973

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=164040, nc=45792, nnz=125859, k=2] [1.6 MB]
Creating vector for yes... [1.3 MB]
Allocating iteration vectors... [2 x 1.3 MB]
TOTAL: [5.4 MB]

Starting iterations...

Iterative method: 92 iterations in 0.24 seconds (average 0.001130, setup 0.14)

Value in the initial state: 0.02448836322008283

Time for model checking: 1.446 seconds.

Result: 0.02448836322008283 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"drop"}max=? [ F "deadlock" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=0,Objy1=1,Objx2=1,Objy2=3

Prob0E: 4 iterations in 0.06 seconds (average 0.014000, setup 0.00)

Prob1A: 1 iterations in 0.00 seconds (average 0.004000, setup 0.00)

goal = 1, inf = 0, maybe = 164039

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=164040, nc=217820, nnz=432700, k=4] [6.4 MB]
Building sparse matrix (transition rewards)... [n=164040, nc=217820, nnz=0, k=4] [1.5 MB]
Creating vector for state rewards... [1.3 MB]
Creating vector for inf... [1.3 MB]
Allocating iteration vectors... [2 x 1.3 MB]
TOTAL: [12.9 MB]

Starting iterations...

Iterative method: 90 iterations in 0.55 seconds (average 0.002311, setup 0.34)

Value in the initial state: 0.18018031374004223

Time for model checking: 0.756 seconds.

Result: 0.18018031374004223 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"drop"}min=? [ F "deadlock" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=0,Objy1=1,Objx2=1,Objy2=3

Prob0A: 4 iterations in 0.03 seconds (average 0.008000, setup 0.00)

Prob1E: 5 iterations in 0.04 seconds (average 0.008000, setup 0.00)

Warning: PRISM hasn't checked for zero-reward loops.
Your minimum rewards may be too low...

goal = 1, inf = 0, maybe = 164039

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=164040, nc=217820, nnz=432700, k=4] [6.4 MB]
Building sparse matrix (transition rewards)... [n=164040, nc=217820, nnz=0, k=4] [1.5 MB]
Creating vector for state rewards... [1.3 MB]
Creating vector for inf... [1.3 MB]
Allocating iteration vectors... [2 x 1.3 MB]
TOTAL: [12.9 MB]

Starting iterations...

Iterative method: 82 iterations in 0.44 seconds (average 0.002390, setup 0.24)

Value in the initial state: 0.13577912838010303

Time for model checking: 0.732 seconds.

Result: 0.13577912838010303 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"charge"}max=? [ F "deadlock" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=0,Objy1=1,Objx2=1,Objy2=3

Prob0E: 4 iterations in 0.02 seconds (average 0.006000, setup 0.00)

Prob1A: 1 iterations in 0.00 seconds (average 0.004000, setup 0.00)

goal = 1, inf = 0, maybe = 164039

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=164040, nc=217820, nnz=432700, k=4] [6.4 MB]
Building sparse matrix (transition rewards)... [n=164040, nc=217820, nnz=4730, k=4] [1.5 MB]
Creating vector for state rewards... [1.3 MB]
Creating vector for inf... [1.3 MB]
Allocating iteration vectors... [2 x 1.3 MB]
TOTAL: [12.9 MB]

Starting iterations...

Iterative method: 86 iterations in 0.44 seconds (average 0.002419, setup 0.24)

Value in the initial state: 0.9806624095538479

Time for model checking: 0.569 seconds.

Result: 0.9806624095538479 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"charge"}min=? [ F "deadlock" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=0,Objy1=1,Objx2=1,Objy2=3

Prob0A: 4 iterations in 0.02 seconds (average 0.005000, setup 0.00)

Prob1E: 5 iterations in 0.04 seconds (average 0.008000, setup 0.00)

Warning: PRISM hasn't checked for zero-reward loops.
Your minimum rewards may be too low...

goal = 1, inf = 0, maybe = 164039

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=164040, nc=217820, nnz=432700, k=4] [6.4 MB]
Building sparse matrix (transition rewards)... [n=164040, nc=217820, nnz=4730, k=4] [1.5 MB]
Creating vector for state rewards... [1.3 MB]
Creating vector for inf... [1.3 MB]
Allocating iteration vectors... [2 x 1.3 MB]
TOTAL: [12.9 MB]

Starting iterations...

Iterative method: 94 iterations in 0.47 seconds (average 0.002426, setup 0.24)

Value in the initial state: 0.12215400692077677

Time for model checking: 0.612 seconds.

Result: 0.12215400692077677 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"time"}max=? [ F "deadlock" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=0,Objy1=1,Objx2=1,Objy2=3

Prob0E: 4 iterations in 0.02 seconds (average 0.006000, setup 0.00)

Prob1A: 1 iterations in 0.00 seconds (average 0.004000, setup 0.00)

goal = 1, inf = 0, maybe = 164039

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=164040, nc=217820, nnz=432700, k=4] [6.4 MB]
Building sparse matrix (transition rewards)... [n=164040, nc=217820, nnz=82973, k=4] [2.4 MB]
Creating vector for state rewards... [1.3 MB]
Creating vector for inf... [1.3 MB]
Allocating iteration vectors... [2 x 1.3 MB]
TOTAL: [13.8 MB]

Starting iterations...

Iterative method: 92 iterations in 0.50 seconds (average 0.002435, setup 0.27)

Value in the initial state: 345.48585148063626

Time for model checking: 0.541 seconds.

Result: 345.48585148063626 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"time"}min=? [ F "deadlock" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=0,Objy1=1,Objx2=1,Objy2=3

Prob0A: 4 iterations in 0.02 seconds (average 0.006000, setup 0.00)

Prob1E: 5 iterations in 0.04 seconds (average 0.008800, setup 0.00)

Warning: PRISM hasn't checked for zero-reward loops.
Your minimum rewards may be too low...

goal = 1, inf = 0, maybe = 164039

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=164040, nc=217820, nnz=432700, k=4] [6.4 MB]
Building sparse matrix (transition rewards)... [n=164040, nc=217820, nnz=82973, k=4] [2.4 MB]
Creating vector for state rewards... [1.3 MB]
Creating vector for inf... [1.3 MB]
Allocating iteration vectors... [2 x 1.3 MB]
TOTAL: [13.8 MB]

Starting iterations...

Iterative method: 100 iterations in 0.51 seconds (average 0.002520, setup 0.26)

Value in the initial state: 124.62522265926742

Time for model checking: 0.619 seconds.

Result: 124.62522265926742 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"success"}max=? [ F "deadlock" ]/Pmin=? [ F "MissionSuccessful" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=0,Objy1=1,Objx2=1,Objy2=3

Prob0E: 4 iterations in 0.03 seconds (average 0.007000, setup 0.00)

Prob1A: 1 iterations in 0.00 seconds (average 0.004000, setup 0.00)

goal = 1, inf = 0, maybe = 164039

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=164040, nc=217820, nnz=432700, k=4] [6.4 MB]
Building sparse matrix (transition rewards)... [n=164040, nc=217820, nnz=1876, k=4] [1.5 MB]
Creating vector for state rewards... [1.3 MB]
Creating vector for inf... [1.3 MB]
Allocating iteration vectors... [2 x 1.3 MB]
TOTAL: [12.9 MB]

Starting iterations...

Iterative method: 95 iterations in 0.47 seconds (average 0.002400, setup 0.24)

Prob0E: 67 iterations in 0.47 seconds (average 0.006985, setup 0.00)

Prob1A: 3 iterations in 0.04 seconds (average 0.012000, setup 0.00)

yes = 952, no = 140232, maybe = 22856

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=164040, nc=38735, nnz=105467, k=2] [1.4 MB]
Creating vector for yes... [1.3 MB]
Allocating iteration vectors... [2 x 1.3 MB]
TOTAL: [5.2 MB]

Starting iterations...

Iterative method: 86 iterations in 0.23 seconds (average 0.001070, setup 0.14)

Value in the initial state: 361.8724839473747

Time for model checking: 1.279 seconds.

Result: 361.8724839473747 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"success"}min=? [ F "deadlock" ]/Pmax=? [ F "MissionSuccessful" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=0,Objy1=1,Objx2=1,Objy2=3

Prob0A: 4 iterations in 0.08 seconds (average 0.020000, setup 0.00)

Prob1E: 5 iterations in 0.04 seconds (average 0.008800, setup 0.00)

Warning: PRISM hasn't checked for zero-reward loops.
Your minimum rewards may be too low...

goal = 1, inf = 0, maybe = 164039

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=164040, nc=217820, nnz=432700, k=4] [6.4 MB]
Building sparse matrix (transition rewards)... [n=164040, nc=217820, nnz=1876, k=4] [1.5 MB]
Creating vector for state rewards... [1.3 MB]
Creating vector for inf... [1.3 MB]
Allocating iteration vectors... [2 x 1.3 MB]
TOTAL: [12.9 MB]

Starting iterations...

Iterative method: 93 iterations in 0.49 seconds (average 0.002409, setup 0.26)

Prob0A: 54 iterations in 0.30 seconds (average 0.005481, setup 0.00)

Prob1E: 4 iterations in 0.03 seconds (average 0.007000, setup 0.00)

yes = 952, no = 136115, maybe = 26973

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=164040, nc=45792, nnz=125859, k=2] [1.6 MB]
Creating vector for yes... [1.3 MB]
Allocating iteration vectors... [2 x 1.3 MB]
TOTAL: [5.4 MB]

Starting iterations...

Iterative method: 94 iterations in 0.24 seconds (average 0.001106, setup 0.13)

Value in the initial state: 125.70788594694127

Time for model checking: 1.191 seconds.

Result: 125.70788594694127 (value in the initial state)

---------------------------------------------------------------------

Note: There were 17 warnings during computation.

