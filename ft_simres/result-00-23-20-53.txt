PRISM
=====

Version: 4.3
Date: Tue Aug 02 12:26:57 BST 2016
Hostname: eglinton
Memory limits: cudd=1g, java(heap)=981.5m
Command line: prism uav_sim_ft_mdp.prism -s uav_mdp.props -const 'Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=2,Objy1=0,Objx2=5,Objy2=3,'

Parsing model file "uav_sim_ft_mdp.prism"...

Parsing properties file "uav_mdp.props"...

18 properties:
(1) Pmax=? [ F "MissionSuccessful" ]
(2) Pmin=? [ F "MissionSuccessful" ]
(3) Pmax=? [ F "MissionTimeUp" ]
(4) Pmin=? [ F "MissionTimeUp" ]
(5) Pmax=? [ F "Crash" ]
(6) Pmin=? [ F "Crash" ]
(7) Pmax=? [ F "AllAreaSearched" ]
(8) Pmin=? [ F "AllAreaSearched" ]
(9) Pmax=? [ F "MissionTimeUp"|"AllAreaSearched"|"Crash" ]
(10) Pmin=? [ F "MissionTimeUp"|"AllAreaSearched"|"Crash" ]
(11) R{"drop"}max=? [ F "deadlock" ]
(12) R{"drop"}min=? [ F "deadlock" ]
(13) R{"charge"}max=? [ F "deadlock" ]
(14) R{"charge"}min=? [ F "deadlock" ]
(15) R{"time"}max=? [ F "deadlock" ]
(16) R{"time"}min=? [ F "deadlock" ]
(17) R{"success"}max=? [ F "deadlock" ]/Pmin=? [ F "MissionSuccessful" ]
(18) R{"success"}min=? [ F "deadlock" ]/Pmax=? [ F "MissionSuccessful" ]

Type:        MDP
Modules:     Obj Move Robot T 
Variables:   NoOfObjs posx posy s1 retx rety b t c 

---------------------------------------------------------------------

Model checking: Pmax=? [ F "MissionSuccessful" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=2,Objy1=0,Objx2=5,Objy2=3

Building model...
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=2,Objy1=0,Objx2=5,Objy2=3

Warning: Guard for command 7 of module "Move" is never satisfied.

Warning: Guard for command 8 of module "Move" is never satisfied.

Warning: Guard for command 9 of module "Move" is never satisfied.

Warning: Guard for command 7 of module "Robot" is never satisfied.

Warning: Update 2 of command 3 of module "Robot" doesn't do anything ("(s1'=13)", line 249, column 44)

Warning: Update 2 of command 27 of module "T" doesn't do anything ("(c'=1)", line 372, column 108)

Warning: Guard for command 28 of module "T" is never satisfied.

Warning: Update 2 of command 9 of module "T" doesn't do anything ("(c'=1)", line 349, column 112)

Warning: Guard for command 10 of module "T" is never satisfied.

Warning: Update 2 of command 51 of module "T" doesn't do anything ("(c'=1)", line 403, column 113)

Warning: Guard for command 52 of module "T" is never satisfied.

Computing reachable states...

Reachability (BFS): 81 iterations in 1.03 seconds (average 0.012741, setup 0.00)

Time for model construction: 2.385 seconds.

Warning: Deadlocks detected and fixed in 1 states

Type:        MDP
States:      248403 (1 initial)
Transitions: 660020
Choices:     327211

Transition matrix: 178147 nodes (76 terminal), 660020 minterms, vars: 34r/34c/18nd

Prob0A: 60 iterations in 0.56 seconds (average 0.009333, setup 0.00)

Prob1E: 4 iterations in 0.06 seconds (average 0.015000, setup 0.00)

yes = 930, no = 212784, maybe = 34689

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=248403, nc=59107, nnz=167784, k=2] [2.2 MB]
Creating vector for yes... [1.9 MB]
Allocating iteration vectors... [2 x 1.9 MB]
TOTAL: [7.9 MB]

Starting iterations...

Iterative method: 98 iterations in 0.42 seconds (average 0.001755, setup 0.25)

Value in the initial state: 0.9773462156643811

Time for model checking: 1.082 seconds.

Result: 0.9773462156643811 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmin=? [ F "MissionSuccessful" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=2,Objy1=0,Objx2=5,Objy2=3

Prob0E: 67 iterations in 0.58 seconds (average 0.008657, setup 0.00)

Prob1A: 3 iterations in 0.06 seconds (average 0.020000, setup 0.00)

yes = 930, no = 217390, maybe = 30083

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=248403, nc=51487, nnz=146629, k=2] [2.0 MB]
Creating vector for yes... [1.9 MB]
Allocating iteration vectors... [2 x 1.9 MB]
TOTAL: [7.6 MB]

Starting iterations...

Iterative method: 91 iterations in 0.37 seconds (average 0.001714, setup 0.21)

Value in the initial state: 0.7322953382970124

Time for model checking: 1.04 seconds.

Result: 0.7322953382970124 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmax=? [ F "MissionTimeUp" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=2,Objy1=0,Objx2=5,Objy2=3

Prob0A: 62 iterations in 0.68 seconds (average 0.010968, setup 0.00)

Prob1E: 4 iterations in 0.04 seconds (average 0.011000, setup 0.00)

yes = 512, no = 140744, maybe = 107147

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=248403, nc=176043, nnz=479616, k=4] [5.9 MB]
Creating vector for yes... [1.9 MB]
Allocating iteration vectors... [2 x 1.9 MB]
TOTAL: [11.6 MB]

Starting iterations...

Iterative method: 93 iterations in 0.60 seconds (average 0.002237, setup 0.40)

Value in the initial state: 0.0030243149373404515

Time for model checking: 1.36 seconds.

Result: 0.0030243149373404515 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmin=? [ F "MissionTimeUp" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=2,Objy1=0,Objx2=5,Objy2=3

Prob0E: 68 iterations in 0.78 seconds (average 0.011529, setup 0.00)

Prob1A: 3 iterations in 0.05 seconds (average 0.017333, setup 0.00)

yes = 512, no = 141120, maybe = 106771

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=248403, nc=175367, nnz=478268, k=4] [5.9 MB]
Creating vector for yes... [1.9 MB]
Allocating iteration vectors... [2 x 1.9 MB]
TOTAL: [11.6 MB]

Starting iterations...

Iterative method: 108 iterations in 0.55 seconds (average 0.002296, setup 0.30)

Value in the initial state: 1.1153398672874962E-5

Time for model checking: 1.405 seconds.

Result: 1.1153398672874962E-5 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmax=? [ F "Crash" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=2,Objy1=0,Objx2=5,Objy2=3

Prob0A: 3 iterations in 0.08 seconds (average 0.026667, setup 0.00)

Prob1E: 232 iterations in 3.30 seconds (average 0.014241, setup 0.00)

yes = 121660, no = 2697, maybe = 124046

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=248403, nc=202854, nnz=535663, k=4] [6.6 MB]
Creating vector for yes... [1.9 MB]
Allocating iteration vectors... [2 x 1.9 MB]
TOTAL: [12.2 MB]

Starting iterations...

Iterative method: 92 iterations in 0.58 seconds (average 0.002391, setup 0.36)

Value in the initial state: 0.0630724045029183

Time for model checking: 4.018 seconds.

Result: 0.0630724045029183 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmin=? [ F "Crash" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=2,Objy1=0,Objx2=5,Objy2=3

Prob0E: 3 iterations in 0.07 seconds (average 0.022667, setup 0.00)

Prob1A: 51 iterations in 0.70 seconds (average 0.013804, setup 0.00)

yes = 121660, no = 2697, maybe = 124046

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=248403, nc=202854, nnz=535663, k=4] [6.6 MB]
Creating vector for yes... [1.9 MB]
Allocating iteration vectors... [2 x 1.9 MB]
TOTAL: [12.2 MB]

Starting iterations...

Iterative method: 97 iterations in 0.56 seconds (average 0.002474, setup 0.32)

Value in the initial state: 0.020625925121482272

Time for model checking: 1.372 seconds.

Result: 0.020625925121482272 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmax=? [ F "AllAreaSearched" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=2,Objy1=0,Objx2=5,Objy2=3

Prob0A: 62 iterations in 0.55 seconds (average 0.008839, setup 0.00)

Prob1E: 4 iterations in 0.05 seconds (average 0.013000, setup 0.00)

yes = 1254, no = 197753, maybe = 49396

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=248403, nc=80254, nnz=219604, k=2] [2.8 MB]
Creating vector for yes... [1.9 MB]
Allocating iteration vectors... [2 x 1.9 MB]
TOTAL: [8.5 MB]

Starting iterations...

Iterative method: 89 iterations in 0.46 seconds (average 0.002697, setup 0.22)

Value in the initial state: 0.23802415588301037

Time for model checking: 1.092 seconds.

Result: 0.23802415588301037 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmin=? [ F "AllAreaSearched" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=2,Objy1=0,Objx2=5,Objy2=3

Prob0E: 71 iterations in 0.51 seconds (average 0.007155, setup 0.00)

Prob1A: 3 iterations in 0.04 seconds (average 0.014667, setup 0.00)

yes = 1254, no = 204890, maybe = 42259

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=248403, nc=68699, nnz=187576, k=2] [2.4 MB]
Creating vector for yes... [1.9 MB]
Allocating iteration vectors... [2 x 1.9 MB]
TOTAL: [8.1 MB]

Starting iterations...

Iterative method: 90 iterations in 0.30 seconds (average 0.001778, setup 0.14)

Value in the initial state: 0.0012804947295308082

Time for model checking: 0.87 seconds.

Result: 0.0012804947295308082 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmax=? [ F "MissionTimeUp"|"AllAreaSearched"|"Crash" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=2,Objy1=0,Objx2=5,Objy2=3

Prob0A: 3 iterations in 0.09 seconds (average 0.030667, setup 0.00)

Prob1E: 260 iterations in 3.40 seconds (average 0.013092, setup 0.00)

yes = 217389, no = 931, maybe = 30083

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=248403, nc=51487, nnz=146629, k=2] [2.0 MB]
Creating vector for yes... [1.9 MB]
Allocating iteration vectors... [2 x 1.9 MB]
TOTAL: [7.6 MB]

Starting iterations...

Iterative method: 84 iterations in 0.32 seconds (average 0.001524, setup 0.20)

Value in the initial state: 0.26770399088761077

Time for model checking: 3.867 seconds.

Result: 0.26770399088761077 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmin=? [ F "MissionTimeUp"|"AllAreaSearched"|"Crash" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=2,Objy1=0,Objx2=5,Objy2=3

Prob0E: 3 iterations in 0.08 seconds (average 0.025333, setup 0.00)

Prob1A: 60 iterations in 0.65 seconds (average 0.010800, setup 0.00)

yes = 212783, no = 931, maybe = 34689

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=248403, nc=59107, nnz=167784, k=2] [2.2 MB]
Creating vector for yes... [1.9 MB]
Allocating iteration vectors... [2 x 1.9 MB]
TOTAL: [7.9 MB]

Starting iterations...

Iterative method: 95 iterations in 0.36 seconds (average 0.001642, setup 0.20)

Value in the initial state: 0.02265352227917268

Time for model checking: 1.114 seconds.

Result: 0.02265352227917268 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"drop"}max=? [ F "deadlock" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=2,Objy1=0,Objx2=5,Objy2=3

Prob0E: 4 iterations in 0.08 seconds (average 0.020000, setup 0.00)

Prob1A: 1 iterations in 0.01 seconds (average 0.008000, setup 0.00)

goal = 1, inf = 0, maybe = 248402

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=248403, nc=327210, nnz=660019, k=4] [9.7 MB]
Building sparse matrix (transition rewards)... [n=248403, nc=327210, nnz=0, k=4] [2.2 MB]
Creating vector for state rewards... [1.9 MB]
Creating vector for inf... [1.9 MB]
Allocating iteration vectors... [2 x 1.9 MB]
TOTAL: [19.5 MB]

Starting iterations...

Iterative method: 93 iterations in 0.80 seconds (average 0.003656, setup 0.46)

Value in the initial state: 0.3048863225283177

Time for model checking: 0.909 seconds.

Result: 0.3048863225283177 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"drop"}min=? [ F "deadlock" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=2,Objy1=0,Objx2=5,Objy2=3

Prob0A: 4 iterations in 0.06 seconds (average 0.016000, setup 0.00)

Prob1E: 5 iterations in 0.05 seconds (average 0.010400, setup 0.00)

Warning: PRISM hasn't checked for zero-reward loops.
Your minimum rewards may be too low...

goal = 1, inf = 0, maybe = 248402

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=248403, nc=327210, nnz=660019, k=4] [9.7 MB]
Building sparse matrix (transition rewards)... [n=248403, nc=327210, nnz=0, k=4] [2.2 MB]
Creating vector for state rewards... [1.9 MB]
Creating vector for inf... [1.9 MB]
Allocating iteration vectors... [2 x 1.9 MB]
TOTAL: [19.5 MB]

Starting iterations...

Iterative method: 86 iterations in 0.66 seconds (average 0.003628, setup 0.35)

Value in the initial state: 0.20548874072607748

Time for model checking: 0.801 seconds.

Result: 0.20548874072607748 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"charge"}max=? [ F "deadlock" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=2,Objy1=0,Objx2=5,Objy2=3

Prob0E: 4 iterations in 0.03 seconds (average 0.007000, setup 0.00)

Prob1A: 1 iterations in 0.00 seconds (average 0.004000, setup 0.00)

goal = 1, inf = 0, maybe = 248402

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=248403, nc=327210, nnz=660019, k=4] [9.7 MB]
Building sparse matrix (transition rewards)... [n=248403, nc=327210, nnz=7514, k=4] [2.3 MB]
Creating vector for state rewards... [1.9 MB]
Creating vector for inf... [1.9 MB]
Allocating iteration vectors... [2 x 1.9 MB]
TOTAL: [19.6 MB]

Starting iterations...

Iterative method: 89 iterations in 0.67 seconds (average 0.003640, setup 0.35)

Value in the initial state: 0.9797451207541203

Time for model checking: 0.731 seconds.

Result: 0.9797451207541203 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"charge"}min=? [ F "deadlock" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=2,Objy1=0,Objx2=5,Objy2=3

Prob0A: 4 iterations in 0.02 seconds (average 0.005000, setup 0.00)

Prob1E: 5 iterations in 0.04 seconds (average 0.008800, setup 0.00)

Warning: PRISM hasn't checked for zero-reward loops.
Your minimum rewards may be too low...

goal = 1, inf = 0, maybe = 248402

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=248403, nc=327210, nnz=660019, k=4] [9.7 MB]
Building sparse matrix (transition rewards)... [n=248403, nc=327210, nnz=7514, k=4] [2.3 MB]
Creating vector for state rewards... [1.9 MB]
Creating vector for inf... [1.9 MB]
Allocating iteration vectors... [2 x 1.9 MB]
TOTAL: [19.6 MB]

Starting iterations...

Iterative method: 99 iterations in 0.72 seconds (average 0.003677, setup 0.35)

Value in the initial state: 0.08062981718694201

Time for model checking: 0.805 seconds.

Result: 0.08062981718694201 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"time"}max=? [ F "deadlock" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=2,Objy1=0,Objx2=5,Objy2=3

Prob0E: 4 iterations in 0.02 seconds (average 0.006000, setup 0.00)

Prob1A: 1 iterations in 0.00 seconds (average 0.004000, setup 0.00)

goal = 1, inf = 0, maybe = 248402

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=248403, nc=327210, nnz=660019, k=4] [9.7 MB]
Building sparse matrix (transition rewards)... [n=248403, nc=327210, nnz=124355, k=4] [3.6 MB]
Creating vector for state rewards... [1.9 MB]
Creating vector for inf... [1.9 MB]
Allocating iteration vectors... [2 x 1.9 MB]
TOTAL: [20.9 MB]

Starting iterations...

Iterative method: 93 iterations in 0.80 seconds (average 0.004430, setup 0.39)

Value in the initial state: 346.37084954255096

Time for model checking: 0.855 seconds.

Result: 346.37084954255096 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"time"}min=? [ F "deadlock" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=2,Objy1=0,Objx2=5,Objy2=3

Prob0A: 4 iterations in 0.02 seconds (average 0.006000, setup 0.00)

Prob1E: 5 iterations in 0.04 seconds (average 0.008000, setup 0.00)

Warning: PRISM hasn't checked for zero-reward loops.
Your minimum rewards may be too low...

goal = 1, inf = 0, maybe = 248402

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=248403, nc=327210, nnz=660019, k=4] [9.7 MB]
Building sparse matrix (transition rewards)... [n=248403, nc=327210, nnz=124355, k=4] [3.6 MB]
Creating vector for state rewards... [1.9 MB]
Creating vector for inf... [1.9 MB]
Allocating iteration vectors... [2 x 1.9 MB]
TOTAL: [20.9 MB]

Starting iterations...

Iterative method: 103 iterations in 0.85 seconds (average 0.004544, setup 0.38)

Value in the initial state: 114.18224696111716

Time for model checking: 0.939 seconds.

Result: 114.18224696111716 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"success"}max=? [ F "deadlock" ]/Pmin=? [ F "MissionSuccessful" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=2,Objy1=0,Objx2=5,Objy2=3

Prob0E: 4 iterations in 0.04 seconds (average 0.009000, setup 0.00)

Prob1A: 1 iterations in 0.01 seconds (average 0.008000, setup 0.00)

goal = 1, inf = 0, maybe = 248402

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=248403, nc=327210, nnz=660019, k=4] [9.7 MB]
Building sparse matrix (transition rewards)... [n=248403, nc=327210, nnz=1832, k=4] [2.2 MB]
Creating vector for state rewards... [1.9 MB]
Creating vector for inf... [1.9 MB]
Allocating iteration vectors... [2 x 1.9 MB]
TOTAL: [19.5 MB]

Starting iterations...

Iterative method: 95 iterations in 0.74 seconds (average 0.003663, setup 0.39)

Prob0E: 67 iterations in 0.56 seconds (average 0.008299, setup 0.00)

Prob1A: 3 iterations in 0.04 seconds (average 0.014667, setup 0.00)

yes = 930, no = 217390, maybe = 30083

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=248403, nc=51487, nnz=146629, k=2] [2.0 MB]
Creating vector for yes... [1.9 MB]
Allocating iteration vectors... [2 x 1.9 MB]
TOTAL: [7.6 MB]

Starting iterations...

Iterative method: 91 iterations in 0.29 seconds (average 0.001495, setup 0.15)

Value in the initial state: 461.6151918952959

Time for model checking: 1.687 seconds.

Result: 461.6151918952959 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"success"}min=? [ F "deadlock" ]/Pmax=? [ F "MissionSuccessful" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=2,Objy1=0,Objx2=5,Objy2=3

Prob0A: 4 iterations in 0.08 seconds (average 0.021000, setup 0.00)

Prob1E: 5 iterations in 0.04 seconds (average 0.008000, setup 0.00)

Warning: PRISM hasn't checked for zero-reward loops.
Your minimum rewards may be too low...

goal = 1, inf = 0, maybe = 248402

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=248403, nc=327210, nnz=660019, k=4] [9.7 MB]
Building sparse matrix (transition rewards)... [n=248403, nc=327210, nnz=1832, k=4] [2.2 MB]
Creating vector for state rewards... [1.9 MB]
Creating vector for inf... [1.9 MB]
Allocating iteration vectors... [2 x 1.9 MB]
TOTAL: [19.5 MB]

Starting iterations...

Iterative method: 96 iterations in 0.72 seconds (average 0.003375, setup 0.39)

Prob0A: 60 iterations in 0.26 seconds (average 0.004333, setup 0.00)

Prob1E: 4 iterations in 0.03 seconds (average 0.008000, setup 0.00)

yes = 930, no = 212784, maybe = 34689

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=248403, nc=59107, nnz=167784, k=2] [2.2 MB]
Creating vector for yes... [1.9 MB]
Allocating iteration vectors... [2 x 1.9 MB]
TOTAL: [7.9 MB]

Starting iterations...

Iterative method: 98 iterations in 0.30 seconds (average 0.001592, setup 0.15)

Value in the initial state: 81.7411205232216

Time for model checking: 1.451 seconds.

Result: 81.7411205232216 (value in the initial state)

---------------------------------------------------------------------

Note: There were 16 warnings during computation.

