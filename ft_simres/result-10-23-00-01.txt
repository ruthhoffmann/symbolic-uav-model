PRISM
=====

Version: 4.3
Date: Tue Aug 02 12:34:39 BST 2016
Hostname: eglinton
Memory limits: cudd=1g, java(heap)=981.5m
Command line: prism uav_sim_ft_mdp.prism -s uav_mdp.props -const 'Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=0,Objy1=0,Objx2=0,Objy2=1,'

Parsing model file "uav_sim_ft_mdp.prism"...

Parsing properties file "uav_mdp.props"...

18 properties:
(1) Pmax=? [ F "MissionSuccessful" ]
(2) Pmin=? [ F "MissionSuccessful" ]
(3) Pmax=? [ F "MissionTimeUp" ]
(4) Pmin=? [ F "MissionTimeUp" ]
(5) Pmax=? [ F "Crash" ]
(6) Pmin=? [ F "Crash" ]
(7) Pmax=? [ F "AllAreaSearched" ]
(8) Pmin=? [ F "AllAreaSearched" ]
(9) Pmax=? [ F "MissionTimeUp"|"AllAreaSearched"|"Crash" ]
(10) Pmin=? [ F "MissionTimeUp"|"AllAreaSearched"|"Crash" ]
(11) R{"drop"}max=? [ F "deadlock" ]
(12) R{"drop"}min=? [ F "deadlock" ]
(13) R{"charge"}max=? [ F "deadlock" ]
(14) R{"charge"}min=? [ F "deadlock" ]
(15) R{"time"}max=? [ F "deadlock" ]
(16) R{"time"}min=? [ F "deadlock" ]
(17) R{"success"}max=? [ F "deadlock" ]/Pmin=? [ F "MissionSuccessful" ]
(18) R{"success"}min=? [ F "deadlock" ]/Pmax=? [ F "MissionSuccessful" ]

Type:        MDP
Modules:     Obj Move Robot T 
Variables:   NoOfObjs posx posy s1 retx rety b t c 

---------------------------------------------------------------------

Model checking: Pmax=? [ F "MissionSuccessful" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=0,Objy1=0,Objx2=0,Objy2=1

Building model...
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=0,Objy1=0,Objx2=0,Objy2=1

Warning: Guard for command 4 of module "Move" is never satisfied.

Warning: Guard for command 7 of module "Move" is never satisfied.

Warning: Guard for command 8 of module "Move" is never satisfied.

Warning: Guard for command 9 of module "Move" is never satisfied.

Warning: Guard for command 7 of module "Robot" is never satisfied.

Warning: Update 2 of command 3 of module "Robot" doesn't do anything ("(s1'=13)", line 249, column 44)

Warning: Update 2 of command 27 of module "T" doesn't do anything ("(c'=1)", line 372, column 108)

Warning: Guard for command 28 of module "T" is never satisfied.

Warning: Update 2 of command 9 of module "T" doesn't do anything ("(c'=1)", line 349, column 112)

Warning: Guard for command 10 of module "T" is never satisfied.

Warning: Update 2 of command 51 of module "T" doesn't do anything ("(c'=1)", line 403, column 113)

Warning: Guard for command 52 of module "T" is never satisfied.

Computing reachable states...

Reachability (BFS): 74 iterations in 1.07 seconds (average 0.014486, setup 0.00)

Time for model construction: 3.628 seconds.

Warning: Deadlocks detected and fixed in 1 states

Type:        MDP
States:      235303 (1 initial)
Transitions: 605854
Choices:     306089

Transition matrix: 173999 nodes (76 terminal), 605854 minterms, vars: 34r/34c/18nd

Prob0A: 52 iterations in 0.41 seconds (average 0.007923, setup 0.00)

Prob1E: 4 iterations in 0.06 seconds (average 0.016000, setup 0.00)

yes = 950, no = 201307, maybe = 33046

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=235303, nc=56835, nnz=161010, k=2] [2.1 MB]
Creating vector for yes... [1.8 MB]
Allocating iteration vectors... [2 x 1.8 MB]
TOTAL: [7.5 MB]

Starting iterations...

Iterative method: 88 iterations in 0.40 seconds (average 0.001636, setup 0.25)

Value in the initial state: 0.9805918670232614

Time for model checking: 0.998 seconds.

Result: 0.9805918670232614 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmin=? [ F "MissionSuccessful" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=0,Objy1=0,Objx2=0,Objy2=1

Prob0E: 54 iterations in 0.45 seconds (average 0.008370, setup 0.00)

Prob1A: 3 iterations in 0.05 seconds (average 0.017333, setup 0.00)

yes = 950, no = 206641, maybe = 27712

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=235303, nc=47605, nnz=134141, k=2] [1.8 MB]
Creating vector for yes... [1.8 MB]
Allocating iteration vectors... [2 x 1.8 MB]
TOTAL: [7.2 MB]

Starting iterations...

Iterative method: 82 iterations in 0.34 seconds (average 0.001512, setup 0.21)

Value in the initial state: 0.7417832896471537

Time for model checking: 1.192 seconds.

Result: 0.7417832896471537 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmax=? [ F "MissionTimeUp" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=0,Objy1=0,Objx2=0,Objy2=1

Prob0A: 57 iterations in 0.59 seconds (average 0.010316, setup 0.00)

Prob1E: 4 iterations in 0.06 seconds (average 0.014000, setup 0.00)

yes = 524, no = 143113, maybe = 91666

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=235303, nc=152735, nnz=415484, k=4] [5.1 MB]
Creating vector for yes... [1.8 MB]
Allocating iteration vectors... [2 x 1.8 MB]
TOTAL: [10.5 MB]

Starting iterations...

Iterative method: 89 iterations in 0.48 seconds (average 0.002112, setup 0.30)

Value in the initial state: 6.178418181338529E-4

Time for model checking: 1.365 seconds.

Result: 6.178418181338529E-4 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmin=? [ F "MissionTimeUp" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=0,Objy1=0,Objx2=0,Objy2=1

Prob0E: 63 iterations in 0.76 seconds (average 0.012000, setup 0.00)

Prob1A: 3 iterations in 0.05 seconds (average 0.017333, setup 0.00)

yes = 524, no = 143470, maybe = 91309

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=235303, nc=152097, nnz=414175, k=4] [5.1 MB]
Creating vector for yes... [1.8 MB]
Allocating iteration vectors... [2 x 1.8 MB]
TOTAL: [10.5 MB]

Starting iterations...

Iterative method: 105 iterations in 0.50 seconds (average 0.002095, setup 0.28)

Value in the initial state: 2.7950507507654654E-6

Time for model checking: 1.529 seconds.

Result: 2.7950507507654654E-6 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmax=? [ F "Crash" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=0,Objy1=0,Objx2=0,Objy2=1

Prob0A: 3 iterations in 0.08 seconds (average 0.026667, setup 0.00)

Prob1E: 204 iterations in 2.78 seconds (average 0.013608, setup 0.00)

yes = 115359, no = 2696, maybe = 117248

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=235303, nc=188034, nnz=487799, k=4] [6.0 MB]
Creating vector for yes... [1.8 MB]
Allocating iteration vectors... [2 x 1.8 MB]
TOTAL: [11.4 MB]

Starting iterations...

Iterative method: 85 iterations in 0.54 seconds (average 0.002212, setup 0.36)

Value in the initial state: 0.031155402557656742

Time for model checking: 4.524 seconds.

Result: 0.031155402557656742 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmin=? [ F "Crash" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=0,Objy1=0,Objx2=0,Objy2=1

Prob0E: 3 iterations in 0.07 seconds (average 0.024000, setup 0.00)

Prob1A: 49 iterations in 0.68 seconds (average 0.013796, setup 0.00)

yes = 115359, no = 2696, maybe = 117248

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=235303, nc=188034, nnz=487799, k=4] [6.0 MB]
Creating vector for yes... [1.8 MB]
Allocating iteration vectors... [2 x 1.8 MB]
TOTAL: [11.4 MB]

Starting iterations...

Iterative method: 87 iterations in 0.50 seconds (average 0.002345, setup 0.30)

Value in the initial state: 0.017290011631894926

Time for model checking: 1.548 seconds.

Result: 0.017290011631894926 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmax=? [ F "AllAreaSearched" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=0,Objy1=0,Objx2=0,Objy2=1

Prob0A: 63 iterations in 0.47 seconds (average 0.007429, setup 0.00)

Prob1E: 4 iterations in 0.04 seconds (average 0.011000, setup 0.00)

yes = 1221, no = 188650, maybe = 45432

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=235303, nc=68052, nnz=175115, k=2] [2.3 MB]
Creating vector for yes... [1.8 MB]
Allocating iteration vectors... [2 x 1.8 MB]
TOTAL: [7.7 MB]

Starting iterations...

Iterative method: 86 iterations in 0.32 seconds (average 0.001721, setup 0.17)

Value in the initial state: 0.2296176619242075

Time for model checking: 1.276 seconds.

Result: 0.2296176619242075 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmin=? [ F "AllAreaSearched" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=0,Objy1=0,Objx2=0,Objy2=1

Prob0E: 66 iterations in 0.49 seconds (average 0.007394, setup 0.00)

Prob1A: 3 iterations in 0.04 seconds (average 0.013333, setup 0.00)

yes = 1221, no = 193873, maybe = 40209

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=235303, nc=59273, nnz=150550, k=2] [2.0 MB]
Creating vector for yes... [1.8 MB]
Allocating iteration vectors... [2 x 1.8 MB]
TOTAL: [7.4 MB]

Starting iterations...

Iterative method: 93 iterations in 0.28 seconds (average 0.001634, setup 0.13)

Value in the initial state: 0.0017484238040957132

Time for model checking: 1.246 seconds.

Result: 0.0017484238040957132 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmax=? [ F "MissionTimeUp"|"AllAreaSearched"|"Crash" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=0,Objy1=0,Objx2=0,Objy2=1

Prob0A: 3 iterations in 0.08 seconds (average 0.028000, setup 0.00)

Prob1E: 212 iterations in 2.90 seconds (average 0.013660, setup 0.00)

yes = 206640, no = 951, maybe = 27712

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=235303, nc=47605, nnz=134141, k=2] [1.8 MB]
Creating vector for yes... [1.8 MB]
Allocating iteration vectors... [2 x 1.8 MB]
TOTAL: [7.2 MB]

Starting iterations...

Iterative method: 76 iterations in 0.43 seconds (average 0.001579, setup 0.31)

Value in the initial state: 0.25821628753947484

Time for model checking: 4.235 seconds.

Result: 0.25821628753947484 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmin=? [ F "MissionTimeUp"|"AllAreaSearched"|"Crash" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=0,Objy1=0,Objx2=0,Objy2=1

Prob0E: 3 iterations in 0.09 seconds (average 0.029333, setup 0.00)

Prob1A: 52 iterations in 0.43 seconds (average 0.008308, setup 0.00)

yes = 201306, no = 951, maybe = 33046

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=235303, nc=56835, nnz=161010, k=2] [2.1 MB]
Creating vector for yes... [1.8 MB]
Allocating iteration vectors... [2 x 1.8 MB]
TOTAL: [7.5 MB]

Starting iterations...

Iterative method: 87 iterations in 0.31 seconds (average 0.001609, setup 0.17)

Value in the initial state: 0.019407403694568337

Time for model checking: 1.052 seconds.

Result: 0.019407403694568337 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"drop"}max=? [ F "deadlock" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=0,Objy1=0,Objx2=0,Objy2=1

Prob0E: 4 iterations in 0.05 seconds (average 0.012000, setup 0.00)

Prob1A: 1 iterations in 0.00 seconds (average 0.004000, setup 0.00)

goal = 1, inf = 0, maybe = 235302

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=235303, nc=306088, nnz=605853, k=4] [9.0 MB]
Building sparse matrix (transition rewards)... [n=235303, nc=306088, nnz=0, k=4] [2.1 MB]
Creating vector for state rewards... [1.8 MB]
Creating vector for inf... [1.8 MB]
Allocating iteration vectors... [2 x 1.8 MB]
TOTAL: [18.2 MB]

Starting iterations...

Iterative method: 85 iterations in 0.71 seconds (average 0.003435, setup 0.42)

Value in the initial state: 0.21141828014846464

Time for model checking: 1.011 seconds.

Result: 0.21141828014846464 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"drop"}min=? [ F "deadlock" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=0,Objy1=0,Objx2=0,Objy2=1

Prob0A: 4 iterations in 0.04 seconds (average 0.011000, setup 0.00)

Prob1E: 5 iterations in 0.05 seconds (average 0.010400, setup 0.00)

Warning: PRISM hasn't checked for zero-reward loops.
Your minimum rewards may be too low...

goal = 1, inf = 0, maybe = 235302

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=235303, nc=306088, nnz=605853, k=4] [9.0 MB]
Building sparse matrix (transition rewards)... [n=235303, nc=306088, nnz=0, k=4] [2.1 MB]
Creating vector for state rewards... [1.8 MB]
Creating vector for inf... [1.8 MB]
Allocating iteration vectors... [2 x 1.8 MB]
TOTAL: [18.2 MB]

Starting iterations...

Iterative method: 75 iterations in 0.59 seconds (average 0.003467, setup 0.33)

Value in the initial state: 0.18451133067130854

Time for model checking: 1.057 seconds.

Result: 0.18451133067130854 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"charge"}max=? [ F "deadlock" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=0,Objy1=0,Objx2=0,Objy2=1

Prob0E: 4 iterations in 0.02 seconds (average 0.006000, setup 0.00)

Prob1A: 1 iterations in 0.01 seconds (average 0.008000, setup 0.00)

goal = 1, inf = 0, maybe = 235302

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=235303, nc=306088, nnz=605853, k=4] [9.0 MB]
Building sparse matrix (transition rewards)... [n=235303, nc=306088, nnz=6458, k=4] [2.1 MB]
Creating vector for state rewards... [1.8 MB]
Creating vector for inf... [1.8 MB]
Allocating iteration vectors... [2 x 1.8 MB]
TOTAL: [18.3 MB]

Starting iterations...

Iterative method: 83 iterations in 0.61 seconds (average 0.003422, setup 0.33)

Value in the initial state: 0.29184102693324904

Time for model checking: 0.707 seconds.

Result: 0.29184102693324904 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"charge"}min=? [ F "deadlock" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=0,Objy1=0,Objx2=0,Objy2=1

Prob0A: 4 iterations in 0.02 seconds (average 0.006000, setup 0.00)

Prob1E: 5 iterations in 0.04 seconds (average 0.008000, setup 0.00)

Warning: PRISM hasn't checked for zero-reward loops.
Your minimum rewards may be too low...

goal = 1, inf = 0, maybe = 235302

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=235303, nc=306088, nnz=605853, k=4] [9.0 MB]
Building sparse matrix (transition rewards)... [n=235303, nc=306088, nnz=6458, k=4] [2.1 MB]
Creating vector for state rewards... [1.8 MB]
Creating vector for inf... [1.8 MB]
Allocating iteration vectors... [2 x 1.8 MB]
TOTAL: [18.3 MB]

Starting iterations...

Iterative method: 88 iterations in 0.64 seconds (average 0.003455, setup 0.33)

Value in the initial state: 0.05211136646432376

Time for model checking: 0.906 seconds.

Result: 0.05211136646432376 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"time"}max=? [ F "deadlock" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=0,Objy1=0,Objx2=0,Objy2=1

Prob0E: 4 iterations in 0.02 seconds (average 0.005000, setup 0.00)

Prob1A: 1 iterations in 0.00 seconds (average 0.004000, setup 0.00)

goal = 1, inf = 0, maybe = 235302

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=235303, nc=306088, nnz=605853, k=4] [9.0 MB]
Building sparse matrix (transition rewards)... [n=235303, nc=306088, nnz=118053, k=4] [3.4 MB]
Creating vector for state rewards... [1.8 MB]
Creating vector for inf... [1.8 MB]
Allocating iteration vectors... [2 x 1.8 MB]
TOTAL: [19.6 MB]

Starting iterations...

Iterative method: 88 iterations in 0.68 seconds (average 0.003545, setup 0.37)

Value in the initial state: 171.85706474289262

Time for model checking: 0.962 seconds.

Result: 171.85706474289262 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"time"}min=? [ F "deadlock" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=0,Objy1=0,Objx2=0,Objy2=1

Prob0A: 4 iterations in 0.02 seconds (average 0.006000, setup 0.00)

Prob1E: 5 iterations in 0.04 seconds (average 0.008800, setup 0.00)

Warning: PRISM hasn't checked for zero-reward loops.
Your minimum rewards may be too low...

goal = 1, inf = 0, maybe = 235302

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=235303, nc=306088, nnz=605853, k=4] [9.0 MB]
Building sparse matrix (transition rewards)... [n=235303, nc=306088, nnz=118053, k=4] [3.4 MB]
Creating vector for state rewards... [1.8 MB]
Creating vector for inf... [1.8 MB]
Allocating iteration vectors... [2 x 1.8 MB]
TOTAL: [19.6 MB]

Starting iterations...

Iterative method: 94 iterations in 0.70 seconds (average 0.003574, setup 0.36)

Value in the initial state: 95.79578981479405

Time for model checking: 1.165 seconds.

Result: 95.79578981479405 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"success"}max=? [ F "deadlock" ]/Pmin=? [ F "MissionSuccessful" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=0,Objy1=0,Objx2=0,Objy2=1

Prob0E: 4 iterations in 0.02 seconds (average 0.006000, setup 0.00)

Prob1A: 1 iterations in 0.00 seconds (average 0.004000, setup 0.00)

goal = 1, inf = 0, maybe = 235302

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=235303, nc=306088, nnz=605853, k=4] [9.0 MB]
Building sparse matrix (transition rewards)... [n=235303, nc=306088, nnz=1872, k=4] [2.1 MB]
Creating vector for state rewards... [1.8 MB]
Creating vector for inf... [1.8 MB]
Allocating iteration vectors... [2 x 1.8 MB]
TOTAL: [18.3 MB]

Starting iterations...

Iterative method: 88 iterations in 0.62 seconds (average 0.003455, setup 0.32)

Prob0E: 54 iterations in 0.38 seconds (average 0.006963, setup 0.00)

Prob1A: 3 iterations in 0.04 seconds (average 0.014667, setup 0.00)

yes = 950, no = 206641, maybe = 27712

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=235303, nc=47605, nnz=134141, k=2] [1.8 MB]
Creating vector for yes... [1.8 MB]
Allocating iteration vectors... [2 x 1.8 MB]
TOTAL: [7.2 MB]

Starting iterations...

Iterative method: 82 iterations in 0.29 seconds (average 0.001561, setup 0.16)

Value in the initial state: 227.03097115605942

Time for model checking: 1.715 seconds.

Result: 227.03097115605942 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"success"}min=? [ F "deadlock" ]/Pmax=? [ F "MissionSuccessful" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=0,Objy1=0,Objx2=0,Objy2=1

Prob0A: 4 iterations in 0.04 seconds (average 0.011000, setup 0.00)

Prob1E: 5 iterations in 0.06 seconds (average 0.011200, setup 0.00)

Warning: PRISM hasn't checked for zero-reward loops.
Your minimum rewards may be too low...

goal = 1, inf = 0, maybe = 235302

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=235303, nc=306088, nnz=605853, k=4] [9.0 MB]
Building sparse matrix (transition rewards)... [n=235303, nc=306088, nnz=1872, k=4] [2.1 MB]
Creating vector for state rewards... [1.8 MB]
Creating vector for inf... [1.8 MB]
Allocating iteration vectors... [2 x 1.8 MB]
TOTAL: [18.3 MB]

Starting iterations...

Iterative method: 86 iterations in 0.66 seconds (average 0.003442, setup 0.37)

Prob0A: 52 iterations in 0.37 seconds (average 0.007154, setup 0.00)

Prob1E: 4 iterations in 0.04 seconds (average 0.009000, setup 0.00)

yes = 950, no = 201307, maybe = 33046

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=235303, nc=56835, nnz=161010, k=2] [2.1 MB]
Creating vector for yes... [1.8 MB]
Allocating iteration vectors... [2 x 1.8 MB]
TOTAL: [7.5 MB]

Starting iterations...

Iterative method: 88 iterations in 0.29 seconds (average 0.001545, setup 0.16)

Value in the initial state: 79.22941664506949

Time for model checking: 1.526 seconds.

Result: 79.22941664506949 (value in the initial state)

---------------------------------------------------------------------

Note: There were 17 warnings during computation.

