PRISM
=====

Version: 4.3
Date: Tue Aug 02 12:39:30 BST 2016
Hostname: eglinton
Memory limits: cudd=1g, java(heap)=981.5m
Command line: prism uav_sim_ft_mdp.prism -s uav_mdp.props -const 'Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=2,Objy1=3,Objx2=6,Objy2=2,'

Parsing model file "uav_sim_ft_mdp.prism"...

Parsing properties file "uav_mdp.props"...

18 properties:
(1) Pmax=? [ F "MissionSuccessful" ]
(2) Pmin=? [ F "MissionSuccessful" ]
(3) Pmax=? [ F "MissionTimeUp" ]
(4) Pmin=? [ F "MissionTimeUp" ]
(5) Pmax=? [ F "Crash" ]
(6) Pmin=? [ F "Crash" ]
(7) Pmax=? [ F "AllAreaSearched" ]
(8) Pmin=? [ F "AllAreaSearched" ]
(9) Pmax=? [ F "MissionTimeUp"|"AllAreaSearched"|"Crash" ]
(10) Pmin=? [ F "MissionTimeUp"|"AllAreaSearched"|"Crash" ]
(11) R{"drop"}max=? [ F "deadlock" ]
(12) R{"drop"}min=? [ F "deadlock" ]
(13) R{"charge"}max=? [ F "deadlock" ]
(14) R{"charge"}min=? [ F "deadlock" ]
(15) R{"time"}max=? [ F "deadlock" ]
(16) R{"time"}min=? [ F "deadlock" ]
(17) R{"success"}max=? [ F "deadlock" ]/Pmin=? [ F "MissionSuccessful" ]
(18) R{"success"}min=? [ F "deadlock" ]/Pmax=? [ F "MissionSuccessful" ]

Type:        MDP
Modules:     Obj Move Robot T 
Variables:   NoOfObjs posx posy s1 retx rety b t c 

---------------------------------------------------------------------

Model checking: Pmax=? [ F "MissionSuccessful" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=2,Objy1=3,Objx2=6,Objy2=2

Building model...
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=2,Objy1=3,Objx2=6,Objy2=2

Warning: Guard for command 7 of module "Move" is never satisfied.

Warning: Guard for command 8 of module "Move" is never satisfied.

Warning: Guard for command 9 of module "Move" is never satisfied.

Warning: Guard for command 7 of module "Robot" is never satisfied.

Warning: Update 2 of command 3 of module "Robot" doesn't do anything ("(s1'=13)", line 249, column 44)

Warning: Update 2 of command 27 of module "T" doesn't do anything ("(c'=1)", line 372, column 108)

Warning: Guard for command 28 of module "T" is never satisfied.

Warning: Update 2 of command 9 of module "T" doesn't do anything ("(c'=1)", line 349, column 112)

Warning: Guard for command 10 of module "T" is never satisfied.

Warning: Update 2 of command 51 of module "T" doesn't do anything ("(c'=1)", line 403, column 113)

Warning: Guard for command 52 of module "T" is never satisfied.

Computing reachable states...

Reachability (BFS): 82 iterations in 0.46 seconds (average 0.005659, setup 0.00)

Time for model construction: 1.503 seconds.

Warning: Deadlocks detected and fixed in 1 states

Type:        MDP
States:      108785 (1 initial)
Transitions: 300950
Choices:     146663

Transition matrix: 153997 nodes (76 terminal), 300950 minterms, vars: 34r/34c/18nd

Prob0A: 67 iterations in 0.23 seconds (average 0.003463, setup 0.00)

Prob1E: 4 iterations in 0.04 seconds (average 0.009000, setup 0.00)

yes = 207, no = 101323, maybe = 7255

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=108785, nc=13232, nnz=39671, k=2] [584.1 KB]
Creating vector for yes... [849.9 KB]
Allocating iteration vectors... [2 x 849.9 KB]
TOTAL: [3.1 MB]

Starting iterations...

Iterative method: 100 iterations in 0.16 seconds (average 0.000640, setup 0.10)

Value in the initial state: 0.013485018805349719

Time for model checking: 0.444 seconds.

Result: 0.013485018805349719 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmin=? [ F "MissionSuccessful" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=2,Objy1=3,Objx2=6,Objy2=2

Prob0E: 23 iterations in 0.07 seconds (average 0.002957, setup 0.00)

Prob1A: 3 iterations in 0.02 seconds (average 0.006667, setup 0.00)

yes = 207, no = 102631, maybe = 5947

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=108785, nc=10867, nnz=32446, k=2] [497.1 KB]
Creating vector for yes... [849.9 KB]
Allocating iteration vectors... [2 x 849.9 KB]
TOTAL: [3.0 MB]

Starting iterations...

Iterative method: 43 iterations in 0.06 seconds (average 0.000558, setup 0.04)

Value in the initial state: 0.0

Time for model checking: 0.15 seconds.

Result: 0.0 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmax=? [ F "MissionTimeUp" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=2,Objy1=3,Objx2=6,Objy2=2

Prob0A: 70 iterations in 0.40 seconds (average 0.005771, setup 0.00)

Prob1E: 4 iterations in 0.04 seconds (average 0.010000, setup 0.00)

yes = 240, no = 70043, maybe = 38502

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=108785, nc=69643, nnz=200214, k=4] [2.5 MB]
Creating vector for yes... [849.9 KB]
Allocating iteration vectors... [2 x 849.9 KB]
TOTAL: [5.0 MB]

Starting iterations...

Iterative method: 95 iterations in 0.28 seconds (average 0.000926, setup 0.19)

Value in the initial state: 2.036619307339902E-4

Time for model checking: 0.744 seconds.

Result: 2.036619307339902E-4 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmin=? [ F "MissionTimeUp" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=2,Objy1=3,Objx2=6,Objy2=2

Prob0E: 77 iterations in 0.48 seconds (average 0.006182, setup 0.00)

Prob1A: 3 iterations in 0.04 seconds (average 0.012000, setup 0.00)

yes = 240, no = 70314, maybe = 38231

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=108785, nc=69153, nnz=199211, k=4] [2.4 MB]
Creating vector for yes... [849.9 KB]
Allocating iteration vectors... [2 x 849.9 KB]
TOTAL: [4.9 MB]

Starting iterations...

Iterative method: 111 iterations in 0.26 seconds (average 0.000937, setup 0.16)

Value in the initial state: 7.473945724623176E-7

Time for model checking: 0.794 seconds.

Result: 7.473945724623176E-7 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmax=? [ F "Crash" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=2,Objy1=3,Objx2=6,Objy2=2

Prob0A: 3 iterations in 0.10 seconds (average 0.034667, setup 0.00)

Prob1E: 180 iterations in 1.69 seconds (average 0.009400, setup 0.00)

yes = 53103, no = 1687, maybe = 53995

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=108785, nc=91873, nnz=246160, k=4] [3.0 MB]
Creating vector for yes... [849.9 KB]
Allocating iteration vectors... [2 x 849.9 KB]
TOTAL: [5.5 MB]

Starting iterations...

Iterative method: 92 iterations in 0.32 seconds (average 0.001000, setup 0.23)

Value in the initial state: 0.01758587443929784

Time for model checking: 2.089 seconds.

Result: 0.01758587443929784 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmin=? [ F "Crash" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=2,Objy1=3,Objx2=6,Objy2=2

Prob0E: 3 iterations in 0.06 seconds (average 0.018667, setup 0.00)

Prob1A: 47 iterations in 0.34 seconds (average 0.007319, setup 0.00)

yes = 53103, no = 1687, maybe = 53995

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=108785, nc=91873, nnz=246160, k=4] [3.0 MB]
Creating vector for yes... [849.9 KB]
Allocating iteration vectors... [2 x 849.9 KB]
TOTAL: [5.5 MB]

Starting iterations...

Iterative method: 87 iterations in 0.28 seconds (average 0.001057, setup 0.19)

Value in the initial state: 0.013515758641832494

Time for model checking: 0.698 seconds.

Result: 0.013515758641832494 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmax=? [ F "AllAreaSearched" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=2,Objy1=3,Objx2=6,Objy2=2

Prob0A: 47 iterations in 0.23 seconds (average 0.004851, setup 0.00)

Prob1E: 4 iterations in 0.03 seconds (average 0.008000, setup 0.00)

yes = 1239, no = 73898, maybe = 33648

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=108785, nc=55143, nnz=150505, k=2] [1.9 MB]
Creating vector for yes... [849.9 KB]
Allocating iteration vectors... [2 x 849.9 KB]
TOTAL: [4.4 MB]

Starting iterations...

Iterative method: 93 iterations in 0.23 seconds (average 0.000817, setup 0.16)

Value in the initial state: 0.9864817085885121

Time for model checking: 0.509 seconds.

Result: 0.9864817085885121 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmin=? [ F "AllAreaSearched" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=2,Objy1=3,Objx2=6,Objy2=2

Prob0E: 47 iterations in 0.27 seconds (average 0.005787, setup 0.00)

Prob1A: 3 iterations in 0.03 seconds (average 0.010667, setup 0.00)

yes = 1239, no = 76783, maybe = 30763

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=108785, nc=49659, nnz=133964, k=2] [1.7 MB]
Creating vector for yes... [849.9 KB]
Allocating iteration vectors... [2 x 849.9 KB]
TOTAL: [4.2 MB]

Starting iterations...

Iterative method: 93 iterations in 0.22 seconds (average 0.000817, setup 0.14)

Value in the initial state: 0.9690473385049901

Time for model checking: 0.534 seconds.

Result: 0.9690473385049901 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmax=? [ F "MissionTimeUp"|"AllAreaSearched"|"Crash" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=2,Objy1=3,Objx2=6,Objy2=2

Prob0A: 3 iterations in 0.06 seconds (average 0.021333, setup 0.00)

Prob1E: 88 iterations in 0.72 seconds (average 0.008182, setup 0.00)

yes = 102630, no = 208, maybe = 5947

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=108785, nc=10867, nnz=32446, k=2] [497.1 KB]
Creating vector for yes... [849.9 KB]
Allocating iteration vectors... [2 x 849.9 KB]
TOTAL: [3.0 MB]

Starting iterations...

Iterative method: 42 iterations in 0.08 seconds (average 0.000476, setup 0.06)

Value in the initial state: 1.0

Time for model checking: 0.881 seconds.

Result: 1.0 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmin=? [ F "MissionTimeUp"|"AllAreaSearched"|"Crash" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=2,Objy1=3,Objx2=6,Objy2=2

Prob0E: 3 iterations in 0.03 seconds (average 0.009333, setup 0.00)

Prob1A: 67 iterations in 0.35 seconds (average 0.005254, setup 0.00)

yes = 101322, no = 208, maybe = 7255

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=108785, nc=13232, nnz=39671, k=2] [584.1 KB]
Creating vector for yes... [849.9 KB]
Allocating iteration vectors... [2 x 849.9 KB]
TOTAL: [3.1 MB]

Starting iterations...

Iterative method: 75 iterations in 0.13 seconds (average 0.000640, setup 0.08)

Value in the initial state: 0.9865103493048821

Time for model checking: 0.525 seconds.

Result: 0.9865103493048821 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"drop"}max=? [ F "deadlock" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=2,Objy1=3,Objx2=6,Objy2=2

Prob0E: 4 iterations in 0.06 seconds (average 0.016000, setup 0.00)

Prob1A: 1 iterations in 0.00 seconds (average 0.004000, setup 0.00)

goal = 1, inf = 0, maybe = 108784

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=108785, nc=146662, nnz=300949, k=4] [4.4 MB]
Building sparse matrix (transition rewards)... [n=108785, nc=146662, nnz=0, k=4] [997.8 KB]
Creating vector for state rewards... [849.9 KB]
Creating vector for inf... [849.9 KB]
Allocating iteration vectors... [2 x 849.9 KB]
TOTAL: [8.7 MB]

Starting iterations...

Iterative method: 88 iterations in 0.40 seconds (average 0.001591, setup 0.26)

Value in the initial state: 0.13968656967290247

Time for model checking: 0.482 seconds.

Result: 0.13968656967290247 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"drop"}min=? [ F "deadlock" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=2,Objy1=3,Objx2=6,Objy2=2

Prob0A: 4 iterations in 0.04 seconds (average 0.009000, setup 0.00)

Prob1E: 5 iterations in 0.03 seconds (average 0.006400, setup 0.00)

Warning: PRISM hasn't checked for zero-reward loops.
Your minimum rewards may be too low...

goal = 1, inf = 0, maybe = 108784

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=108785, nc=146662, nnz=300949, k=4] [4.4 MB]
Building sparse matrix (transition rewards)... [n=108785, nc=146662, nnz=0, k=4] [997.8 KB]
Creating vector for state rewards... [849.9 KB]
Creating vector for inf... [849.9 KB]
Allocating iteration vectors... [2 x 849.9 KB]
TOTAL: [8.7 MB]

Starting iterations...

Iterative method: 78 iterations in 0.30 seconds (average 0.001590, setup 0.17)

Value in the initial state: 0.13684260857236166

Time for model checking: 0.383 seconds.

Result: 0.13684260857236166 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"charge"}max=? [ F "deadlock" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=2,Objy1=3,Objx2=6,Objy2=2

Prob0E: 4 iterations in 0.02 seconds (average 0.006000, setup 0.00)

Prob1A: 1 iterations in 0.00 seconds (average 0.004000, setup 0.00)

goal = 1, inf = 0, maybe = 108784

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=108785, nc=146662, nnz=300949, k=4] [4.4 MB]
Building sparse matrix (transition rewards)... [n=108785, nc=146662, nnz=3621, k=4] [1.0 MB]
Creating vector for state rewards... [849.9 KB]
Creating vector for inf... [849.9 KB]
Allocating iteration vectors... [2 x 849.9 KB]
TOTAL: [8.8 MB]

Starting iterations...

Iterative method: 91 iterations in 0.32 seconds (average 0.001582, setup 0.18)

Value in the initial state: 0.029339183696409532

Time for model checking: 0.359 seconds.

Result: 0.029339183696409532 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"charge"}min=? [ F "deadlock" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=2,Objy1=3,Objx2=6,Objy2=2

Prob0A: 4 iterations in 0.02 seconds (average 0.005000, setup 0.00)

Prob1E: 5 iterations in 0.03 seconds (average 0.006400, setup 0.00)

Warning: PRISM hasn't checked for zero-reward loops.
Your minimum rewards may be too low...

goal = 1, inf = 0, maybe = 108784

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=108785, nc=146662, nnz=300949, k=4] [4.4 MB]
Building sparse matrix (transition rewards)... [n=108785, nc=146662, nnz=3621, k=4] [1.0 MB]
Creating vector for state rewards... [849.9 KB]
Creating vector for inf... [849.9 KB]
Allocating iteration vectors... [2 x 849.9 KB]
TOTAL: [8.8 MB]

Starting iterations...

Iterative method: 102 iterations in 0.34 seconds (average 0.001608, setup 0.18)

Value in the initial state: 0.004350065410284161

Time for model checking: 0.407 seconds.

Result: 0.004350065410284161 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"time"}max=? [ F "deadlock" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=2,Objy1=3,Objx2=6,Objy2=2

Prob0E: 4 iterations in 0.02 seconds (average 0.005000, setup 0.00)

Prob1A: 1 iterations in 0.00 seconds (average 0.004000, setup 0.00)

goal = 1, inf = 0, maybe = 108784

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=108785, nc=146662, nnz=300949, k=4] [4.4 MB]
Building sparse matrix (transition rewards)... [n=108785, nc=146662, nnz=54788, k=4] [1.6 MB]
Creating vector for state rewards... [849.9 KB]
Creating vector for inf... [849.9 KB]
Allocating iteration vectors... [2 x 849.9 KB]
TOTAL: [9.3 MB]

Starting iterations...

Iterative method: 95 iterations in 0.35 seconds (average 0.001600, setup 0.20)

Value in the initial state: 97.45417476045982

Time for model checking: 0.386 seconds.

Result: 97.45417476045982 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"time"}min=? [ F "deadlock" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=2,Objy1=3,Objx2=6,Objy2=2

Prob0A: 4 iterations in 0.02 seconds (average 0.005000, setup 0.00)

Prob1E: 5 iterations in 0.03 seconds (average 0.006400, setup 0.00)

Warning: PRISM hasn't checked for zero-reward loops.
Your minimum rewards may be too low...

goal = 1, inf = 0, maybe = 108784

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=108785, nc=146662, nnz=300949, k=4] [4.4 MB]
Building sparse matrix (transition rewards)... [n=108785, nc=146662, nnz=54788, k=4] [1.6 MB]
Creating vector for state rewards... [849.9 KB]
Creating vector for inf... [849.9 KB]
Allocating iteration vectors... [2 x 849.9 KB]
TOTAL: [9.3 MB]

Starting iterations...

Iterative method: 101 iterations in 0.36 seconds (average 0.001624, setup 0.20)

Value in the initial state: 75.02237885951888

Time for model checking: 0.425 seconds.

Result: 75.02237885951888 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"success"}max=? [ F "deadlock" ]/Pmin=? [ F "MissionSuccessful" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=2,Objy1=3,Objx2=6,Objy2=2

Prob0E: 4 iterations in 0.02 seconds (average 0.006000, setup 0.00)

Prob1A: 1 iterations in 0.00 seconds (average 0.000000, setup 0.00)

goal = 1, inf = 0, maybe = 108784

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=108785, nc=146662, nnz=300949, k=4] [4.4 MB]
Building sparse matrix (transition rewards)... [n=108785, nc=146662, nnz=408, k=4] [1002.6 KB]
Creating vector for state rewards... [849.9 KB]
Creating vector for inf... [849.9 KB]
Allocating iteration vectors... [2 x 849.9 KB]
TOTAL: [8.7 MB]

Starting iterations...

Iterative method: 97 iterations in 0.32 seconds (average 0.001526, setup 0.17)

Prob0E: 23 iterations in 0.06 seconds (average 0.002609, setup 0.00)

Prob1A: 3 iterations in 0.02 seconds (average 0.006667, setup 0.00)

yes = 207, no = 102631, maybe = 5947

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=108785, nc=10867, nnz=32446, k=2] [497.1 KB]
Creating vector for yes... [849.9 KB]
Allocating iteration vectors... [2 x 849.9 KB]
TOTAL: [3.0 MB]

Starting iterations...

Iterative method: 43 iterations in 0.07 seconds (average 0.000558, setup 0.05)

Value in the initial state: Infinity

Time for model checking: 0.529 seconds.

Result: Infinity (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"success"}min=? [ F "deadlock" ]/Pmax=? [ F "MissionSuccessful" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=2,Objy1=3,Objx2=6,Objy2=2

Prob0A: 4 iterations in 0.02 seconds (average 0.006000, setup 0.00)

Prob1E: 5 iterations in 0.03 seconds (average 0.005600, setup 0.00)

Warning: PRISM hasn't checked for zero-reward loops.
Your minimum rewards may be too low...

goal = 1, inf = 0, maybe = 108784

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=108785, nc=146662, nnz=300949, k=4] [4.4 MB]
Building sparse matrix (transition rewards)... [n=108785, nc=146662, nnz=408, k=4] [1002.6 KB]
Creating vector for state rewards... [849.9 KB]
Creating vector for inf... [849.9 KB]
Allocating iteration vectors... [2 x 849.9 KB]
TOTAL: [8.7 MB]

Starting iterations...

Iterative method: 48 iterations in 0.27 seconds (average 0.001583, setup 0.20)

Prob0A: 67 iterations in 0.18 seconds (average 0.002687, setup 0.00)

Prob1E: 4 iterations in 0.02 seconds (average 0.006000, setup 0.00)

yes = 207, no = 101323, maybe = 7255

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=108785, nc=13232, nnz=39671, k=2] [584.1 KB]
Creating vector for yes... [849.9 KB]
Allocating iteration vectors... [2 x 849.9 KB]
TOTAL: [3.1 MB]

Starting iterations...

Iterative method: 100 iterations in 0.13 seconds (average 0.000600, setup 0.07)

Value in the initial state: 0.0

Time for model checking: 0.673 seconds.

Result: 0.0 (value in the initial state)

---------------------------------------------------------------------

Note: There were 16 warnings during computation.

