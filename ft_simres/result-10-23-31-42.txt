PRISM
=====

Version: 4.3
Date: Tue Aug 02 12:40:15 BST 2016
Hostname: eglinton
Memory limits: cudd=1g, java(heap)=981.5m
Command line: prism uav_sim_ft_mdp.prism -s uav_mdp.props -const 'Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=3,Objy1=1,Objx2=4,Objy2=2,'

Parsing model file "uav_sim_ft_mdp.prism"...

Parsing properties file "uav_mdp.props"...

18 properties:
(1) Pmax=? [ F "MissionSuccessful" ]
(2) Pmin=? [ F "MissionSuccessful" ]
(3) Pmax=? [ F "MissionTimeUp" ]
(4) Pmin=? [ F "MissionTimeUp" ]
(5) Pmax=? [ F "Crash" ]
(6) Pmin=? [ F "Crash" ]
(7) Pmax=? [ F "AllAreaSearched" ]
(8) Pmin=? [ F "AllAreaSearched" ]
(9) Pmax=? [ F "MissionTimeUp"|"AllAreaSearched"|"Crash" ]
(10) Pmin=? [ F "MissionTimeUp"|"AllAreaSearched"|"Crash" ]
(11) R{"drop"}max=? [ F "deadlock" ]
(12) R{"drop"}min=? [ F "deadlock" ]
(13) R{"charge"}max=? [ F "deadlock" ]
(14) R{"charge"}min=? [ F "deadlock" ]
(15) R{"time"}max=? [ F "deadlock" ]
(16) R{"time"}min=? [ F "deadlock" ]
(17) R{"success"}max=? [ F "deadlock" ]/Pmin=? [ F "MissionSuccessful" ]
(18) R{"success"}min=? [ F "deadlock" ]/Pmax=? [ F "MissionSuccessful" ]

Type:        MDP
Modules:     Obj Move Robot T 
Variables:   NoOfObjs posx posy s1 retx rety b t c 

---------------------------------------------------------------------

Model checking: Pmax=? [ F "MissionSuccessful" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=3,Objy1=1,Objx2=4,Objy2=2

Building model...
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=3,Objy1=1,Objx2=4,Objy2=2

Warning: Guard for command 7 of module "Move" is never satisfied.

Warning: Guard for command 8 of module "Move" is never satisfied.

Warning: Guard for command 9 of module "Move" is never satisfied.

Warning: Guard for command 7 of module "Robot" is never satisfied.

Warning: Update 2 of command 3 of module "Robot" doesn't do anything ("(s1'=13)", line 249, column 44)

Warning: Update 2 of command 27 of module "T" doesn't do anything ("(c'=1)", line 372, column 108)

Warning: Guard for command 28 of module "T" is never satisfied.

Warning: Update 2 of command 9 of module "T" doesn't do anything ("(c'=1)", line 349, column 112)

Warning: Guard for command 10 of module "T" is never satisfied.

Warning: Update 2 of command 51 of module "T" doesn't do anything ("(c'=1)", line 403, column 113)

Warning: Guard for command 52 of module "T" is never satisfied.

Computing reachable states...

Reachability (BFS): 76 iterations in 0.78 seconds (average 0.010263, setup 0.00)

Time for model construction: 1.844 seconds.

Warning: Deadlocks detected and fixed in 1 states

Type:        MDP
States:      195573 (1 initial)
Transitions: 520518
Choices:     259418

Transition matrix: 168459 nodes (76 terminal), 520518 minterms, vars: 34r/34c/18nd

Prob0A: 47 iterations in 0.36 seconds (average 0.007660, setup 0.00)

Prob1E: 4 iterations in 0.05 seconds (average 0.013000, setup 0.00)

yes = 949, no = 163951, maybe = 30673

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=195573, nc=53738, nnz=152585, k=2] [2.0 MB]
Creating vector for yes... [1.5 MB]
Allocating iteration vectors... [2 x 1.5 MB]
TOTAL: [6.5 MB]

Starting iterations...

Iterative method: 90 iterations in 0.32 seconds (average 0.001333, setup 0.20)

Value in the initial state: 0.9801792468678581

Time for model checking: 0.76 seconds.

Result: 0.9801792468678581 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmin=? [ F "MissionSuccessful" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=3,Objy1=1,Objx2=4,Objy2=2

Prob0E: 47 iterations in 0.44 seconds (average 0.009447, setup 0.00)

Prob1A: 3 iterations in 0.04 seconds (average 0.012000, setup 0.00)

yes = 949, no = 168400, maybe = 26224

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=195573, nc=45828, nnz=129694, k=2] [1.7 MB]
Creating vector for yes... [1.5 MB]
Allocating iteration vectors... [2 x 1.5 MB]
TOTAL: [6.2 MB]

Starting iterations...

Iterative method: 84 iterations in 0.25 seconds (average 0.001190, setup 0.15)

Value in the initial state: 0.7534888211478422

Time for model checking: 0.75 seconds.

Result: 0.7534888211478422 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmax=? [ F "MissionTimeUp" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=3,Objy1=1,Objx2=4,Objy2=2

Prob0A: 62 iterations in 0.44 seconds (average 0.007097, setup 0.00)

Prob1E: 4 iterations in 0.04 seconds (average 0.010000, setup 0.00)

yes = 376, no = 119202, maybe = 75995

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=195573, nc=129892, nnz=357664, k=4] [4.4 MB]
Creating vector for yes... [1.5 MB]
Allocating iteration vectors... [2 x 1.5 MB]
TOTAL: [8.9 MB]

Starting iterations...

Iterative method: 88 iterations in 0.38 seconds (average 0.001591, setup 0.24)

Value in the initial state: 7.829412574395923E-4

Time for model checking: 0.88 seconds.

Result: 7.829412574395923E-4 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmin=? [ F "MissionTimeUp" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=3,Objy1=1,Objx2=4,Objy2=2

Prob0E: 69 iterations in 0.62 seconds (average 0.008986, setup 0.00)

Prob1A: 3 iterations in 0.03 seconds (average 0.010667, setup 0.00)

yes = 376, no = 119540, maybe = 75657

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=195573, nc=129292, nnz=356394, k=4] [4.4 MB]
Creating vector for yes... [1.5 MB]
Allocating iteration vectors... [2 x 1.5 MB]
TOTAL: [8.9 MB]

Starting iterations...

Iterative method: 103 iterations in 0.38 seconds (average 0.001709, setup 0.21)

Value in the initial state: 2.14551139336301E-6

Time for model checking: 1.065 seconds.

Result: 2.14551139336301E-6 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmax=? [ F "Crash" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=3,Objy1=1,Objx2=4,Objy2=2

Prob0A: 3 iterations in 0.08 seconds (average 0.025333, setup 0.00)

Prob1E: 180 iterations in 2.36 seconds (average 0.013089, setup 0.00)

yes = 95849, no = 2574, maybe = 97150

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=195573, nc=160995, nnz=422095, k=4] [5.2 MB]
Creating vector for yes... [1.5 MB]
Allocating iteration vectors... [2 x 1.5 MB]
TOTAL: [9.6 MB]

Starting iterations...

Iterative method: 85 iterations in 0.42 seconds (average 0.001788, setup 0.27)

Value in the initial state: 0.03080895200865671

Time for model checking: 2.899 seconds.

Result: 0.03080895200865671 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmin=? [ F "Crash" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=3,Objy1=1,Objx2=4,Objy2=2

Prob0E: 3 iterations in 0.05 seconds (average 0.017333, setup 0.00)

Prob1A: 46 iterations in 0.41 seconds (average 0.008957, setup 0.00)

yes = 95849, no = 2574, maybe = 97150

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=195573, nc=160995, nnz=422095, k=4] [5.2 MB]
Creating vector for yes... [1.5 MB]
Allocating iteration vectors... [2 x 1.5 MB]
TOTAL: [9.6 MB]

Starting iterations...

Iterative method: 85 iterations in 0.37 seconds (average 0.001882, setup 0.21)

Value in the initial state: 0.017843082388471345

Time for model checking: 0.844 seconds.

Result: 0.017843082388471345 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmax=? [ F "AllAreaSearched" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=3,Objy1=1,Objx2=4,Objy2=2

Prob0A: 52 iterations in 0.31 seconds (average 0.005923, setup 0.00)

Prob1E: 4 iterations in 0.04 seconds (average 0.011000, setup 0.00)

yes = 1248, no = 150624, maybe = 43701

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=195573, nc=68776, nnz=182692, k=2] [2.3 MB]
Creating vector for yes... [1.5 MB]
Allocating iteration vectors... [2 x 1.5 MB]
TOTAL: [6.8 MB]

Starting iterations...

Iterative method: 85 iterations in 0.28 seconds (average 0.001412, setup 0.16)

Value in the initial state: 0.21869133910337843

Time for model checking: 0.652 seconds.

Result: 0.21869133910337843 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmin=? [ F "AllAreaSearched" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=3,Objy1=1,Objx2=4,Objy2=2

Prob0E: 62 iterations in 0.37 seconds (average 0.005935, setup 0.00)

Prob1A: 3 iterations in 0.03 seconds (average 0.010667, setup 0.00)

yes = 1248, no = 156260, maybe = 38065

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=195573, nc=59152, nnz=155572, k=2] [2.0 MB]
Creating vector for yes... [1.5 MB]
Allocating iteration vectors... [2 x 1.5 MB]
TOTAL: [6.5 MB]

Starting iterations...

Iterative method: 91 iterations in 0.23 seconds (average 0.001319, setup 0.11)

Value in the initial state: 0.001765417640334494

Time for model checking: 0.65 seconds.

Result: 0.001765417640334494 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmax=? [ F "MissionTimeUp"|"AllAreaSearched"|"Crash" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=3,Objy1=1,Objx2=4,Objy2=2

Prob0A: 3 iterations in 0.11 seconds (average 0.036000, setup 0.00)

Prob1E: 184 iterations in 2.36 seconds (average 0.012848, setup 0.00)

yes = 168399, no = 950, maybe = 26224

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=195573, nc=45828, nnz=129694, k=2] [1.7 MB]
Creating vector for yes... [1.5 MB]
Allocating iteration vectors... [2 x 1.5 MB]
TOTAL: [6.2 MB]

Starting iterations...

Iterative method: 78 iterations in 0.26 seconds (average 0.001231, setup 0.16)

Value in the initial state: 0.24651100955193095

Time for model checking: 2.72 seconds.

Result: 0.24651100955193095 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmin=? [ F "MissionTimeUp"|"AllAreaSearched"|"Crash" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=3,Objy1=1,Objx2=4,Objy2=2

Prob0E: 3 iterations in 0.04 seconds (average 0.013333, setup 0.00)

Prob1A: 47 iterations in 0.33 seconds (average 0.007064, setup 0.00)

yes = 163950, no = 950, maybe = 30673

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=195573, nc=53738, nnz=152585, k=2] [2.0 MB]
Creating vector for yes... [1.5 MB]
Allocating iteration vectors... [2 x 1.5 MB]
TOTAL: [6.5 MB]

Starting iterations...

Iterative method: 88 iterations in 0.24 seconds (average 0.001364, setup 0.12)

Value in the initial state: 0.019820274554433706

Time for model checking: 0.635 seconds.

Result: 0.019820274554433706 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"drop"}max=? [ F "deadlock" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=3,Objy1=1,Objx2=4,Objy2=2

Prob0E: 4 iterations in 0.05 seconds (average 0.013000, setup 0.00)

Prob1A: 1 iterations in 0.01 seconds (average 0.008000, setup 0.00)

goal = 1, inf = 0, maybe = 195572

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=195573, nc=259417, nnz=520517, k=4] [7.7 MB]
Building sparse matrix (transition rewards)... [n=195573, nc=259417, nnz=0, k=4] [1.7 MB]
Creating vector for state rewards... [1.5 MB]
Creating vector for inf... [1.5 MB]
Allocating iteration vectors... [2 x 1.5 MB]
TOTAL: [15.4 MB]

Starting iterations...

Iterative method: 86 iterations in 0.58 seconds (average 0.002837, setup 0.34)

Value in the initial state: 0.18200151721527524

Time for model checking: 0.656 seconds.

Result: 0.18200151721527524 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"drop"}min=? [ F "deadlock" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=3,Objy1=1,Objx2=4,Objy2=2

Prob0A: 4 iterations in 0.03 seconds (average 0.008000, setup 0.00)

Prob1E: 5 iterations in 0.04 seconds (average 0.008000, setup 0.00)

Warning: PRISM hasn't checked for zero-reward loops.
Your minimum rewards may be too low...

goal = 1, inf = 0, maybe = 195572

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=195573, nc=259417, nnz=520517, k=4] [7.7 MB]
Building sparse matrix (transition rewards)... [n=195573, nc=259417, nnz=0, k=4] [1.7 MB]
Creating vector for state rewards... [1.5 MB]
Creating vector for inf... [1.5 MB]
Allocating iteration vectors... [2 x 1.5 MB]
TOTAL: [15.4 MB]

Starting iterations...

Iterative method: 78 iterations in 0.49 seconds (average 0.002872, setup 0.26)

Value in the initial state: 0.15697147743348308

Time for model checking: 0.582 seconds.

Result: 0.15697147743348308 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"charge"}max=? [ F "deadlock" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=3,Objy1=1,Objx2=4,Objy2=2

Prob0E: 4 iterations in 0.02 seconds (average 0.006000, setup 0.00)

Prob1A: 1 iterations in 0.00 seconds (average 0.004000, setup 0.00)

goal = 1, inf = 0, maybe = 195572

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=195573, nc=259417, nnz=520517, k=4] [7.7 MB]
Building sparse matrix (transition rewards)... [n=195573, nc=259417, nnz=5786, k=4] [1.8 MB]
Creating vector for state rewards... [1.5 MB]
Creating vector for inf... [1.5 MB]
Allocating iteration vectors... [2 x 1.5 MB]
TOTAL: [15.5 MB]

Starting iterations...

Iterative method: 83 iterations in 0.52 seconds (average 0.002843, setup 0.28)

Value in the initial state: 0.27485628009201496

Time for model checking: 0.552 seconds.

Result: 0.27485628009201496 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"charge"}min=? [ F "deadlock" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=3,Objy1=1,Objx2=4,Objy2=2

Prob0A: 4 iterations in 0.02 seconds (average 0.006000, setup 0.00)

Prob1E: 5 iterations in 0.04 seconds (average 0.007200, setup 0.00)

Warning: PRISM hasn't checked for zero-reward loops.
Your minimum rewards may be too low...

goal = 1, inf = 0, maybe = 195572

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=195573, nc=259417, nnz=520517, k=4] [7.7 MB]
Building sparse matrix (transition rewards)... [n=195573, nc=259417, nnz=5786, k=4] [1.8 MB]
Creating vector for state rewards... [1.5 MB]
Creating vector for inf... [1.5 MB]
Allocating iteration vectors... [2 x 1.5 MB]
TOTAL: [15.5 MB]

Starting iterations...

Iterative method: 91 iterations in 0.54 seconds (average 0.002857, setup 0.28)

Value in the initial state: 0.05183138403290841

Time for model checking: 0.613 seconds.

Result: 0.05183138403290841 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"time"}max=? [ F "deadlock" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=3,Objy1=1,Objx2=4,Objy2=2

Prob0E: 4 iterations in 0.02 seconds (average 0.004000, setup 0.00)

Prob1A: 1 iterations in 0.00 seconds (average 0.004000, setup 0.00)

goal = 1, inf = 0, maybe = 195572

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=195573, nc=259417, nnz=520517, k=4] [7.7 MB]
Building sparse matrix (transition rewards)... [n=195573, nc=259417, nnz=98421, k=4] [2.9 MB]
Creating vector for state rewards... [1.5 MB]
Creating vector for inf... [1.5 MB]
Allocating iteration vectors... [2 x 1.5 MB]
TOTAL: [16.5 MB]

Starting iterations...

Iterative method: 88 iterations in 0.57 seconds (average 0.002909, setup 0.31)

Value in the initial state: 169.97313211657675

Time for model checking: 0.604 seconds.

Result: 169.97313211657675 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"time"}min=? [ F "deadlock" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=3,Objy1=1,Objx2=4,Objy2=2

Prob0A: 4 iterations in 0.02 seconds (average 0.005000, setup 0.00)

Prob1E: 5 iterations in 0.04 seconds (average 0.008000, setup 0.00)

Warning: PRISM hasn't checked for zero-reward loops.
Your minimum rewards may be too low...

goal = 1, inf = 0, maybe = 195572

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=195573, nc=259417, nnz=520517, k=4] [7.7 MB]
Building sparse matrix (transition rewards)... [n=195573, nc=259417, nnz=98421, k=4] [2.9 MB]
Creating vector for state rewards... [1.5 MB]
Creating vector for inf... [1.5 MB]
Allocating iteration vectors... [2 x 1.5 MB]
TOTAL: [16.5 MB]

Starting iterations...

Iterative method: 95 iterations in 0.59 seconds (average 0.002905, setup 0.31)

Value in the initial state: 98.8751027702152

Time for model checking: 0.658 seconds.

Result: 98.8751027702152 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"success"}max=? [ F "deadlock" ]/Pmin=? [ F "MissionSuccessful" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=3,Objy1=1,Objx2=4,Objy2=2

Prob0E: 4 iterations in 0.02 seconds (average 0.004000, setup 0.00)

Prob1A: 1 iterations in 0.00 seconds (average 0.004000, setup 0.00)

goal = 1, inf = 0, maybe = 195572

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=195573, nc=259417, nnz=520517, k=4] [7.7 MB]
Building sparse matrix (transition rewards)... [n=195573, nc=259417, nnz=1870, k=4] [1.8 MB]
Creating vector for state rewards... [1.5 MB]
Creating vector for inf... [1.5 MB]
Allocating iteration vectors... [2 x 1.5 MB]
TOTAL: [15.4 MB]

Starting iterations...

Iterative method: 91 iterations in 0.54 seconds (average 0.002857, setup 0.28)

Prob0E: 47 iterations in 0.22 seconds (average 0.004766, setup 0.00)

Prob1A: 3 iterations in 0.04 seconds (average 0.012000, setup 0.00)

yes = 949, no = 168400, maybe = 26224

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=195573, nc=45828, nnz=129694, k=2] [1.7 MB]
Creating vector for yes... [1.5 MB]
Allocating iteration vectors... [2 x 1.5 MB]
TOTAL: [6.2 MB]

Starting iterations...

Iterative method: 84 iterations in 0.22 seconds (average 0.001238, setup 0.12)

Value in the initial state: 221.00270503121004

Time for model checking: 1.086 seconds.

Result: 221.00270503121004 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"success"}min=? [ F "deadlock" ]/Pmax=? [ F "MissionSuccessful" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=3,Objy1=1,Objx2=4,Objy2=2

Prob0A: 4 iterations in 0.03 seconds (average 0.007000, setup 0.00)

Prob1E: 5 iterations in 0.04 seconds (average 0.008000, setup 0.00)

Warning: PRISM hasn't checked for zero-reward loops.
Your minimum rewards may be too low...

goal = 1, inf = 0, maybe = 195572

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=195573, nc=259417, nnz=520517, k=4] [7.7 MB]
Building sparse matrix (transition rewards)... [n=195573, nc=259417, nnz=1870, k=4] [1.8 MB]
Creating vector for state rewards... [1.5 MB]
Creating vector for inf... [1.5 MB]
Allocating iteration vectors... [2 x 1.5 MB]
TOTAL: [15.4 MB]

Starting iterations...

Iterative method: 88 iterations in 0.53 seconds (average 0.002864, setup 0.28)

Prob0A: 47 iterations in 0.24 seconds (average 0.005191, setup 0.00)

Prob1E: 4 iterations in 0.03 seconds (average 0.007000, setup 0.00)

yes = 949, no = 163951, maybe = 30673

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=195573, nc=53738, nnz=152585, k=2] [2.0 MB]
Creating vector for yes... [1.5 MB]
Allocating iteration vectors... [2 x 1.5 MB]
TOTAL: [6.5 MB]

Starting iterations...

Iterative method: 90 iterations in 0.24 seconds (average 0.001289, setup 0.12)

Value in the initial state: 79.22046322761643

Time for model checking: 1.142 seconds.

Result: 79.22046322761643 (value in the initial state)

---------------------------------------------------------------------

Note: There were 16 warnings during computation.

