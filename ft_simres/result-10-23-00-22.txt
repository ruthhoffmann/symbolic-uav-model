PRISM
=====

Version: 4.3
Date: Tue Aug 02 12:34:39 BST 2016
Hostname: eglinton
Memory limits: cudd=1g, java(heap)=981.5m
Command line: prism uav_sim_ft_mdp.prism -s uav_mdp.props -const 'Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=0,Objy1=0,Objx2=2,Objy2=2,'

Parsing model file "uav_sim_ft_mdp.prism"...

Parsing properties file "uav_mdp.props"...

18 properties:
(1) Pmax=? [ F "MissionSuccessful" ]
(2) Pmin=? [ F "MissionSuccessful" ]
(3) Pmax=? [ F "MissionTimeUp" ]
(4) Pmin=? [ F "MissionTimeUp" ]
(5) Pmax=? [ F "Crash" ]
(6) Pmin=? [ F "Crash" ]
(7) Pmax=? [ F "AllAreaSearched" ]
(8) Pmin=? [ F "AllAreaSearched" ]
(9) Pmax=? [ F "MissionTimeUp"|"AllAreaSearched"|"Crash" ]
(10) Pmin=? [ F "MissionTimeUp"|"AllAreaSearched"|"Crash" ]
(11) R{"drop"}max=? [ F "deadlock" ]
(12) R{"drop"}min=? [ F "deadlock" ]
(13) R{"charge"}max=? [ F "deadlock" ]
(14) R{"charge"}min=? [ F "deadlock" ]
(15) R{"time"}max=? [ F "deadlock" ]
(16) R{"time"}min=? [ F "deadlock" ]
(17) R{"success"}max=? [ F "deadlock" ]/Pmin=? [ F "MissionSuccessful" ]
(18) R{"success"}min=? [ F "deadlock" ]/Pmax=? [ F "MissionSuccessful" ]

Type:        MDP
Modules:     Obj Move Robot T 
Variables:   NoOfObjs posx posy s1 retx rety b t c 

---------------------------------------------------------------------

Model checking: Pmax=? [ F "MissionSuccessful" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=0,Objy1=0,Objx2=2,Objy2=2

Building model...
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=0,Objy1=0,Objx2=2,Objy2=2

Warning: Guard for command 7 of module "Move" is never satisfied.

Warning: Guard for command 8 of module "Move" is never satisfied.

Warning: Guard for command 9 of module "Move" is never satisfied.

Warning: Guard for command 7 of module "Robot" is never satisfied.

Warning: Update 2 of command 3 of module "Robot" doesn't do anything ("(s1'=13)", line 249, column 44)

Warning: Update 2 of command 27 of module "T" doesn't do anything ("(c'=1)", line 372, column 108)

Warning: Guard for command 28 of module "T" is never satisfied.

Warning: Update 2 of command 9 of module "T" doesn't do anything ("(c'=1)", line 349, column 112)

Warning: Guard for command 10 of module "T" is never satisfied.

Warning: Update 2 of command 51 of module "T" doesn't do anything ("(c'=1)", line 403, column 113)

Warning: Guard for command 52 of module "T" is never satisfied.

Computing reachable states...

Reachability (BFS): 74 iterations in 0.90 seconds (average 0.012162, setup 0.00)

Time for model construction: 3.356 seconds.

Warning: Deadlocks detected and fixed in 1 states

Type:        MDP
States:      214167 (1 initial)
Transitions: 537507
Choices:     275999

Transition matrix: 171791 nodes (76 terminal), 537507 minterms, vars: 34r/34c/18nd

Prob0A: 54 iterations in 0.37 seconds (average 0.006889, setup 0.00)

Prob1E: 4 iterations in 0.05 seconds (average 0.012000, setup 0.00)

yes = 958, no = 183518, maybe = 29691

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=214167, nc=50084, nnz=138746, k=2] [1.8 MB]
Creating vector for yes... [1.6 MB]
Allocating iteration vectors... [2 x 1.6 MB]
TOTAL: [6.7 MB]

Starting iterations...

Iterative method: 89 iterations in 0.34 seconds (average 0.001438, setup 0.22)

Value in the initial state: 0.9807395566354086

Time for model checking: 0.971 seconds.

Result: 0.9807395566354086 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmin=? [ F "MissionSuccessful" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=0,Objy1=0,Objx2=2,Objy2=2

Prob0E: 56 iterations in 0.42 seconds (average 0.007571, setup 0.00)

Prob1A: 3 iterations in 0.05 seconds (average 0.017333, setup 0.00)

yes = 958, no = 188417, maybe = 24792

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=214167, nc=41864, nnz=114870, k=2] [1.6 MB]
Creating vector for yes... [1.6 MB]
Allocating iteration vectors... [2 x 1.6 MB]
TOTAL: [6.5 MB]

Starting iterations...

Iterative method: 85 iterations in 0.30 seconds (average 0.001365, setup 0.18)

Value in the initial state: 0.7580080428074316

Time for model checking: 1.035 seconds.

Result: 0.7580080428074316 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmax=? [ F "MissionTimeUp" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=0,Objy1=0,Objx2=2,Objy2=2

Prob0A: 60 iterations in 0.61 seconds (average 0.010200, setup 0.00)

Prob1E: 4 iterations in 0.04 seconds (average 0.011000, setup 0.00)

yes = 468, no = 130321, maybe = 83378

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=214167, nc=135382, nnz=361960, k=4] [4.5 MB]
Creating vector for yes... [1.6 MB]
Allocating iteration vectors... [2 x 1.6 MB]
TOTAL: [9.4 MB]

Starting iterations...

Iterative method: 90 iterations in 0.46 seconds (average 0.001867, setup 0.29)

Value in the initial state: 4.5576538115981086E-4

Time for model checking: 1.622 seconds.

Result: 4.5576538115981086E-4 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmin=? [ F "MissionTimeUp" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=0,Objy1=0,Objx2=2,Objy2=2

Prob0E: 66 iterations in 0.73 seconds (average 0.011091, setup 0.00)

Prob1A: 3 iterations in 0.06 seconds (average 0.021333, setup 0.00)

yes = 468, no = 130621, maybe = 83078

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=214167, nc=134858, nnz=360768, k=4] [4.5 MB]
Creating vector for yes... [1.6 MB]
Allocating iteration vectors... [2 x 1.6 MB]
TOTAL: [9.4 MB]

Starting iterations...

Iterative method: 103 iterations in 0.48 seconds (average 0.001942, setup 0.28)

Value in the initial state: 2.0527553400934365E-6

Time for model checking: 2.076 seconds.

Result: 2.0527553400934365E-6 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmax=? [ F "Crash" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=0,Objy1=0,Objx2=2,Objy2=2

Prob0A: 3 iterations in 0.08 seconds (average 0.026667, setup 0.00)

Prob1E: 202 iterations in 2.67 seconds (average 0.013208, setup 0.00)

yes = 105155, no = 2661, maybe = 106351

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=214167, nc=168183, nnz=429691, k=4] [5.3 MB]
Creating vector for yes... [1.6 MB]
Allocating iteration vectors... [2 x 1.6 MB]
TOTAL: [10.2 MB]

Starting iterations...

Iterative method: 86 iterations in 0.48 seconds (average 0.001953, setup 0.31)

Value in the initial state: 0.030469556795022272

Time for model checking: 4.441 seconds.

Result: 0.030469556795022272 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmin=? [ F "Crash" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=0,Objy1=0,Objx2=2,Objy2=2

Prob0E: 3 iterations in 0.06 seconds (average 0.018667, setup 0.00)

Prob1A: 51 iterations in 0.58 seconds (average 0.011451, setup 0.00)

yes = 105155, no = 2661, maybe = 106351

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=214167, nc=168183, nnz=429691, k=4] [5.3 MB]
Creating vector for yes... [1.6 MB]
Allocating iteration vectors... [2 x 1.6 MB]
TOTAL: [10.2 MB]

Starting iterations...

Iterative method: 84 iterations in 0.42 seconds (average 0.002095, setup 0.24)

Value in the initial state: 0.017273570001844728

Time for model checking: 1.456 seconds.

Result: 0.017273570001844728 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmax=? [ F "AllAreaSearched" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=0,Objy1=0,Objx2=2,Objy2=2

Prob0A: 62 iterations in 0.50 seconds (average 0.008000, setup 0.00)

Prob1E: 4 iterations in 0.04 seconds (average 0.010000, setup 0.00)

yes = 1234, no = 172187, maybe = 40746

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=214167, nc=60786, nnz=153346, k=2] [2.0 MB]
Creating vector for yes... [1.6 MB]
Allocating iteration vectors... [2 x 1.6 MB]
TOTAL: [6.9 MB]

Starting iterations...

Iterative method: 84 iterations in 0.30 seconds (average 0.001524, setup 0.17)

Value in the initial state: 0.21430481092065864

Time for model checking: 1.15 seconds.

Result: 0.21430481092065864 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmin=? [ F "AllAreaSearched" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=0,Objy1=0,Objx2=2,Objy2=2

Prob0E: 65 iterations in 0.40 seconds (average 0.006215, setup 0.00)

Prob1A: 3 iterations in 0.03 seconds (average 0.009333, setup 0.00)

yes = 1234, no = 177284, maybe = 35649

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=214167, nc=52791, nnz=131838, k=2] [1.8 MB]
Creating vector for yes... [1.6 MB]
Allocating iteration vectors... [2 x 1.6 MB]
TOTAL: [6.7 MB]

Starting iterations...

Iterative method: 91 iterations in 0.25 seconds (average 0.001495, setup 0.11)

Value in the initial state: 0.0017605418970622036

Time for model checking: 0.749 seconds.

Result: 0.0017605418970622036 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmax=? [ F "MissionTimeUp"|"AllAreaSearched"|"Crash" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=0,Objy1=0,Objx2=2,Objy2=2

Prob0A: 3 iterations in 0.08 seconds (average 0.026667, setup 0.00)

Prob1E: 220 iterations in 2.70 seconds (average 0.012273, setup 0.00)

yes = 188416, no = 959, maybe = 24792

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=214167, nc=41864, nnz=114870, k=2] [1.6 MB]
Creating vector for yes... [1.6 MB]
Allocating iteration vectors... [2 x 1.6 MB]
TOTAL: [6.5 MB]

Starting iterations...

Iterative method: 78 iterations in 0.30 seconds (average 0.001282, setup 0.20)

Value in the initial state: 0.24199157706357466

Time for model checking: 3.658 seconds.

Result: 0.24199157706357466 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmin=? [ F "MissionTimeUp"|"AllAreaSearched"|"Crash" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=0,Objy1=0,Objx2=2,Objy2=2

Prob0E: 3 iterations in 0.05 seconds (average 0.017333, setup 0.00)

Prob1A: 54 iterations in 0.58 seconds (average 0.010667, setup 0.00)

yes = 183517, no = 959, maybe = 29691

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=214167, nc=50084, nnz=138746, k=2] [1.8 MB]
Creating vector for yes... [1.6 MB]
Allocating iteration vectors... [2 x 1.6 MB]
TOTAL: [6.7 MB]

Starting iterations...

Iterative method: 88 iterations in 0.29 seconds (average 0.001455, setup 0.16)

Value in the initial state: 0.019259483336376396

Time for model checking: 1.166 seconds.

Result: 0.019259483336376396 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"drop"}max=? [ F "deadlock" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=0,Objy1=0,Objx2=2,Objy2=2

Prob0E: 4 iterations in 0.08 seconds (average 0.019000, setup 0.00)

Prob1A: 1 iterations in 0.01 seconds (average 0.012000, setup 0.00)

goal = 1, inf = 0, maybe = 214166

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=214167, nc=275998, nnz=537506, k=4] [8.0 MB]
Building sparse matrix (transition rewards)... [n=214167, nc=275998, nnz=0, k=4] [1.9 MB]
Creating vector for state rewards... [1.6 MB]
Creating vector for inf... [1.6 MB]
Allocating iteration vectors... [2 x 1.6 MB]
TOTAL: [16.4 MB]

Starting iterations...

Iterative method: 86 iterations in 0.64 seconds (average 0.003070, setup 0.37)

Value in the initial state: 0.17641809945278

Time for model checking: 0.772 seconds.

Result: 0.17641809945278 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"drop"}min=? [ F "deadlock" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=0,Objy1=0,Objx2=2,Objy2=2

Prob0A: 4 iterations in 0.02 seconds (average 0.006000, setup 0.00)

Prob1E: 5 iterations in 0.04 seconds (average 0.008000, setup 0.00)

Warning: PRISM hasn't checked for zero-reward loops.
Your minimum rewards may be too low...

goal = 1, inf = 0, maybe = 214166

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=214167, nc=275998, nnz=537506, k=4] [8.0 MB]
Building sparse matrix (transition rewards)... [n=214167, nc=275998, nnz=0, k=4] [1.9 MB]
Creating vector for state rewards... [1.6 MB]
Creating vector for inf... [1.6 MB]
Allocating iteration vectors... [2 x 1.6 MB]
TOTAL: [16.4 MB]

Starting iterations...

Iterative method: 76 iterations in 0.53 seconds (average 0.003105, setup 0.30)

Value in the initial state: 0.1631088040989524

Time for model checking: 0.625 seconds.

Result: 0.1631088040989524 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"charge"}max=? [ F "deadlock" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=0,Objy1=0,Objx2=2,Objy2=2

Prob0E: 4 iterations in 0.03 seconds (average 0.007000, setup 0.00)

Prob1A: 1 iterations in 0.00 seconds (average 0.004000, setup 0.00)

goal = 1, inf = 0, maybe = 214166

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=214167, nc=275998, nnz=537506, k=4] [8.0 MB]
Building sparse matrix (transition rewards)... [n=214167, nc=275998, nnz=5402, k=4] [1.9 MB]
Creating vector for state rewards... [1.6 MB]
Creating vector for inf... [1.6 MB]
Allocating iteration vectors... [2 x 1.6 MB]
TOTAL: [16.5 MB]

Starting iterations...

Iterative method: 86 iterations in 0.56 seconds (average 0.003116, setup 0.30)

Value in the initial state: 0.2721818844590741

Time for model checking: 0.663 seconds.

Result: 0.2721818844590741 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"charge"}min=? [ F "deadlock" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=0,Objy1=0,Objx2=2,Objy2=2

Prob0A: 4 iterations in 0.02 seconds (average 0.006000, setup 0.00)

Prob1E: 5 iterations in 0.04 seconds (average 0.008800, setup 0.00)

Warning: PRISM hasn't checked for zero-reward loops.
Your minimum rewards may be too low...

goal = 1, inf = 0, maybe = 214166

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=214167, nc=275998, nnz=537506, k=4] [8.0 MB]
Building sparse matrix (transition rewards)... [n=214167, nc=275998, nnz=5402, k=4] [1.9 MB]
Creating vector for state rewards... [1.6 MB]
Creating vector for inf... [1.6 MB]
Allocating iteration vectors... [2 x 1.6 MB]
TOTAL: [16.5 MB]

Starting iterations...

Iterative method: 89 iterations in 0.58 seconds (average 0.003146, setup 0.30)

Value in the initial state: 0.04142362533542691

Time for model checking: 1.092 seconds.

Result: 0.04142362533542691 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"time"}max=? [ F "deadlock" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=0,Objy1=0,Objx2=2,Objy2=2

Prob0E: 4 iterations in 0.02 seconds (average 0.006000, setup 0.00)

Prob1A: 1 iterations in 0.00 seconds (average 0.004000, setup 0.00)

goal = 1, inf = 0, maybe = 214166

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=214167, nc=275998, nnz=537506, k=4] [8.0 MB]
Building sparse matrix (transition rewards)... [n=214167, nc=275998, nnz=107814, k=4] [3.1 MB]
Creating vector for state rewards... [1.6 MB]
Creating vector for inf... [1.6 MB]
Allocating iteration vectors... [2 x 1.6 MB]
TOTAL: [17.7 MB]

Starting iterations...

Iterative method: 89 iterations in 0.62 seconds (average 0.003146, setup 0.34)

Value in the initial state: 168.13157101333155

Time for model checking: 1.022 seconds.

Result: 168.13157101333155 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"time"}min=? [ F "deadlock" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=0,Objy1=0,Objx2=2,Objy2=2

Prob0A: 4 iterations in 0.03 seconds (average 0.007000, setup 0.00)

Prob1E: 5 iterations in 0.04 seconds (average 0.008800, setup 0.00)

Warning: PRISM hasn't checked for zero-reward loops.
Your minimum rewards may be too low...

goal = 1, inf = 0, maybe = 214166

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=214167, nc=275998, nnz=537506, k=4] [8.0 MB]
Building sparse matrix (transition rewards)... [n=214167, nc=275998, nnz=107814, k=4] [3.1 MB]
Creating vector for state rewards... [1.6 MB]
Creating vector for inf... [1.6 MB]
Allocating iteration vectors... [2 x 1.6 MB]
TOTAL: [17.7 MB]

Starting iterations...

Iterative method: 96 iterations in 0.64 seconds (average 0.003208, setup 0.33)

Value in the initial state: 95.73163749473912

Time for model checking: 0.866 seconds.

Result: 95.73163749473912 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"success"}max=? [ F "deadlock" ]/Pmin=? [ F "MissionSuccessful" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=0,Objy1=0,Objx2=2,Objy2=2

Prob0E: 4 iterations in 0.02 seconds (average 0.006000, setup 0.00)

Prob1A: 1 iterations in 0.00 seconds (average 0.004000, setup 0.00)

goal = 1, inf = 0, maybe = 214166

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=214167, nc=275998, nnz=537506, k=4] [8.0 MB]
Building sparse matrix (transition rewards)... [n=214167, nc=275998, nnz=1888, k=4] [1.9 MB]
Creating vector for state rewards... [1.6 MB]
Creating vector for inf... [1.6 MB]
Allocating iteration vectors... [2 x 1.6 MB]
TOTAL: [16.4 MB]

Starting iterations...

Iterative method: 91 iterations in 0.58 seconds (average 0.003077, setup 0.30)

Prob0E: 56 iterations in 0.37 seconds (average 0.006571, setup 0.00)

Prob1A: 3 iterations in 0.04 seconds (average 0.013333, setup 0.00)

yes = 958, no = 188417, maybe = 24792

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=214167, nc=41864, nnz=114870, k=2] [1.6 MB]
Creating vector for yes... [1.6 MB]
Allocating iteration vectors... [2 x 1.6 MB]
TOTAL: [6.5 MB]

Starting iterations...

Iterative method: 85 iterations in 0.28 seconds (average 0.001365, setup 0.16)

Value in the initial state: 217.4419917240092

Time for model checking: 1.806 seconds.

Result: 217.4419917240092 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"success"}min=? [ F "deadlock" ]/Pmax=? [ F "MissionSuccessful" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=0,Objy1=0,Objx2=2,Objy2=2

Prob0A: 4 iterations in 0.06 seconds (average 0.014000, setup 0.00)

Prob1E: 5 iterations in 0.04 seconds (average 0.007200, setup 0.00)

Warning: PRISM hasn't checked for zero-reward loops.
Your minimum rewards may be too low...

goal = 1, inf = 0, maybe = 214166

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=214167, nc=275998, nnz=537506, k=4] [8.0 MB]
Building sparse matrix (transition rewards)... [n=214167, nc=275998, nnz=1888, k=4] [1.9 MB]
Creating vector for state rewards... [1.6 MB]
Creating vector for inf... [1.6 MB]
Allocating iteration vectors... [2 x 1.6 MB]
TOTAL: [16.4 MB]

Starting iterations...

Iterative method: 88 iterations in 0.59 seconds (average 0.003091, setup 0.32)

Prob0A: 54 iterations in 0.34 seconds (average 0.006296, setup 0.00)

Prob1E: 4 iterations in 0.04 seconds (average 0.009000, setup 0.00)

yes = 958, no = 183518, maybe = 29691

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=214167, nc=50084, nnz=138746, k=2] [1.8 MB]
Creating vector for yes... [1.6 MB]
Allocating iteration vectors... [2 x 1.6 MB]
TOTAL: [6.7 MB]

Starting iterations...

Iterative method: 89 iterations in 0.27 seconds (average 0.001438, setup 0.14)

Value in the initial state: 79.97508646280785

Time for model checking: 1.407 seconds.

Result: 79.97508646280785 (value in the initial state)

---------------------------------------------------------------------

Note: There were 16 warnings during computation.

