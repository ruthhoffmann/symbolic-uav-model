PRISM
=====

Version: 4.3
Date: Tue Aug 02 12:38:41 BST 2016
Hostname: eglinton
Memory limits: cudd=1g, java(heap)=981.5m
Command line: prism uav_sim_ft_mdp.prism -s uav_mdp.props -const 'Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=2,Objy1=1,Objx2=6,Objy2=2,'

Parsing model file "uav_sim_ft_mdp.prism"...

Parsing properties file "uav_mdp.props"...

18 properties:
(1) Pmax=? [ F "MissionSuccessful" ]
(2) Pmin=? [ F "MissionSuccessful" ]
(3) Pmax=? [ F "MissionTimeUp" ]
(4) Pmin=? [ F "MissionTimeUp" ]
(5) Pmax=? [ F "Crash" ]
(6) Pmin=? [ F "Crash" ]
(7) Pmax=? [ F "AllAreaSearched" ]
(8) Pmin=? [ F "AllAreaSearched" ]
(9) Pmax=? [ F "MissionTimeUp"|"AllAreaSearched"|"Crash" ]
(10) Pmin=? [ F "MissionTimeUp"|"AllAreaSearched"|"Crash" ]
(11) R{"drop"}max=? [ F "deadlock" ]
(12) R{"drop"}min=? [ F "deadlock" ]
(13) R{"charge"}max=? [ F "deadlock" ]
(14) R{"charge"}min=? [ F "deadlock" ]
(15) R{"time"}max=? [ F "deadlock" ]
(16) R{"time"}min=? [ F "deadlock" ]
(17) R{"success"}max=? [ F "deadlock" ]/Pmin=? [ F "MissionSuccessful" ]
(18) R{"success"}min=? [ F "deadlock" ]/Pmax=? [ F "MissionSuccessful" ]

Type:        MDP
Modules:     Obj Move Robot T 
Variables:   NoOfObjs posx posy s1 retx rety b t c 

---------------------------------------------------------------------

Model checking: Pmax=? [ F "MissionSuccessful" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=2,Objy1=1,Objx2=6,Objy2=2

Building model...
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=2,Objy1=1,Objx2=6,Objy2=2

Warning: Guard for command 7 of module "Move" is never satisfied.

Warning: Guard for command 8 of module "Move" is never satisfied.

Warning: Guard for command 9 of module "Move" is never satisfied.

Warning: Guard for command 7 of module "Robot" is never satisfied.

Warning: Update 2 of command 3 of module "Robot" doesn't do anything ("(s1'=13)", line 249, column 44)

Warning: Update 2 of command 27 of module "T" doesn't do anything ("(c'=1)", line 372, column 108)

Warning: Guard for command 28 of module "T" is never satisfied.

Warning: Update 2 of command 9 of module "T" doesn't do anything ("(c'=1)", line 349, column 112)

Warning: Guard for command 10 of module "T" is never satisfied.

Warning: Update 2 of command 51 of module "T" doesn't do anything ("(c'=1)", line 403, column 113)

Warning: Guard for command 52 of module "T" is never satisfied.

Computing reachable states...

Reachability (BFS): 78 iterations in 0.86 seconds (average 0.011077, setup 0.00)

Time for model construction: 2.074 seconds.

Warning: Deadlocks detected and fixed in 1 states

Type:        MDP
States:      232935 (1 initial)
Transitions: 647972
Choices:     313808

Transition matrix: 173059 nodes (76 terminal), 647972 minterms, vars: 34r/34c/18nd

Prob0A: 51 iterations in 0.46 seconds (average 0.009020, setup 0.00)

Prob1E: 4 iterations in 0.06 seconds (average 0.014000, setup 0.00)

yes = 925, no = 195325, maybe = 36685

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=232935, nc=64550, nnz=187431, k=2] [2.4 MB]
Creating vector for yes... [1.8 MB]
Allocating iteration vectors... [2 x 1.8 MB]
TOTAL: [7.8 MB]

Starting iterations...

Iterative method: 98 iterations in 0.38 seconds (average 0.001592, setup 0.22)

Value in the initial state: 0.9780260793662152

Time for model checking: 0.927 seconds.

Result: 0.9780260793662152 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmin=? [ F "MissionSuccessful" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=2,Objy1=1,Objx2=6,Objy2=2

Prob0E: 65 iterations in 0.52 seconds (average 0.008000, setup 0.00)

Prob1A: 3 iterations in 0.04 seconds (average 0.013333, setup 0.00)

yes = 925, no = 200285, maybe = 31725

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=232935, nc=55659, nnz=161580, k=2] [2.1 MB]
Creating vector for yes... [1.8 MB]
Allocating iteration vectors... [2 x 1.8 MB]
TOTAL: [7.5 MB]

Starting iterations...

Iterative method: 91 iterations in 0.30 seconds (average 0.001538, setup 0.16)

Value in the initial state: 0.7251114940896854

Time for model checking: 0.886 seconds.

Result: 0.7251114940896854 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmax=? [ F "MissionTimeUp" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=2,Objy1=1,Objx2=6,Objy2=2

Prob0A: 63 iterations in 0.50 seconds (average 0.007937, setup 0.00)

Prob1E: 4 iterations in 0.04 seconds (average 0.009000, setup 0.00)

yes = 420, no = 135067, maybe = 97448

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=232935, nc=168377, nnz=471404, k=4] [5.8 MB]
Creating vector for yes... [1.8 MB]
Allocating iteration vectors... [2 x 1.8 MB]
TOTAL: [11.1 MB]

Starting iterations...

Iterative method: 93 iterations in 0.50 seconds (average 0.002194, setup 0.29)

Value in the initial state: 0.003001465767743045

Time for model checking: 1.039 seconds.

Result: 0.003001465767743045 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmin=? [ F "MissionTimeUp" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=2,Objy1=1,Objx2=6,Objy2=2

Prob0E: 70 iterations in 0.71 seconds (average 0.010114, setup 0.00)

Prob1A: 3 iterations in 0.04 seconds (average 0.014667, setup 0.00)

yes = 420, no = 135481, maybe = 97034

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=232935, nc=167625, nnz=469978, k=4] [5.8 MB]
Creating vector for yes... [1.8 MB]
Allocating iteration vectors... [2 x 1.8 MB]
TOTAL: [11.1 MB]

Starting iterations...

Iterative method: 110 iterations in 0.50 seconds (average 0.002109, setup 0.27)

Value in the initial state: 1.3941801880624243E-5

Time for model checking: 1.282 seconds.

Result: 1.3941801880624243E-5 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmax=? [ F "Crash" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=2,Objy1=1,Objx2=6,Objy2=2

Prob0A: 3 iterations in 0.07 seconds (average 0.024000, setup 0.00)

Prob1E: 244 iterations in 2.78 seconds (average 0.011393, setup 0.00)

yes = 113934, no = 2574, maybe = 116427

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=232935, nc=197300, nnz=531464, k=4] [6.5 MB]
Creating vector for yes... [1.8 MB]
Allocating iteration vectors... [2 x 1.8 MB]
TOTAL: [11.8 MB]

Starting iterations...

Iterative method: 91 iterations in 0.54 seconds (average 0.002242, setup 0.34)

Value in the initial state: 0.06263324829603598

Time for model checking: 3.434 seconds.

Result: 0.06263324829603598 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmin=? [ F "Crash" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=2,Objy1=1,Objx2=6,Objy2=2

Prob0E: 3 iterations in 0.06 seconds (average 0.018667, setup 0.00)

Prob1A: 46 iterations in 0.51 seconds (average 0.011130, setup 0.00)

yes = 113934, no = 2574, maybe = 116427

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=232935, nc=197300, nnz=531464, k=4] [6.5 MB]
Creating vector for yes... [1.8 MB]
Allocating iteration vectors... [2 x 1.8 MB]
TOTAL: [11.8 MB]

Starting iterations...

Iterative method: 97 iterations in 0.48 seconds (average 0.002351, setup 0.26)

Value in the initial state: 0.019945516170991884

Time for model checking: 1.075 seconds.

Result: 0.019945516170991884 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmax=? [ F "AllAreaSearched" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=2,Objy1=1,Objx2=6,Objy2=2

Prob0A: 53 iterations in 0.50 seconds (average 0.009509, setup 0.00)

Prob1E: 4 iterations in 0.04 seconds (average 0.011000, setup 0.00)

yes = 1228, no = 178960, maybe = 52747

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=232935, nc=86607, nnz=241508, k=2] [3.1 MB]
Creating vector for yes... [1.8 MB]
Allocating iteration vectors... [2 x 1.8 MB]
TOTAL: [8.4 MB]

Starting iterations...

Iterative method: 90 iterations in 0.37 seconds (average 0.001778, setup 0.21)

Value in the initial state: 0.24536217563870952

Time for model checking: 0.9 seconds.

Result: 0.24536217563870952 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmin=? [ F "AllAreaSearched" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=2,Objy1=1,Objx2=6,Objy2=2

Prob0E: 64 iterations in 0.46 seconds (average 0.007125, setup 0.00)

Prob1A: 3 iterations in 0.05 seconds (average 0.017333, setup 0.00)

yes = 1228, no = 186210, maybe = 45497

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=232935, nc=74005, nnz=205396, k=2] [2.6 MB]
Creating vector for yes... [1.8 MB]
Allocating iteration vectors... [2 x 1.8 MB]
TOTAL: [8.0 MB]

Starting iterations...

Iterative method: 91 iterations in 0.30 seconds (average 0.001670, setup 0.15)

Value in the initial state: 0.00127290943470788

Time for model checking: 0.819 seconds.

Result: 0.00127290943470788 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmax=? [ F "MissionTimeUp"|"AllAreaSearched"|"Crash" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=2,Objy1=1,Objx2=6,Objy2=2

Prob0A: 3 iterations in 0.07 seconds (average 0.022667, setup 0.00)

Prob1E: 252 iterations in 2.76 seconds (average 0.010937, setup 0.00)

yes = 200284, no = 926, maybe = 31725

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=232935, nc=55659, nnz=161580, k=2] [2.1 MB]
Creating vector for yes... [1.8 MB]
Allocating iteration vectors... [2 x 1.8 MB]
TOTAL: [7.5 MB]

Starting iterations...

Iterative method: 84 iterations in 0.31 seconds (average 0.001667, setup 0.17)

Value in the initial state: 0.27488790481088066

Time for model checking: 3.162 seconds.

Result: 0.27488790481088066 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmin=? [ F "MissionTimeUp"|"AllAreaSearched"|"Crash" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=2,Objy1=1,Objx2=6,Objy2=2

Prob0E: 3 iterations in 0.05 seconds (average 0.016000, setup 0.00)

Prob1A: 51 iterations in 0.42 seconds (average 0.008157, setup 0.00)

yes = 195324, no = 926, maybe = 36685

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=232935, nc=64550, nnz=187431, k=2] [2.4 MB]
Creating vector for yes... [1.8 MB]
Allocating iteration vectors... [2 x 1.8 MB]
TOTAL: [7.8 MB]

Starting iterations...

Iterative method: 94 iterations in 0.32 seconds (average 0.001574, setup 0.17)

Value in the initial state: 0.021973574547224074

Time for model checking: 0.824 seconds.

Result: 0.021973574547224074 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"drop"}max=? [ F "deadlock" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=2,Objy1=1,Objx2=6,Objy2=2

Prob0E: 4 iterations in 0.05 seconds (average 0.013000, setup 0.00)

Prob1A: 1 iterations in 0.00 seconds (average 0.004000, setup 0.00)

goal = 1, inf = 0, maybe = 232934

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=232935, nc=313807, nnz=647971, k=4] [9.5 MB]
Building sparse matrix (transition rewards)... [n=232935, nc=313807, nnz=0, k=4] [2.1 MB]
Creating vector for state rewards... [1.8 MB]
Creating vector for inf... [1.8 MB]
Allocating iteration vectors... [2 x 1.8 MB]
TOTAL: [18.7 MB]

Starting iterations...

Iterative method: 93 iterations in 0.72 seconds (average 0.003441, setup 0.40)

Value in the initial state: 0.3255283658259003

Time for model checking: 0.804 seconds.

Result: 0.3255283658259003 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"drop"}min=? [ F "deadlock" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=2,Objy1=1,Objx2=6,Objy2=2

Prob0A: 4 iterations in 0.04 seconds (average 0.009000, setup 0.00)

Prob1E: 5 iterations in 0.04 seconds (average 0.008000, setup 0.00)

Warning: PRISM hasn't checked for zero-reward loops.
Your minimum rewards may be too low...

goal = 1, inf = 0, maybe = 232934

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=232935, nc=313807, nnz=647971, k=4] [9.5 MB]
Building sparse matrix (transition rewards)... [n=232935, nc=313807, nnz=0, k=4] [2.1 MB]
Creating vector for state rewards... [1.8 MB]
Creating vector for inf... [1.8 MB]
Allocating iteration vectors... [2 x 1.8 MB]
TOTAL: [18.7 MB]

Starting iterations...

Iterative method: 84 iterations in 0.62 seconds (average 0.003429, setup 0.33)

Value in the initial state: 0.19873303318049837

Time for model checking: 0.714 seconds.

Result: 0.19873303318049837 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"charge"}max=? [ F "deadlock" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=2,Objy1=1,Objx2=6,Objy2=2

Prob0E: 4 iterations in 0.02 seconds (average 0.005000, setup 0.00)

Prob1A: 1 iterations in 0.01 seconds (average 0.008000, setup 0.00)

goal = 1, inf = 0, maybe = 232934

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=232935, nc=313807, nnz=647971, k=4] [9.5 MB]
Building sparse matrix (transition rewards)... [n=232935, nc=313807, nnz=7898, k=4] [2.2 MB]
Creating vector for state rewards... [1.8 MB]
Creating vector for inf... [1.8 MB]
Allocating iteration vectors... [2 x 1.8 MB]
TOTAL: [18.8 MB]

Starting iterations...

Iterative method: 90 iterations in 0.64 seconds (average 0.003422, setup 0.34)

Value in the initial state: 0.9861506916648305

Time for model checking: 0.685 seconds.

Result: 0.9861506916648305 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"charge"}min=? [ F "deadlock" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=2,Objy1=1,Objx2=6,Objy2=2

Prob0A: 4 iterations in 0.02 seconds (average 0.006000, setup 0.00)

Prob1E: 5 iterations in 0.04 seconds (average 0.008000, setup 0.00)

Warning: PRISM hasn't checked for zero-reward loops.
Your minimum rewards may be too low...

goal = 1, inf = 0, maybe = 232934

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=232935, nc=313807, nnz=647971, k=4] [9.5 MB]
Building sparse matrix (transition rewards)... [n=232935, nc=313807, nnz=7898, k=4] [2.2 MB]
Creating vector for state rewards... [1.8 MB]
Creating vector for inf... [1.8 MB]
Allocating iteration vectors... [2 x 1.8 MB]
TOTAL: [18.8 MB]

Starting iterations...

Iterative method: 98 iterations in 0.68 seconds (average 0.003510, setup 0.34)

Value in the initial state: 0.08324540649711883

Time for model checking: 0.756 seconds.

Result: 0.08324540649711883 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"time"}max=? [ F "deadlock" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=2,Objy1=1,Objx2=6,Objy2=2

Prob0E: 4 iterations in 0.02 seconds (average 0.006000, setup 0.00)

Prob1A: 1 iterations in 0.00 seconds (average 0.004000, setup 0.00)

goal = 1, inf = 0, maybe = 232934

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=232935, nc=313807, nnz=647971, k=4] [9.5 MB]
Building sparse matrix (transition rewards)... [n=232935, nc=313807, nnz=116506, k=4] [3.4 MB]
Creating vector for state rewards... [1.8 MB]
Creating vector for inf... [1.8 MB]
Allocating iteration vectors... [2 x 1.8 MB]
TOTAL: [20.0 MB]

Starting iterations...

Iterative method: 94 iterations in 0.70 seconds (average 0.003489, setup 0.37)

Value in the initial state: 343.9813868323245

Time for model checking: 0.74 seconds.

Result: 343.9813868323245 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"time"}min=? [ F "deadlock" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=2,Objy1=1,Objx2=6,Objy2=2

Prob0A: 4 iterations in 0.02 seconds (average 0.005000, setup 0.00)

Prob1E: 5 iterations in 0.04 seconds (average 0.007200, setup 0.00)

Warning: PRISM hasn't checked for zero-reward loops.
Your minimum rewards may be too low...

goal = 1, inf = 0, maybe = 232934

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=232935, nc=313807, nnz=647971, k=4] [9.5 MB]
Building sparse matrix (transition rewards)... [n=232935, nc=313807, nnz=116506, k=4] [3.4 MB]
Creating vector for state rewards... [1.8 MB]
Creating vector for inf... [1.8 MB]
Allocating iteration vectors... [2 x 1.8 MB]
TOTAL: [20.0 MB]

Starting iterations...

Iterative method: 102 iterations in 0.72 seconds (average 0.003569, setup 0.36)

Value in the initial state: 110.43879079400574

Time for model checking: 0.804 seconds.

Result: 110.43879079400574 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"success"}max=? [ F "deadlock" ]/Pmin=? [ F "MissionSuccessful" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=2,Objy1=1,Objx2=6,Objy2=2

Prob0E: 4 iterations in 0.02 seconds (average 0.006000, setup 0.00)

Prob1A: 1 iterations in 0.00 seconds (average 0.004000, setup 0.00)

goal = 1, inf = 0, maybe = 232934

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=232935, nc=313807, nnz=647971, k=4] [9.5 MB]
Building sparse matrix (transition rewards)... [n=232935, nc=313807, nnz=1822, k=4] [2.1 MB]
Creating vector for state rewards... [1.8 MB]
Creating vector for inf... [1.8 MB]
Allocating iteration vectors... [2 x 1.8 MB]
TOTAL: [18.7 MB]

Starting iterations...

Iterative method: 95 iterations in 0.65 seconds (average 0.003411, setup 0.33)

Prob0E: 65 iterations in 0.33 seconds (average 0.005108, setup 0.00)

Prob1A: 3 iterations in 0.04 seconds (average 0.012000, setup 0.00)

yes = 925, no = 200285, maybe = 31725

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=232935, nc=55659, nnz=161580, k=2] [2.1 MB]
Creating vector for yes... [1.8 MB]
Allocating iteration vectors... [2 x 1.8 MB]
TOTAL: [7.5 MB]

Starting iterations...

Iterative method: 91 iterations in 0.27 seconds (average 0.001451, setup 0.14)

Value in the initial state: 462.6592142092117

Time for model checking: 1.354 seconds.

Result: 462.6592142092117 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"success"}min=? [ F "deadlock" ]/Pmax=? [ F "MissionSuccessful" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=2,Objy1=1,Objx2=6,Objy2=2

Prob0A: 4 iterations in 0.05 seconds (average 0.013000, setup 0.00)

Prob1E: 5 iterations in 0.04 seconds (average 0.007200, setup 0.00)

Warning: PRISM hasn't checked for zero-reward loops.
Your minimum rewards may be too low...

goal = 1, inf = 0, maybe = 232934

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=232935, nc=313807, nnz=647971, k=4] [9.5 MB]
Building sparse matrix (transition rewards)... [n=232935, nc=313807, nnz=1822, k=4] [2.1 MB]
Creating vector for state rewards... [1.8 MB]
Creating vector for inf... [1.8 MB]
Allocating iteration vectors... [2 x 1.8 MB]
TOTAL: [18.7 MB]

Starting iterations...

Iterative method: 96 iterations in 0.64 seconds (average 0.003208, setup 0.33)

Prob0A: 51 iterations in 0.21 seconds (average 0.004157, setup 0.00)

Prob1E: 4 iterations in 0.03 seconds (average 0.007000, setup 0.00)

yes = 925, no = 195325, maybe = 36685

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=232935, nc=64550, nnz=187431, k=2] [2.4 MB]
Creating vector for yes... [1.8 MB]
Allocating iteration vectors... [2 x 1.8 MB]
TOTAL: [7.8 MB]

Starting iterations...

Iterative method: 98 iterations in 0.25 seconds (average 0.001388, setup 0.12)

Value in the initial state: 79.2460811587454

Time for model checking: 1.229 seconds.

Result: 79.2460811587454 (value in the initial state)

---------------------------------------------------------------------

Note: There were 16 warnings during computation.

