PRISM
=====

Version: 4.3
Date: Tue Aug 02 12:25:34 BST 2016
Hostname: eglinton
Memory limits: cudd=1g, java(heap)=981.5m
Command line: prism uav_sim_ft_mdp.prism -s uav_mdp.props -const 'Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=1,Objy1=1,Objx2=5,Objy2=2,'

Parsing model file "uav_sim_ft_mdp.prism"...

Parsing properties file "uav_mdp.props"...

18 properties:
(1) Pmax=? [ F "MissionSuccessful" ]
(2) Pmin=? [ F "MissionSuccessful" ]
(3) Pmax=? [ F "MissionTimeUp" ]
(4) Pmin=? [ F "MissionTimeUp" ]
(5) Pmax=? [ F "Crash" ]
(6) Pmin=? [ F "Crash" ]
(7) Pmax=? [ F "AllAreaSearched" ]
(8) Pmin=? [ F "AllAreaSearched" ]
(9) Pmax=? [ F "MissionTimeUp"|"AllAreaSearched"|"Crash" ]
(10) Pmin=? [ F "MissionTimeUp"|"AllAreaSearched"|"Crash" ]
(11) R{"drop"}max=? [ F "deadlock" ]
(12) R{"drop"}min=? [ F "deadlock" ]
(13) R{"charge"}max=? [ F "deadlock" ]
(14) R{"charge"}min=? [ F "deadlock" ]
(15) R{"time"}max=? [ F "deadlock" ]
(16) R{"time"}min=? [ F "deadlock" ]
(17) R{"success"}max=? [ F "deadlock" ]/Pmin=? [ F "MissionSuccessful" ]
(18) R{"success"}min=? [ F "deadlock" ]/Pmax=? [ F "MissionSuccessful" ]

Type:        MDP
Modules:     Obj Move Robot T 
Variables:   NoOfObjs posx posy s1 retx rety b t c 

---------------------------------------------------------------------

Model checking: Pmax=? [ F "MissionSuccessful" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=1,Objy1=1,Objx2=5,Objy2=2

Building model...
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=1,Objy1=1,Objx2=5,Objy2=2

Warning: Guard for command 7 of module "Move" is never satisfied.

Warning: Guard for command 8 of module "Move" is never satisfied.

Warning: Guard for command 9 of module "Move" is never satisfied.

Warning: Guard for command 7 of module "Robot" is never satisfied.

Warning: Update 2 of command 3 of module "Robot" doesn't do anything ("(s1'=13)", line 249, column 44)

Warning: Update 2 of command 27 of module "T" doesn't do anything ("(c'=1)", line 372, column 108)

Warning: Guard for command 28 of module "T" is never satisfied.

Warning: Update 2 of command 9 of module "T" doesn't do anything ("(c'=1)", line 349, column 112)

Warning: Guard for command 10 of module "T" is never satisfied.

Warning: Update 2 of command 51 of module "T" doesn't do anything ("(c'=1)", line 403, column 113)

Warning: Guard for command 52 of module "T" is never satisfied.

Computing reachable states...

Reachability (BFS): 77 iterations in 0.82 seconds (average 0.010597, setup 0.00)

Time for model construction: 2.386 seconds.

Warning: Deadlocks detected and fixed in 1 states

Type:        MDP
States:      210163 (1 initial)
Transitions: 577886
Choices:     282369

Transition matrix: 169398 nodes (76 terminal), 577886 minterms, vars: 34r/34c/18nd

Prob0A: 49 iterations in 0.43 seconds (average 0.008735, setup 0.00)

Prob1E: 4 iterations in 0.06 seconds (average 0.014000, setup 0.00)

yes = 937, no = 175613, maybe = 33613

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=210163, nc=59431, nnz=171402, k=2] [2.2 MB]
Creating vector for yes... [1.6 MB]
Allocating iteration vectors... [2 x 1.6 MB]
TOTAL: [7.0 MB]

Starting iterations...

Iterative method: 94 iterations in 0.35 seconds (average 0.001362, setup 0.22)

Value in the initial state: 0.9788331568462555

Time for model checking: 1.132 seconds.

Result: 0.9788331568462555 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmin=? [ F "MissionSuccessful" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=1,Objy1=1,Objx2=5,Objy2=2

Prob0E: 49 iterations in 0.47 seconds (average 0.009633, setup 0.00)

Prob1A: 3 iterations in 0.04 seconds (average 0.013333, setup 0.00)

yes = 937, no = 180273, maybe = 28953

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=210163, nc=51034, nnz=147002, k=2] [1.9 MB]
Creating vector for yes... [1.6 MB]
Allocating iteration vectors... [2 x 1.6 MB]
TOTAL: [6.7 MB]

Starting iterations...

Iterative method: 87 iterations in 0.29 seconds (average 0.001379, setup 0.17)

Value in the initial state: 0.7373581768752605

Time for model checking: 0.829 seconds.

Result: 0.7373581768752605 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmax=? [ F "MissionTimeUp" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=1,Objy1=1,Objx2=5,Objy2=2

Prob0A: 63 iterations in 0.51 seconds (average 0.008127, setup 0.00)

Prob1E: 4 iterations in 0.06 seconds (average 0.016000, setup 0.00)

yes = 380, no = 125143, maybe = 84640

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=210163, nc=146893, nnz=410146, k=4] [5.0 MB]
Creating vector for yes... [1.6 MB]
Allocating iteration vectors... [2 x 1.6 MB]
TOTAL: [9.8 MB]

Starting iterations...

Iterative method: 91 iterations in 0.46 seconds (average 0.001846, setup 0.29)

Value in the initial state: 0.0011707919766365117

Time for model checking: 1.114 seconds.

Result: 0.0011707919766365117 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmin=? [ F "MissionTimeUp" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=1,Objy1=1,Objx2=5,Objy2=2

Prob0E: 71 iterations in 0.75 seconds (average 0.010592, setup 0.00)

Prob1A: 3 iterations in 0.05 seconds (average 0.016000, setup 0.00)

yes = 380, no = 125519, maybe = 84264

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=210163, nc=146217, nnz=408798, k=4] [5.0 MB]
Creating vector for yes... [1.6 MB]
Allocating iteration vectors... [2 x 1.6 MB]
TOTAL: [9.8 MB]

Starting iterations...

Iterative method: 105 iterations in 0.46 seconds (average 0.001867, setup 0.26)

Value in the initial state: 5.83839225346265E-6

Time for model checking: 1.348 seconds.

Result: 5.83839225346265E-6 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmax=? [ F "Crash" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=1,Objy1=1,Objx2=5,Objy2=2

Prob0A: 3 iterations in 0.07 seconds (average 0.024000, setup 0.00)

Prob1E: 188 iterations in 2.30 seconds (average 0.012255, setup 0.00)

yes = 102874, no = 2556, maybe = 104733

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=210163, nc=176939, nnz=472456, k=4] [5.8 MB]
Creating vector for yes... [1.6 MB]
Allocating iteration vectors... [2 x 1.6 MB]
TOTAL: [10.6 MB]

Starting iterations...

Iterative method: 87 iterations in 0.50 seconds (average 0.002069, setup 0.32)

Value in the initial state: 0.032024402147729045

Time for model checking: 3.291 seconds.

Result: 0.032024402147729045 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmin=? [ F "Crash" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=1,Objy1=1,Objx2=5,Objy2=2

Prob0E: 3 iterations in 0.06 seconds (average 0.021333, setup 0.00)

Prob1A: 47 iterations in 0.46 seconds (average 0.009872, setup 0.00)

yes = 102874, no = 2556, maybe = 104733

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=210163, nc=176939, nnz=472456, k=4] [5.8 MB]
Creating vector for yes... [1.6 MB]
Allocating iteration vectors... [2 x 1.6 MB]
TOTAL: [10.6 MB]

Starting iterations...

Iterative method: 93 iterations in 0.43 seconds (average 0.002108, setup 0.23)

Value in the initial state: 0.01913599951729968

Time for model checking: 1.178 seconds.

Result: 0.01913599951729968 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmax=? [ F "AllAreaSearched" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=1,Objy1=1,Objx2=5,Objy2=2

Prob0A: 51 iterations in 0.48 seconds (average 0.009490, setup 0.00)

Prob1E: 4 iterations in 0.04 seconds (average 0.011000, setup 0.00)

yes = 1238, no = 160824, maybe = 48101

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=210163, nc=77898, nnz=213328, k=2] [2.7 MB]
Creating vector for yes... [1.6 MB]
Allocating iteration vectors... [2 x 1.6 MB]
TOTAL: [7.5 MB]

Starting iterations...

Iterative method: 88 iterations in 0.32 seconds (average 0.001636, setup 0.18)

Value in the initial state: 0.23399971305108297

Time for model checking: 0.87 seconds.

Result: 0.23399971305108297 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmin=? [ F "AllAreaSearched" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=1,Objy1=1,Objx2=5,Objy2=2

Prob0E: 63 iterations in 0.41 seconds (average 0.006540, setup 0.00)

Prob1A: 3 iterations in 0.03 seconds (average 0.010667, setup 0.00)

yes = 1238, no = 167293, maybe = 41632

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=210163, nc=66538, nnz=180672, k=2] [2.3 MB]
Creating vector for yes... [1.6 MB]
Allocating iteration vectors... [2 x 1.6 MB]
TOTAL: [7.1 MB]

Starting iterations...

Iterative method: 91 iterations in 0.27 seconds (average 0.001582, setup 0.12)

Value in the initial state: 0.001761750184198415

Time for model checking: 0.771 seconds.

Result: 0.001761750184198415 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmax=? [ F "MissionTimeUp"|"AllAreaSearched"|"Crash" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=1,Objy1=1,Objx2=5,Objy2=2

Prob0A: 3 iterations in 0.08 seconds (average 0.026667, setup 0.00)

Prob1E: 192 iterations in 2.29 seconds (average 0.011917, setup 0.00)

yes = 180272, no = 938, maybe = 28953

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=210163, nc=51034, nnz=147002, k=2] [1.9 MB]
Creating vector for yes... [1.6 MB]
Allocating iteration vectors... [2 x 1.6 MB]
TOTAL: [6.7 MB]

Starting iterations...

Iterative method: 80 iterations in 0.30 seconds (average 0.001400, setup 0.19)

Value in the initial state: 0.26264111197134093

Time for model checking: 2.927 seconds.

Result: 0.26264111197134093 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmin=? [ F "MissionTimeUp"|"AllAreaSearched"|"Crash" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=1,Objy1=1,Objx2=5,Objy2=2

Prob0E: 3 iterations in 0.06 seconds (average 0.020000, setup 0.00)

Prob1A: 49 iterations in 0.44 seconds (average 0.009061, setup 0.00)

yes = 175612, no = 938, maybe = 33613

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=210163, nc=59431, nnz=171402, k=2] [2.2 MB]
Creating vector for yes... [1.6 MB]
Allocating iteration vectors... [2 x 1.6 MB]
TOTAL: [7.0 MB]

Starting iterations...

Iterative method: 91 iterations in 0.30 seconds (average 0.001495, setup 0.16)

Value in the initial state: 0.021166279651460937

Time for model checking: 0.828 seconds.

Result: 0.021166279651460937 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"drop"}max=? [ F "deadlock" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=1,Objy1=1,Objx2=5,Objy2=2

Prob0E: 4 iterations in 0.06 seconds (average 0.016000, setup 0.00)

Prob1A: 1 iterations in 0.00 seconds (average 0.004000, setup 0.00)

goal = 1, inf = 0, maybe = 210162

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=210163, nc=282368, nnz=577885, k=4] [8.5 MB]
Building sparse matrix (transition rewards)... [n=210163, nc=282368, nnz=0, k=4] [1.9 MB]
Creating vector for state rewards... [1.6 MB]
Creating vector for inf... [1.6 MB]
Allocating iteration vectors... [2 x 1.6 MB]
TOTAL: [16.8 MB]

Starting iterations...

Iterative method: 91 iterations in 0.68 seconds (average 0.003121, setup 0.39)

Value in the initial state: 0.21657819590906438

Time for model checking: 0.844 seconds.

Result: 0.21657819590906438 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"drop"}min=? [ F "deadlock" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=1,Objy1=1,Objx2=5,Objy2=2

Prob0A: 4 iterations in 0.03 seconds (average 0.008000, setup 0.00)

Prob1E: 5 iterations in 0.04 seconds (average 0.008800, setup 0.00)

Warning: PRISM hasn't checked for zero-reward loops.
Your minimum rewards may be too low...

goal = 1, inf = 0, maybe = 210162

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=210163, nc=282368, nnz=577885, k=4] [8.5 MB]
Building sparse matrix (transition rewards)... [n=210163, nc=282368, nnz=0, k=4] [1.9 MB]
Creating vector for state rewards... [1.6 MB]
Creating vector for inf... [1.6 MB]
Allocating iteration vectors... [2 x 1.6 MB]
TOTAL: [16.8 MB]

Starting iterations...

Iterative method: 81 iterations in 0.56 seconds (average 0.003160, setup 0.31)

Value in the initial state: 0.17794278505591338

Time for model checking: 0.724 seconds.

Result: 0.17794278505591338 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"charge"}max=? [ F "deadlock" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=1,Objy1=1,Objx2=5,Objy2=2

Prob0E: 4 iterations in 0.02 seconds (average 0.006000, setup 0.00)

Prob1A: 1 iterations in 0.01 seconds (average 0.008000, setup 0.00)

goal = 1, inf = 0, maybe = 210162

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=210163, nc=282368, nnz=577885, k=4] [8.5 MB]
Building sparse matrix (transition rewards)... [n=210163, nc=282368, nnz=6842, k=4] [2.0 MB]
Creating vector for state rewards... [1.6 MB]
Creating vector for inf... [1.6 MB]
Allocating iteration vectors... [2 x 1.6 MB]
TOTAL: [16.9 MB]

Starting iterations...

Iterative method: 86 iterations in 0.58 seconds (average 0.003116, setup 0.31)

Value in the initial state: 0.29401467536839526

Time for model checking: 0.706 seconds.

Result: 0.29401467536839526 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"charge"}min=? [ F "deadlock" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=1,Objy1=1,Objx2=5,Objy2=2

Prob0A: 4 iterations in 0.02 seconds (average 0.006000, setup 0.00)

Prob1E: 5 iterations in 0.04 seconds (average 0.008800, setup 0.00)

Warning: PRISM hasn't checked for zero-reward loops.
Your minimum rewards may be too low...

goal = 1, inf = 0, maybe = 210162

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=210163, nc=282368, nnz=577885, k=4] [8.5 MB]
Building sparse matrix (transition rewards)... [n=210163, nc=282368, nnz=6842, k=4] [2.0 MB]
Creating vector for state rewards... [1.6 MB]
Creating vector for inf... [1.6 MB]
Allocating iteration vectors... [2 x 1.6 MB]
TOTAL: [16.9 MB]

Starting iterations...

Iterative method: 94 iterations in 0.64 seconds (average 0.003234, setup 0.34)

Value in the initial state: 0.06717814334629378

Time for model checking: 0.725 seconds.

Result: 0.06717814334629378 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"time"}max=? [ F "deadlock" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=1,Objy1=1,Objx2=5,Objy2=2

Prob0E: 4 iterations in 0.02 seconds (average 0.005000, setup 0.00)

Prob1A: 1 iterations in 0.00 seconds (average 0.004000, setup 0.00)

goal = 1, inf = 0, maybe = 210162

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=210163, nc=282368, nnz=577885, k=4] [8.5 MB]
Building sparse matrix (transition rewards)... [n=210163, nc=282368, nnz=105428, k=4] [3.1 MB]
Creating vector for state rewards... [1.6 MB]
Creating vector for inf... [1.6 MB]
Allocating iteration vectors... [2 x 1.6 MB]
TOTAL: [18.0 MB]

Starting iterations...

Iterative method: 89 iterations in 0.63 seconds (average 0.003191, setup 0.34)

Value in the initial state: 176.60366331340842

Time for model checking: 0.666 seconds.

Result: 176.60366331340842 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"time"}min=? [ F "deadlock" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=1,Objy1=1,Objx2=5,Objy2=2

Prob0A: 4 iterations in 0.02 seconds (average 0.006000, setup 0.00)

Prob1E: 5 iterations in 0.04 seconds (average 0.008800, setup 0.00)

Warning: PRISM hasn't checked for zero-reward loops.
Your minimum rewards may be too low...

goal = 1, inf = 0, maybe = 210162

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=210163, nc=282368, nnz=577885, k=4] [8.5 MB]
Building sparse matrix (transition rewards)... [n=210163, nc=282368, nnz=105428, k=4] [3.1 MB]
Creating vector for state rewards... [1.6 MB]
Creating vector for inf... [1.6 MB]
Allocating iteration vectors... [2 x 1.6 MB]
TOTAL: [18.0 MB]

Starting iterations...

Iterative method: 99 iterations in 0.66 seconds (average 0.003232, setup 0.34)

Value in the initial state: 105.97891916415749

Time for model checking: 0.74 seconds.

Result: 105.97891916415749 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"success"}max=? [ F "deadlock" ]/Pmin=? [ F "MissionSuccessful" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=1,Objy1=1,Objx2=5,Objy2=2

Prob0E: 4 iterations in 0.03 seconds (average 0.007000, setup 0.00)

Prob1A: 1 iterations in 0.00 seconds (average 0.004000, setup 0.00)

goal = 1, inf = 0, maybe = 210162

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=210163, nc=282368, nnz=577885, k=4] [8.5 MB]
Building sparse matrix (transition rewards)... [n=210163, nc=282368, nnz=1846, k=4] [1.9 MB]
Creating vector for state rewards... [1.6 MB]
Creating vector for inf... [1.6 MB]
Allocating iteration vectors... [2 x 1.6 MB]
TOTAL: [16.8 MB]

Starting iterations...

Iterative method: 93 iterations in 0.58 seconds (average 0.003054, setup 0.30)

Prob0E: 49 iterations in 0.48 seconds (average 0.009878, setup 0.00)

Prob1A: 3 iterations in 0.04 seconds (average 0.012000, setup 0.00)

yes = 937, no = 180273, maybe = 28953

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=210163, nc=51034, nnz=147002, k=2] [1.9 MB]
Creating vector for yes... [1.6 MB]
Allocating iteration vectors... [2 x 1.6 MB]
TOTAL: [6.7 MB]

Starting iterations...

Iterative method: 87 iterations in 0.26 seconds (average 0.001333, setup 0.15)

Value in the initial state: 234.4440320598508

Time for model checking: 1.438 seconds.

Result: 234.4440320598508 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"success"}min=? [ F "deadlock" ]/Pmax=? [ F "MissionSuccessful" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=1,Objy1=1,Objx2=5,Objy2=2

Prob0A: 4 iterations in 0.10 seconds (average 0.025000, setup 0.00)

Prob1E: 5 iterations in 0.05 seconds (average 0.009600, setup 0.00)

Warning: PRISM hasn't checked for zero-reward loops.
Your minimum rewards may be too low...

goal = 1, inf = 0, maybe = 210162

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=210163, nc=282368, nnz=577885, k=4] [8.5 MB]
Building sparse matrix (transition rewards)... [n=210163, nc=282368, nnz=1846, k=4] [1.9 MB]
Creating vector for state rewards... [1.6 MB]
Creating vector for inf... [1.6 MB]
Allocating iteration vectors... [2 x 1.6 MB]
TOTAL: [16.8 MB]

Starting iterations...

Iterative method: 92 iterations in 0.63 seconds (average 0.003087, setup 0.34)

Prob0A: 49 iterations in 0.25 seconds (average 0.005143, setup 0.00)

Prob1E: 4 iterations in 0.03 seconds (average 0.008000, setup 0.00)

yes = 937, no = 175613, maybe = 33613

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=210163, nc=59431, nnz=171402, k=2] [2.2 MB]
Creating vector for yes... [1.6 MB]
Allocating iteration vectors... [2 x 1.6 MB]
TOTAL: [7.0 MB]

Starting iterations...

Iterative method: 94 iterations in 0.28 seconds (average 0.001362, setup 0.16)

Value in the initial state: 78.6232593818448

Time for model checking: 1.345 seconds.

Result: 78.6232593818448 (value in the initial state)

---------------------------------------------------------------------

Note: There were 16 warnings during computation.

