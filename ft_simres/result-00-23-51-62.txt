PRISM
=====

Version: 4.3
Date: Tue Aug 02 12:32:23 BST 2016
Hostname: eglinton
Memory limits: cudd=1g, java(heap)=981.5m
Command line: prism uav_sim_ft_mdp.prism -s uav_mdp.props -const 'Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=5,Objy1=1,Objx2=6,Objy2=2,'

Parsing model file "uav_sim_ft_mdp.prism"...

Parsing properties file "uav_mdp.props"...

18 properties:
(1) Pmax=? [ F "MissionSuccessful" ]
(2) Pmin=? [ F "MissionSuccessful" ]
(3) Pmax=? [ F "MissionTimeUp" ]
(4) Pmin=? [ F "MissionTimeUp" ]
(5) Pmax=? [ F "Crash" ]
(6) Pmin=? [ F "Crash" ]
(7) Pmax=? [ F "AllAreaSearched" ]
(8) Pmin=? [ F "AllAreaSearched" ]
(9) Pmax=? [ F "MissionTimeUp"|"AllAreaSearched"|"Crash" ]
(10) Pmin=? [ F "MissionTimeUp"|"AllAreaSearched"|"Crash" ]
(11) R{"drop"}max=? [ F "deadlock" ]
(12) R{"drop"}min=? [ F "deadlock" ]
(13) R{"charge"}max=? [ F "deadlock" ]
(14) R{"charge"}min=? [ F "deadlock" ]
(15) R{"time"}max=? [ F "deadlock" ]
(16) R{"time"}min=? [ F "deadlock" ]
(17) R{"success"}max=? [ F "deadlock" ]/Pmin=? [ F "MissionSuccessful" ]
(18) R{"success"}min=? [ F "deadlock" ]/Pmax=? [ F "MissionSuccessful" ]

Type:        MDP
Modules:     Obj Move Robot T 
Variables:   NoOfObjs posx posy s1 retx rety b t c 

---------------------------------------------------------------------

Model checking: Pmax=? [ F "MissionSuccessful" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=5,Objy1=1,Objx2=6,Objy2=2

Building model...
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=5,Objy1=1,Objx2=6,Objy2=2

Warning: Guard for command 7 of module "Move" is never satisfied.

Warning: Guard for command 8 of module "Move" is never satisfied.

Warning: Guard for command 9 of module "Move" is never satisfied.

Warning: Guard for command 7 of module "Robot" is never satisfied.

Warning: Update 2 of command 3 of module "Robot" doesn't do anything ("(s1'=13)", line 249, column 44)

Warning: Update 2 of command 27 of module "T" doesn't do anything ("(c'=1)", line 372, column 108)

Warning: Guard for command 28 of module "T" is never satisfied.

Warning: Update 2 of command 9 of module "T" doesn't do anything ("(c'=1)", line 349, column 112)

Warning: Guard for command 10 of module "T" is never satisfied.

Warning: Update 2 of command 51 of module "T" doesn't do anything ("(c'=1)", line 403, column 113)

Warning: Guard for command 52 of module "T" is never satisfied.

Computing reachable states...

Reachability (BFS): 80 iterations in 1.00 seconds (average 0.012450, setup 0.00)

Time for model construction: 1.946 seconds.

Warning: Deadlocks detected and fixed in 1 states

Type:        MDP
States:      251945 (1 initial)
Transitions: 698638
Choices:     338284

Transition matrix: 176849 nodes (76 terminal), 698638 minterms, vars: 34r/34c/18nd

Prob0A: 53 iterations in 0.40 seconds (average 0.007547, setup 0.00)

Prob1E: 4 iterations in 0.07 seconds (average 0.018000, setup 0.00)

yes = 921, no = 212664, maybe = 38360

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=251945, nc=67021, nnz=194829, k=2] [2.5 MB]
Creating vector for yes... [1.9 MB]
Allocating iteration vectors... [2 x 1.9 MB]
TOTAL: [8.3 MB]

Starting iterations...

Iterative method: 99 iterations in 0.46 seconds (average 0.001939, setup 0.27)

Value in the initial state: 0.9769645270449044

Time for model checking: 0.995 seconds.

Result: 0.9769645270449044 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmin=? [ F "MissionSuccessful" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=5,Objy1=1,Objx2=6,Objy2=2

Prob0E: 66 iterations in 0.48 seconds (average 0.007273, setup 0.00)

Prob1A: 3 iterations in 0.07 seconds (average 0.022667, setup 0.00)

yes = 921, no = 218197, maybe = 32827

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=251945, nc=57218, nnz=166019, k=2] [2.2 MB]
Creating vector for yes... [1.9 MB]
Allocating iteration vectors... [2 x 1.9 MB]
TOTAL: [8.0 MB]

Starting iterations...

Iterative method: 92 iterations in 0.42 seconds (average 0.002348, setup 0.20)

Value in the initial state: 0.7141378611957422

Time for model checking: 1.014 seconds.

Result: 0.7141378611957422 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmax=? [ F "MissionTimeUp" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=5,Objy1=1,Objx2=6,Objy2=2

Prob0A: 60 iterations in 0.46 seconds (average 0.007600, setup 0.00)

Prob1E: 4 iterations in 0.03 seconds (average 0.008000, setup 0.00)

yes = 456, no = 144193, maybe = 107296

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=251945, nc=183758, nnz=513227, k=4] [6.3 MB]
Creating vector for yes... [1.9 MB]
Allocating iteration vectors... [2 x 1.9 MB]
TOTAL: [12.1 MB]

Starting iterations...

Iterative method: 94 iterations in 0.48 seconds (average 0.002213, setup 0.28)

Value in the initial state: 0.0032874254777222555

Time for model checking: 0.982 seconds.

Result: 0.0032874254777222555 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmin=? [ F "MissionTimeUp" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=5,Objy1=1,Objx2=6,Objy2=2

Prob0E: 68 iterations in 0.62 seconds (average 0.009059, setup 0.00)

Prob1A: 3 iterations in 0.05 seconds (average 0.016000, setup 0.00)

yes = 456, no = 144626, maybe = 106863

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=251945, nc=182968, nnz=511762, k=4] [6.3 MB]
Creating vector for yes... [1.9 MB]
Allocating iteration vectors... [2 x 1.9 MB]
TOTAL: [12.0 MB]

Starting iterations...

Iterative method: 110 iterations in 0.52 seconds (average 0.002182, setup 0.28)

Value in the initial state: 2.0741386148499732E-5

Time for model checking: 1.213 seconds.

Result: 2.0741386148499732E-5 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmax=? [ F "Crash" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=5,Objy1=1,Objx2=6,Objy2=2

Prob0A: 3 iterations in 0.06 seconds (average 0.020000, setup 0.00)

Prob1E: 248 iterations in 2.32 seconds (average 0.009339, setup 0.00)

yes = 123171, no = 2604, maybe = 126170

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=251945, nc=212509, nnz=572863, k=4] [7.0 MB]
Creating vector for yes... [1.9 MB]
Allocating iteration vectors... [2 x 1.9 MB]
TOTAL: [12.8 MB]

Starting iterations...

Iterative method: 93 iterations in 0.55 seconds (average 0.002280, setup 0.34)

Value in the initial state: 0.06301334009156533

Time for model checking: 2.952 seconds.

Result: 0.06301334009156533 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmin=? [ F "Crash" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=5,Objy1=1,Objx2=6,Objy2=2

Prob0E: 3 iterations in 0.11 seconds (average 0.036000, setup 0.00)

Prob1A: 46 iterations in 0.58 seconds (average 0.012522, setup 0.00)

yes = 123171, no = 2604, maybe = 126170

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=251945, nc=212509, nnz=572863, k=4] [7.0 MB]
Creating vector for yes... [1.9 MB]
Allocating iteration vectors... [2 x 1.9 MB]
TOTAL: [12.8 MB]

Starting iterations...

Iterative method: 98 iterations in 0.54 seconds (average 0.002367, setup 0.30)

Value in the initial state: 0.020927641717085416

Time for model checking: 1.262 seconds.

Result: 0.020927641717085416 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmax=? [ F "AllAreaSearched" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=5,Objy1=1,Objx2=6,Objy2=2

Prob0A: 57 iterations in 0.26 seconds (average 0.004561, setup 0.00)

Prob1E: 4 iterations in 0.03 seconds (average 0.008000, setup 0.00)

yes = 1226, no = 197742, maybe = 52977

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=251945, nc=86572, nnz=240426, k=2] [3.1 MB]
Creating vector for yes... [1.9 MB]
Allocating iteration vectors... [2 x 1.9 MB]
TOTAL: [8.8 MB]

Starting iterations...

Iterative method: 91 iterations in 0.32 seconds (average 0.001758, setup 0.16)

Value in the initial state: 0.2556125551511473

Time for model checking: 0.632 seconds.

Result: 0.2556125551511473 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmin=? [ F "AllAreaSearched" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=5,Objy1=1,Objx2=6,Objy2=2

Prob0E: 72 iterations in 0.34 seconds (average 0.004667, setup 0.00)

Prob1A: 3 iterations in 0.03 seconds (average 0.009333, setup 0.00)

yes = 1226, no = 205743, maybe = 44976

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=251945, nc=73145, nnz=202513, k=2] [2.6 MB]
Creating vector for yes... [1.9 MB]
Allocating iteration vectors... [2 x 1.9 MB]
TOTAL: [8.4 MB]

Starting iterations...

Iterative method: 92 iterations in 0.28 seconds (average 0.001565, setup 0.13)

Value in the initial state: 0.0012884681818240622

Time for model checking: 0.664 seconds.

Result: 0.0012884681818240622 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmax=? [ F "MissionTimeUp"|"AllAreaSearched"|"Crash" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=5,Objy1=1,Objx2=6,Objy2=2

Prob0A: 3 iterations in 0.10 seconds (average 0.033333, setup 0.00)

Prob1E: 256 iterations in 2.71 seconds (average 0.010578, setup 0.00)

yes = 218196, no = 922, maybe = 32827

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=251945, nc=57218, nnz=166019, k=2] [2.2 MB]
Creating vector for yes... [1.9 MB]
Allocating iteration vectors... [2 x 1.9 MB]
TOTAL: [8.0 MB]

Starting iterations...

Iterative method: 86 iterations in 0.32 seconds (average 0.001535, setup 0.19)

Value in the initial state: 0.28586149045156556

Time for model checking: 3.143 seconds.

Result: 0.28586149045156556 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmin=? [ F "MissionTimeUp"|"AllAreaSearched"|"Crash" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=5,Objy1=1,Objx2=6,Objy2=2

Prob0E: 3 iterations in 0.05 seconds (average 0.017333, setup 0.00)

Prob1A: 53 iterations in 0.49 seconds (average 0.009283, setup 0.00)

yes = 212663, no = 922, maybe = 38360

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=251945, nc=67021, nnz=194829, k=2] [2.5 MB]
Creating vector for yes... [1.9 MB]
Allocating iteration vectors... [2 x 1.9 MB]
TOTAL: [8.3 MB]

Starting iterations...

Iterative method: 95 iterations in 0.31 seconds (average 0.001600, setup 0.16)

Value in the initial state: 0.023035074501359297

Time for model checking: 0.884 seconds.

Result: 0.023035074501359297 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"drop"}max=? [ F "deadlock" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=5,Objy1=1,Objx2=6,Objy2=2

Prob0E: 4 iterations in 0.08 seconds (average 0.020000, setup 0.00)

Prob1A: 1 iterations in 0.01 seconds (average 0.008000, setup 0.00)

goal = 1, inf = 0, maybe = 251944

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=251945, nc=338283, nnz=698637, k=4] [10.2 MB]
Building sparse matrix (transition rewards)... [n=251945, nc=338283, nnz=0, k=4] [2.3 MB]
Creating vector for state rewards... [1.9 MB]
Creating vector for inf... [1.9 MB]
Allocating iteration vectors... [2 x 1.9 MB]
TOTAL: [20.2 MB]

Starting iterations...

Iterative method: 93 iterations in 0.74 seconds (average 0.003570, setup 0.41)

Value in the initial state: 0.35226130265551

Time for model checking: 0.85 seconds.

Result: 0.35226130265551 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"drop"}min=? [ F "deadlock" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=5,Objy1=1,Objx2=6,Objy2=2

Prob0A: 4 iterations in 0.02 seconds (average 0.006000, setup 0.00)

Prob1E: 5 iterations in 0.04 seconds (average 0.008800, setup 0.00)

Warning: PRISM hasn't checked for zero-reward loops.
Your minimum rewards may be too low...

goal = 1, inf = 0, maybe = 251944

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=251945, nc=338283, nnz=698637, k=4] [10.2 MB]
Building sparse matrix (transition rewards)... [n=251945, nc=338283, nnz=0, k=4] [2.3 MB]
Creating vector for state rewards... [1.9 MB]
Creating vector for inf... [1.9 MB]
Allocating iteration vectors... [2 x 1.9 MB]
TOTAL: [20.2 MB]

Starting iterations...

Iterative method: 85 iterations in 0.65 seconds (average 0.003624, setup 0.34)

Value in the initial state: 0.22557650768907778

Time for model checking: 0.734 seconds.

Result: 0.22557650768907778 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"charge"}max=? [ F "deadlock" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=5,Objy1=1,Objx2=6,Objy2=2

Prob0E: 4 iterations in 0.02 seconds (average 0.006000, setup 0.00)

Prob1A: 1 iterations in 0.00 seconds (average 0.004000, setup 0.00)

goal = 1, inf = 0, maybe = 251944

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=251945, nc=338283, nnz=698637, k=4] [10.2 MB]
Building sparse matrix (transition rewards)... [n=251945, nc=338283, nnz=8570, k=4] [2.3 MB]
Creating vector for state rewards... [1.9 MB]
Creating vector for inf... [1.9 MB]
Allocating iteration vectors... [2 x 1.9 MB]
TOTAL: [20.3 MB]

Starting iterations...

Iterative method: 90 iterations in 0.66 seconds (average 0.003600, setup 0.34)

Value in the initial state: 0.9844425285701298

Time for model checking: 0.71 seconds.

Result: 0.9844425285701298 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"charge"}min=? [ F "deadlock" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=5,Objy1=1,Objx2=6,Objy2=2

Prob0A: 4 iterations in 0.02 seconds (average 0.006000, setup 0.00)

Prob1E: 5 iterations in 0.04 seconds (average 0.008800, setup 0.00)

Warning: PRISM hasn't checked for zero-reward loops.
Your minimum rewards may be too low...

goal = 1, inf = 0, maybe = 251944

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=251945, nc=338283, nnz=698637, k=4] [10.2 MB]
Building sparse matrix (transition rewards)... [n=251945, nc=338283, nnz=8570, k=4] [2.3 MB]
Creating vector for state rewards... [1.9 MB]
Creating vector for inf... [1.9 MB]
Allocating iteration vectors... [2 x 1.9 MB]
TOTAL: [20.3 MB]

Starting iterations...

Iterative method: 99 iterations in 0.70 seconds (average 0.003636, setup 0.34)

Value in the initial state: 0.09872387411998286

Time for model checking: 0.779 seconds.

Result: 0.09872387411998286 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"time"}max=? [ F "deadlock" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=5,Objy1=1,Objx2=6,Objy2=2

Prob0E: 4 iterations in 0.02 seconds (average 0.005000, setup 0.00)

Prob1A: 1 iterations in 0.00 seconds (average 0.004000, setup 0.00)

goal = 1, inf = 0, maybe = 251944

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=251945, nc=338283, nnz=698637, k=4] [10.2 MB]
Building sparse matrix (transition rewards)... [n=251945, nc=338283, nnz=125773, k=4] [3.7 MB]
Creating vector for state rewards... [1.9 MB]
Creating vector for inf... [1.9 MB]
Allocating iteration vectors... [2 x 1.9 MB]
TOTAL: [21.6 MB]

Starting iterations...

Iterative method: 95 iterations in 0.74 seconds (average 0.003663, setup 0.39)

Value in the initial state: 346.0406952343069

Time for model checking: 0.776 seconds.

Result: 346.0406952343069 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"time"}min=? [ F "deadlock" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=5,Objy1=1,Objx2=6,Objy2=2

Prob0A: 4 iterations in 0.02 seconds (average 0.005000, setup 0.00)

Prob1E: 5 iterations in 0.04 seconds (average 0.008000, setup 0.00)

Warning: PRISM hasn't checked for zero-reward loops.
Your minimum rewards may be too low...

goal = 1, inf = 0, maybe = 251944

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=251945, nc=338283, nnz=698637, k=4] [10.2 MB]
Building sparse matrix (transition rewards)... [n=251945, nc=338283, nnz=125773, k=4] [3.7 MB]
Creating vector for state rewards... [1.9 MB]
Creating vector for inf... [1.9 MB]
Allocating iteration vectors... [2 x 1.9 MB]
TOTAL: [21.6 MB]

Starting iterations...

Iterative method: 104 iterations in 0.76 seconds (average 0.003654, setup 0.38)

Value in the initial state: 115.82223583372519

Time for model checking: 0.83 seconds.

Result: 115.82223583372519 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"success"}max=? [ F "deadlock" ]/Pmin=? [ F "MissionSuccessful" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=5,Objy1=1,Objx2=6,Objy2=2

Prob0E: 4 iterations in 0.02 seconds (average 0.006000, setup 0.00)

Prob1A: 1 iterations in 0.00 seconds (average 0.000000, setup 0.00)

goal = 1, inf = 0, maybe = 251944

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=251945, nc=338283, nnz=698637, k=4] [10.2 MB]
Building sparse matrix (transition rewards)... [n=251945, nc=338283, nnz=1814, k=4] [2.3 MB]
Creating vector for state rewards... [1.9 MB]
Creating vector for inf... [1.9 MB]
Allocating iteration vectors... [2 x 1.9 MB]
TOTAL: [20.2 MB]

Starting iterations...

Iterative method: 96 iterations in 0.68 seconds (average 0.003333, setup 0.36)

Prob0E: 66 iterations in 0.28 seconds (average 0.004303, setup 0.00)

Prob1A: 3 iterations in 0.04 seconds (average 0.014667, setup 0.00)

yes = 921, no = 218197, maybe = 32827

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=251945, nc=57218, nnz=166019, k=2] [2.2 MB]
Creating vector for yes... [1.9 MB]
Allocating iteration vectors... [2 x 1.9 MB]
TOTAL: [8.0 MB]

Starting iterations...

Iterative method: 92 iterations in 0.27 seconds (average 0.001435, setup 0.14)

Value in the initial state: 472.50328655971197

Time for model checking: 1.335 seconds.

Result: 472.50328655971197 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"success"}min=? [ F "deadlock" ]/Pmax=? [ F "MissionSuccessful" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=5,Objy1=1,Objx2=6,Objy2=2

Prob0A: 4 iterations in 0.07 seconds (average 0.018000, setup 0.00)

Prob1E: 5 iterations in 0.03 seconds (average 0.006400, setup 0.00)

Warning: PRISM hasn't checked for zero-reward loops.
Your minimum rewards may be too low...

goal = 1, inf = 0, maybe = 251944

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=251945, nc=338283, nnz=698637, k=4] [10.2 MB]
Building sparse matrix (transition rewards)... [n=251945, nc=338283, nnz=1814, k=4] [2.3 MB]
Creating vector for state rewards... [1.9 MB]
Creating vector for inf... [1.9 MB]
Allocating iteration vectors... [2 x 1.9 MB]
TOTAL: [20.2 MB]

Starting iterations...

Iterative method: 97 iterations in 0.78 seconds (average 0.003959, setup 0.40)

Prob0A: 53 iterations in 0.26 seconds (average 0.004981, setup 0.00)

Prob1E: 4 iterations in 0.04 seconds (average 0.009000, setup 0.00)

yes = 921, no = 212664, maybe = 38360

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=251945, nc=67021, nnz=194829, k=2] [2.5 MB]
Creating vector for yes... [1.9 MB]
Allocating iteration vectors... [2 x 1.9 MB]
TOTAL: [8.3 MB]

Starting iterations...

Iterative method: 99 iterations in 0.32 seconds (average 0.001535, setup 0.16)

Value in the initial state: 80.12069049254839

Time for model checking: 1.514 seconds.

Result: 80.12069049254839 (value in the initial state)

---------------------------------------------------------------------

Note: There were 16 warnings during computation.

