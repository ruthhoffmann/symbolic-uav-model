PRISM
=====

Version: 4.3
Date: Tue Aug 02 12:24:29 BST 2016
Hostname: eglinton
Memory limits: cudd=1g, java(heap)=981.5m
Command line: prism uav_sim_ft_mdp.prism -s uav_mdp.props -const 'Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=0,Objy1=3,Objx2=1,Objy2=0,'

Parsing model file "uav_sim_ft_mdp.prism"...

Parsing properties file "uav_mdp.props"...

18 properties:
(1) Pmax=? [ F "MissionSuccessful" ]
(2) Pmin=? [ F "MissionSuccessful" ]
(3) Pmax=? [ F "MissionTimeUp" ]
(4) Pmin=? [ F "MissionTimeUp" ]
(5) Pmax=? [ F "Crash" ]
(6) Pmin=? [ F "Crash" ]
(7) Pmax=? [ F "AllAreaSearched" ]
(8) Pmin=? [ F "AllAreaSearched" ]
(9) Pmax=? [ F "MissionTimeUp"|"AllAreaSearched"|"Crash" ]
(10) Pmin=? [ F "MissionTimeUp"|"AllAreaSearched"|"Crash" ]
(11) R{"drop"}max=? [ F "deadlock" ]
(12) R{"drop"}min=? [ F "deadlock" ]
(13) R{"charge"}max=? [ F "deadlock" ]
(14) R{"charge"}min=? [ F "deadlock" ]
(15) R{"time"}max=? [ F "deadlock" ]
(16) R{"time"}min=? [ F "deadlock" ]
(17) R{"success"}max=? [ F "deadlock" ]/Pmin=? [ F "MissionSuccessful" ]
(18) R{"success"}min=? [ F "deadlock" ]/Pmax=? [ F "MissionSuccessful" ]

Type:        MDP
Modules:     Obj Move Robot T 
Variables:   NoOfObjs posx posy s1 retx rety b t c 

---------------------------------------------------------------------

Model checking: Pmax=? [ F "MissionSuccessful" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=0,Objy1=3,Objx2=1,Objy2=0

Building model...
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=0,Objy1=3,Objx2=1,Objy2=0

Warning: Guard for command 7 of module "Move" is never satisfied.

Warning: Guard for command 8 of module "Move" is never satisfied.

Warning: Guard for command 9 of module "Move" is never satisfied.

Warning: Guard for command 6 of module "Robot" is never satisfied.

Warning: Update 2 of command 3 of module "Robot" doesn't do anything ("(s1'=13)", line 249, column 44)

Warning: Update 2 of command 27 of module "T" doesn't do anything ("(c'=1)", line 372, column 108)

Warning: Guard for command 28 of module "T" is never satisfied.

Warning: Update 2 of command 9 of module "T" doesn't do anything ("(c'=1)", line 349, column 112)

Warning: Guard for command 10 of module "T" is never satisfied.

Warning: Update 2 of command 51 of module "T" doesn't do anything ("(c'=1)", line 403, column 113)

Warning: Guard for command 52 of module "T" is never satisfied.

Computing reachable states...

Reachability (BFS): 80 iterations in 0.98 seconds (average 0.012200, setup 0.00)

Time for model construction: 2.648 seconds.

Warning: Deadlocks detected and fixed in 1 states

Type:        MDP
States:      205639 (1 initial)
Transitions: 537512
Choices:     268459

Transition matrix: 180870 nodes (76 terminal), 537512 minterms, vars: 34r/34c/18nd

Prob0A: 63 iterations in 0.49 seconds (average 0.007746, setup 0.00)

Prob1E: 4 iterations in 0.06 seconds (average 0.015000, setup 0.00)

yes = 710, no = 169559, maybe = 35370

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=205639, nc=58781, nnz=161291, k=2] [2.1 MB]
Creating vector for yes... [1.6 MB]
Allocating iteration vectors... [2 x 1.6 MB]
TOTAL: [6.8 MB]

Starting iterations...

Iterative method: 98 iterations in 0.38 seconds (average 0.001469, setup 0.23)

Value in the initial state: 0.7285015442911869

Time for model checking: 1.037 seconds.

Result: 0.7285015442911869 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmin=? [ F "MissionSuccessful" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=0,Objy1=3,Objx2=1,Objy2=0

Prob0E: 73 iterations in 0.60 seconds (average 0.008274, setup 0.00)

Prob1A: 3 iterations in 0.06 seconds (average 0.020000, setup 0.00)

yes = 710, no = 177597, maybe = 27332

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=205639, nc=45639, nnz=125476, k=2] [1.7 MB]
Creating vector for yes... [1.6 MB]
Allocating iteration vectors... [2 x 1.6 MB]
TOTAL: [6.4 MB]

Starting iterations...

Iterative method: 94 iterations in 0.32 seconds (average 0.001404, setup 0.19)

Value in the initial state: 0.0073023117134808175

Time for model checking: 1.299 seconds.

Result: 0.0073023117134808175 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmax=? [ F "MissionTimeUp" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=0,Objy1=3,Objx2=1,Objy2=0

Prob0A: 55 iterations in 0.48 seconds (average 0.008800, setup 0.00)

Prob1E: 4 iterations in 0.04 seconds (average 0.010000, setup 0.00)

yes = 440, no = 162908, maybe = 42291

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=205639, nc=71089, nnz=192802, k=4] [2.5 MB]
Creating vector for yes... [1.6 MB]
Allocating iteration vectors... [2 x 1.6 MB]
TOTAL: [7.2 MB]

Starting iterations...

Iterative method: 91 iterations in 0.31 seconds (average 0.001495, setup 0.18)

Value in the initial state: 5.777139473051201E-5

Time for model checking: 1.022 seconds.

Result: 5.777139473051201E-5 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmin=? [ F "MissionTimeUp" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=0,Objy1=3,Objx2=1,Objy2=0

Prob0E: 62 iterations in 0.44 seconds (average 0.007032, setup 0.00)

Prob1A: 3 iterations in 0.04 seconds (average 0.013333, setup 0.00)

yes = 440, no = 163160, maybe = 42039

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=205639, nc=70637, nnz=191838, k=4] [2.5 MB]
Creating vector for yes... [1.6 MB]
Allocating iteration vectors... [2 x 1.6 MB]
TOTAL: [7.2 MB]

Starting iterations...

Iterative method: 109 iterations in 0.34 seconds (average 0.001468, setup 0.18)

Value in the initial state: 2.331768437919062E-7

Time for model checking: 1.056 seconds.

Result: 2.331768437919062E-7 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmax=? [ F "Crash" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=0,Objy1=3,Objx2=1,Objy2=0

Prob0A: 3 iterations in 0.11 seconds (average 0.036000, setup 0.00)

Prob1E: 219 iterations in 3.39 seconds (average 0.015470, setup 0.00)

yes = 100718, no = 1371, maybe = 103550

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=205639, nc=166370, nnz=435423, k=4] [5.3 MB]
Creating vector for yes... [1.6 MB]
Allocating iteration vectors... [2 x 1.6 MB]
TOTAL: [10.0 MB]

Starting iterations...

Iterative method: 93 iterations in 0.50 seconds (average 0.001978, setup 0.32)

Value in the initial state: 0.020451966060717346

Time for model checking: 4.66 seconds.

Result: 0.020451966060717346 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmin=? [ F "Crash" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=0,Objy1=3,Objx2=1,Objy2=0

Prob0E: 3 iterations in 0.07 seconds (average 0.024000, setup 0.00)

Prob1A: 55 iterations in 0.63 seconds (average 0.011491, setup 0.00)

yes = 100718, no = 1371, maybe = 103550

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=205639, nc=166370, nnz=435423, k=4] [5.3 MB]
Creating vector for yes... [1.6 MB]
Allocating iteration vectors... [2 x 1.6 MB]
TOTAL: [10.0 MB]

Starting iterations...

Iterative method: 94 iterations in 0.45 seconds (average 0.001957, setup 0.26)

Value in the initial state: 0.017089331336781036

Time for model checking: 1.203 seconds.

Result: 0.017089331336781036 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmax=? [ F "AllAreaSearched" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=0,Objy1=3,Objx2=1,Objy2=0

Prob0A: 57 iterations in 0.65 seconds (average 0.011439, setup 0.00)

Prob1E: 4 iterations in 0.05 seconds (average 0.012000, setup 0.00)

yes = 220, no = 137589, maybe = 67830

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=205639, nc=106627, nnz=284393, k=2] [3.6 MB]
Creating vector for yes... [1.6 MB]
Allocating iteration vectors... [2 x 1.6 MB]
TOTAL: [8.3 MB]

Starting iterations...

Iterative method: 93 iterations in 0.38 seconds (average 0.001634, setup 0.23)

Value in the initial state: 0.9747852560577082

Time for model checking: 1.385 seconds.

Result: 0.9747852560577082 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmin=? [ F "AllAreaSearched" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=0,Objy1=3,Objx2=1,Objy2=0

Prob0E: 70 iterations in 0.60 seconds (average 0.008629, setup 0.00)

Prob1A: 3 iterations in 0.04 seconds (average 0.014667, setup 0.00)

yes = 220, no = 139889, maybe = 65530

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=205639, nc=102274, nnz=271323, k=2] [3.4 MB]
Creating vector for yes... [1.6 MB]
Allocating iteration vectors... [2 x 1.6 MB]
TOTAL: [8.1 MB]

Starting iterations...

Iterative method: 99 iterations in 0.39 seconds (average 0.001697, setup 0.22)

Value in the initial state: 0.25238953806504716

Time for model checking: 1.497 seconds.

Result: 0.25238953806504716 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmax=? [ F "MissionTimeUp"|"AllAreaSearched"|"Crash" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=0,Objy1=3,Objx2=1,Objy2=0

Prob0A: 3 iterations in 0.09 seconds (average 0.029333, setup 0.00)

Prob1E: 280 iterations in 3.86 seconds (average 0.013786, setup 0.00)

yes = 177596, no = 711, maybe = 27332

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=205639, nc=45639, nnz=125476, k=2] [1.7 MB]
Creating vector for yes... [1.6 MB]
Allocating iteration vectors... [2 x 1.6 MB]
TOTAL: [6.4 MB]

Starting iterations...

Iterative method: 82 iterations in 0.33 seconds (average 0.001317, setup 0.22)

Value in the initial state: 0.9926969081332849

Time for model checking: 4.745 seconds.

Result: 0.9926969081332849 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmin=? [ F "MissionTimeUp"|"AllAreaSearched"|"Crash" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=0,Objy1=3,Objx2=1,Objy2=0

Prob0E: 3 iterations in 0.07 seconds (average 0.022667, setup 0.00)

Prob1A: 63 iterations in 0.73 seconds (average 0.011556, setup 0.00)

yes = 169558, no = 711, maybe = 35370

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=205639, nc=58781, nnz=161291, k=2] [2.1 MB]
Creating vector for yes... [1.6 MB]
Allocating iteration vectors... [2 x 1.6 MB]
TOTAL: [6.8 MB]

Starting iterations...

Iterative method: 90 iterations in 0.33 seconds (average 0.001422, setup 0.20)

Value in the initial state: 0.2714981683958352

Time for model checking: 1.292 seconds.

Result: 0.2714981683958352 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"drop"}max=? [ F "deadlock" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=0,Objy1=3,Objx2=1,Objy2=0

Prob0E: 4 iterations in 0.10 seconds (average 0.025000, setup 0.00)

Prob1A: 1 iterations in 0.02 seconds (average 0.016000, setup 0.00)

goal = 1, inf = 0, maybe = 205638

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=205639, nc=268458, nnz=537511, k=4] [8.0 MB]
Building sparse matrix (transition rewards)... [n=205639, nc=268458, nnz=0, k=4] [1.8 MB]
Creating vector for state rewards... [1.6 MB]
Creating vector for inf... [1.6 MB]
Allocating iteration vectors... [2 x 1.6 MB]
TOTAL: [16.0 MB]

Starting iterations...

Iterative method: 96 iterations in 0.70 seconds (average 0.003042, setup 0.40)

Value in the initial state: 0.1870028236295315

Time for model checking: 0.829 seconds.

Result: 0.1870028236295315 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"drop"}min=? [ F "deadlock" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=0,Objy1=3,Objx2=1,Objy2=0

Prob0A: 4 iterations in 0.03 seconds (average 0.008000, setup 0.00)

Prob1E: 5 iterations in 0.05 seconds (average 0.009600, setup 0.00)

Warning: PRISM hasn't checked for zero-reward loops.
Your minimum rewards may be too low...

goal = 1, inf = 0, maybe = 205638

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=205639, nc=268458, nnz=537511, k=4] [8.0 MB]
Building sparse matrix (transition rewards)... [n=205639, nc=268458, nnz=0, k=4] [1.8 MB]
Creating vector for state rewards... [1.6 MB]
Creating vector for inf... [1.6 MB]
Allocating iteration vectors... [2 x 1.6 MB]
TOTAL: [16.0 MB]

Starting iterations...

Iterative method: 88 iterations in 0.56 seconds (average 0.003091, setup 0.29)

Value in the initial state: 0.15074469647376273

Time for model checking: 0.655 seconds.

Result: 0.15074469647376273 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"charge"}max=? [ F "deadlock" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=0,Objy1=3,Objx2=1,Objy2=0

Prob0E: 4 iterations in 0.03 seconds (average 0.007000, setup 0.00)

Prob1A: 1 iterations in 0.01 seconds (average 0.008000, setup 0.00)

goal = 1, inf = 0, maybe = 205638

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=205639, nc=268458, nnz=537511, k=4] [8.0 MB]
Building sparse matrix (transition rewards)... [n=205639, nc=268458, nnz=6348, k=4] [1.9 MB]
Creating vector for state rewards... [1.6 MB]
Creating vector for inf... [1.6 MB]
Allocating iteration vectors... [2 x 1.6 MB]
TOTAL: [16.1 MB]

Starting iterations...

Iterative method: 93 iterations in 0.58 seconds (average 0.003011, setup 0.30)

Value in the initial state: 0.9766474771736108

Time for model checking: 0.629 seconds.

Result: 0.9766474771736108 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"charge"}min=? [ F "deadlock" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=0,Objy1=3,Objx2=1,Objy2=0

Prob0A: 4 iterations in 0.03 seconds (average 0.007000, setup 0.00)

Prob1E: 5 iterations in 0.04 seconds (average 0.008800, setup 0.00)

Warning: PRISM hasn't checked for zero-reward loops.
Your minimum rewards may be too low...

goal = 1, inf = 0, maybe = 205638

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=205639, nc=268458, nnz=537511, k=4] [8.0 MB]
Building sparse matrix (transition rewards)... [n=205639, nc=268458, nnz=6348, k=4] [1.9 MB]
Creating vector for state rewards... [1.6 MB]
Creating vector for inf... [1.6 MB]
Allocating iteration vectors... [2 x 1.6 MB]
TOTAL: [16.1 MB]

Starting iterations...

Iterative method: 99 iterations in 0.60 seconds (average 0.003071, setup 0.30)

Value in the initial state: 0.23082713122001716

Time for model checking: 0.689 seconds.

Result: 0.23082713122001716 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"time"}max=? [ F "deadlock" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=0,Objy1=3,Objx2=1,Objy2=0

Prob0E: 4 iterations in 0.03 seconds (average 0.007000, setup 0.00)

Prob1A: 1 iterations in 0.00 seconds (average 0.004000, setup 0.00)

goal = 1, inf = 0, maybe = 205638

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=205639, nc=268458, nnz=537511, k=4] [8.0 MB]
Building sparse matrix (transition rewards)... [n=205639, nc=268458, nnz=102087, k=4] [3.0 MB]
Creating vector for state rewards... [1.6 MB]
Creating vector for inf... [1.6 MB]
Allocating iteration vectors... [2 x 1.6 MB]
TOTAL: [17.2 MB]

Starting iterations...

Iterative method: 94 iterations in 0.63 seconds (average 0.003064, setup 0.34)

Value in the initial state: 113.42609196359318

Time for model checking: 0.674 seconds.

Result: 113.42609196359318 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"time"}min=? [ F "deadlock" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=0,Objy1=3,Objx2=1,Objy2=0

Prob0A: 4 iterations in 0.02 seconds (average 0.006000, setup 0.00)

Prob1E: 5 iterations in 0.04 seconds (average 0.008800, setup 0.00)

Warning: PRISM hasn't checked for zero-reward loops.
Your minimum rewards may be too low...

goal = 1, inf = 0, maybe = 205638

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=205639, nc=268458, nnz=537511, k=4] [8.0 MB]
Building sparse matrix (transition rewards)... [n=205639, nc=268458, nnz=102087, k=4] [3.0 MB]
Creating vector for state rewards... [1.6 MB]
Creating vector for inf... [1.6 MB]
Allocating iteration vectors... [2 x 1.6 MB]
TOTAL: [17.2 MB]

Starting iterations...

Iterative method: 105 iterations in 0.66 seconds (average 0.003086, setup 0.34)

Value in the initial state: 94.87760720725989

Time for model checking: 0.741 seconds.

Result: 94.87760720725989 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"success"}max=? [ F "deadlock" ]/Pmin=? [ F "MissionSuccessful" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=0,Objy1=3,Objx2=1,Objy2=0

Prob0E: 4 iterations in 0.02 seconds (average 0.006000, setup 0.00)

Prob1A: 1 iterations in 0.00 seconds (average 0.004000, setup 0.00)

goal = 1, inf = 0, maybe = 205638

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=205639, nc=268458, nnz=537511, k=4] [8.0 MB]
Building sparse matrix (transition rewards)... [n=205639, nc=268458, nnz=1403, k=4] [1.8 MB]
Creating vector for state rewards... [1.6 MB]
Creating vector for inf... [1.6 MB]
Allocating iteration vectors... [2 x 1.6 MB]
TOTAL: [16.1 MB]

Starting iterations...

Iterative method: 96 iterations in 0.58 seconds (average 0.002917, setup 0.30)

Prob0E: 73 iterations in 0.41 seconds (average 0.005589, setup 0.00)

Prob1A: 3 iterations in 0.04 seconds (average 0.012000, setup 0.00)

yes = 710, no = 177597, maybe = 27332

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=205639, nc=45639, nnz=125476, k=2] [1.7 MB]
Creating vector for yes... [1.6 MB]
Allocating iteration vectors... [2 x 1.6 MB]
TOTAL: [6.4 MB]

Starting iterations...

Iterative method: 94 iterations in 0.27 seconds (average 0.001277, setup 0.15)

Value in the initial state: 11751.032674310103

Time for model checking: 1.342 seconds.

Result: 11751.032674310103 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"success"}min=? [ F "deadlock" ]/Pmax=? [ F "MissionSuccessful" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=0,Objy1=3,Objx2=1,Objy2=0

Prob0A: 4 iterations in 0.06 seconds (average 0.015000, setup 0.00)

Prob1E: 5 iterations in 0.03 seconds (average 0.006400, setup 0.00)

Warning: PRISM hasn't checked for zero-reward loops.
Your minimum rewards may be too low...

goal = 1, inf = 0, maybe = 205638

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=205639, nc=268458, nnz=537511, k=4] [8.0 MB]
Building sparse matrix (transition rewards)... [n=205639, nc=268458, nnz=1403, k=4] [1.8 MB]
Creating vector for state rewards... [1.6 MB]
Creating vector for inf... [1.6 MB]
Allocating iteration vectors... [2 x 1.6 MB]
TOTAL: [16.1 MB]

Starting iterations...

Iterative method: 99 iterations in 0.61 seconds (average 0.002869, setup 0.33)

Prob0A: 63 iterations in 0.29 seconds (average 0.004635, setup 0.00)

Prob1E: 4 iterations in 0.03 seconds (average 0.008000, setup 0.00)

yes = 710, no = 169559, maybe = 35370

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=205639, nc=58781, nnz=161291, k=2] [2.1 MB]
Creating vector for yes... [1.6 MB]
Allocating iteration vectors... [2 x 1.6 MB]
TOTAL: [6.8 MB]

Starting iterations...

Iterative method: 98 iterations in 0.28 seconds (average 0.001306, setup 0.15)

Value in the initial state: 3.501859490188935

Time for model checking: 1.305 seconds.

Result: 3.501859490188935 (value in the initial state)

---------------------------------------------------------------------

Note: There were 16 warnings during computation.

