PRISM
=====

Version: 4.3
Date: Tue Aug 02 12:26:30 BST 2016
Hostname: eglinton
Memory limits: cudd=1g, java(heap)=981.5m
Command line: prism uav_sim_ft_mdp.prism -s uav_mdp.props -const 'Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=1,Objy1=3,Objx2=3,Objy2=1,'

Parsing model file "uav_sim_ft_mdp.prism"...

Parsing properties file "uav_mdp.props"...

18 properties:
(1) Pmax=? [ F "MissionSuccessful" ]
(2) Pmin=? [ F "MissionSuccessful" ]
(3) Pmax=? [ F "MissionTimeUp" ]
(4) Pmin=? [ F "MissionTimeUp" ]
(5) Pmax=? [ F "Crash" ]
(6) Pmin=? [ F "Crash" ]
(7) Pmax=? [ F "AllAreaSearched" ]
(8) Pmin=? [ F "AllAreaSearched" ]
(9) Pmax=? [ F "MissionTimeUp"|"AllAreaSearched"|"Crash" ]
(10) Pmin=? [ F "MissionTimeUp"|"AllAreaSearched"|"Crash" ]
(11) R{"drop"}max=? [ F "deadlock" ]
(12) R{"drop"}min=? [ F "deadlock" ]
(13) R{"charge"}max=? [ F "deadlock" ]
(14) R{"charge"}min=? [ F "deadlock" ]
(15) R{"time"}max=? [ F "deadlock" ]
(16) R{"time"}min=? [ F "deadlock" ]
(17) R{"success"}max=? [ F "deadlock" ]/Pmin=? [ F "MissionSuccessful" ]
(18) R{"success"}min=? [ F "deadlock" ]/Pmax=? [ F "MissionSuccessful" ]

Type:        MDP
Modules:     Obj Move Robot T 
Variables:   NoOfObjs posx posy s1 retx rety b t c 

---------------------------------------------------------------------

Model checking: Pmax=? [ F "MissionSuccessful" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=1,Objy1=3,Objx2=3,Objy2=1

Building model...
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=1,Objy1=3,Objx2=3,Objy2=1

Warning: Guard for command 7 of module "Move" is never satisfied.

Warning: Guard for command 8 of module "Move" is never satisfied.

Warning: Guard for command 9 of module "Move" is never satisfied.

Warning: Guard for command 7 of module "Robot" is never satisfied.

Warning: Update 2 of command 3 of module "Robot" doesn't do anything ("(s1'=13)", line 249, column 44)

Warning: Update 2 of command 27 of module "T" doesn't do anything ("(c'=1)", line 372, column 108)

Warning: Guard for command 28 of module "T" is never satisfied.

Warning: Update 2 of command 9 of module "T" doesn't do anything ("(c'=1)", line 349, column 112)

Warning: Guard for command 10 of module "T" is never satisfied.

Warning: Update 2 of command 51 of module "T" doesn't do anything ("(c'=1)", line 403, column 113)

Warning: Guard for command 52 of module "T" is never satisfied.

Computing reachable states...

Reachability (BFS): 81 iterations in 0.76 seconds (average 0.009383, setup 0.00)

Time for model construction: 1.956 seconds.

Warning: Deadlocks detected and fixed in 1 states

Type:        MDP
States:      172369 (1 initial)
Transitions: 446432
Choices:     226670

Transition matrix: 166243 nodes (76 terminal), 446432 minterms, vars: 34r/34c/18nd

Prob0A: 54 iterations in 0.40 seconds (average 0.007407, setup 0.00)

Prob1E: 4 iterations in 0.06 seconds (average 0.014000, setup 0.00)

yes = 952, no = 144236, maybe = 27181

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=172369, nc=45629, nnz=124690, k=2] [1.6 MB]
Creating vector for yes... [1.3 MB]
Allocating iteration vectors... [2 x 1.3 MB]
TOTAL: [5.6 MB]

Starting iterations...

Iterative method: 94 iterations in 0.31 seconds (average 0.001149, setup 0.20)

Value in the initial state: 0.9755100828246209

Time for model checking: 0.797 seconds.

Result: 0.9755100828246209 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmin=? [ F "MissionSuccessful" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=1,Objy1=3,Objx2=3,Objy2=1

Prob0E: 67 iterations in 0.50 seconds (average 0.007463, setup 0.00)

Prob1A: 3 iterations in 0.05 seconds (average 0.017333, setup 0.00)

yes = 952, no = 148331, maybe = 23086

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=172369, nc=38764, nnz=104942, k=2] [1.4 MB]
Creating vector for yes... [1.3 MB]
Allocating iteration vectors... [2 x 1.3 MB]
TOTAL: [5.3 MB]

Starting iterations...

Iterative method: 88 iterations in 0.26 seconds (average 0.001136, setup 0.16)

Value in the initial state: 0.933553202528308

Time for model checking: 0.842 seconds.

Result: 0.933553202528308 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmax=? [ F "MissionTimeUp" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=1,Objy1=3,Objx2=3,Objy2=1

Prob0A: 62 iterations in 0.53 seconds (average 0.008581, setup 0.00)

Prob1E: 4 iterations in 0.04 seconds (average 0.010000, setup 0.00)

yes = 332, no = 100184, maybe = 71853

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=172369, nc=115466, nnz=307792, k=4] [3.8 MB]
Creating vector for yes... [1.3 MB]
Allocating iteration vectors... [2 x 1.3 MB]
TOTAL: [7.7 MB]

Starting iterations...

Iterative method: 91 iterations in 0.37 seconds (average 0.001495, setup 0.24)

Value in the initial state: 8.172792301218474E-4

Time for model checking: 0.974 seconds.

Result: 8.172792301218474E-4 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmin=? [ F "MissionTimeUp" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=1,Objy1=3,Objx2=3,Objy2=1

Prob0E: 69 iterations in 0.68 seconds (average 0.009797, setup 0.00)

Prob1A: 3 iterations in 0.04 seconds (average 0.014667, setup 0.00)

yes = 332, no = 100261, maybe = 71776

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=172369, nc=115345, nnz=307429, k=4] [3.8 MB]
Creating vector for yes... [1.3 MB]
Allocating iteration vectors... [2 x 1.3 MB]
TOTAL: [7.7 MB]

Starting iterations...

Iterative method: 106 iterations in 0.38 seconds (average 0.001434, setup 0.23)

Value in the initial state: 4.68847112292069E-6

Time for model checking: 1.134 seconds.

Result: 4.68847112292069E-6 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmax=? [ F "Crash" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=1,Objy1=3,Objx2=3,Objy2=1

Prob0A: 3 iterations in 0.07 seconds (average 0.024000, setup 0.00)

Prob1E: 256 iterations in 2.99 seconds (average 0.011687, setup 0.00)

yes = 84559, no = 2587, maybe = 85223

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=172369, nc=139524, nnz=359286, k=4] [4.4 MB]
Creating vector for yes... [1.3 MB]
Allocating iteration vectors... [2 x 1.3 MB]
TOTAL: [8.4 MB]

Starting iterations...

Iterative method: 90 iterations in 0.40 seconds (average 0.001556, setup 0.26)

Value in the initial state: 0.06287726131190546

Time for model checking: 3.509 seconds.

Result: 0.06287726131190546 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmin=? [ F "Crash" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=1,Objy1=3,Objx2=3,Objy2=1

Prob0E: 3 iterations in 0.06 seconds (average 0.018667, setup 0.00)

Prob1A: 53 iterations in 0.44 seconds (average 0.008302, setup 0.00)

yes = 84559, no = 2587, maybe = 85223

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=172369, nc=139524, nnz=359286, k=4] [4.4 MB]
Creating vector for yes... [1.3 MB]
Allocating iteration vectors... [2 x 1.3 MB]
TOTAL: [8.4 MB]

Starting iterations...

Iterative method: 94 iterations in 0.33 seconds (average 0.001617, setup 0.18)

Value in the initial state: 0.02253198976458105

Time for model checking: 0.848 seconds.

Result: 0.02253198976458105 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmax=? [ F "AllAreaSearched" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=1,Objy1=3,Objx2=3,Objy2=1

Prob0A: 53 iterations in 0.35 seconds (average 0.006642, setup 0.00)

Prob1E: 4 iterations in 0.05 seconds (average 0.013000, setup 0.00)

yes = 1302, no = 127309, maybe = 43758

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=172369, nc=70952, nnz=188546, k=2] [2.4 MB]
Creating vector for yes... [1.3 MB]
Allocating iteration vectors... [2 x 1.3 MB]
TOTAL: [6.3 MB]

Starting iterations...

Iterative method: 91 iterations in 0.30 seconds (average 0.001319, setup 0.18)

Value in the initial state: 0.003200876517000469

Time for model checking: 0.717 seconds.

Result: 0.003200876517000469 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmin=? [ F "AllAreaSearched" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=1,Objy1=3,Objx2=3,Objy2=1

Prob0E: 67 iterations in 0.44 seconds (average 0.006507, setup 0.00)

Prob1A: 3 iterations in 0.04 seconds (average 0.012000, setup 0.00)

yes = 1302, no = 132985, maybe = 38082

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=172369, nc=61695, nnz=162292, k=2] [2.1 MB]
Creating vector for yes... [1.3 MB]
Allocating iteration vectors... [2 x 1.3 MB]
TOTAL: [6.0 MB]

Starting iterations...

Iterative method: 90 iterations in 0.24 seconds (average 0.001244, setup 0.13)

Value in the initial state: 0.0012345695842390081

Time for model checking: 0.732 seconds.

Result: 0.0012345695842390081 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmax=? [ F "MissionTimeUp"|"AllAreaSearched"|"Crash" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=1,Objy1=3,Objx2=3,Objy2=1

Prob0A: 3 iterations in 0.08 seconds (average 0.025333, setup 0.00)

Prob1E: 260 iterations in 3.33 seconds (average 0.012815, setup 0.00)

yes = 148330, no = 953, maybe = 23086

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=172369, nc=38764, nnz=104942, k=2] [1.4 MB]
Creating vector for yes... [1.3 MB]
Allocating iteration vectors... [2 x 1.3 MB]
TOTAL: [5.3 MB]

Starting iterations...

Iterative method: 85 iterations in 0.25 seconds (average 0.001035, setup 0.16)

Value in the initial state: 0.06644679604565361

Time for model checking: 3.679 seconds.

Result: 0.06644679604565361 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmin=? [ F "MissionTimeUp"|"AllAreaSearched"|"Crash" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=1,Objy1=3,Objx2=3,Objy2=1

Prob0E: 3 iterations in 0.04 seconds (average 0.014667, setup 0.00)

Prob1A: 54 iterations in 0.42 seconds (average 0.007852, setup 0.00)

yes = 144235, no = 953, maybe = 27181

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=172369, nc=45629, nnz=124690, k=2] [1.6 MB]
Creating vector for yes... [1.3 MB]
Allocating iteration vectors... [2 x 1.3 MB]
TOTAL: [5.6 MB]

Starting iterations...

Iterative method: 92 iterations in 0.23 seconds (average 0.001174, setup 0.12)

Value in the initial state: 0.024489216749532954

Time for model checking: 0.726 seconds.

Result: 0.024489216749532954 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"drop"}max=? [ F "deadlock" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=1,Objy1=3,Objx2=3,Objy2=1

Prob0E: 4 iterations in 0.04 seconds (average 0.010000, setup 0.00)

Prob1A: 1 iterations in 0.00 seconds (average 0.004000, setup 0.00)

goal = 1, inf = 0, maybe = 172368

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=172369, nc=226669, nnz=446431, k=4] [6.6 MB]
Building sparse matrix (transition rewards)... [n=172369, nc=226669, nnz=0, k=4] [1.5 MB]
Creating vector for state rewards... [1.3 MB]
Creating vector for inf... [1.3 MB]
Allocating iteration vectors... [2 x 1.3 MB]
TOTAL: [13.4 MB]

Starting iterations...

Iterative method: 90 iterations in 0.54 seconds (average 0.002489, setup 0.32)

Value in the initial state: 0.18015612199815736

Time for model checking: 0.62 seconds.

Result: 0.18015612199815736 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"drop"}min=? [ F "deadlock" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=1,Objy1=3,Objx2=3,Objy2=1

Prob0A: 4 iterations in 0.03 seconds (average 0.008000, setup 0.00)

Prob1E: 5 iterations in 0.04 seconds (average 0.008800, setup 0.00)

Warning: PRISM hasn't checked for zero-reward loops.
Your minimum rewards may be too low...

goal = 1, inf = 0, maybe = 172368

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=172369, nc=226669, nnz=446431, k=4] [6.6 MB]
Building sparse matrix (transition rewards)... [n=172369, nc=226669, nnz=0, k=4] [1.5 MB]
Creating vector for state rewards... [1.3 MB]
Creating vector for inf... [1.3 MB]
Allocating iteration vectors... [2 x 1.3 MB]
TOTAL: [13.4 MB]

Starting iterations...

Iterative method: 82 iterations in 0.45 seconds (average 0.002390, setup 0.25)

Value in the initial state: 0.13583727666745873

Time for model checking: 0.536 seconds.

Result: 0.13583727666745873 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"charge"}max=? [ F "deadlock" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=1,Objy1=3,Objx2=3,Objy2=1

Prob0E: 4 iterations in 0.02 seconds (average 0.005000, setup 0.00)

Prob1A: 1 iterations in 0.00 seconds (average 0.004000, setup 0.00)

goal = 1, inf = 0, maybe = 172368

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=172369, nc=226669, nnz=446431, k=4] [6.6 MB]
Building sparse matrix (transition rewards)... [n=172369, nc=226669, nnz=4730, k=4] [1.6 MB]
Creating vector for state rewards... [1.3 MB]
Creating vector for inf... [1.3 MB]
Allocating iteration vectors... [2 x 1.3 MB]
TOTAL: [13.5 MB]

Starting iterations...

Iterative method: 87 iterations in 0.46 seconds (average 0.002483, setup 0.25)

Value in the initial state: 0.9796804452408431

Time for model checking: 0.503 seconds.

Result: 0.9796804452408431 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"charge"}min=? [ F "deadlock" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=1,Objy1=3,Objx2=3,Objy2=1

Prob0A: 4 iterations in 0.02 seconds (average 0.006000, setup 0.00)

Prob1E: 5 iterations in 0.04 seconds (average 0.008800, setup 0.00)

Warning: PRISM hasn't checked for zero-reward loops.
Your minimum rewards may be too low...

goal = 1, inf = 0, maybe = 172368

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=172369, nc=226669, nnz=446431, k=4] [6.6 MB]
Building sparse matrix (transition rewards)... [n=172369, nc=226669, nnz=4730, k=4] [1.6 MB]
Creating vector for state rewards... [1.3 MB]
Creating vector for inf... [1.3 MB]
Allocating iteration vectors... [2 x 1.3 MB]
TOTAL: [13.5 MB]

Starting iterations...

Iterative method: 94 iterations in 0.48 seconds (average 0.002511, setup 0.25)

Value in the initial state: 0.12198127742252823

Time for model checking: 0.564 seconds.

Result: 0.12198127742252823 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"time"}max=? [ F "deadlock" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=1,Objy1=3,Objx2=3,Objy2=1

Prob0E: 4 iterations in 0.02 seconds (average 0.006000, setup 0.00)

Prob1A: 1 iterations in 0.00 seconds (average 0.004000, setup 0.00)

goal = 1, inf = 0, maybe = 172368

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=172369, nc=226669, nnz=446431, k=4] [6.6 MB]
Building sparse matrix (transition rewards)... [n=172369, nc=226669, nnz=87144, k=4] [2.5 MB]
Creating vector for state rewards... [1.3 MB]
Creating vector for inf... [1.3 MB]
Allocating iteration vectors... [2 x 1.3 MB]
TOTAL: [14.4 MB]

Starting iterations...

Iterative method: 92 iterations in 0.51 seconds (average 0.002478, setup 0.28)

Value in the initial state: 345.4563819860021

Time for model checking: 0.554 seconds.

Result: 345.4563819860021 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"time"}min=? [ F "deadlock" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=1,Objy1=3,Objx2=3,Objy2=1

Prob0A: 4 iterations in 0.02 seconds (average 0.005000, setup 0.00)

Prob1E: 5 iterations in 0.05 seconds (average 0.009600, setup 0.00)

Warning: PRISM hasn't checked for zero-reward loops.
Your minimum rewards may be too low...

goal = 1, inf = 0, maybe = 172368

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=172369, nc=226669, nnz=446431, k=4] [6.6 MB]
Building sparse matrix (transition rewards)... [n=172369, nc=226669, nnz=87144, k=4] [2.5 MB]
Creating vector for state rewards... [1.3 MB]
Creating vector for inf... [1.3 MB]
Allocating iteration vectors... [2 x 1.3 MB]
TOTAL: [14.4 MB]

Starting iterations...

Iterative method: 100 iterations in 0.53 seconds (average 0.002560, setup 0.28)

Value in the initial state: 124.62717483874108

Time for model checking: 0.611 seconds.

Result: 124.62717483874108 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"success"}max=? [ F "deadlock" ]/Pmin=? [ F "MissionSuccessful" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=1,Objy1=3,Objx2=3,Objy2=1

Prob0E: 4 iterations in 0.02 seconds (average 0.006000, setup 0.00)

Prob1A: 1 iterations in 0.00 seconds (average 0.004000, setup 0.00)

goal = 1, inf = 0, maybe = 172368

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=172369, nc=226669, nnz=446431, k=4] [6.6 MB]
Building sparse matrix (transition rewards)... [n=172369, nc=226669, nnz=1876, k=4] [1.5 MB]
Creating vector for state rewards... [1.3 MB]
Creating vector for inf... [1.3 MB]
Allocating iteration vectors... [2 x 1.3 MB]
TOTAL: [13.4 MB]

Starting iterations...

Iterative method: 95 iterations in 0.48 seconds (average 0.002442, setup 0.24)

Prob0E: 67 iterations in 0.39 seconds (average 0.005851, setup 0.00)

Prob1A: 3 iterations in 0.04 seconds (average 0.013333, setup 0.00)

yes = 952, no = 148331, maybe = 23086

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=172369, nc=38764, nnz=104942, k=2] [1.4 MB]
Creating vector for yes... [1.3 MB]
Allocating iteration vectors... [2 x 1.3 MB]
TOTAL: [5.3 MB]

Starting iterations...

Iterative method: 88 iterations in 0.20 seconds (average 0.001091, setup 0.10)

Value in the initial state: 361.62924659099343

Time for model checking: 1.152 seconds.

Result: 361.62924659099343 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"success"}min=? [ F "deadlock" ]/Pmax=? [ F "MissionSuccessful" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=1,Objy1=3,Objx2=3,Objy2=1

Prob0A: 4 iterations in 0.06 seconds (average 0.015000, setup 0.00)

Prob1E: 5 iterations in 0.05 seconds (average 0.009600, setup 0.00)

Warning: PRISM hasn't checked for zero-reward loops.
Your minimum rewards may be too low...

goal = 1, inf = 0, maybe = 172368

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=172369, nc=226669, nnz=446431, k=4] [6.6 MB]
Building sparse matrix (transition rewards)... [n=172369, nc=226669, nnz=1876, k=4] [1.5 MB]
Creating vector for state rewards... [1.3 MB]
Creating vector for inf... [1.3 MB]
Allocating iteration vectors... [2 x 1.3 MB]
TOTAL: [13.4 MB]

Starting iterations...

Iterative method: 93 iterations in 0.49 seconds (average 0.002409, setup 0.27)

Prob0A: 54 iterations in 0.29 seconds (average 0.005333, setup 0.00)

Prob1E: 4 iterations in 0.03 seconds (average 0.008000, setup 0.00)

yes = 952, no = 144236, maybe = 27181

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=172369, nc=45629, nnz=124690, k=2] [1.6 MB]
Creating vector for yes... [1.3 MB]
Allocating iteration vectors... [2 x 1.3 MB]
TOTAL: [5.6 MB]

Starting iterations...

Iterative method: 94 iterations in 0.23 seconds (average 0.001106, setup 0.13)

Value in the initial state: 125.7975402741223

Time for model checking: 1.165 seconds.

Result: 125.7975402741223 (value in the initial state)

---------------------------------------------------------------------

Note: There were 16 warnings during computation.

