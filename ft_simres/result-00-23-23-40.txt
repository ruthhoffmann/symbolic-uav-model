PRISM
=====

Version: 4.3
Date: Tue Aug 02 12:28:17 BST 2016
Hostname: eglinton
Memory limits: cudd=1g, java(heap)=981.5m
Command line: prism uav_sim_ft_mdp.prism -s uav_mdp.props -const 'Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=2,Objy1=3,Objx2=4,Objy2=0,'

Parsing model file "uav_sim_ft_mdp.prism"...

Parsing properties file "uav_mdp.props"...

18 properties:
(1) Pmax=? [ F "MissionSuccessful" ]
(2) Pmin=? [ F "MissionSuccessful" ]
(3) Pmax=? [ F "MissionTimeUp" ]
(4) Pmin=? [ F "MissionTimeUp" ]
(5) Pmax=? [ F "Crash" ]
(6) Pmin=? [ F "Crash" ]
(7) Pmax=? [ F "AllAreaSearched" ]
(8) Pmin=? [ F "AllAreaSearched" ]
(9) Pmax=? [ F "MissionTimeUp"|"AllAreaSearched"|"Crash" ]
(10) Pmin=? [ F "MissionTimeUp"|"AllAreaSearched"|"Crash" ]
(11) R{"drop"}max=? [ F "deadlock" ]
(12) R{"drop"}min=? [ F "deadlock" ]
(13) R{"charge"}max=? [ F "deadlock" ]
(14) R{"charge"}min=? [ F "deadlock" ]
(15) R{"time"}max=? [ F "deadlock" ]
(16) R{"time"}min=? [ F "deadlock" ]
(17) R{"success"}max=? [ F "deadlock" ]/Pmin=? [ F "MissionSuccessful" ]
(18) R{"success"}min=? [ F "deadlock" ]/Pmax=? [ F "MissionSuccessful" ]

Type:        MDP
Modules:     Obj Move Robot T 
Variables:   NoOfObjs posx posy s1 retx rety b t c 

---------------------------------------------------------------------

Model checking: Pmax=? [ F "MissionSuccessful" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=2,Objy1=3,Objx2=4,Objy2=0

Building model...
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=2,Objy1=3,Objx2=4,Objy2=0

Warning: Guard for command 7 of module "Move" is never satisfied.

Warning: Guard for command 8 of module "Move" is never satisfied.

Warning: Guard for command 9 of module "Move" is never satisfied.

Warning: Guard for command 7 of module "Robot" is never satisfied.

Warning: Update 2 of command 3 of module "Robot" doesn't do anything ("(s1'=13)", line 249, column 44)

Warning: Update 2 of command 27 of module "T" doesn't do anything ("(c'=1)", line 372, column 108)

Warning: Guard for command 28 of module "T" is never satisfied.

Warning: Update 2 of command 9 of module "T" doesn't do anything ("(c'=1)", line 349, column 112)

Warning: Guard for command 10 of module "T" is never satisfied.

Warning: Update 2 of command 51 of module "T" doesn't do anything ("(c'=1)", line 403, column 113)

Warning: Guard for command 52 of module "T" is never satisfied.

Computing reachable states...

Reachability (BFS): 79 iterations in 0.70 seconds (average 0.008810, setup 0.00)

Time for model construction: 1.784 seconds.

Warning: Deadlocks detected and fixed in 1 states

Type:        MDP
States:      137892 (1 initial)
Transitions: 326478
Choices:     172407

Transition matrix: 159484 nodes (76 terminal), 326478 minterms, vars: 34r/34c/18nd

Prob0A: 53 iterations in 0.33 seconds (average 0.006189, setup 0.00)

Prob1E: 4 iterations in 0.05 seconds (average 0.013000, setup 0.00)

yes = 210, no = 131078, maybe = 6604

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=137892, nc=12114, nnz=36065, k=2] [569.1 KB]
Creating vector for yes... [1.1 MB]
Allocating iteration vectors... [2 x 1.1 MB]
TOTAL: [3.7 MB]

Starting iterations...

Iterative method: 85 iterations in 0.20 seconds (average 0.000800, setup 0.13)

Value in the initial state: 0.0016537167140888232

Time for model checking: 0.598 seconds.

Result: 0.0016537167140888232 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmin=? [ F "MissionSuccessful" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=2,Objy1=3,Objx2=4,Objy2=0

Prob0E: 22 iterations in 0.11 seconds (average 0.004909, setup 0.00)

Prob1A: 3 iterations in 0.03 seconds (average 0.009333, setup 0.00)

yes = 210, no = 132572, maybe = 5110

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=137892, nc=9352, nnz=27541, k=2] [466.5 KB]
Creating vector for yes... [1.1 MB]
Allocating iteration vectors... [2 x 1.1 MB]
TOTAL: [3.6 MB]

Starting iterations...

Iterative method: 42 iterations in 0.07 seconds (average 0.000762, setup 0.04)

Value in the initial state: 0.0

Time for model checking: 0.238 seconds.

Result: 0.0 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmax=? [ F "MissionTimeUp" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=2,Objy1=3,Objx2=4,Objy2=0

Prob0A: 57 iterations in 0.62 seconds (average 0.010877, setup 0.00)

Prob1E: 4 iterations in 0.06 seconds (average 0.015000, setup 0.00)

yes = 404, no = 97333, maybe = 40155

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=137892, nc=68440, nnz=187264, k=4] [2.3 MB]
Creating vector for yes... [1.1 MB]
Allocating iteration vectors... [2 x 1.1 MB]
TOTAL: [5.5 MB]

Starting iterations...

Iterative method: 93 iterations in 0.42 seconds (average 0.000989, setup 0.32)

Value in the initial state: 8.856505111538141E-5

Time for model checking: 1.123 seconds.

Result: 8.856505111538141E-5 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmin=? [ F "MissionTimeUp" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=2,Objy1=3,Objx2=4,Objy2=0

Prob0E: 65 iterations in 0.48 seconds (average 0.007323, setup 0.00)

Prob1A: 3 iterations in 0.04 seconds (average 0.014667, setup 0.00)

yes = 404, no = 97585, maybe = 39903

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=137892, nc=67988, nnz=186300, k=4] [2.3 MB]
Creating vector for yes... [1.1 MB]
Allocating iteration vectors... [2 x 1.1 MB]
TOTAL: [5.5 MB]

Starting iterations...

Iterative method: 107 iterations in 0.28 seconds (average 0.001084, setup 0.16)

Value in the initial state: 2.195477178076311E-7

Time for model checking: 0.819 seconds.

Result: 2.195477178076311E-7 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmax=? [ F "Crash" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=2,Objy1=3,Objx2=4,Objy2=0

Prob0A: 3 iterations in 0.08 seconds (average 0.025333, setup 0.00)

Prob1E: 176 iterations in 2.52 seconds (average 0.014341, setup 0.00)

yes = 67683, no = 1731, maybe = 68478

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=137892, nc=102993, nnz=257064, k=4] [3.2 MB]
Creating vector for yes... [1.1 MB]
Allocating iteration vectors... [2 x 1.1 MB]
TOTAL: [6.3 MB]

Starting iterations...

Iterative method: 90 iterations in 0.35 seconds (average 0.001244, setup 0.24)

Value in the initial state: 0.01685473699942997

Time for model checking: 2.973 seconds.

Result: 0.01685473699942997 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmin=? [ F "Crash" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=2,Objy1=3,Objx2=4,Objy2=0

Prob0E: 3 iterations in 0.06 seconds (average 0.020000, setup 0.00)

Prob1A: 42 iterations in 0.52 seconds (average 0.012286, setup 0.00)

yes = 67683, no = 1731, maybe = 68478

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=137892, nc=102993, nnz=257064, k=4] [3.2 MB]
Creating vector for yes... [1.1 MB]
Allocating iteration vectors... [2 x 1.1 MB]
TOTAL: [6.3 MB]

Starting iterations...

Iterative method: 81 iterations in 0.27 seconds (average 0.001185, setup 0.18)

Value in the initial state: 0.013127137143128048

Time for model checking: 0.866 seconds.

Result: 0.013127137143128048 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmax=? [ F "AllAreaSearched" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=2,Objy1=3,Objx2=4,Objy2=0

Prob0A: 49 iterations in 0.28 seconds (average 0.005714, setup 0.00)

Prob1E: 4 iterations in 0.03 seconds (average 0.008000, setup 0.00)

yes = 1116, no = 98950, maybe = 37826

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=137892, nc=51596, nnz=124576, k=2] [1.6 MB]
Creating vector for yes... [1.1 MB]
Allocating iteration vectors... [2 x 1.1 MB]
TOTAL: [4.8 MB]

Starting iterations...

Iterative method: 88 iterations in 0.23 seconds (average 0.001000, setup 0.14)

Value in the initial state: 0.9868706343042318

Time for model checking: 0.552 seconds.

Result: 0.9868706343042318 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmin=? [ F "AllAreaSearched" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=2,Objy1=3,Objx2=4,Objy2=0

Prob0E: 53 iterations in 0.33 seconds (average 0.006264, setup 0.00)

Prob1A: 3 iterations in 0.03 seconds (average 0.010667, setup 0.00)

yes = 1116, no = 101302, maybe = 35474

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=137892, nc=47139, nnz=111194, k=2] [1.4 MB]
Creating vector for yes... [1.1 MB]
Allocating iteration vectors... [2 x 1.1 MB]
TOTAL: [4.6 MB]

Starting iterations...

Iterative method: 89 iterations in 0.20 seconds (average 0.001034, setup 0.11)

Value in the initial state: 0.981492003324479

Time for model checking: 0.578 seconds.

Result: 0.981492003324479 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmax=? [ F "MissionTimeUp"|"AllAreaSearched"|"Crash" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=2,Objy1=3,Objx2=4,Objy2=0

Prob0A: 3 iterations in 0.08 seconds (average 0.028000, setup 0.00)

Prob1E: 84 iterations in 0.79 seconds (average 0.009381, setup 0.00)

yes = 132571, no = 211, maybe = 5110

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=137892, nc=9352, nnz=27541, k=2] [466.5 KB]
Creating vector for yes... [1.1 MB]
Allocating iteration vectors... [2 x 1.1 MB]
TOTAL: [3.6 MB]

Starting iterations...

Iterative method: 40 iterations in 0.08 seconds (average 0.000700, setup 0.05)

Value in the initial state: 1.0

Time for model checking: 0.943 seconds.

Result: 1.0 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmin=? [ F "MissionTimeUp"|"AllAreaSearched"|"Crash" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=2,Objy1=3,Objx2=4,Objy2=0

Prob0E: 3 iterations in 0.04 seconds (average 0.012000, setup 0.00)

Prob1A: 53 iterations in 0.30 seconds (average 0.005585, setup 0.00)

yes = 131077, no = 211, maybe = 6604

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=137892, nc=12114, nnz=36065, k=2] [569.1 KB]
Creating vector for yes... [1.1 MB]
Allocating iteration vectors... [2 x 1.1 MB]
TOTAL: [3.7 MB]

Starting iterations...

Iterative method: 55 iterations in 0.10 seconds (average 0.000727, setup 0.06)

Value in the initial state: 0.9983441639965432

Time for model checking: 0.443 seconds.

Result: 0.9983441639965432 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"drop"}max=? [ F "deadlock" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=2,Objy1=3,Objx2=4,Objy2=0

Prob0E: 4 iterations in 0.03 seconds (average 0.008000, setup 0.00)

Prob1A: 1 iterations in 0.01 seconds (average 0.008000, setup 0.00)

goal = 1, inf = 0, maybe = 137891

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=137892, nc=172406, nnz=326477, k=4] [4.9 MB]
Building sparse matrix (transition rewards)... [n=137892, nc=172406, nnz=0, k=4] [1.2 MB]
Creating vector for state rewards... [1.1 MB]
Creating vector for inf... [1.1 MB]
Allocating iteration vectors... [2 x 1.1 MB]
TOTAL: [10.3 MB]

Starting iterations...

Iterative method: 66 iterations in 0.38 seconds (average 0.001879, setup 0.26)

Value in the initial state: 0.11008204412525681

Time for model checking: 0.442 seconds.

Result: 0.11008204412525681 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"drop"}min=? [ F "deadlock" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=2,Objy1=3,Objx2=4,Objy2=0

Prob0A: 4 iterations in 0.02 seconds (average 0.006000, setup 0.00)

Prob1E: 5 iterations in 0.03 seconds (average 0.006400, setup 0.00)

Warning: PRISM hasn't checked for zero-reward loops.
Your minimum rewards may be too low...

goal = 1, inf = 0, maybe = 137891

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=137892, nc=172406, nnz=326477, k=4] [4.9 MB]
Building sparse matrix (transition rewards)... [n=137892, nc=172406, nnz=0, k=4] [1.2 MB]
Creating vector for state rewards... [1.1 MB]
Creating vector for inf... [1.1 MB]
Allocating iteration vectors... [2 x 1.1 MB]
TOTAL: [10.3 MB]

Starting iterations...

Iterative method: 58 iterations in 0.30 seconds (average 0.001862, setup 0.19)

Value in the initial state: 0.10978425318475343

Time for model checking: 0.371 seconds.

Result: 0.10978425318475343 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"charge"}max=? [ F "deadlock" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=2,Objy1=3,Objx2=4,Objy2=0

Prob0E: 4 iterations in 0.02 seconds (average 0.005000, setup 0.00)

Prob1A: 1 iterations in 0.00 seconds (average 0.004000, setup 0.00)

goal = 1, inf = 0, maybe = 137891

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=137892, nc=172406, nnz=326477, k=4] [4.9 MB]
Building sparse matrix (transition rewards)... [n=137892, nc=172406, nnz=2949, k=4] [1.2 MB]
Creating vector for state rewards... [1.1 MB]
Creating vector for inf... [1.1 MB]
Allocating iteration vectors... [2 x 1.1 MB]
TOTAL: [10.3 MB]

Starting iterations...

Iterative method: 77 iterations in 0.34 seconds (average 0.001818, setup 0.20)

Value in the initial state: 0.004176159029609847

Time for model checking: 0.373 seconds.

Result: 0.004176159029609847 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"charge"}min=? [ F "deadlock" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=2,Objy1=3,Objx2=4,Objy2=0

Prob0A: 4 iterations in 0.02 seconds (average 0.006000, setup 0.00)

Prob1E: 5 iterations in 0.04 seconds (average 0.007200, setup 0.00)

Warning: PRISM hasn't checked for zero-reward loops.
Your minimum rewards may be too low...

goal = 1, inf = 0, maybe = 137891

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=137892, nc=172406, nnz=326477, k=4] [4.9 MB]
Building sparse matrix (transition rewards)... [n=137892, nc=172406, nnz=2949, k=4] [1.2 MB]
Creating vector for state rewards... [1.1 MB]
Creating vector for inf... [1.1 MB]
Allocating iteration vectors... [2 x 1.1 MB]
TOTAL: [10.3 MB]

Starting iterations...

Iterative method: 88 iterations in 0.36 seconds (average 0.001909, setup 0.20)

Value in the initial state: 2.863121755005198E-4

Time for model checking: 0.431 seconds.

Result: 2.863121755005198E-4 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"time"}max=? [ F "deadlock" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=2,Objy1=3,Objx2=4,Objy2=0

Prob0E: 4 iterations in 0.04 seconds (average 0.011000, setup 0.00)

Prob1A: 1 iterations in 0.01 seconds (average 0.008000, setup 0.00)

goal = 1, inf = 0, maybe = 137891

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=137892, nc=172406, nnz=326477, k=4] [4.9 MB]
Building sparse matrix (transition rewards)... [n=137892, nc=172406, nnz=69412, k=4] [2.0 MB]
Creating vector for state rewards... [1.1 MB]
Creating vector for inf... [1.1 MB]
Allocating iteration vectors... [2 x 1.1 MB]
TOTAL: [11.1 MB]

Starting iterations...

Iterative method: 93 iterations in 0.55 seconds (average 0.003011, setup 0.27)

Value in the initial state: 93.4473409007762

Time for model checking: 0.61 seconds.

Result: 93.4473409007762 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"time"}min=? [ F "deadlock" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=2,Objy1=3,Objx2=4,Objy2=0

Prob0A: 4 iterations in 0.03 seconds (average 0.007000, setup 0.00)

Prob1E: 5 iterations in 0.05 seconds (average 0.009600, setup 0.00)

Warning: PRISM hasn't checked for zero-reward loops.
Your minimum rewards may be too low...

goal = 1, inf = 0, maybe = 137891

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=137892, nc=172406, nnz=326477, k=4] [4.9 MB]
Building sparse matrix (transition rewards)... [n=137892, nc=172406, nnz=69412, k=4] [2.0 MB]
Creating vector for state rewards... [1.1 MB]
Creating vector for inf... [1.1 MB]
Allocating iteration vectors... [2 x 1.1 MB]
TOTAL: [11.1 MB]

Starting iterations...

Iterative method: 95 iterations in 0.42 seconds (average 0.001979, setup 0.24)

Value in the initial state: 72.87647633842798

Time for model checking: 0.509 seconds.

Result: 72.87647633842798 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"success"}max=? [ F "deadlock" ]/Pmin=? [ F "MissionSuccessful" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=2,Objy1=3,Objx2=4,Objy2=0

Prob0E: 4 iterations in 0.02 seconds (average 0.005000, setup 0.00)

Prob1A: 1 iterations in 0.00 seconds (average 0.004000, setup 0.00)

goal = 1, inf = 0, maybe = 137891

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=137892, nc=172406, nnz=326477, k=4] [4.9 MB]
Building sparse matrix (transition rewards)... [n=137892, nc=172406, nnz=414, k=4] [1.2 MB]
Creating vector for state rewards... [1.1 MB]
Creating vector for inf... [1.1 MB]
Allocating iteration vectors... [2 x 1.1 MB]
TOTAL: [10.3 MB]

Starting iterations...

Iterative method: 82 iterations in 0.41 seconds (average 0.002049, setup 0.24)

Prob0E: 22 iterations in 0.08 seconds (average 0.003636, setup 0.00)

Prob1A: 3 iterations in 0.02 seconds (average 0.005333, setup 0.00)

yes = 210, no = 132572, maybe = 5110

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=137892, nc=9352, nnz=27541, k=2] [466.5 KB]
Creating vector for yes... [1.1 MB]
Allocating iteration vectors... [2 x 1.1 MB]
TOTAL: [3.6 MB]

Starting iterations...

Iterative method: 42 iterations in 0.06 seconds (average 0.000857, setup 0.03)

Value in the initial state: Infinity

Time for model checking: 0.624 seconds.

Result: Infinity (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"success"}min=? [ F "deadlock" ]/Pmax=? [ F "MissionSuccessful" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=2,Objy1=3,Objx2=4,Objy2=0

Prob0A: 4 iterations in 0.03 seconds (average 0.007000, setup 0.00)

Prob1E: 5 iterations in 0.04 seconds (average 0.008000, setup 0.00)

Warning: PRISM hasn't checked for zero-reward loops.
Your minimum rewards may be too low...

goal = 1, inf = 0, maybe = 137891

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=137892, nc=172406, nnz=326477, k=4] [4.9 MB]
Building sparse matrix (transition rewards)... [n=137892, nc=172406, nnz=414, k=4] [1.2 MB]
Creating vector for state rewards... [1.1 MB]
Creating vector for inf... [1.1 MB]
Allocating iteration vectors... [2 x 1.1 MB]
TOTAL: [10.3 MB]

Starting iterations...

Iterative method: 47 iterations in 0.36 seconds (average 0.002553, setup 0.24)

Prob0A: 53 iterations in 0.14 seconds (average 0.002717, setup 0.00)

Prob1E: 4 iterations in 0.02 seconds (average 0.005000, setup 0.00)

yes = 210, no = 131078, maybe = 6604

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=137892, nc=12114, nnz=36065, k=2] [569.1 KB]
Creating vector for yes... [1.1 MB]
Allocating iteration vectors... [2 x 1.1 MB]
TOTAL: [3.7 MB]

Starting iterations...

Iterative method: 85 iterations in 0.11 seconds (average 0.000800, setup 0.04)

Value in the initial state: 0.0

Time for model checking: 0.699 seconds.

Result: 0.0 (value in the initial state)

---------------------------------------------------------------------

Note: There were 16 warnings during computation.

