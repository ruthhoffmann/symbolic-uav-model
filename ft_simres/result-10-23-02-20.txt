PRISM
=====

Version: 4.3
Date: Tue Aug 02 12:35:49 BST 2016
Hostname: eglinton
Memory limits: cudd=1g, java(heap)=981.5m
Command line: prism uav_sim_ft_mdp.prism -s uav_mdp.props -const 'Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=0,Objy1=2,Objx2=2,Objy2=0,'

Parsing model file "uav_sim_ft_mdp.prism"...

Parsing properties file "uav_mdp.props"...

18 properties:
(1) Pmax=? [ F "MissionSuccessful" ]
(2) Pmin=? [ F "MissionSuccessful" ]
(3) Pmax=? [ F "MissionTimeUp" ]
(4) Pmin=? [ F "MissionTimeUp" ]
(5) Pmax=? [ F "Crash" ]
(6) Pmin=? [ F "Crash" ]
(7) Pmax=? [ F "AllAreaSearched" ]
(8) Pmin=? [ F "AllAreaSearched" ]
(9) Pmax=? [ F "MissionTimeUp"|"AllAreaSearched"|"Crash" ]
(10) Pmin=? [ F "MissionTimeUp"|"AllAreaSearched"|"Crash" ]
(11) R{"drop"}max=? [ F "deadlock" ]
(12) R{"drop"}min=? [ F "deadlock" ]
(13) R{"charge"}max=? [ F "deadlock" ]
(14) R{"charge"}min=? [ F "deadlock" ]
(15) R{"time"}max=? [ F "deadlock" ]
(16) R{"time"}min=? [ F "deadlock" ]
(17) R{"success"}max=? [ F "deadlock" ]/Pmin=? [ F "MissionSuccessful" ]
(18) R{"success"}min=? [ F "deadlock" ]/Pmax=? [ F "MissionSuccessful" ]

Type:        MDP
Modules:     Obj Move Robot T 
Variables:   NoOfObjs posx posy s1 retx rety b t c 

---------------------------------------------------------------------

Model checking: Pmax=? [ F "MissionSuccessful" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=0,Objy1=2,Objx2=2,Objy2=0

Building model...
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=0,Objy1=2,Objx2=2,Objy2=0

Warning: Guard for command 7 of module "Move" is never satisfied.

Warning: Guard for command 8 of module "Move" is never satisfied.

Warning: Guard for command 9 of module "Move" is never satisfied.

Warning: Guard for command 7 of module "Robot" is never satisfied.

Warning: Update 2 of command 3 of module "Robot" doesn't do anything ("(s1'=13)", line 249, column 44)

Warning: Update 2 of command 27 of module "T" doesn't do anything ("(c'=1)", line 372, column 108)

Warning: Guard for command 28 of module "T" is never satisfied.

Warning: Update 2 of command 9 of module "T" doesn't do anything ("(c'=1)", line 349, column 112)

Warning: Guard for command 10 of module "T" is never satisfied.

Warning: Update 2 of command 51 of module "T" doesn't do anything ("(c'=1)", line 403, column 113)

Warning: Guard for command 52 of module "T" is never satisfied.

Computing reachable states...

Reachability (BFS): 74 iterations in 0.90 seconds (average 0.012216, setup 0.00)

Time for model construction: 2.552 seconds.

Warning: Deadlocks detected and fixed in 1 states

Type:        MDP
States:      230242 (1 initial)
Transitions: 598746
Choices:     300972

Transition matrix: 176281 nodes (76 terminal), 598746 minterms, vars: 34r/34c/18nd

Prob0A: 51 iterations in 0.40 seconds (average 0.007843, setup 0.00)

Prob1E: 4 iterations in 0.06 seconds (average 0.014000, setup 0.00)

yes = 949, no = 196358, maybe = 32935

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=230242, nc=56834, nnz=161302, k=2] [2.1 MB]
Creating vector for yes... [1.8 MB]
Allocating iteration vectors... [2 x 1.8 MB]
TOTAL: [7.4 MB]

Starting iterations...

Iterative method: 89 iterations in 0.40 seconds (average 0.001573, setup 0.26)

Value in the initial state: 0.9804482560768013

Time for model checking: 1.081 seconds.

Result: 0.9804482560768013 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmin=? [ F "MissionSuccessful" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=0,Objy1=2,Objx2=2,Objy2=0

Prob0E: 53 iterations in 0.46 seconds (average 0.008604, setup 0.00)

Prob1A: 3 iterations in 0.06 seconds (average 0.018667, setup 0.00)

yes = 949, no = 201645, maybe = 27648

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=230242, nc=47639, nnz=134450, k=2] [1.8 MB]
Creating vector for yes... [1.8 MB]
Allocating iteration vectors... [2 x 1.8 MB]
TOTAL: [7.1 MB]

Starting iterations...

Iterative method: 83 iterations in 0.33 seconds (average 0.001494, setup 0.21)

Value in the initial state: 0.7417047646853394

Time for model checking: 0.974 seconds.

Result: 0.7417047646853394 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmax=? [ F "MissionTimeUp" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=0,Objy1=2,Objx2=2,Objy2=0

Prob0A: 56 iterations in 0.60 seconds (average 0.010786, setup 0.00)

Prob1E: 4 iterations in 0.04 seconds (average 0.011000, setup 0.00)

yes = 500, no = 139667, maybe = 90075

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=230242, nc=150988, nnz=412452, k=4] [5.1 MB]
Creating vector for yes... [1.8 MB]
Allocating iteration vectors... [2 x 1.8 MB]
TOTAL: [10.4 MB]

Starting iterations...

Iterative method: 90 iterations in 0.47 seconds (average 0.002044, setup 0.29)

Value in the initial state: 7.024421185454413E-4

Time for model checking: 1.351 seconds.

Result: 7.024421185454413E-4 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmin=? [ F "MissionTimeUp" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=0,Objy1=2,Objx2=2,Objy2=0

Prob0E: 63 iterations in 0.69 seconds (average 0.010984, setup 0.00)

Prob1A: 3 iterations in 0.04 seconds (average 0.013333, setup 0.00)

yes = 500, no = 140024, maybe = 89718

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=230242, nc=150350, nnz=411143, k=4] [5.1 MB]
Creating vector for yes... [1.8 MB]
Allocating iteration vectors... [2 x 1.8 MB]
TOTAL: [10.3 MB]

Starting iterations...

Iterative method: 105 iterations in 0.48 seconds (average 0.002057, setup 0.26)

Value in the initial state: 2.9707063877672884E-6

Time for model checking: 1.46 seconds.

Result: 2.9707063877672884E-6 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmax=? [ F "Crash" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=0,Objy1=2,Objx2=2,Objy2=0

Prob0A: 3 iterations in 0.09 seconds (average 0.029333, setup 0.00)

Prob1E: 201 iterations in 2.89 seconds (average 0.014368, setup 0.00)

yes = 112869, no = 2670, maybe = 114703

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=230242, nc=185433, nnz=483207, k=4] [5.9 MB]
Creating vector for yes... [1.8 MB]
Allocating iteration vectors... [2 x 1.8 MB]
TOTAL: [11.2 MB]

Starting iterations...

Iterative method: 86 iterations in 0.54 seconds (average 0.002140, setup 0.35)

Value in the initial state: 0.03128276669623428

Time for model checking: 4.747 seconds.

Result: 0.03128276669623428 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmin=? [ F "Crash" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=0,Objy1=2,Objx2=2,Objy2=0

Prob0E: 3 iterations in 0.08 seconds (average 0.025333, setup 0.00)

Prob1A: 48 iterations in 0.58 seconds (average 0.012167, setup 0.00)

yes = 112869, no = 2670, maybe = 114703

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=230242, nc=185433, nnz=483207, k=4] [5.9 MB]
Creating vector for yes... [1.8 MB]
Allocating iteration vectors... [2 x 1.8 MB]
TOTAL: [11.2 MB]

Starting iterations...

Iterative method: 88 iterations in 0.45 seconds (average 0.002227, setup 0.25)

Value in the initial state: 0.01744439874898195

Time for model checking: 1.152 seconds.

Result: 0.01744439874898195 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmax=? [ F "AllAreaSearched" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=0,Objy1=2,Objx2=2,Objy2=0

Prob0A: 61 iterations in 0.48 seconds (average 0.007869, setup 0.00)

Prob1E: 4 iterations in 0.05 seconds (average 0.012000, setup 0.00)

yes = 1220, no = 183544, maybe = 45478

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=230242, nc=68823, nnz=178530, k=2] [2.3 MB]
Creating vector for yes... [1.8 MB]
Allocating iteration vectors... [2 x 1.8 MB]
TOTAL: [7.6 MB]

Starting iterations...

Iterative method: 86 iterations in 0.32 seconds (average 0.001721, setup 0.17)

Value in the initial state: 0.22968328634061713

Time for model checking: 0.865 seconds.

Result: 0.22968328634061713 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmin=? [ F "AllAreaSearched" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=0,Objy1=2,Objx2=2,Objy2=0

Prob0E: 64 iterations in 0.43 seconds (average 0.006750, setup 0.00)

Prob1A: 3 iterations in 0.04 seconds (average 0.012000, setup 0.00)

yes = 1220, no = 189422, maybe = 39600

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=230242, nc=59198, nnz=151903, k=2] [2.0 MB]
Creating vector for yes... [1.8 MB]
Allocating iteration vectors... [2 x 1.8 MB]
TOTAL: [7.3 MB]

Starting iterations...

Iterative method: 93 iterations in 0.26 seconds (average 0.001591, setup 0.12)

Value in the initial state: 0.0017486648049429786

Time for model checking: 0.756 seconds.

Result: 0.0017486648049429786 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmax=? [ F "MissionTimeUp"|"AllAreaSearched"|"Crash" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=0,Objy1=2,Objx2=2,Objy2=0

Prob0A: 3 iterations in 0.08 seconds (average 0.028000, setup 0.00)

Prob1E: 208 iterations in 2.83 seconds (average 0.013615, setup 0.00)

yes = 201644, no = 950, maybe = 27648

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=230242, nc=47639, nnz=134450, k=2] [1.8 MB]
Creating vector for yes... [1.8 MB]
Allocating iteration vectors... [2 x 1.8 MB]
TOTAL: [7.1 MB]

Starting iterations...

Iterative method: 77 iterations in 0.31 seconds (average 0.001455, setup 0.20)

Value in the initial state: 0.25829482868575165

Time for model checking: 4.066 seconds.

Result: 0.25829482868575165 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmin=? [ F "MissionTimeUp"|"AllAreaSearched"|"Crash" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=0,Objy1=2,Objx2=2,Objy2=0

Prob0E: 3 iterations in 0.06 seconds (average 0.018667, setup 0.00)

Prob1A: 51 iterations in 0.60 seconds (average 0.011686, setup 0.00)

yes = 196357, no = 950, maybe = 32935

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=230242, nc=56834, nnz=161302, k=2] [2.1 MB]
Creating vector for yes... [1.8 MB]
Allocating iteration vectors... [2 x 1.8 MB]
TOTAL: [7.4 MB]

Starting iterations...

Iterative method: 87 iterations in 0.32 seconds (average 0.001471, setup 0.19)

Value in the initial state: 0.0195510908972697

Time for model checking: 1.175 seconds.

Result: 0.0195510908972697 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"drop"}max=? [ F "deadlock" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=0,Objy1=2,Objx2=2,Objy2=0

Prob0E: 4 iterations in 0.08 seconds (average 0.020000, setup 0.00)

Prob1A: 1 iterations in 0.01 seconds (average 0.008000, setup 0.00)

goal = 1, inf = 0, maybe = 230241

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=230242, nc=300971, nnz=598745, k=4] [8.9 MB]
Building sparse matrix (transition rewards)... [n=230242, nc=300971, nnz=0, k=4] [2.0 MB]
Creating vector for state rewards... [1.8 MB]
Creating vector for inf... [1.8 MB]
Allocating iteration vectors... [2 x 1.8 MB]
TOTAL: [17.9 MB]

Starting iterations...

Iterative method: 86 iterations in 0.70 seconds (average 0.003302, setup 0.42)

Value in the initial state: 0.21138937298988456

Time for model checking: 0.817 seconds.

Result: 0.21138937298988456 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"drop"}min=? [ F "deadlock" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=0,Objy1=2,Objx2=2,Objy2=0

Prob0A: 4 iterations in 0.03 seconds (average 0.008000, setup 0.00)

Prob1E: 5 iterations in 0.05 seconds (average 0.010400, setup 0.00)

Warning: PRISM hasn't checked for zero-reward loops.
Your minimum rewards may be too low...

goal = 1, inf = 0, maybe = 230241

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=230242, nc=300971, nnz=598745, k=4] [8.9 MB]
Building sparse matrix (transition rewards)... [n=230242, nc=300971, nnz=0, k=4] [2.0 MB]
Creating vector for state rewards... [1.8 MB]
Creating vector for inf... [1.8 MB]
Allocating iteration vectors... [2 x 1.8 MB]
TOTAL: [17.9 MB]

Starting iterations...

Iterative method: 76 iterations in 0.58 seconds (average 0.003368, setup 0.32)

Value in the initial state: 0.1844448178198703

Time for model checking: 0.688 seconds.

Result: 0.1844448178198703 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"charge"}max=? [ F "deadlock" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=0,Objy1=2,Objx2=2,Objy2=0

Prob0E: 4 iterations in 0.03 seconds (average 0.007000, setup 0.00)

Prob1A: 1 iterations in 0.01 seconds (average 0.008000, setup 0.00)

goal = 1, inf = 0, maybe = 230241

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=230242, nc=300971, nnz=598745, k=4] [8.9 MB]
Building sparse matrix (transition rewards)... [n=230242, nc=300971, nnz=6458, k=4] [2.1 MB]
Creating vector for state rewards... [1.8 MB]
Creating vector for inf... [1.8 MB]
Allocating iteration vectors... [2 x 1.8 MB]
TOTAL: [18.0 MB]

Starting iterations...

Iterative method: 84 iterations in 0.61 seconds (average 0.003381, setup 0.33)

Value in the initial state: 0.29191810479065505

Time for model checking: 0.659 seconds.

Result: 0.29191810479065505 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"charge"}min=? [ F "deadlock" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=0,Objy1=2,Objx2=2,Objy2=0

Prob0A: 4 iterations in 0.03 seconds (average 0.007000, setup 0.00)

Prob1E: 5 iterations in 0.05 seconds (average 0.010400, setup 0.00)

Warning: PRISM hasn't checked for zero-reward loops.
Your minimum rewards may be too low...

goal = 1, inf = 0, maybe = 230241

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=230242, nc=300971, nnz=598745, k=4] [8.9 MB]
Building sparse matrix (transition rewards)... [n=230242, nc=300971, nnz=6458, k=4] [2.1 MB]
Creating vector for state rewards... [1.8 MB]
Creating vector for inf... [1.8 MB]
Allocating iteration vectors... [2 x 1.8 MB]
TOTAL: [18.0 MB]

Starting iterations...

Iterative method: 89 iterations in 0.62 seconds (average 0.003416, setup 0.32)

Value in the initial state: 0.05214713368551754

Time for model checking: 0.713 seconds.

Result: 0.05214713368551754 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"time"}max=? [ F "deadlock" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=0,Objy1=2,Objx2=2,Objy2=0

Prob0E: 4 iterations in 0.03 seconds (average 0.007000, setup 0.00)

Prob1A: 1 iterations in 0.00 seconds (average 0.004000, setup 0.00)

goal = 1, inf = 0, maybe = 230241

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=230242, nc=300971, nnz=598745, k=4] [8.9 MB]
Building sparse matrix (transition rewards)... [n=230242, nc=300971, nnz=115537, k=4] [3.3 MB]
Creating vector for state rewards... [1.8 MB]
Creating vector for inf... [1.8 MB]
Allocating iteration vectors... [2 x 1.8 MB]
TOTAL: [19.3 MB]

Starting iterations...

Iterative method: 87 iterations in 0.66 seconds (average 0.003402, setup 0.36)

Value in the initial state: 172.5576529886271

Time for model checking: 0.704 seconds.

Result: 172.5576529886271 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"time"}min=? [ F "deadlock" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=0,Objy1=2,Objx2=2,Objy2=0

Prob0A: 4 iterations in 0.02 seconds (average 0.006000, setup 0.00)

Prob1E: 5 iterations in 0.04 seconds (average 0.008800, setup 0.00)

Warning: PRISM hasn't checked for zero-reward loops.
Your minimum rewards may be too low...

goal = 1, inf = 0, maybe = 230241

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=230242, nc=300971, nnz=598745, k=4] [8.9 MB]
Building sparse matrix (transition rewards)... [n=230242, nc=300971, nnz=115537, k=4] [3.3 MB]
Creating vector for state rewards... [1.8 MB]
Creating vector for inf... [1.8 MB]
Allocating iteration vectors... [2 x 1.8 MB]
TOTAL: [19.3 MB]

Starting iterations...

Iterative method: 95 iterations in 0.69 seconds (average 0.003453, setup 0.36)

Value in the initial state: 96.65367085128905

Time for model checking: 0.76 seconds.

Result: 96.65367085128905 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"success"}max=? [ F "deadlock" ]/Pmin=? [ F "MissionSuccessful" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=0,Objy1=2,Objx2=2,Objy2=0

Prob0E: 4 iterations in 0.02 seconds (average 0.006000, setup 0.00)

Prob1A: 1 iterations in 0.00 seconds (average 0.004000, setup 0.00)

goal = 1, inf = 0, maybe = 230241

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=230242, nc=300971, nnz=598745, k=4] [8.9 MB]
Building sparse matrix (transition rewards)... [n=230242, nc=300971, nnz=1870, k=4] [2.0 MB]
Creating vector for state rewards... [1.8 MB]
Creating vector for inf... [1.8 MB]
Allocating iteration vectors... [2 x 1.8 MB]
TOTAL: [18.0 MB]

Starting iterations...

Iterative method: 90 iterations in 0.61 seconds (average 0.003289, setup 0.32)

Prob0E: 53 iterations in 0.32 seconds (average 0.006038, setup 0.00)

Prob1A: 3 iterations in 0.04 seconds (average 0.013333, setup 0.00)

yes = 949, no = 201645, maybe = 27648

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=230242, nc=47639, nnz=134450, k=2] [1.8 MB]
Creating vector for yes... [1.8 MB]
Allocating iteration vectors... [2 x 1.8 MB]
TOTAL: [7.1 MB]

Starting iterations...

Iterative method: 83 iterations in 0.27 seconds (average 0.001446, setup 0.15)

Value in the initial state: 227.96481299903246

Time for model checking: 1.317 seconds.

Result: 227.96481299903246 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"success"}min=? [ F "deadlock" ]/Pmax=? [ F "MissionSuccessful" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=0,Objy1=2,Objx2=2,Objy2=0

Prob0A: 4 iterations in 0.06 seconds (average 0.016000, setup 0.00)

Prob1E: 5 iterations in 0.05 seconds (average 0.009600, setup 0.00)

Warning: PRISM hasn't checked for zero-reward loops.
Your minimum rewards may be too low...

goal = 1, inf = 0, maybe = 230241

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=230242, nc=300971, nnz=598745, k=4] [8.9 MB]
Building sparse matrix (transition rewards)... [n=230242, nc=300971, nnz=1870, k=4] [2.0 MB]
Creating vector for state rewards... [1.8 MB]
Creating vector for inf... [1.8 MB]
Allocating iteration vectors... [2 x 1.8 MB]
TOTAL: [18.0 MB]

Starting iterations...

Iterative method: 87 iterations in 0.63 seconds (average 0.003310, setup 0.34)

Prob0A: 51 iterations in 0.26 seconds (average 0.005098, setup 0.00)

Prob1E: 4 iterations in 0.03 seconds (average 0.007000, setup 0.00)

yes = 949, no = 196358, maybe = 32935

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=230242, nc=56834, nnz=161302, k=2] [2.1 MB]
Creating vector for yes... [1.8 MB]
Allocating iteration vectors... [2 x 1.8 MB]
TOTAL: [7.4 MB]

Starting iterations...

Iterative method: 89 iterations in 0.27 seconds (average 0.001483, setup 0.14)

Value in the initial state: 79.22541293531633

Time for model checking: 1.31 seconds.

Result: 79.22541293531633 (value in the initial state)

---------------------------------------------------------------------

Note: There were 16 warnings during computation.

