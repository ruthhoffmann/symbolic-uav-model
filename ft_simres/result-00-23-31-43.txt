PRISM
=====

Version: 4.3
Date: Tue Aug 02 12:29:05 BST 2016
Hostname: eglinton
Memory limits: cudd=1g, java(heap)=981.5m
Command line: prism uav_sim_ft_mdp.prism -s uav_mdp.props -const 'Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=3,Objy1=1,Objx2=4,Objy2=3,'

Parsing model file "uav_sim_ft_mdp.prism"...

Parsing properties file "uav_mdp.props"...

18 properties:
(1) Pmax=? [ F "MissionSuccessful" ]
(2) Pmin=? [ F "MissionSuccessful" ]
(3) Pmax=? [ F "MissionTimeUp" ]
(4) Pmin=? [ F "MissionTimeUp" ]
(5) Pmax=? [ F "Crash" ]
(6) Pmin=? [ F "Crash" ]
(7) Pmax=? [ F "AllAreaSearched" ]
(8) Pmin=? [ F "AllAreaSearched" ]
(9) Pmax=? [ F "MissionTimeUp"|"AllAreaSearched"|"Crash" ]
(10) Pmin=? [ F "MissionTimeUp"|"AllAreaSearched"|"Crash" ]
(11) R{"drop"}max=? [ F "deadlock" ]
(12) R{"drop"}min=? [ F "deadlock" ]
(13) R{"charge"}max=? [ F "deadlock" ]
(14) R{"charge"}min=? [ F "deadlock" ]
(15) R{"time"}max=? [ F "deadlock" ]
(16) R{"time"}min=? [ F "deadlock" ]
(17) R{"success"}max=? [ F "deadlock" ]/Pmin=? [ F "MissionSuccessful" ]
(18) R{"success"}min=? [ F "deadlock" ]/Pmax=? [ F "MissionSuccessful" ]

Type:        MDP
Modules:     Obj Move Robot T 
Variables:   NoOfObjs posx posy s1 retx rety b t c 

---------------------------------------------------------------------

Model checking: Pmax=? [ F "MissionSuccessful" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=3,Objy1=1,Objx2=4,Objy2=3

Building model...
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=3,Objy1=1,Objx2=4,Objy2=3

Warning: Guard for command 7 of module "Move" is never satisfied.

Warning: Guard for command 8 of module "Move" is never satisfied.

Warning: Guard for command 9 of module "Move" is never satisfied.

Warning: Guard for command 7 of module "Robot" is never satisfied.

Warning: Update 2 of command 3 of module "Robot" doesn't do anything ("(s1'=13)", line 249, column 44)

Warning: Update 2 of command 27 of module "T" doesn't do anything ("(c'=1)", line 372, column 108)

Warning: Guard for command 28 of module "T" is never satisfied.

Warning: Update 2 of command 9 of module "T" doesn't do anything ("(c'=1)", line 349, column 112)

Warning: Guard for command 10 of module "T" is never satisfied.

Warning: Update 2 of command 51 of module "T" doesn't do anything ("(c'=1)", line 403, column 113)

Warning: Guard for command 52 of module "T" is never satisfied.

Computing reachable states...

Reachability (BFS): 79 iterations in 0.77 seconds (average 0.009722, setup 0.00)

Time for model construction: 1.885 seconds.

Warning: Deadlocks detected and fixed in 1 states

Type:        MDP
States:      194175 (1 initial)
Transitions: 516397
Choices:     257440

Transition matrix: 168327 nodes (76 terminal), 516397 minterms, vars: 34r/34c/18nd

Prob0A: 52 iterations in 0.44 seconds (average 0.008462, setup 0.00)

Prob1E: 4 iterations in 0.05 seconds (average 0.013000, setup 0.00)

yes = 944, no = 162867, maybe = 30364

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=194175, nc=52034, nnz=145887, k=2] [1.9 MB]
Creating vector for yes... [1.5 MB]
Allocating iteration vectors... [2 x 1.5 MB]
TOTAL: [6.3 MB]

Starting iterations...

Iterative method: 94 iterations in 0.33 seconds (average 0.001404, setup 0.20)

Value in the initial state: 0.9784001672817317

Time for model checking: 0.851 seconds.

Result: 0.9784001672817317 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmin=? [ F "MissionSuccessful" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=3,Objy1=1,Objx2=4,Objy2=3

Prob0E: 65 iterations in 0.49 seconds (average 0.007508, setup 0.00)

Prob1A: 3 iterations in 0.04 seconds (average 0.012000, setup 0.00)

yes = 944, no = 167282, maybe = 25949

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=194175, nc=44424, nnz=123965, k=2] [1.6 MB]
Creating vector for yes... [1.5 MB]
Allocating iteration vectors... [2 x 1.5 MB]
TOTAL: [6.1 MB]

Starting iterations...

Iterative method: 89 iterations in 0.24 seconds (average 0.001258, setup 0.13)

Value in the initial state: 0.7572449786897297

Time for model checking: 0.792 seconds.

Result: 0.7572449786897297 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmax=? [ F "MissionTimeUp" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=3,Objy1=1,Objx2=4,Objy2=3

Prob0A: 62 iterations in 0.41 seconds (average 0.006645, setup 0.00)

Prob1E: 4 iterations in 0.03 seconds (average 0.008000, setup 0.00)

yes = 360, no = 113332, maybe = 80483

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=194175, nc=133739, nnz=364127, k=4] [4.5 MB]
Creating vector for yes... [1.5 MB]
Allocating iteration vectors... [2 x 1.5 MB]
TOTAL: [8.9 MB]

Starting iterations...

Iterative method: 90 iterations in 0.40 seconds (average 0.001733, setup 0.24)

Value in the initial state: 0.0013009527585290846

Time for model checking: 0.848 seconds.

Result: 0.0013009527585290846 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmin=? [ F "MissionTimeUp" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=3,Objy1=1,Objx2=4,Objy2=3

Prob0E: 69 iterations in 0.64 seconds (average 0.009217, setup 0.00)

Prob1A: 3 iterations in 0.04 seconds (average 0.013333, setup 0.00)

yes = 360, no = 113670, maybe = 80145

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=194175, nc=133139, nnz=362857, k=4] [4.5 MB]
Creating vector for yes... [1.5 MB]
Allocating iteration vectors... [2 x 1.5 MB]
TOTAL: [8.9 MB]

Starting iterations...

Iterative method: 105 iterations in 0.40 seconds (average 0.001752, setup 0.22)

Value in the initial state: 4.586412966334053E-6

Time for model checking: 1.091 seconds.

Result: 4.586412966334053E-6 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmax=? [ F "Crash" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=3,Objy1=1,Objx2=4,Objy2=3

Prob0A: 3 iterations in 0.06 seconds (average 0.021333, setup 0.00)

Prob1E: 248 iterations in 2.64 seconds (average 0.010645, setup 0.00)

yes = 95149, no = 2583, maybe = 96443

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=194175, nc=159708, nnz=418665, k=4] [5.1 MB]
Creating vector for yes... [1.5 MB]
Allocating iteration vectors... [2 x 1.5 MB]
TOTAL: [9.6 MB]

Starting iterations...

Iterative method: 89 iterations in 0.44 seconds (average 0.001798, setup 0.28)

Value in the initial state: 0.062324864769052066

Time for model checking: 3.164 seconds.

Result: 0.062324864769052066 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmin=? [ F "Crash" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=3,Objy1=1,Objx2=4,Objy2=3

Prob0E: 3 iterations in 0.06 seconds (average 0.018667, setup 0.00)

Prob1A: 46 iterations in 0.42 seconds (average 0.009217, setup 0.00)

yes = 95149, no = 2583, maybe = 96443

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=194175, nc=159708, nnz=418665, k=4] [5.1 MB]
Creating vector for yes... [1.5 MB]
Allocating iteration vectors... [2 x 1.5 MB]
TOTAL: [9.6 MB]

Starting iterations...

Iterative method: 94 iterations in 0.38 seconds (average 0.001872, setup 0.20)

Value in the initial state: 0.019584835773222578

Time for model checking: 0.879 seconds.

Result: 0.019584835773222578 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmax=? [ F "AllAreaSearched" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=3,Objy1=1,Objx2=4,Objy2=3

Prob0A: 52 iterations in 0.33 seconds (average 0.006385, setup 0.00)

Prob1E: 4 iterations in 0.04 seconds (average 0.010000, setup 0.00)

yes = 1278, no = 149853, maybe = 43044

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=194175, nc=69925, nnz=188767, k=2] [2.4 MB]
Creating vector for yes... [1.5 MB]
Allocating iteration vectors... [2 x 1.5 MB]
TOTAL: [6.9 MB]

Starting iterations...

Iterative method: 85 iterations in 0.28 seconds (average 0.001412, setup 0.16)

Value in the initial state: 0.21501721552751038

Time for model checking: 0.685 seconds.

Result: 0.21501721552751038 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmin=? [ F "AllAreaSearched" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=3,Objy1=1,Objx2=4,Objy2=3

Prob0E: 68 iterations in 0.44 seconds (average 0.006529, setup 0.00)

Prob1A: 3 iterations in 0.04 seconds (average 0.012000, setup 0.00)

yes = 1278, no = 156139, maybe = 36758

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=194175, nc=59651, nnz=160347, k=2] [2.1 MB]
Creating vector for yes... [1.5 MB]
Allocating iteration vectors... [2 x 1.5 MB]
TOTAL: [6.5 MB]

Starting iterations...

Iterative method: 88 iterations in 0.24 seconds (average 0.001318, setup 0.13)

Value in the initial state: 0.0012477569727550695

Time for model checking: 0.738 seconds.

Result: 0.0012477569727550695 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmax=? [ F "MissionTimeUp"|"AllAreaSearched"|"Crash" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=3,Objy1=1,Objx2=4,Objy2=3

Prob0A: 3 iterations in 0.11 seconds (average 0.037333, setup 0.00)

Prob1E: 252 iterations in 3.12 seconds (average 0.012381, setup 0.00)

yes = 167281, no = 945, maybe = 25949

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=194175, nc=44424, nnz=123965, k=2] [1.6 MB]
Creating vector for yes... [1.5 MB]
Allocating iteration vectors... [2 x 1.5 MB]
TOTAL: [6.1 MB]

Starting iterations...

Iterative method: 83 iterations in 0.27 seconds (average 0.001253, setup 0.17)

Value in the initial state: 0.24275472658002323

Time for model checking: 3.501 seconds.

Result: 0.24275472658002323 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmin=? [ F "MissionTimeUp"|"AllAreaSearched"|"Crash" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=3,Objy1=1,Objx2=4,Objy2=3

Prob0E: 3 iterations in 0.04 seconds (average 0.013333, setup 0.00)

Prob1A: 52 iterations in 0.40 seconds (average 0.007692, setup 0.00)

yes = 162866, no = 945, maybe = 30364

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=194175, nc=52034, nnz=145887, k=2] [1.9 MB]
Creating vector for yes... [1.5 MB]
Allocating iteration vectors... [2 x 1.5 MB]
TOTAL: [6.3 MB]

Starting iterations...

Iterative method: 91 iterations in 0.26 seconds (average 0.001319, setup 0.14)

Value in the initial state: 0.021599400525121805

Time for model checking: 0.713 seconds.

Result: 0.021599400525121805 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"drop"}max=? [ F "deadlock" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=3,Objy1=1,Objx2=4,Objy2=3

Prob0E: 4 iterations in 0.04 seconds (average 0.010000, setup 0.00)

Prob1A: 1 iterations in 0.01 seconds (average 0.008000, setup 0.00)

goal = 1, inf = 0, maybe = 194174

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=194175, nc=257439, nnz=516396, k=4] [7.6 MB]
Building sparse matrix (transition rewards)... [n=194175, nc=257439, nnz=0, k=4] [1.7 MB]
Creating vector for state rewards... [1.5 MB]
Creating vector for inf... [1.5 MB]
Allocating iteration vectors... [2 x 1.5 MB]
TOTAL: [15.3 MB]

Starting iterations...

Iterative method: 89 iterations in 0.60 seconds (average 0.002742, setup 0.35)

Value in the initial state: 0.2299241245901566

Time for model checking: 0.652 seconds.

Result: 0.2299241245901566 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"drop"}min=? [ F "deadlock" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=3,Objy1=1,Objx2=4,Objy2=3

Prob0A: 4 iterations in 0.03 seconds (average 0.008000, setup 0.00)

Prob1E: 5 iterations in 0.04 seconds (average 0.008000, setup 0.00)

Warning: PRISM hasn't checked for zero-reward loops.
Your minimum rewards may be too low...

goal = 1, inf = 0, maybe = 194174

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=194175, nc=257439, nnz=516396, k=4] [7.6 MB]
Building sparse matrix (transition rewards)... [n=194175, nc=257439, nnz=0, k=4] [1.7 MB]
Creating vector for state rewards... [1.5 MB]
Creating vector for inf... [1.5 MB]
Allocating iteration vectors... [2 x 1.5 MB]
TOTAL: [15.3 MB]

Starting iterations...

Iterative method: 83 iterations in 0.50 seconds (average 0.002795, setup 0.27)

Value in the initial state: 0.15706974431048346

Time for model checking: 0.598 seconds.

Result: 0.15706974431048346 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"charge"}max=? [ F "deadlock" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=3,Objy1=1,Objx2=4,Objy2=3

Prob0E: 4 iterations in 0.02 seconds (average 0.006000, setup 0.00)

Prob1A: 1 iterations in 0.00 seconds (average 0.004000, setup 0.00)

goal = 1, inf = 0, maybe = 194174

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=194175, nc=257439, nnz=516396, k=4] [7.6 MB]
Building sparse matrix (transition rewards)... [n=194175, nc=257439, nnz=5786, k=4] [1.8 MB]
Creating vector for state rewards... [1.5 MB]
Creating vector for inf... [1.5 MB]
Allocating iteration vectors... [2 x 1.5 MB]
TOTAL: [15.3 MB]

Starting iterations...

Iterative method: 85 iterations in 0.51 seconds (average 0.002824, setup 0.27)

Value in the initial state: 0.9816289774011154

Time for model checking: 0.554 seconds.

Result: 0.9816289774011154 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"charge"}min=? [ F "deadlock" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=3,Objy1=1,Objx2=4,Objy2=3

Prob0A: 4 iterations in 0.02 seconds (average 0.006000, setup 0.00)

Prob1E: 5 iterations in 0.04 seconds (average 0.008000, setup 0.00)

Warning: PRISM hasn't checked for zero-reward loops.
Your minimum rewards may be too low...

goal = 1, inf = 0, maybe = 194174

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=194175, nc=257439, nnz=516396, k=4] [7.6 MB]
Building sparse matrix (transition rewards)... [n=194175, nc=257439, nnz=5786, k=4] [1.8 MB]
Creating vector for state rewards... [1.5 MB]
Creating vector for inf... [1.5 MB]
Allocating iteration vectors... [2 x 1.5 MB]
TOTAL: [15.3 MB]

Starting iterations...

Iterative method: 95 iterations in 0.55 seconds (average 0.002863, setup 0.28)

Value in the initial state: 0.06269402000255404

Time for model checking: 0.623 seconds.

Result: 0.06269402000255404 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"time"}max=? [ F "deadlock" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=3,Objy1=1,Objx2=4,Objy2=3

Prob0E: 4 iterations in 0.02 seconds (average 0.006000, setup 0.00)

Prob1A: 1 iterations in 0.00 seconds (average 0.004000, setup 0.00)

goal = 1, inf = 0, maybe = 194174

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=194175, nc=257439, nnz=516396, k=4] [7.6 MB]
Building sparse matrix (transition rewards)... [n=194175, nc=257439, nnz=97730, k=4] [2.8 MB]
Creating vector for state rewards... [1.5 MB]
Creating vector for inf... [1.5 MB]
Allocating iteration vectors... [2 x 1.5 MB]
TOTAL: [16.4 MB]

Starting iterations...

Iterative method: 91 iterations in 0.58 seconds (average 0.002901, setup 0.31)

Value in the initial state: 342.4587580658137

Time for model checking: 0.607 seconds.

Result: 342.4587580658137 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"time"}min=? [ F "deadlock" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=3,Objy1=1,Objx2=4,Objy2=3

Prob0A: 4 iterations in 0.02 seconds (average 0.006000, setup 0.00)

Prob1E: 5 iterations in 0.04 seconds (average 0.008800, setup 0.00)

Warning: PRISM hasn't checked for zero-reward loops.
Your minimum rewards may be too low...

goal = 1, inf = 0, maybe = 194174

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=194175, nc=257439, nnz=516396, k=4] [7.6 MB]
Building sparse matrix (transition rewards)... [n=194175, nc=257439, nnz=97730, k=4] [2.8 MB]
Creating vector for state rewards... [1.5 MB]
Creating vector for inf... [1.5 MB]
Allocating iteration vectors... [2 x 1.5 MB]
TOTAL: [16.4 MB]

Starting iterations...

Iterative method: 99 iterations in 0.59 seconds (average 0.002909, setup 0.30)

Value in the initial state: 108.46837445971369

Time for model checking: 0.671 seconds.

Result: 108.46837445971369 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"success"}max=? [ F "deadlock" ]/Pmin=? [ F "MissionSuccessful" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=3,Objy1=1,Objx2=4,Objy2=3

Prob0E: 4 iterations in 0.03 seconds (average 0.007000, setup 0.00)

Prob1A: 1 iterations in 0.00 seconds (average 0.004000, setup 0.00)

goal = 1, inf = 0, maybe = 194174

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=194175, nc=257439, nnz=516396, k=4] [7.6 MB]
Building sparse matrix (transition rewards)... [n=194175, nc=257439, nnz=1860, k=4] [1.7 MB]
Creating vector for state rewards... [1.5 MB]
Creating vector for inf... [1.5 MB]
Allocating iteration vectors... [2 x 1.5 MB]
TOTAL: [15.3 MB]

Starting iterations...

Iterative method: 93 iterations in 0.53 seconds (average 0.002753, setup 0.28)

Prob0E: 65 iterations in 0.36 seconds (average 0.005477, setup 0.00)

Prob1A: 3 iterations in 0.04 seconds (average 0.014667, setup 0.00)

yes = 944, no = 167282, maybe = 25949

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=194175, nc=44424, nnz=123965, k=2] [1.6 MB]
Creating vector for yes... [1.5 MB]
Allocating iteration vectors... [2 x 1.5 MB]
TOTAL: [6.1 MB]

Starting iterations...

Iterative method: 89 iterations in 0.24 seconds (average 0.001258, setup 0.13)

Value in the initial state: 441.8638546315937

Time for model checking: 1.238 seconds.

Result: 441.8638546315937 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"success"}min=? [ F "deadlock" ]/Pmax=? [ F "MissionSuccessful" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=3,Objy1=1,Objx2=4,Objy2=3

Prob0A: 4 iterations in 0.05 seconds (average 0.012000, setup 0.00)

Prob1E: 5 iterations in 0.05 seconds (average 0.009600, setup 0.00)

Warning: PRISM hasn't checked for zero-reward loops.
Your minimum rewards may be too low...

goal = 1, inf = 0, maybe = 194174

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=194175, nc=257439, nnz=516396, k=4] [7.6 MB]
Building sparse matrix (transition rewards)... [n=194175, nc=257439, nnz=1860, k=4] [1.7 MB]
Creating vector for state rewards... [1.5 MB]
Creating vector for inf... [1.5 MB]
Allocating iteration vectors... [2 x 1.5 MB]
TOTAL: [15.3 MB]

Starting iterations...

Iterative method: 93 iterations in 0.54 seconds (average 0.002796, setup 0.28)

Prob0A: 52 iterations in 0.26 seconds (average 0.005000, setup 0.00)

Prob1E: 4 iterations in 0.03 seconds (average 0.007000, setup 0.00)

yes = 944, no = 162867, maybe = 30364

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=194175, nc=52034, nnz=145887, k=2] [1.9 MB]
Creating vector for yes... [1.5 MB]
Allocating iteration vectors... [2 x 1.5 MB]
TOTAL: [6.3 MB]

Starting iterations...

Iterative method: 94 iterations in 0.24 seconds (average 0.001277, setup 0.12)

Value in the initial state: 80.6606245875452

Time for model checking: 1.187 seconds.

Result: 80.6606245875452 (value in the initial state)

---------------------------------------------------------------------

Note: There were 16 warnings during computation.

