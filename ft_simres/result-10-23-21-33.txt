PRISM
=====

Version: 4.3
Date: Tue Aug 02 12:38:41 BST 2016
Hostname: eglinton
Memory limits: cudd=1g, java(heap)=981.5m
Command line: prism uav_sim_ft_mdp.prism -s uav_mdp.props -const 'Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=2,Objy1=1,Objx2=3,Objy2=3,'

Parsing model file "uav_sim_ft_mdp.prism"...

Parsing properties file "uav_mdp.props"...

18 properties:
(1) Pmax=? [ F "MissionSuccessful" ]
(2) Pmin=? [ F "MissionSuccessful" ]
(3) Pmax=? [ F "MissionTimeUp" ]
(4) Pmin=? [ F "MissionTimeUp" ]
(5) Pmax=? [ F "Crash" ]
(6) Pmin=? [ F "Crash" ]
(7) Pmax=? [ F "AllAreaSearched" ]
(8) Pmin=? [ F "AllAreaSearched" ]
(9) Pmax=? [ F "MissionTimeUp"|"AllAreaSearched"|"Crash" ]
(10) Pmin=? [ F "MissionTimeUp"|"AllAreaSearched"|"Crash" ]
(11) R{"drop"}max=? [ F "deadlock" ]
(12) R{"drop"}min=? [ F "deadlock" ]
(13) R{"charge"}max=? [ F "deadlock" ]
(14) R{"charge"}min=? [ F "deadlock" ]
(15) R{"time"}max=? [ F "deadlock" ]
(16) R{"time"}min=? [ F "deadlock" ]
(17) R{"success"}max=? [ F "deadlock" ]/Pmin=? [ F "MissionSuccessful" ]
(18) R{"success"}min=? [ F "deadlock" ]/Pmax=? [ F "MissionSuccessful" ]

Type:        MDP
Modules:     Obj Move Robot T 
Variables:   NoOfObjs posx posy s1 retx rety b t c 

---------------------------------------------------------------------

Model checking: Pmax=? [ F "MissionSuccessful" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=2,Objy1=1,Objx2=3,Objy2=3

Building model...
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=2,Objy1=1,Objx2=3,Objy2=3

Warning: Guard for command 7 of module "Move" is never satisfied.

Warning: Guard for command 8 of module "Move" is never satisfied.

Warning: Guard for command 9 of module "Move" is never satisfied.

Warning: Guard for command 7 of module "Robot" is never satisfied.

Warning: Update 2 of command 3 of module "Robot" doesn't do anything ("(s1'=13)", line 249, column 44)

Warning: Update 2 of command 27 of module "T" doesn't do anything ("(c'=1)", line 372, column 108)

Warning: Guard for command 28 of module "T" is never satisfied.

Warning: Update 2 of command 9 of module "T" doesn't do anything ("(c'=1)", line 349, column 112)

Warning: Guard for command 10 of module "T" is never satisfied.

Warning: Update 2 of command 51 of module "T" doesn't do anything ("(c'=1)", line 403, column 113)

Warning: Guard for command 52 of module "T" is never satisfied.

Computing reachable states...

Reachability (BFS): 79 iterations in 0.71 seconds (average 0.008962, setup 0.00)

Time for model construction: 1.958 seconds.

Warning: Deadlocks detected and fixed in 1 states

Type:        MDP
States:      170819 (1 initial)
Transitions: 444204
Choices:     225069

Transition matrix: 165332 nodes (76 terminal), 444204 minterms, vars: 34r/34c/18nd

Prob0A: 52 iterations in 0.39 seconds (average 0.007462, setup 0.00)

Prob1E: 4 iterations in 0.05 seconds (average 0.012000, setup 0.00)

yes = 954, no = 142730, maybe = 27135

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=170819, nc=46058, nnz=126601, k=2] [1.7 MB]
Creating vector for yes... [1.3 MB]
Allocating iteration vectors... [2 x 1.3 MB]
TOTAL: [5.6 MB]

Starting iterations...

Iterative method: 92 iterations in 0.31 seconds (average 0.001130, setup 0.21)

Value in the initial state: 0.9788618111843573

Time for model checking: 0.788 seconds.

Result: 0.9788618111843573 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmin=? [ F "MissionSuccessful" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=2,Objy1=1,Objx2=3,Objy2=3

Prob0E: 65 iterations in 0.58 seconds (average 0.008923, setup 0.00)

Prob1A: 3 iterations in 0.05 seconds (average 0.017333, setup 0.00)

yes = 954, no = 146922, maybe = 22943

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=170819, nc=38880, nnz=105950, k=2] [1.4 MB]
Creating vector for yes... [1.3 MB]
Allocating iteration vectors... [2 x 1.3 MB]
TOTAL: [5.3 MB]

Starting iterations...

Iterative method: 88 iterations in 0.34 seconds (average 0.001227, setup 0.23)

Value in the initial state: 0.7767125517906569

Time for model checking: 1.005 seconds.

Result: 0.7767125517906569 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmax=? [ F "MissionTimeUp" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=2,Objy1=1,Objx2=3,Objy2=3

Prob0A: 63 iterations in 0.52 seconds (average 0.008254, setup 0.00)

Prob1E: 4 iterations in 0.04 seconds (average 0.010000, setup 0.00)

yes = 320, no = 101007, maybe = 69492

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=170819, nc=113673, nnz=305057, k=4] [3.8 MB]
Creating vector for yes... [1.3 MB]
Allocating iteration vectors... [2 x 1.3 MB]
TOTAL: [7.7 MB]

Starting iterations...

Iterative method: 90 iterations in 0.36 seconds (average 0.001467, setup 0.23)

Value in the initial state: 9.38912978413253E-4

Time for model checking: 0.941 seconds.

Result: 9.38912978413253E-4 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmin=? [ F "MissionTimeUp" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=2,Objy1=1,Objx2=3,Objy2=3

Prob0E: 70 iterations in 0.68 seconds (average 0.009771, setup 0.00)

Prob1A: 3 iterations in 0.04 seconds (average 0.014667, setup 0.00)

yes = 320, no = 101307, maybe = 69192

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=170819, nc=113149, nnz=303865, k=4] [3.7 MB]
Creating vector for yes... [1.3 MB]
Allocating iteration vectors... [2 x 1.3 MB]
TOTAL: [7.7 MB]

Starting iterations...

Iterative method: 105 iterations in 0.38 seconds (average 0.001486, setup 0.22)

Value in the initial state: 2.294669337310756E-6

Time for model checking: 1.137 seconds.

Result: 2.294669337310756E-6 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmax=? [ F "Crash" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=2,Objy1=1,Objx2=3,Objy2=3

Prob0A: 3 iterations in 0.08 seconds (average 0.028000, setup 0.00)

Prob1E: 248 iterations in 2.82 seconds (average 0.011371, setup 0.00)

yes = 83842, no = 2575, maybe = 84402

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=170819, nc=138652, nnz=357787, k=4] [4.4 MB]
Creating vector for yes... [1.3 MB]
Allocating iteration vectors... [2 x 1.3 MB]
TOTAL: [8.3 MB]

Starting iterations...

Iterative method: 88 iterations in 0.40 seconds (average 0.001591, setup 0.26)

Value in the initial state: 0.06205223170853303

Time for model checking: 3.33 seconds.

Result: 0.06205223170853303 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmin=? [ F "Crash" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=2,Objy1=1,Objx2=3,Objy2=3

Prob0E: 3 iterations in 0.06 seconds (average 0.020000, setup 0.00)

Prob1A: 46 iterations in 0.40 seconds (average 0.008696, setup 0.00)

yes = 83842, no = 2575, maybe = 84402

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=170819, nc=138652, nnz=357787, k=4] [4.4 MB]
Creating vector for yes... [1.3 MB]
Allocating iteration vectors... [2 x 1.3 MB]
TOTAL: [8.3 MB]

Starting iterations...

Iterative method: 88 iterations in 0.32 seconds (average 0.001636, setup 0.18)

Value in the initial state: 0.019159886201892823

Time for model checking: 0.798 seconds.

Result: 0.019159886201892823 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmax=? [ F "AllAreaSearched" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=2,Objy1=1,Objx2=3,Objy2=3

Prob0A: 50 iterations in 0.33 seconds (average 0.006640, setup 0.00)

Prob1E: 4 iterations in 0.04 seconds (average 0.010000, setup 0.00)

yes = 1300, no = 132140, maybe = 37379

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=170819, nc=60177, nnz=158387, k=2] [2.0 MB]
Creating vector for yes... [1.3 MB]
Allocating iteration vectors... [2 x 1.3 MB]
TOTAL: [5.9 MB]

Starting iterations...

Iterative method: 83 iterations in 0.26 seconds (average 0.001205, setup 0.16)

Value in the initial state: 0.19631162527968385

Time for model checking: 0.654 seconds.

Result: 0.19631162527968385 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmin=? [ F "AllAreaSearched" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=2,Objy1=1,Objx2=3,Objy2=3

Prob0E: 67 iterations in 0.42 seconds (average 0.006269, setup 0.00)

Prob1A: 3 iterations in 0.03 seconds (average 0.010667, setup 0.00)

yes = 1300, no = 137879, maybe = 31640

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=170819, nc=50937, nnz=132994, k=2] [1.7 MB]
Creating vector for yes... [1.3 MB]
Allocating iteration vectors... [2 x 1.3 MB]
TOTAL: [5.6 MB]

Starting iterations...

Iterative method: 86 iterations in 0.22 seconds (average 0.001209, setup 0.11)

Value in the initial state: 0.001230572702469246

Time for model checking: 0.677 seconds.

Result: 0.001230572702469246 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmax=? [ F "MissionTimeUp"|"AllAreaSearched"|"Crash" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=2,Objy1=1,Objx2=3,Objy2=3

Prob0A: 3 iterations in 0.10 seconds (average 0.032000, setup 0.00)

Prob1E: 252 iterations in 3.03 seconds (average 0.012016, setup 0.00)

yes = 146921, no = 955, maybe = 22943

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=170819, nc=38880, nnz=105950, k=2] [1.4 MB]
Creating vector for yes... [1.3 MB]
Allocating iteration vectors... [2 x 1.3 MB]
TOTAL: [5.3 MB]

Starting iterations...

Iterative method: 82 iterations in 0.26 seconds (average 0.001122, setup 0.16)

Value in the initial state: 0.22328723344206083

Time for model checking: 3.381 seconds.

Result: 0.22328723344206083 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmin=? [ F "MissionTimeUp"|"AllAreaSearched"|"Crash" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=2,Objy1=1,Objx2=3,Objy2=3

Prob0E: 3 iterations in 0.04 seconds (average 0.014667, setup 0.00)

Prob1A: 52 iterations in 0.46 seconds (average 0.008923, setup 0.00)

yes = 142729, no = 955, maybe = 27135

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=170819, nc=46058, nnz=126601, k=2] [1.7 MB]
Creating vector for yes... [1.3 MB]
Allocating iteration vectors... [2 x 1.3 MB]
TOTAL: [5.6 MB]

Starting iterations...

Iterative method: 90 iterations in 0.26 seconds (average 0.001200, setup 0.15)

Value in the initial state: 0.02113732504642691

Time for model checking: 0.792 seconds.

Result: 0.02113732504642691 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"drop"}max=? [ F "deadlock" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=2,Objy1=1,Objx2=3,Objy2=3

Prob0E: 4 iterations in 0.07 seconds (average 0.018000, setup 0.00)

Prob1A: 1 iterations in 0.01 seconds (average 0.008000, setup 0.00)

goal = 1, inf = 0, maybe = 170818

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=170819, nc=225068, nnz=444203, k=4] [6.6 MB]
Building sparse matrix (transition rewards)... [n=170819, nc=225068, nnz=0, k=4] [1.5 MB]
Creating vector for state rewards... [1.3 MB]
Creating vector for inf... [1.3 MB]
Allocating iteration vectors... [2 x 1.3 MB]
TOTAL: [13.3 MB]

Starting iterations...

Iterative method: 88 iterations in 0.54 seconds (average 0.002455, setup 0.32)

Value in the initial state: 0.18018031374004218

Time for model checking: 0.629 seconds.

Result: 0.18018031374004218 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"drop"}min=? [ F "deadlock" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=2,Objy1=1,Objx2=3,Objy2=3

Prob0A: 4 iterations in 0.02 seconds (average 0.006000, setup 0.00)

Prob1E: 5 iterations in 0.04 seconds (average 0.008800, setup 0.00)

Warning: PRISM hasn't checked for zero-reward loops.
Your minimum rewards may be too low...

goal = 1, inf = 0, maybe = 170818

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=170819, nc=225068, nnz=444203, k=4] [6.6 MB]
Building sparse matrix (transition rewards)... [n=170819, nc=225068, nnz=0, k=4] [1.5 MB]
Creating vector for state rewards... [1.3 MB]
Creating vector for inf... [1.3 MB]
Allocating iteration vectors... [2 x 1.3 MB]
TOTAL: [13.3 MB]

Starting iterations...

Iterative method: 81 iterations in 0.45 seconds (average 0.002469, setup 0.25)

Value in the initial state: 0.13497265639355388

Time for model checking: 0.528 seconds.

Result: 0.13497265639355388 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"charge"}max=? [ F "deadlock" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=2,Objy1=1,Objx2=3,Objy2=3

Prob0E: 4 iterations in 0.02 seconds (average 0.006000, setup 0.00)

Prob1A: 1 iterations in 0.00 seconds (average 0.004000, setup 0.00)

goal = 1, inf = 0, maybe = 170818

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=170819, nc=225068, nnz=444203, k=4] [6.6 MB]
Building sparse matrix (transition rewards)... [n=170819, nc=225068, nnz=4730, k=4] [1.6 MB]
Creating vector for state rewards... [1.3 MB]
Creating vector for inf... [1.3 MB]
Allocating iteration vectors... [2 x 1.3 MB]
TOTAL: [13.4 MB]

Starting iterations...

Iterative method: 84 iterations in 0.46 seconds (average 0.002476, setup 0.25)

Value in the initial state: 0.9806624095538476

Time for model checking: 0.493 seconds.

Result: 0.9806624095538476 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"charge"}min=? [ F "deadlock" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=2,Objy1=1,Objx2=3,Objy2=3

Prob0A: 4 iterations in 0.02 seconds (average 0.006000, setup 0.00)

Prob1E: 5 iterations in 0.04 seconds (average 0.008000, setup 0.00)

Warning: PRISM hasn't checked for zero-reward loops.
Your minimum rewards may be too low...

goal = 1, inf = 0, maybe = 170818

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=170819, nc=225068, nnz=444203, k=4] [6.6 MB]
Building sparse matrix (transition rewards)... [n=170819, nc=225068, nnz=4730, k=4] [1.6 MB]
Creating vector for state rewards... [1.3 MB]
Creating vector for inf... [1.3 MB]
Allocating iteration vectors... [2 x 1.3 MB]
TOTAL: [13.4 MB]

Starting iterations...

Iterative method: 94 iterations in 0.48 seconds (average 0.002468, setup 0.25)

Value in the initial state: 0.05304746368652868

Time for model checking: 0.558 seconds.

Result: 0.05304746368652868 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"time"}max=? [ F "deadlock" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=2,Objy1=1,Objx2=3,Objy2=3

Prob0E: 4 iterations in 0.02 seconds (average 0.004000, setup 0.00)

Prob1A: 1 iterations in 0.01 seconds (average 0.008000, setup 0.00)

goal = 1, inf = 0, maybe = 170818

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=170819, nc=225068, nnz=444203, k=4] [6.6 MB]
Building sparse matrix (transition rewards)... [n=170819, nc=225068, nnz=86415, k=4] [2.5 MB]
Creating vector for state rewards... [1.3 MB]
Creating vector for inf... [1.3 MB]
Allocating iteration vectors... [2 x 1.3 MB]
TOTAL: [14.3 MB]

Starting iterations...

Iterative method: 91 iterations in 0.51 seconds (average 0.002549, setup 0.28)

Value in the initial state: 340.9833792918979

Time for model checking: 0.545 seconds.

Result: 340.9833792918979 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"time"}min=? [ F "deadlock" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=2,Objy1=1,Objx2=3,Objy2=3

Prob0A: 4 iterations in 0.02 seconds (average 0.006000, setup 0.00)

Prob1E: 5 iterations in 0.04 seconds (average 0.008000, setup 0.00)

Warning: PRISM hasn't checked for zero-reward loops.
Your minimum rewards may be too low...

goal = 1, inf = 0, maybe = 170818

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=170819, nc=225068, nnz=444203, k=4] [6.6 MB]
Building sparse matrix (transition rewards)... [n=170819, nc=225068, nnz=86415, k=4] [2.5 MB]
Creating vector for state rewards... [1.3 MB]
Creating vector for inf... [1.3 MB]
Allocating iteration vectors... [2 x 1.3 MB]
TOTAL: [14.3 MB]

Starting iterations...

Iterative method: 98 iterations in 0.52 seconds (average 0.002571, setup 0.27)

Value in the initial state: 106.14235845119043

Time for model checking: 0.594 seconds.

Result: 106.14235845119043 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"success"}max=? [ F "deadlock" ]/Pmin=? [ F "MissionSuccessful" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=2,Objy1=1,Objx2=3,Objy2=3

Prob0E: 4 iterations in 0.02 seconds (average 0.006000, setup 0.00)

Prob1A: 1 iterations in 0.00 seconds (average 0.004000, setup 0.00)

goal = 1, inf = 0, maybe = 170818

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=170819, nc=225068, nnz=444203, k=4] [6.6 MB]
Building sparse matrix (transition rewards)... [n=170819, nc=225068, nnz=1880, k=4] [1.5 MB]
Creating vector for state rewards... [1.3 MB]
Creating vector for inf... [1.3 MB]
Allocating iteration vectors... [2 x 1.3 MB]
TOTAL: [13.3 MB]

Starting iterations...

Iterative method: 93 iterations in 0.46 seconds (average 0.002452, setup 0.24)

Prob0E: 65 iterations in 0.36 seconds (average 0.005600, setup 0.00)

Prob1A: 3 iterations in 0.04 seconds (average 0.012000, setup 0.00)

yes = 954, no = 146922, maybe = 22943

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=170819, nc=38880, nnz=105950, k=2] [1.4 MB]
Creating vector for yes... [1.3 MB]
Allocating iteration vectors... [2 x 1.3 MB]
TOTAL: [5.3 MB]

Starting iterations...

Iterative method: 88 iterations in 0.21 seconds (average 0.001000, setup 0.12)

Value in the initial state: 429.1408566568296

Time for model checking: 1.146 seconds.

Result: 429.1408566568296 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"success"}min=? [ F "deadlock" ]/Pmax=? [ F "MissionSuccessful" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=2,Objy1=1,Objx2=3,Objy2=3

Prob0A: 4 iterations in 0.05 seconds (average 0.013000, setup 0.00)

Prob1E: 5 iterations in 0.04 seconds (average 0.008000, setup 0.00)

Warning: PRISM hasn't checked for zero-reward loops.
Your minimum rewards may be too low...

goal = 1, inf = 0, maybe = 170818

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=170819, nc=225068, nnz=444203, k=4] [6.6 MB]
Building sparse matrix (transition rewards)... [n=170819, nc=225068, nnz=1880, k=4] [1.5 MB]
Creating vector for state rewards... [1.3 MB]
Creating vector for inf... [1.3 MB]
Allocating iteration vectors... [2 x 1.3 MB]
TOTAL: [13.3 MB]

Starting iterations...

Iterative method: 91 iterations in 0.48 seconds (average 0.002462, setup 0.25)

Prob0A: 52 iterations in 0.26 seconds (average 0.005077, setup 0.00)

Prob1E: 4 iterations in 0.03 seconds (average 0.007000, setup 0.00)

yes = 954, no = 142730, maybe = 27135

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=170819, nc=46058, nnz=126601, k=2] [1.7 MB]
Creating vector for yes... [1.3 MB]
Allocating iteration vectors... [2 x 1.3 MB]
TOTAL: [5.6 MB]

Starting iterations...

Iterative method: 92 iterations in 0.22 seconds (average 0.001174, setup 0.11)

Value in the initial state: 82.37140529954165

Time for model checking: 1.091 seconds.

Result: 82.37140529954165 (value in the initial state)

---------------------------------------------------------------------

Note: There were 16 warnings during computation.

