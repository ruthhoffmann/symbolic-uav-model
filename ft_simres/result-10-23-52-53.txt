PRISM
=====

Version: 4.3
Date: Tue Aug 02 12:44:01 BST 2016
Hostname: eglinton
Memory limits: cudd=1g, java(heap)=981.5m
Command line: prism uav_sim_ft_mdp.prism -s uav_mdp.props -const 'Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=5,Objy1=2,Objx2=5,Objy2=3,'

Parsing model file "uav_sim_ft_mdp.prism"...

Parsing properties file "uav_mdp.props"...

18 properties:
(1) Pmax=? [ F "MissionSuccessful" ]
(2) Pmin=? [ F "MissionSuccessful" ]
(3) Pmax=? [ F "MissionTimeUp" ]
(4) Pmin=? [ F "MissionTimeUp" ]
(5) Pmax=? [ F "Crash" ]
(6) Pmin=? [ F "Crash" ]
(7) Pmax=? [ F "AllAreaSearched" ]
(8) Pmin=? [ F "AllAreaSearched" ]
(9) Pmax=? [ F "MissionTimeUp"|"AllAreaSearched"|"Crash" ]
(10) Pmin=? [ F "MissionTimeUp"|"AllAreaSearched"|"Crash" ]
(11) R{"drop"}max=? [ F "deadlock" ]
(12) R{"drop"}min=? [ F "deadlock" ]
(13) R{"charge"}max=? [ F "deadlock" ]
(14) R{"charge"}min=? [ F "deadlock" ]
(15) R{"time"}max=? [ F "deadlock" ]
(16) R{"time"}min=? [ F "deadlock" ]
(17) R{"success"}max=? [ F "deadlock" ]/Pmin=? [ F "MissionSuccessful" ]
(18) R{"success"}min=? [ F "deadlock" ]/Pmax=? [ F "MissionSuccessful" ]

Type:        MDP
Modules:     Obj Move Robot T 
Variables:   NoOfObjs posx posy s1 retx rety b t c 

---------------------------------------------------------------------

Model checking: Pmax=? [ F "MissionSuccessful" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=5,Objy1=2,Objx2=5,Objy2=3

Building model...
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=5,Objy1=2,Objx2=5,Objy2=3

Warning: Guard for command 7 of module "Move" is never satisfied.

Warning: Guard for command 8 of module "Move" is never satisfied.

Warning: Guard for command 9 of module "Move" is never satisfied.

Warning: Guard for command 7 of module "Robot" is never satisfied.

Warning: Update 2 of command 3 of module "Robot" doesn't do anything ("(s1'=13)", line 249, column 44)

Warning: Update 2 of command 27 of module "T" doesn't do anything ("(c'=1)", line 372, column 108)

Warning: Guard for command 28 of module "T" is never satisfied.

Warning: Update 2 of command 9 of module "T" doesn't do anything ("(c'=1)", line 349, column 112)

Warning: Guard for command 10 of module "T" is never satisfied.

Warning: Update 2 of command 51 of module "T" doesn't do anything ("(c'=1)", line 403, column 113)

Warning: Guard for command 52 of module "T" is never satisfied.

Computing reachable states...

Reachability (BFS): 82 iterations in 0.49 seconds (average 0.006000, setup 0.00)

Time for model construction: 1.249 seconds.

Warning: Deadlocks detected and fixed in 1 states

Type:        MDP
States:      198890 (1 initial)
Transitions: 577077
Choices:     274083

Transition matrix: 162508 nodes (76 terminal), 577077 minterms, vars: 34r/34c/18nd

Prob0A: 53 iterations in 0.25 seconds (average 0.004679, setup 0.00)

Prob1E: 4 iterations in 0.07 seconds (average 0.018000, setup 0.00)

yes = 930, no = 163252, maybe = 34708

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=198890, nc=62649, nnz=183537, k=2] [2.3 MB]
Creating vector for yes... [1.5 MB]
Allocating iteration vectors... [2 x 1.5 MB]
TOTAL: [6.9 MB]

Starting iterations...

Iterative method: 97 iterations in 0.37 seconds (average 0.001402, setup 0.24)

Value in the initial state: 0.9773341410894809

Time for model checking: 0.716 seconds.

Result: 0.9773341410894809 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmin=? [ F "MissionSuccessful" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=5,Objy1=2,Objx2=5,Objy2=3

Prob0E: 67 iterations in 0.51 seconds (average 0.007642, setup 0.00)

Prob1A: 3 iterations in 0.03 seconds (average 0.010667, setup 0.00)

yes = 930, no = 168421, maybe = 29539

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=198890, nc=53052, nnz=155344, k=2] [2.0 MB]
Creating vector for yes... [1.5 MB]
Allocating iteration vectors... [2 x 1.5 MB]
TOTAL: [6.6 MB]

Starting iterations...

Iterative method: 92 iterations in 0.26 seconds (average 0.001261, setup 0.15)

Value in the initial state: 0.7298346215139312

Time for model checking: 0.827 seconds.

Result: 0.7298346215139312 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmax=? [ F "MissionTimeUp" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=5,Objy1=2,Objx2=5,Objy2=3

Prob0A: 69 iterations in 0.26 seconds (average 0.003826, setup 0.00)

Prob1E: 4 iterations in 0.04 seconds (average 0.009000, setup 0.00)

yes = 252, no = 116090, maybe = 82548

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=198890, nc=147886, nnz=421882, k=4] [5.2 MB]
Creating vector for yes... [1.5 MB]
Allocating iteration vectors... [2 x 1.5 MB]
TOTAL: [9.7 MB]

Starting iterations...

Iterative method: 93 iterations in 0.39 seconds (average 0.001720, setup 0.23)

Value in the initial state: 0.003097653732393926

Time for model checking: 0.71 seconds.

Result: 0.003097653732393926 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmin=? [ F "MissionTimeUp" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=5,Objy1=2,Objx2=5,Objy2=3

Prob0E: 76 iterations in 0.48 seconds (average 0.006316, setup 0.00)

Prob1A: 3 iterations in 0.03 seconds (average 0.010667, setup 0.00)

yes = 252, no = 116504, maybe = 82134

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=198890, nc=147134, nnz=420456, k=4] [5.1 MB]
Creating vector for yes... [1.5 MB]
Allocating iteration vectors... [2 x 1.5 MB]
TOTAL: [9.7 MB]

Starting iterations...

Iterative method: 108 iterations in 0.43 seconds (average 0.001815, setup 0.23)

Value in the initial state: 1.4131682642869662E-5

Time for model checking: 0.975 seconds.

Result: 1.4131682642869662E-5 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmax=? [ F "Crash" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=5,Objy1=2,Objx2=5,Objy2=3

Prob0A: 3 iterations in 0.07 seconds (average 0.022667, setup 0.00)

Prob1E: 256 iterations in 2.24 seconds (average 0.008766, setup 0.00)

yes = 97102, no = 2437, maybe = 99351

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=198890, nc=174544, nnz=477538, k=4] [5.8 MB]
Creating vector for yes... [1.5 MB]
Allocating iteration vectors... [2 x 1.5 MB]
TOTAL: [10.4 MB]

Starting iterations...

Iterative method: 92 iterations in 0.48 seconds (average 0.002217, setup 0.27)

Value in the initial state: 0.0631246525953652

Time for model checking: 2.81 seconds.

Result: 0.0631246525953652 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmin=? [ F "Crash" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=5,Objy1=2,Objx2=5,Objy2=3

Prob0E: 3 iterations in 0.06 seconds (average 0.021333, setup 0.00)

Prob1A: 53 iterations in 0.38 seconds (average 0.007170, setup 0.00)

yes = 97102, no = 2437, maybe = 99351

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=198890, nc=174544, nnz=477538, k=4] [5.8 MB]
Creating vector for yes... [1.5 MB]
Allocating iteration vectors... [2 x 1.5 MB]
TOTAL: [10.4 MB]

Starting iterations...

Iterative method: 98 iterations in 0.51 seconds (average 0.002939, setup 0.22)

Value in the initial state: 0.020615082108814676

Time for model checking: 0.968 seconds.

Result: 0.020615082108814676 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmax=? [ F "AllAreaSearched" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=5,Objy1=2,Objx2=5,Objy2=3

Prob0A: 53 iterations in 0.25 seconds (average 0.004755, setup 0.00)

Prob1E: 4 iterations in 0.03 seconds (average 0.008000, setup 0.00)

yes = 1254, no = 151034, maybe = 46602

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=198890, nc=79228, nnz=223476, k=2] [2.8 MB]
Creating vector for yes... [1.5 MB]
Allocating iteration vectors... [2 x 1.5 MB]
TOTAL: [7.4 MB]

Starting iterations...

Iterative method: 89 iterations in 0.33 seconds (average 0.001483, setup 0.20)

Value in the initial state: 0.2405764750518605

Time for model checking: 0.632 seconds.

Result: 0.2405764750518605 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmin=? [ F "AllAreaSearched" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=5,Objy1=2,Objx2=5,Objy2=3

Prob0E: 71 iterations in 0.32 seconds (average 0.004507, setup 0.00)

Prob1A: 3 iterations in 0.02 seconds (average 0.008000, setup 0.00)

yes = 1254, no = 158652, maybe = 38984

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=198890, nc=65302, nnz=181411, k=2] [2.3 MB]
Creating vector for yes... [1.5 MB]
Allocating iteration vectors... [2 x 1.5 MB]
TOTAL: [6.9 MB]

Starting iterations...

Iterative method: 92 iterations in 0.23 seconds (average 0.001348, setup 0.11)

Value in the initial state: 0.0012738922638195899

Time for model checking: 0.588 seconds.

Result: 0.0012738922638195899 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmax=? [ F "MissionTimeUp"|"AllAreaSearched"|"Crash" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=5,Objy1=2,Objx2=5,Objy2=3

Prob0A: 3 iterations in 0.09 seconds (average 0.030667, setup 0.00)

Prob1E: 260 iterations in 2.62 seconds (average 0.010077, setup 0.00)

yes = 168420, no = 931, maybe = 29539

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=198890, nc=53052, nnz=155344, k=2] [2.0 MB]
Creating vector for yes... [1.5 MB]
Allocating iteration vectors... [2 x 1.5 MB]
TOTAL: [6.6 MB]

Starting iterations...

Iterative method: 86 iterations in 0.34 seconds (average 0.001349, setup 0.22)

Value in the initial state: 0.2701649814580125

Time for model checking: 3.052 seconds.

Result: 0.2701649814580125 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmin=? [ F "MissionTimeUp"|"AllAreaSearched"|"Crash" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=5,Objy1=2,Objx2=5,Objy2=3

Prob0E: 3 iterations in 0.04 seconds (average 0.014667, setup 0.00)

Prob1A: 53 iterations in 0.40 seconds (average 0.007472, setup 0.00)

yes = 163251, no = 931, maybe = 34708

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=198890, nc=62649, nnz=183537, k=2] [2.3 MB]
Creating vector for yes... [1.5 MB]
Allocating iteration vectors... [2 x 1.5 MB]
TOTAL: [6.9 MB]

Starting iterations...

Iterative method: 95 iterations in 0.32 seconds (average 0.001726, setup 0.16)

Value in the initial state: 0.022665276105681875

Time for model checking: 0.79 seconds.

Result: 0.022665276105681875 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"drop"}max=? [ F "deadlock" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=5,Objy1=2,Objx2=5,Objy2=3

Prob0E: 4 iterations in 0.08 seconds (average 0.020000, setup 0.00)

Prob1A: 1 iterations in 0.01 seconds (average 0.008000, setup 0.00)

goal = 1, inf = 0, maybe = 198889

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=198890, nc=274082, nnz=577076, k=4] [8.4 MB]
Building sparse matrix (transition rewards)... [n=198890, nc=274082, nnz=0, k=4] [1.8 MB]
Creating vector for state rewards... [1.5 MB]
Creating vector for inf... [1.5 MB]
Allocating iteration vectors... [2 x 1.5 MB]
TOTAL: [16.3 MB]

Starting iterations...

Iterative method: 93 iterations in 0.61 seconds (average 0.002839, setup 0.34)

Value in the initial state: 0.30505559323868775

Time for model checking: 0.699 seconds.

Result: 0.30505559323868775 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"drop"}min=? [ F "deadlock" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=5,Objy1=2,Objx2=5,Objy2=3

Prob0A: 4 iterations in 0.02 seconds (average 0.005000, setup 0.00)

Prob1E: 5 iterations in 0.03 seconds (average 0.006400, setup 0.00)

Warning: PRISM hasn't checked for zero-reward loops.
Your minimum rewards may be too low...

goal = 1, inf = 0, maybe = 198889

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=198890, nc=274082, nnz=577076, k=4] [8.4 MB]
Building sparse matrix (transition rewards)... [n=198890, nc=274082, nnz=0, k=4] [1.8 MB]
Creating vector for state rewards... [1.5 MB]
Creating vector for inf... [1.5 MB]
Allocating iteration vectors... [2 x 1.5 MB]
TOTAL: [16.3 MB]

Starting iterations...

Iterative method: 85 iterations in 0.68 seconds (average 0.004000, setup 0.34)

Value in the initial state: 0.2047775571883251

Time for model checking: 0.755 seconds.

Result: 0.2047775571883251 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"charge"}max=? [ F "deadlock" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=5,Objy1=2,Objx2=5,Objy2=3

Prob0E: 4 iterations in 0.02 seconds (average 0.005000, setup 0.00)

Prob1A: 1 iterations in 0.00 seconds (average 0.004000, setup 0.00)

goal = 1, inf = 0, maybe = 198889

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=198890, nc=274082, nnz=577076, k=4] [8.4 MB]
Building sparse matrix (transition rewards)... [n=198890, nc=274082, nnz=7514, k=4] [1.9 MB]
Creating vector for state rewards... [1.5 MB]
Creating vector for inf... [1.5 MB]
Allocating iteration vectors... [2 x 1.5 MB]
TOTAL: [16.4 MB]

Starting iterations...

Iterative method: 87 iterations in 0.56 seconds (average 0.003034, setup 0.30)

Value in the initial state: 0.9866491021954186

Time for model checking: 0.593 seconds.

Result: 0.9866491021954186 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"charge"}min=? [ F "deadlock" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=5,Objy1=2,Objx2=5,Objy2=3

Prob0A: 4 iterations in 0.02 seconds (average 0.004000, setup 0.00)

Prob1E: 5 iterations in 0.03 seconds (average 0.006400, setup 0.00)

Warning: PRISM hasn't checked for zero-reward loops.
Your minimum rewards may be too low...

goal = 1, inf = 0, maybe = 198889

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=198890, nc=274082, nnz=577076, k=4] [8.4 MB]
Building sparse matrix (transition rewards)... [n=198890, nc=274082, nnz=7514, k=4] [1.9 MB]
Creating vector for state rewards... [1.5 MB]
Creating vector for inf... [1.5 MB]
Allocating iteration vectors... [2 x 1.5 MB]
TOTAL: [16.4 MB]

Starting iterations...

Iterative method: 99 iterations in 0.56 seconds (average 0.002869, setup 0.28)

Value in the initial state: 0.08326720042032294

Time for model checking: 0.62 seconds.

Result: 0.08326720042032294 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"time"}max=? [ F "deadlock" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=5,Objy1=2,Objx2=5,Objy2=3

Prob0E: 4 iterations in 0.02 seconds (average 0.004000, setup 0.00)

Prob1A: 1 iterations in 0.00 seconds (average 0.004000, setup 0.00)

goal = 1, inf = 0, maybe = 198889

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=198890, nc=274082, nnz=577076, k=4] [8.4 MB]
Building sparse matrix (transition rewards)... [n=198890, nc=274082, nnz=99537, k=4] [2.9 MB]
Creating vector for state rewards... [1.5 MB]
Creating vector for inf... [1.5 MB]
Allocating iteration vectors... [2 x 1.5 MB]
TOTAL: [17.4 MB]

Starting iterations...

Iterative method: 93 iterations in 0.58 seconds (average 0.002882, setup 0.31)

Value in the initial state: 346.6008194964741

Time for model checking: 0.608 seconds.

Result: 346.6008194964741 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"time"}min=? [ F "deadlock" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=5,Objy1=2,Objx2=5,Objy2=3

Prob0A: 4 iterations in 0.02 seconds (average 0.004000, setup 0.00)

Prob1E: 5 iterations in 0.03 seconds (average 0.005600, setup 0.00)

Warning: PRISM hasn't checked for zero-reward loops.
Your minimum rewards may be too low...

goal = 1, inf = 0, maybe = 198889

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=198890, nc=274082, nnz=577076, k=4] [8.4 MB]
Building sparse matrix (transition rewards)... [n=198890, nc=274082, nnz=99537, k=4] [2.9 MB]
Creating vector for state rewards... [1.5 MB]
Creating vector for inf... [1.5 MB]
Allocating iteration vectors... [2 x 1.5 MB]
TOTAL: [17.4 MB]

Starting iterations...

Iterative method: 103 iterations in 0.61 seconds (average 0.002990, setup 0.30)

Value in the initial state: 114.12129709391152

Time for model checking: 0.658 seconds.

Result: 114.12129709391152 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"success"}max=? [ F "deadlock" ]/Pmin=? [ F "MissionSuccessful" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=5,Objy1=2,Objx2=5,Objy2=3

Prob0E: 4 iterations in 0.02 seconds (average 0.004000, setup 0.00)

Prob1A: 1 iterations in 0.00 seconds (average 0.004000, setup 0.00)

goal = 1, inf = 0, maybe = 198889

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=198890, nc=274082, nnz=577076, k=4] [8.4 MB]
Building sparse matrix (transition rewards)... [n=198890, nc=274082, nnz=1832, k=4] [1.8 MB]
Creating vector for state rewards... [1.5 MB]
Creating vector for inf... [1.5 MB]
Allocating iteration vectors... [2 x 1.5 MB]
TOTAL: [16.3 MB]

Starting iterations...

Iterative method: 97 iterations in 0.72 seconds (average 0.003299, setup 0.40)

Prob0E: 67 iterations in 0.26 seconds (average 0.003940, setup 0.00)

Prob1A: 3 iterations in 0.03 seconds (average 0.010667, setup 0.00)

yes = 930, no = 168421, maybe = 29539

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=198890, nc=53052, nnz=155344, k=2] [2.0 MB]
Creating vector for yes... [1.5 MB]
Allocating iteration vectors... [2 x 1.5 MB]
TOTAL: [6.6 MB]

Starting iterations...

Iterative method: 92 iterations in 0.26 seconds (average 0.001348, setup 0.13)

Value in the initial state: 463.2847760224441

Time for model checking: 1.323 seconds.

Result: 463.2847760224441 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"success"}min=? [ F "deadlock" ]/Pmax=? [ F "MissionSuccessful" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=5,Objy1=2,Objx2=5,Objy2=3

Prob0A: 4 iterations in 0.07 seconds (average 0.017000, setup 0.00)

Prob1E: 5 iterations in 0.05 seconds (average 0.010400, setup 0.00)

Warning: PRISM hasn't checked for zero-reward loops.
Your minimum rewards may be too low...

goal = 1, inf = 0, maybe = 198889

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=198890, nc=274082, nnz=577076, k=4] [8.4 MB]
Building sparse matrix (transition rewards)... [n=198890, nc=274082, nnz=1832, k=4] [1.8 MB]
Creating vector for state rewards... [1.5 MB]
Creating vector for inf... [1.5 MB]
Allocating iteration vectors... [2 x 1.5 MB]
TOTAL: [16.3 MB]

Starting iterations...

Iterative method: 96 iterations in 0.67 seconds (average 0.003042, setup 0.38)

Prob0A: 53 iterations in 0.19 seconds (average 0.003623, setup 0.00)

Prob1E: 4 iterations in 0.02 seconds (average 0.006000, setup 0.00)

yes = 930, no = 163252, maybe = 34708

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=198890, nc=62649, nnz=183537, k=2] [2.3 MB]
Creating vector for yes... [1.5 MB]
Allocating iteration vectors... [2 x 1.5 MB]
TOTAL: [6.9 MB]

Starting iterations...

Iterative method: 97 iterations in 0.26 seconds (average 0.001361, setup 0.12)

Value in the initial state: 80.893767235118

Time for model checking: 1.271 seconds.

Result: 80.893767235118 (value in the initial state)

---------------------------------------------------------------------

Note: There were 16 warnings during computation.

