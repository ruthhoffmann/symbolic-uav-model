PRISM
=====

Version: 4.3
Date: Tue Aug 02 12:39:30 BST 2016
Hostname: eglinton
Memory limits: cudd=1g, java(heap)=981.5m
Command line: prism uav_sim_ft_mdp.prism -s uav_mdp.props -const 'Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=2,Objy1=3,Objx2=5,Objy2=1,'

Parsing model file "uav_sim_ft_mdp.prism"...

Parsing properties file "uav_mdp.props"...

18 properties:
(1) Pmax=? [ F "MissionSuccessful" ]
(2) Pmin=? [ F "MissionSuccessful" ]
(3) Pmax=? [ F "MissionTimeUp" ]
(4) Pmin=? [ F "MissionTimeUp" ]
(5) Pmax=? [ F "Crash" ]
(6) Pmin=? [ F "Crash" ]
(7) Pmax=? [ F "AllAreaSearched" ]
(8) Pmin=? [ F "AllAreaSearched" ]
(9) Pmax=? [ F "MissionTimeUp"|"AllAreaSearched"|"Crash" ]
(10) Pmin=? [ F "MissionTimeUp"|"AllAreaSearched"|"Crash" ]
(11) R{"drop"}max=? [ F "deadlock" ]
(12) R{"drop"}min=? [ F "deadlock" ]
(13) R{"charge"}max=? [ F "deadlock" ]
(14) R{"charge"}min=? [ F "deadlock" ]
(15) R{"time"}max=? [ F "deadlock" ]
(16) R{"time"}min=? [ F "deadlock" ]
(17) R{"success"}max=? [ F "deadlock" ]/Pmin=? [ F "MissionSuccessful" ]
(18) R{"success"}min=? [ F "deadlock" ]/Pmax=? [ F "MissionSuccessful" ]

Type:        MDP
Modules:     Obj Move Robot T 
Variables:   NoOfObjs posx posy s1 retx rety b t c 

---------------------------------------------------------------------

Model checking: Pmax=? [ F "MissionSuccessful" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=2,Objy1=3,Objx2=5,Objy2=1

Building model...
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=2,Objy1=3,Objx2=5,Objy2=1

Warning: Guard for command 7 of module "Move" is never satisfied.

Warning: Guard for command 8 of module "Move" is never satisfied.

Warning: Guard for command 9 of module "Move" is never satisfied.

Warning: Guard for command 7 of module "Robot" is never satisfied.

Warning: Update 2 of command 3 of module "Robot" doesn't do anything ("(s1'=13)", line 249, column 44)

Warning: Update 2 of command 27 of module "T" doesn't do anything ("(c'=1)", line 372, column 108)

Warning: Guard for command 28 of module "T" is never satisfied.

Warning: Update 2 of command 9 of module "T" doesn't do anything ("(c'=1)", line 349, column 112)

Warning: Guard for command 10 of module "T" is never satisfied.

Warning: Update 2 of command 51 of module "T" doesn't do anything ("(c'=1)", line 403, column 113)

Warning: Guard for command 52 of module "T" is never satisfied.

Computing reachable states...

Reachability (BFS): 78 iterations in 0.64 seconds (average 0.008154, setup 0.00)

Time for model construction: 1.76 seconds.

Warning: Deadlocks detected and fixed in 1 states

Type:        MDP
States:      127357 (1 initial)
Transitions: 309417
Choices:     161311

Transition matrix: 157711 nodes (76 terminal), 309417 minterms, vars: 34r/34c/18nd

Prob0A: 56 iterations in 0.22 seconds (average 0.003929, setup 0.00)

Prob1E: 4 iterations in 0.04 seconds (average 0.010000, setup 0.00)

yes = 210, no = 120639, maybe = 6508

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=127357, nc=11922, nnz=35449, k=2] [551.4 KB]
Creating vector for yes... [995.0 KB]
Allocating iteration vectors... [2 x 995.0 KB]
TOTAL: [3.5 MB]

Starting iterations...

Iterative method: 88 iterations in 0.15 seconds (average 0.000727, setup 0.09)

Value in the initial state: 0.002426813863906017

Time for model checking: 0.441 seconds.

Result: 0.002426813863906017 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmin=? [ F "MissionSuccessful" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=2,Objy1=3,Objx2=5,Objy2=1

Prob0E: 22 iterations in 0.09 seconds (average 0.004000, setup 0.00)

Prob1A: 3 iterations in 0.02 seconds (average 0.006667, setup 0.00)

yes = 210, no = 122037, maybe = 5110

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=127357, nc=9352, nnz=27541, k=2] [456.3 KB]
Creating vector for yes... [995.0 KB]
Allocating iteration vectors... [2 x 995.0 KB]
TOTAL: [3.4 MB]

Starting iterations...

Iterative method: 42 iterations in 0.06 seconds (average 0.000667, setup 0.03)

Value in the initial state: 0.0

Time for model checking: 0.164 seconds.

Result: 0.0 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmax=? [ F "MissionTimeUp" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=2,Objy1=3,Objx2=5,Objy2=1

Prob0A: 60 iterations in 0.45 seconds (average 0.007533, setup 0.00)

Prob1E: 4 iterations in 0.04 seconds (average 0.010000, setup 0.00)

yes = 356, no = 89414, maybe = 37587

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=127357, nc=65188, nnz=180440, k=4] [2.2 MB]
Creating vector for yes... [995.0 KB]
Allocating iteration vectors... [2 x 995.0 KB]
TOTAL: [5.2 MB]

Starting iterations...

Iterative method: 90 iterations in 0.28 seconds (average 0.001022, setup 0.18)

Value in the initial state: 8.559970830071744E-5

Time for model checking: 0.784 seconds.

Result: 8.559970830071744E-5 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmin=? [ F "MissionTimeUp" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=2,Objy1=3,Objx2=5,Objy2=1

Prob0E: 68 iterations in 0.47 seconds (average 0.006882, setup 0.00)

Prob1A: 3 iterations in 0.04 seconds (average 0.012000, setup 0.00)

yes = 356, no = 89666, maybe = 37335

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=127357, nc=64736, nnz=179476, k=4] [2.2 MB]
Creating vector for yes... [995.0 KB]
Allocating iteration vectors... [2 x 995.0 KB]
TOTAL: [5.2 MB]

Starting iterations...

Iterative method: 109 iterations in 0.26 seconds (average 0.000991, setup 0.16)

Value in the initial state: 2.584087037661091E-7

Time for model checking: 0.795 seconds.

Result: 2.584087037661091E-7 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmax=? [ F "Crash" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=2,Objy1=3,Objx2=5,Objy2=1

Prob0A: 3 iterations in 0.07 seconds (average 0.024000, setup 0.00)

Prob1E: 176 iterations in 2.04 seconds (average 0.011614, setup 0.00)

yes = 62467, no = 1708, maybe = 63182

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=127357, nc=97136, nnz=245242, k=4] [3.0 MB]
Creating vector for yes... [995.0 KB]
Allocating iteration vectors... [2 x 995.0 KB]
TOTAL: [5.9 MB]

Starting iterations...

Iterative method: 88 iterations in 0.32 seconds (average 0.001136, setup 0.22)

Value in the initial state: 0.016861227030392355

Time for model checking: 2.456 seconds.

Result: 0.016861227030392355 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmin=? [ F "Crash" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=2,Objy1=3,Objx2=5,Objy2=1

Prob0E: 3 iterations in 0.05 seconds (average 0.017333, setup 0.00)

Prob1A: 45 iterations in 0.39 seconds (average 0.008622, setup 0.00)

yes = 62467, no = 1708, maybe = 63182

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=127357, nc=97136, nnz=245242, k=4] [3.0 MB]
Creating vector for yes... [995.0 KB]
Allocating iteration vectors... [2 x 995.0 KB]
TOTAL: [5.9 MB]

Starting iterations...

Iterative method: 83 iterations in 0.22 seconds (average 0.001157, setup 0.13)

Value in the initial state: 0.013126737195644046

Time for model checking: 0.691 seconds.

Result: 0.013126737195644046 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmax=? [ F "AllAreaSearched" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=2,Objy1=3,Objx2=5,Objy2=1

Prob0A: 45 iterations in 0.30 seconds (average 0.006667, setup 0.00)

Prob1E: 4 iterations in 0.04 seconds (average 0.009000, setup 0.00)

yes = 1141, no = 89758, maybe = 36458

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=127357, nc=51503, nnz=127386, k=2] [1.6 MB]
Creating vector for yes... [995.0 KB]
Allocating iteration vectors... [2 x 995.0 KB]
TOTAL: [4.5 MB]

Starting iterations...

Iterative method: 88 iterations in 0.23 seconds (average 0.000955, setup 0.15)

Value in the initial state: 0.9868710424455778

Time for model checking: 0.581 seconds.

Result: 0.9868710424455778 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmin=? [ F "AllAreaSearched" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=2,Objy1=3,Objx2=5,Objy2=1

Prob0E: 49 iterations in 0.23 seconds (average 0.004735, setup 0.00)

Prob1A: 3 iterations in 0.02 seconds (average 0.008000, setup 0.00)

yes = 1141, no = 92110, maybe = 34106

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=127357, nc=47046, nnz=114004, k=2] [1.5 MB]
Creating vector for yes... [995.0 KB]
Allocating iteration vectors... [2 x 995.0 KB]
TOTAL: [4.4 MB]

Starting iterations...

Iterative method: 90 iterations in 0.17 seconds (average 0.000933, setup 0.09)

Value in the initial state: 0.9807766441541617

Time for model checking: 0.44 seconds.

Result: 0.9807766441541617 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmax=? [ F "MissionTimeUp"|"AllAreaSearched"|"Crash" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=2,Objy1=3,Objx2=5,Objy2=1

Prob0A: 3 iterations in 0.08 seconds (average 0.028000, setup 0.00)

Prob1E: 84 iterations in 0.70 seconds (average 0.008286, setup 0.00)

yes = 122036, no = 211, maybe = 5110

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=127357, nc=9352, nnz=27541, k=2] [456.3 KB]
Creating vector for yes... [995.0 KB]
Allocating iteration vectors... [2 x 995.0 KB]
TOTAL: [3.4 MB]

Starting iterations...

Iterative method: 40 iterations in 0.07 seconds (average 0.000700, setup 0.04)

Value in the initial state: 1.0

Time for model checking: 0.836 seconds.

Result: 1.0 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmin=? [ F "MissionTimeUp"|"AllAreaSearched"|"Crash" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=2,Objy1=3,Objx2=5,Objy2=1

Prob0E: 3 iterations in 0.02 seconds (average 0.008000, setup 0.00)

Prob1A: 56 iterations in 0.30 seconds (average 0.005429, setup 0.00)

yes = 120638, no = 211, maybe = 6508

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=127357, nc=11922, nnz=35449, k=2] [551.4 KB]
Creating vector for yes... [995.0 KB]
Allocating iteration vectors... [2 x 995.0 KB]
TOTAL: [3.5 MB]

Starting iterations...

Iterative method: 59 iterations in 0.10 seconds (average 0.000746, setup 0.06)

Value in the initial state: 0.9975709528071858

Time for model checking: 0.437 seconds.

Result: 0.9975709528071858 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"drop"}max=? [ F "deadlock" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=2,Objy1=3,Objx2=5,Objy2=1

Prob0E: 4 iterations in 0.04 seconds (average 0.011000, setup 0.00)

Prob1A: 1 iterations in 0.00 seconds (average 0.004000, setup 0.00)

goal = 1, inf = 0, maybe = 127356

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=127357, nc=161310, nnz=309416, k=4] [4.6 MB]
Building sparse matrix (transition rewards)... [n=127357, nc=161310, nnz=0, k=4] [1.1 MB]
Creating vector for state rewards... [995.0 KB]
Creating vector for inf... [995.0 KB]
Allocating iteration vectors... [2 x 995.0 KB]
TOTAL: [9.6 MB]

Starting iterations...

Iterative method: 70 iterations in 0.38 seconds (average 0.001771, setup 0.26)

Value in the initial state: 0.11010558991335015

Time for model checking: 0.437 seconds.

Result: 0.11010558991335015 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"drop"}min=? [ F "deadlock" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=2,Objy1=3,Objx2=5,Objy2=1

Prob0A: 4 iterations in 0.02 seconds (average 0.006000, setup 0.00)

Prob1E: 5 iterations in 0.03 seconds (average 0.006400, setup 0.00)

Warning: PRISM hasn't checked for zero-reward loops.
Your minimum rewards may be too low...

goal = 1, inf = 0, maybe = 127356

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=127357, nc=161310, nnz=309416, k=4] [4.6 MB]
Building sparse matrix (transition rewards)... [n=127357, nc=161310, nnz=0, k=4] [1.1 MB]
Creating vector for state rewards... [995.0 KB]
Creating vector for inf... [995.0 KB]
Allocating iteration vectors... [2 x 995.0 KB]
TOTAL: [9.6 MB]

Starting iterations...

Iterative method: 62 iterations in 0.30 seconds (average 0.001742, setup 0.19)

Value in the initial state: 0.10968896160792455

Time for model checking: 0.36 seconds.

Result: 0.10968896160792455 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"charge"}max=? [ F "deadlock" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=2,Objy1=3,Objx2=5,Objy2=1

Prob0E: 4 iterations in 0.02 seconds (average 0.004000, setup 0.00)

Prob1A: 1 iterations in 0.00 seconds (average 0.004000, setup 0.00)

goal = 1, inf = 0, maybe = 127356

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=127357, nc=161310, nnz=309416, k=4] [4.6 MB]
Building sparse matrix (transition rewards)... [n=127357, nc=161310, nnz=2949, k=4] [1.1 MB]
Creating vector for state rewards... [995.0 KB]
Creating vector for inf... [995.0 KB]
Allocating iteration vectors... [2 x 995.0 KB]
TOTAL: [9.7 MB]

Starting iterations...

Iterative method: 79 iterations in 0.33 seconds (average 0.001772, setup 0.19)

Value in the initial state: 0.0064943774237106126

Time for model checking: 0.357 seconds.

Result: 0.0064943774237106126 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"charge"}min=? [ F "deadlock" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=2,Objy1=3,Objx2=5,Objy2=1

Prob0A: 4 iterations in 0.02 seconds (average 0.005000, setup 0.00)

Prob1E: 5 iterations in 0.03 seconds (average 0.005600, setup 0.00)

Warning: PRISM hasn't checked for zero-reward loops.
Your minimum rewards may be too low...

goal = 1, inf = 0, maybe = 127356

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=127357, nc=161310, nnz=309416, k=4] [4.6 MB]
Building sparse matrix (transition rewards)... [n=127357, nc=161310, nnz=2949, k=4] [1.1 MB]
Creating vector for state rewards... [995.0 KB]
Creating vector for inf... [995.0 KB]
Allocating iteration vectors... [2 x 995.0 KB]
TOTAL: [9.7 MB]

Starting iterations...

Iterative method: 90 iterations in 0.34 seconds (average 0.001778, setup 0.18)

Value in the initial state: 5.240170477093026E-4

Time for model checking: 0.404 seconds.

Result: 5.240170477093026E-4 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"time"}max=? [ F "deadlock" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=2,Objy1=3,Objx2=5,Objy2=1

Prob0E: 4 iterations in 0.02 seconds (average 0.004000, setup 0.00)

Prob1A: 1 iterations in 0.00 seconds (average 0.004000, setup 0.00)

goal = 1, inf = 0, maybe = 127356

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=127357, nc=161310, nnz=309416, k=4] [4.6 MB]
Building sparse matrix (transition rewards)... [n=127357, nc=161310, nnz=64173, k=4] [1.8 MB]
Creating vector for state rewards... [995.0 KB]
Creating vector for inf... [995.0 KB]
Allocating iteration vectors... [2 x 995.0 KB]
TOTAL: [10.4 MB]

Starting iterations...

Iterative method: 93 iterations in 0.38 seconds (average 0.001806, setup 0.21)

Value in the initial state: 93.48303002005666

Time for model checking: 0.416 seconds.

Result: 93.48303002005666 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"time"}min=? [ F "deadlock" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=2,Objy1=3,Objx2=5,Objy2=1

Prob0A: 4 iterations in 0.02 seconds (average 0.004000, setup 0.00)

Prob1E: 5 iterations in 0.03 seconds (average 0.006400, setup 0.00)

Warning: PRISM hasn't checked for zero-reward loops.
Your minimum rewards may be too low...

goal = 1, inf = 0, maybe = 127356

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=127357, nc=161310, nnz=309416, k=4] [4.6 MB]
Building sparse matrix (transition rewards)... [n=127357, nc=161310, nnz=64173, k=4] [1.8 MB]
Creating vector for state rewards... [995.0 KB]
Creating vector for inf... [995.0 KB]
Allocating iteration vectors... [2 x 995.0 KB]
TOTAL: [10.4 MB]

Starting iterations...

Iterative method: 95 iterations in 0.38 seconds (average 0.001811, setup 0.20)

Value in the initial state: 72.87412261366214

Time for model checking: 0.439 seconds.

Result: 72.87412261366214 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"success"}max=? [ F "deadlock" ]/Pmin=? [ F "MissionSuccessful" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=2,Objy1=3,Objx2=5,Objy2=1

Prob0E: 4 iterations in 0.02 seconds (average 0.005000, setup 0.00)

Prob1A: 1 iterations in 0.00 seconds (average 0.004000, setup 0.00)

goal = 1, inf = 0, maybe = 127356

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=127357, nc=161310, nnz=309416, k=4] [4.6 MB]
Building sparse matrix (transition rewards)... [n=127357, nc=161310, nnz=414, k=4] [1.1 MB]
Creating vector for state rewards... [995.0 KB]
Creating vector for inf... [995.0 KB]
Allocating iteration vectors... [2 x 995.0 KB]
TOTAL: [9.6 MB]

Starting iterations...

Iterative method: 86 iterations in 0.33 seconds (average 0.001767, setup 0.18)

Prob0E: 22 iterations in 0.05 seconds (average 0.002364, setup 0.00)

Prob1A: 3 iterations in 0.01 seconds (average 0.004000, setup 0.00)

yes = 210, no = 122037, maybe = 5110

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=127357, nc=9352, nnz=27541, k=2] [456.3 KB]
Creating vector for yes... [995.0 KB]
Allocating iteration vectors... [2 x 995.0 KB]
TOTAL: [3.4 MB]

Starting iterations...

Iterative method: 42 iterations in 0.05 seconds (average 0.000762, setup 0.02)

Value in the initial state: Infinity

Time for model checking: 0.491 seconds.

Result: Infinity (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"success"}min=? [ F "deadlock" ]/Pmax=? [ F "MissionSuccessful" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=2,Objy1=3,Objx2=5,Objy2=1

Prob0A: 4 iterations in 0.03 seconds (average 0.008000, setup 0.00)

Prob1E: 5 iterations in 0.03 seconds (average 0.006400, setup 0.00)

Warning: PRISM hasn't checked for zero-reward loops.
Your minimum rewards may be too low...

goal = 1, inf = 0, maybe = 127356

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=127357, nc=161310, nnz=309416, k=4] [4.6 MB]
Building sparse matrix (transition rewards)... [n=127357, nc=161310, nnz=414, k=4] [1.1 MB]
Creating vector for state rewards... [995.0 KB]
Creating vector for inf... [995.0 KB]
Allocating iteration vectors... [2 x 995.0 KB]
TOTAL: [9.6 MB]

Starting iterations...

Iterative method: 47 iterations in 0.27 seconds (average 0.001702, setup 0.19)

Prob0A: 56 iterations in 0.13 seconds (average 0.002286, setup 0.00)

Prob1E: 4 iterations in 0.02 seconds (average 0.004000, setup 0.00)

yes = 210, no = 120639, maybe = 6508

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=127357, nc=11922, nnz=35449, k=2] [551.4 KB]
Creating vector for yes... [995.0 KB]
Allocating iteration vectors... [2 x 995.0 KB]
TOTAL: [3.5 MB]

Starting iterations...

Iterative method: 88 iterations in 0.10 seconds (average 0.000682, setup 0.04)

Value in the initial state: 0.0

Time for model checking: 0.572 seconds.

Result: 0.0 (value in the initial state)

---------------------------------------------------------------------

Note: There were 16 warnings during computation.

