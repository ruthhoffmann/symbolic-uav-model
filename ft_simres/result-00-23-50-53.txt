PRISM
=====

Version: 4.3
Date: Tue Aug 02 12:31:57 BST 2016
Hostname: eglinton
Memory limits: cudd=1g, java(heap)=981.5m
Command line: prism uav_sim_ft_mdp.prism -s uav_mdp.props -const 'Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=5,Objy1=0,Objx2=5,Objy2=3,'

Parsing model file "uav_sim_ft_mdp.prism"...

Parsing properties file "uav_mdp.props"...

18 properties:
(1) Pmax=? [ F "MissionSuccessful" ]
(2) Pmin=? [ F "MissionSuccessful" ]
(3) Pmax=? [ F "MissionTimeUp" ]
(4) Pmin=? [ F "MissionTimeUp" ]
(5) Pmax=? [ F "Crash" ]
(6) Pmin=? [ F "Crash" ]
(7) Pmax=? [ F "AllAreaSearched" ]
(8) Pmin=? [ F "AllAreaSearched" ]
(9) Pmax=? [ F "MissionTimeUp"|"AllAreaSearched"|"Crash" ]
(10) Pmin=? [ F "MissionTimeUp"|"AllAreaSearched"|"Crash" ]
(11) R{"drop"}max=? [ F "deadlock" ]
(12) R{"drop"}min=? [ F "deadlock" ]
(13) R{"charge"}max=? [ F "deadlock" ]
(14) R{"charge"}min=? [ F "deadlock" ]
(15) R{"time"}max=? [ F "deadlock" ]
(16) R{"time"}min=? [ F "deadlock" ]
(17) R{"success"}max=? [ F "deadlock" ]/Pmin=? [ F "MissionSuccessful" ]
(18) R{"success"}min=? [ F "deadlock" ]/Pmax=? [ F "MissionSuccessful" ]

Type:        MDP
Modules:     Obj Move Robot T 
Variables:   NoOfObjs posx posy s1 retx rety b t c 

---------------------------------------------------------------------

Model checking: Pmax=? [ F "MissionSuccessful" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=5,Objy1=0,Objx2=5,Objy2=3

Building model...
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=5,Objy1=0,Objx2=5,Objy2=3

Warning: Guard for command 7 of module "Move" is never satisfied.

Warning: Guard for command 8 of module "Move" is never satisfied.

Warning: Guard for command 9 of module "Move" is never satisfied.

Warning: Guard for command 7 of module "Robot" is never satisfied.

Warning: Update 2 of command 3 of module "Robot" doesn't do anything ("(s1'=13)", line 249, column 44)

Warning: Update 2 of command 27 of module "T" doesn't do anything ("(c'=1)", line 372, column 108)

Warning: Guard for command 28 of module "T" is never satisfied.

Warning: Update 2 of command 9 of module "T" doesn't do anything ("(c'=1)", line 349, column 112)

Warning: Guard for command 10 of module "T" is never satisfied.

Warning: Update 2 of command 51 of module "T" doesn't do anything ("(c'=1)", line 403, column 113)

Warning: Guard for command 52 of module "T" is never satisfied.

Computing reachable states...

Reachability (BFS): 81 iterations in 0.77 seconds (average 0.009531, setup 0.00)

Time for model construction: 1.729 seconds.

Warning: Deadlocks detected and fixed in 1 states

Type:        MDP
States:      240703 (1 initial)
Transitions: 647362
Choices:     319022

Transition matrix: 176251 nodes (76 terminal), 647362 minterms, vars: 34r/34c/18nd

Prob0A: 57 iterations in 0.36 seconds (average 0.006386, setup 0.00)

Prob1E: 4 iterations in 0.05 seconds (average 0.013000, setup 0.00)

yes = 930, no = 204763, maybe = 35010

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=240703, nc=60031, nnz=171300, k=2] [2.2 MB]
Creating vector for yes... [1.8 MB]
Allocating iteration vectors... [2 x 1.8 MB]
TOTAL: [7.8 MB]

Starting iterations...

Iterative method: 97 iterations in 0.35 seconds (average 0.001608, setup 0.19)

Value in the initial state: 0.9773416800796834

Time for model checking: 0.827 seconds.

Result: 0.9773416800796834 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmin=? [ F "MissionSuccessful" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=5,Objy1=0,Objx2=5,Objy2=3

Prob0E: 67 iterations in 0.42 seconds (average 0.006269, setup 0.00)

Prob1A: 3 iterations in 0.03 seconds (average 0.010667, setup 0.00)

yes = 930, no = 209280, maybe = 30493

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=240703, nc=52396, nnz=149381, k=2] [2.0 MB]
Creating vector for yes... [1.8 MB]
Allocating iteration vectors... [2 x 1.8 MB]
TOTAL: [7.5 MB]

Starting iterations...

Iterative method: 92 iterations in 0.29 seconds (average 0.001478, setup 0.15)

Value in the initial state: 0.7323902875555116

Time for model checking: 0.777 seconds.

Result: 0.7323902875555116 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmax=? [ F "MissionTimeUp" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=5,Objy1=0,Objx2=5,Objy2=3

Prob0A: 59 iterations in 0.50 seconds (average 0.008407, setup 0.00)

Prob1E: 4 iterations in 0.04 seconds (average 0.010000, setup 0.00)

yes = 476, no = 136930, maybe = 103297

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=240703, nc=171704, nnz=470808, k=4] [5.8 MB]
Creating vector for yes... [1.8 MB]
Allocating iteration vectors... [2 x 1.8 MB]
TOTAL: [11.3 MB]

Starting iterations...

Iterative method: 94 iterations in 0.49 seconds (average 0.002085, setup 0.29)

Value in the initial state: 0.00214759571164589

Time for model checking: 1.034 seconds.

Result: 0.00214759571164589 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmin=? [ F "MissionTimeUp" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=5,Objy1=0,Objx2=5,Objy2=3

Prob0E: 65 iterations in 0.71 seconds (average 0.010954, setup 0.00)

Prob1A: 3 iterations in 0.04 seconds (average 0.013333, setup 0.00)

yes = 476, no = 137306, maybe = 102921

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=240703, nc=171028, nnz=469460, k=4] [5.8 MB]
Creating vector for yes... [1.8 MB]
Allocating iteration vectors... [2 x 1.8 MB]
TOTAL: [11.3 MB]

Starting iterations...

Iterative method: 110 iterations in 0.48 seconds (average 0.002109, setup 0.25)

Value in the initial state: 1.2489561186747048E-5

Time for model checking: 1.259 seconds.

Result: 1.2489561186747048E-5 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmax=? [ F "Crash" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=5,Objy1=0,Objx2=5,Objy2=3

Prob0A: 3 iterations in 0.05 seconds (average 0.017333, setup 0.00)

Prob1E: 240 iterations in 2.60 seconds (average 0.010833, setup 0.00)

yes = 117846, no = 2661, maybe = 120196

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=240703, nc=198515, nnz=526855, k=4] [6.4 MB]
Creating vector for yes... [1.8 MB]
Allocating iteration vectors... [2 x 1.8 MB]
TOTAL: [12.0 MB]

Starting iterations...

Iterative method: 92 iterations in 0.52 seconds (average 0.002174, setup 0.32)

Value in the initial state: 0.06305336826532892

Time for model checking: 3.198 seconds.

Result: 0.06305336826532892 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmin=? [ F "Crash" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=5,Objy1=0,Objx2=5,Objy2=3

Prob0E: 3 iterations in 0.05 seconds (average 0.017333, setup 0.00)

Prob1A: 48 iterations in 0.54 seconds (average 0.011167, setup 0.00)

yes = 117846, no = 2661, maybe = 120196

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=240703, nc=198515, nnz=526855, k=4] [6.4 MB]
Creating vector for yes... [1.8 MB]
Allocating iteration vectors... [2 x 1.8 MB]
TOTAL: [12.0 MB]

Starting iterations...

Iterative method: 98 iterations in 0.50 seconds (average 0.002245, setup 0.28)

Value in the initial state: 0.020625334559622538

Time for model checking: 1.125 seconds.

Result: 0.020625334559622538 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmax=? [ F "AllAreaSearched" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=5,Objy1=0,Objx2=5,Objy2=3

Prob0A: 59 iterations in 0.41 seconds (average 0.006983, setup 0.00)

Prob1E: 4 iterations in 0.04 seconds (average 0.010000, setup 0.00)

yes = 1254, no = 190093, maybe = 49356

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=240703, nc=80205, nnz=219767, k=2] [2.8 MB]
Creating vector for yes... [1.8 MB]
Allocating iteration vectors... [2 x 1.8 MB]
TOTAL: [8.3 MB]

Starting iterations...

Iterative method: 90 iterations in 0.33 seconds (average 0.001733, setup 0.18)

Value in the initial state: 0.23819287679341775

Time for model checking: 0.807 seconds.

Result: 0.23819287679341775 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmin=? [ F "AllAreaSearched" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=5,Objy1=0,Objx2=5,Objy2=3

Prob0E: 71 iterations in 0.39 seconds (average 0.005521, setup 0.00)

Prob1A: 3 iterations in 0.04 seconds (average 0.012000, setup 0.00)

yes = 1254, no = 197954, maybe = 41495

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=240703, nc=67698, nnz=185384, k=2] [2.4 MB]
Creating vector for yes... [1.8 MB]
Allocating iteration vectors... [2 x 1.8 MB]
TOTAL: [7.9 MB]

Starting iterations...

Iterative method: 90 iterations in 0.28 seconds (average 0.001644, setup 0.13)

Value in the initial state: 0.001278805637500928

Time for model checking: 0.717 seconds.

Result: 0.001278805637500928 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmax=? [ F "MissionTimeUp"|"AllAreaSearched"|"Crash" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=5,Objy1=0,Objx2=5,Objy2=3

Prob0A: 3 iterations in 0.10 seconds (average 0.032000, setup 0.00)

Prob1E: 260 iterations in 3.39 seconds (average 0.013031, setup 0.00)

yes = 209279, no = 931, maybe = 30493

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=240703, nc=52396, nnz=149381, k=2] [2.0 MB]
Creating vector for yes... [1.8 MB]
Allocating iteration vectors... [2 x 1.8 MB]
TOTAL: [7.5 MB]

Starting iterations...

Iterative method: 86 iterations in 0.48 seconds (average 0.002465, setup 0.26)

Value in the initial state: 0.26760923027673944

Time for model checking: 3.984 seconds.

Result: 0.26760923027673944 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmin=? [ F "MissionTimeUp"|"AllAreaSearched"|"Crash" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=5,Objy1=0,Objx2=5,Objy2=3

Prob0E: 3 iterations in 0.06 seconds (average 0.021333, setup 0.00)

Prob1A: 57 iterations in 0.54 seconds (average 0.009474, setup 0.00)

yes = 204762, no = 931, maybe = 35010

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=240703, nc=60031, nnz=171300, k=2] [2.2 MB]
Creating vector for yes... [1.8 MB]
Allocating iteration vectors... [2 x 1.8 MB]
TOTAL: [7.8 MB]

Starting iterations...

Iterative method: 95 iterations in 0.31 seconds (average 0.001684, setup 0.15)

Value in the initial state: 0.022657743156873435

Time for model checking: 0.946 seconds.

Result: 0.022657743156873435 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"drop"}max=? [ F "deadlock" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=5,Objy1=0,Objx2=5,Objy2=3

Prob0E: 4 iterations in 0.05 seconds (average 0.013000, setup 0.00)

Prob1A: 1 iterations in 0.01 seconds (average 0.008000, setup 0.00)

goal = 1, inf = 0, maybe = 240702

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=240703, nc=319021, nnz=647361, k=4] [9.5 MB]
Building sparse matrix (transition rewards)... [n=240703, nc=319021, nnz=0, k=4] [2.1 MB]
Creating vector for state rewards... [1.8 MB]
Creating vector for inf... [1.8 MB]
Allocating iteration vectors... [2 x 1.8 MB]
TOTAL: [19.0 MB]

Starting iterations...

Iterative method: 93 iterations in 0.92 seconds (average 0.004688, setup 0.48)

Value in the initial state: 0.30485597837385947

Time for model checking: 0.996 seconds.

Result: 0.30485597837385947 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"drop"}min=? [ F "deadlock" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=5,Objy1=0,Objx2=5,Objy2=3

Prob0A: 4 iterations in 0.04 seconds (average 0.010000, setup 0.00)

Prob1E: 5 iterations in 0.04 seconds (average 0.008000, setup 0.00)

Warning: PRISM hasn't checked for zero-reward loops.
Your minimum rewards may be too low...

goal = 1, inf = 0, maybe = 240702

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=240703, nc=319021, nnz=647361, k=4] [9.5 MB]
Building sparse matrix (transition rewards)... [n=240703, nc=319021, nnz=0, k=4] [2.1 MB]
Creating vector for state rewards... [1.8 MB]
Creating vector for inf... [1.8 MB]
Allocating iteration vectors... [2 x 1.8 MB]
TOTAL: [19.0 MB]

Starting iterations...

Iterative method: 85 iterations in 0.70 seconds (average 0.004188, setup 0.35)

Value in the initial state: 0.20541183756116174

Time for model checking: 0.796 seconds.

Result: 0.20541183756116174 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"charge"}max=? [ F "deadlock" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=5,Objy1=0,Objx2=5,Objy2=3

Prob0E: 4 iterations in 0.04 seconds (average 0.011000, setup 0.00)

Prob1A: 1 iterations in 0.00 seconds (average 0.004000, setup 0.00)

goal = 1, inf = 0, maybe = 240702

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=240703, nc=319021, nnz=647361, k=4] [9.5 MB]
Building sparse matrix (transition rewards)... [n=240703, nc=319021, nnz=7514, k=4] [2.2 MB]
Creating vector for state rewards... [1.8 MB]
Creating vector for inf... [1.8 MB]
Allocating iteration vectors... [2 x 1.8 MB]
TOTAL: [19.1 MB]

Starting iterations...

Iterative method: 88 iterations in 0.85 seconds (average 0.004045, setup 0.49)

Value in the initial state: 0.9803548176094857

Time for model checking: 0.912 seconds.

Result: 0.9803548176094857 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"charge"}min=? [ F "deadlock" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=5,Objy1=0,Objx2=5,Objy2=3

Prob0A: 4 iterations in 0.04 seconds (average 0.009000, setup 0.00)

Prob1E: 5 iterations in 0.05 seconds (average 0.010400, setup 0.00)

Warning: PRISM hasn't checked for zero-reward loops.
Your minimum rewards may be too low...

goal = 1, inf = 0, maybe = 240702

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=240703, nc=319021, nnz=647361, k=4] [9.5 MB]
Building sparse matrix (transition rewards)... [n=240703, nc=319021, nnz=7514, k=4] [2.2 MB]
Creating vector for state rewards... [1.8 MB]
Creating vector for inf... [1.8 MB]
Allocating iteration vectors... [2 x 1.8 MB]
TOTAL: [19.1 MB]

Starting iterations...

Iterative method: 99 iterations in 0.83 seconds (average 0.003596, setup 0.47)

Value in the initial state: 0.08078383684984601

Time for model checking: 0.94 seconds.

Result: 0.08078383684984601 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"time"}max=? [ F "deadlock" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=5,Objy1=0,Objx2=5,Objy2=3

Prob0E: 4 iterations in 0.02 seconds (average 0.006000, setup 0.00)

Prob1A: 1 iterations in 0.00 seconds (average 0.004000, setup 0.00)

goal = 1, inf = 0, maybe = 240702

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=240703, nc=319021, nnz=647361, k=4] [9.5 MB]
Building sparse matrix (transition rewards)... [n=240703, nc=319021, nnz=120505, k=4] [3.5 MB]
Creating vector for state rewards... [1.8 MB]
Creating vector for inf... [1.8 MB]
Allocating iteration vectors... [2 x 1.8 MB]
TOTAL: [20.4 MB]

Starting iterations...

Iterative method: 94 iterations in 0.79 seconds (average 0.004426, setup 0.37)

Value in the initial state: 346.3388589925978

Time for model checking: 0.839 seconds.

Result: 346.3388589925978 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"time"}min=? [ F "deadlock" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=5,Objy1=0,Objx2=5,Objy2=3

Prob0A: 4 iterations in 0.05 seconds (average 0.013000, setup 0.00)

Prob1E: 5 iterations in 0.08 seconds (average 0.016000, setup 0.00)

Warning: PRISM hasn't checked for zero-reward loops.
Your minimum rewards may be too low...

goal = 1, inf = 0, maybe = 240702

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=240703, nc=319021, nnz=647361, k=4] [9.5 MB]
Building sparse matrix (transition rewards)... [n=240703, nc=319021, nnz=120505, k=4] [3.5 MB]
Creating vector for state rewards... [1.8 MB]
Creating vector for inf... [1.8 MB]
Allocating iteration vectors... [2 x 1.8 MB]
TOTAL: [20.4 MB]

Starting iterations...

Iterative method: 103 iterations in 0.92 seconds (average 0.004388, setup 0.47)

Value in the initial state: 114.1787064549774

Time for model checking: 1.079 seconds.

Result: 114.1787064549774 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"success"}max=? [ F "deadlock" ]/Pmin=? [ F "MissionSuccessful" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=5,Objy1=0,Objx2=5,Objy2=3

Prob0E: 4 iterations in 0.02 seconds (average 0.006000, setup 0.00)

Prob1A: 1 iterations in 0.00 seconds (average 0.004000, setup 0.00)

goal = 1, inf = 0, maybe = 240702

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=240703, nc=319021, nnz=647361, k=4] [9.5 MB]
Building sparse matrix (transition rewards)... [n=240703, nc=319021, nnz=1832, k=4] [2.2 MB]
Creating vector for state rewards... [1.8 MB]
Creating vector for inf... [1.8 MB]
Allocating iteration vectors... [2 x 1.8 MB]
TOTAL: [19.0 MB]

Starting iterations...

Iterative method: 95 iterations in 0.67 seconds (average 0.003537, setup 0.33)

Prob0E: 67 iterations in 0.29 seconds (average 0.004358, setup 0.00)

Prob1A: 3 iterations in 0.04 seconds (average 0.012000, setup 0.00)

yes = 930, no = 209280, maybe = 30493

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=240703, nc=52396, nnz=149381, k=2] [2.0 MB]
Creating vector for yes... [1.8 MB]
Allocating iteration vectors... [2 x 1.8 MB]
TOTAL: [7.5 MB]

Starting iterations...

Iterative method: 92 iterations in 0.25 seconds (average 0.001391, setup 0.12)

Value in the initial state: 461.56756577362324

Time for model checking: 1.315 seconds.

Result: 461.56756577362324 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"success"}min=? [ F "deadlock" ]/Pmax=? [ F "MissionSuccessful" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=5,Objy1=0,Objx2=5,Objy2=3

Prob0A: 4 iterations in 0.07 seconds (average 0.017000, setup 0.00)

Prob1E: 5 iterations in 0.04 seconds (average 0.007200, setup 0.00)

Warning: PRISM hasn't checked for zero-reward loops.
Your minimum rewards may be too low...

goal = 1, inf = 0, maybe = 240702

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=240703, nc=319021, nnz=647361, k=4] [9.5 MB]
Building sparse matrix (transition rewards)... [n=240703, nc=319021, nnz=1832, k=4] [2.2 MB]
Creating vector for state rewards... [1.8 MB]
Creating vector for inf... [1.8 MB]
Allocating iteration vectors... [2 x 1.8 MB]
TOTAL: [19.0 MB]

Starting iterations...

Iterative method: 96 iterations in 0.64 seconds (average 0.003208, setup 0.33)

Prob0A: 57 iterations in 0.23 seconds (average 0.004070, setup 0.00)

Prob1E: 4 iterations in 0.03 seconds (average 0.007000, setup 0.00)

yes = 930, no = 204763, maybe = 35010

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=240703, nc=60031, nnz=171300, k=2] [2.2 MB]
Creating vector for yes... [1.8 MB]
Allocating iteration vectors... [2 x 1.8 MB]
TOTAL: [7.8 MB]

Starting iterations...

Iterative method: 97 iterations in 0.27 seconds (average 0.001443, setup 0.13)

Value in the initial state: 81.76821049741963

Time for model checking: 1.273 seconds.

Result: 81.76821049741963 (value in the initial state)

---------------------------------------------------------------------

Note: There were 16 warnings during computation.

