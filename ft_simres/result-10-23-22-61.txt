PRISM
=====

Version: 4.3
Date: Tue Aug 02 12:39:06 BST 2016
Hostname: eglinton
Memory limits: cudd=1g, java(heap)=981.5m
Command line: prism uav_sim_ft_mdp.prism -s uav_mdp.props -const 'Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=2,Objy1=2,Objx2=6,Objy2=1,'

Parsing model file "uav_sim_ft_mdp.prism"...

Parsing properties file "uav_mdp.props"...

18 properties:
(1) Pmax=? [ F "MissionSuccessful" ]
(2) Pmin=? [ F "MissionSuccessful" ]
(3) Pmax=? [ F "MissionTimeUp" ]
(4) Pmin=? [ F "MissionTimeUp" ]
(5) Pmax=? [ F "Crash" ]
(6) Pmin=? [ F "Crash" ]
(7) Pmax=? [ F "AllAreaSearched" ]
(8) Pmin=? [ F "AllAreaSearched" ]
(9) Pmax=? [ F "MissionTimeUp"|"AllAreaSearched"|"Crash" ]
(10) Pmin=? [ F "MissionTimeUp"|"AllAreaSearched"|"Crash" ]
(11) R{"drop"}max=? [ F "deadlock" ]
(12) R{"drop"}min=? [ F "deadlock" ]
(13) R{"charge"}max=? [ F "deadlock" ]
(14) R{"charge"}min=? [ F "deadlock" ]
(15) R{"time"}max=? [ F "deadlock" ]
(16) R{"time"}min=? [ F "deadlock" ]
(17) R{"success"}max=? [ F "deadlock" ]/Pmin=? [ F "MissionSuccessful" ]
(18) R{"success"}min=? [ F "deadlock" ]/Pmax=? [ F "MissionSuccessful" ]

Type:        MDP
Modules:     Obj Move Robot T 
Variables:   NoOfObjs posx posy s1 retx rety b t c 

---------------------------------------------------------------------

Model checking: Pmax=? [ F "MissionSuccessful" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=2,Objy1=2,Objx2=6,Objy2=1

Building model...
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=2,Objy1=2,Objx2=6,Objy2=1

Warning: Guard for command 7 of module "Move" is never satisfied.

Warning: Guard for command 8 of module "Move" is never satisfied.

Warning: Guard for command 9 of module "Move" is never satisfied.

Warning: Guard for command 7 of module "Robot" is never satisfied.

Warning: Update 2 of command 3 of module "Robot" doesn't do anything ("(s1'=13)", line 249, column 44)

Warning: Update 2 of command 27 of module "T" doesn't do anything ("(c'=1)", line 372, column 108)

Warning: Guard for command 28 of module "T" is never satisfied.

Warning: Update 2 of command 9 of module "T" doesn't do anything ("(c'=1)", line 349, column 112)

Warning: Guard for command 10 of module "T" is never satisfied.

Warning: Update 2 of command 51 of module "T" doesn't do anything ("(c'=1)", line 403, column 113)

Warning: Guard for command 52 of module "T" is never satisfied.

Computing reachable states...

Reachability (BFS): 81 iterations in 0.86 seconds (average 0.010568, setup 0.00)

Time for model construction: 2.026 seconds.

Warning: Deadlocks detected and fixed in 1 states

Type:        MDP
States:      209123 (1 initial)
Transitions: 551716
Choices:     275849

Transition matrix: 174857 nodes (76 terminal), 551716 minterms, vars: 34r/34c/18nd

Prob0A: 48 iterations in 0.36 seconds (average 0.007417, setup 0.00)

Prob1E: 4 iterations in 0.05 seconds (average 0.013000, setup 0.00)

yes = 954, no = 176703, maybe = 31466

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=209123, nc=54842, nnz=156044, k=2] [2.0 MB]
Creating vector for yes... [1.6 MB]
Allocating iteration vectors... [2 x 1.6 MB]
TOTAL: [6.8 MB]

Starting iterations...

Iterative method: 93 iterations in 0.37 seconds (average 0.001333, setup 0.25)

Value in the initial state: 0.9799801578179745

Time for model checking: 0.815 seconds.

Result: 0.9799801578179745 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmin=? [ F "MissionSuccessful" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=2,Objy1=2,Objx2=6,Objy2=1

Prob0E: 50 iterations in 0.44 seconds (average 0.008720, setup 0.00)

Prob1A: 3 iterations in 0.05 seconds (average 0.017333, setup 0.00)

yes = 954, no = 182382, maybe = 25787

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=209123, nc=44713, nnz=126079, k=2] [1.7 MB]
Creating vector for yes... [1.6 MB]
Allocating iteration vectors... [2 x 1.6 MB]
TOTAL: [6.5 MB]

Starting iterations...

Iterative method: 87 iterations in 0.32 seconds (average 0.001333, setup 0.21)

Value in the initial state: 0.7451327679681082

Time for model checking: 0.837 seconds.

Result: 0.7451327679681082 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmax=? [ F "MissionTimeUp" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=2,Objy1=2,Objx2=6,Objy2=1

Prob0A: 60 iterations in 0.71 seconds (average 0.011867, setup 0.00)

Prob1E: 4 iterations in 0.04 seconds (average 0.011000, setup 0.00)

yes = 440, no = 128088, maybe = 80595

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=209123, nc=137296, nnz=377618, k=4] [4.7 MB]
Creating vector for yes... [1.6 MB]
Allocating iteration vectors... [2 x 1.6 MB]
TOTAL: [9.4 MB]

Starting iterations...

Iterative method: 90 iterations in 0.42 seconds (average 0.001822, setup 0.26)

Value in the initial state: 0.0014525343955790798

Time for model checking: 1.196 seconds.

Result: 0.0014525343955790798 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmin=? [ F "MissionTimeUp" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=2,Objy1=2,Objx2=6,Objy2=1

Prob0E: 67 iterations in 0.56 seconds (average 0.008299, setup 0.00)

Prob1A: 3 iterations in 0.04 seconds (average 0.013333, setup 0.00)

yes = 440, no = 128445, maybe = 80238

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=209123, nc=136658, nnz=376309, k=4] [4.6 MB]
Creating vector for yes... [1.6 MB]
Allocating iteration vectors... [2 x 1.6 MB]
TOTAL: [9.4 MB]

Starting iterations...

Iterative method: 108 iterations in 0.44 seconds (average 0.001852, setup 0.24)

Value in the initial state: 4.124067351210706E-6

Time for model checking: 1.045 seconds.

Result: 4.124067351210706E-6 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmax=? [ F "Crash" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=2,Objy1=2,Objx2=6,Objy2=1

Prob0A: 3 iterations in 0.08 seconds (average 0.026667, setup 0.00)

Prob1E: 187 iterations in 2.49 seconds (average 0.013305, setup 0.00)

yes = 102452, no = 2653, maybe = 104018

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=209123, nc=170744, nnz=446611, k=4] [5.5 MB]
Creating vector for yes... [1.6 MB]
Allocating iteration vectors... [2 x 1.6 MB]
TOTAL: [10.3 MB]

Starting iterations...

Iterative method: 89 iterations in 0.48 seconds (average 0.001978, setup 0.31)

Value in the initial state: 0.03154510936234517

Time for model checking: 3.066 seconds.

Result: 0.03154510936234517 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmin=? [ F "Crash" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=2,Objy1=2,Objx2=6,Objy2=1

Prob0E: 3 iterations in 0.06 seconds (average 0.018667, setup 0.00)

Prob1A: 45 iterations in 0.47 seconds (average 0.010400, setup 0.00)

yes = 102452, no = 2653, maybe = 104018

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=209123, nc=170744, nnz=446611, k=4] [5.5 MB]
Creating vector for yes... [1.6 MB]
Allocating iteration vectors... [2 x 1.6 MB]
TOTAL: [10.3 MB]

Starting iterations...

Iterative method: 92 iterations in 0.42 seconds (average 0.002043, setup 0.24)

Value in the initial state: 0.018046204430735138

Time for model checking: 0.971 seconds.

Result: 0.018046204430735138 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmax=? [ F "AllAreaSearched" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=2,Objy1=2,Objx2=6,Objy2=1

Prob0A: 56 iterations in 0.40 seconds (average 0.007143, setup 0.00)

Prob1E: 4 iterations in 0.04 seconds (average 0.011000, setup 0.00)

yes = 1258, no = 167765, maybe = 40100

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=209123, nc=60436, nnz=153745, k=2] [2.0 MB]
Creating vector for yes... [1.6 MB]
Allocating iteration vectors... [2 x 1.6 MB]
TOTAL: [6.8 MB]

Starting iterations...

Iterative method: 85 iterations in 0.29 seconds (average 0.001459, setup 0.17)

Value in the initial state: 0.22607811467968347

Time for model checking: 0.756 seconds.

Result: 0.22607811467968347 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmin=? [ F "AllAreaSearched" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=2,Objy1=2,Objx2=6,Objy2=1

Prob0E: 63 iterations in 0.44 seconds (average 0.006984, setup 0.00)

Prob1A: 3 iterations in 0.04 seconds (average 0.012000, setup 0.00)

yes = 1258, no = 173054, maybe = 34811

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=209123, nc=51894, nnz=130402, k=2] [1.7 MB]
Creating vector for yes... [1.6 MB]
Allocating iteration vectors... [2 x 1.6 MB]
TOTAL: [6.5 MB]

Starting iterations...

Iterative method: 91 iterations in 0.23 seconds (average 0.001363, setup 0.10)

Value in the initial state: 0.0017507488047899126

Time for model checking: 0.726 seconds.

Result: 0.0017507488047899126 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmax=? [ F "MissionTimeUp"|"AllAreaSearched"|"Crash" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=2,Objy1=2,Objx2=6,Objy2=1

Prob0A: 3 iterations in 0.11 seconds (average 0.037333, setup 0.00)

Prob1E: 196 iterations in 2.56 seconds (average 0.013082, setup 0.00)

yes = 182381, no = 955, maybe = 25787

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=209123, nc=44713, nnz=126079, k=2] [1.7 MB]
Creating vector for yes... [1.6 MB]
Allocating iteration vectors... [2 x 1.6 MB]
TOTAL: [6.5 MB]

Starting iterations...

Iterative method: 80 iterations in 0.28 seconds (average 0.001350, setup 0.18)

Value in the initial state: 0.254867101600469

Time for model checking: 2.963 seconds.

Result: 0.254867101600469 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmin=? [ F "MissionTimeUp"|"AllAreaSearched"|"Crash" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=2,Objy1=2,Objx2=6,Objy2=1

Prob0E: 3 iterations in 0.06 seconds (average 0.020000, setup 0.00)

Prob1A: 48 iterations in 0.46 seconds (average 0.009667, setup 0.00)

yes = 176702, no = 955, maybe = 31466

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=209123, nc=54842, nnz=156044, k=2] [2.0 MB]
Creating vector for yes... [1.6 MB]
Allocating iteration vectors... [2 x 1.6 MB]
TOTAL: [6.8 MB]

Starting iterations...

Iterative method: 92 iterations in 0.28 seconds (average 0.001304, setup 0.16)

Value in the initial state: 0.02001908333229881

Time for model checking: 0.85 seconds.

Result: 0.02001908333229881 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"drop"}max=? [ F "deadlock" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=2,Objy1=2,Objx2=6,Objy2=1

Prob0E: 4 iterations in 0.06 seconds (average 0.014000, setup 0.00)

Prob1A: 1 iterations in 0.00 seconds (average 0.004000, setup 0.00)

goal = 1, inf = 0, maybe = 209122

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=209123, nc=275848, nnz=551715, k=4] [8.2 MB]
Building sparse matrix (transition rewards)... [n=209123, nc=275848, nnz=0, k=4] [1.9 MB]
Creating vector for state rewards... [1.6 MB]
Creating vector for inf... [1.6 MB]
Allocating iteration vectors... [2 x 1.6 MB]
TOTAL: [16.4 MB]

Starting iterations...

Iterative method: 90 iterations in 0.66 seconds (average 0.003022, setup 0.38)

Value in the initial state: 0.20491150597090274

Time for model checking: 0.732 seconds.

Result: 0.20491150597090274 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"drop"}min=? [ F "deadlock" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=2,Objy1=2,Objx2=6,Objy2=1

Prob0A: 4 iterations in 0.04 seconds (average 0.009000, setup 0.00)

Prob1E: 5 iterations in 0.06 seconds (average 0.011200, setup 0.00)

Warning: PRISM hasn't checked for zero-reward loops.
Your minimum rewards may be too low...

goal = 1, inf = 0, maybe = 209122

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=209123, nc=275848, nnz=551715, k=4] [8.2 MB]
Building sparse matrix (transition rewards)... [n=209123, nc=275848, nnz=0, k=4] [1.9 MB]
Creating vector for state rewards... [1.6 MB]
Creating vector for inf... [1.6 MB]
Allocating iteration vectors... [2 x 1.6 MB]
TOTAL: [16.4 MB]

Starting iterations...

Iterative method: 78 iterations in 0.54 seconds (average 0.003077, setup 0.30)

Value in the initial state: 0.19055866528399312

Time for model checking: 0.642 seconds.

Result: 0.19055866528399312 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"charge"}max=? [ F "deadlock" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=2,Objy1=2,Objx2=6,Objy2=1

Prob0E: 4 iterations in 0.03 seconds (average 0.007000, setup 0.00)

Prob1A: 1 iterations in 0.01 seconds (average 0.008000, setup 0.00)

goal = 1, inf = 0, maybe = 209122

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=209123, nc=275848, nnz=551715, k=4] [8.2 MB]
Building sparse matrix (transition rewards)... [n=209123, nc=275848, nnz=6074, k=4] [1.9 MB]
Creating vector for state rewards... [1.6 MB]
Creating vector for inf... [1.6 MB]
Allocating iteration vectors... [2 x 1.6 MB]
TOTAL: [16.5 MB]

Starting iterations...

Iterative method: 87 iterations in 0.57 seconds (average 0.003080, setup 0.30)

Value in the initial state: 0.29328238424394654

Time for model checking: 0.611 seconds.

Result: 0.29328238424394654 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"charge"}min=? [ F "deadlock" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=2,Objy1=2,Objx2=6,Objy2=1

Prob0A: 4 iterations in 0.03 seconds (average 0.007000, setup 0.00)

Prob1E: 5 iterations in 0.05 seconds (average 0.009600, setup 0.00)

Warning: PRISM hasn't checked for zero-reward loops.
Your minimum rewards may be too low...

goal = 1, inf = 0, maybe = 209122

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=209123, nc=275848, nnz=551715, k=4] [8.2 MB]
Building sparse matrix (transition rewards)... [n=209123, nc=275848, nnz=6074, k=4] [1.9 MB]
Creating vector for state rewards... [1.6 MB]
Creating vector for inf... [1.6 MB]
Allocating iteration vectors... [2 x 1.6 MB]
TOTAL: [16.5 MB]

Starting iterations...

Iterative method: 93 iterations in 0.59 seconds (average 0.003097, setup 0.30)

Value in the initial state: 0.046221760140038724

Time for model checking: 0.672 seconds.

Result: 0.046221760140038724 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"time"}max=? [ F "deadlock" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=2,Objy1=2,Objx2=6,Objy2=1

Prob0E: 4 iterations in 0.02 seconds (average 0.006000, setup 0.00)

Prob1A: 1 iterations in 0.01 seconds (average 0.008000, setup 0.00)

goal = 1, inf = 0, maybe = 209122

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=209123, nc=275848, nnz=551715, k=4] [8.2 MB]
Building sparse matrix (transition rewards)... [n=209123, nc=275848, nnz=105103, k=4] [3.1 MB]
Creating vector for state rewards... [1.6 MB]
Creating vector for inf... [1.6 MB]
Allocating iteration vectors... [2 x 1.6 MB]
TOTAL: [17.6 MB]

Starting iterations...

Iterative method: 92 iterations in 0.62 seconds (average 0.003043, setup 0.34)

Value in the initial state: 173.90347656612764

Time for model checking: 0.663 seconds.

Result: 173.90347656612764 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"time"}min=? [ F "deadlock" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=2,Objy1=2,Objx2=6,Objy2=1

Prob0A: 4 iterations in 0.03 seconds (average 0.007000, setup 0.00)

Prob1E: 5 iterations in 0.05 seconds (average 0.009600, setup 0.00)

Warning: PRISM hasn't checked for zero-reward loops.
Your minimum rewards may be too low...

goal = 1, inf = 0, maybe = 209122

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=209123, nc=275848, nnz=551715, k=4] [8.2 MB]
Building sparse matrix (transition rewards)... [n=209123, nc=275848, nnz=105103, k=4] [3.1 MB]
Creating vector for state rewards... [1.6 MB]
Creating vector for inf... [1.6 MB]
Allocating iteration vectors... [2 x 1.6 MB]
TOTAL: [17.6 MB]

Starting iterations...

Iterative method: 99 iterations in 0.63 seconds (average 0.003111, setup 0.32)

Value in the initial state: 99.97673849065372

Time for model checking: 0.727 seconds.

Result: 99.97673849065372 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"success"}max=? [ F "deadlock" ]/Pmin=? [ F "MissionSuccessful" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=2,Objy1=2,Objx2=6,Objy2=1

Prob0E: 4 iterations in 0.02 seconds (average 0.006000, setup 0.00)

Prob1A: 1 iterations in 0.00 seconds (average 0.004000, setup 0.00)

goal = 1, inf = 0, maybe = 209122

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=209123, nc=275848, nnz=551715, k=4] [8.2 MB]
Building sparse matrix (transition rewards)... [n=209123, nc=275848, nnz=1880, k=4] [1.9 MB]
Creating vector for state rewards... [1.6 MB]
Creating vector for inf... [1.6 MB]
Allocating iteration vectors... [2 x 1.6 MB]
TOTAL: [16.4 MB]

Starting iterations...

Iterative method: 96 iterations in 0.58 seconds (average 0.003042, setup 0.29)

Prob0E: 50 iterations in 0.32 seconds (average 0.006320, setup 0.00)

Prob1A: 3 iterations in 0.04 seconds (average 0.014667, setup 0.00)

yes = 954, no = 182382, maybe = 25787

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=209123, nc=44713, nnz=126079, k=2] [1.7 MB]
Creating vector for yes... [1.6 MB]
Allocating iteration vectors... [2 x 1.6 MB]
TOTAL: [6.5 MB]

Starting iterations...

Iterative method: 87 iterations in 0.25 seconds (average 0.001287, setup 0.14)

Value in the initial state: 228.49288645871823

Time for model checking: 1.262 seconds.

Result: 228.49288645871823 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"success"}min=? [ F "deadlock" ]/Pmax=? [ F "MissionSuccessful" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=2,Objy1=2,Objx2=6,Objy2=1

Prob0A: 4 iterations in 0.06 seconds (average 0.014000, setup 0.00)

Prob1E: 5 iterations in 0.04 seconds (average 0.007200, setup 0.00)

Warning: PRISM hasn't checked for zero-reward loops.
Your minimum rewards may be too low...

goal = 1, inf = 0, maybe = 209122

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=209123, nc=275848, nnz=551715, k=4] [8.2 MB]
Building sparse matrix (transition rewards)... [n=209123, nc=275848, nnz=1880, k=4] [1.9 MB]
Creating vector for state rewards... [1.6 MB]
Creating vector for inf... [1.6 MB]
Allocating iteration vectors... [2 x 1.6 MB]
TOTAL: [16.4 MB]

Starting iterations...

Iterative method: 89 iterations in 0.56 seconds (average 0.003011, setup 0.30)

Prob0A: 48 iterations in 0.23 seconds (average 0.004833, setup 0.00)

Prob1E: 4 iterations in 0.03 seconds (average 0.007000, setup 0.00)

yes = 954, no = 176703, maybe = 31466

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=209123, nc=54842, nnz=156044, k=2] [2.0 MB]
Creating vector for yes... [1.6 MB]
Allocating iteration vectors... [2 x 1.6 MB]
TOTAL: [6.8 MB]

Starting iterations...

Iterative method: 93 iterations in 0.25 seconds (average 0.001333, setup 0.12)

Value in the initial state: 81.14639229611416

Time for model checking: 1.16 seconds.

Result: 81.14639229611416 (value in the initial state)

---------------------------------------------------------------------

Note: There were 16 warnings during computation.

