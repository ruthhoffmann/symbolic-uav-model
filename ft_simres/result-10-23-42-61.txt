PRISM
=====

Version: 4.3
Date: Tue Aug 02 12:42:19 BST 2016
Hostname: eglinton
Memory limits: cudd=1g, java(heap)=981.5m
Command line: prism uav_sim_ft_mdp.prism -s uav_mdp.props -const 'Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=4,Objy1=2,Objx2=6,Objy2=1,'

Parsing model file "uav_sim_ft_mdp.prism"...

Parsing properties file "uav_mdp.props"...

18 properties:
(1) Pmax=? [ F "MissionSuccessful" ]
(2) Pmin=? [ F "MissionSuccessful" ]
(3) Pmax=? [ F "MissionTimeUp" ]
(4) Pmin=? [ F "MissionTimeUp" ]
(5) Pmax=? [ F "Crash" ]
(6) Pmin=? [ F "Crash" ]
(7) Pmax=? [ F "AllAreaSearched" ]
(8) Pmin=? [ F "AllAreaSearched" ]
(9) Pmax=? [ F "MissionTimeUp"|"AllAreaSearched"|"Crash" ]
(10) Pmin=? [ F "MissionTimeUp"|"AllAreaSearched"|"Crash" ]
(11) R{"drop"}max=? [ F "deadlock" ]
(12) R{"drop"}min=? [ F "deadlock" ]
(13) R{"charge"}max=? [ F "deadlock" ]
(14) R{"charge"}min=? [ F "deadlock" ]
(15) R{"time"}max=? [ F "deadlock" ]
(16) R{"time"}min=? [ F "deadlock" ]
(17) R{"success"}max=? [ F "deadlock" ]/Pmin=? [ F "MissionSuccessful" ]
(18) R{"success"}min=? [ F "deadlock" ]/Pmax=? [ F "MissionSuccessful" ]

Type:        MDP
Modules:     Obj Move Robot T 
Variables:   NoOfObjs posx posy s1 retx rety b t c 

---------------------------------------------------------------------

Model checking: Pmax=? [ F "MissionSuccessful" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=4,Objy1=2,Objx2=6,Objy2=1

Building model...
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=4,Objy1=2,Objx2=6,Objy2=1

Warning: Guard for command 7 of module "Move" is never satisfied.

Warning: Guard for command 8 of module "Move" is never satisfied.

Warning: Guard for command 9 of module "Move" is never satisfied.

Warning: Guard for command 7 of module "Robot" is never satisfied.

Warning: Update 2 of command 3 of module "Robot" doesn't do anything ("(s1'=13)", line 249, column 44)

Warning: Update 2 of command 27 of module "T" doesn't do anything ("(c'=1)", line 372, column 108)

Warning: Guard for command 28 of module "T" is never satisfied.

Warning: Update 2 of command 9 of module "T" doesn't do anything ("(c'=1)", line 349, column 112)

Warning: Guard for command 10 of module "T" is never satisfied.

Warning: Update 2 of command 51 of module "T" doesn't do anything ("(c'=1)", line 403, column 113)

Warning: Guard for command 52 of module "T" is never satisfied.

Computing reachable states...

Reachability (BFS): 82 iterations in 0.77 seconds (average 0.009415, setup 0.00)

Time for model construction: 1.777 seconds.

Warning: Deadlocks detected and fixed in 1 states

Type:        MDP
States:      227448 (1 initial)
Transitions: 613967
Choices:     302285

Transition matrix: 175361 nodes (76 terminal), 613967 minterms, vars: 34r/34c/18nd

Prob0A: 51 iterations in 0.37 seconds (average 0.007216, setup 0.00)

Prob1E: 4 iterations in 0.05 seconds (average 0.013000, setup 0.00)

yes = 941, no = 192283, maybe = 34224

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=227448, nc=59479, nnz=170619, k=2] [2.2 MB]
Creating vector for yes... [1.7 MB]
Allocating iteration vectors... [2 x 1.7 MB]
TOTAL: [7.4 MB]

Starting iterations...

Iterative method: 95 iterations in 0.34 seconds (average 0.001558, setup 0.20)

Value in the initial state: 0.9783613685387154

Time for model checking: 0.791 seconds.

Result: 0.9783613685387154 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmin=? [ F "MissionSuccessful" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=4,Objy1=2,Objx2=6,Objy2=1

Prob0E: 53 iterations in 0.55 seconds (average 0.010340, setup 0.00)

Prob1A: 3 iterations in 0.04 seconds (average 0.014667, setup 0.00)

yes = 941, no = 198073, maybe = 28434

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=227448, nc=49239, nnz=140262, k=2] [1.9 MB]
Creating vector for yes... [1.7 MB]
Allocating iteration vectors... [2 x 1.7 MB]
TOTAL: [7.1 MB]

Starting iterations...

Iterative method: 90 iterations in 0.36 seconds (average 0.001911, setup 0.19)

Value in the initial state: 0.7331354224302742

Time for model checking: 0.993 seconds.

Result: 0.7331354224302742 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmax=? [ F "MissionTimeUp" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=4,Objy1=2,Objx2=6,Objy2=1

Prob0A: 60 iterations in 0.64 seconds (average 0.010600, setup 0.00)

Prob1E: 4 iterations in 0.04 seconds (average 0.011000, setup 0.00)

yes = 424, no = 134748, maybe = 92276

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=227448, nc=157169, nnz=435546, k=4] [5.4 MB]
Creating vector for yes... [1.7 MB]
Allocating iteration vectors... [2 x 1.7 MB]
TOTAL: [10.6 MB]

Starting iterations...

Iterative method: 92 iterations in 0.66 seconds (average 0.002565, setup 0.42)

Value in the initial state: 0.0016949307252244823

Time for model checking: 1.358 seconds.

Result: 0.0016949307252244823 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmin=? [ F "MissionTimeUp" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=4,Objy1=2,Objx2=6,Objy2=1

Prob0E: 67 iterations in 1.02 seconds (average 0.015164, setup 0.00)

Prob1A: 3 iterations in 0.05 seconds (average 0.017333, setup 0.00)

yes = 424, no = 135143, maybe = 91881

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=227448, nc=156455, nnz=434159, k=4] [5.3 MB]
Creating vector for yes... [1.7 MB]
Allocating iteration vectors... [2 x 1.7 MB]
TOTAL: [10.5 MB]

Starting iterations...

Iterative method: 109 iterations in 0.55 seconds (average 0.002092, setup 0.32)

Value in the initial state: 1.1232293424609624E-5

Time for model checking: 1.649 seconds.

Result: 1.1232293424609624E-5 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmax=? [ F "Crash" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=4,Objy1=2,Objx2=6,Objy2=1

Prob0A: 3 iterations in 0.18 seconds (average 0.061333, setup 0.00)

Prob1E: 198 iterations in 2.78 seconds (average 0.014040, setup 0.00)

yes = 111311, no = 2625, maybe = 113512

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=227448, nc=188349, nnz=500031, k=4] [6.1 MB]
Creating vector for yes... [1.7 MB]
Allocating iteration vectors... [2 x 1.7 MB]
TOTAL: [11.3 MB]

Starting iterations...

Iterative method: 91 iterations in 0.79 seconds (average 0.003033, setup 0.51)

Value in the initial state: 0.032827363856729665

Time for model checking: 3.781 seconds.

Result: 0.032827363856729665 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmin=? [ F "Crash" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=4,Objy1=2,Objx2=6,Objy2=1

Prob0E: 3 iterations in 0.08 seconds (average 0.028000, setup 0.00)

Prob1A: 45 iterations in 0.56 seconds (average 0.012444, setup 0.00)

yes = 111311, no = 2625, maybe = 113512

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=227448, nc=188349, nnz=500031, k=4] [6.1 MB]
Creating vector for yes... [1.7 MB]
Allocating iteration vectors... [2 x 1.7 MB]
TOTAL: [11.3 MB]

Starting iterations...

Iterative method: 96 iterations in 0.58 seconds (average 0.002208, setup 0.36)

Value in the initial state: 0.01964814220058154

Time for model checking: 1.252 seconds.

Result: 0.01964814220058154 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmax=? [ F "AllAreaSearched" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=4,Objy1=2,Objx2=6,Objy2=1

Prob0A: 57 iterations in 0.33 seconds (average 0.005825, setup 0.00)

Prob1E: 4 iterations in 0.04 seconds (average 0.010000, setup 0.00)

yes = 1259, no = 181924, maybe = 44265

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=227448, nc=69216, nnz=183198, k=2] [2.4 MB]
Creating vector for yes... [1.7 MB]
Allocating iteration vectors... [2 x 1.7 MB]
TOTAL: [7.6 MB]

Starting iterations...

Iterative method: 88 iterations in 0.37 seconds (average 0.001682, setup 0.22)

Value in the initial state: 0.23731709857197442

Time for model checking: 0.761 seconds.

Result: 0.23731709857197442 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmin=? [ F "AllAreaSearched" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=4,Objy1=2,Objx2=6,Objy2=1

Prob0E: 64 iterations in 0.50 seconds (average 0.007875, setup 0.00)

Prob1A: 3 iterations in 0.04 seconds (average 0.012000, setup 0.00)

yes = 1259, no = 188431, maybe = 37758

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=227448, nc=58643, nnz=153947, k=2] [2.0 MB]
Creating vector for yes... [1.7 MB]
Allocating iteration vectors... [2 x 1.7 MB]
TOTAL: [7.2 MB]

Starting iterations...

Iterative method: 90 iterations in 0.26 seconds (average 0.001556, setup 0.12)

Value in the initial state: 0.0017518457461779503

Time for model checking: 0.824 seconds.

Result: 0.0017518457461779503 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmax=? [ F "MissionTimeUp"|"AllAreaSearched"|"Crash" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=4,Objy1=2,Objx2=6,Objy2=1

Prob0A: 3 iterations in 0.14 seconds (average 0.048000, setup 0.00)

Prob1E: 208 iterations in 3.20 seconds (average 0.015404, setup 0.00)

yes = 198072, no = 942, maybe = 28434

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=227448, nc=49239, nnz=140262, k=2] [1.9 MB]
Creating vector for yes... [1.7 MB]
Allocating iteration vectors... [2 x 1.7 MB]
TOTAL: [7.1 MB]

Starting iterations...

Iterative method: 83 iterations in 0.29 seconds (average 0.001494, setup 0.17)

Value in the initial state: 0.26686409053914284

Time for model checking: 3.666 seconds.

Result: 0.26686409053914284 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmin=? [ F "MissionTimeUp"|"AllAreaSearched"|"Crash" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=4,Objy1=2,Objx2=6,Objy2=1

Prob0E: 3 iterations in 0.06 seconds (average 0.021333, setup 0.00)

Prob1A: 51 iterations in 0.85 seconds (average 0.016627, setup 0.00)

yes = 192282, no = 942, maybe = 34224

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=227448, nc=59479, nnz=170619, k=2] [2.2 MB]
Creating vector for yes... [1.7 MB]
Allocating iteration vectors... [2 x 1.7 MB]
TOTAL: [7.4 MB]

Starting iterations...

Iterative method: 94 iterations in 0.31 seconds (average 0.001532, setup 0.16)

Value in the initial state: 0.02163756146745074

Time for model checking: 1.249 seconds.

Result: 0.02163756146745074 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"drop"}max=? [ F "deadlock" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=4,Objy1=2,Objx2=6,Objy2=1

Prob0E: 4 iterations in 0.08 seconds (average 0.021000, setup 0.00)

Prob1A: 1 iterations in 0.01 seconds (average 0.012000, setup 0.00)

goal = 1, inf = 0, maybe = 227447

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=227448, nc=302284, nnz=613966, k=4] [9.0 MB]
Building sparse matrix (transition rewards)... [n=227448, nc=302284, nnz=0, k=4] [2.0 MB]
Creating vector for state rewards... [1.7 MB]
Creating vector for inf... [1.7 MB]
Allocating iteration vectors... [2 x 1.7 MB]
TOTAL: [18.0 MB]

Starting iterations...

Iterative method: 93 iterations in 0.70 seconds (average 0.003226, setup 0.40)

Value in the initial state: 0.23928304904220896

Time for model checking: 0.804 seconds.

Result: 0.23928304904220896 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"drop"}min=? [ F "deadlock" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=4,Objy1=2,Objx2=6,Objy2=1

Prob0A: 4 iterations in 0.03 seconds (average 0.007000, setup 0.00)

Prob1E: 5 iterations in 0.04 seconds (average 0.008000, setup 0.00)

Warning: PRISM hasn't checked for zero-reward loops.
Your minimum rewards may be too low...

goal = 1, inf = 0, maybe = 227447

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=227448, nc=302284, nnz=613966, k=4] [9.0 MB]
Building sparse matrix (transition rewards)... [n=227448, nc=302284, nnz=0, k=4] [2.0 MB]
Creating vector for state rewards... [1.7 MB]
Creating vector for inf... [1.7 MB]
Allocating iteration vectors... [2 x 1.7 MB]
TOTAL: [18.0 MB]

Starting iterations...

Iterative method: 83 iterations in 0.57 seconds (average 0.003181, setup 0.30)

Value in the initial state: 0.21194617216476816

Time for model checking: 0.658 seconds.

Result: 0.21194617216476816 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"charge"}max=? [ F "deadlock" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=4,Objy1=2,Objx2=6,Objy2=1

Prob0E: 4 iterations in 0.02 seconds (average 0.006000, setup 0.00)

Prob1A: 1 iterations in 0.01 seconds (average 0.008000, setup 0.00)

goal = 1, inf = 0, maybe = 227447

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=227448, nc=302284, nnz=613966, k=4] [9.0 MB]
Building sparse matrix (transition rewards)... [n=227448, nc=302284, nnz=7130, k=4] [2.1 MB]
Creating vector for state rewards... [1.7 MB]
Creating vector for inf... [1.7 MB]
Allocating iteration vectors... [2 x 1.7 MB]
TOTAL: [18.1 MB]

Starting iterations...

Iterative method: 89 iterations in 0.58 seconds (average 0.003056, setup 0.30)

Value in the initial state: 0.3108783363558473

Time for model checking: 0.618 seconds.

Result: 0.3108783363558473 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"charge"}min=? [ F "deadlock" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=4,Objy1=2,Objx2=6,Objy2=1

Prob0A: 4 iterations in 0.02 seconds (average 0.005000, setup 0.00)

Prob1E: 5 iterations in 0.03 seconds (average 0.006400, setup 0.00)

Warning: PRISM hasn't checked for zero-reward loops.
Your minimum rewards may be too low...

goal = 1, inf = 0, maybe = 227447

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=227448, nc=302284, nnz=613966, k=4] [9.0 MB]
Building sparse matrix (transition rewards)... [n=227448, nc=302284, nnz=7130, k=4] [2.1 MB]
Creating vector for state rewards... [1.7 MB]
Creating vector for inf... [1.7 MB]
Allocating iteration vectors... [2 x 1.7 MB]
TOTAL: [18.1 MB]

Starting iterations...

Iterative method: 95 iterations in 0.80 seconds (average 0.004800, setup 0.34)

Value in the initial state: 0.06395829530600836

Time for model checking: 0.855 seconds.

Result: 0.06395829530600836 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"time"}max=? [ F "deadlock" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=4,Objy1=2,Objx2=6,Objy2=1

Prob0E: 4 iterations in 0.02 seconds (average 0.005000, setup 0.00)

Prob1A: 1 iterations in 0.00 seconds (average 0.000000, setup 0.00)

goal = 1, inf = 0, maybe = 227447

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=227448, nc=302284, nnz=613966, k=4] [9.0 MB]
Building sparse matrix (transition rewards)... [n=227448, nc=302284, nnz=113934, k=4] [3.3 MB]
Creating vector for state rewards... [1.7 MB]
Creating vector for inf... [1.7 MB]
Allocating iteration vectors... [2 x 1.7 MB]
TOTAL: [19.3 MB]

Starting iterations...

Iterative method: 94 iterations in 0.60 seconds (average 0.003064, setup 0.31)

Value in the initial state: 180.96300048470434

Time for model checking: 0.64 seconds.

Result: 180.96300048470434 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"time"}min=? [ F "deadlock" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=4,Objy1=2,Objx2=6,Objy2=1

Prob0A: 4 iterations in 0.02 seconds (average 0.004000, setup 0.00)

Prob1E: 5 iterations in 0.03 seconds (average 0.006400, setup 0.00)

Warning: PRISM hasn't checked for zero-reward loops.
Your minimum rewards may be too low...

goal = 1, inf = 0, maybe = 227447

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=227448, nc=302284, nnz=613966, k=4] [9.0 MB]
Building sparse matrix (transition rewards)... [n=227448, nc=302284, nnz=113934, k=4] [3.3 MB]
Creating vector for state rewards... [1.7 MB]
Creating vector for inf... [1.7 MB]
Allocating iteration vectors... [2 x 1.7 MB]
TOTAL: [19.3 MB]

Starting iterations...

Iterative method: 101 iterations in 0.64 seconds (average 0.003129, setup 0.33)

Value in the initial state: 108.78787143172316

Time for model checking: 0.697 seconds.

Result: 108.78787143172316 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"success"}max=? [ F "deadlock" ]/Pmin=? [ F "MissionSuccessful" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=4,Objy1=2,Objx2=6,Objy2=1

Prob0E: 4 iterations in 0.02 seconds (average 0.005000, setup 0.00)

Prob1A: 1 iterations in 0.00 seconds (average 0.000000, setup 0.00)

goal = 1, inf = 0, maybe = 227447

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=227448, nc=302284, nnz=613966, k=4] [9.0 MB]
Building sparse matrix (transition rewards)... [n=227448, nc=302284, nnz=1854, k=4] [2.0 MB]
Creating vector for state rewards... [1.7 MB]
Creating vector for inf... [1.7 MB]
Allocating iteration vectors... [2 x 1.7 MB]
TOTAL: [18.0 MB]

Starting iterations...

Iterative method: 97 iterations in 0.57 seconds (average 0.002928, setup 0.29)

Prob0E: 53 iterations in 0.23 seconds (average 0.004302, setup 0.00)

Prob1A: 3 iterations in 0.03 seconds (average 0.010667, setup 0.00)

yes = 941, no = 198073, maybe = 28434

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=227448, nc=49239, nnz=140262, k=2] [1.9 MB]
Creating vector for yes... [1.7 MB]
Allocating iteration vectors... [2 x 1.7 MB]
TOTAL: [7.1 MB]

Starting iterations...

Iterative method: 90 iterations in 0.25 seconds (average 0.001289, setup 0.13)

Value in the initial state: 241.56426675464513

Time for model checking: 1.133 seconds.

Result: 241.56426675464513 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"success"}min=? [ F "deadlock" ]/Pmax=? [ F "MissionSuccessful" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=4,Objy1=2,Objx2=6,Objy2=1

Prob0A: 4 iterations in 0.04 seconds (average 0.009000, setup 0.00)

Prob1E: 5 iterations in 0.04 seconds (average 0.007200, setup 0.00)

Warning: PRISM hasn't checked for zero-reward loops.
Your minimum rewards may be too low...

goal = 1, inf = 0, maybe = 227447

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=227448, nc=302284, nnz=613966, k=4] [9.0 MB]
Building sparse matrix (transition rewards)... [n=227448, nc=302284, nnz=1854, k=4] [2.0 MB]
Creating vector for state rewards... [1.7 MB]
Creating vector for inf... [1.7 MB]
Allocating iteration vectors... [2 x 1.7 MB]
TOTAL: [18.0 MB]

Starting iterations...

Iterative method: 93 iterations in 0.60 seconds (average 0.002968, setup 0.32)

Prob0A: 51 iterations in 0.21 seconds (average 0.004157, setup 0.00)

Prob1E: 4 iterations in 0.03 seconds (average 0.007000, setup 0.00)

yes = 941, no = 192283, maybe = 34224

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=227448, nc=59479, nnz=170619, k=2] [2.2 MB]
Creating vector for yes... [1.7 MB]
Allocating iteration vectors... [2 x 1.7 MB]
TOTAL: [7.4 MB]

Starting iterations...

Iterative method: 95 iterations in 0.24 seconds (average 0.001347, setup 0.12)

Value in the initial state: 81.8578601455524

Time for model checking: 1.186 seconds.

Result: 81.8578601455524 (value in the initial state)

---------------------------------------------------------------------

Note: There were 16 warnings during computation.

