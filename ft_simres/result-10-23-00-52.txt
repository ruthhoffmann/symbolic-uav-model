PRISM
=====

Version: 4.3
Date: Tue Aug 02 12:34:39 BST 2016
Hostname: eglinton
Memory limits: cudd=1g, java(heap)=981.5m
Command line: prism uav_sim_ft_mdp.prism -s uav_mdp.props -const 'Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=0,Objy1=0,Objx2=5,Objy2=2,'

Parsing model file "uav_sim_ft_mdp.prism"...

Parsing properties file "uav_mdp.props"...

18 properties:
(1) Pmax=? [ F "MissionSuccessful" ]
(2) Pmin=? [ F "MissionSuccessful" ]
(3) Pmax=? [ F "MissionTimeUp" ]
(4) Pmin=? [ F "MissionTimeUp" ]
(5) Pmax=? [ F "Crash" ]
(6) Pmin=? [ F "Crash" ]
(7) Pmax=? [ F "AllAreaSearched" ]
(8) Pmin=? [ F "AllAreaSearched" ]
(9) Pmax=? [ F "MissionTimeUp"|"AllAreaSearched"|"Crash" ]
(10) Pmin=? [ F "MissionTimeUp"|"AllAreaSearched"|"Crash" ]
(11) R{"drop"}max=? [ F "deadlock" ]
(12) R{"drop"}min=? [ F "deadlock" ]
(13) R{"charge"}max=? [ F "deadlock" ]
(14) R{"charge"}min=? [ F "deadlock" ]
(15) R{"time"}max=? [ F "deadlock" ]
(16) R{"time"}min=? [ F "deadlock" ]
(17) R{"success"}max=? [ F "deadlock" ]/Pmin=? [ F "MissionSuccessful" ]
(18) R{"success"}min=? [ F "deadlock" ]/Pmax=? [ F "MissionSuccessful" ]

Type:        MDP
Modules:     Obj Move Robot T 
Variables:   NoOfObjs posx posy s1 retx rety b t c 

---------------------------------------------------------------------

Model checking: Pmax=? [ F "MissionSuccessful" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=0,Objy1=0,Objx2=5,Objy2=2

Building model...
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=0,Objy1=0,Objx2=5,Objy2=2

Warning: Guard for command 7 of module "Move" is never satisfied.

Warning: Guard for command 8 of module "Move" is never satisfied.

Warning: Guard for command 9 of module "Move" is never satisfied.

Warning: Guard for command 7 of module "Robot" is never satisfied.

Warning: Update 2 of command 3 of module "Robot" doesn't do anything ("(s1'=13)", line 249, column 44)

Warning: Update 2 of command 27 of module "T" doesn't do anything ("(c'=1)", line 372, column 108)

Warning: Guard for command 28 of module "T" is never satisfied.

Warning: Update 2 of command 9 of module "T" doesn't do anything ("(c'=1)", line 349, column 112)

Warning: Guard for command 10 of module "T" is never satisfied.

Warning: Update 2 of command 51 of module "T" doesn't do anything ("(c'=1)", line 403, column 113)

Warning: Guard for command 52 of module "T" is never satisfied.

Computing reachable states...

Reachability (BFS): 78 iterations in 1.10 seconds (average 0.014103, setup 0.00)

Time for model construction: 2.735 seconds.

Warning: Deadlocks detected and fixed in 1 states

Type:        MDP
States:      255082 (1 initial)
Transitions: 671982
Choices:     334620

Transition matrix: 178039 nodes (76 terminal), 671982 minterms, vars: 34r/34c/18nd

Prob0A: 59 iterations in 0.49 seconds (average 0.008339, setup 0.00)

Prob1E: 4 iterations in 0.06 seconds (average 0.015000, setup 0.00)

yes = 933, no = 218962, maybe = 35187

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=255082, nc=60043, nnz=170740, k=2] [2.3 MB]
Creating vector for yes... [1.9 MB]
Allocating iteration vectors... [2 x 1.9 MB]
TOTAL: [8.1 MB]

Starting iterations...

Iterative method: 95 iterations in 0.42 seconds (average 0.001811, setup 0.25)

Value in the initial state: 0.9781956789741385

Time for model checking: 1.319 seconds.

Result: 0.9781956789741385 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmin=? [ F "MissionSuccessful" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=0,Objy1=0,Objx2=5,Objy2=2

Prob0E: 61 iterations in 0.55 seconds (average 0.008984, setup 0.00)

Prob1A: 3 iterations in 0.06 seconds (average 0.020000, setup 0.00)

yes = 933, no = 223135, maybe = 31014

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=255082, nc=53176, nnz=151415, k=2] [2.0 MB]
Creating vector for yes... [1.9 MB]
Allocating iteration vectors... [2 x 1.9 MB]
TOTAL: [7.9 MB]

Starting iterations...

Iterative method: 90 iterations in 0.36 seconds (average 0.001733, setup 0.21)

Value in the initial state: 0.7297869473682621

Time for model checking: 1.42 seconds.

Result: 0.7297869473682621 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmax=? [ F "MissionTimeUp" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=0,Objy1=0,Objx2=5,Objy2=2

Prob0A: 62 iterations in 0.66 seconds (average 0.010710, setup 0.00)

Prob1E: 4 iterations in 0.06 seconds (average 0.016000, setup 0.00)

yes = 552, no = 147147, maybe = 107383

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=255082, nc=177032, nnz=482332, k=4] [5.9 MB]
Creating vector for yes... [1.9 MB]
Allocating iteration vectors... [2 x 1.9 MB]
TOTAL: [11.8 MB]

Starting iterations...

Iterative method: 92 iterations in 0.54 seconds (average 0.002304, setup 0.33)

Value in the initial state: 9.018627463718933E-4

Time for model checking: 1.772 seconds.

Result: 9.018627463718933E-4 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmin=? [ F "MissionTimeUp" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=0,Objy1=0,Objx2=5,Objy2=2

Prob0E: 68 iterations in 0.89 seconds (average 0.013059, setup 0.00)

Prob1A: 3 iterations in 0.06 seconds (average 0.018667, setup 0.00)

yes = 552, no = 147523, maybe = 107007

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=255082, nc=176356, nnz=480984, k=4] [5.9 MB]
Creating vector for yes... [1.9 MB]
Allocating iteration vectors... [2 x 1.9 MB]
TOTAL: [11.8 MB]

Starting iterations...

Iterative method: 107 iterations in 0.56 seconds (average 0.002355, setup 0.31)

Value in the initial state: 9.461972655036396E-6

Time for model checking: 2.292 seconds.

Result: 9.461972655036396E-6 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmax=? [ F "Crash" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=0,Objy1=0,Objx2=5,Objy2=2

Prob0A: 3 iterations in 0.09 seconds (average 0.030667, setup 0.00)

Prob1E: 214 iterations in 3.16 seconds (average 0.014748, setup 0.00)

yes = 124979, no = 2722, maybe = 127381

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=255082, nc=206919, nnz=544281, k=4] [6.7 MB]
Creating vector for yes... [1.9 MB]
Allocating iteration vectors... [2 x 1.9 MB]
TOTAL: [12.5 MB]

Starting iterations...

Iterative method: 88 iterations in 0.59 seconds (average 0.002500, setup 0.37)

Value in the initial state: 0.032860938145947545

Time for model checking: 5.561 seconds.

Result: 0.032860938145947545 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmin=? [ F "Crash" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=0,Objy1=0,Objx2=5,Objy2=2

Prob0E: 3 iterations in 0.08 seconds (average 0.025333, setup 0.00)

Prob1A: 53 iterations in 0.68 seconds (average 0.012830, setup 0.00)

yes = 124979, no = 2722, maybe = 127381

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=255082, nc=206919, nnz=544281, k=4] [6.7 MB]
Creating vector for yes... [1.9 MB]
Allocating iteration vectors... [2 x 1.9 MB]
TOTAL: [12.5 MB]

Starting iterations...

Iterative method: 95 iterations in 0.56 seconds (average 0.002484, setup 0.32)

Value in the initial state: 0.019807495123844214

Time for model checking: 1.558 seconds.

Result: 0.019807495123844214 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmax=? [ F "AllAreaSearched" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=0,Objy1=0,Objx2=5,Objy2=2

Prob0A: 64 iterations in 0.46 seconds (average 0.007188, setup 0.00)

Prob1E: 4 iterations in 0.05 seconds (average 0.012000, setup 0.00)

yes = 1236, no = 204341, maybe = 49505

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=255082, nc=78854, nnz=213171, k=2] [2.8 MB]
Creating vector for yes... [1.9 MB]
Allocating iteration vectors... [2 x 1.9 MB]
TOTAL: [8.6 MB]

Starting iterations...

Iterative method: 89 iterations in 0.34 seconds (average 0.001753, setup 0.19)

Value in the initial state: 0.2408224781398802

Time for model checking: 1.125 seconds.

Result: 0.2408224781398802 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmin=? [ F "AllAreaSearched" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=0,Objy1=0,Objx2=5,Objy2=2

Prob0E: 68 iterations in 0.52 seconds (average 0.007588, setup 0.00)

Prob1A: 3 iterations in 0.05 seconds (average 0.016000, setup 0.00)

yes = 1236, no = 210324, maybe = 43522

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=255082, nc=68670, nnz=184359, k=2] [2.4 MB]
Creating vector for yes... [1.9 MB]
Allocating iteration vectors... [2 x 1.9 MB]
TOTAL: [8.3 MB]

Starting iterations...

Iterative method: 92 iterations in 0.33 seconds (average 0.001870, setup 0.16)

Value in the initial state: 0.0017552610072188763

Time for model checking: 1.041 seconds.

Result: 0.0017552610072188763 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmax=? [ F "MissionTimeUp"|"AllAreaSearched"|"Crash" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=0,Objy1=0,Objx2=5,Objy2=2

Prob0A: 3 iterations in 0.09 seconds (average 0.030667, setup 0.00)

Prob1E: 240 iterations in 3.29 seconds (average 0.013717, setup 0.00)

yes = 223134, no = 934, maybe = 31014

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=255082, nc=53176, nnz=151415, k=2] [2.0 MB]
Creating vector for yes... [1.9 MB]
Allocating iteration vectors... [2 x 1.9 MB]
TOTAL: [7.9 MB]

Starting iterations...

Iterative method: 83 iterations in 0.34 seconds (average 0.001735, setup 0.20)

Value in the initial state: 0.270212482811395

Time for model checking: 4.624 seconds.

Result: 0.270212482811395 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmin=? [ F "MissionTimeUp"|"AllAreaSearched"|"Crash" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=0,Objy1=0,Objx2=5,Objy2=2

Prob0E: 3 iterations in 0.06 seconds (average 0.020000, setup 0.00)

Prob1A: 59 iterations in 0.65 seconds (average 0.011051, setup 0.00)

yes = 218961, no = 934, maybe = 35187

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=255082, nc=60043, nnz=170740, k=2] [2.3 MB]
Creating vector for yes... [1.9 MB]
Allocating iteration vectors... [2 x 1.9 MB]
TOTAL: [8.1 MB]

Starting iterations...

Iterative method: 93 iterations in 0.35 seconds (average 0.001849, setup 0.18)

Value in the initial state: 0.021803796828817258

Time for model checking: 2.104 seconds.

Result: 0.021803796828817258 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"drop"}max=? [ F "deadlock" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=0,Objy1=0,Objx2=5,Objy2=2

Prob0E: 4 iterations in 0.08 seconds (average 0.021000, setup 0.00)

Prob1A: 1 iterations in 0.01 seconds (average 0.012000, setup 0.00)

goal = 1, inf = 0, maybe = 255081

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=255082, nc=334619, nnz=671981, k=4] [9.9 MB]
Building sparse matrix (transition rewards)... [n=255082, nc=334619, nnz=0, k=4] [2.2 MB]
Creating vector for state rewards... [1.9 MB]
Creating vector for inf... [1.9 MB]
Allocating iteration vectors... [2 x 1.9 MB]
TOTAL: [20.0 MB]

Starting iterations...

Iterative method: 92 iterations in 0.81 seconds (average 0.003739, setup 0.46)

Value in the initial state: 0.24547126254327564

Time for model checking: 0.959 seconds.

Result: 0.24547126254327564 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"drop"}min=? [ F "deadlock" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=0,Objy1=0,Objx2=5,Objy2=2

Prob0A: 4 iterations in 0.05 seconds (average 0.013000, setup 0.00)

Prob1E: 5 iterations in 0.07 seconds (average 0.013600, setup 0.00)

Warning: PRISM hasn't checked for zero-reward loops.
Your minimum rewards may be too low...

goal = 1, inf = 0, maybe = 255081

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=255082, nc=334619, nnz=671981, k=4] [9.9 MB]
Building sparse matrix (transition rewards)... [n=255082, nc=334619, nnz=0, k=4] [2.2 MB]
Creating vector for state rewards... [1.9 MB]
Creating vector for inf... [1.9 MB]
Allocating iteration vectors... [2 x 1.9 MB]
TOTAL: [20.0 MB]

Starting iterations...

Iterative method: 83 iterations in 0.66 seconds (average 0.003614, setup 0.36)

Value in the initial state: 0.205343329051727

Time for model checking: 1.019 seconds.

Result: 0.205343329051727 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"charge"}max=? [ F "deadlock" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=0,Objy1=0,Objx2=5,Objy2=2

Prob0E: 4 iterations in 0.03 seconds (average 0.008000, setup 0.00)

Prob1A: 1 iterations in 0.01 seconds (average 0.008000, setup 0.00)

goal = 1, inf = 0, maybe = 255081

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=255082, nc=334619, nnz=671981, k=4] [9.9 MB]
Building sparse matrix (transition rewards)... [n=255082, nc=334619, nnz=7514, k=4] [2.3 MB]
Creating vector for state rewards... [1.9 MB]
Creating vector for inf... [1.9 MB]
Allocating iteration vectors... [2 x 1.9 MB]
TOTAL: [20.1 MB]

Starting iterations...

Iterative method: 88 iterations in 0.68 seconds (average 0.003773, setup 0.35)

Value in the initial state: 0.9793972587268037

Time for model checking: 0.77 seconds.

Result: 0.9793972587268037 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"charge"}min=? [ F "deadlock" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=0,Objy1=0,Objx2=5,Objy2=2

Prob0A: 4 iterations in 0.03 seconds (average 0.007000, setup 0.00)

Prob1E: 5 iterations in 0.05 seconds (average 0.009600, setup 0.00)

Warning: PRISM hasn't checked for zero-reward loops.
Your minimum rewards may be too low...

goal = 1, inf = 0, maybe = 255081

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=255082, nc=334619, nnz=671981, k=4] [9.9 MB]
Building sparse matrix (transition rewards)... [n=255082, nc=334619, nnz=7514, k=4] [2.3 MB]
Creating vector for state rewards... [1.9 MB]
Creating vector for inf... [1.9 MB]
Allocating iteration vectors... [2 x 1.9 MB]
TOTAL: [20.1 MB]

Starting iterations...

Iterative method: 96 iterations in 0.73 seconds (average 0.003833, setup 0.36)

Value in the initial state: 0.07070252781498933

Time for model checking: 0.842 seconds.

Result: 0.07070252781498933 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"time"}max=? [ F "deadlock" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=0,Objy1=0,Objx2=5,Objy2=2

Prob0E: 4 iterations in 0.03 seconds (average 0.007000, setup 0.00)

Prob1A: 1 iterations in 0.00 seconds (average 0.004000, setup 0.00)

goal = 1, inf = 0, maybe = 255081

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=255082, nc=334619, nnz=671981, k=4] [9.9 MB]
Building sparse matrix (transition rewards)... [n=255082, nc=334619, nnz=127699, k=4] [3.7 MB]
Creating vector for state rewards... [1.9 MB]
Creating vector for inf... [1.9 MB]
Allocating iteration vectors... [2 x 1.9 MB]
TOTAL: [21.4 MB]

Starting iterations...

Iterative method: 91 iterations in 0.76 seconds (average 0.003824, setup 0.41)

Value in the initial state: 181.22106944859985

Time for model checking: 0.971 seconds.

Result: 181.22106944859985 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"time"}min=? [ F "deadlock" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=0,Objy1=0,Objx2=5,Objy2=2

Prob0A: 4 iterations in 0.03 seconds (average 0.007000, setup 0.00)

Prob1E: 5 iterations in 0.06 seconds (average 0.012000, setup 0.00)

Warning: PRISM hasn't checked for zero-reward loops.
Your minimum rewards may be too low...

goal = 1, inf = 0, maybe = 255081

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=255082, nc=334619, nnz=671981, k=4] [9.9 MB]
Building sparse matrix (transition rewards)... [n=255082, nc=334619, nnz=127699, k=4] [3.7 MB]
Creating vector for state rewards... [1.9 MB]
Creating vector for inf... [1.9 MB]
Allocating iteration vectors... [2 x 1.9 MB]
TOTAL: [21.4 MB]

Starting iterations...

Iterative method: 101 iterations in 0.78 seconds (average 0.003881, setup 0.39)

Value in the initial state: 109.67314462265897

Time for model checking: 0.894 seconds.

Result: 109.67314462265897 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"success"}max=? [ F "deadlock" ]/Pmin=? [ F "MissionSuccessful" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=0,Objy1=0,Objx2=5,Objy2=2

Prob0E: 4 iterations in 0.02 seconds (average 0.006000, setup 0.00)

Prob1A: 1 iterations in 0.01 seconds (average 0.008000, setup 0.00)

goal = 1, inf = 0, maybe = 255081

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=255082, nc=334619, nnz=671981, k=4] [9.9 MB]
Building sparse matrix (transition rewards)... [n=255082, nc=334619, nnz=1838, k=4] [2.3 MB]
Creating vector for state rewards... [1.9 MB]
Creating vector for inf... [1.9 MB]
Allocating iteration vectors... [2 x 1.9 MB]
TOTAL: [20.0 MB]

Starting iterations...

Iterative method: 93 iterations in 0.70 seconds (average 0.003699, setup 0.36)

Prob0E: 61 iterations in 0.39 seconds (average 0.006361, setup 0.00)

Prob1A: 3 iterations in 0.04 seconds (average 0.013333, setup 0.00)

yes = 933, no = 223135, maybe = 31014

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=255082, nc=53176, nnz=151415, k=2] [2.0 MB]
Creating vector for yes... [1.9 MB]
Allocating iteration vectors... [2 x 1.9 MB]
TOTAL: [7.9 MB]

Starting iterations...

Iterative method: 90 iterations in 0.28 seconds (average 0.001600, setup 0.14)

Value in the initial state: 243.0646141242892

Time for model checking: 1.48 seconds.

Result: 243.0646141242892 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"success"}min=? [ F "deadlock" ]/Pmax=? [ F "MissionSuccessful" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=0,Objy1=0,Objx2=5,Objy2=2

Prob0A: 4 iterations in 0.06 seconds (average 0.015000, setup 0.00)

Prob1E: 5 iterations in 0.05 seconds (average 0.010400, setup 0.00)

Warning: PRISM hasn't checked for zero-reward loops.
Your minimum rewards may be too low...

goal = 1, inf = 0, maybe = 255081

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=255082, nc=334619, nnz=671981, k=4] [9.9 MB]
Building sparse matrix (transition rewards)... [n=255082, nc=334619, nnz=1838, k=4] [2.3 MB]
Creating vector for state rewards... [1.9 MB]
Creating vector for inf... [1.9 MB]
Allocating iteration vectors... [2 x 1.9 MB]
TOTAL: [20.0 MB]

Starting iterations...

Iterative method: 94 iterations in 0.72 seconds (average 0.003617, setup 0.38)

Prob0A: 59 iterations in 0.31 seconds (average 0.005220, setup 0.00)

Prob1E: 4 iterations in 0.03 seconds (average 0.008000, setup 0.00)

yes = 933, no = 218962, maybe = 35187

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=255082, nc=60043, nnz=170740, k=2] [2.3 MB]
Creating vector for yes... [1.9 MB]
Allocating iteration vectors... [2 x 1.9 MB]
TOTAL: [8.1 MB]

Starting iterations...

Iterative method: 95 iterations in 0.28 seconds (average 0.001516, setup 0.13)

Value in the initial state: 80.78583956322421

Time for model checking: 1.479 seconds.

Result: 80.78583956322421 (value in the initial state)

---------------------------------------------------------------------

Note: There were 16 warnings during computation.

