PRISM
=====

Version: 4.3
Date: Tue Aug 02 12:27:28 BST 2016
Hostname: eglinton
Memory limits: cudd=1g, java(heap)=981.5m
Command line: prism uav_sim_ft_mdp.prism -s uav_mdp.props -const 'Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=2,Objy1=1,Objx2=6,Objy2=3,'

Parsing model file "uav_sim_ft_mdp.prism"...

Parsing properties file "uav_mdp.props"...

18 properties:
(1) Pmax=? [ F "MissionSuccessful" ]
(2) Pmin=? [ F "MissionSuccessful" ]
(3) Pmax=? [ F "MissionTimeUp" ]
(4) Pmin=? [ F "MissionTimeUp" ]
(5) Pmax=? [ F "Crash" ]
(6) Pmin=? [ F "Crash" ]
(7) Pmax=? [ F "AllAreaSearched" ]
(8) Pmin=? [ F "AllAreaSearched" ]
(9) Pmax=? [ F "MissionTimeUp"|"AllAreaSearched"|"Crash" ]
(10) Pmin=? [ F "MissionTimeUp"|"AllAreaSearched"|"Crash" ]
(11) R{"drop"}max=? [ F "deadlock" ]
(12) R{"drop"}min=? [ F "deadlock" ]
(13) R{"charge"}max=? [ F "deadlock" ]
(14) R{"charge"}min=? [ F "deadlock" ]
(15) R{"time"}max=? [ F "deadlock" ]
(16) R{"time"}min=? [ F "deadlock" ]
(17) R{"success"}max=? [ F "deadlock" ]/Pmin=? [ F "MissionSuccessful" ]
(18) R{"success"}min=? [ F "deadlock" ]/Pmax=? [ F "MissionSuccessful" ]

Type:        MDP
Modules:     Obj Move Robot T 
Variables:   NoOfObjs posx posy s1 retx rety b t c 

---------------------------------------------------------------------

Model checking: Pmax=? [ F "MissionSuccessful" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=2,Objy1=1,Objx2=6,Objy2=3

Building model...
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=2,Objy1=1,Objx2=6,Objy2=3

Warning: Guard for command 7 of module "Move" is never satisfied.

Warning: Guard for command 8 of module "Move" is never satisfied.

Warning: Guard for command 9 of module "Move" is never satisfied.

Warning: Guard for command 7 of module "Robot" is never satisfied.

Warning: Update 2 of command 3 of module "Robot" doesn't do anything ("(s1'=13)", line 249, column 44)

Warning: Update 2 of command 27 of module "T" doesn't do anything ("(c'=1)", line 372, column 108)

Warning: Guard for command 28 of module "T" is never satisfied.

Warning: Update 2 of command 9 of module "T" doesn't do anything ("(c'=1)", line 349, column 112)

Warning: Guard for command 10 of module "T" is never satisfied.

Warning: Update 2 of command 51 of module "T" doesn't do anything ("(c'=1)", line 403, column 113)

Warning: Guard for command 52 of module "T" is never satisfied.

Computing reachable states...

Reachability (BFS): 79 iterations in 0.86 seconds (average 0.010835, setup 0.00)

Time for model construction: 2.076 seconds.

Warning: Deadlocks detected and fixed in 1 states

Type:        MDP
States:      232067 (1 initial)
Transitions: 645720
Choices:     312590

Transition matrix: 173358 nodes (76 terminal), 645720 minterms, vars: 34r/34c/18nd

Prob0A: 52 iterations in 0.38 seconds (average 0.007231, setup 0.00)

Prob1E: 4 iterations in 0.05 seconds (average 0.012000, setup 0.00)

yes = 924, no = 194561, maybe = 36582

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=232067, nc=64145, nnz=185873, k=2] [2.4 MB]
Creating vector for yes... [1.8 MB]
Allocating iteration vectors... [2 x 1.8 MB]
TOTAL: [7.7 MB]

Starting iterations...

Iterative method: 99 iterations in 0.38 seconds (average 0.001697, setup 0.22)

Value in the initial state: 0.9774377529477039

Time for model checking: 0.85 seconds.

Result: 0.9774377529477039 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmin=? [ F "MissionSuccessful" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=2,Objy1=1,Objx2=6,Objy2=3

Prob0E: 66 iterations in 0.50 seconds (average 0.007636, setup 0.00)

Prob1A: 3 iterations in 0.06 seconds (average 0.020000, setup 0.00)

yes = 924, no = 199488, maybe = 31655

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=232067, nc=55365, nnz=160377, k=2] [2.1 MB]
Creating vector for yes... [1.8 MB]
Allocating iteration vectors... [2 x 1.8 MB]
TOTAL: [7.4 MB]

Starting iterations...

Iterative method: 92 iterations in 0.40 seconds (average 0.002261, setup 0.20)

Value in the initial state: 0.7252218743472492

Time for model checking: 1.012 seconds.

Result: 0.7252218743472492 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmax=? [ F "MissionTimeUp" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=2,Objy1=1,Objx2=6,Objy2=3

Prob0A: 63 iterations in 0.62 seconds (average 0.009841, setup 0.00)

Prob1E: 4 iterations in 0.04 seconds (average 0.011000, setup 0.00)

yes = 404, no = 133559, maybe = 98104

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=232067, nc=168722, nnz=471753, k=4] [5.8 MB]
Creating vector for yes... [1.8 MB]
Allocating iteration vectors... [2 x 1.8 MB]
TOTAL: [11.1 MB]

Starting iterations...

Iterative method: 94 iterations in 0.52 seconds (average 0.002128, setup 0.32)

Value in the initial state: 0.003015998628266002

Time for model checking: 1.207 seconds.

Result: 0.003015998628266002 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmin=? [ F "MissionTimeUp" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=2,Objy1=1,Objx2=6,Objy2=3

Prob0E: 70 iterations in 0.64 seconds (average 0.009200, setup 0.00)

Prob1A: 3 iterations in 0.06 seconds (average 0.018667, setup 0.00)

yes = 404, no = 133973, maybe = 97690

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=232067, nc=167970, nnz=470327, k=4] [5.8 MB]
Creating vector for yes... [1.8 MB]
Allocating iteration vectors... [2 x 1.8 MB]
TOTAL: [11.1 MB]

Starting iterations...

Iterative method: 111 iterations in 0.55 seconds (average 0.002162, setup 0.31)

Value in the initial state: 1.4706191592318233E-5

Time for model checking: 1.262 seconds.

Result: 1.4706191592318233E-5 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmax=? [ F "Crash" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=2,Objy1=1,Objx2=6,Objy2=3

Prob0A: 3 iterations in 0.08 seconds (average 0.028000, setup 0.00)

Prob1E: 252 iterations in 3.10 seconds (average 0.012302, setup 0.00)

yes = 113498, no = 2563, maybe = 116006

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=232067, nc=196529, nnz=529659, k=4] [6.5 MB]
Creating vector for yes... [1.8 MB]
Allocating iteration vectors... [2 x 1.8 MB]
TOTAL: [11.8 MB]

Starting iterations...

Iterative method: 92 iterations in 0.54 seconds (average 0.002261, setup 0.34)

Value in the initial state: 0.06297160149814543

Time for model checking: 3.78 seconds.

Result: 0.06297160149814543 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmin=? [ F "Crash" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=2,Objy1=1,Objx2=6,Objy2=3

Prob0E: 3 iterations in 0.06 seconds (average 0.020000, setup 0.00)

Prob1A: 46 iterations in 0.50 seconds (average 0.010870, setup 0.00)

yes = 113498, no = 2563, maybe = 116006

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=232067, nc=196529, nnz=529659, k=4] [6.5 MB]
Creating vector for yes... [1.8 MB]
Allocating iteration vectors... [2 x 1.8 MB]
TOTAL: [11.8 MB]

Starting iterations...

Iterative method: 98 iterations in 0.48 seconds (average 0.002327, setup 0.26)

Value in the initial state: 0.0204737706389675

Time for model checking: 1.067 seconds.

Result: 0.0204737706389675 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmax=? [ F "AllAreaSearched" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=2,Objy1=1,Objx2=6,Objy2=3

Prob0A: 53 iterations in 0.45 seconds (average 0.008528, setup 0.00)

Prob1E: 4 iterations in 0.04 seconds (average 0.011000, setup 0.00)

yes = 1234, no = 178057, maybe = 52776

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=232067, nc=87152, nnz=243864, k=2] [3.1 MB]
Creating vector for yes... [1.8 MB]
Allocating iteration vectors... [2 x 1.8 MB]
TOTAL: [8.4 MB]

Starting iterations...

Iterative method: 90 iterations in 0.35 seconds (average 0.001733, setup 0.19)

Value in the initial state: 0.2453013931529875

Time for model checking: 0.865 seconds.

Result: 0.2453013931529875 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmin=? [ F "AllAreaSearched" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=2,Objy1=1,Objx2=6,Objy2=3

Prob0E: 71 iterations in 0.45 seconds (average 0.006310, setup 0.00)

Prob1A: 3 iterations in 0.03 seconds (average 0.010667, setup 0.00)

yes = 1234, no = 185437, maybe = 45396

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=232067, nc=74420, nnz=207492, k=2] [2.7 MB]
Creating vector for yes... [1.8 MB]
Allocating iteration vectors... [2 x 1.8 MB]
TOTAL: [8.0 MB]

Starting iterations...

Iterative method: 91 iterations in 0.30 seconds (average 0.001670, setup 0.14)

Value in the initial state: 0.0012735352592565777

Time for model checking: 0.788 seconds.

Result: 0.0012735352592565777 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmax=? [ F "MissionTimeUp"|"AllAreaSearched"|"Crash" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=2,Objy1=1,Objx2=6,Objy2=3

Prob0A: 3 iterations in 0.11 seconds (average 0.036000, setup 0.00)

Prob1E: 256 iterations in 3.32 seconds (average 0.012984, setup 0.00)

yes = 199487, no = 925, maybe = 31655

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=232067, nc=55365, nnz=160377, k=2] [2.1 MB]
Creating vector for yes... [1.8 MB]
Allocating iteration vectors... [2 x 1.8 MB]
TOTAL: [7.4 MB]

Starting iterations...

Iterative method: 85 iterations in 0.33 seconds (average 0.001506, setup 0.20)

Value in the initial state: 0.27477752348808676

Time for model checking: 3.775 seconds.

Result: 0.27477752348808676 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmin=? [ F "MissionTimeUp"|"AllAreaSearched"|"Crash" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=2,Objy1=1,Objx2=6,Objy2=3

Prob0E: 3 iterations in 0.08 seconds (average 0.028000, setup 0.00)

Prob1A: 52 iterations in 0.53 seconds (average 0.010154, setup 0.00)

yes = 194560, no = 925, maybe = 36582

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=232067, nc=64145, nnz=185873, k=2] [2.4 MB]
Creating vector for yes... [1.8 MB]
Allocating iteration vectors... [2 x 1.8 MB]
TOTAL: [7.7 MB]

Starting iterations...

Iterative method: 95 iterations in 0.37 seconds (average 0.001558, setup 0.22)

Value in the initial state: 0.022561950803823644

Time for model checking: 1.022 seconds.

Result: 0.022561950803823644 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"drop"}max=? [ F "deadlock" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=2,Objy1=1,Objx2=6,Objy2=3

Prob0E: 4 iterations in 0.08 seconds (average 0.020000, setup 0.00)

Prob1A: 1 iterations in 0.01 seconds (average 0.008000, setup 0.00)

goal = 1, inf = 0, maybe = 232066

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=232067, nc=312589, nnz=645719, k=4] [9.5 MB]
Building sparse matrix (transition rewards)... [n=232067, nc=312589, nnz=0, k=4] [2.1 MB]
Creating vector for state rewards... [1.8 MB]
Creating vector for inf... [1.8 MB]
Allocating iteration vectors... [2 x 1.8 MB]
TOTAL: [18.6 MB]

Starting iterations...

Iterative method: 93 iterations in 0.75 seconds (average 0.003441, setup 0.43)

Value in the initial state: 0.32552752687100756

Time for model checking: 0.855 seconds.

Result: 0.32552752687100756 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"drop"}min=? [ F "deadlock" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=2,Objy1=1,Objx2=6,Objy2=3

Prob0A: 4 iterations in 0.02 seconds (average 0.006000, setup 0.00)

Prob1E: 5 iterations in 0.04 seconds (average 0.008000, setup 0.00)

Warning: PRISM hasn't checked for zero-reward loops.
Your minimum rewards may be too low...

goal = 1, inf = 0, maybe = 232066

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=232067, nc=312589, nnz=645719, k=4] [9.5 MB]
Building sparse matrix (transition rewards)... [n=232067, nc=312589, nnz=0, k=4] [2.1 MB]
Creating vector for state rewards... [1.8 MB]
Creating vector for inf... [1.8 MB]
Allocating iteration vectors... [2 x 1.8 MB]
TOTAL: [18.6 MB]

Starting iterations...

Iterative method: 85 iterations in 0.63 seconds (average 0.003482, setup 0.33)

Value in the initial state: 0.1987735593543558

Time for model checking: 0.7 seconds.

Result: 0.1987735593543558 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"charge"}max=? [ F "deadlock" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=2,Objy1=1,Objx2=6,Objy2=3

Prob0E: 4 iterations in 0.02 seconds (average 0.004000, setup 0.00)

Prob1A: 1 iterations in 0.00 seconds (average 0.004000, setup 0.00)

goal = 1, inf = 0, maybe = 232066

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=232067, nc=312589, nnz=645719, k=4] [9.5 MB]
Building sparse matrix (transition rewards)... [n=232067, nc=312589, nnz=7898, k=4] [2.2 MB]
Creating vector for state rewards... [1.8 MB]
Creating vector for inf... [1.8 MB]
Allocating iteration vectors... [2 x 1.8 MB]
TOTAL: [18.7 MB]

Starting iterations...

Iterative method: 90 iterations in 0.64 seconds (average 0.003467, setup 0.33)

Value in the initial state: 0.9858855602462173

Time for model checking: 0.678 seconds.

Result: 0.9858855602462173 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"charge"}min=? [ F "deadlock" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=2,Objy1=1,Objx2=6,Objy2=3

Prob0A: 4 iterations in 0.02 seconds (average 0.006000, setup 0.00)

Prob1E: 5 iterations in 0.04 seconds (average 0.007200, setup 0.00)

Warning: PRISM hasn't checked for zero-reward loops.
Your minimum rewards may be too low...

goal = 1, inf = 0, maybe = 232066

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=232067, nc=312589, nnz=645719, k=4] [9.5 MB]
Building sparse matrix (transition rewards)... [n=232067, nc=312589, nnz=7898, k=4] [2.2 MB]
Creating vector for state rewards... [1.8 MB]
Creating vector for inf... [1.8 MB]
Allocating iteration vectors... [2 x 1.8 MB]
TOTAL: [18.7 MB]

Starting iterations...

Iterative method: 99 iterations in 0.67 seconds (average 0.003394, setup 0.34)

Value in the initial state: 0.09267777105625646

Time for model checking: 0.743 seconds.

Result: 0.09267777105625646 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"time"}max=? [ F "deadlock" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=2,Objy1=1,Objx2=6,Objy2=3

Prob0E: 4 iterations in 0.02 seconds (average 0.005000, setup 0.00)

Prob1A: 1 iterations in 0.00 seconds (average 0.004000, setup 0.00)

goal = 1, inf = 0, maybe = 232066

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=232067, nc=312589, nnz=645719, k=4] [9.5 MB]
Building sparse matrix (transition rewards)... [n=232067, nc=312589, nnz=116059, k=4] [3.4 MB]
Creating vector for state rewards... [1.8 MB]
Creating vector for inf... [1.8 MB]
Allocating iteration vectors... [2 x 1.8 MB]
TOTAL: [20.0 MB]

Starting iterations...

Iterative method: 95 iterations in 0.70 seconds (average 0.003453, setup 0.37)

Value in the initial state: 345.8249784713261

Time for model checking: 0.731 seconds.

Result: 345.8249784713261 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"time"}min=? [ F "deadlock" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=2,Objy1=1,Objx2=6,Objy2=3

Prob0A: 4 iterations in 0.02 seconds (average 0.005000, setup 0.00)

Prob1E: 5 iterations in 0.03 seconds (average 0.005600, setup 0.00)

Warning: PRISM hasn't checked for zero-reward loops.
Your minimum rewards may be too low...

goal = 1, inf = 0, maybe = 232066

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=232067, nc=312589, nnz=645719, k=4] [9.5 MB]
Building sparse matrix (transition rewards)... [n=232067, nc=312589, nnz=116059, k=4] [3.4 MB]
Creating vector for state rewards... [1.8 MB]
Creating vector for inf... [1.8 MB]
Allocating iteration vectors... [2 x 1.8 MB]
TOTAL: [20.0 MB]

Starting iterations...

Iterative method: 103 iterations in 0.72 seconds (average 0.003417, setup 0.37)

Value in the initial state: 113.3364010237164

Time for model checking: 0.789 seconds.

Result: 113.3364010237164 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"success"}max=? [ F "deadlock" ]/Pmin=? [ F "MissionSuccessful" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=2,Objy1=1,Objx2=6,Objy2=3

Prob0E: 4 iterations in 0.02 seconds (average 0.005000, setup 0.00)

Prob1A: 1 iterations in 0.00 seconds (average 0.004000, setup 0.00)

goal = 1, inf = 0, maybe = 232066

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=232067, nc=312589, nnz=645719, k=4] [9.5 MB]
Building sparse matrix (transition rewards)... [n=232067, nc=312589, nnz=1820, k=4] [2.1 MB]
Creating vector for state rewards... [1.8 MB]
Creating vector for inf... [1.8 MB]
Allocating iteration vectors... [2 x 1.8 MB]
TOTAL: [18.6 MB]

Starting iterations...

Iterative method: 95 iterations in 0.62 seconds (average 0.003074, setup 0.32)

Prob0E: 66 iterations in 0.26 seconds (average 0.004000, setup 0.00)

Prob1A: 3 iterations in 0.03 seconds (average 0.009333, setup 0.00)

yes = 924, no = 199488, maybe = 31655

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=232067, nc=55365, nnz=160377, k=2] [2.1 MB]
Creating vector for yes... [1.8 MB]
Allocating iteration vectors... [2 x 1.8 MB]
TOTAL: [7.4 MB]

Starting iterations...

Iterative method: 92 iterations in 0.25 seconds (average 0.001391, setup 0.12)

Value in the initial state: 465.0418574086511

Time for model checking: 1.205 seconds.

Result: 465.0418574086511 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"success"}min=? [ F "deadlock" ]/Pmax=? [ F "MissionSuccessful" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=2,Objy1=1,Objx2=6,Objy2=3

Prob0A: 4 iterations in 0.03 seconds (average 0.007000, setup 0.00)

Prob1E: 5 iterations in 0.03 seconds (average 0.005600, setup 0.00)

Warning: PRISM hasn't checked for zero-reward loops.
Your minimum rewards may be too low...

goal = 1, inf = 0, maybe = 232066

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=232067, nc=312589, nnz=645719, k=4] [9.5 MB]
Building sparse matrix (transition rewards)... [n=232067, nc=312589, nnz=1820, k=4] [2.1 MB]
Creating vector for state rewards... [1.8 MB]
Creating vector for inf... [1.8 MB]
Allocating iteration vectors... [2 x 1.8 MB]
TOTAL: [18.6 MB]

Starting iterations...

Iterative method: 97 iterations in 0.62 seconds (average 0.003134, setup 0.31)

Prob0A: 52 iterations in 0.20 seconds (average 0.003846, setup 0.00)

Prob1E: 4 iterations in 0.02 seconds (average 0.006000, setup 0.00)

yes = 924, no = 194561, maybe = 36582

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=232067, nc=64145, nnz=185873, k=2] [2.4 MB]
Creating vector for yes... [1.8 MB]
Allocating iteration vectors... [2 x 1.8 MB]
TOTAL: [7.7 MB]

Starting iterations...

Iterative method: 99 iterations in 0.26 seconds (average 0.001414, setup 0.12)

Value in the initial state: 79.32195988848433

Time for model checking: 1.187 seconds.

Result: 79.32195988848433 (value in the initial state)

---------------------------------------------------------------------

Note: There were 16 warnings during computation.

