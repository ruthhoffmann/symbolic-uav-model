PRISM
=====

Version: 4.3
Date: Tue Aug 02 12:25:34 BST 2016
Hostname: eglinton
Memory limits: cudd=1g, java(heap)=981.5m
Command line: prism uav_sim_ft_mdp.prism -s uav_mdp.props -const 'Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=1,Objy1=1,Objx2=5,Objy2=3,'

Parsing model file "uav_sim_ft_mdp.prism"...

Parsing properties file "uav_mdp.props"...

18 properties:
(1) Pmax=? [ F "MissionSuccessful" ]
(2) Pmin=? [ F "MissionSuccessful" ]
(3) Pmax=? [ F "MissionTimeUp" ]
(4) Pmin=? [ F "MissionTimeUp" ]
(5) Pmax=? [ F "Crash" ]
(6) Pmin=? [ F "Crash" ]
(7) Pmax=? [ F "AllAreaSearched" ]
(8) Pmin=? [ F "AllAreaSearched" ]
(9) Pmax=? [ F "MissionTimeUp"|"AllAreaSearched"|"Crash" ]
(10) Pmin=? [ F "MissionTimeUp"|"AllAreaSearched"|"Crash" ]
(11) R{"drop"}max=? [ F "deadlock" ]
(12) R{"drop"}min=? [ F "deadlock" ]
(13) R{"charge"}max=? [ F "deadlock" ]
(14) R{"charge"}min=? [ F "deadlock" ]
(15) R{"time"}max=? [ F "deadlock" ]
(16) R{"time"}min=? [ F "deadlock" ]
(17) R{"success"}max=? [ F "deadlock" ]/Pmin=? [ F "MissionSuccessful" ]
(18) R{"success"}min=? [ F "deadlock" ]/Pmax=? [ F "MissionSuccessful" ]

Type:        MDP
Modules:     Obj Move Robot T 
Variables:   NoOfObjs posx posy s1 retx rety b t c 

---------------------------------------------------------------------

Model checking: Pmax=? [ F "MissionSuccessful" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=1,Objy1=1,Objx2=5,Objy2=3

Building model...
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=1,Objy1=1,Objx2=5,Objy2=3

Warning: Guard for command 7 of module "Move" is never satisfied.

Warning: Guard for command 8 of module "Move" is never satisfied.

Warning: Guard for command 9 of module "Move" is never satisfied.

Warning: Guard for command 7 of module "Robot" is never satisfied.

Warning: Update 2 of command 3 of module "Robot" doesn't do anything ("(s1'=13)", line 249, column 44)

Warning: Update 2 of command 27 of module "T" doesn't do anything ("(c'=1)", line 372, column 108)

Warning: Guard for command 28 of module "T" is never satisfied.

Warning: Update 2 of command 9 of module "T" doesn't do anything ("(c'=1)", line 349, column 112)

Warning: Guard for command 10 of module "T" is never satisfied.

Warning: Update 2 of command 51 of module "T" doesn't do anything ("(c'=1)", line 403, column 113)

Warning: Guard for command 52 of module "T" is never satisfied.

Computing reachable states...

Reachability (BFS): 79 iterations in 0.84 seconds (average 0.010684, setup 0.00)

Time for model construction: 2.282 seconds.

Warning: Deadlocks detected and fixed in 1 states

Type:        MDP
States:      209385 (1 initial)
Transitions: 575635
Choices:     281199

Transition matrix: 170250 nodes (76 terminal), 575635 minterms, vars: 34r/34c/18nd

Prob0A: 52 iterations in 0.46 seconds (average 0.008846, setup 0.00)

Prob1E: 4 iterations in 0.06 seconds (average 0.014000, setup 0.00)

yes = 934, no = 174887, maybe = 33564

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=209385, nc=58592, nnz=167879, k=2] [2.2 MB]
Creating vector for yes... [1.6 MB]
Allocating iteration vectors... [2 x 1.6 MB]
TOTAL: [7.0 MB]

Starting iterations...

Iterative method: 96 iterations in 0.36 seconds (average 0.001458, setup 0.22)

Value in the initial state: 0.9779284466677742

Time for model checking: 0.949 seconds.

Result: 0.9779284466677742 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmin=? [ F "MissionSuccessful" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=1,Objy1=1,Objx2=5,Objy2=3

Prob0E: 66 iterations in 0.53 seconds (average 0.008000, setup 0.00)

Prob1A: 3 iterations in 0.04 seconds (average 0.013333, setup 0.00)

yes = 934, no = 179561, maybe = 28890

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=209385, nc=50304, nnz=143854, k=2] [1.9 MB]
Creating vector for yes... [1.6 MB]
Allocating iteration vectors... [2 x 1.6 MB]
TOTAL: [6.7 MB]

Starting iterations...

Iterative method: 90 iterations in 0.30 seconds (average 0.001422, setup 0.17)

Value in the initial state: 0.7407911717791829

Time for model checking: 0.941 seconds.

Result: 0.7407911717791829 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmax=? [ F "MissionTimeUp" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=1,Objy1=1,Objx2=5,Objy2=3

Prob0A: 63 iterations in 0.52 seconds (average 0.008254, setup 0.00)

Prob1E: 4 iterations in 0.04 seconds (average 0.010000, setup 0.00)

yes = 364, no = 121650, maybe = 87371

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=209385, nc=149210, nnz=414221, k=4] [5.1 MB]
Creating vector for yes... [1.6 MB]
Allocating iteration vectors... [2 x 1.6 MB]
TOTAL: [9.9 MB]

Starting iterations...

Iterative method: 92 iterations in 0.47 seconds (average 0.001913, setup 0.30)

Value in the initial state: 0.002118702416100426

Time for model checking: 1.343 seconds.

Result: 0.002118702416100426 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmin=? [ F "MissionTimeUp" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=1,Objy1=1,Objx2=5,Objy2=3

Prob0E: 71 iterations in 0.73 seconds (average 0.010310, setup 0.00)

Prob1A: 3 iterations in 0.05 seconds (average 0.017333, setup 0.00)

yes = 364, no = 122026, maybe = 86995

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=209385, nc=148534, nnz=412873, k=4] [5.1 MB]
Creating vector for yes... [1.6 MB]
Allocating iteration vectors... [2 x 1.6 MB]
TOTAL: [9.9 MB]

Starting iterations...

Iterative method: 107 iterations in 0.47 seconds (average 0.001869, setup 0.27)

Value in the initial state: 7.531328839944074E-6

Time for model checking: 1.347 seconds.

Result: 7.531328839944074E-6 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmax=? [ F "Crash" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=1,Objy1=1,Objx2=5,Objy2=3

Prob0A: 3 iterations in 0.09 seconds (average 0.029333, setup 0.00)

Prob1E: 252 iterations in 2.79 seconds (average 0.011079, setup 0.00)

yes = 102473, no = 2555, maybe = 104357

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=209385, nc=176171, nnz=470607, k=4] [5.8 MB]
Creating vector for yes... [1.6 MB]
Allocating iteration vectors... [2 x 1.6 MB]
TOTAL: [10.5 MB]

Starting iterations...

Iterative method: 91 iterations in 0.51 seconds (average 0.001978, setup 0.33)

Value in the initial state: 0.0626418003458363

Time for model checking: 3.668 seconds.

Result: 0.0626418003458363 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmin=? [ F "Crash" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=1,Objy1=1,Objx2=5,Objy2=3

Prob0E: 3 iterations in 0.06 seconds (average 0.021333, setup 0.00)

Prob1A: 47 iterations in 0.48 seconds (average 0.010213, setup 0.00)

yes = 102473, no = 2555, maybe = 104357

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=209385, nc=176171, nnz=470607, k=4] [5.8 MB]
Creating vector for yes... [1.6 MB]
Allocating iteration vectors... [2 x 1.6 MB]
TOTAL: [10.5 MB]

Starting iterations...

Iterative method: 95 iterations in 0.44 seconds (average 0.002105, setup 0.24)

Value in the initial state: 0.02002629366418324

Time for model checking: 1.092 seconds.

Result: 0.02002629366418324 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmax=? [ F "AllAreaSearched" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=1,Objy1=1,Objx2=5,Objy2=3

Prob0A: 51 iterations in 0.46 seconds (average 0.009098, setup 0.00)

Prob1E: 4 iterations in 0.04 seconds (average 0.010000, setup 0.00)

yes = 1256, no = 159926, maybe = 48203

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=209385, nc=79462, nnz=219830, k=2] [2.8 MB]
Creating vector for yes... [1.6 MB]
Allocating iteration vectors... [2 x 1.6 MB]
TOTAL: [7.6 MB]

Starting iterations...

Iterative method: 88 iterations in 0.32 seconds (average 0.001591, setup 0.18)

Value in the initial state: 0.23050260876052586

Time for model checking: 0.882 seconds.

Result: 0.23050260876052586 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmin=? [ F "AllAreaSearched" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=1,Objy1=1,Objx2=5,Objy2=3

Prob0E: 70 iterations in 0.40 seconds (average 0.005771, setup 0.00)

Prob1A: 3 iterations in 0.03 seconds (average 0.009333, setup 0.00)

yes = 1256, no = 166785, maybe = 41344

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=209385, nc=67712, nnz=186394, k=2] [2.4 MB]
Creating vector for yes... [1.6 MB]
Allocating iteration vectors... [2 x 1.6 MB]
TOTAL: [7.2 MB]

Starting iterations...

Iterative method: 89 iterations in 0.26 seconds (average 0.001483, setup 0.13)

Value in the initial state: 0.001260981416029517

Time for model checking: 0.743 seconds.

Result: 0.001260981416029517 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmax=? [ F "MissionTimeUp"|"AllAreaSearched"|"Crash" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=1,Objy1=1,Objx2=5,Objy2=3

Prob0A: 3 iterations in 0.08 seconds (average 0.028000, setup 0.00)

Prob1E: 256 iterations in 3.08 seconds (average 0.012031, setup 0.00)

yes = 179560, no = 935, maybe = 28890

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=209385, nc=50304, nnz=143854, k=2] [1.9 MB]
Creating vector for yes... [1.6 MB]
Allocating iteration vectors... [2 x 1.6 MB]
TOTAL: [6.7 MB]

Starting iterations...

Iterative method: 83 iterations in 0.31 seconds (average 0.001349, setup 0.20)

Value in the initial state: 0.2592081239282701

Time for model checking: 3.495 seconds.

Result: 0.2592081239282701 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmin=? [ F "MissionTimeUp"|"AllAreaSearched"|"Crash" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=1,Objy1=1,Objx2=5,Objy2=3

Prob0E: 3 iterations in 0.06 seconds (average 0.018667, setup 0.00)

Prob1A: 52 iterations in 0.50 seconds (average 0.009538, setup 0.00)

yes = 174886, no = 935, maybe = 33564

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=209385, nc=58592, nnz=167879, k=2] [2.2 MB]
Creating vector for yes... [1.6 MB]
Allocating iteration vectors... [2 x 1.6 MB]
TOTAL: [7.0 MB]

Starting iterations...

Iterative method: 93 iterations in 0.31 seconds (average 0.001505, setup 0.17)

Value in the initial state: 0.022070995244541078

Time for model checking: 0.89 seconds.

Result: 0.022070995244541078 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"drop"}max=? [ F "deadlock" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=1,Objy1=1,Objx2=5,Objy2=3

Prob0E: 4 iterations in 0.06 seconds (average 0.016000, setup 0.00)

Prob1A: 1 iterations in 0.01 seconds (average 0.008000, setup 0.00)

goal = 1, inf = 0, maybe = 209384

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=209385, nc=281198, nnz=575634, k=4] [8.5 MB]
Building sparse matrix (transition rewards)... [n=209385, nc=281198, nnz=0, k=4] [1.9 MB]
Creating vector for state rewards... [1.6 MB]
Creating vector for inf... [1.6 MB]
Allocating iteration vectors... [2 x 1.6 MB]
TOTAL: [16.7 MB]

Starting iterations...

Iterative method: 91 iterations in 0.68 seconds (average 0.003121, setup 0.40)

Value in the initial state: 0.2779380754018652

Time for model checking: 0.764 seconds.

Result: 0.2779380754018652 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"drop"}min=? [ F "deadlock" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=1,Objy1=1,Objx2=5,Objy2=3

Prob0A: 4 iterations in 0.04 seconds (average 0.009000, setup 0.00)

Prob1E: 5 iterations in 0.04 seconds (average 0.008800, setup 0.00)

Warning: PRISM hasn't checked for zero-reward loops.
Your minimum rewards may be too low...

goal = 1, inf = 0, maybe = 209384

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=209385, nc=281198, nnz=575634, k=4] [8.5 MB]
Building sparse matrix (transition rewards)... [n=209385, nc=281198, nnz=0, k=4] [1.9 MB]
Creating vector for state rewards... [1.6 MB]
Creating vector for inf... [1.6 MB]
Allocating iteration vectors... [2 x 1.6 MB]
TOTAL: [16.7 MB]

Starting iterations...

Iterative method: 84 iterations in 0.57 seconds (average 0.003143, setup 0.30)

Value in the initial state: 0.17802535520607152

Time for model checking: 0.659 seconds.

Result: 0.17802535520607152 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"charge"}max=? [ F "deadlock" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=1,Objy1=1,Objx2=5,Objy2=3

Prob0E: 4 iterations in 0.02 seconds (average 0.006000, setup 0.00)

Prob1A: 1 iterations in 0.00 seconds (average 0.004000, setup 0.00)

goal = 1, inf = 0, maybe = 209384

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=209385, nc=281198, nnz=575634, k=4] [8.5 MB]
Building sparse matrix (transition rewards)... [n=209385, nc=281198, nnz=6842, k=4] [1.9 MB]
Creating vector for state rewards... [1.6 MB]
Creating vector for inf... [1.6 MB]
Allocating iteration vectors... [2 x 1.6 MB]
TOTAL: [16.8 MB]

Starting iterations...

Iterative method: 86 iterations in 0.58 seconds (average 0.003070, setup 0.31)

Value in the initial state: 0.9844790741001516

Time for model checking: 0.617 seconds.

Result: 0.9844790741001516 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"charge"}min=? [ F "deadlock" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=1,Objy1=1,Objx2=5,Objy2=3

Prob0A: 4 iterations in 0.02 seconds (average 0.005000, setup 0.00)

Prob1E: 5 iterations in 0.04 seconds (average 0.008000, setup 0.00)

Warning: PRISM hasn't checked for zero-reward loops.
Your minimum rewards may be too low...

goal = 1, inf = 0, maybe = 209384

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=209385, nc=281198, nnz=575634, k=4] [8.5 MB]
Building sparse matrix (transition rewards)... [n=209385, nc=281198, nnz=6842, k=4] [1.9 MB]
Creating vector for state rewards... [1.6 MB]
Creating vector for inf... [1.6 MB]
Allocating iteration vectors... [2 x 1.6 MB]
TOTAL: [16.8 MB]

Starting iterations...

Iterative method: 97 iterations in 0.61 seconds (average 0.003093, setup 0.31)

Value in the initial state: 0.07708571052537867

Time for model checking: 0.685 seconds.

Result: 0.07708571052537867 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"time"}max=? [ F "deadlock" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=1,Objy1=1,Objx2=5,Objy2=3

Prob0E: 4 iterations in 0.02 seconds (average 0.006000, setup 0.00)

Prob1A: 1 iterations in 0.00 seconds (average 0.004000, setup 0.00)

goal = 1, inf = 0, maybe = 209384

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=209385, nc=281198, nnz=575634, k=4] [8.5 MB]
Building sparse matrix (transition rewards)... [n=209385, nc=281198, nnz=105026, k=4] [3.1 MB]
Creating vector for state rewards... [1.6 MB]
Creating vector for inf... [1.6 MB]
Allocating iteration vectors... [2 x 1.6 MB]
TOTAL: [17.9 MB]

Starting iterations...

Iterative method: 92 iterations in 0.63 seconds (average 0.003087, setup 0.34)

Value in the initial state: 344.10573813933763

Time for model checking: 0.672 seconds.

Result: 344.10573813933763 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"time"}min=? [ F "deadlock" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=1,Objy1=1,Objx2=5,Objy2=3

Prob0A: 4 iterations in 0.02 seconds (average 0.005000, setup 0.00)

Prob1E: 5 iterations in 0.04 seconds (average 0.008000, setup 0.00)

Warning: PRISM hasn't checked for zero-reward loops.
Your minimum rewards may be too low...

goal = 1, inf = 0, maybe = 209384

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=209385, nc=281198, nnz=575634, k=4] [8.5 MB]
Building sparse matrix (transition rewards)... [n=209385, nc=281198, nnz=105026, k=4] [3.1 MB]
Creating vector for state rewards... [1.6 MB]
Creating vector for inf... [1.6 MB]
Allocating iteration vectors... [2 x 1.6 MB]
TOTAL: [17.9 MB]

Starting iterations...

Iterative method: 101 iterations in 0.66 seconds (average 0.003208, setup 0.33)

Value in the initial state: 110.88611221187071

Time for model checking: 0.727 seconds.

Result: 110.88611221187071 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"success"}max=? [ F "deadlock" ]/Pmin=? [ F "MissionSuccessful" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=1,Objy1=1,Objx2=5,Objy2=3

Prob0E: 4 iterations in 0.02 seconds (average 0.006000, setup 0.00)

Prob1A: 1 iterations in 0.01 seconds (average 0.008000, setup 0.00)

goal = 1, inf = 0, maybe = 209384

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=209385, nc=281198, nnz=575634, k=4] [8.5 MB]
Building sparse matrix (transition rewards)... [n=209385, nc=281198, nnz=1840, k=4] [1.9 MB]
Creating vector for state rewards... [1.6 MB]
Creating vector for inf... [1.6 MB]
Allocating iteration vectors... [2 x 1.6 MB]
TOTAL: [16.7 MB]

Starting iterations...

Iterative method: 96 iterations in 0.58 seconds (average 0.003042, setup 0.29)

Prob0E: 66 iterations in 0.46 seconds (average 0.007030, setup 0.00)

Prob1A: 3 iterations in 0.04 seconds (average 0.012000, setup 0.00)

yes = 934, no = 179561, maybe = 28890

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=209385, nc=50304, nnz=143854, k=2] [1.9 MB]
Creating vector for yes... [1.6 MB]
Allocating iteration vectors... [2 x 1.6 MB]
TOTAL: [6.7 MB]

Starting iterations...

Iterative method: 90 iterations in 0.27 seconds (average 0.001333, setup 0.15)

Value in the initial state: 453.46537047183125

Time for model checking: 1.434 seconds.

Result: 453.46537047183125 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"success"}min=? [ F "deadlock" ]/Pmax=? [ F "MissionSuccessful" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=1,Objy1=1,Objx2=5,Objy2=3

Prob0A: 4 iterations in 0.08 seconds (average 0.020000, setup 0.00)

Prob1E: 5 iterations in 0.04 seconds (average 0.008000, setup 0.00)

Warning: PRISM hasn't checked for zero-reward loops.
Your minimum rewards may be too low...

goal = 1, inf = 0, maybe = 209384

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=209385, nc=281198, nnz=575634, k=4] [8.5 MB]
Building sparse matrix (transition rewards)... [n=209385, nc=281198, nnz=1840, k=4] [1.9 MB]
Creating vector for state rewards... [1.6 MB]
Creating vector for inf... [1.6 MB]
Allocating iteration vectors... [2 x 1.6 MB]
TOTAL: [16.7 MB]

Starting iterations...

Iterative method: 95 iterations in 0.64 seconds (average 0.003074, setup 0.34)

Prob0A: 52 iterations in 0.22 seconds (average 0.004231, setup 0.00)

Prob1E: 4 iterations in 0.03 seconds (average 0.007000, setup 0.00)

yes = 934, no = 174887, maybe = 33564

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=209385, nc=58592, nnz=167879, k=2] [2.2 MB]
Creating vector for yes... [1.6 MB]
Allocating iteration vectors... [2 x 1.6 MB]
TOTAL: [7.0 MB]

Starting iterations...

Iterative method: 96 iterations in 0.29 seconds (average 0.001417, setup 0.15)

Value in the initial state: 79.92276017887589

Time for model checking: 1.317 seconds.

Result: 79.92276017887589 (value in the initial state)

---------------------------------------------------------------------

Note: There were 16 warnings during computation.

