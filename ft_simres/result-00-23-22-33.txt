PRISM
=====

Version: 4.3
Date: Tue Aug 02 12:27:53 BST 2016
Hostname: eglinton
Memory limits: cudd=1g, java(heap)=981.5m
Command line: prism uav_sim_ft_mdp.prism -s uav_mdp.props -const 'Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=2,Objy1=2,Objx2=3,Objy2=3,'

Parsing model file "uav_sim_ft_mdp.prism"...

Parsing properties file "uav_mdp.props"...

18 properties:
(1) Pmax=? [ F "MissionSuccessful" ]
(2) Pmin=? [ F "MissionSuccessful" ]
(3) Pmax=? [ F "MissionTimeUp" ]
(4) Pmin=? [ F "MissionTimeUp" ]
(5) Pmax=? [ F "Crash" ]
(6) Pmin=? [ F "Crash" ]
(7) Pmax=? [ F "AllAreaSearched" ]
(8) Pmin=? [ F "AllAreaSearched" ]
(9) Pmax=? [ F "MissionTimeUp"|"AllAreaSearched"|"Crash" ]
(10) Pmin=? [ F "MissionTimeUp"|"AllAreaSearched"|"Crash" ]
(11) R{"drop"}max=? [ F "deadlock" ]
(12) R{"drop"}min=? [ F "deadlock" ]
(13) R{"charge"}max=? [ F "deadlock" ]
(14) R{"charge"}min=? [ F "deadlock" ]
(15) R{"time"}max=? [ F "deadlock" ]
(16) R{"time"}min=? [ F "deadlock" ]
(17) R{"success"}max=? [ F "deadlock" ]/Pmin=? [ F "MissionSuccessful" ]
(18) R{"success"}min=? [ F "deadlock" ]/Pmax=? [ F "MissionSuccessful" ]

Type:        MDP
Modules:     Obj Move Robot T 
Variables:   NoOfObjs posx posy s1 retx rety b t c 

---------------------------------------------------------------------

Model checking: Pmax=? [ F "MissionSuccessful" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=2,Objy1=2,Objx2=3,Objy2=3

Building model...
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=2,Objy1=2,Objx2=3,Objy2=3

Warning: Guard for command 7 of module "Move" is never satisfied.

Warning: Guard for command 8 of module "Move" is never satisfied.

Warning: Guard for command 9 of module "Move" is never satisfied.

Warning: Guard for command 7 of module "Robot" is never satisfied.

Warning: Update 2 of command 3 of module "Robot" doesn't do anything ("(s1'=13)", line 249, column 44)

Warning: Update 2 of command 27 of module "T" doesn't do anything ("(c'=1)", line 372, column 108)

Warning: Guard for command 28 of module "T" is never satisfied.

Warning: Update 2 of command 9 of module "T" doesn't do anything ("(c'=1)", line 349, column 112)

Warning: Guard for command 10 of module "T" is never satisfied.

Warning: Update 2 of command 51 of module "T" doesn't do anything ("(c'=1)", line 403, column 113)

Warning: Guard for command 52 of module "T" is never satisfied.

Computing reachable states...

Reachability (BFS): 80 iterations in 0.59 seconds (average 0.007400, setup 0.00)

Time for model construction: 1.665 seconds.

Warning: Deadlocks detected and fixed in 1 states

Type:        MDP
States:      143733 (1 initial)
Transitions: 378406
Choices:     191553

Transition matrix: 157893 nodes (76 terminal), 378406 minterms, vars: 34r/34c/18nd

Prob0A: 51 iterations in 0.29 seconds (average 0.005647, setup 0.00)

Prob1E: 4 iterations in 0.04 seconds (average 0.011000, setup 0.00)

yes = 958, no = 118089, maybe = 24686

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=143733, nc=42806, nnz=117504, k=2] [1.5 MB]
Creating vector for yes... [1.1 MB]
Allocating iteration vectors... [2 x 1.1 MB]
TOTAL: [4.8 MB]

Starting iterations...

Iterative method: 89 iterations in 0.26 seconds (average 0.000989, setup 0.18)

Value in the initial state: 0.9799365673677385

Time for model checking: 0.619 seconds.

Result: 0.9799365673677385 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmin=? [ F "MissionSuccessful" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=2,Objy1=2,Objx2=3,Objy2=3

Prob0E: 64 iterations in 0.40 seconds (average 0.006313, setup 0.00)

Prob1A: 3 iterations in 0.04 seconds (average 0.013333, setup 0.00)

yes = 958, no = 121353, maybe = 21422

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=143733, nc=37110, nnz=101337, k=2] [1.3 MB]
Creating vector for yes... [1.1 MB]
Allocating iteration vectors... [2 x 1.1 MB]
TOTAL: [4.6 MB]

Starting iterations...

Iterative method: 86 iterations in 0.22 seconds (average 0.000884, setup 0.14)

Value in the initial state: 0.7871140211303868

Time for model checking: 0.685 seconds.

Result: 0.7871140211303868 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmax=? [ F "MissionTimeUp" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=2,Objy1=2,Objx2=3,Objy2=3

Prob0A: 67 iterations in 0.43 seconds (average 0.006448, setup 0.00)

Prob1E: 4 iterations in 0.03 seconds (average 0.008000, setup 0.00)

yes = 232, no = 87769, maybe = 55732

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=143733, nc=93464, nnz=252509, k=4] [3.1 MB]
Creating vector for yes... [1.1 MB]
Allocating iteration vectors... [2 x 1.1 MB]
TOTAL: [6.4 MB]

Starting iterations...

Iterative method: 86 iterations in 0.30 seconds (average 0.001256, setup 0.19)

Value in the initial state: 5.766141831271883E-4

Time for model checking: 0.784 seconds.

Result: 5.766141831271883E-4 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmin=? [ F "MissionTimeUp" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=2,Objy1=2,Objx2=3,Objy2=3

Prob0E: 74 iterations in 0.46 seconds (average 0.006216, setup 0.00)

Prob1A: 3 iterations in 0.04 seconds (average 0.012000, setup 0.00)

yes = 232, no = 88069, maybe = 55432

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=143733, nc=92940, nnz=251317, k=4] [3.1 MB]
Creating vector for yes... [1.1 MB]
Allocating iteration vectors... [2 x 1.1 MB]
TOTAL: [6.4 MB]

Starting iterations...

Iterative method: 103 iterations in 0.32 seconds (average 0.001204, setup 0.19)

Value in the initial state: 1.0101535828740373E-6

Time for model checking: 0.828 seconds.

Result: 1.0101535828740373E-6 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmax=? [ F "Crash" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=2,Objy1=2,Objx2=3,Objy2=3

Prob0A: 3 iterations in 0.07 seconds (average 0.024000, setup 0.00)

Prob1E: 228 iterations in 2.28 seconds (average 0.010018, setup 0.00)

yes = 70576, no = 2493, maybe = 70664

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=143733, nc=118484, nnz=305337, k=4] [3.7 MB]
Creating vector for yes... [1.1 MB]
Allocating iteration vectors... [2 x 1.1 MB]
TOTAL: [7.0 MB]

Starting iterations...

Iterative method: 87 iterations in 0.36 seconds (average 0.001333, setup 0.24)

Value in the initial state: 0.06158189300011085

Time for model checking: 2.725 seconds.

Result: 0.06158189300011085 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmin=? [ F "Crash" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=2,Objy1=2,Objx2=3,Objy2=3

Prob0E: 3 iterations in 0.05 seconds (average 0.017333, setup 0.00)

Prob1A: 50 iterations in 0.38 seconds (average 0.007520, setup 0.00)

yes = 70576, no = 2493, maybe = 70664

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=143733, nc=118484, nnz=305337, k=4] [3.7 MB]
Creating vector for yes... [1.1 MB]
Allocating iteration vectors... [2 x 1.1 MB]
TOTAL: [7.0 MB]

Starting iterations...

Iterative method: 83 iterations in 0.26 seconds (average 0.001349, setup 0.14)

Value in the initial state: 0.018130622006344462

Time for model checking: 0.705 seconds.

Result: 0.018130622006344462 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmax=? [ F "AllAreaSearched" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=2,Objy1=2,Objx2=3,Objy2=3

Prob0A: 50 iterations in 0.31 seconds (average 0.006160, setup 0.00)

Prob1E: 4 iterations in 0.04 seconds (average 0.009000, setup 0.00)

yes = 1302, no = 106764, maybe = 35667

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=143733, nc=58792, nnz=156291, k=2] [2.0 MB]
Creating vector for yes... [1.1 MB]
Allocating iteration vectors... [2 x 1.1 MB]
TOTAL: [5.3 MB]

Starting iterations...

Iterative method: 81 iterations in 0.22 seconds (average 0.001086, setup 0.13)

Value in the initial state: 0.186815905517289

Time for model checking: 0.581 seconds.

Result: 0.186815905517289 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmin=? [ F "AllAreaSearched" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=2,Objy1=2,Objx2=3,Objy2=3

Prob0E: 60 iterations in 0.30 seconds (average 0.005067, setup 0.00)

Prob1A: 3 iterations in 0.02 seconds (average 0.006667, setup 0.00)

yes = 1302, no = 112129, maybe = 30302

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=143733, nc=49705, nnz=130580, k=2] [1.7 MB]
Creating vector for yes... [1.1 MB]
Allocating iteration vectors... [2 x 1.1 MB]
TOTAL: [5.0 MB]

Starting iterations...

Iterative method: 86 iterations in 0.18 seconds (average 0.001070, setup 0.09)

Value in the initial state: 0.0012145453588055849

Time for model checking: 0.508 seconds.

Result: 0.0012145453588055849 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmax=? [ F "MissionTimeUp"|"AllAreaSearched"|"Crash" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=2,Objy1=2,Objx2=3,Objy2=3

Prob0A: 3 iterations in 0.08 seconds (average 0.028000, setup 0.00)

Prob1E: 248 iterations in 2.47 seconds (average 0.009952, setup 0.00)

yes = 121352, no = 959, maybe = 21422

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=143733, nc=37110, nnz=101337, k=2] [1.3 MB]
Creating vector for yes... [1.1 MB]
Allocating iteration vectors... [2 x 1.1 MB]
TOTAL: [4.6 MB]

Starting iterations...

Iterative method: 80 iterations in 0.21 seconds (average 0.000850, setup 0.14)

Value in the initial state: 0.21288582006936643

Time for model checking: 2.758 seconds.

Result: 0.21288582006936643 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmin=? [ F "MissionTimeUp"|"AllAreaSearched"|"Crash" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=2,Objy1=2,Objx2=3,Objy2=3

Prob0E: 3 iterations in 0.03 seconds (average 0.010667, setup 0.00)

Prob1A: 51 iterations in 0.34 seconds (average 0.006588, setup 0.00)

yes = 118088, no = 959, maybe = 24686

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=143733, nc=42806, nnz=117504, k=2] [1.5 MB]
Creating vector for yes... [1.1 MB]
Allocating iteration vectors... [2 x 1.1 MB]
TOTAL: [4.8 MB]

Starting iterations...

Iterative method: 87 iterations in 0.19 seconds (average 0.000920, setup 0.11)

Value in the initial state: 0.02006238966625851

Time for model checking: 0.586 seconds.

Result: 0.02006238966625851 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"drop"}max=? [ F "deadlock" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=2,Objy1=2,Objx2=3,Objy2=3

Prob0E: 4 iterations in 0.04 seconds (average 0.011000, setup 0.00)

Prob1A: 1 iterations in 0.00 seconds (average 0.004000, setup 0.00)

goal = 1, inf = 0, maybe = 143732

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=143733, nc=191552, nnz=378405, k=4] [5.6 MB]
Building sparse matrix (transition rewards)... [n=143733, nc=191552, nnz=0, k=4] [1.3 MB]
Creating vector for state rewards... [1.1 MB]
Creating vector for inf... [1.1 MB]
Allocating iteration vectors... [2 x 1.1 MB]
TOTAL: [11.3 MB]

Starting iterations...

Iterative method: 85 iterations in 0.45 seconds (average 0.002024, setup 0.28)

Value in the initial state: 0.15297782589401368

Time for model checking: 0.511 seconds.

Result: 0.15297782589401368 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"drop"}min=? [ F "deadlock" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=2,Objy1=2,Objx2=3,Objy2=3

Prob0A: 4 iterations in 0.02 seconds (average 0.006000, setup 0.00)

Prob1E: 5 iterations in 0.04 seconds (average 0.007200, setup 0.00)

Warning: PRISM hasn't checked for zero-reward loops.
Your minimum rewards may be too low...

goal = 1, inf = 0, maybe = 143732

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=143733, nc=191552, nnz=378405, k=4] [5.6 MB]
Building sparse matrix (transition rewards)... [n=143733, nc=191552, nnz=0, k=4] [1.3 MB]
Creating vector for state rewards... [1.1 MB]
Creating vector for inf... [1.1 MB]
Allocating iteration vectors... [2 x 1.1 MB]
TOTAL: [11.3 MB]

Starting iterations...

Iterative method: 79 iterations in 0.37 seconds (average 0.002076, setup 0.21)

Value in the initial state: 0.10747133416249703

Time for model checking: 0.442 seconds.

Result: 0.10747133416249703 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"charge"}max=? [ F "deadlock" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=2,Objy1=2,Objx2=3,Objy2=3

Prob0E: 4 iterations in 0.02 seconds (average 0.005000, setup 0.00)

Prob1A: 1 iterations in 0.00 seconds (average 0.004000, setup 0.00)

goal = 1, inf = 0, maybe = 143732

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=143733, nc=191552, nnz=378405, k=4] [5.6 MB]
Building sparse matrix (transition rewards)... [n=143733, nc=191552, nnz=4058, k=4] [1.3 MB]
Creating vector for state rewards... [1.1 MB]
Creating vector for inf... [1.1 MB]
Allocating iteration vectors... [2 x 1.1 MB]
TOTAL: [11.3 MB]

Starting iterations...

Iterative method: 82 iterations in 0.38 seconds (average 0.002049, setup 0.21)

Value in the initial state: 0.9800106831410706

Time for model checking: 0.413 seconds.

Result: 0.9800106831410706 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"charge"}min=? [ F "deadlock" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=2,Objy1=2,Objx2=3,Objy2=3

Prob0A: 4 iterations in 0.02 seconds (average 0.005000, setup 0.00)

Prob1E: 5 iterations in 0.04 seconds (average 0.008000, setup 0.00)

Warning: PRISM hasn't checked for zero-reward loops.
Your minimum rewards may be too low...

goal = 1, inf = 0, maybe = 143732

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=143733, nc=191552, nnz=378405, k=4] [5.6 MB]
Building sparse matrix (transition rewards)... [n=143733, nc=191552, nnz=4058, k=4] [1.3 MB]
Creating vector for state rewards... [1.1 MB]
Creating vector for inf... [1.1 MB]
Allocating iteration vectors... [2 x 1.1 MB]
TOTAL: [11.3 MB]

Starting iterations...

Iterative method: 91 iterations in 0.40 seconds (average 0.002110, setup 0.21)

Value in the initial state: 0.03760994708426536

Time for model checking: 0.47 seconds.

Result: 0.03760994708426536 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"time"}max=? [ F "deadlock" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=2,Objy1=2,Objx2=3,Objy2=3

Prob0E: 4 iterations in 0.02 seconds (average 0.005000, setup 0.00)

Prob1A: 1 iterations in 0.00 seconds (average 0.004000, setup 0.00)

goal = 1, inf = 0, maybe = 143732

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=143733, nc=191552, nnz=378405, k=4] [5.6 MB]
Building sparse matrix (transition rewards)... [n=143733, nc=191552, nnz=73067, k=4] [2.1 MB]
Creating vector for state rewards... [1.1 MB]
Creating vector for inf... [1.1 MB]
Allocating iteration vectors... [2 x 1.1 MB]
TOTAL: [12.1 MB]

Starting iterations...

Iterative method: 89 iterations in 0.42 seconds (average 0.002112, setup 0.24)

Value in the initial state: 338.4603189567415

Time for model checking: 0.464 seconds.

Result: 338.4603189567415 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"time"}min=? [ F "deadlock" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=2,Objy1=2,Objx2=3,Objy2=3

Prob0A: 4 iterations in 0.02 seconds (average 0.004000, setup 0.00)

Prob1E: 5 iterations in 0.04 seconds (average 0.007200, setup 0.00)

Warning: PRISM hasn't checked for zero-reward loops.
Your minimum rewards may be too low...

goal = 1, inf = 0, maybe = 143732

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=143733, nc=191552, nnz=378405, k=4] [5.6 MB]
Building sparse matrix (transition rewards)... [n=143733, nc=191552, nnz=73067, k=4] [2.1 MB]
Creating vector for state rewards... [1.1 MB]
Creating vector for inf... [1.1 MB]
Allocating iteration vectors... [2 x 1.1 MB]
TOTAL: [12.1 MB]

Starting iterations...

Iterative method: 94 iterations in 0.43 seconds (average 0.002085, setup 0.23)

Value in the initial state: 100.4986140590679

Time for model checking: 0.497 seconds.

Result: 100.4986140590679 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"success"}max=? [ F "deadlock" ]/Pmin=? [ F "MissionSuccessful" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=2,Objy1=2,Objx2=3,Objy2=3

Prob0E: 4 iterations in 0.02 seconds (average 0.005000, setup 0.00)

Prob1A: 1 iterations in 0.00 seconds (average 0.004000, setup 0.00)

goal = 1, inf = 0, maybe = 143732

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=143733, nc=191552, nnz=378405, k=4] [5.6 MB]
Building sparse matrix (transition rewards)... [n=143733, nc=191552, nnz=1888, k=4] [1.3 MB]
Creating vector for state rewards... [1.1 MB]
Creating vector for inf... [1.1 MB]
Allocating iteration vectors... [2 x 1.1 MB]
TOTAL: [11.3 MB]

Starting iterations...

Iterative method: 91 iterations in 0.39 seconds (average 0.002022, setup 0.20)

Prob0E: 64 iterations in 0.37 seconds (average 0.005750, setup 0.00)

Prob1A: 3 iterations in 0.03 seconds (average 0.010667, setup 0.00)

yes = 958, no = 121353, maybe = 21422

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=143733, nc=37110, nnz=101337, k=2] [1.3 MB]
Creating vector for yes... [1.1 MB]
Allocating iteration vectors... [2 x 1.1 MB]
TOTAL: [4.6 MB]

Starting iterations...

Iterative method: 86 iterations in 0.15 seconds (average 0.000930, setup 0.07)

Value in the initial state: 420.46174318127424

Time for model checking: 1.006 seconds.

Result: 420.46174318127424 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"success"}min=? [ F "deadlock" ]/Pmax=? [ F "MissionSuccessful" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=2,Objy1=2,Objx2=3,Objy2=3

Prob0A: 4 iterations in 0.02 seconds (average 0.005000, setup 0.00)

Prob1E: 5 iterations in 0.03 seconds (average 0.006400, setup 0.00)

Warning: PRISM hasn't checked for zero-reward loops.
Your minimum rewards may be too low...

goal = 1, inf = 0, maybe = 143732

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=143733, nc=191552, nnz=378405, k=4] [5.6 MB]
Building sparse matrix (transition rewards)... [n=143733, nc=191552, nnz=1888, k=4] [1.3 MB]
Creating vector for state rewards... [1.1 MB]
Creating vector for inf... [1.1 MB]
Allocating iteration vectors... [2 x 1.1 MB]
TOTAL: [11.3 MB]

Starting iterations...

Iterative method: 90 iterations in 0.40 seconds (average 0.002089, setup 0.21)

Prob0A: 51 iterations in 0.22 seconds (average 0.004392, setup 0.00)

Prob1E: 4 iterations in 0.02 seconds (average 0.006000, setup 0.00)

yes = 958, no = 118089, maybe = 24686

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=143733, nc=42806, nnz=117504, k=2] [1.5 MB]
Creating vector for yes... [1.1 MB]
Allocating iteration vectors... [2 x 1.1 MB]
TOTAL: [4.8 MB]

Starting iterations...

Iterative method: 89 iterations in 0.20 seconds (average 0.000989, setup 0.11)

Value in the initial state: 80.80287793564744

Time for model checking: 0.925 seconds.

Result: 80.80287793564744 (value in the initial state)

---------------------------------------------------------------------

Note: There were 16 warnings during computation.

