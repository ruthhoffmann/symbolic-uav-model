PRISM
=====

Version: 4.3
Date: Tue Aug 02 12:37:17 BST 2016
Hostname: eglinton
Memory limits: cudd=1g, java(heap)=981.5m
Command line: prism uav_sim_ft_mdp.prism -s uav_mdp.props -const 'Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=1,Objy1=2,Objx2=4,Objy2=0,'

Parsing model file "uav_sim_ft_mdp.prism"...

Parsing properties file "uav_mdp.props"...

18 properties:
(1) Pmax=? [ F "MissionSuccessful" ]
(2) Pmin=? [ F "MissionSuccessful" ]
(3) Pmax=? [ F "MissionTimeUp" ]
(4) Pmin=? [ F "MissionTimeUp" ]
(5) Pmax=? [ F "Crash" ]
(6) Pmin=? [ F "Crash" ]
(7) Pmax=? [ F "AllAreaSearched" ]
(8) Pmin=? [ F "AllAreaSearched" ]
(9) Pmax=? [ F "MissionTimeUp"|"AllAreaSearched"|"Crash" ]
(10) Pmin=? [ F "MissionTimeUp"|"AllAreaSearched"|"Crash" ]
(11) R{"drop"}max=? [ F "deadlock" ]
(12) R{"drop"}min=? [ F "deadlock" ]
(13) R{"charge"}max=? [ F "deadlock" ]
(14) R{"charge"}min=? [ F "deadlock" ]
(15) R{"time"}max=? [ F "deadlock" ]
(16) R{"time"}min=? [ F "deadlock" ]
(17) R{"success"}max=? [ F "deadlock" ]/Pmin=? [ F "MissionSuccessful" ]
(18) R{"success"}min=? [ F "deadlock" ]/Pmax=? [ F "MissionSuccessful" ]

Type:        MDP
Modules:     Obj Move Robot T 
Variables:   NoOfObjs posx posy s1 retx rety b t c 

---------------------------------------------------------------------

Model checking: Pmax=? [ F "MissionSuccessful" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=1,Objy1=2,Objx2=4,Objy2=0

Building model...
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=1,Objy1=2,Objx2=4,Objy2=0

Warning: Guard for command 7 of module "Move" is never satisfied.

Warning: Guard for command 8 of module "Move" is never satisfied.

Warning: Guard for command 9 of module "Move" is never satisfied.

Warning: Guard for command 7 of module "Robot" is never satisfied.

Warning: Update 2 of command 3 of module "Robot" doesn't do anything ("(s1'=13)", line 249, column 44)

Warning: Update 2 of command 27 of module "T" doesn't do anything ("(c'=1)", line 372, column 108)

Warning: Guard for command 28 of module "T" is never satisfied.

Warning: Update 2 of command 9 of module "T" doesn't do anything ("(c'=1)", line 349, column 112)

Warning: Guard for command 10 of module "T" is never satisfied.

Warning: Update 2 of command 51 of module "T" doesn't do anything ("(c'=1)", line 403, column 113)

Warning: Guard for command 52 of module "T" is never satisfied.

Computing reachable states...

Reachability (BFS): 74 iterations in 0.94 seconds (average 0.012757, setup 0.00)

Time for model construction: 2.345 seconds.

Warning: Deadlocks detected and fixed in 1 states

Type:        MDP
States:      204531 (1 initial)
Transitions: 522686
Choices:     266036

Transition matrix: 174584 nodes (76 terminal), 522686 minterms, vars: 34r/34c/18nd

Prob0A: 49 iterations in 0.44 seconds (average 0.009061, setup 0.00)

Prob1E: 4 iterations in 0.06 seconds (average 0.016000, setup 0.00)

yes = 959, no = 173873, maybe = 29699

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=204531, nc=51095, nnz=143067, k=2] [1.9 MB]
Creating vector for yes... [1.6 MB]
Allocating iteration vectors... [2 x 1.6 MB]
TOTAL: [6.6 MB]

Starting iterations...

Iterative method: 89 iterations in 0.34 seconds (average 0.001393, setup 0.22)

Value in the initial state: 0.9808367299738332

Time for model checking: 0.877 seconds.

Result: 0.9808367299738332 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmin=? [ F "MissionSuccessful" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=1,Objy1=2,Objx2=4,Objy2=0

Prob0E: 51 iterations in 0.46 seconds (average 0.008941, setup 0.00)

Prob1A: 3 iterations in 0.04 seconds (average 0.014667, setup 0.00)

yes = 959, no = 178941, maybe = 24631

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=204531, nc=42249, nnz=117242, k=2] [1.6 MB]
Creating vector for yes... [1.6 MB]
Allocating iteration vectors... [2 x 1.6 MB]
TOTAL: [6.3 MB]

Starting iterations...

Iterative method: 82 iterations in 0.28 seconds (average 0.001317, setup 0.17)

Value in the initial state: 0.7576452919906504

Time for model checking: 0.795 seconds.

Result: 0.7576452919906504 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmax=? [ F "MissionTimeUp" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=1,Objy1=2,Objx2=4,Objy2=0

Prob0A: 57 iterations in 0.53 seconds (average 0.009333, setup 0.00)

Prob1E: 4 iterations in 0.05 seconds (average 0.012000, setup 0.00)

yes = 432, no = 126600, maybe = 77499

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=204531, nc=129142, nnz=349817, k=4] [4.3 MB]
Creating vector for yes... [1.6 MB]
Allocating iteration vectors... [2 x 1.6 MB]
TOTAL: [9.0 MB]

Starting iterations...

Iterative method: 87 iterations in 0.42 seconds (average 0.001701, setup 0.27)

Value in the initial state: 6.750398112081543E-4

Time for model checking: 1.022 seconds.

Result: 6.750398112081543E-4 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmin=? [ F "MissionTimeUp" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=1,Objy1=2,Objx2=4,Objy2=0

Prob0E: 65 iterations in 0.72 seconds (average 0.011138, setup 0.00)

Prob1A: 3 iterations in 0.04 seconds (average 0.013333, setup 0.00)

yes = 432, no = 126919, maybe = 77180

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=204531, nc=128580, nnz=348586, k=4] [4.3 MB]
Creating vector for yes... [1.6 MB]
Allocating iteration vectors... [2 x 1.6 MB]
TOTAL: [9.0 MB]

Starting iterations...

Iterative method: 104 iterations in 0.42 seconds (average 0.001769, setup 0.23)

Value in the initial state: 1.5306275101425028E-6

Time for model checking: 1.202 seconds.

Result: 1.5306275101425028E-6 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmax=? [ F "Crash" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=1,Objy1=2,Objx2=4,Objy2=0

Prob0A: 3 iterations in 0.08 seconds (average 0.028000, setup 0.00)

Prob1E: 191 iterations in 2.58 seconds (average 0.013529, setup 0.00)

yes = 100358, no = 2630, maybe = 101543

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=204531, nc=163048, nnz=419698, k=4] [5.2 MB]
Creating vector for yes... [1.6 MB]
Allocating iteration vectors... [2 x 1.6 MB]
TOTAL: [9.8 MB]

Starting iterations...

Iterative method: 85 iterations in 0.46 seconds (average 0.001882, setup 0.30)

Value in the initial state: 0.03034696062893393

Time for model checking: 3.163 seconds.

Result: 0.03034696062893393 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmin=? [ F "Crash" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=1,Objy1=2,Objx2=4,Objy2=0

Prob0E: 3 iterations in 0.06 seconds (average 0.020000, setup 0.00)

Prob1A: 47 iterations in 0.55 seconds (average 0.011660, setup 0.00)

yes = 100358, no = 2630, maybe = 101543

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=204531, nc=163048, nnz=419698, k=4] [5.2 MB]
Creating vector for yes... [1.6 MB]
Allocating iteration vectors... [2 x 1.6 MB]
TOTAL: [9.8 MB]

Starting iterations...

Iterative method: 83 iterations in 0.38 seconds (average 0.001976, setup 0.22)

Value in the initial state: 0.017095002645733794

Time for model checking: 1.03 seconds.

Result: 0.017095002645733794 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmax=? [ F "AllAreaSearched" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=1,Objy1=2,Objx2=4,Objy2=0

Prob0A: 58 iterations in 0.54 seconds (average 0.009310, setup 0.00)

Prob1E: 4 iterations in 0.04 seconds (average 0.011000, setup 0.00)

yes = 1238, no = 162386, maybe = 40907

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=204531, nc=60798, nnz=153387, k=2] [2.0 MB]
Creating vector for yes... [1.6 MB]
Allocating iteration vectors... [2 x 1.6 MB]
TOTAL: [6.7 MB]

Starting iterations...

Iterative method: 84 iterations in 0.29 seconds (average 0.001476, setup 0.16)

Value in the initial state: 0.2145313321211543

Time for model checking: 0.919 seconds.

Result: 0.2145313321211543 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmin=? [ F "AllAreaSearched" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=1,Objy1=2,Objx2=4,Objy2=0

Prob0E: 62 iterations in 0.49 seconds (average 0.007871, setup 0.00)

Prob1A: 3 iterations in 0.04 seconds (average 0.013333, setup 0.00)

yes = 1238, no = 167797, maybe = 35496

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=204531, nc=52231, nnz=130294, k=2] [1.7 MB]
Creating vector for yes... [1.6 MB]
Allocating iteration vectors... [2 x 1.6 MB]
TOTAL: [6.4 MB]

Starting iterations...

Iterative method: 91 iterations in 0.25 seconds (average 0.001407, setup 0.12)

Value in the initial state: 0.0017592224914963275

Time for model checking: 0.794 seconds.

Result: 0.0017592224914963275 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmax=? [ F "MissionTimeUp"|"AllAreaSearched"|"Crash" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=1,Objy1=2,Objx2=4,Objy2=0

Prob0A: 3 iterations in 0.09 seconds (average 0.030667, setup 0.00)

Prob1E: 200 iterations in 2.37 seconds (average 0.011840, setup 0.00)

yes = 178940, no = 960, maybe = 24631

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=204531, nc=42249, nnz=117242, k=2] [1.6 MB]
Creating vector for yes... [1.6 MB]
Allocating iteration vectors... [2 x 1.6 MB]
TOTAL: [6.3 MB]

Starting iterations...

Iterative method: 77 iterations in 0.29 seconds (average 0.001299, setup 0.19)

Value in the initial state: 0.2423539936126252

Time for model checking: 2.764 seconds.

Result: 0.2423539936126252 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmin=? [ F "MissionTimeUp"|"AllAreaSearched"|"Crash" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=1,Objy1=2,Objx2=4,Objy2=0

Prob0E: 3 iterations in 0.05 seconds (average 0.017333, setup 0.00)

Prob1A: 49 iterations in 0.44 seconds (average 0.008980, setup 0.00)

yes = 173872, no = 960, maybe = 29699

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=204531, nc=51095, nnz=143067, k=2] [1.9 MB]
Creating vector for yes... [1.6 MB]
Allocating iteration vectors... [2 x 1.6 MB]
TOTAL: [6.6 MB]

Starting iterations...

Iterative method: 88 iterations in 0.28 seconds (average 0.001455, setup 0.15)

Value in the initial state: 0.01916257672349787

Time for model checking: 0.797 seconds.

Result: 0.01916257672349787 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"drop"}max=? [ F "deadlock" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=1,Objy1=2,Objx2=4,Objy2=0

Prob0E: 4 iterations in 0.06 seconds (average 0.015000, setup 0.00)

Prob1A: 1 iterations in 0.01 seconds (average 0.008000, setup 0.00)

goal = 1, inf = 0, maybe = 204530

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=204531, nc=266035, nnz=522685, k=4] [7.8 MB]
Building sparse matrix (transition rewards)... [n=204531, nc=266035, nnz=0, k=4] [1.8 MB]
Creating vector for state rewards... [1.6 MB]
Creating vector for inf... [1.6 MB]
Allocating iteration vectors... [2 x 1.6 MB]
TOTAL: [15.8 MB]

Starting iterations...

Iterative method: 85 iterations in 0.63 seconds (average 0.002965, setup 0.38)

Value in the initial state: 0.17642773047631147

Time for model checking: 0.719 seconds.

Result: 0.17642773047631147 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"drop"}min=? [ F "deadlock" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=1,Objy1=2,Objx2=4,Objy2=0

Prob0A: 4 iterations in 0.04 seconds (average 0.010000, setup 0.00)

Prob1E: 5 iterations in 0.04 seconds (average 0.008000, setup 0.00)

Warning: PRISM hasn't checked for zero-reward loops.
Your minimum rewards may be too low...

goal = 1, inf = 0, maybe = 204530

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=204531, nc=266035, nnz=522685, k=4] [7.8 MB]
Building sparse matrix (transition rewards)... [n=204531, nc=266035, nnz=0, k=4] [1.8 MB]
Creating vector for state rewards... [1.6 MB]
Creating vector for inf... [1.6 MB]
Allocating iteration vectors... [2 x 1.6 MB]
TOTAL: [15.8 MB]

Starting iterations...

Iterative method: 76 iterations in 0.52 seconds (average 0.002947, setup 0.30)

Value in the initial state: 0.16300172382288183

Time for model checking: 0.612 seconds.

Result: 0.16300172382288183 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"charge"}max=? [ F "deadlock" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=1,Objy1=2,Objx2=4,Objy2=0

Prob0E: 4 iterations in 0.02 seconds (average 0.006000, setup 0.00)

Prob1A: 1 iterations in 0.00 seconds (average 0.004000, setup 0.00)

goal = 1, inf = 0, maybe = 204530

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=204531, nc=266035, nnz=522685, k=4] [7.8 MB]
Building sparse matrix (transition rewards)... [n=204531, nc=266035, nnz=5402, k=4] [1.9 MB]
Creating vector for state rewards... [1.6 MB]
Creating vector for inf... [1.6 MB]
Allocating iteration vectors... [2 x 1.6 MB]
TOTAL: [15.9 MB]

Starting iterations...

Iterative method: 83 iterations in 0.54 seconds (average 0.002988, setup 0.29)

Value in the initial state: 0.27316385890392375

Time for model checking: 0.581 seconds.

Result: 0.27316385890392375 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"charge"}min=? [ F "deadlock" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=1,Objy1=2,Objx2=4,Objy2=0

Prob0A: 4 iterations in 0.02 seconds (average 0.006000, setup 0.00)

Prob1E: 5 iterations in 0.04 seconds (average 0.008000, setup 0.00)

Warning: PRISM hasn't checked for zero-reward loops.
Your minimum rewards may be too low...

goal = 1, inf = 0, maybe = 204530

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=204531, nc=266035, nnz=522685, k=4] [7.8 MB]
Building sparse matrix (transition rewards)... [n=204531, nc=266035, nnz=5402, k=4] [1.9 MB]
Creating vector for state rewards... [1.6 MB]
Creating vector for inf... [1.6 MB]
Allocating iteration vectors... [2 x 1.6 MB]
TOTAL: [15.9 MB]

Starting iterations...

Iterative method: 88 iterations in 0.56 seconds (average 0.003000, setup 0.30)

Value in the initial state: 0.04172855552236458

Time for model checking: 0.633 seconds.

Result: 0.04172855552236458 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"time"}max=? [ F "deadlock" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=1,Objy1=2,Objx2=4,Objy2=0

Prob0E: 4 iterations in 0.03 seconds (average 0.007000, setup 0.00)

Prob1A: 1 iterations in 0.00 seconds (average 0.004000, setup 0.00)

goal = 1, inf = 0, maybe = 204530

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=204531, nc=266035, nnz=522685, k=4] [7.8 MB]
Building sparse matrix (transition rewards)... [n=204531, nc=266035, nnz=102986, k=4] [3.0 MB]
Creating vector for state rewards... [1.6 MB]
Creating vector for inf... [1.6 MB]
Allocating iteration vectors... [2 x 1.6 MB]
TOTAL: [17.0 MB]

Starting iterations...

Iterative method: 87 iterations in 0.60 seconds (average 0.003034, setup 0.34)

Value in the initial state: 167.4364559708043

Time for model checking: 0.638 seconds.

Result: 167.4364559708043 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"time"}min=? [ F "deadlock" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=1,Objy1=2,Objx2=4,Objy2=0

Prob0A: 4 iterations in 0.02 seconds (average 0.006000, setup 0.00)

Prob1E: 5 iterations in 0.04 seconds (average 0.008800, setup 0.00)

Warning: PRISM hasn't checked for zero-reward loops.
Your minimum rewards may be too low...

goal = 1, inf = 0, maybe = 204530

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=204531, nc=266035, nnz=522685, k=4] [7.8 MB]
Building sparse matrix (transition rewards)... [n=204531, nc=266035, nnz=102986, k=4] [3.0 MB]
Creating vector for state rewards... [1.6 MB]
Creating vector for inf... [1.6 MB]
Allocating iteration vectors... [2 x 1.6 MB]
TOTAL: [17.0 MB]

Starting iterations...

Iterative method: 95 iterations in 0.62 seconds (average 0.002989, setup 0.33)

Value in the initial state: 94.73976431433636

Time for model checking: 0.698 seconds.

Result: 94.73976431433636 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"success"}max=? [ F "deadlock" ]/Pmin=? [ F "MissionSuccessful" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=1,Objy1=2,Objx2=4,Objy2=0

Prob0E: 4 iterations in 0.03 seconds (average 0.007000, setup 0.00)

Prob1A: 1 iterations in 0.00 seconds (average 0.004000, setup 0.00)

goal = 1, inf = 0, maybe = 204530

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=204531, nc=266035, nnz=522685, k=4] [7.8 MB]
Building sparse matrix (transition rewards)... [n=204531, nc=266035, nnz=1890, k=4] [1.8 MB]
Creating vector for state rewards... [1.6 MB]
Creating vector for inf... [1.6 MB]
Allocating iteration vectors... [2 x 1.6 MB]
TOTAL: [15.8 MB]

Starting iterations...

Iterative method: 92 iterations in 0.55 seconds (average 0.002870, setup 0.29)

Prob0E: 51 iterations in 0.34 seconds (average 0.006745, setup 0.00)

Prob1A: 3 iterations in 0.06 seconds (average 0.021333, setup 0.00)

yes = 959, no = 178941, maybe = 24631

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=204531, nc=42249, nnz=117242, k=2] [1.6 MB]
Creating vector for yes... [1.6 MB]
Allocating iteration vectors... [2 x 1.6 MB]
TOTAL: [6.3 MB]

Starting iterations...

Iterative method: 82 iterations in 0.23 seconds (average 0.001317, setup 0.12)

Value in the initial state: 216.6404288739724

Time for model checking: 1.257 seconds.

Result: 216.6404288739724 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"success"}min=? [ F "deadlock" ]/Pmax=? [ F "MissionSuccessful" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=1,Objy1=2,Objx2=4,Objy2=0

Prob0A: 4 iterations in 0.04 seconds (average 0.011000, setup 0.00)

Prob1E: 5 iterations in 0.04 seconds (average 0.007200, setup 0.00)

Warning: PRISM hasn't checked for zero-reward loops.
Your minimum rewards may be too low...

goal = 1, inf = 0, maybe = 204530

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=204531, nc=266035, nnz=522685, k=4] [7.8 MB]
Building sparse matrix (transition rewards)... [n=204531, nc=266035, nnz=1890, k=4] [1.8 MB]
Creating vector for state rewards... [1.6 MB]
Creating vector for inf... [1.6 MB]
Allocating iteration vectors... [2 x 1.6 MB]
TOTAL: [15.8 MB]

Starting iterations...

Iterative method: 86 iterations in 0.56 seconds (average 0.002930, setup 0.31)

Prob0A: 49 iterations in 0.27 seconds (average 0.005551, setup 0.00)

Prob1E: 4 iterations in 0.04 seconds (average 0.010000, setup 0.00)

yes = 959, no = 173873, maybe = 29699

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=204531, nc=51095, nnz=143067, k=2] [1.9 MB]
Creating vector for yes... [1.6 MB]
Allocating iteration vectors... [2 x 1.6 MB]
TOTAL: [6.6 MB]

Starting iterations...

Iterative method: 89 iterations in 0.26 seconds (average 0.001258, setup 0.14)

Value in the initial state: 79.87001823110629

Time for model checking: 1.245 seconds.

Result: 79.87001823110629 (value in the initial state)

---------------------------------------------------------------------

Note: There were 16 warnings during computation.

