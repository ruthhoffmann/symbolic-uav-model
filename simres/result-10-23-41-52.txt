PRISM
=====

Version: 4.3
Date: Wed Aug 03 16:15:53 BST 2016
Hostname: eglinton
Memory limits: cudd=1g, java(heap)=981.5m
Command line: prism uav_sim_mdp.prism -s uav_mdp.props -const 'Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=4,Objy1=1,Objx2=5,Objy2=2,'

Parsing model file "uav_sim_mdp.prism"...

Parsing properties file "uav_mdp.props"...

18 properties:
(1) Pmax=? [ F "MissionSuccessful" ]
(2) Pmin=? [ F "MissionSuccessful" ]
(3) Pmax=? [ F "MissionTimeUp" ]
(4) Pmin=? [ F "MissionTimeUp" ]
(5) Pmax=? [ F "Crash" ]
(6) Pmin=? [ F "Crash" ]
(7) Pmax=? [ F "AllAreaSearched" ]
(8) Pmin=? [ F "AllAreaSearched" ]
(9) Pmax=? [ F "MissionTimeUp"|"AllAreaSearched"|"Crash" ]
(10) Pmin=? [ F "MissionTimeUp"|"AllAreaSearched"|"Crash" ]
(11) R{"drop"}max=? [ F "deadlock" ]
(12) R{"drop"}min=? [ F "deadlock" ]
(13) R{"charge"}max=? [ F "deadlock" ]
(14) R{"charge"}min=? [ F "deadlock" ]
(15) R{"time"}max=? [ F "deadlock" ]
(16) R{"time"}min=? [ F "deadlock" ]
(17) R{"success"}max=? [ F "deadlock" ]/Pmin=? [ F "MissionSuccessful" ]
(18) R{"success"}min=? [ F "deadlock" ]/Pmax=? [ F "MissionSuccessful" ]

Type:        MDP
Modules:     Obj Move Robot T 
Variables:   NoOfObjs posx posy s1 retx rety b t c 

---------------------------------------------------------------------

Model checking: Pmax=? [ F "MissionSuccessful" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=4,Objy1=1,Objx2=5,Objy2=2

Building model...
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=4,Objy1=1,Objx2=5,Objy2=2

Warning: Guard for command 7 of module "Move" is never satisfied.

Warning: Guard for command 8 of module "Move" is never satisfied.

Warning: Guard for command 9 of module "Move" is never satisfied.

Warning: Guard for command 7 of module "Robot" is never satisfied.

Warning: Update 2 of command 3 of module "Robot" doesn't do anything ("(s1'=13)", line 210, column 44)

Warning: Update 2 of command 27 of module "T" doesn't do anything ("(c'=1)", line 323, column 108)

Warning: Guard for command 28 of module "T" is never satisfied.

Warning: Update 2 of command 9 of module "T" doesn't do anything ("(c'=1)", line 300, column 112)

Warning: Guard for command 10 of module "T" is never satisfied.

Warning: Update 2 of command 51 of module "T" doesn't do anything ("(c'=1)", line 354, column 113)

Warning: Guard for command 52 of module "T" is never satisfied.

Computing reachable states...

Reachability (BFS): 88 iterations in 1.16 seconds (average 0.013227, setup 0.00)

Time for model construction: 2.763 seconds.

Warning: Deadlocks detected and fixed in 1 states

Type:        MDP
States:      269313 (1 initial)
Transitions: 724107
Choices:     358879

Transition matrix: 220095 nodes (91 terminal), 724107 minterms, vars: 34r/34c/18nd

Prob0A: 50 iterations in 0.35 seconds (average 0.007040, setup 0.00)

Prob1E: 4 iterations in 0.06 seconds (average 0.015000, setup 0.00)

yes = 1152, no = 224692, maybe = 43469

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=269313, nc=76026, nnz=216215, k=2] [2.8 MB]
Creating vector for yes... [2.1 MB]
Allocating iteration vectors... [2 x 2.1 MB]
TOTAL: [9.0 MB]

Starting iterations...

Iterative method: 97 iterations in 0.56 seconds (average 0.001814, setup 0.38)

Value in the initial state: 0.9821331297852166

Time for model checking: 0.983 seconds.

Result: 0.9821331297852166 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmin=? [ F "MissionSuccessful" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=4,Objy1=1,Objx2=5,Objy2=2

Prob0E: 52 iterations in 0.45 seconds (average 0.008692, setup 0.00)

Prob1A: 3 iterations in 0.05 seconds (average 0.017333, setup 0.00)

yes = 1152, no = 230675, maybe = 37486

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=269313, nc=65386, nnz=185819, k=2] [2.4 MB]
Creating vector for yes... [2.1 MB]
Allocating iteration vectors... [2 x 2.1 MB]
TOTAL: [8.6 MB]

Starting iterations...

Iterative method: 89 iterations in 0.36 seconds (average 0.001753, setup 0.20)

Value in the initial state: 0.817541727296494

Time for model checking: 0.891 seconds.

Result: 0.817541727296494 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmax=? [ F "MissionTimeUp" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=4,Objy1=1,Objx2=5,Objy2=2

Prob0A: 72 iterations in 0.62 seconds (average 0.008556, setup 0.00)

Prob1E: 4 iterations in 0.04 seconds (average 0.011000, setup 0.00)

yes = 420, no = 160287, maybe = 108606

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=269313, nc=185636, nnz=509588, k=4] [6.3 MB]
Creating vector for yes... [2.1 MB]
Allocating iteration vectors... [2 x 2.1 MB]
TOTAL: [12.4 MB]

Starting iterations...

Iterative method: 96 iterations in 0.52 seconds (average 0.002333, setup 0.30)

Value in the initial state: 2.9579650387454666E-4

Time for model checking: 1.222 seconds.

Result: 2.9579650387454666E-4 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmin=? [ F "MissionTimeUp" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=4,Objy1=1,Objx2=5,Objy2=2

Prob0E: 86 iterations in 0.76 seconds (average 0.008884, setup 0.00)

Prob1A: 3 iterations in 0.05 seconds (average 0.016000, setup 0.00)

yes = 420, no = 160732, maybe = 108161

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=269313, nc=184843, nnz=507931, k=4] [6.2 MB]
Creating vector for yes... [2.1 MB]
Allocating iteration vectors... [2 x 2.1 MB]
TOTAL: [12.4 MB]

Starting iterations...

Iterative method: 113 iterations in 0.57 seconds (average 0.002336, setup 0.30)

Value in the initial state: 3.669570243401279E-7

Time for model checking: 1.396 seconds.

Result: 3.669570243401279E-7 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmax=? [ F "Crash" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=4,Objy1=1,Objx2=5,Objy2=2

Prob0A: 3 iterations in 0.08 seconds (average 0.025333, setup 0.00)

Prob1E: 195 iterations in 2.74 seconds (average 0.014051, setup 0.00)

yes = 131829, no = 3181, maybe = 134303

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=269313, nc=223869, nnz=589097, k=4] [7.2 MB]
Creating vector for yes... [2.1 MB]
Allocating iteration vectors... [2 x 2.1 MB]
TOTAL: [13.4 MB]

Starting iterations...

Iterative method: 92 iterations in 0.58 seconds (average 0.002522, setup 0.35)

Value in the initial state: 0.030433077239267827

Time for model checking: 3.42 seconds.

Result: 0.030433077239267827 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmin=? [ F "Crash" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=4,Objy1=1,Objx2=5,Objy2=2

Prob0E: 3 iterations in 0.06 seconds (average 0.020000, setup 0.00)

Prob1A: 49 iterations in 0.54 seconds (average 0.011020, setup 0.00)

yes = 131829, no = 3181, maybe = 134303

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=269313, nc=223869, nnz=589097, k=4] [7.2 MB]
Creating vector for yes... [2.1 MB]
Allocating iteration vectors... [2 x 2.1 MB]
TOTAL: [13.4 MB]

Starting iterations...

Iterative method: 87 iterations in 0.53 seconds (average 0.002575, setup 0.31)

Value in the initial state: 0.01580172842298807

Time for model checking: 1.15 seconds.

Result: 0.01580172842298807 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmax=? [ F "AllAreaSearched" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=4,Objy1=1,Objx2=5,Objy2=2

Prob0A: 55 iterations in 0.36 seconds (average 0.006545, setup 0.00)

Prob1E: 4 iterations in 0.04 seconds (average 0.011000, setup 0.00)

yes = 1608, no = 204133, maybe = 63572

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=269313, nc=102110, nnz=276764, k=2] [3.5 MB]
Creating vector for yes... [2.1 MB]
Allocating iteration vectors... [2 x 2.1 MB]
TOTAL: [9.7 MB]

Starting iterations...

Iterative method: 89 iterations in 0.41 seconds (average 0.002022, setup 0.23)

Value in the initial state: 0.15686245098424811

Time for model checking: 0.827 seconds.

Result: 0.15686245098424811 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmin=? [ F "AllAreaSearched" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=4,Objy1=1,Objx2=5,Objy2=2

Prob0E: 63 iterations in 0.50 seconds (average 0.007937, setup 0.00)

Prob1A: 3 iterations in 0.04 seconds (average 0.013333, setup 0.00)

yes = 1608, no = 213053, maybe = 54652

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=269313, nc=86815, nnz=233587, k=2] [3.0 MB]
Creating vector for yes... [2.1 MB]
Allocating iteration vectors... [2 x 2.1 MB]
TOTAL: [9.2 MB]

Starting iterations...

Iterative method: 95 iterations in 0.34 seconds (average 0.001937, setup 0.16)

Value in the initial state: 0.0017576453121234386

Time for model checking: 0.892 seconds.

Result: 0.0017576453121234386 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmax=? [ F "MissionTimeUp"|"AllAreaSearched"|"Crash" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=4,Objy1=1,Objx2=5,Objy2=2

Prob0A: 3 iterations in 0.11 seconds (average 0.036000, setup 0.00)

Prob1E: 200 iterations in 2.33 seconds (average 0.011640, setup 0.00)

yes = 230674, no = 1153, maybe = 37486

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=269313, nc=65386, nnz=185819, k=2] [2.4 MB]
Creating vector for yes... [2.1 MB]
Allocating iteration vectors... [2 x 2.1 MB]
TOTAL: [8.6 MB]

Starting iterations...

Iterative method: 84 iterations in 0.34 seconds (average 0.001714, setup 0.20)

Value in the initial state: 0.18245780234826717

Time for model checking: 2.781 seconds.

Result: 0.18245780234826717 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmin=? [ F "MissionTimeUp"|"AllAreaSearched"|"Crash" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=4,Objy1=1,Objx2=5,Objy2=2

Prob0E: 3 iterations in 0.05 seconds (average 0.017333, setup 0.00)

Prob1A: 50 iterations in 0.44 seconds (average 0.008800, setup 0.00)

yes = 224691, no = 1153, maybe = 43469

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=269313, nc=76026, nnz=216215, k=2] [2.8 MB]
Creating vector for yes... [2.1 MB]
Allocating iteration vectors... [2 x 2.1 MB]
TOTAL: [9.0 MB]

Starting iterations...

Iterative method: 94 iterations in 0.35 seconds (average 0.001830, setup 0.18)

Value in the initial state: 0.01786568658232731

Time for model checking: 0.874 seconds.

Result: 0.01786568658232731 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"drop"}max=? [ F "deadlock" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=4,Objy1=1,Objx2=5,Objy2=2

Prob0E: 4 iterations in 0.06 seconds (average 0.015000, setup 0.00)

Prob1A: 1 iterations in 0.01 seconds (average 0.008000, setup 0.00)

goal = 1, inf = 0, maybe = 269312

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=269313, nc=358878, nnz=724106, k=4] [10.7 MB]
Building sparse matrix (transition rewards)... [n=269313, nc=358878, nnz=0, k=4] [2.4 MB]
Creating vector for state rewards... [2.1 MB]
Creating vector for inf... [2.1 MB]
Allocating iteration vectors... [2 x 2.1 MB]
TOTAL: [21.3 MB]

Starting iterations...

Iterative method: 92 iterations in 0.81 seconds (average 0.003826, setup 0.46)

Value in the initial state: 0.2179857895193073

Time for model checking: 0.896 seconds.

Result: 0.2179857895193073 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"drop"}min=? [ F "deadlock" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=4,Objy1=1,Objx2=5,Objy2=2

Prob0A: 4 iterations in 0.05 seconds (average 0.012000, setup 0.00)

Prob1E: 5 iterations in 0.05 seconds (average 0.010400, setup 0.00)

Warning: PRISM hasn't checked for zero-reward loops.
Your minimum rewards may be too low...

goal = 1, inf = 0, maybe = 269312

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=269313, nc=358878, nnz=724106, k=4] [10.7 MB]
Building sparse matrix (transition rewards)... [n=269313, nc=358878, nnz=0, k=4] [2.4 MB]
Creating vector for state rewards... [2.1 MB]
Creating vector for inf... [2.1 MB]
Allocating iteration vectors... [2 x 2.1 MB]
TOTAL: [21.3 MB]

Starting iterations...

Iterative method: 83 iterations in 0.68 seconds (average 0.003904, setup 0.35)

Value in the initial state: 0.18343361107504388

Time for model checking: 0.796 seconds.

Result: 0.18343361107504388 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"charge"}max=? [ F "deadlock" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=4,Objy1=1,Objx2=5,Objy2=2

Prob0E: 4 iterations in 0.03 seconds (average 0.007000, setup 0.00)

Prob1A: 1 iterations in 0.01 seconds (average 0.008000, setup 0.00)

goal = 1, inf = 0, maybe = 269312

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=269313, nc=358878, nnz=724106, k=4] [10.7 MB]
Building sparse matrix (transition rewards)... [n=269313, nc=358878, nnz=8252, k=4] [2.5 MB]
Creating vector for state rewards... [2.1 MB]
Creating vector for inf... [2.1 MB]
Allocating iteration vectors... [2 x 2.1 MB]
TOTAL: [21.4 MB]

Starting iterations...

Iterative method: 89 iterations in 0.71 seconds (average 0.003910, setup 0.36)

Value in the initial state: 0.29259069969919926

Time for model checking: 0.761 seconds.

Result: 0.29259069969919926 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"charge"}min=? [ F "deadlock" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=4,Objy1=1,Objx2=5,Objy2=2

Prob0A: 4 iterations in 0.02 seconds (average 0.006000, setup 0.00)

Prob1E: 5 iterations in 0.05 seconds (average 0.009600, setup 0.00)

Warning: PRISM hasn't checked for zero-reward loops.
Your minimum rewards may be too low...

goal = 1, inf = 0, maybe = 269312

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=269313, nc=358878, nnz=724106, k=4] [10.7 MB]
Building sparse matrix (transition rewards)... [n=269313, nc=358878, nnz=8252, k=4] [2.5 MB]
Creating vector for state rewards... [2.1 MB]
Creating vector for inf... [2.1 MB]
Allocating iteration vectors... [2 x 2.1 MB]
TOTAL: [21.4 MB]

Starting iterations...

Iterative method: 99 iterations in 0.76 seconds (average 0.003960, setup 0.37)

Value in the initial state: 0.03294262402672327

Time for model checking: 0.848 seconds.

Result: 0.03294262402672327 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"time"}max=? [ F "deadlock" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=4,Objy1=1,Objx2=5,Objy2=2

Prob0E: 4 iterations in 0.02 seconds (average 0.006000, setup 0.00)

Prob1A: 1 iterations in 0.01 seconds (average 0.008000, setup 0.00)

goal = 1, inf = 0, maybe = 269312

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=269313, nc=358878, nnz=724106, k=4] [10.7 MB]
Building sparse matrix (transition rewards)... [n=269313, nc=358878, nnz=135008, k=4] [3.9 MB]
Creating vector for state rewards... [2.1 MB]
Creating vector for inf... [2.1 MB]
Allocating iteration vectors... [2 x 2.1 MB]
TOTAL: [22.8 MB]

Starting iterations...

Iterative method: 95 iterations in 0.78 seconds (average 0.003958, setup 0.40)

Value in the initial state: 167.94027038665774

Time for model checking: 0.837 seconds.

Result: 167.94027038665774 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"time"}min=? [ F "deadlock" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=4,Objy1=1,Objx2=5,Objy2=2

Prob0A: 4 iterations in 0.02 seconds (average 0.006000, setup 0.00)

Prob1E: 5 iterations in 0.05 seconds (average 0.009600, setup 0.00)

Warning: PRISM hasn't checked for zero-reward loops.
Your minimum rewards may be too low...

goal = 1, inf = 0, maybe = 269312

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=269313, nc=358878, nnz=724106, k=4] [10.7 MB]
Building sparse matrix (transition rewards)... [n=269313, nc=358878, nnz=135008, k=4] [3.9 MB]
Creating vector for state rewards... [2.1 MB]
Creating vector for inf... [2.1 MB]
Allocating iteration vectors... [2 x 2.1 MB]
TOTAL: [22.8 MB]

Starting iterations...

Iterative method: 101 iterations in 0.81 seconds (average 0.004040, setup 0.40)

Value in the initial state: 87.6381398883302

Time for model checking: 0.904 seconds.

Result: 87.6381398883302 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"success"}max=? [ F "deadlock" ]/Pmin=? [ F "MissionSuccessful" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=4,Objy1=1,Objx2=5,Objy2=2

Prob0E: 4 iterations in 0.02 seconds (average 0.006000, setup 0.00)

Prob1A: 1 iterations in 0.01 seconds (average 0.008000, setup 0.00)

goal = 1, inf = 0, maybe = 269312

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=269313, nc=358878, nnz=724106, k=4] [10.7 MB]
Building sparse matrix (transition rewards)... [n=269313, nc=358878, nnz=2270, k=4] [2.4 MB]
Creating vector for state rewards... [2.1 MB]
Creating vector for inf... [2.1 MB]
Allocating iteration vectors... [2 x 2.1 MB]
TOTAL: [21.3 MB]

Starting iterations...

Iterative method: 96 iterations in 0.73 seconds (average 0.003833, setup 0.36)

Prob0E: 52 iterations in 0.38 seconds (average 0.007308, setup 0.00)

Prob1A: 3 iterations in 0.05 seconds (average 0.017333, setup 0.00)

yes = 1152, no = 230675, maybe = 37486

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=269313, nc=65386, nnz=185819, k=2] [2.4 MB]
Creating vector for yes... [2.1 MB]
Allocating iteration vectors... [2 x 2.1 MB]
TOTAL: [8.6 MB]

Starting iterations...

Iterative method: 89 iterations in 0.34 seconds (average 0.001843, setup 0.17)

Value in the initial state: 201.42979212829795

Time for model checking: 1.613 seconds.

Result: 201.42979212829795 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"success"}min=? [ F "deadlock" ]/Pmax=? [ F "MissionSuccessful" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=4,Objy1=1,Objx2=5,Objy2=2

Prob0A: 4 iterations in 0.08 seconds (average 0.021000, setup 0.00)

Prob1E: 5 iterations in 0.07 seconds (average 0.013600, setup 0.00)

Warning: PRISM hasn't checked for zero-reward loops.
Your minimum rewards may be too low...

goal = 1, inf = 0, maybe = 269312

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=269313, nc=358878, nnz=724106, k=4] [10.7 MB]
Building sparse matrix (transition rewards)... [n=269313, nc=358878, nnz=2270, k=4] [2.4 MB]
Creating vector for state rewards... [2.1 MB]
Creating vector for inf... [2.1 MB]
Allocating iteration vectors... [2 x 2.1 MB]
TOTAL: [21.3 MB]

Starting iterations...

Iterative method: 96 iterations in 0.95 seconds (average 0.004500, setup 0.52)

Prob0A: 50 iterations in 0.35 seconds (average 0.006960, setup 0.00)

Prob1E: 4 iterations in 0.04 seconds (average 0.009000, setup 0.00)

yes = 1152, no = 224692, maybe = 43469

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=269313, nc=76026, nnz=216215, k=2] [2.8 MB]
Creating vector for yes... [2.1 MB]
Allocating iteration vectors... [2 x 2.1 MB]
TOTAL: [9.0 MB]

Starting iterations...

Iterative method: 97 iterations in 0.35 seconds (average 0.001856, setup 0.17)

Value in the initial state: 76.54268120118859

Time for model checking: 1.891 seconds.

Result: 76.54268120118859 (value in the initial state)

---------------------------------------------------------------------

Note: There were 16 warnings during computation.

