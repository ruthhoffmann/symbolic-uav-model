PRISM
=====

Version: 4.3
Date: Wed Aug 03 15:59:24 BST 2016
Hostname: eglinton
Memory limits: cudd=1g, java(heap)=981.5m
Command line: prism uav_sim_mdp.prism -s uav_mdp.props -const 'Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=3,Objy1=1,Objx2=4,Objy2=3,'

Parsing model file "uav_sim_mdp.prism"...

Parsing properties file "uav_mdp.props"...

18 properties:
(1) Pmax=? [ F "MissionSuccessful" ]
(2) Pmin=? [ F "MissionSuccessful" ]
(3) Pmax=? [ F "MissionTimeUp" ]
(4) Pmin=? [ F "MissionTimeUp" ]
(5) Pmax=? [ F "Crash" ]
(6) Pmin=? [ F "Crash" ]
(7) Pmax=? [ F "AllAreaSearched" ]
(8) Pmin=? [ F "AllAreaSearched" ]
(9) Pmax=? [ F "MissionTimeUp"|"AllAreaSearched"|"Crash" ]
(10) Pmin=? [ F "MissionTimeUp"|"AllAreaSearched"|"Crash" ]
(11) R{"drop"}max=? [ F "deadlock" ]
(12) R{"drop"}min=? [ F "deadlock" ]
(13) R{"charge"}max=? [ F "deadlock" ]
(14) R{"charge"}min=? [ F "deadlock" ]
(15) R{"time"}max=? [ F "deadlock" ]
(16) R{"time"}min=? [ F "deadlock" ]
(17) R{"success"}max=? [ F "deadlock" ]/Pmin=? [ F "MissionSuccessful" ]
(18) R{"success"}min=? [ F "deadlock" ]/Pmax=? [ F "MissionSuccessful" ]

Type:        MDP
Modules:     Obj Move Robot T 
Variables:   NoOfObjs posx posy s1 retx rety b t c 

---------------------------------------------------------------------

Model checking: Pmax=? [ F "MissionSuccessful" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=3,Objy1=1,Objx2=4,Objy2=3

Building model...
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=3,Objy1=1,Objx2=4,Objy2=3

Warning: Guard for command 7 of module "Move" is never satisfied.

Warning: Guard for command 8 of module "Move" is never satisfied.

Warning: Guard for command 9 of module "Move" is never satisfied.

Warning: Guard for command 7 of module "Robot" is never satisfied.

Warning: Update 2 of command 3 of module "Robot" doesn't do anything ("(s1'=13)", line 210, column 44)

Warning: Update 2 of command 27 of module "T" doesn't do anything ("(c'=1)", line 323, column 108)

Warning: Guard for command 28 of module "T" is never satisfied.

Warning: Update 2 of command 9 of module "T" doesn't do anything ("(c'=1)", line 300, column 112)

Warning: Guard for command 10 of module "T" is never satisfied.

Warning: Update 2 of command 51 of module "T" doesn't do anything ("(c'=1)", line 354, column 113)

Warning: Guard for command 52 of module "T" is never satisfied.

Computing reachable states...

Reachability (BFS): 88 iterations in 1.33 seconds (average 0.015091, setup 0.00)

Time for model construction: 3.381 seconds.

Warning: Deadlocks detected and fixed in 1 states

Type:        MDP
States:      235756 (1 initial)
Transitions: 622057
Choices:     312668

Transition matrix: 215474 nodes (93 terminal), 622057 minterms, vars: 34r/34c/18nd

Prob0A: 52 iterations in 0.49 seconds (average 0.009462, setup 0.00)

Prob1E: 4 iterations in 0.08 seconds (average 0.019000, setup 0.00)

yes = 1143, no = 195808, maybe = 38805

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=235756, nc=66603, nnz=185192, k=2] [2.4 MB]
Creating vector for yes... [1.8 MB]
Allocating iteration vectors... [2 x 1.8 MB]
TOTAL: [7.8 MB]

Starting iterations...

Iterative method: 97 iterations in 0.41 seconds (average 0.001649, setup 0.25)

Value in the initial state: 0.9815022856798367

Time for model checking: 1.016 seconds.

Result: 0.9815022856798367 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmin=? [ F "MissionSuccessful" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=3,Objy1=1,Objx2=4,Objy2=3

Prob0E: 53 iterations in 0.48 seconds (average 0.009057, setup 0.00)

Prob1A: 3 iterations in 0.05 seconds (average 0.016000, setup 0.00)

yes = 1143, no = 201216, maybe = 33397

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=235756, nc=57242, nnz=158650, k=2] [2.1 MB]
Creating vector for yes... [1.8 MB]
Allocating iteration vectors... [2 x 1.8 MB]
TOTAL: [7.5 MB]

Starting iterations...

Iterative method: 92 iterations in 0.31 seconds (average 0.001565, setup 0.17)

Value in the initial state: 0.8376141498055989

Time for model checking: 0.836 seconds.

Result: 0.8376141498055989 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmax=? [ F "MissionTimeUp" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=3,Objy1=1,Objx2=4,Objy2=3

Prob0A: 73 iterations in 0.70 seconds (average 0.009589, setup 0.00)

Prob1E: 4 iterations in 0.04 seconds (average 0.010000, setup 0.00)

yes = 368, no = 139307, maybe = 96081

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=235756, nc=160033, nnz=431749, k=4] [5.3 MB]
Creating vector for yes... [1.8 MB]
Allocating iteration vectors... [2 x 1.8 MB]
TOTAL: [10.7 MB]

Starting iterations...

Iterative method: 99 iterations in 0.51 seconds (average 0.002101, setup 0.30)

Value in the initial state: 1.6224635193245676E-4

Time for model checking: 1.263 seconds.

Result: 1.6224635193245676E-4 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmin=? [ F "MissionTimeUp" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=3,Objy1=1,Objx2=4,Objy2=3

Prob0E: 87 iterations in 0.97 seconds (average 0.011126, setup 0.00)

Prob1A: 3 iterations in 0.05 seconds (average 0.017333, setup 0.00)

yes = 368, no = 139597, maybe = 95791

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=235756, nc=159524, nnz=430677, k=4] [5.3 MB]
Creating vector for yes... [1.8 MB]
Allocating iteration vectors... [2 x 1.8 MB]
TOTAL: [10.7 MB]

Starting iterations...

Iterative method: 115 iterations in 0.53 seconds (average 0.002087, setup 0.29)

Value in the initial state: 1.75087335674245E-7

Time for model checking: 1.591 seconds.

Result: 1.75087335674245E-7 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmax=? [ F "Crash" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=3,Objy1=1,Objx2=4,Objy2=3

Prob0A: 3 iterations in 0.11 seconds (average 0.037333, setup 0.00)

Prob1E: 196 iterations in 2.91 seconds (average 0.014837, setup 0.00)

yes = 115597, no = 3158, maybe = 117001

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=235756, nc=193913, nnz=503302, k=4] [6.2 MB]
Creating vector for yes... [1.8 MB]
Allocating iteration vectors... [2 x 1.8 MB]
TOTAL: [11.6 MB]

Starting iterations...

Iterative method: 94 iterations in 0.55 seconds (average 0.002128, setup 0.35)

Value in the initial state: 0.02970512710109376

Time for model checking: 3.589 seconds.

Result: 0.02970512710109376 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmin=? [ F "Crash" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=3,Objy1=1,Objx2=4,Objy2=3

Prob0E: 4 iterations in 0.08 seconds (average 0.021000, setup 0.00)

Prob1A: 50 iterations in 0.62 seconds (average 0.012480, setup 0.00)

yes = 115597, no = 3158, maybe = 117001

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=235756, nc=193913, nnz=503302, k=4] [6.2 MB]
Creating vector for yes... [1.8 MB]
Allocating iteration vectors... [2 x 1.8 MB]
TOTAL: [11.6 MB]

Starting iterations...

Iterative method: 88 iterations in 0.47 seconds (average 0.002273, setup 0.27)

Value in the initial state: 0.01640379412021263

Time for model checking: 1.199 seconds.

Result: 0.01640379412021263 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmax=? [ F "AllAreaSearched" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=3,Objy1=1,Objx2=4,Objy2=3

Prob0A: 53 iterations in 0.48 seconds (average 0.009057, setup 0.00)

Prob1E: 4 iterations in 0.06 seconds (average 0.015000, setup 0.00)

yes = 1646, no = 174094, maybe = 60016

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=235756, nc=97672, nnz=262646, k=2] [3.3 MB]
Creating vector for yes... [1.8 MB]
Allocating iteration vectors... [2 x 1.8 MB]
TOTAL: [8.7 MB]

Starting iterations...

Iterative method: 87 iterations in 0.38 seconds (average 0.001839, setup 0.22)

Value in the initial state: 0.1375123290412305

Time for model checking: 0.95 seconds.

Result: 0.1375123290412305 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmin=? [ F "AllAreaSearched" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=3,Objy1=1,Objx2=4,Objy2=3

Prob0E: 62 iterations in 0.60 seconds (average 0.009677, setup 0.00)

Prob1A: 3 iterations in 0.06 seconds (average 0.020000, setup 0.00)

yes = 1646, no = 181602, maybe = 52508

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=235756, nc=85056, nnz=226928, k=2] [2.9 MB]
Creating vector for yes... [1.8 MB]
Allocating iteration vectors... [2 x 1.8 MB]
TOTAL: [8.3 MB]

Starting iterations...

Iterative method: 96 iterations in 0.35 seconds (average 0.001708, setup 0.18)

Value in the initial state: 0.0018041219155834393

Time for model checking: 1.032 seconds.

Result: 0.0018041219155834393 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmax=? [ F "MissionTimeUp"|"AllAreaSearched"|"Crash" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=3,Objy1=1,Objx2=4,Objy2=3

Prob0A: 3 iterations in 0.08 seconds (average 0.028000, setup 0.00)

Prob1E: 204 iterations in 2.66 seconds (average 0.013020, setup 0.00)

yes = 201215, no = 1144, maybe = 33397

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=235756, nc=57242, nnz=158650, k=2] [2.1 MB]
Creating vector for yes... [1.8 MB]
Allocating iteration vectors... [2 x 1.8 MB]
TOTAL: [7.5 MB]

Starting iterations...

Iterative method: 86 iterations in 0.33 seconds (average 0.001535, setup 0.20)

Value in the initial state: 0.16238566085497197

Time for model checking: 3.104 seconds.

Result: 0.16238566085497197 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmin=? [ F "MissionTimeUp"|"AllAreaSearched"|"Crash" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=3,Objy1=1,Objx2=4,Objy2=3

Prob0E: 4 iterations in 0.06 seconds (average 0.015000, setup 0.00)

Prob1A: 52 iterations in 0.52 seconds (average 0.009923, setup 0.00)

yes = 195807, no = 1144, maybe = 38805

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=235756, nc=66603, nnz=185192, k=2] [2.4 MB]
Creating vector for yes... [1.8 MB]
Allocating iteration vectors... [2 x 1.8 MB]
TOTAL: [7.8 MB]

Starting iterations...

Iterative method: 95 iterations in 0.33 seconds (average 0.001600, setup 0.18)

Value in the initial state: 0.0184963179599636

Time for model checking: 0.928 seconds.

Result: 0.0184963179599636 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"drop"}max=? [ F "deadlock" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=3,Objy1=1,Objx2=4,Objy2=3

Prob0E: 5 iterations in 0.07 seconds (average 0.013600, setup 0.00)

Prob1A: 1 iterations in 0.01 seconds (average 0.008000, setup 0.00)

goal = 1, inf = 0, maybe = 235755

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=235756, nc=312667, nnz=622056, k=4] [9.2 MB]
Building sparse matrix (transition rewards)... [n=235756, nc=312667, nnz=0, k=4] [2.1 MB]
Creating vector for state rewards... [1.8 MB]
Creating vector for inf... [1.8 MB]
Allocating iteration vectors... [2 x 1.8 MB]
TOTAL: [18.5 MB]

Starting iterations...

Iterative method: 92 iterations in 0.74 seconds (average 0.003304, setup 0.44)

Value in the initial state: 0.18261274182790757

Time for model checking: 0.839 seconds.

Result: 0.18261274182790757 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"drop"}min=? [ F "deadlock" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=3,Objy1=1,Objx2=4,Objy2=3

Prob0A: 4 iterations in 0.05 seconds (average 0.013000, setup 0.00)

Prob1E: 5 iterations in 0.05 seconds (average 0.010400, setup 0.00)

Warning: PRISM hasn't checked for zero-reward loops.
Your minimum rewards may be too low...

goal = 1, inf = 0, maybe = 235755

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=235756, nc=312667, nnz=622056, k=4] [9.2 MB]
Building sparse matrix (transition rewards)... [n=235756, nc=312667, nnz=0, k=4] [2.1 MB]
Creating vector for state rewards... [1.8 MB]
Creating vector for inf... [1.8 MB]
Allocating iteration vectors... [2 x 1.8 MB]
TOTAL: [18.5 MB]

Starting iterations...

Iterative method: 85 iterations in 0.60 seconds (average 0.003341, setup 0.32)

Value in the initial state: 0.15929242930197757

Time for model checking: 0.727 seconds.

Result: 0.15929242930197757 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"charge"}max=? [ F "deadlock" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=3,Objy1=1,Objx2=4,Objy2=3

Prob0E: 5 iterations in 0.04 seconds (average 0.007200, setup 0.00)

Prob1A: 1 iterations in 0.01 seconds (average 0.008000, setup 0.00)

goal = 1, inf = 0, maybe = 235755

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=235756, nc=312667, nnz=622056, k=4] [9.2 MB]
Building sparse matrix (transition rewards)... [n=235756, nc=312667, nnz=6908, k=4] [2.2 MB]
Creating vector for state rewards... [1.8 MB]
Creating vector for inf... [1.8 MB]
Allocating iteration vectors... [2 x 1.8 MB]
TOTAL: [18.6 MB]

Starting iterations...

Iterative method: 91 iterations in 0.63 seconds (average 0.003341, setup 0.32)

Value in the initial state: 0.2733664060583817

Time for model checking: 0.696 seconds.

Result: 0.2733664060583817 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"charge"}min=? [ F "deadlock" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=3,Objy1=1,Objx2=4,Objy2=3

Prob0A: 4 iterations in 0.03 seconds (average 0.007000, setup 0.00)

Prob1E: 5 iterations in 0.05 seconds (average 0.009600, setup 0.00)

Warning: PRISM hasn't checked for zero-reward loops.
Your minimum rewards may be too low...

goal = 1, inf = 0, maybe = 235755

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=235756, nc=312667, nnz=622056, k=4] [9.2 MB]
Building sparse matrix (transition rewards)... [n=235756, nc=312667, nnz=6908, k=4] [2.2 MB]
Creating vector for state rewards... [1.8 MB]
Creating vector for inf... [1.8 MB]
Allocating iteration vectors... [2 x 1.8 MB]
TOTAL: [18.6 MB]

Starting iterations...

Iterative method: 98 iterations in 0.67 seconds (average 0.003469, setup 0.33)

Value in the initial state: 0.03991168654754074

Time for model checking: 0.755 seconds.

Result: 0.03991168654754074 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"time"}max=? [ F "deadlock" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=3,Objy1=1,Objx2=4,Objy2=3

Prob0E: 5 iterations in 0.03 seconds (average 0.005600, setup 0.00)

Prob1A: 1 iterations in 0.01 seconds (average 0.008000, setup 0.00)

goal = 1, inf = 0, maybe = 235755

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=235756, nc=312667, nnz=622056, k=4] [9.2 MB]
Building sparse matrix (transition rewards)... [n=235756, nc=312667, nnz=118753, k=4] [3.5 MB]
Creating vector for state rewards... [1.8 MB]
Creating vector for inf... [1.8 MB]
Allocating iteration vectors... [2 x 1.8 MB]
TOTAL: [19.9 MB]

Starting iterations...

Iterative method: 96 iterations in 0.84 seconds (average 0.004292, setup 0.43)

Value in the initial state: 163.9706502456022

Time for model checking: 0.893 seconds.

Result: 163.9706502456022 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"time"}min=? [ F "deadlock" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=3,Objy1=1,Objx2=4,Objy2=3

Prob0A: 4 iterations in 0.02 seconds (average 0.006000, setup 0.00)

Prob1E: 5 iterations in 0.05 seconds (average 0.009600, setup 0.00)

Warning: PRISM hasn't checked for zero-reward loops.
Your minimum rewards may be too low...

goal = 1, inf = 0, maybe = 235755

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=235756, nc=312667, nnz=622056, k=4] [9.2 MB]
Building sparse matrix (transition rewards)... [n=235756, nc=312667, nnz=118753, k=4] [3.5 MB]
Creating vector for state rewards... [1.8 MB]
Creating vector for inf... [1.8 MB]
Allocating iteration vectors... [2 x 1.8 MB]
TOTAL: [19.9 MB]

Starting iterations...

Iterative method: 101 iterations in 0.74 seconds (average 0.003604, setup 0.37)

Value in the initial state: 90.95371544911474

Time for model checking: 0.825 seconds.

Result: 90.95371544911474 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"success"}max=? [ F "deadlock" ]/Pmin=? [ F "MissionSuccessful" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=3,Objy1=1,Objx2=4,Objy2=3

Prob0E: 5 iterations in 0.03 seconds (average 0.005600, setup 0.00)

Prob1A: 1 iterations in 0.00 seconds (average 0.004000, setup 0.00)

goal = 1, inf = 0, maybe = 235755

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=235756, nc=312667, nnz=622056, k=4] [9.2 MB]
Building sparse matrix (transition rewards)... [n=235756, nc=312667, nnz=2252, k=4] [2.1 MB]
Creating vector for state rewards... [1.8 MB]
Creating vector for inf... [1.8 MB]
Allocating iteration vectors... [2 x 1.8 MB]
TOTAL: [18.5 MB]

Starting iterations...

Iterative method: 99 iterations in 0.66 seconds (average 0.003475, setup 0.32)

Prob0E: 53 iterations in 0.43 seconds (average 0.008151, setup 0.00)

Prob1A: 3 iterations in 0.05 seconds (average 0.017333, setup 0.00)

yes = 1143, no = 201216, maybe = 33397

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=235756, nc=57242, nnz=158650, k=2] [2.1 MB]
Creating vector for yes... [1.8 MB]
Allocating iteration vectors... [2 x 1.8 MB]
TOTAL: [7.5 MB]

Starting iterations...

Iterative method: 92 iterations in 0.31 seconds (average 0.001565, setup 0.16)

Value in the initial state: 192.06934541570087

Time for model checking: 1.531 seconds.

Result: 192.06934541570087 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"success"}min=? [ F "deadlock" ]/Pmax=? [ F "MissionSuccessful" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=3,Objy1=1,Objx2=4,Objy2=3

Prob0A: 4 iterations in 0.07 seconds (average 0.018000, setup 0.00)

Prob1E: 5 iterations in 0.06 seconds (average 0.012000, setup 0.00)

Warning: PRISM hasn't checked for zero-reward loops.
Your minimum rewards may be too low...

goal = 1, inf = 0, maybe = 235755

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=235756, nc=312667, nnz=622056, k=4] [9.2 MB]
Building sparse matrix (transition rewards)... [n=235756, nc=312667, nnz=2252, k=4] [2.1 MB]
Creating vector for state rewards... [1.8 MB]
Creating vector for inf... [1.8 MB]
Allocating iteration vectors... [2 x 1.8 MB]
TOTAL: [18.5 MB]

Starting iterations...

Iterative method: 97 iterations in 0.74 seconds (average 0.003423, setup 0.40)

Prob0A: 52 iterations in 0.43 seconds (average 0.008231, setup 0.00)

Prob1E: 4 iterations in 0.04 seconds (average 0.010000, setup 0.00)

yes = 1143, no = 195808, maybe = 38805

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=235756, nc=66603, nnz=185192, k=2] [2.4 MB]
Creating vector for yes... [1.8 MB]
Allocating iteration vectors... [2 x 1.8 MB]
TOTAL: [7.8 MB]

Starting iterations...

Iterative method: 97 iterations in 0.33 seconds (average 0.001608, setup 0.17)

Value in the initial state: 78.70123866844816

Time for model checking: 1.7 seconds.

Result: 78.70123866844816 (value in the initial state)

---------------------------------------------------------------------

Note: There were 16 warnings during computation.

