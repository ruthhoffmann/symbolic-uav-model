PRISM
=====

Version: 4.3
Date: Wed Aug 03 16:09:54 BST 2016
Hostname: eglinton
Memory limits: cudd=1g, java(heap)=981.5m
Command line: prism uav_sim_mdp.prism -s uav_mdp.props -const 'Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=1,Objy1=2,Objx2=4,Objy2=3,'

Parsing model file "uav_sim_mdp.prism"...

Parsing properties file "uav_mdp.props"...

18 properties:
(1) Pmax=? [ F "MissionSuccessful" ]
(2) Pmin=? [ F "MissionSuccessful" ]
(3) Pmax=? [ F "MissionTimeUp" ]
(4) Pmin=? [ F "MissionTimeUp" ]
(5) Pmax=? [ F "Crash" ]
(6) Pmin=? [ F "Crash" ]
(7) Pmax=? [ F "AllAreaSearched" ]
(8) Pmin=? [ F "AllAreaSearched" ]
(9) Pmax=? [ F "MissionTimeUp"|"AllAreaSearched"|"Crash" ]
(10) Pmin=? [ F "MissionTimeUp"|"AllAreaSearched"|"Crash" ]
(11) R{"drop"}max=? [ F "deadlock" ]
(12) R{"drop"}min=? [ F "deadlock" ]
(13) R{"charge"}max=? [ F "deadlock" ]
(14) R{"charge"}min=? [ F "deadlock" ]
(15) R{"time"}max=? [ F "deadlock" ]
(16) R{"time"}min=? [ F "deadlock" ]
(17) R{"success"}max=? [ F "deadlock" ]/Pmin=? [ F "MissionSuccessful" ]
(18) R{"success"}min=? [ F "deadlock" ]/Pmax=? [ F "MissionSuccessful" ]

Type:        MDP
Modules:     Obj Move Robot T 
Variables:   NoOfObjs posx posy s1 retx rety b t c 

---------------------------------------------------------------------

Model checking: Pmax=? [ F "MissionSuccessful" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=1,Objy1=2,Objx2=4,Objy2=3

Building model...
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=1,Objy1=2,Objx2=4,Objy2=3

Warning: Guard for command 7 of module "Move" is never satisfied.

Warning: Guard for command 8 of module "Move" is never satisfied.

Warning: Guard for command 9 of module "Move" is never satisfied.

Warning: Guard for command 7 of module "Robot" is never satisfied.

Warning: Update 2 of command 3 of module "Robot" doesn't do anything ("(s1'=13)", line 210, column 44)

Warning: Update 2 of command 27 of module "T" doesn't do anything ("(c'=1)", line 323, column 108)

Warning: Guard for command 28 of module "T" is never satisfied.

Warning: Update 2 of command 9 of module "T" doesn't do anything ("(c'=1)", line 300, column 112)

Warning: Guard for command 10 of module "T" is never satisfied.

Warning: Update 2 of command 51 of module "T" doesn't do anything ("(c'=1)", line 354, column 113)

Warning: Guard for command 52 of module "T" is never satisfied.

Computing reachable states...

Reachability (BFS): 87 iterations in 1.00 seconds (average 0.011494, setup 0.00)

Time for model construction: 3.503 seconds.

Warning: Deadlocks detected and fixed in 1 states

Type:        MDP
States:      197878 (1 initial)
Transitions: 528015
Choices:     265008

Transition matrix: 202175 nodes (89 terminal), 528015 minterms, vars: 34r/34c/18nd

Prob0A: 51 iterations in 0.40 seconds (average 0.007922, setup 0.00)

Prob1E: 4 iterations in 0.06 seconds (average 0.015000, setup 0.00)

yes = 1098, no = 161969, maybe = 34811

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=197878, nc=60786, nnz=168342, k=2] [2.2 MB]
Creating vector for yes... [1.5 MB]
Allocating iteration vectors... [2 x 1.5 MB]
TOTAL: [6.7 MB]

Starting iterations...

Iterative method: 95 iterations in 0.37 seconds (average 0.001347, setup 0.24)

Value in the initial state: 0.9825417719154067

Time for model checking: 0.882 seconds.

Result: 0.9825417719154067 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmin=? [ F "MissionSuccessful" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=1,Objy1=2,Objx2=4,Objy2=3

Prob0E: 51 iterations in 0.42 seconds (average 0.008157, setup 0.00)

Prob1A: 3 iterations in 0.07 seconds (average 0.022667, setup 0.00)

yes = 1098, no = 166053, maybe = 30727

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=197878, nc=53614, nnz=148204, k=2] [1.9 MB]
Creating vector for yes... [1.5 MB]
Allocating iteration vectors... [2 x 1.5 MB]
TOTAL: [6.5 MB]

Starting iterations...

Iterative method: 90 iterations in 0.33 seconds (average 0.001333, setup 0.21)

Value in the initial state: 0.8535867217094179

Time for model checking: 1.022 seconds.

Result: 0.8535867217094179 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmax=? [ F "MissionTimeUp" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=1,Objy1=2,Objx2=4,Objy2=3

Prob0A: 76 iterations in 0.72 seconds (average 0.009474, setup 0.00)

Prob1E: 4 iterations in 0.04 seconds (average 0.011000, setup 0.00)

yes = 276, no = 119559, maybe = 78043

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=197878, nc=132815, nnz=359992, k=4] [4.4 MB]
Creating vector for yes... [1.5 MB]
Allocating iteration vectors... [2 x 1.5 MB]
TOTAL: [9.0 MB]

Starting iterations...

Iterative method: 98 iterations in 0.43 seconds (average 0.001755, setup 0.26)

Value in the initial state: 8.640802349429734E-5

Time for model checking: 1.408 seconds.

Result: 8.640802349429734E-5 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmin=? [ F "MissionTimeUp" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=1,Objy1=2,Objx2=4,Objy2=3

Prob0E: 90 iterations in 0.77 seconds (average 0.008578, setup 0.00)

Prob1A: 3 iterations in 0.05 seconds (average 0.016000, setup 0.00)

yes = 276, no = 119835, maybe = 77767

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=197878, nc=132329, nnz=358984, k=4] [4.4 MB]
Creating vector for yes... [1.5 MB]
Allocating iteration vectors... [2 x 1.5 MB]
TOTAL: [9.0 MB]

Starting iterations...

Iterative method: 113 iterations in 0.44 seconds (average 0.001770, setup 0.24)

Value in the initial state: 6.888230413685699E-8

Time for model checking: 1.355 seconds.

Result: 6.888230413685699E-8 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmax=? [ F "Crash" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=1,Objy1=2,Objx2=4,Objy2=3

Prob0A: 3 iterations in 0.08 seconds (average 0.028000, setup 0.00)

Prob1E: 196 iterations in 2.73 seconds (average 0.013939, setup 0.00)

yes = 97027, no = 2959, maybe = 97892

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=197878, nc=165022, nnz=428029, k=4] [5.2 MB]
Creating vector for yes... [1.5 MB]
Allocating iteration vectors... [2 x 1.5 MB]
TOTAL: [9.8 MB]

Starting iterations...

Iterative method: 92 iterations in 0.49 seconds (average 0.001870, setup 0.32)

Value in the initial state: 0.02860763564267076

Time for model checking: 3.77 seconds.

Result: 0.02860763564267076 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmin=? [ F "Crash" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=1,Objy1=2,Objx2=4,Objy2=3

Prob0E: 3 iterations in 0.05 seconds (average 0.017333, setup 0.00)

Prob1A: 51 iterations in 0.65 seconds (average 0.012784, setup 0.00)

yes = 97027, no = 2959, maybe = 97892

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=197878, nc=165022, nnz=428029, k=4] [5.2 MB]
Creating vector for yes... [1.5 MB]
Allocating iteration vectors... [2 x 1.5 MB]
TOTAL: [9.8 MB]

Starting iterations...

Iterative method: 86 iterations in 0.43 seconds (average 0.001907, setup 0.26)

Value in the initial state: 0.015410407258903731

Time for model checking: 1.171 seconds.

Result: 0.015410407258903731 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmax=? [ F "AllAreaSearched" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=1,Objy1=2,Objx2=4,Objy2=3

Prob0A: 53 iterations in 0.34 seconds (average 0.006340, setup 0.00)

Prob1E: 4 iterations in 0.04 seconds (average 0.010000, setup 0.00)

yes = 1584, no = 140509, maybe = 55785

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=197878, nc=92500, nnz=249798, k=2] [3.1 MB]
Creating vector for yes... [1.5 MB]
Allocating iteration vectors... [2 x 1.5 MB]
TOTAL: [7.7 MB]

Starting iterations...

Iterative method: 85 iterations in 0.33 seconds (average 0.001600, setup 0.20)

Value in the initial state: 0.12238413906318783

Time for model checking: 0.716 seconds.

Result: 0.12238413906318783 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmin=? [ F "AllAreaSearched" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=1,Objy1=2,Objx2=4,Objy2=3

Prob0E: 54 iterations in 0.32 seconds (average 0.005852, setup 0.00)

Prob1A: 3 iterations in 0.03 seconds (average 0.010667, setup 0.00)

yes = 1584, no = 146789, maybe = 49505

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=197878, nc=81544, nnz=218838, k=2] [2.8 MB]
Creating vector for yes... [1.5 MB]
Allocating iteration vectors... [2 x 1.5 MB]
TOTAL: [7.3 MB]

Starting iterations...

Iterative method: 94 iterations in 0.28 seconds (average 0.001532, setup 0.14)

Value in the initial state: 0.0018148139495512425

Time for model checking: 0.644 seconds.

Result: 0.0018148139495512425 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmax=? [ F "MissionTimeUp"|"AllAreaSearched"|"Crash" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=1,Objy1=2,Objx2=4,Objy2=3

Prob0A: 3 iterations in 0.08 seconds (average 0.028000, setup 0.00)

Prob1E: 200 iterations in 2.73 seconds (average 0.013640, setup 0.00)

yes = 166052, no = 1099, maybe = 30727

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=197878, nc=53614, nnz=148204, k=2] [1.9 MB]
Creating vector for yes... [1.5 MB]
Allocating iteration vectors... [2 x 1.5 MB]
TOTAL: [6.5 MB]

Starting iterations...

Iterative method: 84 iterations in 0.31 seconds (average 0.001333, setup 0.20)

Value in the initial state: 0.146412905975736

Time for model checking: 3.17 seconds.

Result: 0.146412905975736 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmin=? [ F "MissionTimeUp"|"AllAreaSearched"|"Crash" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=1,Objy1=2,Objx2=4,Objy2=3

Prob0E: 3 iterations in 0.06 seconds (average 0.018667, setup 0.00)

Prob1A: 51 iterations in 0.48 seconds (average 0.009490, setup 0.00)

yes = 161968, no = 1099, maybe = 34811

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=197878, nc=60786, nnz=168342, k=2] [2.2 MB]
Creating vector for yes... [1.5 MB]
Allocating iteration vectors... [2 x 1.5 MB]
TOTAL: [6.7 MB]

Starting iterations...

Iterative method: 92 iterations in 0.29 seconds (average 0.001391, setup 0.16)

Value in the initial state: 0.017456974738387403

Time for model checking: 0.847 seconds.

Result: 0.017456974738387403 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"drop"}max=? [ F "deadlock" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=1,Objy1=2,Objx2=4,Objy2=3

Prob0E: 4 iterations in 0.06 seconds (average 0.015000, setup 0.00)

Prob1A: 1 iterations in 0.01 seconds (average 0.008000, setup 0.00)

goal = 1, inf = 0, maybe = 197877

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=197878, nc=265007, nnz=528014, k=4] [7.8 MB]
Building sparse matrix (transition rewards)... [n=197878, nc=265007, nnz=0, k=4] [1.8 MB]
Creating vector for state rewards... [1.5 MB]
Creating vector for inf... [1.5 MB]
Allocating iteration vectors... [2 x 1.5 MB]
TOTAL: [15.6 MB]

Starting iterations...

Iterative method: 90 iterations in 0.65 seconds (average 0.002889, setup 0.39)

Value in the initial state: 0.1539203292716064

Time for model checking: 0.736 seconds.

Result: 0.1539203292716064 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"drop"}min=? [ F "deadlock" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=1,Objy1=2,Objx2=4,Objy2=3

Prob0A: 4 iterations in 0.04 seconds (average 0.009000, setup 0.00)

Prob1E: 5 iterations in 0.06 seconds (average 0.011200, setup 0.00)

Warning: PRISM hasn't checked for zero-reward loops.
Your minimum rewards may be too low...

goal = 1, inf = 0, maybe = 197877

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=197878, nc=265007, nnz=528014, k=4] [7.8 MB]
Building sparse matrix (transition rewards)... [n=197878, nc=265007, nnz=0, k=4] [1.8 MB]
Creating vector for state rewards... [1.5 MB]
Creating vector for inf... [1.5 MB]
Allocating iteration vectors... [2 x 1.5 MB]
TOTAL: [15.6 MB]

Starting iterations...

Iterative method: 84 iterations in 0.52 seconds (average 0.002810, setup 0.28)

Value in the initial state: 0.13239525665582552

Time for model checking: 0.625 seconds.

Result: 0.13239525665582552 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"charge"}max=? [ F "deadlock" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=1,Objy1=2,Objx2=4,Objy2=3

Prob0E: 4 iterations in 0.02 seconds (average 0.006000, setup 0.00)

Prob1A: 1 iterations in 0.01 seconds (average 0.008000, setup 0.00)

goal = 1, inf = 0, maybe = 197877

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=197878, nc=265007, nnz=528014, k=4] [7.8 MB]
Building sparse matrix (transition rewards)... [n=197878, nc=265007, nnz=5869, k=4] [1.8 MB]
Creating vector for state rewards... [1.5 MB]
Creating vector for inf... [1.5 MB]
Allocating iteration vectors... [2 x 1.5 MB]
TOTAL: [15.7 MB]

Starting iterations...

Iterative method: 89 iterations in 0.53 seconds (average 0.002876, setup 0.28)

Value in the initial state: 0.2540939851956009

Time for model checking: 0.575 seconds.

Result: 0.2540939851956009 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"charge"}min=? [ F "deadlock" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=1,Objy1=2,Objx2=4,Objy2=3

Prob0A: 4 iterations in 0.02 seconds (average 0.006000, setup 0.00)

Prob1E: 5 iterations in 0.05 seconds (average 0.009600, setup 0.00)

Warning: PRISM hasn't checked for zero-reward loops.
Your minimum rewards may be too low...

goal = 1, inf = 0, maybe = 197877

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=197878, nc=265007, nnz=528014, k=4] [7.8 MB]
Building sparse matrix (transition rewards)... [n=197878, nc=265007, nnz=5869, k=4] [1.8 MB]
Creating vector for state rewards... [1.5 MB]
Creating vector for inf... [1.5 MB]
Allocating iteration vectors... [2 x 1.5 MB]
TOTAL: [15.7 MB]

Starting iterations...

Iterative method: 97 iterations in 0.56 seconds (average 0.002928, setup 0.28)

Value in the initial state: 0.023025746830843272

Time for model checking: 0.641 seconds.

Result: 0.023025746830843272 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"time"}max=? [ F "deadlock" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=1,Objy1=2,Objx2=4,Objy2=3

Prob0E: 4 iterations in 0.02 seconds (average 0.006000, setup 0.00)

Prob1A: 1 iterations in 0.01 seconds (average 0.008000, setup 0.00)

goal = 1, inf = 0, maybe = 197877

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=197878, nc=265007, nnz=528014, k=4] [7.8 MB]
Building sparse matrix (transition rewards)... [n=197878, nc=265007, nnz=99984, k=4] [2.9 MB]
Creating vector for state rewards... [1.5 MB]
Creating vector for inf... [1.5 MB]
Allocating iteration vectors... [2 x 1.5 MB]
TOTAL: [16.8 MB]

Starting iterations...

Iterative method: 95 iterations in 0.58 seconds (average 0.002863, setup 0.31)

Value in the initial state: 157.96166975098436

Time for model checking: 0.632 seconds.

Result: 157.96166975098436 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"time"}min=? [ F "deadlock" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=1,Objy1=2,Objx2=4,Objy2=3

Prob0A: 4 iterations in 0.02 seconds (average 0.006000, setup 0.00)

Prob1E: 5 iterations in 0.05 seconds (average 0.009600, setup 0.00)

Warning: PRISM hasn't checked for zero-reward loops.
Your minimum rewards may be too low...

goal = 1, inf = 0, maybe = 197877

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=197878, nc=265007, nnz=528014, k=4] [7.8 MB]
Building sparse matrix (transition rewards)... [n=197878, nc=265007, nnz=99984, k=4] [2.9 MB]
Creating vector for state rewards... [1.5 MB]
Creating vector for inf... [1.5 MB]
Allocating iteration vectors... [2 x 1.5 MB]
TOTAL: [16.8 MB]

Starting iterations...

Iterative method: 96 iterations in 0.58 seconds (average 0.002917, setup 0.30)

Value in the initial state: 85.50319129370216

Time for model checking: 0.678 seconds.

Result: 85.50319129370216 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"success"}max=? [ F "deadlock" ]/Pmin=? [ F "MissionSuccessful" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=1,Objy1=2,Objx2=4,Objy2=3

Prob0E: 4 iterations in 0.02 seconds (average 0.006000, setup 0.00)

Prob1A: 1 iterations in 0.00 seconds (average 0.004000, setup 0.00)

goal = 1, inf = 0, maybe = 197877

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=197878, nc=265007, nnz=528014, k=4] [7.8 MB]
Building sparse matrix (transition rewards)... [n=197878, nc=265007, nnz=2164, k=4] [1.8 MB]
Creating vector for state rewards... [1.5 MB]
Creating vector for inf... [1.5 MB]
Allocating iteration vectors... [2 x 1.5 MB]
TOTAL: [15.6 MB]

Starting iterations...

Iterative method: 95 iterations in 0.55 seconds (average 0.002905, setup 0.27)

Prob0E: 51 iterations in 0.36 seconds (average 0.006980, setup 0.00)

Prob1A: 3 iterations in 0.04 seconds (average 0.014667, setup 0.00)

yes = 1098, no = 166053, maybe = 30727

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=197878, nc=53614, nnz=148204, k=2] [1.9 MB]
Creating vector for yes... [1.5 MB]
Allocating iteration vectors... [2 x 1.5 MB]
TOTAL: [6.5 MB]

Starting iterations...

Iterative method: 90 iterations in 0.27 seconds (average 0.001333, setup 0.15)

Value in the initial state: 181.63612027385292

Time for model checking: 1.293 seconds.

Result: 181.63612027385292 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"success"}min=? [ F "deadlock" ]/Pmax=? [ F "MissionSuccessful" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=1,Objy1=2,Objx2=4,Objy2=3

Prob0A: 4 iterations in 0.06 seconds (average 0.015000, setup 0.00)

Prob1E: 5 iterations in 0.05 seconds (average 0.009600, setup 0.00)

Warning: PRISM hasn't checked for zero-reward loops.
Your minimum rewards may be too low...

goal = 1, inf = 0, maybe = 197877

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=197878, nc=265007, nnz=528014, k=4] [7.8 MB]
Building sparse matrix (transition rewards)... [n=197878, nc=265007, nnz=2164, k=4] [1.8 MB]
Creating vector for state rewards... [1.5 MB]
Creating vector for inf... [1.5 MB]
Allocating iteration vectors... [2 x 1.5 MB]
TOTAL: [15.6 MB]

Starting iterations...

Iterative method: 96 iterations in 0.58 seconds (average 0.002958, setup 0.29)

Prob0A: 51 iterations in 0.32 seconds (average 0.006353, setup 0.00)

Prob1E: 4 iterations in 0.03 seconds (average 0.008000, setup 0.00)

yes = 1098, no = 161969, maybe = 34811

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=197878, nc=60786, nnz=168342, k=2] [2.2 MB]
Creating vector for yes... [1.5 MB]
Allocating iteration vectors... [2 x 1.5 MB]
TOTAL: [6.7 MB]

Starting iterations...

Iterative method: 95 iterations in 0.36 seconds (average 0.001432, setup 0.23)

Value in the initial state: 77.86142856564801

Time for model checking: 1.424 seconds.

Result: 77.86142856564801 (value in the initial state)

---------------------------------------------------------------------

Note: There were 16 warnings during computation.

