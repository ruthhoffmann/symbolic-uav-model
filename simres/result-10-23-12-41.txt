PRISM
=====

Version: 4.3
Date: Wed Aug 03 16:09:54 BST 2016
Hostname: eglinton
Memory limits: cudd=1g, java(heap)=981.5m
Command line: prism uav_sim_mdp.prism -s uav_mdp.props -const 'Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=1,Objy1=2,Objx2=4,Objy2=1,'

Parsing model file "uav_sim_mdp.prism"...

Parsing properties file "uav_mdp.props"...

18 properties:
(1) Pmax=? [ F "MissionSuccessful" ]
(2) Pmin=? [ F "MissionSuccessful" ]
(3) Pmax=? [ F "MissionTimeUp" ]
(4) Pmin=? [ F "MissionTimeUp" ]
(5) Pmax=? [ F "Crash" ]
(6) Pmin=? [ F "Crash" ]
(7) Pmax=? [ F "AllAreaSearched" ]
(8) Pmin=? [ F "AllAreaSearched" ]
(9) Pmax=? [ F "MissionTimeUp"|"AllAreaSearched"|"Crash" ]
(10) Pmin=? [ F "MissionTimeUp"|"AllAreaSearched"|"Crash" ]
(11) R{"drop"}max=? [ F "deadlock" ]
(12) R{"drop"}min=? [ F "deadlock" ]
(13) R{"charge"}max=? [ F "deadlock" ]
(14) R{"charge"}min=? [ F "deadlock" ]
(15) R{"time"}max=? [ F "deadlock" ]
(16) R{"time"}min=? [ F "deadlock" ]
(17) R{"success"}max=? [ F "deadlock" ]/Pmin=? [ F "MissionSuccessful" ]
(18) R{"success"}min=? [ F "deadlock" ]/Pmax=? [ F "MissionSuccessful" ]

Type:        MDP
Modules:     Obj Move Robot T 
Variables:   NoOfObjs posx posy s1 retx rety b t c 

---------------------------------------------------------------------

Model checking: Pmax=? [ F "MissionSuccessful" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=1,Objy1=2,Objx2=4,Objy2=1

Building model...
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=1,Objy1=2,Objx2=4,Objy2=1

Warning: Guard for command 7 of module "Move" is never satisfied.

Warning: Guard for command 8 of module "Move" is never satisfied.

Warning: Guard for command 9 of module "Move" is never satisfied.

Warning: Guard for command 7 of module "Robot" is never satisfied.

Warning: Update 2 of command 3 of module "Robot" doesn't do anything ("(s1'=13)", line 210, column 44)

Warning: Update 2 of command 27 of module "T" doesn't do anything ("(c'=1)", line 323, column 108)

Warning: Guard for command 28 of module "T" is never satisfied.

Warning: Update 2 of command 9 of module "T" doesn't do anything ("(c'=1)", line 300, column 112)

Warning: Guard for command 10 of module "T" is never satisfied.

Warning: Update 2 of command 51 of module "T" doesn't do anything ("(c'=1)", line 354, column 113)

Warning: Guard for command 52 of module "T" is never satisfied.

Computing reachable states...

Reachability (BFS): 88 iterations in 1.12 seconds (average 0.012727, setup 0.00)

Time for model construction: 3.447 seconds.

Warning: Deadlocks detected and fixed in 1 states

Type:        MDP
States:      212640 (1 initial)
Transitions: 538734
Choices:     277796

Transition matrix: 213163 nodes (89 terminal), 538734 minterms, vars: 34r/34c/18nd

Prob0A: 44 iterations in 0.33 seconds (average 0.007545, setup 0.00)

Prob1E: 4 iterations in 0.06 seconds (average 0.014000, setup 0.00)

yes = 1138, no = 177793, maybe = 33709

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=212640, nc=59499, nnz=164841, k=2] [2.1 MB]
Creating vector for yes... [1.6 MB]
Allocating iteration vectors... [2 x 1.6 MB]
TOTAL: [7.0 MB]

Starting iterations...

Iterative method: 85 iterations in 0.35 seconds (average 0.001412, setup 0.23)

Value in the initial state: 0.9843149218473474

Time for model checking: 0.881 seconds.

Result: 0.9843149218473474 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmin=? [ F "MissionSuccessful" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=1,Objy1=2,Objx2=4,Objy2=1

Prob0E: 45 iterations in 0.39 seconds (average 0.008711, setup 0.00)

Prob1A: 3 iterations in 0.06 seconds (average 0.018667, setup 0.00)

yes = 1138, no = 182723, maybe = 28779

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=212640, nc=50646, nnz=139534, k=2] [1.8 MB]
Creating vector for yes... [1.6 MB]
Allocating iteration vectors... [2 x 1.6 MB]
TOTAL: [6.7 MB]

Starting iterations...

Iterative method: 84 iterations in 0.33 seconds (average 0.001381, setup 0.21)

Value in the initial state: 0.8627634978558252

Time for model checking: 0.866 seconds.

Result: 0.8627634978558252 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmax=? [ F "MissionTimeUp" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=1,Objy1=2,Objx2=4,Objy2=1

Prob0A: 72 iterations in 0.76 seconds (average 0.010611, setup 0.00)

Prob1E: 4 iterations in 0.04 seconds (average 0.011000, setup 0.00)

yes = 364, no = 138086, maybe = 74190

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=212640, nc=126892, nnz=341339, k=4] [4.2 MB]
Creating vector for yes... [1.6 MB]
Allocating iteration vectors... [2 x 1.6 MB]
TOTAL: [9.1 MB]

Starting iterations...

Iterative method: 94 iterations in 0.43 seconds (average 0.001787, setup 0.26)

Value in the initial state: 3.2609647162532086E-5

Time for model checking: 1.375 seconds.

Result: 3.2609647162532086E-5 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmin=? [ F "MissionTimeUp" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=1,Objy1=2,Objx2=4,Objy2=1

Prob0E: 86 iterations in 0.76 seconds (average 0.008884, setup 0.00)

Prob1A: 3 iterations in 0.05 seconds (average 0.016000, setup 0.00)

yes = 364, no = 138343, maybe = 73933

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=212640, nc=126444, nnz=340370, k=4] [4.2 MB]
Creating vector for yes... [1.6 MB]
Allocating iteration vectors... [2 x 1.6 MB]
TOTAL: [9.1 MB]

Starting iterations...

Iterative method: 111 iterations in 0.45 seconds (average 0.001802, setup 0.25)

Value in the initial state: 2.0525041799832327E-8

Time for model checking: 1.445 seconds.

Result: 2.0525041799832327E-8 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmax=? [ F "Crash" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=1,Objy1=2,Objx2=4,Objy2=1

Prob0A: 3 iterations in 0.09 seconds (average 0.029333, setup 0.00)

Prob1E: 170 iterations in 2.80 seconds (average 0.016494, setup 0.00)

yes = 104425, no = 3083, maybe = 105132

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=212640, nc=170288, nnz=431226, k=4] [5.3 MB]
Creating vector for yes... [1.6 MB]
Allocating iteration vectors... [2 x 1.6 MB]
TOTAL: [10.2 MB]

Starting iterations...

Iterative method: 87 iterations in 0.50 seconds (average 0.002023, setup 0.32)

Value in the initial state: 0.02264683799313748

Time for model checking: 3.783 seconds.

Result: 0.02264683799313748 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmin=? [ F "Crash" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=1,Objy1=2,Objx2=4,Objy2=1

Prob0E: 3 iterations in 0.06 seconds (average 0.021333, setup 0.00)

Prob1A: 43 iterations in 0.51 seconds (average 0.011907, setup 0.00)

yes = 104425, no = 3083, maybe = 105132

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=212640, nc=170288, nnz=431226, k=4] [5.3 MB]
Creating vector for yes... [1.6 MB]
Allocating iteration vectors... [2 x 1.6 MB]
TOTAL: [10.2 MB]

Starting iterations...

Iterative method: 78 iterations in 0.41 seconds (average 0.002103, setup 0.25)

Value in the initial state: 0.013633627199308145

Time for model checking: 1.029 seconds.

Result: 0.013633627199308145 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmax=? [ F "AllAreaSearched" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=1,Objy1=2,Objx2=4,Objy2=1

Prob0A: 53 iterations in 0.45 seconds (average 0.008528, setup 0.00)

Prob1E: 4 iterations in 0.06 seconds (average 0.014000, setup 0.00)

yes = 1580, no = 157037, maybe = 54023

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=212640, nc=81994, nnz=208335, k=2] [2.7 MB]
Creating vector for yes... [1.6 MB]
Allocating iteration vectors... [2 x 1.6 MB]
TOTAL: [7.5 MB]

Starting iterations...

Iterative method: 85 iterations in 0.34 seconds (average 0.001600, setup 0.20)

Value in the initial state: 0.11563923356854383

Time for model checking: 0.871 seconds.

Result: 0.11563923356854383 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmin=? [ F "AllAreaSearched" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=1,Objy1=2,Objx2=4,Objy2=1

Prob0E: 61 iterations in 0.46 seconds (average 0.007541, setup 0.00)

Prob1A: 3 iterations in 0.04 seconds (average 0.013333, setup 0.00)

yes = 1580, no = 162830, maybe = 48230

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=212640, nc=71868, nnz=179452, k=2] [2.3 MB]
Creating vector for yes... [1.6 MB]
Allocating iteration vectors... [2 x 1.6 MB]
TOTAL: [7.2 MB]

Starting iterations...

Iterative method: 95 iterations in 0.29 seconds (average 0.001600, setup 0.14)

Value in the initial state: 0.0018700715017545466

Time for model checking: 0.811 seconds.

Result: 0.0018700715017545466 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmax=? [ F "MissionTimeUp"|"AllAreaSearched"|"Crash" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=1,Objy1=2,Objx2=4,Objy2=1

Prob0A: 3 iterations in 0.10 seconds (average 0.032000, setup 0.00)

Prob1E: 172 iterations in 2.76 seconds (average 0.016047, setup 0.00)

yes = 182722, no = 1139, maybe = 28779

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=212640, nc=50646, nnz=139534, k=2] [1.8 MB]
Creating vector for yes... [1.6 MB]
Allocating iteration vectors... [2 x 1.6 MB]
TOTAL: [6.7 MB]

Starting iterations...

Iterative method: 77 iterations in 0.47 seconds (average 0.001403, setup 0.36)

Value in the initial state: 0.13723614126235611

Time for model checking: 3.366 seconds.

Result: 0.13723614126235611 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmin=? [ F "MissionTimeUp"|"AllAreaSearched"|"Crash" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=1,Objy1=2,Objx2=4,Objy2=1

Prob0E: 3 iterations in 0.11 seconds (average 0.036000, setup 0.00)

Prob1A: 44 iterations in 0.42 seconds (average 0.009455, setup 0.00)

yes = 177792, no = 1139, maybe = 33709

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=212640, nc=59499, nnz=164841, k=2] [2.1 MB]
Creating vector for yes... [1.6 MB]
Allocating iteration vectors... [2 x 1.6 MB]
TOTAL: [7.0 MB]

Starting iterations...

Iterative method: 83 iterations in 0.30 seconds (average 0.001494, setup 0.17)

Value in the initial state: 0.01568303743412096

Time for model checking: 0.838 seconds.

Result: 0.01568303743412096 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"drop"}max=? [ F "deadlock" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=1,Objy1=2,Objx2=4,Objy2=1

Prob0E: 4 iterations in 0.05 seconds (average 0.013000, setup 0.00)

Prob1A: 1 iterations in 0.00 seconds (average 0.004000, setup 0.00)

goal = 1, inf = 0, maybe = 212639

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=212640, nc=277795, nnz=538733, k=4] [8.0 MB]
Building sparse matrix (transition rewards)... [n=212640, nc=277795, nnz=0, k=4] [1.9 MB]
Creating vector for state rewards... [1.6 MB]
Creating vector for inf... [1.6 MB]
Allocating iteration vectors... [2 x 1.6 MB]
TOTAL: [16.4 MB]

Starting iterations...

Iterative method: 82 iterations in 0.70 seconds (average 0.002976, setup 0.46)

Value in the initial state: 0.1431335097808148

Time for model checking: 0.779 seconds.

Result: 0.1431335097808148 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"drop"}min=? [ F "deadlock" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=1,Objy1=2,Objx2=4,Objy2=1

Prob0A: 4 iterations in 0.04 seconds (average 0.011000, setup 0.00)

Prob1E: 5 iterations in 0.05 seconds (average 0.010400, setup 0.00)

Warning: PRISM hasn't checked for zero-reward loops.
Your minimum rewards may be too low...

goal = 1, inf = 0, maybe = 212639

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=212640, nc=277795, nnz=538733, k=4] [8.0 MB]
Building sparse matrix (transition rewards)... [n=212640, nc=277795, nnz=0, k=4] [1.9 MB]
Creating vector for state rewards... [1.6 MB]
Creating vector for inf... [1.6 MB]
Allocating iteration vectors... [2 x 1.6 MB]
TOTAL: [16.4 MB]

Starting iterations...

Iterative method: 76 iterations in 0.61 seconds (average 0.004158, setup 0.29)

Value in the initial state: 0.1360388426120432

Time for model checking: 0.726 seconds.

Result: 0.1360388426120432 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"charge"}max=? [ F "deadlock" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=1,Objy1=2,Objx2=4,Objy2=1

Prob0E: 4 iterations in 0.04 seconds (average 0.011000, setup 0.00)

Prob1A: 1 iterations in 0.01 seconds (average 0.008000, setup 0.00)

goal = 1, inf = 0, maybe = 212639

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=212640, nc=277795, nnz=538733, k=4] [8.0 MB]
Building sparse matrix (transition rewards)... [n=212640, nc=277795, nnz=5341, k=4] [1.9 MB]
Creating vector for state rewards... [1.6 MB]
Creating vector for inf... [1.6 MB]
Allocating iteration vectors... [2 x 1.6 MB]
TOTAL: [16.5 MB]

Starting iterations...

Iterative method: 84 iterations in 0.67 seconds (average 0.004048, setup 0.33)

Value in the initial state: 0.15115363856686836

Time for model checking: 0.748 seconds.

Result: 0.15115363856686836 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"charge"}min=? [ F "deadlock" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=1,Objy1=2,Objx2=4,Objy2=1

Prob0A: 4 iterations in 0.05 seconds (average 0.012000, setup 0.00)

Prob1E: 5 iterations in 0.07 seconds (average 0.013600, setup 0.00)

Warning: PRISM hasn't checked for zero-reward loops.
Your minimum rewards may be too low...

goal = 1, inf = 0, maybe = 212639

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=212640, nc=277795, nnz=538733, k=4] [8.0 MB]
Building sparse matrix (transition rewards)... [n=212640, nc=277795, nnz=5341, k=4] [1.9 MB]
Creating vector for state rewards... [1.6 MB]
Creating vector for inf... [1.6 MB]
Allocating iteration vectors... [2 x 1.6 MB]
TOTAL: [16.5 MB]

Starting iterations...

Iterative method: 92 iterations in 0.69 seconds (average 0.003130, setup 0.40)

Value in the initial state: 0.007865457524162935

Time for model checking: 0.824 seconds.

Result: 0.007865457524162935 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"time"}max=? [ F "deadlock" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=1,Objy1=2,Objx2=4,Objy2=1

Prob0E: 4 iterations in 0.03 seconds (average 0.007000, setup 0.00)

Prob1A: 1 iterations in 0.01 seconds (average 0.008000, setup 0.00)

goal = 1, inf = 0, maybe = 212639

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=212640, nc=277795, nnz=538733, k=4] [8.0 MB]
Building sparse matrix (transition rewards)... [n=212640, nc=277795, nnz=107506, k=4] [3.1 MB]
Creating vector for state rewards... [1.6 MB]
Creating vector for inf... [1.6 MB]
Allocating iteration vectors... [2 x 1.6 MB]
TOTAL: [17.6 MB]

Starting iterations...

Iterative method: 91 iterations in 0.64 seconds (average 0.003165, setup 0.35)

Value in the initial state: 125.26325637902585

Time for model checking: 0.68 seconds.

Result: 125.26325637902585 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"time"}min=? [ F "deadlock" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=1,Objy1=2,Objx2=4,Objy2=1

Prob0A: 4 iterations in 0.02 seconds (average 0.006000, setup 0.00)

Prob1E: 5 iterations in 0.05 seconds (average 0.009600, setup 0.00)

Warning: PRISM hasn't checked for zero-reward loops.
Your minimum rewards may be too low...

goal = 1, inf = 0, maybe = 212639

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=212640, nc=277795, nnz=538733, k=4] [8.0 MB]
Building sparse matrix (transition rewards)... [n=212640, nc=277795, nnz=107506, k=4] [3.1 MB]
Creating vector for state rewards... [1.6 MB]
Creating vector for inf... [1.6 MB]
Allocating iteration vectors... [2 x 1.6 MB]
TOTAL: [17.6 MB]

Starting iterations...

Iterative method: 90 iterations in 0.70 seconds (average 0.003911, setup 0.34)

Value in the initial state: 75.68365317246074

Time for model checking: 0.781 seconds.

Result: 75.68365317246074 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"success"}max=? [ F "deadlock" ]/Pmin=? [ F "MissionSuccessful" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=1,Objy1=2,Objx2=4,Objy2=1

Prob0E: 4 iterations in 0.04 seconds (average 0.010000, setup 0.00)

Prob1A: 1 iterations in 0.01 seconds (average 0.008000, setup 0.00)

goal = 1, inf = 0, maybe = 212639

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=212640, nc=277795, nnz=538733, k=4] [8.0 MB]
Building sparse matrix (transition rewards)... [n=212640, nc=277795, nnz=2244, k=4] [1.9 MB]
Creating vector for state rewards... [1.6 MB]
Creating vector for inf... [1.6 MB]
Allocating iteration vectors... [2 x 1.6 MB]
TOTAL: [16.4 MB]

Starting iterations...

Iterative method: 91 iterations in 0.69 seconds (average 0.003560, setup 0.36)

Prob0E: 45 iterations in 0.35 seconds (average 0.007733, setup 0.00)

Prob1A: 3 iterations in 0.05 seconds (average 0.016000, setup 0.00)

yes = 1138, no = 182723, maybe = 28779

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=212640, nc=50646, nnz=139534, k=2] [1.8 MB]
Creating vector for yes... [1.6 MB]
Allocating iteration vectors... [2 x 1.6 MB]
TOTAL: [6.7 MB]

Starting iterations...

Iterative method: 84 iterations in 0.26 seconds (average 0.001333, setup 0.14)

Value in the initial state: 142.73994202462958

Time for model checking: 1.434 seconds.

Result: 142.73994202462958 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"success"}min=? [ F "deadlock" ]/Pmax=? [ F "MissionSuccessful" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=1,Objy1=2,Objx2=4,Objy2=1

Prob0A: 4 iterations in 0.07 seconds (average 0.017000, setup 0.00)

Prob1E: 5 iterations in 0.05 seconds (average 0.010400, setup 0.00)

Warning: PRISM hasn't checked for zero-reward loops.
Your minimum rewards may be too low...

goal = 1, inf = 0, maybe = 212639

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=212640, nc=277795, nnz=538733, k=4] [8.0 MB]
Building sparse matrix (transition rewards)... [n=212640, nc=277795, nnz=2244, k=4] [1.9 MB]
Creating vector for state rewards... [1.6 MB]
Creating vector for inf... [1.6 MB]
Allocating iteration vectors... [2 x 1.6 MB]
TOTAL: [16.4 MB]

Starting iterations...

Iterative method: 88 iterations in 0.60 seconds (average 0.003045, setup 0.33)

Prob0A: 44 iterations in 0.23 seconds (average 0.005273, setup 0.00)

Prob1E: 4 iterations in 0.04 seconds (average 0.009000, setup 0.00)

yes = 1138, no = 177793, maybe = 33709

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=212640, nc=59499, nnz=164841, k=2] [2.1 MB]
Creating vector for yes... [1.6 MB]
Allocating iteration vectors... [2 x 1.6 MB]
TOTAL: [7.0 MB]

Starting iterations...

Iterative method: 85 iterations in 0.28 seconds (average 0.001412, setup 0.16)

Value in the initial state: 73.35389955955011

Time for model checking: 1.272 seconds.

Result: 73.35389955955011 (value in the initial state)

---------------------------------------------------------------------

Note: There were 16 warnings during computation.

