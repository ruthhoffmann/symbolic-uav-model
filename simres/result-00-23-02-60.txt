PRISM
=====

Version: 4.3
Date: Wed Aug 03 15:52:48 BST 2016
Hostname: eglinton
Memory limits: cudd=1g, java(heap)=981.5m
Command line: prism uav_sim_mdp.prism -s uav_mdp.props -const 'Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=0,Objy1=2,Objx2=6,Objy2=0,'

Parsing model file "uav_sim_mdp.prism"...

Parsing properties file "uav_mdp.props"...

18 properties:
(1) Pmax=? [ F "MissionSuccessful" ]
(2) Pmin=? [ F "MissionSuccessful" ]
(3) Pmax=? [ F "MissionTimeUp" ]
(4) Pmin=? [ F "MissionTimeUp" ]
(5) Pmax=? [ F "Crash" ]
(6) Pmin=? [ F "Crash" ]
(7) Pmax=? [ F "AllAreaSearched" ]
(8) Pmin=? [ F "AllAreaSearched" ]
(9) Pmax=? [ F "MissionTimeUp"|"AllAreaSearched"|"Crash" ]
(10) Pmin=? [ F "MissionTimeUp"|"AllAreaSearched"|"Crash" ]
(11) R{"drop"}max=? [ F "deadlock" ]
(12) R{"drop"}min=? [ F "deadlock" ]
(13) R{"charge"}max=? [ F "deadlock" ]
(14) R{"charge"}min=? [ F "deadlock" ]
(15) R{"time"}max=? [ F "deadlock" ]
(16) R{"time"}min=? [ F "deadlock" ]
(17) R{"success"}max=? [ F "deadlock" ]/Pmin=? [ F "MissionSuccessful" ]
(18) R{"success"}min=? [ F "deadlock" ]/Pmax=? [ F "MissionSuccessful" ]

Type:        MDP
Modules:     Obj Move Robot T 
Variables:   NoOfObjs posx posy s1 retx rety b t c 

---------------------------------------------------------------------

Model checking: Pmax=? [ F "MissionSuccessful" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=0,Objy1=2,Objx2=6,Objy2=0

Building model...
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=0,Objy1=2,Objx2=6,Objy2=0

Warning: Guard for command 7 of module "Move" is never satisfied.

Warning: Guard for command 8 of module "Move" is never satisfied.

Warning: Guard for command 9 of module "Move" is never satisfied.

Warning: Guard for command 7 of module "Robot" is never satisfied.

Warning: Update 2 of command 3 of module "Robot" doesn't do anything ("(s1'=13)", line 210, column 44)

Warning: Update 2 of command 27 of module "T" doesn't do anything ("(c'=1)", line 323, column 108)

Warning: Guard for command 28 of module "T" is never satisfied.

Warning: Update 2 of command 9 of module "T" doesn't do anything ("(c'=1)", line 300, column 112)

Warning: Guard for command 10 of module "T" is never satisfied.

Warning: Update 2 of command 51 of module "T" doesn't do anything ("(c'=1)", line 354, column 113)

Warning: Guard for command 52 of module "T" is never satisfied.

Computing reachable states...

Reachability (BFS): 89 iterations in 1.81 seconds (average 0.020360, setup 0.00)

Time for model construction: 4.846 seconds.

Warning: Deadlocks detected and fixed in 1 states

Type:        MDP
States:      297594 (1 initial)
Transitions: 786142
Choices:     392987

Transition matrix: 238488 nodes (95 terminal), 786142 minterms, vars: 34r/34c/18nd

Prob0A: 49 iterations in 0.61 seconds (average 0.012408, setup 0.00)

Prob1E: 4 iterations in 0.09 seconds (average 0.022000, setup 0.00)

yes = 1173, no = 254207, maybe = 42214

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=297594, nc=74280, nnz=212294, k=2] [2.8 MB]
Creating vector for yes... [2.3 MB]
Allocating iteration vectors... [2 x 2.3 MB]
TOTAL: [9.6 MB]

Starting iterations...

Iterative method: 95 iterations in 0.49 seconds (average 0.002105, setup 0.29)

Value in the initial state: 0.9826302569524699

Time for model checking: 1.519 seconds.

Result: 0.9826302569524699 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmin=? [ F "MissionSuccessful" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=0,Objy1=2,Objx2=6,Objy2=0

Prob0E: 51 iterations in 0.57 seconds (average 0.011216, setup 0.00)

Prob1A: 3 iterations in 0.06 seconds (average 0.020000, setup 0.00)

yes = 1173, no = 261836, maybe = 34585

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=297594, nc=60506, nnz=171449, k=2] [2.3 MB]
Creating vector for yes... [2.3 MB]
Allocating iteration vectors... [2 x 2.3 MB]
TOTAL: [9.1 MB]

Starting iterations...

Iterative method: 88 iterations in 0.36 seconds (average 0.002045, setup 0.18)

Value in the initial state: 0.81254527266116

Time for model checking: 1.048 seconds.

Result: 0.81254527266116 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmax=? [ F "MissionTimeUp" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=0,Objy1=2,Objx2=6,Objy2=0

Prob0A: 68 iterations in 0.93 seconds (average 0.013647, setup 0.00)

Prob1E: 4 iterations in 0.06 seconds (average 0.014000, setup 0.00)

yes = 500, no = 183429, maybe = 113665

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=297594, nc=195800, nnz=537833, k=4] [6.6 MB]
Creating vector for yes... [2.3 MB]
Allocating iteration vectors... [2 x 2.3 MB]
TOTAL: [13.4 MB]

Starting iterations...

Iterative method: 96 iterations in 0.63 seconds (average 0.002667, setup 0.37)

Value in the initial state: 4.8175816999568836E-4

Time for model checking: 1.959 seconds.

Result: 4.8175816999568836E-4 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmin=? [ F "MissionTimeUp" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=0,Objy1=2,Objx2=6,Objy2=0

Prob0E: 84 iterations in 1.36 seconds (average 0.016143, setup 0.00)

Prob1A: 3 iterations in 0.07 seconds (average 0.024000, setup 0.00)

yes = 500, no = 183897, maybe = 113197

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=297594, nc=194968, nnz=536073, k=4] [6.6 MB]
Creating vector for yes... [2.3 MB]
Allocating iteration vectors... [2 x 2.3 MB]
TOTAL: [13.4 MB]

Starting iterations...

Iterative method: 112 iterations in 0.66 seconds (average 0.002679, setup 0.36)

Value in the initial state: 1.2358062472884922E-6

Time for model checking: 2.388 seconds.

Result: 1.2358062472884922E-6 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmax=? [ F "Crash" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=0,Objy1=2,Objx2=6,Objy2=0

Prob0A: 3 iterations in 0.13 seconds (average 0.044000, setup 0.00)

Prob1E: 187 iterations in 4.01 seconds (average 0.021455, setup 0.00)

yes = 145701, no = 3360, maybe = 148533

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=297594, nc=243926, nnz=637081, k=4] [7.8 MB]
Creating vector for yes... [2.3 MB]
Allocating iteration vectors... [2 x 2.3 MB]
TOTAL: [14.6 MB]

Starting iterations...

Iterative method: 91 iterations in 0.68 seconds (average 0.002901, setup 0.42)

Value in the initial state: 0.026573724629610725

Time for model checking: 5.93 seconds.

Result: 0.026573724629610725 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmin=? [ F "Crash" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=0,Objy1=2,Objx2=6,Objy2=0

Prob0E: 4 iterations in 0.10 seconds (average 0.024000, setup 0.00)

Prob1A: 46 iterations in 0.64 seconds (average 0.013826, setup 0.00)

yes = 145701, no = 3360, maybe = 148533

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=297594, nc=243926, nnz=637081, k=4] [7.8 MB]
Creating vector for yes... [2.3 MB]
Allocating iteration vectors... [2 x 2.3 MB]
TOTAL: [14.6 MB]

Starting iterations...

Iterative method: 88 iterations in 0.54 seconds (average 0.002955, setup 0.28)

Value in the initial state: 0.015315954384783712

Time for model checking: 1.534 seconds.

Result: 0.015315954384783712 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmax=? [ F "AllAreaSearched" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=0,Objy1=2,Objx2=6,Objy2=0

Prob0A: 59 iterations in 0.56 seconds (average 0.009492, setup 0.00)

Prob1E: 4 iterations in 0.06 seconds (average 0.015000, setup 0.00)

yes = 1686, no = 232114, maybe = 63794

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=297594, nc=96974, nnz=251336, k=2] [3.3 MB]
Creating vector for yes... [2.3 MB]
Allocating iteration vectors... [2 x 2.3 MB]
TOTAL: [10.1 MB]

Starting iterations...

Iterative method: 92 iterations in 0.44 seconds (average 0.002348, setup 0.22)

Value in the initial state: 0.16287967251718333

Time for model checking: 1.226 seconds.

Result: 0.16287967251718333 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmin=? [ F "AllAreaSearched" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=0,Objy1=2,Objx2=6,Objy2=0

Prob0E: 64 iterations in 0.55 seconds (average 0.008625, setup 0.00)

Prob1A: 3 iterations in 0.04 seconds (average 0.013333, setup 0.00)

yes = 1686, no = 239748, maybe = 56160

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=297594, nc=84231, nnz=215884, k=2] [2.8 MB]
Creating vector for yes... [2.3 MB]
Allocating iteration vectors... [2 x 2.3 MB]
TOTAL: [9.6 MB]

Starting iterations...

Iterative method: 98 iterations in 0.36 seconds (average 0.002204, setup 0.14)

Value in the initial state: 0.0018153655304570461

Time for model checking: 1.007 seconds.

Result: 0.0018153655304570461 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmax=? [ F "MissionTimeUp"|"AllAreaSearched"|"Crash" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=0,Objy1=2,Objx2=6,Objy2=0

Prob0A: 3 iterations in 0.11 seconds (average 0.037333, setup 0.00)

Prob1E: 196 iterations in 4.48 seconds (average 0.022857, setup 0.00)

yes = 261835, no = 1174, maybe = 34585

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=297594, nc=60506, nnz=171449, k=2] [2.3 MB]
Creating vector for yes... [2.3 MB]
Allocating iteration vectors... [2 x 2.3 MB]
TOTAL: [9.1 MB]

Starting iterations...

Iterative method: 81 iterations in 0.39 seconds (average 0.001975, setup 0.23)

Value in the initial state: 0.18745443139749632

Time for model checking: 5.706 seconds.

Result: 0.18745443139749632 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmin=? [ F "MissionTimeUp"|"AllAreaSearched"|"Crash" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=0,Objy1=2,Objx2=6,Objy2=0

Prob0E: 4 iterations in 0.08 seconds (average 0.019000, setup 0.00)

Prob1A: 49 iterations in 0.97 seconds (average 0.019755, setup 0.00)

yes = 254206, no = 1174, maybe = 42214

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=297594, nc=74280, nnz=212294, k=2] [2.8 MB]
Creating vector for yes... [2.3 MB]
Allocating iteration vectors... [2 x 2.3 MB]
TOTAL: [9.6 MB]

Starting iterations...

Iterative method: 93 iterations in 0.42 seconds (average 0.002108, setup 0.22)

Value in the initial state: 0.017369123347176062

Time for model checking: 1.495 seconds.

Result: 0.017369123347176062 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"drop"}max=? [ F "deadlock" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=0,Objy1=2,Objx2=6,Objy2=0

Prob0E: 5 iterations in 0.13 seconds (average 0.026400, setup 0.00)

Prob1A: 1 iterations in 0.01 seconds (average 0.012000, setup 0.00)

goal = 1, inf = 0, maybe = 297593

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=297594, nc=392986, nnz=786141, k=4] [11.6 MB]
Building sparse matrix (transition rewards)... [n=297594, nc=392986, nnz=0, k=4] [2.6 MB]
Creating vector for state rewards... [2.3 MB]
Creating vector for inf... [2.3 MB]
Allocating iteration vectors... [2 x 2.3 MB]
TOTAL: [23.3 MB]

Starting iterations...

Iterative method: 91 iterations in 0.90 seconds (average 0.004308, setup 0.51)

Value in the initial state: 0.23436045559488283

Time for model checking: 1.075 seconds.

Result: 0.23436045559488283 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"drop"}min=? [ F "deadlock" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=0,Objy1=2,Objx2=6,Objy2=0

Prob0A: 4 iterations in 0.04 seconds (average 0.010000, setup 0.00)

Prob1E: 5 iterations in 0.07 seconds (average 0.013600, setup 0.00)

Warning: PRISM hasn't checked for zero-reward loops.
Your minimum rewards may be too low...

goal = 1, inf = 0, maybe = 297593

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=297594, nc=392986, nnz=786141, k=4] [11.6 MB]
Building sparse matrix (transition rewards)... [n=297594, nc=392986, nnz=0, k=4] [2.6 MB]
Creating vector for state rewards... [2.3 MB]
Creating vector for inf... [2.3 MB]
Allocating iteration vectors... [2 x 2.3 MB]
TOTAL: [23.3 MB]

Starting iterations...

Iterative method: 80 iterations in 0.75 seconds (average 0.004350, setup 0.40)

Value in the initial state: 0.21497357046415858

Time for model checking: 0.888 seconds.

Result: 0.21497357046415858 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"charge"}max=? [ F "deadlock" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=0,Objy1=2,Objx2=6,Objy2=0

Prob0E: 5 iterations in 0.05 seconds (average 0.009600, setup 0.00)

Prob1A: 1 iterations in 0.01 seconds (average 0.008000, setup 0.00)

goal = 1, inf = 0, maybe = 297593

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=297594, nc=392986, nnz=786141, k=4] [11.6 MB]
Building sparse matrix (transition rewards)... [n=297594, nc=392986, nnz=8928, k=4] [2.7 MB]
Creating vector for state rewards... [2.3 MB]
Creating vector for inf... [2.3 MB]
Allocating iteration vectors... [2 x 2.3 MB]
TOTAL: [23.4 MB]

Starting iterations...

Iterative method: 88 iterations in 0.78 seconds (average 0.004227, setup 0.40)

Value in the initial state: 0.2160914288406275

Time for model checking: 0.861 seconds.

Result: 0.2160914288406275 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"charge"}min=? [ F "deadlock" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=0,Objy1=2,Objx2=6,Objy2=0

Prob0A: 4 iterations in 0.03 seconds (average 0.008000, setup 0.00)

Prob1E: 5 iterations in 0.06 seconds (average 0.012000, setup 0.00)

Warning: PRISM hasn't checked for zero-reward loops.
Your minimum rewards may be too low...

goal = 1, inf = 0, maybe = 297593

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=297594, nc=392986, nnz=786141, k=4] [11.6 MB]
Building sparse matrix (transition rewards)... [n=297594, nc=392986, nnz=8928, k=4] [2.7 MB]
Creating vector for state rewards... [2.3 MB]
Creating vector for inf... [2.3 MB]
Allocating iteration vectors... [2 x 2.3 MB]
TOTAL: [23.4 MB]

Starting iterations...

Iterative method: 96 iterations in 0.83 seconds (average 0.004333, setup 0.42)

Value in the initial state: 0.02972424194667262

Time for model checking: 0.938 seconds.

Result: 0.02972424194667262 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"time"}max=? [ F "deadlock" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=0,Objy1=2,Objx2=6,Objy2=0

Prob0E: 5 iterations in 0.04 seconds (average 0.008800, setup 0.00)

Prob1A: 1 iterations in 0.01 seconds (average 0.008000, setup 0.00)

goal = 1, inf = 0, maybe = 297593

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=297594, nc=392986, nnz=786141, k=4] [11.6 MB]
Building sparse matrix (transition rewards)... [n=297594, nc=392986, nnz=149059, k=4] [4.3 MB]
Creating vector for state rewards... [2.3 MB]
Creating vector for inf... [2.3 MB]
Allocating iteration vectors... [2 x 2.3 MB]
TOTAL: [25.1 MB]

Starting iterations...

Iterative method: 94 iterations in 0.87 seconds (average 0.004383, setup 0.46)

Value in the initial state: 146.75728524809588

Time for model checking: 0.928 seconds.

Result: 146.75728524809588 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"time"}min=? [ F "deadlock" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=0,Objy1=2,Objx2=6,Objy2=0

Prob0A: 4 iterations in 0.03 seconds (average 0.008000, setup 0.00)

Prob1E: 5 iterations in 0.06 seconds (average 0.011200, setup 0.00)

Warning: PRISM hasn't checked for zero-reward loops.
Your minimum rewards may be too low...

goal = 1, inf = 0, maybe = 297593

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=297594, nc=392986, nnz=786141, k=4] [11.6 MB]
Building sparse matrix (transition rewards)... [n=297594, nc=392986, nnz=149059, k=4] [4.3 MB]
Creating vector for state rewards... [2.3 MB]
Creating vector for inf... [2.3 MB]
Allocating iteration vectors... [2 x 2.3 MB]
TOTAL: [25.1 MB]

Starting iterations...

Iterative method: 101 iterations in 0.88 seconds (average 0.004356, setup 0.44)

Value in the initial state: 84.94328378504765

Time for model checking: 0.994 seconds.

Result: 84.94328378504765 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"success"}max=? [ F "deadlock" ]/Pmin=? [ F "MissionSuccessful" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=0,Objy1=2,Objx2=6,Objy2=0

Prob0E: 5 iterations in 0.04 seconds (average 0.007200, setup 0.00)

Prob1A: 1 iterations in 0.00 seconds (average 0.004000, setup 0.00)

goal = 1, inf = 0, maybe = 297593

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=297594, nc=392986, nnz=786141, k=4] [11.6 MB]
Building sparse matrix (transition rewards)... [n=297594, nc=392986, nnz=2310, k=4] [2.7 MB]
Creating vector for state rewards... [2.3 MB]
Creating vector for inf... [2.3 MB]
Allocating iteration vectors... [2 x 2.3 MB]
TOTAL: [23.4 MB]

Starting iterations...

Iterative method: 96 iterations in 0.82 seconds (average 0.004333, setup 0.40)

Prob0E: 51 iterations in 0.33 seconds (average 0.006431, setup 0.00)

Prob1A: 3 iterations in 0.04 seconds (average 0.014667, setup 0.00)

yes = 1173, no = 261836, maybe = 34585

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=297594, nc=60506, nnz=171449, k=2] [2.3 MB]
Creating vector for yes... [2.3 MB]
Allocating iteration vectors... [2 x 2.3 MB]
TOTAL: [9.1 MB]

Starting iterations...

Iterative method: 88 iterations in 0.30 seconds (average 0.001864, setup 0.14)

Value in the initial state: 177.25367351703625

Time for model checking: 1.563 seconds.

Result: 177.25367351703625 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"success"}min=? [ F "deadlock" ]/Pmax=? [ F "MissionSuccessful" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=0,Objy1=2,Objx2=6,Objy2=0

Prob0A: 4 iterations in 0.05 seconds (average 0.012000, setup 0.00)

Prob1E: 5 iterations in 0.06 seconds (average 0.012000, setup 0.00)

Warning: PRISM hasn't checked for zero-reward loops.
Your minimum rewards may be too low...

goal = 1, inf = 0, maybe = 297593

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=297594, nc=392986, nnz=786141, k=4] [11.6 MB]
Building sparse matrix (transition rewards)... [n=297594, nc=392986, nnz=2310, k=4] [2.7 MB]
Creating vector for state rewards... [2.3 MB]
Creating vector for inf... [2.3 MB]
Allocating iteration vectors... [2 x 2.3 MB]
TOTAL: [23.4 MB]

Starting iterations...

Iterative method: 93 iterations in 0.72 seconds (average 0.003871, setup 0.36)

Prob0A: 49 iterations in 0.22 seconds (average 0.004490, setup 0.00)

Prob1E: 4 iterations in 0.02 seconds (average 0.006000, setup 0.00)

yes = 1173, no = 254207, maybe = 42214

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=297594, nc=74280, nnz=212294, k=2] [2.8 MB]
Creating vector for yes... [2.3 MB]
Allocating iteration vectors... [2 x 2.3 MB]
TOTAL: [9.6 MB]

Starting iterations...

Iterative method: 95 iterations in 0.28 seconds (average 0.001768, setup 0.11)

Value in the initial state: 76.59264807815687

Time for model checking: 1.373 seconds.

Result: 76.59264807815687 (value in the initial state)

---------------------------------------------------------------------

Note: There were 16 warnings during computation.

