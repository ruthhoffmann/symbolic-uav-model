PRISM
=====

Version: 4.3
Date: Wed Aug 03 16:13:45 BST 2016
Hostname: eglinton
Memory limits: cudd=1g, java(heap)=981.5m
Command line: prism uav_sim_mdp.prism -s uav_mdp.props -const 'Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=3,Objy1=1,Objx2=4,Objy2=2,'

Parsing model file "uav_sim_mdp.prism"...

Parsing properties file "uav_mdp.props"...

18 properties:
(1) Pmax=? [ F "MissionSuccessful" ]
(2) Pmin=? [ F "MissionSuccessful" ]
(3) Pmax=? [ F "MissionTimeUp" ]
(4) Pmin=? [ F "MissionTimeUp" ]
(5) Pmax=? [ F "Crash" ]
(6) Pmin=? [ F "Crash" ]
(7) Pmax=? [ F "AllAreaSearched" ]
(8) Pmin=? [ F "AllAreaSearched" ]
(9) Pmax=? [ F "MissionTimeUp"|"AllAreaSearched"|"Crash" ]
(10) Pmin=? [ F "MissionTimeUp"|"AllAreaSearched"|"Crash" ]
(11) R{"drop"}max=? [ F "deadlock" ]
(12) R{"drop"}min=? [ F "deadlock" ]
(13) R{"charge"}max=? [ F "deadlock" ]
(14) R{"charge"}min=? [ F "deadlock" ]
(15) R{"time"}max=? [ F "deadlock" ]
(16) R{"time"}min=? [ F "deadlock" ]
(17) R{"success"}max=? [ F "deadlock" ]/Pmin=? [ F "MissionSuccessful" ]
(18) R{"success"}min=? [ F "deadlock" ]/Pmax=? [ F "MissionSuccessful" ]

Type:        MDP
Modules:     Obj Move Robot T 
Variables:   NoOfObjs posx posy s1 retx rety b t c 

---------------------------------------------------------------------

Model checking: Pmax=? [ F "MissionSuccessful" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=3,Objy1=1,Objx2=4,Objy2=2

Building model...
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=3,Objy1=1,Objx2=4,Objy2=2

Warning: Guard for command 7 of module "Move" is never satisfied.

Warning: Guard for command 8 of module "Move" is never satisfied.

Warning: Guard for command 9 of module "Move" is never satisfied.

Warning: Guard for command 7 of module "Robot" is never satisfied.

Warning: Update 2 of command 3 of module "Robot" doesn't do anything ("(s1'=13)", line 210, column 44)

Warning: Update 2 of command 27 of module "T" doesn't do anything ("(c'=1)", line 323, column 108)

Warning: Guard for command 28 of module "T" is never satisfied.

Warning: Update 2 of command 9 of module "T" doesn't do anything ("(c'=1)", line 300, column 112)

Warning: Guard for command 10 of module "T" is never satisfied.

Warning: Update 2 of command 51 of module "T" doesn't do anything ("(c'=1)", line 354, column 113)

Warning: Guard for command 52 of module "T" is never satisfied.

Computing reachable states...

Reachability (BFS): 88 iterations in 1.25 seconds (average 0.014182, setup 0.00)

Time for model construction: 3.43 seconds.

Warning: Deadlocks detected and fixed in 1 states

Type:        MDP
States:      231782 (1 initial)
Transitions: 611276
Choices:     307158

Transition matrix: 212212 nodes (89 terminal), 611276 minterms, vars: 34r/34c/18nd

Prob0A: 47 iterations in 0.34 seconds (average 0.007319, setup 0.00)

Prob1E: 4 iterations in 0.06 seconds (average 0.016000, setup 0.00)

yes = 1111, no = 192395, maybe = 38276

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=231782, nc=67089, nnz=188533, k=2] [2.4 MB]
Creating vector for yes... [1.8 MB]
Allocating iteration vectors... [2 x 1.8 MB]
TOTAL: [7.7 MB]

Starting iterations...

Iterative method: 92 iterations in 0.39 seconds (average 0.001609, setup 0.24)

Value in the initial state: 0.9830372863352754

Time for model checking: 0.811 seconds.

Result: 0.9830372863352754 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmin=? [ F "MissionSuccessful" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=3,Objy1=1,Objx2=4,Objy2=2

Prob0E: 48 iterations in 0.41 seconds (average 0.008583, setup 0.00)

Prob1A: 3 iterations in 0.05 seconds (average 0.016000, setup 0.00)

yes = 1111, no = 197506, maybe = 33165

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=231782, nc=57984, nnz=162576, k=2] [2.1 MB]
Creating vector for yes... [1.8 MB]
Allocating iteration vectors... [2 x 1.8 MB]
TOTAL: [7.4 MB]

Starting iterations...

Iterative method: 88 iterations in 0.33 seconds (average 0.001545, setup 0.20)

Value in the initial state: 0.8425968487150445

Time for model checking: 0.826 seconds.

Result: 0.8425968487150445 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmax=? [ F "MissionTimeUp" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=3,Objy1=1,Objx2=4,Objy2=2

Prob0A: 72 iterations in 0.65 seconds (average 0.009056, setup 0.00)

Prob1E: 4 iterations in 0.04 seconds (average 0.010000, setup 0.00)

yes = 372, no = 141987, maybe = 89423

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=231782, nc=152843, nnz=416332, k=4] [5.1 MB]
Creating vector for yes... [1.8 MB]
Allocating iteration vectors... [2 x 1.8 MB]
TOTAL: [10.4 MB]

Starting iterations...

Iterative method: 94 iterations in 0.46 seconds (average 0.001957, setup 0.28)

Value in the initial state: 8.548452040847175E-5

Time for model checking: 1.168 seconds.

Result: 8.548452040847175E-5 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmin=? [ F "MissionTimeUp" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=3,Objy1=1,Objx2=4,Objy2=2

Prob0E: 87 iterations in 0.80 seconds (average 0.009241, setup 0.00)

Prob1A: 3 iterations in 0.04 seconds (average 0.013333, setup 0.00)

yes = 372, no = 142371, maybe = 89039

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=231782, nc=152165, nnz=414856, k=4] [5.1 MB]
Creating vector for yes... [1.8 MB]
Allocating iteration vectors... [2 x 1.8 MB]
TOTAL: [10.4 MB]

Starting iterations...

Iterative method: 112 iterations in 0.48 seconds (average 0.002036, setup 0.26)

Value in the initial state: 8.752050315413201E-8

Time for model checking: 1.355 seconds.

Result: 8.752050315413201E-8 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmax=? [ F "Crash" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=3,Objy1=1,Objx2=4,Objy2=2

Prob0A: 3 iterations in 0.09 seconds (average 0.029333, setup 0.00)

Prob1E: 180 iterations in 2.67 seconds (average 0.014822, setup 0.00)

yes = 113646, no = 3002, maybe = 115134

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=231782, nc=190510, nnz=494628, k=4] [6.1 MB]
Creating vector for yes... [1.8 MB]
Allocating iteration vectors... [2 x 1.8 MB]
TOTAL: [11.4 MB]

Starting iterations...

Iterative method: 91 iterations in 0.52 seconds (average 0.002154, setup 0.32)

Value in the initial state: 0.02472292168747326

Time for model checking: 3.297 seconds.

Result: 0.02472292168747326 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmin=? [ F "Crash" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=3,Objy1=1,Objx2=4,Objy2=2

Prob0E: 3 iterations in 0.06 seconds (average 0.018667, setup 0.00)

Prob1A: 47 iterations in 0.45 seconds (average 0.009532, setup 0.00)

yes = 113646, no = 3002, maybe = 115134

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=231782, nc=190510, nnz=494628, k=4] [6.1 MB]
Creating vector for yes... [1.8 MB]
Allocating iteration vectors... [2 x 1.8 MB]
TOTAL: [11.4 MB]

Starting iterations...

Iterative method: 83 iterations in 0.42 seconds (average 0.002217, setup 0.24)

Value in the initial state: 0.014925573499757804

Time for model checking: 0.944 seconds.

Result: 0.014925573499757804 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmax=? [ F "AllAreaSearched" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=3,Objy1=1,Objx2=4,Objy2=2

Prob0A: 53 iterations in 0.35 seconds (average 0.006642, setup 0.00)

Prob1E: 4 iterations in 0.04 seconds (average 0.011000, setup 0.00)

yes = 1518, no = 173559, maybe = 56705

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=231782, nc=89616, nnz=237378, k=2] [3.0 MB]
Creating vector for yes... [1.8 MB]
Allocating iteration vectors... [2 x 1.8 MB]
TOTAL: [8.3 MB]

Starting iterations...

Iterative method: 86 iterations in 0.34 seconds (average 0.001628, setup 0.20)

Value in the initial state: 0.13461900906289653

Time for model checking: 0.76 seconds.

Result: 0.13461900906289653 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmin=? [ F "AllAreaSearched" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=3,Objy1=1,Objx2=4,Objy2=2

Prob0E: 62 iterations in 0.43 seconds (average 0.006903, setup 0.00)

Prob1A: 3 iterations in 0.04 seconds (average 0.013333, setup 0.00)

yes = 1518, no = 180974, maybe = 49290

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=231782, nc=76794, nnz=200775, k=2] [2.6 MB]
Creating vector for yes... [1.8 MB]
Allocating iteration vectors... [2 x 1.8 MB]
TOTAL: [7.9 MB]

Starting iterations...

Iterative method: 96 iterations in 0.30 seconds (average 0.001667, setup 0.14)

Value in the initial state: 0.0018479990307391737

Time for model checking: 0.788 seconds.

Result: 0.0018479990307391737 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmax=? [ F "MissionTimeUp"|"AllAreaSearched"|"Crash" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=3,Objy1=1,Objx2=4,Objy2=2

Prob0A: 3 iterations in 0.11 seconds (average 0.037333, setup 0.00)

Prob1E: 184 iterations in 2.65 seconds (average 0.014413, setup 0.00)

yes = 197505, no = 1112, maybe = 33165

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=231782, nc=57984, nnz=162576, k=2] [2.1 MB]
Creating vector for yes... [1.8 MB]
Allocating iteration vectors... [2 x 1.8 MB]
TOTAL: [7.4 MB]

Starting iterations...

Iterative method: 81 iterations in 0.31 seconds (average 0.001531, setup 0.19)

Value in the initial state: 0.1574026960082941

Time for model checking: 3.082 seconds.

Result: 0.1574026960082941 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmin=? [ F "MissionTimeUp"|"AllAreaSearched"|"Crash" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=3,Objy1=1,Objx2=4,Objy2=2

Prob0E: 3 iterations in 0.04 seconds (average 0.013333, setup 0.00)

Prob1A: 47 iterations in 0.41 seconds (average 0.008681, setup 0.00)

yes = 192394, no = 1112, maybe = 38276

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=231782, nc=67089, nnz=188533, k=2] [2.4 MB]
Creating vector for yes... [1.8 MB]
Allocating iteration vectors... [2 x 1.8 MB]
TOTAL: [7.7 MB]

Starting iterations...

Iterative method: 91 iterations in 0.29 seconds (average 0.001538, setup 0.15)

Value in the initial state: 0.01696127337138318

Time for model checking: 0.793 seconds.

Result: 0.01696127337138318 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"drop"}max=? [ F "deadlock" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=3,Objy1=1,Objx2=4,Objy2=2

Prob0E: 4 iterations in 0.04 seconds (average 0.011000, setup 0.00)

Prob1A: 1 iterations in 0.00 seconds (average 0.004000, setup 0.00)

goal = 1, inf = 0, maybe = 231781

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=231782, nc=307157, nnz=611275, k=4] [9.1 MB]
Building sparse matrix (transition rewards)... [n=231782, nc=307157, nnz=0, k=4] [2.1 MB]
Creating vector for state rewards... [1.8 MB]
Creating vector for inf... [1.8 MB]
Allocating iteration vectors... [2 x 1.8 MB]
TOTAL: [18.2 MB]

Starting iterations...

Iterative method: 88 iterations in 0.70 seconds (average 0.003409, setup 0.40)

Value in the initial state: 0.17583880996149434

Time for model checking: 0.762 seconds.

Result: 0.17583880996149434 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"drop"}min=? [ F "deadlock" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=3,Objy1=1,Objx2=4,Objy2=2

Prob0A: 4 iterations in 0.04 seconds (average 0.010000, setup 0.00)

Prob1E: 5 iterations in 0.05 seconds (average 0.009600, setup 0.00)

Warning: PRISM hasn't checked for zero-reward loops.
Your minimum rewards may be too low...

goal = 1, inf = 0, maybe = 231781

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=231782, nc=307157, nnz=611275, k=4] [9.1 MB]
Building sparse matrix (transition rewards)... [n=231782, nc=307157, nnz=0, k=4] [2.1 MB]
Creating vector for state rewards... [1.8 MB]
Creating vector for inf... [1.8 MB]
Allocating iteration vectors... [2 x 1.8 MB]
TOTAL: [18.2 MB]

Starting iterations...

Iterative method: 81 iterations in 0.59 seconds (average 0.003407, setup 0.32)

Value in the initial state: 0.16019021295972785

Time for model checking: 0.689 seconds.

Result: 0.16019021295972785 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"charge"}max=? [ F "deadlock" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=3,Objy1=1,Objx2=4,Objy2=2

Prob0E: 4 iterations in 0.03 seconds (average 0.007000, setup 0.00)

Prob1A: 1 iterations in 0.01 seconds (average 0.008000, setup 0.00)

goal = 1, inf = 0, maybe = 231781

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=231782, nc=307157, nnz=611275, k=4] [9.1 MB]
Building sparse matrix (transition rewards)... [n=231782, nc=307157, nnz=6589, k=4] [2.1 MB]
Creating vector for state rewards... [1.8 MB]
Creating vector for inf... [1.8 MB]
Allocating iteration vectors... [2 x 1.8 MB]
TOTAL: [18.3 MB]

Starting iterations...

Iterative method: 87 iterations in 0.60 seconds (average 0.003356, setup 0.31)

Value in the initial state: 0.17342696944834823

Time for model checking: 0.656 seconds.

Result: 0.17342696944834823 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"charge"}min=? [ F "deadlock" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=3,Objy1=1,Objx2=4,Objy2=2

Prob0A: 4 iterations in 0.03 seconds (average 0.007000, setup 0.00)

Prob1E: 5 iterations in 0.05 seconds (average 0.010400, setup 0.00)

Warning: PRISM hasn't checked for zero-reward loops.
Your minimum rewards may be too low...

goal = 1, inf = 0, maybe = 231781

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=231782, nc=307157, nnz=611275, k=4] [9.1 MB]
Building sparse matrix (transition rewards)... [n=231782, nc=307157, nnz=6589, k=4] [2.1 MB]
Creating vector for state rewards... [1.8 MB]
Creating vector for inf... [1.8 MB]
Allocating iteration vectors... [2 x 1.8 MB]
TOTAL: [18.3 MB]

Starting iterations...

Iterative method: 97 iterations in 0.64 seconds (average 0.003299, setup 0.32)

Value in the initial state: 0.02151872830860963

Time for model checking: 0.74 seconds.

Result: 0.02151872830860963 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"time"}max=? [ F "deadlock" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=3,Objy1=1,Objx2=4,Objy2=2

Prob0E: 4 iterations in 0.03 seconds (average 0.007000, setup 0.00)

Prob1A: 1 iterations in 0.00 seconds (average 0.004000, setup 0.00)

goal = 1, inf = 0, maybe = 231781

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=231782, nc=307157, nnz=611275, k=4] [9.1 MB]
Building sparse matrix (transition rewards)... [n=231782, nc=307157, nnz=116646, k=4] [3.4 MB]
Creating vector for state rewards... [1.8 MB]
Creating vector for inf... [1.8 MB]
Allocating iteration vectors... [2 x 1.8 MB]
TOTAL: [19.5 MB]

Starting iterations...

Iterative method: 94 iterations in 0.68 seconds (average 0.003404, setup 0.36)

Value in the initial state: 136.6654170769622

Time for model checking: 0.733 seconds.

Result: 136.6654170769622 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"time"}min=? [ F "deadlock" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=3,Objy1=1,Objx2=4,Objy2=2

Prob0A: 4 iterations in 0.03 seconds (average 0.007000, setup 0.00)

Prob1E: 5 iterations in 0.05 seconds (average 0.010400, setup 0.00)

Warning: PRISM hasn't checked for zero-reward loops.
Your minimum rewards may be too low...

goal = 1, inf = 0, maybe = 231781

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=231782, nc=307157, nnz=611275, k=4] [9.1 MB]
Building sparse matrix (transition rewards)... [n=231782, nc=307157, nnz=116646, k=4] [3.4 MB]
Creating vector for state rewards... [1.8 MB]
Creating vector for inf... [1.8 MB]
Allocating iteration vectors... [2 x 1.8 MB]
TOTAL: [19.5 MB]

Starting iterations...

Iterative method: 95 iterations in 0.68 seconds (average 0.003495, setup 0.35)

Value in the initial state: 82.81319351612669

Time for model checking: 0.777 seconds.

Result: 82.81319351612669 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"success"}max=? [ F "deadlock" ]/Pmin=? [ F "MissionSuccessful" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=3,Objy1=1,Objx2=4,Objy2=2

Prob0E: 4 iterations in 0.03 seconds (average 0.007000, setup 0.00)

Prob1A: 1 iterations in 0.01 seconds (average 0.008000, setup 0.00)

goal = 1, inf = 0, maybe = 231781

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=231782, nc=307157, nnz=611275, k=4] [9.1 MB]
Building sparse matrix (transition rewards)... [n=231782, nc=307157, nnz=2190, k=4] [2.1 MB]
Creating vector for state rewards... [1.8 MB]
Creating vector for inf... [1.8 MB]
Allocating iteration vectors... [2 x 1.8 MB]
TOTAL: [18.2 MB]

Starting iterations...

Iterative method: 94 iterations in 0.63 seconds (average 0.003277, setup 0.32)

Prob0E: 48 iterations in 0.36 seconds (average 0.007417, setup 0.00)

Prob1A: 3 iterations in 0.05 seconds (average 0.017333, setup 0.00)

yes = 1111, no = 197506, maybe = 33165

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=231782, nc=57984, nnz=162576, k=2] [2.1 MB]
Creating vector for yes... [1.8 MB]
Allocating iteration vectors... [2 x 1.8 MB]
TOTAL: [7.4 MB]

Starting iterations...

Iterative method: 88 iterations in 0.27 seconds (average 0.001500, setup 0.14)

Value in the initial state: 159.317097236119

Time for model checking: 1.382 seconds.

Result: 159.317097236119 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"success"}min=? [ F "deadlock" ]/Pmax=? [ F "MissionSuccessful" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=3,Objy1=1,Objx2=4,Objy2=2

Prob0A: 4 iterations in 0.06 seconds (average 0.016000, setup 0.00)

Prob1E: 5 iterations in 0.06 seconds (average 0.011200, setup 0.00)

Warning: PRISM hasn't checked for zero-reward loops.
Your minimum rewards may be too low...

goal = 1, inf = 0, maybe = 231781

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=231782, nc=307157, nnz=611275, k=4] [9.1 MB]
Building sparse matrix (transition rewards)... [n=231782, nc=307157, nnz=2190, k=4] [2.1 MB]
Creating vector for state rewards... [1.8 MB]
Creating vector for inf... [1.8 MB]
Allocating iteration vectors... [2 x 1.8 MB]
TOTAL: [18.2 MB]

Starting iterations...

Iterative method: 93 iterations in 0.67 seconds (average 0.003312, setup 0.36)

Prob0A: 47 iterations in 0.26 seconds (average 0.005617, setup 0.00)

Prob1E: 4 iterations in 0.04 seconds (average 0.009000, setup 0.00)

yes = 1111, no = 192395, maybe = 38276

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=231782, nc=67089, nnz=188533, k=2] [2.4 MB]
Creating vector for yes... [1.8 MB]
Allocating iteration vectors... [2 x 1.8 MB]
TOTAL: [7.7 MB]

Starting iterations...

Iterative method: 92 iterations in 0.32 seconds (average 0.001522, setup 0.18)

Value in the initial state: 76.77862352081246

Time for model checking: 1.418 seconds.

Result: 76.77862352081246 (value in the initial state)

---------------------------------------------------------------------

Note: There were 16 warnings during computation.

