PRISM
=====

Version: 4.3
Date: Wed Aug 03 16:18:23 BST 2016
Hostname: eglinton
Memory limits: cudd=1g, java(heap)=981.5m
Command line: prism uav_sim_mdp.prism -s uav_mdp.props -const 'Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=5,Objy1=2,Objx2=6,Objy2=3,'

Parsing model file "uav_sim_mdp.prism"...

Parsing properties file "uav_mdp.props"...

18 properties:
(1) Pmax=? [ F "MissionSuccessful" ]
(2) Pmin=? [ F "MissionSuccessful" ]
(3) Pmax=? [ F "MissionTimeUp" ]
(4) Pmin=? [ F "MissionTimeUp" ]
(5) Pmax=? [ F "Crash" ]
(6) Pmin=? [ F "Crash" ]
(7) Pmax=? [ F "AllAreaSearched" ]
(8) Pmin=? [ F "AllAreaSearched" ]
(9) Pmax=? [ F "MissionTimeUp"|"AllAreaSearched"|"Crash" ]
(10) Pmin=? [ F "MissionTimeUp"|"AllAreaSearched"|"Crash" ]
(11) R{"drop"}max=? [ F "deadlock" ]
(12) R{"drop"}min=? [ F "deadlock" ]
(13) R{"charge"}max=? [ F "deadlock" ]
(14) R{"charge"}min=? [ F "deadlock" ]
(15) R{"time"}max=? [ F "deadlock" ]
(16) R{"time"}min=? [ F "deadlock" ]
(17) R{"success"}max=? [ F "deadlock" ]/Pmin=? [ F "MissionSuccessful" ]
(18) R{"success"}min=? [ F "deadlock" ]/Pmax=? [ F "MissionSuccessful" ]

Type:        MDP
Modules:     Obj Move Robot T 
Variables:   NoOfObjs posx posy s1 retx rety b t c 

---------------------------------------------------------------------

Model checking: Pmax=? [ F "MissionSuccessful" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=5,Objy1=2,Objx2=6,Objy2=3

Building model...
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=5,Objy1=2,Objx2=6,Objy2=3

Warning: Guard for command 7 of module "Move" is never satisfied.

Warning: Guard for command 8 of module "Move" is never satisfied.

Warning: Guard for command 9 of module "Move" is never satisfied.

Warning: Guard for command 7 of module "Robot" is never satisfied.

Warning: Update 2 of command 3 of module "Robot" doesn't do anything ("(s1'=13)", line 210, column 44)

Warning: Update 2 of command 27 of module "T" doesn't do anything ("(c'=1)", line 323, column 108)

Warning: Guard for command 28 of module "T" is never satisfied.

Warning: Update 2 of command 9 of module "T" doesn't do anything ("(c'=1)", line 300, column 112)

Warning: Guard for command 10 of module "T" is never satisfied.

Warning: Update 2 of command 51 of module "T" doesn't do anything ("(c'=1)", line 354, column 113)

Warning: Guard for command 52 of module "T" is never satisfied.

Computing reachable states...

Reachability (BFS): 89 iterations in 1.12 seconds (average 0.012539, setup 0.00)

Time for model construction: 2.354 seconds.

Warning: Deadlocks detected and fixed in 1 states

Type:        MDP
States:      256291 (1 initial)
Transitions: 747175
Choices:     354176

Transition matrix: 208533 nodes (89 terminal), 747175 minterms, vars: 34r/34c/18nd

Prob0A: 53 iterations in 0.29 seconds (average 0.005434, setup 0.00)

Prob1E: 4 iterations in 0.04 seconds (average 0.010000, setup 0.00)

yes = 1049, no = 210577, maybe = 44665

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=256291, nc=81011, nnz=238006, k=2] [3.0 MB]
Creating vector for yes... [2.0 MB]
Allocating iteration vectors... [2 x 2.0 MB]
TOTAL: [8.9 MB]

Starting iterations...

Iterative method: 103 iterations in 0.38 seconds (average 0.001631, setup 0.21)

Value in the initial state: 0.9790275504892295

Time for model checking: 0.73 seconds.

Result: 0.9790275504892295 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmin=? [ F "MissionSuccessful" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=5,Objy1=2,Objx2=6,Objy2=3

Prob0E: 53 iterations in 0.32 seconds (average 0.006038, setup 0.00)

Prob1A: 3 iterations in 0.04 seconds (average 0.014667, setup 0.00)

yes = 1049, no = 217064, maybe = 38178

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=256291, nc=68836, nnz=202190, k=2] [2.6 MB]
Creating vector for yes... [2.0 MB]
Allocating iteration vectors... [2 x 2.0 MB]
TOTAL: [8.5 MB]

Starting iterations...

Iterative method: 94 iterations in 0.43 seconds (average 0.002383, setup 0.20)

Value in the initial state: 0.71358846403895

Time for model checking: 0.816 seconds.

Result: 0.71358846403895 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmax=? [ F "MissionTimeUp" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=5,Objy1=2,Objx2=6,Objy2=3

Prob0A: 78 iterations in 0.53 seconds (average 0.006821, setup 0.00)

Prob1E: 4 iterations in 0.03 seconds (average 0.008000, setup 0.00)

yes = 300, no = 148827, maybe = 107164

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=256291, nc=193374, nnz=550441, k=4] [6.7 MB]
Creating vector for yes... [2.0 MB]
Allocating iteration vectors... [2 x 2.0 MB]
TOTAL: [12.6 MB]

Starting iterations...

Iterative method: 100 iterations in 0.65 seconds (average 0.002320, setup 0.42)

Value in the initial state: 0.0014058118779234103

Time for model checking: 1.228 seconds.

Result: 0.0014058118779234103 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmin=? [ F "MissionTimeUp" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=5,Objy1=2,Objx2=6,Objy2=3

Prob0E: 92 iterations in 0.71 seconds (average 0.007739, setup 0.00)

Prob1A: 3 iterations in 0.04 seconds (average 0.012000, setup 0.00)

yes = 300, no = 149217, maybe = 106774

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=256291, nc=192660, nnz=549199, k=4] [6.7 MB]
Creating vector for yes... [2.0 MB]
Allocating iteration vectors... [2 x 2.0 MB]
TOTAL: [12.6 MB]

Starting iterations...

Iterative method: 114 iterations in 0.73 seconds (average 0.003789, setup 0.30)

Value in the initial state: 4.487830895309778E-6

Time for model checking: 1.494 seconds.

Result: 4.487830895309778E-6 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmax=? [ F "Crash" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=5,Objy1=2,Objx2=6,Objy2=3

Prob0A: 3 iterations in 0.11 seconds (average 0.036000, setup 0.00)

Prob1E: 204 iterations in 2.26 seconds (average 0.011078, setup 0.00)

yes = 125025, no = 2830, maybe = 128436

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=256291, nc=226321, nnz=619320, k=4] [7.5 MB]
Creating vector for yes... [2.0 MB]
Allocating iteration vectors... [2 x 2.0 MB]
TOTAL: [13.4 MB]

Starting iterations...

Iterative method: 96 iterations in 0.54 seconds (average 0.002250, setup 0.33)

Value in the initial state: 0.03324820586534793

Time for model checking: 2.948 seconds.

Result: 0.03324820586534793 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmin=? [ F "Crash" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=5,Objy1=2,Objx2=6,Objy2=3

Prob0E: 3 iterations in 0.04 seconds (average 0.012000, setup 0.00)

Prob1A: 53 iterations in 0.35 seconds (average 0.006566, setup 0.00)

yes = 125025, no = 2830, maybe = 128436

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=256291, nc=226321, nnz=619320, k=4] [7.5 MB]
Creating vector for yes... [2.0 MB]
Allocating iteration vectors... [2 x 2.0 MB]
TOTAL: [13.4 MB]

Starting iterations...

Iterative method: 102 iterations in 0.49 seconds (average 0.002353, setup 0.25)

Value in the initial state: 0.01885242111217371

Time for model checking: 0.894 seconds.

Result: 0.01885242111217371 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmax=? [ F "AllAreaSearched" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=5,Objy1=2,Objx2=6,Objy2=3

Prob0A: 57 iterations in 0.29 seconds (average 0.005053, setup 0.00)

Prob1E: 4 iterations in 0.03 seconds (average 0.008000, setup 0.00)

yes = 1480, no = 191755, maybe = 63056

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=256291, nc=107419, nnz=304631, k=2] [3.8 MB]
Creating vector for yes... [2.0 MB]
Allocating iteration vectors... [2 x 2.0 MB]
TOTAL: [9.7 MB]

Starting iterations...

Iterative method: 92 iterations in 0.37 seconds (average 0.001870, setup 0.20)

Value in the initial state: 0.2562240402827359

Time for model checking: 0.707 seconds.

Result: 0.2562240402827359 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmin=? [ F "AllAreaSearched" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=5,Objy1=2,Objx2=6,Objy2=3

Prob0E: 65 iterations in 0.34 seconds (average 0.005231, setup 0.00)

Prob1A: 3 iterations in 0.03 seconds (average 0.009333, setup 0.00)

yes = 1480, no = 200338, maybe = 54473

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=256291, nc=91455, nnz=256750, k=2] [3.3 MB]
Creating vector for yes... [2.0 MB]
Allocating iteration vectors... [2 x 2.0 MB]
TOTAL: [9.1 MB]

Starting iterations...

Iterative method: 99 iterations in 0.32 seconds (average 0.001778, setup 0.14)

Value in the initial state: 0.0017432088142585497

Time for model checking: 0.695 seconds.

Result: 0.0017432088142585497 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmax=? [ F "MissionTimeUp"|"AllAreaSearched"|"Crash" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=5,Objy1=2,Objx2=6,Objy2=3

Prob0A: 3 iterations in 0.06 seconds (average 0.020000, setup 0.00)

Prob1E: 208 iterations in 1.97 seconds (average 0.009462, setup 0.00)

yes = 217063, no = 1050, maybe = 38178

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=256291, nc=68836, nnz=202190, k=2] [2.6 MB]
Creating vector for yes... [2.0 MB]
Allocating iteration vectors... [2 x 2.0 MB]
TOTAL: [8.5 MB]

Starting iterations...

Iterative method: 87 iterations in 0.31 seconds (average 0.001563, setup 0.17)

Value in the initial state: 0.2864111226416261

Time for model checking: 2.363 seconds.

Result: 0.2864111226416261 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmin=? [ F "MissionTimeUp"|"AllAreaSearched"|"Crash" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=5,Objy1=2,Objx2=6,Objy2=3

Prob0E: 3 iterations in 0.04 seconds (average 0.014667, setup 0.00)

Prob1A: 53 iterations in 0.37 seconds (average 0.006943, setup 0.00)

yes = 210576, no = 1050, maybe = 44665

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=256291, nc=81011, nnz=238006, k=2] [3.0 MB]
Creating vector for yes... [2.0 MB]
Allocating iteration vectors... [2 x 2.0 MB]
TOTAL: [8.9 MB]

Starting iterations...

Iterative method: 99 iterations in 0.34 seconds (average 0.001737, setup 0.16)

Value in the initial state: 0.020971960561887867

Time for model checking: 0.768 seconds.

Result: 0.020971960561887867 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"drop"}max=? [ F "deadlock" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=5,Objy1=2,Objx2=6,Objy2=3

Prob0E: 4 iterations in 0.06 seconds (average 0.014000, setup 0.00)

Prob1A: 1 iterations in 0.01 seconds (average 0.008000, setup 0.00)

goal = 1, inf = 0, maybe = 256290

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=256291, nc=354175, nnz=747174, k=4] [10.9 MB]
Building sparse matrix (transition rewards)... [n=256291, nc=354175, nnz=0, k=4] [2.3 MB]
Creating vector for state rewards... [2.0 MB]
Creating vector for inf... [2.0 MB]
Allocating iteration vectors... [2 x 2.0 MB]
TOTAL: [21.0 MB]

Starting iterations...

Iterative method: 99 iterations in 0.79 seconds (average 0.003677, setup 0.42)

Value in the initial state: 0.2830183955068925

Time for model checking: 0.851 seconds.

Result: 0.2830183955068925 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"drop"}min=? [ F "deadlock" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=5,Objy1=2,Objx2=6,Objy2=3

Prob0A: 4 iterations in 0.04 seconds (average 0.009000, setup 0.00)

Prob1E: 5 iterations in 0.04 seconds (average 0.008800, setup 0.00)

Warning: PRISM hasn't checked for zero-reward loops.
Your minimum rewards may be too low...

goal = 1, inf = 0, maybe = 256290

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=256291, nc=354175, nnz=747174, k=4] [10.9 MB]
Building sparse matrix (transition rewards)... [n=256291, nc=354175, nnz=0, k=4] [2.3 MB]
Creating vector for state rewards... [2.0 MB]
Creating vector for inf... [2.0 MB]
Allocating iteration vectors... [2 x 2.0 MB]
TOTAL: [21.0 MB]

Starting iterations...

Iterative method: 87 iterations in 0.66 seconds (average 0.003678, setup 0.34)

Value in the initial state: 0.2277424257209819

Time for model checking: 0.756 seconds.

Result: 0.2277424257209819 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"charge"}max=? [ F "deadlock" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=5,Objy1=2,Objx2=6,Objy2=3

Prob0E: 4 iterations in 0.02 seconds (average 0.005000, setup 0.00)

Prob1A: 1 iterations in 0.01 seconds (average 0.008000, setup 0.00)

goal = 1, inf = 0, maybe = 256290

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=256291, nc=354175, nnz=747174, k=4] [10.9 MB]
Building sparse matrix (transition rewards)... [n=256291, nc=354175, nnz=9805, k=4] [2.4 MB]
Creating vector for state rewards... [2.0 MB]
Creating vector for inf... [2.0 MB]
Allocating iteration vectors... [2 x 2.0 MB]
TOTAL: [21.1 MB]

Starting iterations...

Iterative method: 93 iterations in 0.67 seconds (average 0.003656, setup 0.33)

Value in the initial state: 0.3315573983472689

Time for model checking: 0.721 seconds.

Result: 0.3315573983472689 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"charge"}min=? [ F "deadlock" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=5,Objy1=2,Objx2=6,Objy2=3

Prob0A: 4 iterations in 0.02 seconds (average 0.006000, setup 0.00)

Prob1E: 5 iterations in 0.05 seconds (average 0.009600, setup 0.00)

Warning: PRISM hasn't checked for zero-reward loops.
Your minimum rewards may be too low...

goal = 1, inf = 0, maybe = 256290

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=256291, nc=354175, nnz=747174, k=4] [10.9 MB]
Building sparse matrix (transition rewards)... [n=256291, nc=354175, nnz=9805, k=4] [2.4 MB]
Creating vector for state rewards... [2.0 MB]
Creating vector for inf... [2.0 MB]
Allocating iteration vectors... [2 x 2.0 MB]
TOTAL: [21.1 MB]

Starting iterations...

Iterative method: 102 iterations in 0.80 seconds (average 0.004431, setup 0.34)

Value in the initial state: 0.0798941961060153

Time for model checking: 0.882 seconds.

Result: 0.0798941961060153 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"time"}max=? [ F "deadlock" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=5,Objy1=2,Objx2=6,Objy2=3

Prob0E: 4 iterations in 0.04 seconds (average 0.010000, setup 0.00)

Prob1A: 1 iterations in 0.01 seconds (average 0.008000, setup 0.00)

goal = 1, inf = 0, maybe = 256290

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=256291, nc=354175, nnz=747174, k=4] [10.9 MB]
Building sparse matrix (transition rewards)... [n=256291, nc=354175, nnz=127853, k=4] [3.8 MB]
Creating vector for state rewards... [2.0 MB]
Creating vector for inf... [2.0 MB]
Allocating iteration vectors... [2 x 2.0 MB]
TOTAL: [22.5 MB]

Starting iterations...

Iterative method: 99 iterations in 0.84 seconds (average 0.003960, setup 0.45)

Value in the initial state: 183.29177684428896

Time for model checking: 0.905 seconds.

Result: 183.29177684428896 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"time"}min=? [ F "deadlock" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=5,Objy1=2,Objx2=6,Objy2=3

Prob0A: 4 iterations in 0.02 seconds (average 0.005000, setup 0.00)

Prob1E: 5 iterations in 0.04 seconds (average 0.007200, setup 0.00)

Warning: PRISM hasn't checked for zero-reward loops.
Your minimum rewards may be too low...

goal = 1, inf = 0, maybe = 256290

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=256291, nc=354175, nnz=747174, k=4] [10.9 MB]
Building sparse matrix (transition rewards)... [n=256291, nc=354175, nnz=127853, k=4] [3.8 MB]
Creating vector for state rewards... [2.0 MB]
Creating vector for inf... [2.0 MB]
Allocating iteration vectors... [2 x 2.0 MB]
TOTAL: [22.5 MB]

Starting iterations...

Iterative method: 107 iterations in 0.79 seconds (average 0.003813, setup 0.38)

Value in the initial state: 104.41399241189973

Time for model checking: 0.859 seconds.

Result: 104.41399241189973 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"success"}max=? [ F "deadlock" ]/Pmin=? [ F "MissionSuccessful" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=5,Objy1=2,Objx2=6,Objy2=3

Prob0E: 4 iterations in 0.02 seconds (average 0.006000, setup 0.00)

Prob1A: 1 iterations in 0.00 seconds (average 0.004000, setup 0.00)

goal = 1, inf = 0, maybe = 256290

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=256291, nc=354175, nnz=747174, k=4] [10.9 MB]
Building sparse matrix (transition rewards)... [n=256291, nc=354175, nnz=2066, k=4] [2.4 MB]
Creating vector for state rewards... [2.0 MB]
Creating vector for inf... [2.0 MB]
Allocating iteration vectors... [2 x 2.0 MB]
TOTAL: [21.1 MB]

Starting iterations...

Iterative method: 101 iterations in 0.68 seconds (average 0.003406, setup 0.34)

Prob0E: 53 iterations in 0.29 seconds (average 0.005434, setup 0.00)

Prob1A: 3 iterations in 0.04 seconds (average 0.013333, setup 0.00)

yes = 1049, no = 217064, maybe = 38178

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=256291, nc=68836, nnz=202190, k=2] [2.6 MB]
Creating vector for yes... [2.0 MB]
Allocating iteration vectors... [2 x 2.0 MB]
TOTAL: [8.5 MB]

Starting iterations...

Iterative method: 94 iterations in 0.29 seconds (average 0.001532, setup 0.15)

Value in the initial state: 251.35202542240913

Time for model checking: 1.371 seconds.

Result: 251.35202542240913 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"success"}min=? [ F "deadlock" ]/Pmax=? [ F "MissionSuccessful" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=5,Objy1=2,Objx2=6,Objy2=3

Prob0A: 4 iterations in 0.06 seconds (average 0.014000, setup 0.00)

Prob1E: 5 iterations in 0.04 seconds (average 0.007200, setup 0.00)

Warning: PRISM hasn't checked for zero-reward loops.
Your minimum rewards may be too low...

goal = 1, inf = 0, maybe = 256290

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=256291, nc=354175, nnz=747174, k=4] [10.9 MB]
Building sparse matrix (transition rewards)... [n=256291, nc=354175, nnz=2066, k=4] [2.4 MB]
Creating vector for state rewards... [2.0 MB]
Creating vector for inf... [2.0 MB]
Allocating iteration vectors... [2 x 2.0 MB]
TOTAL: [21.1 MB]

Starting iterations...

Iterative method: 101 iterations in 0.70 seconds (average 0.003525, setup 0.35)

Prob0A: 53 iterations in 0.25 seconds (average 0.004755, setup 0.00)

Prob1E: 4 iterations in 0.02 seconds (average 0.006000, setup 0.00)

yes = 1049, no = 210577, maybe = 44665

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=256291, nc=81011, nnz=238006, k=2] [3.0 MB]
Creating vector for yes... [2.0 MB]
Allocating iteration vectors... [2 x 2.0 MB]
TOTAL: [8.9 MB]

Starting iterations...

Iterative method: 103 iterations in 0.32 seconds (average 0.001592, setup 0.15)

Value in the initial state: 78.39396878936721

Time for model checking: 1.399 seconds.

Result: 78.39396878936721 (value in the initial state)

---------------------------------------------------------------------

Note: There were 16 warnings during computation.

