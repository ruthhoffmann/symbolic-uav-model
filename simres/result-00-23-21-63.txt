PRISM
=====

Version: 4.3
Date: Wed Aug 03 15:57:21 BST 2016
Hostname: eglinton
Memory limits: cudd=1g, java(heap)=981.5m
Command line: prism uav_sim_mdp.prism -s uav_mdp.props -const 'Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=2,Objy1=1,Objx2=6,Objy2=3,'

Parsing model file "uav_sim_mdp.prism"...

Parsing properties file "uav_mdp.props"...

18 properties:
(1) Pmax=? [ F "MissionSuccessful" ]
(2) Pmin=? [ F "MissionSuccessful" ]
(3) Pmax=? [ F "MissionTimeUp" ]
(4) Pmin=? [ F "MissionTimeUp" ]
(5) Pmax=? [ F "Crash" ]
(6) Pmin=? [ F "Crash" ]
(7) Pmax=? [ F "AllAreaSearched" ]
(8) Pmin=? [ F "AllAreaSearched" ]
(9) Pmax=? [ F "MissionTimeUp"|"AllAreaSearched"|"Crash" ]
(10) Pmin=? [ F "MissionTimeUp"|"AllAreaSearched"|"Crash" ]
(11) R{"drop"}max=? [ F "deadlock" ]
(12) R{"drop"}min=? [ F "deadlock" ]
(13) R{"charge"}max=? [ F "deadlock" ]
(14) R{"charge"}min=? [ F "deadlock" ]
(15) R{"time"}max=? [ F "deadlock" ]
(16) R{"time"}min=? [ F "deadlock" ]
(17) R{"success"}max=? [ F "deadlock" ]/Pmin=? [ F "MissionSuccessful" ]
(18) R{"success"}min=? [ F "deadlock" ]/Pmax=? [ F "MissionSuccessful" ]

Type:        MDP
Modules:     Obj Move Robot T 
Variables:   NoOfObjs posx posy s1 retx rety b t c 

---------------------------------------------------------------------

Model checking: Pmax=? [ F "MissionSuccessful" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=2,Objy1=1,Objx2=6,Objy2=3

Building model...
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=2,Objy1=1,Objx2=6,Objy2=3

Warning: Guard for command 7 of module "Move" is never satisfied.

Warning: Guard for command 8 of module "Move" is never satisfied.

Warning: Guard for command 9 of module "Move" is never satisfied.

Warning: Guard for command 7 of module "Robot" is never satisfied.

Warning: Update 2 of command 3 of module "Robot" doesn't do anything ("(s1'=13)", line 210, column 44)

Warning: Update 2 of command 27 of module "T" doesn't do anything ("(c'=1)", line 323, column 108)

Warning: Guard for command 28 of module "T" is never satisfied.

Warning: Update 2 of command 9 of module "T" doesn't do anything ("(c'=1)", line 300, column 112)

Warning: Guard for command 10 of module "T" is never satisfied.

Warning: Update 2 of command 51 of module "T" doesn't do anything ("(c'=1)", line 354, column 113)

Warning: Guard for command 52 of module "T" is never satisfied.

Computing reachable states...

Reachability (BFS): 89 iterations in 1.56 seconds (average 0.017483, setup 0.00)

Time for model construction: 3.719 seconds.

Warning: Deadlocks detected and fixed in 1 states

Type:        MDP
States:      311609 (1 initial)
Transitions: 865007
Choices:     420464

Transition matrix: 222590 nodes (97 terminal), 865007 minterms, vars: 34r/34c/18nd

Prob0A: 52 iterations in 0.53 seconds (average 0.010154, setup 0.00)

Prob1E: 4 iterations in 0.09 seconds (average 0.022000, setup 0.00)

yes = 1247, no = 259370, maybe = 50992

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=311609, nc=89746, nnz=258935, k=2] [3.3 MB]
Creating vector for yes... [2.4 MB]
Allocating iteration vectors... [2 x 2.4 MB]
TOTAL: [10.5 MB]

Starting iterations...

Iterative method: 103 iterations in 0.52 seconds (average 0.002252, setup 0.29)

Value in the initial state: 0.9805642541768546

Time for model checking: 1.176 seconds.

Result: 0.9805642541768546 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmin=? [ F "MissionSuccessful" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=2,Objy1=1,Objx2=6,Objy2=3

Prob0E: 53 iterations in 0.49 seconds (average 0.009283, setup 0.00)

Prob1A: 3 iterations in 0.06 seconds (average 0.018667, setup 0.00)

yes = 1247, no = 265906, maybe = 44456

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=311609, nc=78016, nnz=225109, k=2] [2.9 MB]
Creating vector for yes... [2.4 MB]
Allocating iteration vectors... [2 x 2.4 MB]
TOTAL: [10.1 MB]

Starting iterations...

Iterative method: 94 iterations in 0.40 seconds (average 0.002213, setup 0.20)

Value in the initial state: 0.8009228930782568

Time for model checking: 0.976 seconds.

Result: 0.8009228930782568 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmax=? [ F "MissionTimeUp" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=2,Objy1=1,Objx2=6,Objy2=3

Prob0A: 73 iterations in 0.74 seconds (average 0.010082, setup 0.00)

Prob1E: 4 iterations in 0.05 seconds (average 0.012000, setup 0.00)

yes = 424, no = 180968, maybe = 130217

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=311609, nc=225150, nnz=626488, k=4] [7.7 MB]
Creating vector for yes... [2.4 MB]
Allocating iteration vectors... [2 x 2.4 MB]
TOTAL: [14.8 MB]

Starting iterations...

Iterative method: 100 iterations in 0.67 seconds (average 0.002880, setup 0.38)

Value in the initial state: 6.818999770271693E-4

Time for model checking: 1.448 seconds.

Result: 6.818999770271693E-4 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmin=? [ F "MissionTimeUp" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=2,Objy1=1,Objx2=6,Objy2=3

Prob0E: 88 iterations in 1.00 seconds (average 0.011364, setup 0.00)

Prob1A: 3 iterations in 0.06 seconds (average 0.018667, setup 0.00)

yes = 424, no = 181362, maybe = 129823

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=311609, nc=224443, nnz=625132, k=4] [7.7 MB]
Creating vector for yes... [2.4 MB]
Allocating iteration vectors... [2 x 2.4 MB]
TOTAL: [14.8 MB]

Starting iterations...

Iterative method: 115 iterations in 0.70 seconds (average 0.002887, setup 0.37)

Value in the initial state: 1.9735637519897605E-6

Time for model checking: 1.793 seconds.

Result: 1.9735637519897605E-6 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmax=? [ F "Crash" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=2,Objy1=1,Objx2=6,Objy2=3

Prob0A: 3 iterations in 0.12 seconds (average 0.040000, setup 0.00)

Prob1E: 196 iterations in 3.35 seconds (average 0.017082, setup 0.00)

yes = 152397, no = 3416, maybe = 155796

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=311609, nc=264651, nnz=709194, k=4] [8.7 MB]
Creating vector for yes... [2.4 MB]
Allocating iteration vectors... [2 x 2.4 MB]
TOTAL: [15.8 MB]

Starting iterations...

Iterative method: 95 iterations in 0.73 seconds (average 0.003032, setup 0.44)

Value in the initial state: 0.0320145088531841

Time for model checking: 4.228 seconds.

Result: 0.0320145088531841 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmin=? [ F "Crash" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=2,Objy1=1,Objx2=6,Objy2=3

Prob0E: 4 iterations in 0.10 seconds (average 0.026000, setup 0.00)

Prob1A: 51 iterations in 0.71 seconds (average 0.013961, setup 0.00)

yes = 152397, no = 3416, maybe = 155796

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=311609, nc=264651, nnz=709194, k=4] [8.7 MB]
Creating vector for yes... [2.4 MB]
Allocating iteration vectors... [2 x 2.4 MB]
TOTAL: [15.8 MB]

Starting iterations...

Iterative method: 93 iterations in 0.62 seconds (average 0.003097, setup 0.34)

Value in the initial state: 0.017284096088736708

Time for model checking: 1.478 seconds.

Result: 0.017284096088736708 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmax=? [ F "AllAreaSearched" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=2,Objy1=1,Objx2=6,Objy2=3

Prob0A: 54 iterations in 0.54 seconds (average 0.009926, setup 0.00)

Prob1E: 4 iterations in 0.06 seconds (average 0.015000, setup 0.00)

yes = 1744, no = 232509, maybe = 77356

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=311609, nc=128100, nnz=358061, k=2] [4.5 MB]
Creating vector for yes... [2.4 MB]
Allocating iteration vectors... [2 x 2.4 MB]
TOTAL: [11.6 MB]

Starting iterations...

Iterative method: 92 iterations in 0.49 seconds (average 0.002522, setup 0.26)

Value in the initial state: 0.17172316619771905

Time for model checking: 1.113 seconds.

Result: 0.17172316619771905 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmin=? [ F "AllAreaSearched" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=2,Objy1=1,Objx2=6,Objy2=3

Prob0E: 64 iterations in 0.58 seconds (average 0.009000, setup 0.00)

Prob1A: 3 iterations in 0.05 seconds (average 0.016000, setup 0.00)

yes = 1744, no = 241770, maybe = 68095

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=311609, nc=111778, nnz=310596, k=2] [4.0 MB]
Creating vector for yes... [2.4 MB]
Allocating iteration vectors... [2 x 2.4 MB]
TOTAL: [11.1 MB]

Starting iterations...

Iterative method: 98 iterations in 0.44 seconds (average 0.002408, setup 0.20)

Value in the initial state: 0.0017515813468305724

Time for model checking: 1.075 seconds.

Result: 0.0017515813468305724 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmax=? [ F "MissionTimeUp"|"AllAreaSearched"|"Crash" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=2,Objy1=1,Objx2=6,Objy2=3

Prob0A: 3 iterations in 0.09 seconds (average 0.029333, setup 0.00)

Prob1E: 204 iterations in 2.90 seconds (average 0.014216, setup 0.00)

yes = 265905, no = 1248, maybe = 44456

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=311609, nc=78016, nnz=225109, k=2] [2.9 MB]
Creating vector for yes... [2.4 MB]
Allocating iteration vectors... [2 x 2.4 MB]
TOTAL: [10.1 MB]

Starting iterations...

Iterative method: 88 iterations in 0.43 seconds (average 0.002182, setup 0.24)

Value in the initial state: 0.1990768673801404

Time for model checking: 3.456 seconds.

Result: 0.1990768673801404 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmin=? [ F "MissionTimeUp"|"AllAreaSearched"|"Crash" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=2,Objy1=1,Objx2=6,Objy2=3

Prob0E: 4 iterations in 0.07 seconds (average 0.018000, setup 0.00)

Prob1A: 52 iterations in 0.56 seconds (average 0.010769, setup 0.00)

yes = 259369, no = 1248, maybe = 50992

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=311609, nc=89746, nnz=258935, k=2] [3.3 MB]
Creating vector for yes... [2.4 MB]
Allocating iteration vectors... [2 x 2.4 MB]
TOTAL: [10.5 MB]

Starting iterations...

Iterative method: 99 iterations in 0.44 seconds (average 0.002182, setup 0.22)

Value in the initial state: 0.01943512532353175

Time for model checking: 1.102 seconds.

Result: 0.01943512532353175 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"drop"}max=? [ F "deadlock" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=2,Objy1=1,Objx2=6,Objy2=3

Prob0E: 5 iterations in 0.09 seconds (average 0.017600, setup 0.00)

Prob1A: 1 iterations in 0.01 seconds (average 0.008000, setup 0.00)

goal = 1, inf = 0, maybe = 311608

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=311609, nc=420463, nnz=865006, k=4] [12.7 MB]
Building sparse matrix (transition rewards)... [n=311609, nc=420463, nnz=0, k=4] [2.8 MB]
Creating vector for state rewards... [2.4 MB]
Creating vector for inf... [2.4 MB]
Allocating iteration vectors... [2 x 2.4 MB]
TOTAL: [25.0 MB]

Starting iterations...

Iterative method: 97 iterations in 0.98 seconds (average 0.004495, setup 0.54)

Value in the initial state: 0.25394670166603545

Time for model checking: 1.099 seconds.

Result: 0.25394670166603545 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"drop"}min=? [ F "deadlock" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=2,Objy1=1,Objx2=6,Objy2=3

Prob0A: 4 iterations in 0.04 seconds (average 0.010000, setup 0.00)

Prob1E: 5 iterations in 0.06 seconds (average 0.012000, setup 0.00)

Warning: PRISM hasn't checked for zero-reward loops.
Your minimum rewards may be too low...

goal = 1, inf = 0, maybe = 311608

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=311609, nc=420463, nnz=865006, k=4] [12.7 MB]
Building sparse matrix (transition rewards)... [n=311609, nc=420463, nnz=0, k=4] [2.8 MB]
Creating vector for state rewards... [2.4 MB]
Creating vector for inf... [2.4 MB]
Allocating iteration vectors... [2 x 2.4 MB]
TOTAL: [25.0 MB]

Starting iterations...

Iterative method: 87 iterations in 0.83 seconds (average 0.004690, setup 0.42)

Value in the initial state: 0.20566240166870262

Time for model checking: 0.95 seconds.

Result: 0.20566240166870262 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"charge"}max=? [ F "deadlock" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=2,Objy1=1,Objx2=6,Objy2=3

Prob0E: 5 iterations in 0.04 seconds (average 0.008000, setup 0.00)

Prob1A: 1 iterations in 0.01 seconds (average 0.008000, setup 0.00)

goal = 1, inf = 0, maybe = 311608

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=311609, nc=420463, nnz=865006, k=4] [12.7 MB]
Building sparse matrix (transition rewards)... [n=311609, nc=420463, nnz=10618, k=4] [2.9 MB]
Creating vector for state rewards... [2.4 MB]
Creating vector for inf... [2.4 MB]
Allocating iteration vectors... [2 x 2.4 MB]
TOTAL: [25.1 MB]

Starting iterations...

Iterative method: 93 iterations in 0.84 seconds (average 0.004516, setup 0.42)

Value in the initial state: 0.3114505689915618

Time for model checking: 0.918 seconds.

Result: 0.3114505689915618 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"charge"}min=? [ F "deadlock" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=2,Objy1=1,Objx2=6,Objy2=3

Prob0A: 4 iterations in 0.03 seconds (average 0.007000, setup 0.00)

Prob1E: 5 iterations in 0.05 seconds (average 0.010400, setup 0.00)

Warning: PRISM hasn't checked for zero-reward loops.
Your minimum rewards may be too low...

goal = 1, inf = 0, maybe = 311608

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=311609, nc=420463, nnz=865006, k=4] [12.7 MB]
Building sparse matrix (transition rewards)... [n=311609, nc=420463, nnz=10618, k=4] [2.9 MB]
Creating vector for state rewards... [2.4 MB]
Creating vector for inf... [2.4 MB]
Allocating iteration vectors... [2 x 2.4 MB]
TOTAL: [25.1 MB]

Starting iterations...

Iterative method: 103 iterations in 0.91 seconds (average 0.004699, setup 0.42)

Value in the initial state: 0.060899091697254354

Time for model checking: 1.005 seconds.

Result: 0.060899091697254354 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"time"}max=? [ F "deadlock" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=2,Objy1=1,Objx2=6,Objy2=3

Prob0E: 5 iterations in 0.04 seconds (average 0.007200, setup 0.00)

Prob1A: 1 iterations in 0.01 seconds (average 0.008000, setup 0.00)

goal = 1, inf = 0, maybe = 311608

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=311609, nc=420463, nnz=865006, k=4] [12.7 MB]
Building sparse matrix (transition rewards)... [n=311609, nc=420463, nnz=155811, k=4] [4.6 MB]
Creating vector for state rewards... [2.4 MB]
Creating vector for inf... [2.4 MB]
Allocating iteration vectors... [2 x 2.4 MB]
TOTAL: [26.8 MB]

Starting iterations...

Iterative method: 98 iterations in 0.92 seconds (average 0.004571, setup 0.47)

Value in the initial state: 176.5711371211727

Time for model checking: 0.985 seconds.

Result: 176.5711371211727 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"time"}min=? [ F "deadlock" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=2,Objy1=1,Objx2=6,Objy2=3

Prob0A: 4 iterations in 0.03 seconds (average 0.007000, setup 0.00)

Prob1E: 5 iterations in 0.04 seconds (average 0.008800, setup 0.00)

Warning: PRISM hasn't checked for zero-reward loops.
Your minimum rewards may be too low...

goal = 1, inf = 0, maybe = 311608

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=311609, nc=420463, nnz=865006, k=4] [12.7 MB]
Building sparse matrix (transition rewards)... [n=311609, nc=420463, nnz=155811, k=4] [4.6 MB]
Creating vector for state rewards... [2.4 MB]
Creating vector for inf... [2.4 MB]
Allocating iteration vectors... [2 x 2.4 MB]
TOTAL: [26.8 MB]

Starting iterations...

Iterative method: 107 iterations in 0.97 seconds (average 0.004785, setup 0.46)

Value in the initial state: 95.77664482642363

Time for model checking: 1.052 seconds.

Result: 95.77664482642363 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"success"}max=? [ F "deadlock" ]/Pmin=? [ F "MissionSuccessful" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=2,Objy1=1,Objx2=6,Objy2=3

Prob0E: 5 iterations in 0.03 seconds (average 0.006400, setup 0.00)

Prob1A: 1 iterations in 0.00 seconds (average 0.004000, setup 0.00)

goal = 1, inf = 0, maybe = 311608

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=311609, nc=420463, nnz=865006, k=4] [12.7 MB]
Building sparse matrix (transition rewards)... [n=311609, nc=420463, nnz=2456, k=4] [2.8 MB]
Creating vector for state rewards... [2.4 MB]
Creating vector for inf... [2.4 MB]
Allocating iteration vectors... [2 x 2.4 MB]
TOTAL: [25.0 MB]

Starting iterations...

Iterative method: 100 iterations in 0.86 seconds (average 0.004560, setup 0.40)

Prob0E: 53 iterations in 0.39 seconds (average 0.007321, setup 0.00)

Prob1A: 3 iterations in 0.06 seconds (average 0.018667, setup 0.00)

yes = 1247, no = 265906, maybe = 44456

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=311609, nc=78016, nnz=225109, k=2] [2.9 MB]
Creating vector for yes... [2.4 MB]
Allocating iteration vectors... [2 x 2.4 MB]
TOTAL: [10.1 MB]

Starting iterations...

Iterative method: 94 iterations in 0.36 seconds (average 0.001957, setup 0.18)

Value in the initial state: 215.9173549276974

Time for model checking: 1.752 seconds.

Result: 215.9173549276974 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"success"}min=? [ F "deadlock" ]/Pmax=? [ F "MissionSuccessful" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=2,Objy1=1,Objx2=6,Objy2=3

Prob0A: 4 iterations in 0.09 seconds (average 0.022000, setup 0.00)

Prob1E: 5 iterations in 0.05 seconds (average 0.010400, setup 0.00)

Warning: PRISM hasn't checked for zero-reward loops.
Your minimum rewards may be too low...

goal = 1, inf = 0, maybe = 311608

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=311609, nc=420463, nnz=865006, k=4] [12.7 MB]
Building sparse matrix (transition rewards)... [n=311609, nc=420463, nnz=2456, k=4] [2.8 MB]
Creating vector for state rewards... [2.4 MB]
Creating vector for inf... [2.4 MB]
Allocating iteration vectors... [2 x 2.4 MB]
TOTAL: [25.0 MB]

Starting iterations...

Iterative method: 100 iterations in 0.85 seconds (average 0.004240, setup 0.43)

Prob0A: 52 iterations in 0.28 seconds (average 0.005462, setup 0.00)

Prob1E: 4 iterations in 0.04 seconds (average 0.011000, setup 0.00)

yes = 1247, no = 259370, maybe = 50992

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=311609, nc=89746, nnz=258935, k=2] [3.3 MB]
Creating vector for yes... [2.4 MB]
Allocating iteration vectors... [2 x 2.4 MB]
TOTAL: [10.5 MB]

Starting iterations...

Iterative method: 103 iterations in 0.36 seconds (average 0.001903, setup 0.17)

Value in the initial state: 78.69082411397858

Time for model checking: 1.717 seconds.

Result: 78.69082411397858 (value in the initial state)

---------------------------------------------------------------------

Note: There were 16 warnings during computation.

