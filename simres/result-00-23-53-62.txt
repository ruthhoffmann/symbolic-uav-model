PRISM
=====

Version: 4.3
Date: Wed Aug 03 16:04:37 BST 2016
Hostname: eglinton
Memory limits: cudd=1g, java(heap)=981.5m
Command line: prism uav_sim_mdp.prism -s uav_mdp.props -const 'Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=5,Objy1=3,Objx2=6,Objy2=2,'

Parsing model file "uav_sim_mdp.prism"...

Parsing properties file "uav_mdp.props"...

18 properties:
(1) Pmax=? [ F "MissionSuccessful" ]
(2) Pmin=? [ F "MissionSuccessful" ]
(3) Pmax=? [ F "MissionTimeUp" ]
(4) Pmin=? [ F "MissionTimeUp" ]
(5) Pmax=? [ F "Crash" ]
(6) Pmin=? [ F "Crash" ]
(7) Pmax=? [ F "AllAreaSearched" ]
(8) Pmin=? [ F "AllAreaSearched" ]
(9) Pmax=? [ F "MissionTimeUp"|"AllAreaSearched"|"Crash" ]
(10) Pmin=? [ F "MissionTimeUp"|"AllAreaSearched"|"Crash" ]
(11) R{"drop"}max=? [ F "deadlock" ]
(12) R{"drop"}min=? [ F "deadlock" ]
(13) R{"charge"}max=? [ F "deadlock" ]
(14) R{"charge"}min=? [ F "deadlock" ]
(15) R{"time"}max=? [ F "deadlock" ]
(16) R{"time"}min=? [ F "deadlock" ]
(17) R{"success"}max=? [ F "deadlock" ]/Pmin=? [ F "MissionSuccessful" ]
(18) R{"success"}min=? [ F "deadlock" ]/Pmax=? [ F "MissionSuccessful" ]

Type:        MDP
Modules:     Obj Move Robot T 
Variables:   NoOfObjs posx posy s1 retx rety b t c 

---------------------------------------------------------------------

Model checking: Pmax=? [ F "MissionSuccessful" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=5,Objy1=3,Objx2=6,Objy2=2

Building model...
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=5,Objy1=3,Objx2=6,Objy2=2

Warning: Guard for command 7 of module "Move" is never satisfied.

Warning: Guard for command 8 of module "Move" is never satisfied.

Warning: Guard for command 9 of module "Move" is never satisfied.

Warning: Guard for command 7 of module "Robot" is never satisfied.

Warning: Update 2 of command 3 of module "Robot" doesn't do anything ("(s1'=13)", line 210, column 44)

Warning: Update 2 of command 27 of module "T" doesn't do anything ("(c'=1)", line 323, column 108)

Warning: Guard for command 28 of module "T" is never satisfied.

Warning: Update 2 of command 9 of module "T" doesn't do anything ("(c'=1)", line 300, column 112)

Warning: Guard for command 10 of module "T" is never satisfied.

Warning: Update 2 of command 51 of module "T" doesn't do anything ("(c'=1)", line 354, column 113)

Warning: Guard for command 52 of module "T" is never satisfied.

Computing reachable states...

Reachability (BFS): 90 iterations in 0.82 seconds (average 0.009156, setup 0.00)

Time for model construction: 2.202 seconds.

Warning: Deadlocks detected and fixed in 1 states

Type:        MDP
States:      260061 (1 initial)
Transitions: 759348
Choices:     360050

Transition matrix: 201751 nodes (93 terminal), 759348 minterms, vars: 34r/34c/18nd

Prob0A: 54 iterations in 0.28 seconds (average 0.005185, setup 0.00)

Prob1E: 4 iterations in 0.04 seconds (average 0.010000, setup 0.00)

yes = 1089, no = 213876, maybe = 45096

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=260061, nc=81598, nnz=239269, k=2] [3.1 MB]
Creating vector for yes... [2.0 MB]
Allocating iteration vectors... [2 x 2.0 MB]
TOTAL: [9.0 MB]

Starting iterations...

Iterative method: 103 iterations in 0.39 seconds (average 0.001631, setup 0.22)

Value in the initial state: 0.9789965080120338

Time for model checking: 0.744 seconds.

Result: 0.9789965080120338 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmin=? [ F "MissionSuccessful" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=5,Objy1=3,Objx2=6,Objy2=2

Prob0E: 54 iterations in 0.45 seconds (average 0.008370, setup 0.00)

Prob1A: 3 iterations in 0.03 seconds (average 0.009333, setup 0.00)

yes = 1089, no = 221465, maybe = 37507

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=260061, nc=67360, nnz=196989, k=2] [2.6 MB]
Creating vector for yes... [2.0 MB]
Allocating iteration vectors... [2 x 2.0 MB]
TOTAL: [8.5 MB]

Starting iterations...

Iterative method: 94 iterations in 0.29 seconds (average 0.001574, setup 0.14)

Value in the initial state: 0.722163064249138

Time for model checking: 0.795 seconds.

Result: 0.722163064249138 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmax=? [ F "MissionTimeUp" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=5,Objy1=3,Objx2=6,Objy2=2

Prob0A: 78 iterations in 0.34 seconds (average 0.004308, setup 0.00)

Prob1E: 4 iterations in 0.02 seconds (average 0.006000, setup 0.00)

yes = 272, no = 150585, maybe = 109204

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=260061, nc=196967, nnz=559600, k=4] [6.8 MB]
Creating vector for yes... [2.0 MB]
Allocating iteration vectors... [2 x 2.0 MB]
TOTAL: [12.8 MB]

Starting iterations...

Iterative method: 101 iterations in 0.48 seconds (average 0.002178, setup 0.26)

Value in the initial state: 0.0017590510798557936

Time for model checking: 0.852 seconds.

Result: 0.0017590510798557936 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmin=? [ F "MissionTimeUp" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=5,Objy1=3,Objx2=6,Objy2=2

Prob0E: 93 iterations in 0.57 seconds (average 0.006108, setup 0.00)

Prob1A: 3 iterations in 0.03 seconds (average 0.009333, setup 0.00)

yes = 272, no = 150951, maybe = 108838

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=260061, nc=196306, nnz=558372, k=4] [6.8 MB]
Creating vector for yes... [2.0 MB]
Allocating iteration vectors... [2 x 2.0 MB]
TOTAL: [12.8 MB]

Starting iterations...

Iterative method: 114 iterations in 0.50 seconds (average 0.002175, setup 0.25)

Value in the initial state: 4.828476246219376E-6

Time for model checking: 1.109 seconds.

Result: 4.828476246219376E-6 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmax=? [ F "Crash" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=5,Objy1=3,Objx2=6,Objy2=2

Prob0A: 3 iterations in 0.05 seconds (average 0.017333, setup 0.00)

Prob1E: 204 iterations in 1.68 seconds (average 0.008235, setup 0.00)

yes = 126898, no = 2896, maybe = 130267

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=260061, nc=230256, nnz=629554, k=4] [7.7 MB]
Creating vector for yes... [2.0 MB]
Allocating iteration vectors... [2 x 2.0 MB]
TOTAL: [13.6 MB]

Starting iterations...

Iterative method: 97 iterations in 0.52 seconds (average 0.002227, setup 0.30)

Value in the initial state: 0.03310535443691791

Time for model checking: 2.272 seconds.

Result: 0.03310535443691791 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmin=? [ F "Crash" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=5,Objy1=3,Objx2=6,Objy2=2

Prob0E: 4 iterations in 0.05 seconds (average 0.012000, setup 0.00)

Prob1A: 54 iterations in 0.30 seconds (average 0.005481, setup 0.00)

yes = 126898, no = 2896, maybe = 130267

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=260061, nc=230256, nnz=629554, k=4] [7.7 MB]
Creating vector for yes... [2.0 MB]
Allocating iteration vectors... [2 x 2.0 MB]
TOTAL: [13.6 MB]

Starting iterations...

Iterative method: 103 iterations in 0.49 seconds (average 0.002369, setup 0.25)

Value in the initial state: 0.018874533731804328

Time for model checking: 0.85 seconds.

Result: 0.018874533731804328 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmax=? [ F "AllAreaSearched" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=5,Objy1=3,Objx2=6,Objy2=2

Prob0A: 58 iterations in 0.27 seconds (average 0.004621, setup 0.00)

Prob1E: 4 iterations in 0.03 seconds (average 0.008000, setup 0.00)

yes = 1534, no = 197379, maybe = 61148

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=260061, nc=104463, nnz=294638, k=2] [3.7 MB]
Creating vector for yes... [2.0 MB]
Allocating iteration vectors... [2 x 2.0 MB]
TOTAL: [9.7 MB]

Starting iterations...

Iterative method: 93 iterations in 0.34 seconds (average 0.001763, setup 0.18)

Value in the initial state: 0.24723559271991516

Time for model checking: 0.664 seconds.

Result: 0.24723559271991516 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmin=? [ F "AllAreaSearched" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=5,Objy1=3,Objx2=6,Objy2=2

Prob0E: 65 iterations in 0.33 seconds (average 0.005108, setup 0.00)

Prob1A: 3 iterations in 0.03 seconds (average 0.009333, setup 0.00)

yes = 1534, no = 206697, maybe = 51830

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=260061, nc=87179, nnz=242312, k=2] [3.1 MB]
Creating vector for yes... [2.0 MB]
Allocating iteration vectors... [2 x 2.0 MB]
TOTAL: [9.1 MB]

Starting iterations...

Iterative method: 100 iterations in 0.30 seconds (average 0.001640, setup 0.13)

Value in the initial state: 0.0017695634066923814

Time for model checking: 0.638 seconds.

Result: 0.0017695634066923814 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmax=? [ F "MissionTimeUp"|"AllAreaSearched"|"Crash" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=5,Objy1=3,Objx2=6,Objy2=2

Prob0A: 3 iterations in 0.06 seconds (average 0.020000, setup 0.00)

Prob1E: 208 iterations in 1.78 seconds (average 0.008538, setup 0.00)

yes = 221464, no = 1090, maybe = 37507

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=260061, nc=67360, nnz=196989, k=2] [2.6 MB]
Creating vector for yes... [2.0 MB]
Allocating iteration vectors... [2 x 2.0 MB]
TOTAL: [8.5 MB]

Starting iterations...

Iterative method: 88 iterations in 0.30 seconds (average 0.001545, setup 0.16)

Value in the initial state: 0.27783658127546507

Time for model checking: 2.155 seconds.

Result: 0.27783658127546507 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmin=? [ F "MissionTimeUp"|"AllAreaSearched"|"Crash" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=5,Objy1=3,Objx2=6,Objy2=2

Prob0E: 4 iterations in 0.04 seconds (average 0.010000, setup 0.00)

Prob1A: 54 iterations in 0.30 seconds (average 0.005630, setup 0.00)

yes = 213875, no = 1090, maybe = 45096

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=260061, nc=81598, nnz=239269, k=2] [3.1 MB]
Creating vector for yes... [2.0 MB]
Allocating iteration vectors... [2 x 2.0 MB]
TOTAL: [9.0 MB]

Starting iterations...

Iterative method: 100 iterations in 0.31 seconds (average 0.001640, setup 0.15)

Value in the initial state: 0.021002853142475828

Time for model checking: 0.677 seconds.

Result: 0.021002853142475828 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"drop"}max=? [ F "deadlock" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=5,Objy1=3,Objx2=6,Objy2=2

Prob0E: 5 iterations in 0.04 seconds (average 0.008800, setup 0.00)

Prob1A: 1 iterations in 0.01 seconds (average 0.008000, setup 0.00)

goal = 1, inf = 0, maybe = 260060

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=260061, nc=360049, nnz=759347, k=4] [11.1 MB]
Building sparse matrix (transition rewards)... [n=260061, nc=360049, nnz=0, k=4] [2.4 MB]
Creating vector for state rewards... [2.0 MB]
Creating vector for inf... [2.0 MB]
Allocating iteration vectors... [2 x 2.0 MB]
TOTAL: [21.4 MB]

Starting iterations...

Iterative method: 100 iterations in 0.75 seconds (average 0.003480, setup 0.40)

Value in the initial state: 0.275825960483672

Time for model checking: 0.806 seconds.

Result: 0.275825960483672 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"drop"}min=? [ F "deadlock" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=5,Objy1=3,Objx2=6,Objy2=2

Prob0A: 4 iterations in 0.03 seconds (average 0.008000, setup 0.00)

Prob1E: 5 iterations in 0.03 seconds (average 0.006400, setup 0.00)

Warning: PRISM hasn't checked for zero-reward loops.
Your minimum rewards may be too low...

goal = 1, inf = 0, maybe = 260060

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=260061, nc=360049, nnz=759347, k=4] [11.1 MB]
Building sparse matrix (transition rewards)... [n=260061, nc=360049, nnz=0, k=4] [2.4 MB]
Creating vector for state rewards... [2.0 MB]
Creating vector for inf... [2.0 MB]
Allocating iteration vectors... [2 x 2.0 MB]
TOTAL: [21.4 MB]

Starting iterations...

Iterative method: 88 iterations in 0.64 seconds (average 0.003545, setup 0.33)

Value in the initial state: 0.2345619721450441

Time for model checking: 0.715 seconds.

Result: 0.2345619721450441 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"charge"}max=? [ F "deadlock" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=5,Objy1=3,Objx2=6,Objy2=2

Prob0E: 5 iterations in 0.02 seconds (average 0.004800, setup 0.00)

Prob1A: 1 iterations in 0.00 seconds (average 0.004000, setup 0.00)

goal = 1, inf = 0, maybe = 260060

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=260061, nc=360049, nnz=759347, k=4] [11.1 MB]
Building sparse matrix (transition rewards)... [n=260061, nc=360049, nnz=10214, k=4] [2.5 MB]
Creating vector for state rewards... [2.0 MB]
Creating vector for inf... [2.0 MB]
Allocating iteration vectors... [2 x 2.0 MB]
TOTAL: [21.5 MB]

Starting iterations...

Iterative method: 93 iterations in 0.66 seconds (average 0.003527, setup 0.34)

Value in the initial state: 0.3319074864514156

Time for model checking: 0.695 seconds.

Result: 0.3319074864514156 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"charge"}min=? [ F "deadlock" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=5,Objy1=3,Objx2=6,Objy2=2

Prob0A: 4 iterations in 0.02 seconds (average 0.005000, setup 0.00)

Prob1E: 5 iterations in 0.03 seconds (average 0.006400, setup 0.00)

Warning: PRISM hasn't checked for zero-reward loops.
Your minimum rewards may be too low...

goal = 1, inf = 0, maybe = 260060

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=260061, nc=360049, nnz=759347, k=4] [11.1 MB]
Building sparse matrix (transition rewards)... [n=260061, nc=360049, nnz=10214, k=4] [2.5 MB]
Creating vector for state rewards... [2.0 MB]
Creating vector for inf... [2.0 MB]
Allocating iteration vectors... [2 x 2.0 MB]
TOTAL: [21.5 MB]

Starting iterations...

Iterative method: 103 iterations in 0.69 seconds (average 0.003534, setup 0.32)

Value in the initial state: 0.07765046362692508

Time for model checking: 0.757 seconds.

Result: 0.07765046362692508 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"time"}max=? [ F "deadlock" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=5,Objy1=3,Objx2=6,Objy2=2

Prob0E: 5 iterations in 0.02 seconds (average 0.004800, setup 0.00)

Prob1A: 1 iterations in 0.00 seconds (average 0.004000, setup 0.00)

goal = 1, inf = 0, maybe = 260060

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=260061, nc=360049, nnz=759347, k=4] [11.1 MB]
Building sparse matrix (transition rewards)... [n=260061, nc=360049, nnz=129792, k=4] [3.9 MB]
Creating vector for state rewards... [2.0 MB]
Creating vector for inf... [2.0 MB]
Allocating iteration vectors... [2 x 2.0 MB]
TOTAL: [22.8 MB]

Starting iterations...

Iterative method: 99 iterations in 0.72 seconds (average 0.003556, setup 0.37)

Value in the initial state: 182.4559643509837

Time for model checking: 0.76 seconds.

Result: 182.4559643509837 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"time"}min=? [ F "deadlock" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=5,Objy1=3,Objx2=6,Objy2=2

Prob0A: 4 iterations in 0.02 seconds (average 0.006000, setup 0.00)

Prob1E: 5 iterations in 0.03 seconds (average 0.006400, setup 0.00)

Warning: PRISM hasn't checked for zero-reward loops.
Your minimum rewards may be too low...

goal = 1, inf = 0, maybe = 260060

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=260061, nc=360049, nnz=759347, k=4] [11.1 MB]
Building sparse matrix (transition rewards)... [n=260061, nc=360049, nnz=129792, k=4] [3.9 MB]
Creating vector for state rewards... [2.0 MB]
Creating vector for inf... [2.0 MB]
Allocating iteration vectors... [2 x 2.0 MB]
TOTAL: [22.8 MB]

Starting iterations...

Iterative method: 108 iterations in 0.76 seconds (average 0.003667, setup 0.36)

Value in the initial state: 104.5363429003873

Time for model checking: 0.822 seconds.

Result: 104.5363429003873 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"success"}max=? [ F "deadlock" ]/Pmin=? [ F "MissionSuccessful" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=5,Objy1=3,Objx2=6,Objy2=2

Prob0E: 5 iterations in 0.02 seconds (average 0.003200, setup 0.00)

Prob1A: 1 iterations in 0.00 seconds (average 0.004000, setup 0.00)

goal = 1, inf = 0, maybe = 260060

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=260061, nc=360049, nnz=759347, k=4] [11.1 MB]
Building sparse matrix (transition rewards)... [n=260061, nc=360049, nnz=2144, k=4] [2.4 MB]
Creating vector for state rewards... [2.0 MB]
Creating vector for inf... [2.0 MB]
Allocating iteration vectors... [2 x 2.0 MB]
TOTAL: [21.4 MB]

Starting iterations...

Iterative method: 103 iterations in 0.69 seconds (average 0.003534, setup 0.33)

Prob0E: 54 iterations in 0.24 seconds (average 0.004519, setup 0.00)

Prob1A: 3 iterations in 0.03 seconds (average 0.010667, setup 0.00)

yes = 1089, no = 221465, maybe = 37507

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=260061, nc=67360, nnz=196989, k=2] [2.6 MB]
Creating vector for yes... [2.0 MB]
Allocating iteration vectors... [2 x 2.0 MB]
TOTAL: [8.5 MB]

Starting iterations...

Iterative method: 94 iterations in 0.28 seconds (average 0.001532, setup 0.14)

Value in the initial state: 247.2228323077737

Time for model checking: 1.3 seconds.

Result: 247.2228323077737 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"success"}min=? [ F "deadlock" ]/Pmax=? [ F "MissionSuccessful" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=5,Objy1=3,Objx2=6,Objy2=2

Prob0A: 4 iterations in 0.04 seconds (average 0.010000, setup 0.00)

Prob1E: 5 iterations in 0.03 seconds (average 0.006400, setup 0.00)

Warning: PRISM hasn't checked for zero-reward loops.
Your minimum rewards may be too low...

goal = 1, inf = 0, maybe = 260060

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=260061, nc=360049, nnz=759347, k=4] [11.1 MB]
Building sparse matrix (transition rewards)... [n=260061, nc=360049, nnz=2144, k=4] [2.4 MB]
Creating vector for state rewards... [2.0 MB]
Creating vector for inf... [2.0 MB]
Allocating iteration vectors... [2 x 2.0 MB]
TOTAL: [21.4 MB]

Starting iterations...

Iterative method: 100 iterations in 0.69 seconds (average 0.003480, setup 0.34)

Prob0A: 54 iterations in 0.22 seconds (average 0.004000, setup 0.00)

Prob1E: 4 iterations in 0.03 seconds (average 0.007000, setup 0.00)

yes = 1089, no = 213876, maybe = 45096

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=260061, nc=81598, nnz=239269, k=2] [3.1 MB]
Creating vector for yes... [2.0 MB]
Allocating iteration vectors... [2 x 2.0 MB]
TOTAL: [9.0 MB]

Starting iterations...

Iterative method: 103 iterations in 0.30 seconds (average 0.001592, setup 0.14)

Value in the initial state: 79.16556319309635

Time for model checking: 1.359 seconds.

Result: 79.16556319309635 (value in the initial state)

---------------------------------------------------------------------

Note: There were 16 warnings during computation.

