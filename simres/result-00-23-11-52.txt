PRISM
=====

Version: 4.3
Date: Wed Aug 03 15:54:56 BST 2016
Hostname: eglinton
Memory limits: cudd=1g, java(heap)=981.5m
Command line: prism uav_sim_mdp.prism -s uav_mdp.props -const 'Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=1,Objy1=1,Objx2=5,Objy2=2,'

Parsing model file "uav_sim_mdp.prism"...

Parsing properties file "uav_mdp.props"...

18 properties:
(1) Pmax=? [ F "MissionSuccessful" ]
(2) Pmin=? [ F "MissionSuccessful" ]
(3) Pmax=? [ F "MissionTimeUp" ]
(4) Pmin=? [ F "MissionTimeUp" ]
(5) Pmax=? [ F "Crash" ]
(6) Pmin=? [ F "Crash" ]
(7) Pmax=? [ F "AllAreaSearched" ]
(8) Pmin=? [ F "AllAreaSearched" ]
(9) Pmax=? [ F "MissionTimeUp"|"AllAreaSearched"|"Crash" ]
(10) Pmin=? [ F "MissionTimeUp"|"AllAreaSearched"|"Crash" ]
(11) R{"drop"}max=? [ F "deadlock" ]
(12) R{"drop"}min=? [ F "deadlock" ]
(13) R{"charge"}max=? [ F "deadlock" ]
(14) R{"charge"}min=? [ F "deadlock" ]
(15) R{"time"}max=? [ F "deadlock" ]
(16) R{"time"}min=? [ F "deadlock" ]
(17) R{"success"}max=? [ F "deadlock" ]/Pmin=? [ F "MissionSuccessful" ]
(18) R{"success"}min=? [ F "deadlock" ]/Pmax=? [ F "MissionSuccessful" ]

Type:        MDP
Modules:     Obj Move Robot T 
Variables:   NoOfObjs posx posy s1 retx rety b t c 

---------------------------------------------------------------------

Model checking: Pmax=? [ F "MissionSuccessful" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=1,Objy1=1,Objx2=5,Objy2=2

Building model...
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=1,Objy1=1,Objx2=5,Objy2=2

Warning: Guard for command 7 of module "Move" is never satisfied.

Warning: Guard for command 8 of module "Move" is never satisfied.

Warning: Guard for command 9 of module "Move" is never satisfied.

Warning: Guard for command 7 of module "Robot" is never satisfied.

Warning: Update 2 of command 3 of module "Robot" doesn't do anything ("(s1'=13)", line 210, column 44)

Warning: Update 2 of command 27 of module "T" doesn't do anything ("(c'=1)", line 323, column 108)

Warning: Guard for command 28 of module "T" is never satisfied.

Warning: Update 2 of command 9 of module "T" doesn't do anything ("(c'=1)", line 300, column 112)

Warning: Guard for command 10 of module "T" is never satisfied.

Warning: Update 2 of command 51 of module "T" doesn't do anything ("(c'=1)", line 354, column 113)

Warning: Guard for command 52 of module "T" is never satisfied.

Computing reachable states...

Reachability (BFS): 88 iterations in 1.28 seconds (average 0.014500, setup 0.00)

Time for model construction: 3.563 seconds.

Warning: Deadlocks detected and fixed in 1 states

Type:        MDP
States:      278531 (1 initial)
Transitions: 760030
Choices:     374095

Transition matrix: 220822 nodes (95 terminal), 760030 minterms, vars: 34r/34c/18nd

Prob0A: 49 iterations in 0.43 seconds (average 0.008816, setup 0.00)

Prob1E: 4 iterations in 0.06 seconds (average 0.016000, setup 0.00)

yes = 1216, no = 231078, maybe = 46237

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=278531, nc=81852, nnz=234075, k=2] [3.0 MB]
Creating vector for yes... [2.1 MB]
Allocating iteration vectors... [2 x 2.1 MB]
TOTAL: [9.4 MB]

Starting iterations...

Iterative method: 98 iterations in 0.49 seconds (average 0.002000, setup 0.29)

Value in the initial state: 0.9823661868648096

Time for model checking: 1.05 seconds.

Result: 0.9823661868648096 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmin=? [ F "MissionSuccessful" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=1,Objy1=1,Objx2=5,Objy2=2

Prob0E: 49 iterations in 0.48 seconds (average 0.009878, setup 0.00)

Prob1A: 3 iterations in 0.06 seconds (average 0.021333, setup 0.00)

yes = 1216, no = 237313, maybe = 40002

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=278531, nc=70569, nnz=201645, k=2] [2.6 MB]
Creating vector for yes... [2.1 MB]
Allocating iteration vectors... [2 x 2.1 MB]
TOTAL: [9.0 MB]

Starting iterations...

Iterative method: 90 iterations in 0.40 seconds (average 0.001867, setup 0.24)

Value in the initial state: 0.817425773638384

Time for model checking: 0.989 seconds.

Result: 0.817425773638384 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmax=? [ F "MissionTimeUp" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=1,Objy1=1,Objx2=5,Objy2=2

Prob0A: 74 iterations in 0.76 seconds (average 0.010216, setup 0.00)

Prob1E: 4 iterations in 0.06 seconds (average 0.014000, setup 0.00)

yes = 408, no = 166249, maybe = 111874

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=278531, nc=194260, nnz=536731, k=4] [6.6 MB]
Creating vector for yes... [2.1 MB]
Allocating iteration vectors... [2 x 2.1 MB]
TOTAL: [13.0 MB]

Starting iterations...

Iterative method: 97 iterations in 0.59 seconds (average 0.002557, setup 0.34)

Value in the initial state: 2.693278883028089E-4

Time for model checking: 1.529 seconds.

Result: 2.693278883028089E-4 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmin=? [ F "MissionTimeUp" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=1,Objy1=1,Objx2=5,Objy2=2

Prob0E: 88 iterations in 0.96 seconds (average 0.010909, setup 0.00)

Prob1A: 3 iterations in 0.05 seconds (average 0.016000, setup 0.00)

yes = 408, no = 166717, maybe = 111406

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=278531, nc=193428, nnz=534971, k=4] [6.6 MB]
Creating vector for yes... [2.1 MB]
Allocating iteration vectors... [2 x 2.1 MB]
TOTAL: [12.9 MB]

Starting iterations...

Iterative method: 113 iterations in 0.62 seconds (average 0.002584, setup 0.33)

Value in the initial state: 3.6917969454893074E-7

Time for model checking: 1.967 seconds.

Result: 3.6917969454893074E-7 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmax=? [ F "Crash" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=1,Objy1=1,Objx2=5,Objy2=2

Prob0A: 3 iterations in 0.09 seconds (average 0.030667, setup 0.00)

Prob1E: 184 iterations in 3.18 seconds (average 0.017304, setup 0.00)

yes = 136295, no = 3283, maybe = 138953

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=278531, nc=234517, nnz=620452, k=4] [7.6 MB]
Creating vector for yes... [2.1 MB]
Allocating iteration vectors... [2 x 2.1 MB]
TOTAL: [14.0 MB]

Starting iterations...

Iterative method: 92 iterations in 0.65 seconds (average 0.002696, setup 0.40)

Value in the initial state: 0.03033077929779496

Time for model checking: 4.473 seconds.

Result: 0.03033077929779496 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmin=? [ F "Crash" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=1,Objy1=1,Objx2=5,Objy2=2

Prob0E: 4 iterations in 0.08 seconds (average 0.021000, setup 0.00)

Prob1A: 49 iterations in 0.59 seconds (average 0.012082, setup 0.00)

yes = 136295, no = 3283, maybe = 138953

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=278531, nc=234517, nnz=620452, k=4] [7.6 MB]
Creating vector for yes... [2.1 MB]
Allocating iteration vectors... [2 x 2.1 MB]
TOTAL: [14.0 MB]

Starting iterations...

Iterative method: 87 iterations in 0.56 seconds (average 0.002805, setup 0.31)

Value in the initial state: 0.015570626658506977

Time for model checking: 1.31 seconds.

Result: 0.015570626658506977 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmax=? [ F "AllAreaSearched" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=1,Objy1=1,Objx2=5,Objy2=2

Prob0A: 52 iterations in 0.44 seconds (average 0.008385, setup 0.00)

Prob1E: 4 iterations in 0.06 seconds (average 0.015000, setup 0.00)

yes = 1658, no = 209516, maybe = 67357

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=278531, nc=109274, nnz=297877, k=2] [3.8 MB]
Creating vector for yes... [2.1 MB]
Allocating iteration vectors... [2 x 2.1 MB]
TOTAL: [10.2 MB]

Starting iterations...

Iterative method: 89 iterations in 0.44 seconds (average 0.002202, setup 0.24)

Value in the initial state: 0.1569285952288742

Time for model checking: 0.958 seconds.

Result: 0.1569285952288742 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmin=? [ F "AllAreaSearched" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=1,Objy1=1,Objx2=5,Objy2=2

Prob0E: 63 iterations in 0.53 seconds (average 0.008444, setup 0.00)

Prob1A: 3 iterations in 0.05 seconds (average 0.016000, setup 0.00)

yes = 1658, no = 219187, maybe = 57686

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=278531, nc=92117, nnz=248219, k=2] [3.2 MB]
Creating vector for yes... [2.1 MB]
Allocating iteration vectors... [2 x 2.1 MB]
TOTAL: [9.6 MB]

Starting iterations...

Iterative method: 96 iterations in 0.38 seconds (average 0.002125, setup 0.17)

Value in the initial state: 0.0017571567004491416

Time for model checking: 1.017 seconds.

Result: 0.0017571567004491416 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmax=? [ F "MissionTimeUp"|"AllAreaSearched"|"Crash" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=1,Objy1=1,Objx2=5,Objy2=2

Prob0A: 3 iterations in 0.10 seconds (average 0.034667, setup 0.00)

Prob1E: 188 iterations in 3.13 seconds (average 0.016638, setup 0.00)

yes = 237312, no = 1217, maybe = 40002

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=278531, nc=70569, nnz=201645, k=2] [2.6 MB]
Creating vector for yes... [2.1 MB]
Allocating iteration vectors... [2 x 2.1 MB]
TOTAL: [9.0 MB]

Starting iterations...

Iterative method: 84 iterations in 0.37 seconds (average 0.002000, setup 0.20)

Value in the initial state: 0.1825739436736074

Time for model checking: 4.113 seconds.

Result: 0.1825739436736074 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmin=? [ F "MissionTimeUp"|"AllAreaSearched"|"Crash" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=1,Objy1=1,Objx2=5,Objy2=2

Prob0E: 4 iterations in 0.07 seconds (average 0.018000, setup 0.00)

Prob1A: 49 iterations in 0.52 seconds (average 0.010694, setup 0.00)

yes = 231077, no = 1217, maybe = 46237

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=278531, nc=81852, nnz=234075, k=2] [3.0 MB]
Creating vector for yes... [2.1 MB]
Allocating iteration vectors... [2 x 2.1 MB]
TOTAL: [9.4 MB]

Starting iterations...

Iterative method: 95 iterations in 0.39 seconds (average 0.002063, setup 0.20)

Value in the initial state: 0.017632761814984358

Time for model checking: 1.093 seconds.

Result: 0.017632761814984358 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"drop"}max=? [ F "deadlock" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=1,Objy1=1,Objx2=5,Objy2=2

Prob0E: 5 iterations in 0.08 seconds (average 0.015200, setup 0.00)

Prob1A: 1 iterations in 0.01 seconds (average 0.008000, setup 0.00)

goal = 1, inf = 0, maybe = 278530

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=278531, nc=374094, nnz=760029, k=4] [11.2 MB]
Building sparse matrix (transition rewards)... [n=278531, nc=374094, nnz=0, k=4] [2.5 MB]
Creating vector for state rewards... [2.1 MB]
Creating vector for inf... [2.1 MB]
Allocating iteration vectors... [2 x 2.1 MB]
TOTAL: [22.2 MB]

Starting iterations...

Iterative method: 92 iterations in 0.88 seconds (average 0.004130, setup 0.50)

Value in the initial state: 0.2179395019550043

Time for model checking: 1.01 seconds.

Result: 0.2179395019550043 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"drop"}min=? [ F "deadlock" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=1,Objy1=1,Objx2=5,Objy2=2

Prob0A: 4 iterations in 0.06 seconds (average 0.014000, setup 0.00)

Prob1E: 5 iterations in 0.06 seconds (average 0.012800, setup 0.00)

Warning: PRISM hasn't checked for zero-reward loops.
Your minimum rewards may be too low...

goal = 1, inf = 0, maybe = 278530

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=278531, nc=374094, nnz=760029, k=4] [11.2 MB]
Building sparse matrix (transition rewards)... [n=278531, nc=374094, nnz=0, k=4] [2.5 MB]
Creating vector for state rewards... [2.1 MB]
Creating vector for inf... [2.1 MB]
Allocating iteration vectors... [2 x 2.1 MB]
TOTAL: [22.2 MB]

Starting iterations...

Iterative method: 83 iterations in 0.73 seconds (average 0.004193, setup 0.38)

Value in the initial state: 0.1836384124570387

Time for model checking: 0.865 seconds.

Result: 0.1836384124570387 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"charge"}max=? [ F "deadlock" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=1,Objy1=1,Objx2=5,Objy2=2

Prob0E: 5 iterations in 0.04 seconds (average 0.008800, setup 0.00)

Prob1A: 1 iterations in 0.01 seconds (average 0.008000, setup 0.00)

goal = 1, inf = 0, maybe = 278530

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=278531, nc=374094, nnz=760029, k=4] [11.2 MB]
Building sparse matrix (transition rewards)... [n=278531, nc=374094, nnz=8832, k=4] [2.6 MB]
Creating vector for state rewards... [2.1 MB]
Creating vector for inf... [2.1 MB]
Allocating iteration vectors... [2 x 2.1 MB]
TOTAL: [22.3 MB]

Starting iterations...

Iterative method: 88 iterations in 0.74 seconds (average 0.004136, setup 0.38)

Value in the initial state: 0.2927542249951468

Time for model checking: 0.833 seconds.

Result: 0.2927542249951468 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"charge"}min=? [ F "deadlock" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=1,Objy1=1,Objx2=5,Objy2=2

Prob0A: 4 iterations in 0.03 seconds (average 0.008000, setup 0.00)

Prob1E: 5 iterations in 0.06 seconds (average 0.011200, setup 0.00)

Warning: PRISM hasn't checked for zero-reward loops.
Your minimum rewards may be too low...

goal = 1, inf = 0, maybe = 278530

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=278531, nc=374094, nnz=760029, k=4] [11.2 MB]
Building sparse matrix (transition rewards)... [n=278531, nc=374094, nnz=8832, k=4] [2.6 MB]
Creating vector for state rewards... [2.1 MB]
Creating vector for inf... [2.1 MB]
Allocating iteration vectors... [2 x 2.1 MB]
TOTAL: [22.3 MB]

Starting iterations...

Iterative method: 99 iterations in 0.80 seconds (average 0.004202, setup 0.39)

Value in the initial state: 0.031415340998942935

Time for model checking: 0.904 seconds.

Result: 0.031415340998942935 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"time"}max=? [ F "deadlock" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=1,Objy1=1,Objx2=5,Objy2=2

Prob0E: 5 iterations in 0.04 seconds (average 0.007200, setup 0.00)

Prob1A: 1 iterations in 0.00 seconds (average 0.004000, setup 0.00)

goal = 1, inf = 0, maybe = 278530

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=278531, nc=374094, nnz=760029, k=4] [11.2 MB]
Building sparse matrix (transition rewards)... [n=278531, nc=374094, nnz=139576, k=4] [4.1 MB]
Creating vector for state rewards... [2.1 MB]
Creating vector for inf... [2.1 MB]
Allocating iteration vectors... [2 x 2.1 MB]
TOTAL: [23.8 MB]

Starting iterations...

Iterative method: 95 iterations in 0.83 seconds (average 0.004211, setup 0.43)

Value in the initial state: 167.37766557685342

Time for model checking: 0.888 seconds.

Result: 167.37766557685342 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"time"}min=? [ F "deadlock" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=1,Objy1=1,Objx2=5,Objy2=2

Prob0A: 4 iterations in 0.03 seconds (average 0.007000, setup 0.00)

Prob1E: 5 iterations in 0.06 seconds (average 0.011200, setup 0.00)

Warning: PRISM hasn't checked for zero-reward loops.
Your minimum rewards may be too low...

goal = 1, inf = 0, maybe = 278530

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=278531, nc=374094, nnz=760029, k=4] [11.2 MB]
Building sparse matrix (transition rewards)... [n=278531, nc=374094, nnz=139576, k=4] [4.1 MB]
Creating vector for state rewards... [2.1 MB]
Creating vector for inf... [2.1 MB]
Allocating iteration vectors... [2 x 2.1 MB]
TOTAL: [23.8 MB]

Starting iterations...

Iterative method: 101 iterations in 0.85 seconds (average 0.004277, setup 0.42)

Value in the initial state: 86.35985090654871

Time for model checking: 0.951 seconds.

Result: 86.35985090654871 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"success"}max=? [ F "deadlock" ]/Pmin=? [ F "MissionSuccessful" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=1,Objy1=1,Objx2=5,Objy2=2

Prob0E: 5 iterations in 0.04 seconds (average 0.008000, setup 0.00)

Prob1A: 1 iterations in 0.00 seconds (average 0.004000, setup 0.00)

goal = 1, inf = 0, maybe = 278530

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=278531, nc=374094, nnz=760029, k=4] [11.2 MB]
Building sparse matrix (transition rewards)... [n=278531, nc=374094, nnz=2396, k=4] [2.5 MB]
Creating vector for state rewards... [2.1 MB]
Creating vector for inf... [2.1 MB]
Allocating iteration vectors... [2 x 2.1 MB]
TOTAL: [22.2 MB]

Starting iterations...

Iterative method: 96 iterations in 0.77 seconds (average 0.004125, setup 0.37)

Prob0E: 49 iterations in 0.37 seconds (average 0.007592, setup 0.00)

Prob1A: 3 iterations in 0.05 seconds (average 0.017333, setup 0.00)

yes = 1216, no = 237313, maybe = 40002

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=278531, nc=70569, nnz=201645, k=2] [2.6 MB]
Creating vector for yes... [2.1 MB]
Allocating iteration vectors... [2 x 2.1 MB]
TOTAL: [9.0 MB]

Starting iterations...

Iterative method: 90 iterations in 0.33 seconds (average 0.001822, setup 0.17)

Value in the initial state: 200.76987258611538

Time for model checking: 1.611 seconds.

Result: 200.76987258611538 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"success"}min=? [ F "deadlock" ]/Pmax=? [ F "MissionSuccessful" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=1,Objy1=1,Objx2=5,Objy2=2

Prob0A: 4 iterations in 0.05 seconds (average 0.012000, setup 0.00)

Prob1E: 5 iterations in 0.07 seconds (average 0.013600, setup 0.00)

Warning: PRISM hasn't checked for zero-reward loops.
Your minimum rewards may be too low...

goal = 1, inf = 0, maybe = 278530

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=278531, nc=374094, nnz=760029, k=4] [11.2 MB]
Building sparse matrix (transition rewards)... [n=278531, nc=374094, nnz=2396, k=4] [2.5 MB]
Creating vector for state rewards... [2.1 MB]
Creating vector for inf... [2.1 MB]
Allocating iteration vectors... [2 x 2.1 MB]
TOTAL: [22.2 MB]

Starting iterations...

Iterative method: 96 iterations in 0.82 seconds (average 0.004125, setup 0.42)

Prob0A: 49 iterations in 0.37 seconds (average 0.007510, setup 0.00)

Prob1E: 4 iterations in 0.05 seconds (average 0.012000, setup 0.00)

yes = 1216, no = 231078, maybe = 46237

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=278531, nc=81852, nnz=234075, k=2] [3.0 MB]
Creating vector for yes... [2.1 MB]
Allocating iteration vectors... [2 x 2.1 MB]
TOTAL: [9.4 MB]

Starting iterations...

Iterative method: 98 iterations in 0.38 seconds (average 0.001878, setup 0.20)

Value in the initial state: 76.54688473942059

Time for model checking: 1.762 seconds.

Result: 76.54688473942059 (value in the initial state)

---------------------------------------------------------------------

Note: There were 16 warnings during computation.

