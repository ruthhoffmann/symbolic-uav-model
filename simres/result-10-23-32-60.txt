PRISM
=====

Version: 4.3
Date: Wed Aug 03 16:14:17 BST 2016
Hostname: eglinton
Memory limits: cudd=1g, java(heap)=981.5m
Command line: prism uav_sim_mdp.prism -s uav_mdp.props -const 'Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=3,Objy1=2,Objx2=6,Objy2=0,'

Parsing model file "uav_sim_mdp.prism"...

Parsing properties file "uav_mdp.props"...

18 properties:
(1) Pmax=? [ F "MissionSuccessful" ]
(2) Pmin=? [ F "MissionSuccessful" ]
(3) Pmax=? [ F "MissionTimeUp" ]
(4) Pmin=? [ F "MissionTimeUp" ]
(5) Pmax=? [ F "Crash" ]
(6) Pmin=? [ F "Crash" ]
(7) Pmax=? [ F "AllAreaSearched" ]
(8) Pmin=? [ F "AllAreaSearched" ]
(9) Pmax=? [ F "MissionTimeUp"|"AllAreaSearched"|"Crash" ]
(10) Pmin=? [ F "MissionTimeUp"|"AllAreaSearched"|"Crash" ]
(11) R{"drop"}max=? [ F "deadlock" ]
(12) R{"drop"}min=? [ F "deadlock" ]
(13) R{"charge"}max=? [ F "deadlock" ]
(14) R{"charge"}min=? [ F "deadlock" ]
(15) R{"time"}max=? [ F "deadlock" ]
(16) R{"time"}min=? [ F "deadlock" ]
(17) R{"success"}max=? [ F "deadlock" ]/Pmin=? [ F "MissionSuccessful" ]
(18) R{"success"}min=? [ F "deadlock" ]/Pmax=? [ F "MissionSuccessful" ]

Type:        MDP
Modules:     Obj Move Robot T 
Variables:   NoOfObjs posx posy s1 retx rety b t c 

---------------------------------------------------------------------

Model checking: Pmax=? [ F "MissionSuccessful" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=3,Objy1=2,Objx2=6,Objy2=0

Building model...
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=3,Objy1=2,Objx2=6,Objy2=0

Warning: Guard for command 7 of module "Move" is never satisfied.

Warning: Guard for command 8 of module "Move" is never satisfied.

Warning: Guard for command 9 of module "Move" is never satisfied.

Warning: Guard for command 7 of module "Robot" is never satisfied.

Warning: Update 2 of command 3 of module "Robot" doesn't do anything ("(s1'=13)", line 210, column 44)

Warning: Update 2 of command 27 of module "T" doesn't do anything ("(c'=1)", line 323, column 108)

Warning: Guard for command 28 of module "T" is never satisfied.

Warning: Update 2 of command 9 of module "T" doesn't do anything ("(c'=1)", line 300, column 112)

Warning: Guard for command 10 of module "T" is never satisfied.

Warning: Update 2 of command 51 of module "T" doesn't do anything ("(c'=1)", line 354, column 113)

Warning: Guard for command 52 of module "T" is never satisfied.

Computing reachable states...

Reachability (BFS): 87 iterations in 1.46 seconds (average 0.016782, setup 0.00)

Time for model construction: 3.359 seconds.

Warning: Deadlocks detected and fixed in 1 states

Type:        MDP
States:      252143 (1 initial)
Transitions: 652132
Choices:     330857

Transition matrix: 247235 nodes (91 terminal), 652132 minterms, vars: 34r/34c/18nd

Prob0A: 51 iterations in 0.45 seconds (average 0.008784, setup 0.00)

Prob1E: 4 iterations in 0.06 seconds (average 0.016000, setup 0.00)

yes = 1149, no = 214221, maybe = 36773

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=252143, nc=63621, nnz=177826, k=2] [2.3 MB]
Creating vector for yes... [1.9 MB]
Allocating iteration vectors... [2 x 1.9 MB]
TOTAL: [8.1 MB]

Starting iterations...

Iterative method: 96 iterations in 0.41 seconds (average 0.001750, setup 0.24)

Value in the initial state: 0.982037809377586

Time for model checking: 0.953 seconds.

Result: 0.982037809377586 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmin=? [ F "MissionSuccessful" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=3,Objy1=2,Objx2=6,Objy2=0

Prob0E: 53 iterations in 0.47 seconds (average 0.008830, setup 0.00)

Prob1A: 3 iterations in 0.06 seconds (average 0.020000, setup 0.00)

yes = 1149, no = 220914, maybe = 30080

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=252143, nc=51791, nnz=142998, k=2] [1.9 MB]
Creating vector for yes... [1.9 MB]
Allocating iteration vectors... [2 x 1.9 MB]
TOTAL: [7.7 MB]

Starting iterations...

Iterative method: 90 iterations in 0.34 seconds (average 0.001689, setup 0.19)

Value in the initial state: 0.825941006837228

Time for model checking: 0.904 seconds.

Result: 0.825941006837228 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmax=? [ F "MissionTimeUp" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=3,Objy1=2,Objx2=6,Objy2=0

Prob0A: 69 iterations in 0.89 seconds (average 0.012928, setup 0.00)

Prob1E: 4 iterations in 0.15 seconds (average 0.037000, setup 0.00)

yes = 436, no = 155695, maybe = 96012

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=252143, nc=161946, nnz=437875, k=4] [5.4 MB]
Creating vector for yes... [1.9 MB]
Allocating iteration vectors... [2 x 1.9 MB]
TOTAL: [11.2 MB]

Starting iterations...

Iterative method: 95 iterations in 0.52 seconds (average 0.002105, setup 0.32)

Value in the initial state: 4.5212632772875815E-4

Time for model checking: 1.578 seconds.

Result: 4.5212632772875815E-4 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmin=? [ F "MissionTimeUp" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=3,Objy1=2,Objx2=6,Objy2=0

Prob0E: 83 iterations in 0.91 seconds (average 0.010988, setup 0.00)

Prob1A: 3 iterations in 0.08 seconds (average 0.025333, setup 0.00)

yes = 436, no = 156102, maybe = 95605

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=252143, nc=161229, nnz=436296, k=4] [5.4 MB]
Creating vector for yes... [1.9 MB]
Allocating iteration vectors... [2 x 1.9 MB]
TOTAL: [11.2 MB]

Starting iterations...

Iterative method: 110 iterations in 0.54 seconds (average 0.002145, setup 0.30)

Value in the initial state: 1.128617561855739E-6

Time for model checking: 1.555 seconds.

Result: 1.128617561855739E-6 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmax=? [ F "Crash" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=3,Objy1=2,Objx2=6,Objy2=0

Prob0A: 3 iterations in 0.12 seconds (average 0.038667, setup 0.00)

Prob1E: 195 iterations in 3.61 seconds (average 0.018523, setup 0.00)

yes = 123541, no = 3249, maybe = 125353

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=252143, nc=204067, nnz=525342, k=4] [6.4 MB]
Creating vector for yes... [1.9 MB]
Allocating iteration vectors... [2 x 1.9 MB]
TOTAL: [12.2 MB]

Starting iterations...

Iterative method: 92 iterations in 0.72 seconds (average 0.003739, setup 0.38)

Value in the initial state: 0.030142053789872527

Time for model checking: 4.498 seconds.

Result: 0.030142053789872527 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmin=? [ F "Crash" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=3,Objy1=2,Objx2=6,Objy2=0

Prob0E: 3 iterations in 0.12 seconds (average 0.038667, setup 0.00)

Prob1A: 47 iterations in 0.72 seconds (average 0.015234, setup 0.00)

yes = 123541, no = 3249, maybe = 125353

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=252143, nc=204067, nnz=525342, k=4] [6.4 MB]
Creating vector for yes... [1.9 MB]
Allocating iteration vectors... [2 x 1.9 MB]
TOTAL: [12.2 MB]

Starting iterations...

Iterative method: 87 iterations in 0.54 seconds (average 0.002437, setup 0.33)

Value in the initial state: 0.015912030430991358

Time for model checking: 1.409 seconds.

Result: 0.015912030430991358 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmax=? [ F "AllAreaSearched" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=3,Objy1=2,Objx2=6,Objy2=0

Prob0A: 58 iterations in 0.57 seconds (average 0.009793, setup 0.00)

Prob1E: 4 iterations in 0.07 seconds (average 0.017000, setup 0.00)

yes = 1663, no = 197614, maybe = 52866

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=252143, nc=80046, nnz=202503, k=2] [2.6 MB]
Creating vector for yes... [1.9 MB]
Allocating iteration vectors... [2 x 1.9 MB]
TOTAL: [8.4 MB]

Starting iterations...

Iterative method: 90 iterations in 0.37 seconds (average 0.001778, setup 0.21)

Value in the initial state: 0.14777274191038853

Time for model checking: 1.036 seconds.

Result: 0.14777274191038853 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmin=? [ F "AllAreaSearched" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=3,Objy1=2,Objx2=6,Objy2=0

Prob0E: 63 iterations in 0.57 seconds (average 0.009016, setup 0.00)

Prob1A: 3 iterations in 0.06 seconds (average 0.018667, setup 0.00)

yes = 1663, no = 204346, maybe = 46134

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=252143, nc=69324, nnz=173588, k=2] [2.3 MB]
Creating vector for yes... [1.9 MB]
Allocating iteration vectors... [2 x 1.9 MB]
TOTAL: [8.1 MB]

Starting iterations...

Iterative method: 93 iterations in 0.45 seconds (average 0.003097, setup 0.16)

Value in the initial state: 0.0017571055343859633

Time for model checking: 1.099 seconds.

Result: 0.0017571055343859633 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmax=? [ F "MissionTimeUp"|"AllAreaSearched"|"Crash" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=3,Objy1=2,Objx2=6,Objy2=0

Prob0A: 3 iterations in 0.12 seconds (average 0.041333, setup 0.00)

Prob1E: 204 iterations in 3.43 seconds (average 0.016804, setup 0.00)

yes = 220913, no = 1150, maybe = 30080

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=252143, nc=51791, nnz=142998, k=2] [1.9 MB]
Creating vector for yes... [1.9 MB]
Allocating iteration vectors... [2 x 1.9 MB]
TOTAL: [7.7 MB]

Starting iterations...

Iterative method: 84 iterations in 0.48 seconds (average 0.002619, setup 0.26)

Value in the initial state: 0.17405886030874154

Time for model checking: 4.103 seconds.

Result: 0.17405886030874154 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmin=? [ F "MissionTimeUp"|"AllAreaSearched"|"Crash" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=3,Objy1=2,Objx2=6,Objy2=0

Prob0E: 3 iterations in 0.11 seconds (average 0.037333, setup 0.00)

Prob1A: 51 iterations in 0.68 seconds (average 0.013255, setup 0.00)

yes = 214220, no = 1150, maybe = 36773

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=252143, nc=63621, nnz=177826, k=2] [2.3 MB]
Creating vector for yes... [1.9 MB]
Allocating iteration vectors... [2 x 1.9 MB]
TOTAL: [8.1 MB]

Starting iterations...

Iterative method: 95 iterations in 0.48 seconds (average 0.002611, setup 0.24)

Value in the initial state: 0.01796143296642652

Time for model checking: 1.333 seconds.

Result: 0.01796143296642652 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"drop"}max=? [ F "deadlock" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=3,Objy1=2,Objx2=6,Objy2=0

Prob0E: 4 iterations in 0.11 seconds (average 0.027000, setup 0.00)

Prob1A: 1 iterations in 0.01 seconds (average 0.012000, setup 0.00)

goal = 1, inf = 0, maybe = 252142

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=252143, nc=330856, nnz=652131, k=4] [9.7 MB]
Building sparse matrix (transition rewards)... [n=252143, nc=330856, nnz=0, k=4] [2.2 MB]
Creating vector for state rewards... [1.9 MB]
Creating vector for inf... [1.9 MB]
Allocating iteration vectors... [2 x 1.9 MB]
TOTAL: [19.6 MB]

Starting iterations...

Iterative method: 91 iterations in 0.82 seconds (average 0.003604, setup 0.49)

Value in the initial state: 0.20540064604753253

Time for model checking: 0.961 seconds.

Result: 0.20540064604753253 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"drop"}min=? [ F "deadlock" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=3,Objy1=2,Objx2=6,Objy2=0

Prob0A: 4 iterations in 0.06 seconds (average 0.015000, setup 0.00)

Prob1E: 5 iterations in 0.05 seconds (average 0.010400, setup 0.00)

Warning: PRISM hasn't checked for zero-reward loops.
Your minimum rewards may be too low...

goal = 1, inf = 0, maybe = 252142

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=252143, nc=330856, nnz=652131, k=4] [9.7 MB]
Building sparse matrix (transition rewards)... [n=252143, nc=330856, nnz=0, k=4] [2.2 MB]
Creating vector for state rewards... [1.9 MB]
Creating vector for inf... [1.9 MB]
Allocating iteration vectors... [2 x 1.9 MB]
TOTAL: [19.6 MB]

Starting iterations...

Iterative method: 82 iterations in 0.64 seconds (average 0.003610, setup 0.34)

Value in the initial state: 0.19096798124198658

Time for model checking: 0.772 seconds.

Result: 0.19096798124198658 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"charge"}max=? [ F "deadlock" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=3,Objy1=2,Objx2=6,Objy2=0

Prob0E: 4 iterations in 0.03 seconds (average 0.007000, setup 0.00)

Prob1A: 1 iterations in 0.01 seconds (average 0.008000, setup 0.00)

goal = 1, inf = 0, maybe = 252142

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=252143, nc=330856, nnz=652131, k=4] [9.7 MB]
Building sparse matrix (transition rewards)... [n=252143, nc=330856, nnz=7004, k=4] [2.3 MB]
Creating vector for state rewards... [1.9 MB]
Creating vector for inf... [1.9 MB]
Allocating iteration vectors... [2 x 1.9 MB]
TOTAL: [19.7 MB]

Starting iterations...

Iterative method: 91 iterations in 0.92 seconds (average 0.004615, setup 0.50)

Value in the initial state: 0.291993775861131

Time for model checking: 0.97 seconds.

Result: 0.291993775861131 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"charge"}min=? [ F "deadlock" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=3,Objy1=2,Objx2=6,Objy2=0

Prob0A: 4 iterations in 0.03 seconds (average 0.007000, setup 0.00)

Prob1E: 5 iterations in 0.05 seconds (average 0.010400, setup 0.00)

Warning: PRISM hasn't checked for zero-reward loops.
Your minimum rewards may be too low...

goal = 1, inf = 0, maybe = 252142

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=252143, nc=330856, nnz=652131, k=4] [9.7 MB]
Building sparse matrix (transition rewards)... [n=252143, nc=330856, nnz=7004, k=4] [2.3 MB]
Creating vector for state rewards... [1.9 MB]
Creating vector for inf... [1.9 MB]
Allocating iteration vectors... [2 x 1.9 MB]
TOTAL: [19.7 MB]

Starting iterations...

Iterative method: 97 iterations in 0.69 seconds (average 0.003546, setup 0.34)

Value in the initial state: 0.0312528996106777

Time for model checking: 0.783 seconds.

Result: 0.0312528996106777 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"time"}max=? [ F "deadlock" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=3,Objy1=2,Objx2=6,Objy2=0

Prob0E: 4 iterations in 0.03 seconds (average 0.007000, setup 0.00)

Prob1A: 1 iterations in 0.01 seconds (average 0.008000, setup 0.00)

goal = 1, inf = 0, maybe = 252142

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=252143, nc=330856, nnz=652131, k=4] [9.7 MB]
Building sparse matrix (transition rewards)... [n=252143, nc=330856, nnz=126788, k=4] [3.7 MB]
Creating vector for state rewards... [1.9 MB]
Creating vector for inf... [1.9 MB]
Allocating iteration vectors... [2 x 1.9 MB]
TOTAL: [21.1 MB]

Starting iterations...

Iterative method: 95 iterations in 0.73 seconds (average 0.003495, setup 0.40)

Value in the initial state: 166.32743739616183

Time for model checking: 0.779 seconds.

Result: 166.32743739616183 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"time"}min=? [ F "deadlock" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=3,Objy1=2,Objx2=6,Objy2=0

Prob0A: 4 iterations in 0.03 seconds (average 0.008000, setup 0.00)

Prob1E: 5 iterations in 0.05 seconds (average 0.010400, setup 0.00)

Warning: PRISM hasn't checked for zero-reward loops.
Your minimum rewards may be too low...

goal = 1, inf = 0, maybe = 252142

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=252143, nc=330856, nnz=652131, k=4] [9.7 MB]
Building sparse matrix (transition rewards)... [n=252143, nc=330856, nnz=126788, k=4] [3.7 MB]
Creating vector for state rewards... [1.9 MB]
Creating vector for inf... [1.9 MB]
Allocating iteration vectors... [2 x 1.9 MB]
TOTAL: [21.1 MB]

Starting iterations...

Iterative method: 102 iterations in 0.72 seconds (average 0.003569, setup 0.36)

Value in the initial state: 88.24277341802454

Time for model checking: 0.821 seconds.

Result: 88.24277341802454 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"success"}max=? [ F "deadlock" ]/Pmin=? [ F "MissionSuccessful" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=3,Objy1=2,Objx2=6,Objy2=0

Prob0E: 4 iterations in 0.02 seconds (average 0.006000, setup 0.00)

Prob1A: 1 iterations in 0.01 seconds (average 0.008000, setup 0.00)

goal = 1, inf = 0, maybe = 252142

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=252143, nc=330856, nnz=652131, k=4] [9.7 MB]
Building sparse matrix (transition rewards)... [n=252143, nc=330856, nnz=2264, k=4] [2.2 MB]
Creating vector for state rewards... [1.9 MB]
Creating vector for inf... [1.9 MB]
Allocating iteration vectors... [2 x 1.9 MB]
TOTAL: [19.6 MB]

Starting iterations...

Iterative method: 98 iterations in 0.64 seconds (average 0.003347, setup 0.31)

Prob0E: 53 iterations in 0.30 seconds (average 0.005736, setup 0.00)

Prob1A: 3 iterations in 0.04 seconds (average 0.014667, setup 0.00)

yes = 1149, no = 220914, maybe = 30080

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=252143, nc=51791, nnz=142998, k=2] [1.9 MB]
Creating vector for yes... [1.9 MB]
Allocating iteration vectors... [2 x 1.9 MB]
TOTAL: [7.7 MB]

Starting iterations...

Iterative method: 90 iterations in 0.27 seconds (average 0.001467, setup 0.14)

Value in the initial state: 197.55835797257828

Time for model checking: 1.332 seconds.

Result: 197.55835797257828 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"success"}min=? [ F "deadlock" ]/Pmax=? [ F "MissionSuccessful" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=3,Objy1=2,Objx2=6,Objy2=0

Prob0A: 4 iterations in 0.12 seconds (average 0.030000, setup 0.00)

Prob1E: 5 iterations in 0.09 seconds (average 0.017600, setup 0.00)

Warning: PRISM hasn't checked for zero-reward loops.
Your minimum rewards may be too low...

goal = 1, inf = 0, maybe = 252142

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=252143, nc=330856, nnz=652131, k=4] [9.7 MB]
Building sparse matrix (transition rewards)... [n=252143, nc=330856, nnz=2264, k=4] [2.2 MB]
Creating vector for state rewards... [1.9 MB]
Creating vector for inf... [1.9 MB]
Allocating iteration vectors... [2 x 1.9 MB]
TOTAL: [19.6 MB]

Starting iterations...

Iterative method: 94 iterations in 0.84 seconds (average 0.004340, setup 0.43)

Prob0A: 51 iterations in 0.46 seconds (average 0.008941, setup 0.00)

Prob1E: 4 iterations in 0.05 seconds (average 0.012000, setup 0.00)

yes = 1149, no = 214221, maybe = 36773

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=252143, nc=63621, nnz=177826, k=2] [2.3 MB]
Creating vector for yes... [1.9 MB]
Allocating iteration vectors... [2 x 1.9 MB]
TOTAL: [8.1 MB]

Starting iterations...

Iterative method: 96 iterations in 0.36 seconds (average 0.001667, setup 0.20)

Value in the initial state: 77.06415635326833

Time for model checking: 1.96 seconds.

Result: 77.06415635326833 (value in the initial state)

---------------------------------------------------------------------

Note: There were 16 warnings during computation.

