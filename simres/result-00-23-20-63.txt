PRISM
=====

Version: 4.3
Date: Wed Aug 03 15:56:41 BST 2016
Hostname: eglinton
Memory limits: cudd=1g, java(heap)=981.5m
Command line: prism uav_sim_mdp.prism -s uav_mdp.props -const 'Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=2,Objy1=0,Objx2=6,Objy2=3,'

Parsing model file "uav_sim_mdp.prism"...

Parsing properties file "uav_mdp.props"...

18 properties:
(1) Pmax=? [ F "MissionSuccessful" ]
(2) Pmin=? [ F "MissionSuccessful" ]
(3) Pmax=? [ F "MissionTimeUp" ]
(4) Pmin=? [ F "MissionTimeUp" ]
(5) Pmax=? [ F "Crash" ]
(6) Pmin=? [ F "Crash" ]
(7) Pmax=? [ F "AllAreaSearched" ]
(8) Pmin=? [ F "AllAreaSearched" ]
(9) Pmax=? [ F "MissionTimeUp"|"AllAreaSearched"|"Crash" ]
(10) Pmin=? [ F "MissionTimeUp"|"AllAreaSearched"|"Crash" ]
(11) R{"drop"}max=? [ F "deadlock" ]
(12) R{"drop"}min=? [ F "deadlock" ]
(13) R{"charge"}max=? [ F "deadlock" ]
(14) R{"charge"}min=? [ F "deadlock" ]
(15) R{"time"}max=? [ F "deadlock" ]
(16) R{"time"}min=? [ F "deadlock" ]
(17) R{"success"}max=? [ F "deadlock" ]/Pmin=? [ F "MissionSuccessful" ]
(18) R{"success"}min=? [ F "deadlock" ]/Pmax=? [ F "MissionSuccessful" ]

Type:        MDP
Modules:     Obj Move Robot T 
Variables:   NoOfObjs posx posy s1 retx rety b t c 

---------------------------------------------------------------------

Model checking: Pmax=? [ F "MissionSuccessful" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=2,Objy1=0,Objx2=6,Objy2=3

Building model...
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=2,Objy1=0,Objx2=6,Objy2=3

Warning: Guard for command 7 of module "Move" is never satisfied.

Warning: Guard for command 8 of module "Move" is never satisfied.

Warning: Guard for command 9 of module "Move" is never satisfied.

Warning: Guard for command 7 of module "Robot" is never satisfied.

Warning: Update 2 of command 3 of module "Robot" doesn't do anything ("(s1'=13)", line 210, column 44)

Warning: Update 2 of command 27 of module "T" doesn't do anything ("(c'=1)", line 323, column 108)

Warning: Guard for command 28 of module "T" is never satisfied.

Warning: Update 2 of command 9 of module "T" doesn't do anything ("(c'=1)", line 300, column 112)

Warning: Guard for command 10 of module "T" is never satisfied.

Warning: Update 2 of command 51 of module "T" doesn't do anything ("(c'=1)", line 354, column 113)

Warning: Guard for command 52 of module "T" is never satisfied.

Computing reachable states...

Reachability (BFS): 89 iterations in 2.23 seconds (average 0.025034, setup 0.00)

Time for model construction: 4.367 seconds.

Warning: Deadlocks detected and fixed in 1 states

Type:        MDP
States:      351741 (1 initial)
Transitions: 953435
Choices:     468006

Transition matrix: 231047 nodes (97 terminal), 953435 minterms, vars: 34r/34c/18nd

Prob0A: 61 iterations in 0.63 seconds (average 0.010295, setup 0.00)

Prob1E: 4 iterations in 0.09 seconds (average 0.023000, setup 0.00)

yes = 1242, no = 298544, maybe = 51955

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=351741, nc=89795, nnz=257978, k=2] [3.4 MB]
Creating vector for yes... [2.7 MB]
Allocating iteration vectors... [2 x 2.7 MB]
TOTAL: [11.4 MB]

Starting iterations...

Iterative method: 104 iterations in 0.59 seconds (average 0.002654, setup 0.32)

Value in the initial state: 0.9795143598110484

Time for model checking: 1.343 seconds.

Result: 0.9795143598110484 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmin=? [ F "MissionSuccessful" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=2,Objy1=0,Objx2=6,Objy2=3

Prob0E: 63 iterations in 0.64 seconds (average 0.010159, setup 0.00)

Prob1A: 3 iterations in 0.08 seconds (average 0.025333, setup 0.00)

yes = 1242, no = 305143, maybe = 45356

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=351741, nc=78640, nnz=226170, k=2] [3.0 MB]
Creating vector for yes... [2.7 MB]
Allocating iteration vectors... [2 x 2.7 MB]
TOTAL: [11.0 MB]

Starting iterations...

Iterative method: 95 iterations in 0.53 seconds (average 0.003116, setup 0.23)

Value in the initial state: 0.7151598387097556

Time for model checking: 1.295 seconds.

Result: 0.7151598387097556 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmax=? [ F "MissionTimeUp" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=2,Objy1=0,Objx2=6,Objy2=3

Prob0A: 69 iterations in 1.04 seconds (average 0.015072, setup 0.00)

Prob1E: 4 iterations in 0.06 seconds (average 0.015000, setup 0.00)

yes = 540, no = 200596, maybe = 150605

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=351741, nc=252990, nnz=695335, k=4] [8.5 MB]
Creating vector for yes... [2.7 MB]
Allocating iteration vectors... [2 x 2.7 MB]
TOTAL: [16.6 MB]

Starting iterations...

Iterative method: 101 iterations in 0.75 seconds (average 0.003248, setup 0.42)

Value in the initial state: 7.785584497993316E-4

Time for model checking: 1.864 seconds.

Result: 7.785584497993316E-4 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmin=? [ F "MissionTimeUp" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=2,Objy1=0,Objx2=6,Objy2=3

Prob0E: 82 iterations in 1.10 seconds (average 0.013366, setup 0.00)

Prob1A: 3 iterations in 0.08 seconds (average 0.025333, setup 0.00)

yes = 540, no = 200990, maybe = 150211

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=351741, nc=252283, nnz=693979, k=4] [8.5 MB]
Creating vector for yes... [2.7 MB]
Allocating iteration vectors... [2 x 2.7 MB]
TOTAL: [16.6 MB]

Starting iterations...

Iterative method: 116 iterations in 0.78 seconds (average 0.003207, setup 0.41)

Value in the initial state: 3.3905891082712264E-6

Time for model checking: 1.99 seconds.

Result: 3.3905891082712264E-6 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmax=? [ F "Crash" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=2,Objy1=0,Objx2=6,Objy2=3

Prob0A: 3 iterations in 0.12 seconds (average 0.040000, setup 0.00)

Prob1E: 214 iterations in 4.29 seconds (average 0.020056, setup 0.00)

yes = 172104, no = 3523, maybe = 176114

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=351741, nc=292379, nnz=777808, k=4] [9.5 MB]
Creating vector for yes... [2.7 MB]
Allocating iteration vectors... [2 x 2.7 MB]
TOTAL: [17.6 MB]

Starting iterations...

Iterative method: 96 iterations in 0.83 seconds (average 0.003375, setup 0.51)

Value in the initial state: 0.0331222587142276

Time for model checking: 5.285 seconds.

Result: 0.0331222587142276 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmin=? [ F "Crash" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=2,Objy1=0,Objx2=6,Objy2=3

Prob0E: 4 iterations in 0.18 seconds (average 0.044000, setup 0.00)

Prob1A: 52 iterations in 1.22 seconds (average 0.023462, setup 0.00)

yes = 172104, no = 3523, maybe = 176114

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=351741, nc=292379, nnz=777808, k=4] [9.5 MB]
Creating vector for yes... [2.7 MB]
Allocating iteration vectors... [2 x 2.7 MB]
TOTAL: [17.6 MB]

Starting iterations...

Iterative method: 102 iterations in 1.07 seconds (average 0.004392, setup 0.62)

Value in the initial state: 0.018315627076579282

Time for model checking: 2.546 seconds.

Result: 0.018315627076579282 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmax=? [ F "AllAreaSearched" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=2,Objy1=0,Objx2=6,Objy2=3

Prob0A: 64 iterations in 0.51 seconds (average 0.008000, setup 0.00)

Prob1E: 4 iterations in 0.07 seconds (average 0.018000, setup 0.00)

yes = 1740, no = 273149, maybe = 76852

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=351741, nc=125842, nnz=349452, k=2] [4.5 MB]
Creating vector for yes... [2.7 MB]
Allocating iteration vectors... [2 x 2.7 MB]
TOTAL: [12.5 MB]

Starting iterations...

Iterative method: 92 iterations in 0.53 seconds (average 0.002913, setup 0.26)

Value in the initial state: 0.2546875771427138

Time for model checking: 1.147 seconds.

Result: 0.2546875771427138 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmin=? [ F "AllAreaSearched" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=2,Objy1=0,Objx2=6,Objy2=3

Prob0E: 68 iterations in 0.56 seconds (average 0.008176, setup 0.00)

Prob1A: 3 iterations in 0.06 seconds (average 0.018667, setup 0.00)

yes = 1740, no = 282762, maybe = 67239

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=351741, nc=110032, nnz=305339, k=2] [3.9 MB]
Creating vector for yes... [2.7 MB]
Allocating iteration vectors... [2 x 2.7 MB]
TOTAL: [12.0 MB]

Starting iterations...

Iterative method: 99 iterations in 0.48 seconds (average 0.002707, setup 0.21)

Value in the initial state: 0.0017436338866852945

Time for model checking: 1.114 seconds.

Result: 0.0017436338866852945 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmax=? [ F "MissionTimeUp"|"AllAreaSearched"|"Crash" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=2,Objy1=0,Objx2=6,Objy2=3

Prob0A: 3 iterations in 0.11 seconds (average 0.036000, setup 0.00)

Prob1E: 244 iterations in 4.98 seconds (average 0.020426, setup 0.00)

yes = 305142, no = 1243, maybe = 45356

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=351741, nc=78640, nnz=226170, k=2] [3.0 MB]
Creating vector for yes... [2.7 MB]
Allocating iteration vectors... [2 x 2.7 MB]
TOTAL: [11.0 MB]

Starting iterations...

Iterative method: 88 iterations in 0.50 seconds (average 0.002409, setup 0.29)

Value in the initial state: 0.28483989441617663

Time for model checking: 5.64 seconds.

Result: 0.28483989441617663 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmin=? [ F "MissionTimeUp"|"AllAreaSearched"|"Crash" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=2,Objy1=0,Objx2=6,Objy2=3

Prob0E: 4 iterations in 0.10 seconds (average 0.025000, setup 0.00)

Prob1A: 61 iterations in 0.94 seconds (average 0.015475, setup 0.00)

yes = 298543, no = 1243, maybe = 51955

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=351741, nc=89795, nnz=257978, k=2] [3.4 MB]
Creating vector for yes... [2.7 MB]
Allocating iteration vectors... [2 x 2.7 MB]
TOTAL: [11.4 MB]

Starting iterations...

Iterative method: 100 iterations in 0.51 seconds (average 0.002520, setup 0.26)

Value in the initial state: 0.020485252625485558

Time for model checking: 1.602 seconds.

Result: 0.020485252625485558 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"drop"}max=? [ F "deadlock" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=2,Objy1=0,Objx2=6,Objy2=3

Prob0E: 5 iterations in 0.13 seconds (average 0.025600, setup 0.00)

Prob1A: 1 iterations in 0.01 seconds (average 0.012000, setup 0.00)

goal = 1, inf = 0, maybe = 351740

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=351741, nc=468005, nnz=953434, k=4] [14.0 MB]
Building sparse matrix (transition rewards)... [n=351741, nc=468005, nnz=0, k=4] [3.1 MB]
Creating vector for state rewards... [2.7 MB]
Creating vector for inf... [2.7 MB]
Allocating iteration vectors... [2 x 2.7 MB]
TOTAL: [27.9 MB]

Starting iterations...

Iterative method: 99 iterations in 1.10 seconds (average 0.005212, setup 0.58)

Value in the initial state: 0.283425233089956

Time for model checking: 1.258 seconds.

Result: 0.283425233089956 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"drop"}min=? [ F "deadlock" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=2,Objy1=0,Objx2=6,Objy2=3

Prob0A: 4 iterations in 0.04 seconds (average 0.011000, setup 0.00)

Prob1E: 5 iterations in 0.08 seconds (average 0.015200, setup 0.00)

Warning: PRISM hasn't checked for zero-reward loops.
Your minimum rewards may be too low...

goal = 1, inf = 0, maybe = 351740

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=351741, nc=468005, nnz=953434, k=4] [14.0 MB]
Building sparse matrix (transition rewards)... [n=351741, nc=468005, nnz=0, k=4] [3.1 MB]
Creating vector for state rewards... [2.7 MB]
Creating vector for inf... [2.7 MB]
Allocating iteration vectors... [2 x 2.7 MB]
TOTAL: [27.9 MB]

Starting iterations...

Iterative method: 89 iterations in 1.03 seconds (average 0.006292, setup 0.47)

Value in the initial state: 0.23207397708690847

Time for model checking: 1.188 seconds.

Result: 0.23207397708690847 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"charge"}max=? [ F "deadlock" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=2,Objy1=0,Objx2=6,Objy2=3

Prob0E: 5 iterations in 0.04 seconds (average 0.008800, setup 0.00)

Prob1A: 1 iterations in 0.01 seconds (average 0.008000, setup 0.00)

goal = 1, inf = 0, maybe = 351740

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=351741, nc=468005, nnz=953434, k=4] [14.0 MB]
Building sparse matrix (transition rewards)... [n=351741, nc=468005, nnz=11338, k=4] [3.3 MB]
Creating vector for state rewards... [2.7 MB]
Creating vector for inf... [2.7 MB]
Allocating iteration vectors... [2 x 2.7 MB]
TOTAL: [28.0 MB]

Starting iterations...

Iterative method: 94 iterations in 0.95 seconds (average 0.005021, setup 0.48)

Value in the initial state: 0.3284693996848606

Time for model checking: 1.028 seconds.

Result: 0.3284693996848606 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"charge"}min=? [ F "deadlock" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=2,Objy1=0,Objx2=6,Objy2=3

Prob0A: 4 iterations in 0.03 seconds (average 0.008000, setup 0.00)

Prob1E: 5 iterations in 0.06 seconds (average 0.012000, setup 0.00)

Warning: PRISM hasn't checked for zero-reward loops.
Your minimum rewards may be too low...

goal = 1, inf = 0, maybe = 351740

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=351741, nc=468005, nnz=953434, k=4] [14.0 MB]
Building sparse matrix (transition rewards)... [n=351741, nc=468005, nnz=11338, k=4] [3.3 MB]
Creating vector for state rewards... [2.7 MB]
Creating vector for inf... [2.7 MB]
Allocating iteration vectors... [2 x 2.7 MB]
TOTAL: [28.0 MB]

Starting iterations...

Iterative method: 103 iterations in 1.08 seconds (average 0.005942, setup 0.47)

Value in the initial state: 0.07430904251688092

Time for model checking: 1.196 seconds.

Result: 0.07430904251688092 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"time"}max=? [ F "deadlock" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=2,Objy1=0,Objx2=6,Objy2=3

Prob0E: 5 iterations in 0.05 seconds (average 0.009600, setup 0.00)

Prob1A: 1 iterations in 0.01 seconds (average 0.008000, setup 0.00)

goal = 1, inf = 0, maybe = 351740

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=351741, nc=468005, nnz=953434, k=4] [14.0 MB]
Building sparse matrix (transition rewards)... [n=351741, nc=468005, nnz=175625, k=4] [5.1 MB]
Creating vector for state rewards... [2.7 MB]
Creating vector for inf... [2.7 MB]
Allocating iteration vectors... [2 x 2.7 MB]
TOTAL: [29.9 MB]

Starting iterations...

Iterative method: 99 iterations in 1.11 seconds (average 0.005939, setup 0.52)

Value in the initial state: 182.64491863133182

Time for model checking: 1.2 seconds.

Result: 182.64491863133182 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"time"}min=? [ F "deadlock" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=2,Objy1=0,Objx2=6,Objy2=3

Prob0A: 4 iterations in 0.06 seconds (average 0.015000, setup 0.00)

Prob1E: 5 iterations in 0.09 seconds (average 0.018400, setup 0.00)

Warning: PRISM hasn't checked for zero-reward loops.
Your minimum rewards may be too low...

goal = 1, inf = 0, maybe = 351740

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=351741, nc=468005, nnz=953434, k=4] [14.0 MB]
Building sparse matrix (transition rewards)... [n=351741, nc=468005, nnz=175625, k=4] [5.1 MB]
Creating vector for state rewards... [2.7 MB]
Creating vector for inf... [2.7 MB]
Allocating iteration vectors... [2 x 2.7 MB]
TOTAL: [29.9 MB]

Starting iterations...

Iterative method: 108 iterations in 1.18 seconds (average 0.005333, setup 0.60)

Value in the initial state: 101.44979077038246

Time for model checking: 1.347 seconds.

Result: 101.44979077038246 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"success"}max=? [ F "deadlock" ]/Pmin=? [ F "MissionSuccessful" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=2,Objy1=0,Objx2=6,Objy2=3

Prob0E: 5 iterations in 0.03 seconds (average 0.006400, setup 0.00)

Prob1A: 1 iterations in 0.00 seconds (average 0.004000, setup 0.00)

goal = 1, inf = 0, maybe = 351740

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=351741, nc=468005, nnz=953434, k=4] [14.0 MB]
Building sparse matrix (transition rewards)... [n=351741, nc=468005, nnz=2446, k=4] [3.2 MB]
Creating vector for state rewards... [2.7 MB]
Creating vector for inf... [2.7 MB]
Allocating iteration vectors... [2 x 2.7 MB]
TOTAL: [27.9 MB]

Starting iterations...

Iterative method: 101 iterations in 0.86 seconds (average 0.004515, setup 0.40)

Prob0E: 63 iterations in 0.31 seconds (average 0.004889, setup 0.00)

Prob1A: 3 iterations in 0.04 seconds (average 0.013333, setup 0.00)

yes = 1242, no = 305143, maybe = 45356

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=351741, nc=78640, nnz=226170, k=2] [3.0 MB]
Creating vector for yes... [2.7 MB]
Allocating iteration vectors... [2 x 2.7 MB]
TOTAL: [11.0 MB]

Starting iterations...

Iterative method: 95 iterations in 0.36 seconds (average 0.002021, setup 0.16)

Value in the initial state: 250.03898354755628

Time for model checking: 1.658 seconds.

Result: 250.03898354755628 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"success"}min=? [ F "deadlock" ]/Pmax=? [ F "MissionSuccessful" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=2,Objy1=0,Objx2=6,Objy2=3

Prob0A: 4 iterations in 0.08 seconds (average 0.020000, setup 0.00)

Prob1E: 5 iterations in 0.05 seconds (average 0.009600, setup 0.00)

Warning: PRISM hasn't checked for zero-reward loops.
Your minimum rewards may be too low...

goal = 1, inf = 0, maybe = 351740

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=351741, nc=468005, nnz=953434, k=4] [14.0 MB]
Building sparse matrix (transition rewards)... [n=351741, nc=468005, nnz=2446, k=4] [3.2 MB]
Creating vector for state rewards... [2.7 MB]
Creating vector for inf... [2.7 MB]
Allocating iteration vectors... [2 x 2.7 MB]
TOTAL: [27.9 MB]

Starting iterations...

Iterative method: 101 iterations in 0.92 seconds (average 0.004634, setup 0.46)

Prob0A: 61 iterations in 0.32 seconds (average 0.005246, setup 0.00)

Prob1E: 4 iterations in 0.04 seconds (average 0.011000, setup 0.00)

yes = 1242, no = 298544, maybe = 51955

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=351741, nc=89795, nnz=257978, k=2] [3.4 MB]
Creating vector for yes... [2.7 MB]
Allocating iteration vectors... [2 x 2.7 MB]
TOTAL: [11.4 MB]

Starting iterations...

Iterative method: 104 iterations in 0.40 seconds (average 0.002038, setup 0.18)

Value in the initial state: 78.87689013970294

Time for model checking: 1.883 seconds.

Result: 78.87689013970294 (value in the initial state)

---------------------------------------------------------------------

Note: There were 16 warnings during computation.

