PRISM
=====

Version: 4.3
Date: Wed Aug 03 16:13:05 BST 2016
Hostname: eglinton
Memory limits: cudd=1g, java(heap)=981.5m
Command line: prism uav_sim_mdp.prism -s uav_mdp.props -const 'Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=3,Objy1=0,Objx2=4,Objy2=3,'

Parsing model file "uav_sim_mdp.prism"...

Parsing properties file "uav_mdp.props"...

18 properties:
(1) Pmax=? [ F "MissionSuccessful" ]
(2) Pmin=? [ F "MissionSuccessful" ]
(3) Pmax=? [ F "MissionTimeUp" ]
(4) Pmin=? [ F "MissionTimeUp" ]
(5) Pmax=? [ F "Crash" ]
(6) Pmin=? [ F "Crash" ]
(7) Pmax=? [ F "AllAreaSearched" ]
(8) Pmin=? [ F "AllAreaSearched" ]
(9) Pmax=? [ F "MissionTimeUp"|"AllAreaSearched"|"Crash" ]
(10) Pmin=? [ F "MissionTimeUp"|"AllAreaSearched"|"Crash" ]
(11) R{"drop"}max=? [ F "deadlock" ]
(12) R{"drop"}min=? [ F "deadlock" ]
(13) R{"charge"}max=? [ F "deadlock" ]
(14) R{"charge"}min=? [ F "deadlock" ]
(15) R{"time"}max=? [ F "deadlock" ]
(16) R{"time"}min=? [ F "deadlock" ]
(17) R{"success"}max=? [ F "deadlock" ]/Pmin=? [ F "MissionSuccessful" ]
(18) R{"success"}min=? [ F "deadlock" ]/Pmax=? [ F "MissionSuccessful" ]

Type:        MDP
Modules:     Obj Move Robot T 
Variables:   NoOfObjs posx posy s1 retx rety b t c 

---------------------------------------------------------------------

Model checking: Pmax=? [ F "MissionSuccessful" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=3,Objy1=0,Objx2=4,Objy2=3

Building model...
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=3,Objy1=0,Objx2=4,Objy2=3

Warning: Guard for command 7 of module "Move" is never satisfied.

Warning: Guard for command 8 of module "Move" is never satisfied.

Warning: Guard for command 9 of module "Move" is never satisfied.

Warning: Guard for command 7 of module "Robot" is never satisfied.

Warning: Update 2 of command 3 of module "Robot" doesn't do anything ("(s1'=13)", line 210, column 44)

Warning: Update 2 of command 27 of module "T" doesn't do anything ("(c'=1)", line 323, column 108)

Warning: Guard for command 28 of module "T" is never satisfied.

Warning: Update 2 of command 9 of module "T" doesn't do anything ("(c'=1)", line 300, column 112)

Warning: Guard for command 10 of module "T" is never satisfied.

Warning: Update 2 of command 51 of module "T" doesn't do anything ("(c'=1)", line 354, column 113)

Warning: Guard for command 52 of module "T" is never satisfied.

Computing reachable states...

Reachability (BFS): 88 iterations in 1.66 seconds (average 0.018864, setup 0.00)

Time for model construction: 3.577 seconds.

Warning: Deadlocks detected and fixed in 1 states

Type:        MDP
States:      281529 (1 initial)
Transitions: 730650
Choices:     368829

Transition matrix: 223882 nodes (91 terminal), 730650 minterms, vars: 34r/34c/18nd

Prob0A: 60 iterations in 0.55 seconds (average 0.009200, setup 0.00)

Prob1E: 4 iterations in 0.06 seconds (average 0.016000, setup 0.00)

yes = 1152, no = 239179, maybe = 41198

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=281529, nc=69439, nnz=192764, k=2] [2.5 MB]
Creating vector for yes... [2.1 MB]
Allocating iteration vectors... [2 x 2.1 MB]
TOTAL: [9.0 MB]

Starting iterations...

Iterative method: 99 iterations in 0.48 seconds (average 0.002061, setup 0.28)

Value in the initial state: 0.9802088592450514

Time for model checking: 1.123 seconds.

Result: 0.9802088592450514 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmin=? [ F "MissionSuccessful" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=3,Objy1=0,Objx2=4,Objy2=3

Prob0E: 62 iterations in 0.58 seconds (average 0.009419, setup 0.00)

Prob1A: 3 iterations in 0.08 seconds (average 0.025333, setup 0.00)

yes = 1152, no = 245363, maybe = 35014

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=281529, nc=59256, nnz=163990, k=2] [2.2 MB]
Creating vector for yes... [2.1 MB]
Allocating iteration vectors... [2 x 2.1 MB]
TOTAL: [8.6 MB]

Starting iterations...

Iterative method: 94 iterations in 0.40 seconds (average 0.001915, setup 0.22)

Value in the initial state: 0.7468913731291202

Time for model checking: 1.102 seconds.

Result: 0.7468913731291202 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmax=? [ F "MissionTimeUp" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=3,Objy1=0,Objx2=4,Objy2=3

Prob0A: 70 iterations in 0.92 seconds (average 0.013200, setup 0.00)

Prob1E: 4 iterations in 0.04 seconds (average 0.011000, setup 0.00)

yes = 460, no = 161899, maybe = 119170

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=281529, nc=193408, nnz=517265, k=4] [6.4 MB]
Creating vector for yes... [2.1 MB]
Allocating iteration vectors... [2 x 2.1 MB]
TOTAL: [12.8 MB]

Starting iterations...

Iterative method: 99 iterations in 0.58 seconds (average 0.002465, setup 0.34)

Value in the initial state: 2.6796382094062746E-4

Time for model checking: 1.584 seconds.

Result: 2.6796382094062746E-4 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmin=? [ F "MissionTimeUp" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=3,Objy1=0,Objx2=4,Objy2=3

Prob0E: 82 iterations in 0.92 seconds (average 0.011171, setup 0.00)

Prob1A: 3 iterations in 0.05 seconds (average 0.017333, setup 0.00)

yes = 460, no = 162189, maybe = 118880

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=281529, nc=192899, nnz=516193, k=4] [6.4 MB]
Creating vector for yes... [2.1 MB]
Allocating iteration vectors... [2 x 2.1 MB]
TOTAL: [12.8 MB]

Starting iterations...

Iterative method: 115 iterations in 0.62 seconds (average 0.002574, setup 0.33)

Value in the initial state: 6.555853555119597E-7

Time for model checking: 1.615 seconds.

Result: 6.555853555119597E-7 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmax=? [ F "Crash" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=3,Objy1=0,Objx2=4,Objy2=3

Prob0A: 3 iterations in 0.10 seconds (average 0.033333, setup 0.00)

Prob1E: 208 iterations in 3.64 seconds (average 0.017500, setup 0.00)

yes = 138047, no = 3293, maybe = 140189

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=281529, nc=227489, nnz=589310, k=4] [7.2 MB]
Creating vector for yes... [2.1 MB]
Allocating iteration vectors... [2 x 2.1 MB]
TOTAL: [13.7 MB]

Starting iterations...

Iterative method: 95 iterations in 0.73 seconds (average 0.002821, setup 0.46)

Value in the initial state: 0.03113503107353645

Time for model checking: 4.514 seconds.

Result: 0.03113503107353645 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmin=? [ F "Crash" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=3,Objy1=0,Objx2=4,Objy2=3

Prob0E: 3 iterations in 0.09 seconds (average 0.030667, setup 0.00)

Prob1A: 49 iterations in 1.00 seconds (average 0.020327, setup 0.00)

yes = 138047, no = 3293, maybe = 140189

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=281529, nc=227489, nnz=589310, k=4] [7.2 MB]
Creating vector for yes... [2.1 MB]
Allocating iteration vectors... [2 x 2.1 MB]
TOTAL: [13.7 MB]

Starting iterations...

Iterative method: 91 iterations in 0.64 seconds (average 0.003692, setup 0.31)

Value in the initial state: 0.017721385102342734

Time for model checking: 1.764 seconds.

Result: 0.017721385102342734 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmax=? [ F "AllAreaSearched" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=3,Objy1=0,Objx2=4,Objy2=3

Prob0A: 61 iterations in 0.59 seconds (average 0.009705, setup 0.00)

Prob1E: 4 iterations in 0.06 seconds (average 0.014000, setup 0.00)

yes = 1680, no = 218768, maybe = 61081

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=281529, nc=97618, nnz=259911, k=2] [3.3 MB]
Creating vector for yes... [2.1 MB]
Allocating iteration vectors... [2 x 2.1 MB]
TOTAL: [9.8 MB]

Starting iterations...

Iterative method: 87 iterations in 0.40 seconds (average 0.002069, setup 0.22)

Value in the initial state: 0.224682941048293

Time for model checking: 1.057 seconds.

Result: 0.224682941048293 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmin=? [ F "AllAreaSearched" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=3,Objy1=0,Objx2=4,Objy2=3

Prob0E: 64 iterations in 0.50 seconds (average 0.007812, setup 0.00)

Prob1A: 3 iterations in 0.05 seconds (average 0.016000, setup 0.00)

yes = 1680, no = 226878, maybe = 52971

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=281529, nc=85174, nnz=226825, k=2] [2.9 MB]
Creating vector for yes... [2.1 MB]
Allocating iteration vectors... [2 x 2.1 MB]
TOTAL: [9.4 MB]

Starting iterations...

Iterative method: 96 iterations in 0.42 seconds (average 0.002750, setup 0.16)

Value in the initial state: 0.0017915390050331416

Time for model checking: 0.993 seconds.

Result: 0.0017915390050331416 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmax=? [ F "MissionTimeUp"|"AllAreaSearched"|"Crash" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=3,Objy1=0,Objx2=4,Objy2=3

Prob0A: 3 iterations in 0.13 seconds (average 0.044000, setup 0.00)

Prob1E: 240 iterations in 4.86 seconds (average 0.020250, setup 0.00)

yes = 245362, no = 1153, maybe = 35014

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=281529, nc=59256, nnz=163990, k=2] [2.2 MB]
Creating vector for yes... [2.1 MB]
Allocating iteration vectors... [2 x 2.1 MB]
TOTAL: [8.6 MB]

Starting iterations...

Iterative method: 87 iterations in 0.38 seconds (average 0.001793, setup 0.22)

Value in the initial state: 0.25310834261432075

Time for model checking: 5.433 seconds.

Result: 0.25310834261432075 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmin=? [ F "MissionTimeUp"|"AllAreaSearched"|"Crash" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=3,Objy1=0,Objx2=4,Objy2=3

Prob0E: 3 iterations in 0.07 seconds (average 0.024000, setup 0.00)

Prob1A: 60 iterations in 0.81 seconds (average 0.013467, setup 0.00)

yes = 239178, no = 1153, maybe = 41198

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=281529, nc=69439, nnz=192764, k=2] [2.5 MB]
Creating vector for yes... [2.1 MB]
Allocating iteration vectors... [2 x 2.1 MB]
TOTAL: [9.0 MB]

Starting iterations...

Iterative method: 98 iterations in 0.40 seconds (average 0.002000, setup 0.21)

Value in the initial state: 0.01979004054175568

Time for model checking: 1.326 seconds.

Result: 0.01979004054175568 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"drop"}max=? [ F "deadlock" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=3,Objy1=0,Objx2=4,Objy2=3

Prob0E: 4 iterations in 0.11 seconds (average 0.027000, setup 0.00)

Prob1A: 1 iterations in 0.01 seconds (average 0.012000, setup 0.00)

goal = 1, inf = 0, maybe = 281528

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=281529, nc=368828, nnz=730649, k=4] [10.8 MB]
Building sparse matrix (transition rewards)... [n=281529, nc=368828, nnz=0, k=4] [2.5 MB]
Creating vector for state rewards... [2.1 MB]
Creating vector for inf... [2.1 MB]
Allocating iteration vectors... [2 x 2.1 MB]
TOTAL: [21.9 MB]

Starting iterations...

Iterative method: 96 iterations in 1.15 seconds (average 0.004250, setup 0.74)

Value in the initial state: 0.21172043078857647

Time for model checking: 1.286 seconds.

Result: 0.21172043078857647 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"drop"}min=? [ F "deadlock" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=3,Objy1=0,Objx2=4,Objy2=3

Prob0A: 4 iterations in 0.12 seconds (average 0.029000, setup 0.00)

Prob1E: 5 iterations in 0.07 seconds (average 0.014400, setup 0.00)

Warning: PRISM hasn't checked for zero-reward loops.
Your minimum rewards may be too low...

goal = 1, inf = 0, maybe = 281528

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=281529, nc=368828, nnz=730649, k=4] [10.8 MB]
Building sparse matrix (transition rewards)... [n=281529, nc=368828, nnz=0, k=4] [2.5 MB]
Creating vector for state rewards... [2.1 MB]
Creating vector for inf... [2.1 MB]
Allocating iteration vectors... [2 x 2.1 MB]
TOTAL: [21.9 MB]

Starting iterations...

Iterative method: 87 iterations in 0.74 seconds (average 0.004092, setup 0.38)

Value in the initial state: 0.18490179128461434

Time for model checking: 0.942 seconds.

Result: 0.18490179128461434 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"charge"}max=? [ F "deadlock" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=3,Objy1=0,Objx2=4,Objy2=3

Prob0E: 4 iterations in 0.04 seconds (average 0.010000, setup 0.00)

Prob1A: 1 iterations in 0.01 seconds (average 0.008000, setup 0.00)

goal = 1, inf = 0, maybe = 281528

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=281529, nc=368828, nnz=730649, k=4] [10.8 MB]
Building sparse matrix (transition rewards)... [n=281529, nc=368828, nnz=7841, k=4] [2.6 MB]
Creating vector for state rewards... [2.1 MB]
Creating vector for inf... [2.1 MB]
Allocating iteration vectors... [2 x 2.1 MB]
TOTAL: [22.0 MB]

Starting iterations...

Iterative method: 94 iterations in 0.77 seconds (average 0.004085, setup 0.39)

Value in the initial state: 0.29194430560843315

Time for model checking: 0.827 seconds.

Result: 0.29194430560843315 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"charge"}min=? [ F "deadlock" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=3,Objy1=0,Objx2=4,Objy2=3

Prob0A: 4 iterations in 0.04 seconds (average 0.009000, setup 0.00)

Prob1E: 5 iterations in 0.06 seconds (average 0.012000, setup 0.00)

Warning: PRISM hasn't checked for zero-reward loops.
Your minimum rewards may be too low...

goal = 1, inf = 0, maybe = 281528

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=281529, nc=368828, nnz=730649, k=4] [10.8 MB]
Building sparse matrix (transition rewards)... [n=281529, nc=368828, nnz=7841, k=4] [2.6 MB]
Creating vector for state rewards... [2.1 MB]
Creating vector for inf... [2.1 MB]
Allocating iteration vectors... [2 x 2.1 MB]
TOTAL: [22.0 MB]

Starting iterations...

Iterative method: 100 iterations in 0.80 seconds (average 0.004120, setup 0.38)

Value in the initial state: 0.0518872992659884

Time for model checking: 0.902 seconds.

Result: 0.0518872992659884 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"time"}max=? [ F "deadlock" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=3,Objy1=0,Objx2=4,Objy2=3

Prob0E: 4 iterations in 0.04 seconds (average 0.009000, setup 0.00)

Prob1A: 1 iterations in 0.01 seconds (average 0.008000, setup 0.00)

goal = 1, inf = 0, maybe = 281528

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=281529, nc=368828, nnz=730649, k=4] [10.8 MB]
Building sparse matrix (transition rewards)... [n=281529, nc=368828, nnz=141338, k=4] [4.1 MB]
Creating vector for state rewards... [2.1 MB]
Creating vector for inf... [2.1 MB]
Allocating iteration vectors... [2 x 2.1 MB]
TOTAL: [23.5 MB]

Starting iterations...

Iterative method: 98 iterations in 0.83 seconds (average 0.004163, setup 0.42)

Value in the initial state: 171.82449474621856

Time for model checking: 0.893 seconds.

Result: 171.82449474621856 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"time"}min=? [ F "deadlock" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=3,Objy1=0,Objx2=4,Objy2=3

Prob0A: 4 iterations in 0.04 seconds (average 0.009000, setup 0.00)

Prob1E: 5 iterations in 0.06 seconds (average 0.012000, setup 0.00)

Warning: PRISM hasn't checked for zero-reward loops.
Your minimum rewards may be too low...

goal = 1, inf = 0, maybe = 281528

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=281529, nc=368828, nnz=730649, k=4] [10.8 MB]
Building sparse matrix (transition rewards)... [n=281529, nc=368828, nnz=141338, k=4] [4.1 MB]
Creating vector for state rewards... [2.1 MB]
Creating vector for inf... [2.1 MB]
Allocating iteration vectors... [2 x 2.1 MB]
TOTAL: [23.5 MB]

Starting iterations...

Iterative method: 105 iterations in 0.86 seconds (average 0.004152, setup 0.42)

Value in the initial state: 98.209183082756

Time for model checking: 0.964 seconds.

Result: 98.209183082756 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"success"}max=? [ F "deadlock" ]/Pmin=? [ F "MissionSuccessful" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=3,Objy1=0,Objx2=4,Objy2=3

Prob0E: 4 iterations in 0.03 seconds (average 0.007000, setup 0.00)

Prob1A: 1 iterations in 0.01 seconds (average 0.008000, setup 0.00)

goal = 1, inf = 0, maybe = 281528

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=281529, nc=368828, nnz=730649, k=4] [10.8 MB]
Building sparse matrix (transition rewards)... [n=281529, nc=368828, nnz=2270, k=4] [2.5 MB]
Creating vector for state rewards... [2.1 MB]
Creating vector for inf... [2.1 MB]
Allocating iteration vectors... [2 x 2.1 MB]
TOTAL: [21.9 MB]

Starting iterations...

Iterative method: 100 iterations in 0.77 seconds (average 0.004000, setup 0.37)

Prob0E: 62 iterations in 0.37 seconds (average 0.006000, setup 0.00)

Prob1A: 3 iterations in 0.05 seconds (average 0.017333, setup 0.00)

yes = 1152, no = 245363, maybe = 35014

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=281529, nc=59256, nnz=163990, k=2] [2.2 MB]
Creating vector for yes... [2.1 MB]
Allocating iteration vectors... [2 x 2.1 MB]
TOTAL: [8.6 MB]

Starting iterations...

Iterative method: 94 iterations in 0.35 seconds (average 0.001787, setup 0.18)

Value in the initial state: 225.598123077308

Time for model checking: 1.608 seconds.

Result: 225.598123077308 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"success"}min=? [ F "deadlock" ]/Pmax=? [ F "MissionSuccessful" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=3,Objy1=0,Objx2=4,Objy2=3

Prob0A: 4 iterations in 0.09 seconds (average 0.023000, setup 0.00)

Prob1E: 5 iterations in 0.07 seconds (average 0.013600, setup 0.00)

Warning: PRISM hasn't checked for zero-reward loops.
Your minimum rewards may be too low...

goal = 1, inf = 0, maybe = 281528

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=281529, nc=368828, nnz=730649, k=4] [10.8 MB]
Building sparse matrix (transition rewards)... [n=281529, nc=368828, nnz=2270, k=4] [2.5 MB]
Creating vector for state rewards... [2.1 MB]
Creating vector for inf... [2.1 MB]
Allocating iteration vectors... [2 x 2.1 MB]
TOTAL: [21.9 MB]

Starting iterations...

Iterative method: 98 iterations in 0.85 seconds (average 0.004000, setup 0.46)

Prob0A: 60 iterations in 0.39 seconds (average 0.006467, setup 0.00)

Prob1E: 4 iterations in 0.05 seconds (average 0.012000, setup 0.00)

yes = 1152, no = 239179, maybe = 41198

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=281529, nc=69439, nnz=192764, k=2] [2.5 MB]
Creating vector for yes... [2.1 MB]
Allocating iteration vectors... [2 x 2.1 MB]
TOTAL: [9.0 MB]

Starting iterations...

Iterative method: 99 iterations in 0.36 seconds (average 0.001697, setup 0.20)

Value in the initial state: 79.31205377177604

Time for model checking: 1.85 seconds.

Result: 79.31205377177604 (value in the initial state)

---------------------------------------------------------------------

Note: There were 16 warnings during computation.

