PRISM
=====

Version: 4.3
Date: Wed Aug 03 16:13:45 BST 2016
Hostname: eglinton
Memory limits: cudd=1g, java(heap)=981.5m
Command line: prism uav_sim_mdp.prism -s uav_mdp.props -const 'Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=3,Objy1=1,Objx2=4,Objy2=3,'

Parsing model file "uav_sim_mdp.prism"...

Parsing properties file "uav_mdp.props"...

18 properties:
(1) Pmax=? [ F "MissionSuccessful" ]
(2) Pmin=? [ F "MissionSuccessful" ]
(3) Pmax=? [ F "MissionTimeUp" ]
(4) Pmin=? [ F "MissionTimeUp" ]
(5) Pmax=? [ F "Crash" ]
(6) Pmin=? [ F "Crash" ]
(7) Pmax=? [ F "AllAreaSearched" ]
(8) Pmin=? [ F "AllAreaSearched" ]
(9) Pmax=? [ F "MissionTimeUp"|"AllAreaSearched"|"Crash" ]
(10) Pmin=? [ F "MissionTimeUp"|"AllAreaSearched"|"Crash" ]
(11) R{"drop"}max=? [ F "deadlock" ]
(12) R{"drop"}min=? [ F "deadlock" ]
(13) R{"charge"}max=? [ F "deadlock" ]
(14) R{"charge"}min=? [ F "deadlock" ]
(15) R{"time"}max=? [ F "deadlock" ]
(16) R{"time"}min=? [ F "deadlock" ]
(17) R{"success"}max=? [ F "deadlock" ]/Pmin=? [ F "MissionSuccessful" ]
(18) R{"success"}min=? [ F "deadlock" ]/Pmax=? [ F "MissionSuccessful" ]

Type:        MDP
Modules:     Obj Move Robot T 
Variables:   NoOfObjs posx posy s1 retx rety b t c 

---------------------------------------------------------------------

Model checking: Pmax=? [ F "MissionSuccessful" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=3,Objy1=1,Objx2=4,Objy2=3

Building model...
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=3,Objy1=1,Objx2=4,Objy2=3

Warning: Guard for command 7 of module "Move" is never satisfied.

Warning: Guard for command 8 of module "Move" is never satisfied.

Warning: Guard for command 9 of module "Move" is never satisfied.

Warning: Guard for command 7 of module "Robot" is never satisfied.

Warning: Update 2 of command 3 of module "Robot" doesn't do anything ("(s1'=13)", line 210, column 44)

Warning: Update 2 of command 27 of module "T" doesn't do anything ("(c'=1)", line 323, column 108)

Warning: Guard for command 28 of module "T" is never satisfied.

Warning: Update 2 of command 9 of module "T" doesn't do anything ("(c'=1)", line 300, column 112)

Warning: Guard for command 10 of module "T" is never satisfied.

Warning: Update 2 of command 51 of module "T" doesn't do anything ("(c'=1)", line 354, column 113)

Warning: Guard for command 52 of module "T" is never satisfied.

Computing reachable states...

Reachability (BFS): 88 iterations in 1.23 seconds (average 0.013955, setup 0.00)

Time for model construction: 3.027 seconds.

Warning: Deadlocks detected and fixed in 1 states

Type:        MDP
States:      238168 (1 initial)
Transitions: 629000
Choices:     315897

Transition matrix: 215581 nodes (91 terminal), 629000 minterms, vars: 34r/34c/18nd

Prob0A: 52 iterations in 0.40 seconds (average 0.007769, setup 0.00)

Prob1E: 4 iterations in 0.06 seconds (average 0.014000, setup 0.00)

yes = 1157, no = 197446, maybe = 39565

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=238168, nc=67928, nnz=188995, k=2] [2.5 MB]
Creating vector for yes... [1.8 MB]
Allocating iteration vectors... [2 x 1.8 MB]
TOTAL: [7.9 MB]

Starting iterations...

Iterative method: 97 iterations in 0.40 seconds (average 0.001691, setup 0.23)

Value in the initial state: 0.9813606132397965

Time for model checking: 0.876 seconds.

Result: 0.9813606132397965 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmin=? [ F "MissionSuccessful" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=3,Objy1=1,Objx2=4,Objy2=3

Prob0E: 53 iterations in 0.42 seconds (average 0.007925, setup 0.00)

Prob1A: 3 iterations in 0.06 seconds (average 0.018667, setup 0.00)

yes = 1157, no = 202849, maybe = 34162

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=238168, nc=58562, nnz=162462, k=2] [2.1 MB]
Creating vector for yes... [1.8 MB]
Allocating iteration vectors... [2 x 1.8 MB]
TOTAL: [7.6 MB]

Starting iterations...

Iterative method: 92 iterations in 0.34 seconds (average 0.001565, setup 0.20)

Value in the initial state: 0.8376190561860457

Time for model checking: 0.841 seconds.

Result: 0.8376190561860457 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmax=? [ F "MissionTimeUp" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=3,Objy1=1,Objx2=4,Objy2=3

Prob0A: 72 iterations in 0.74 seconds (average 0.010278, setup 0.00)

Prob1E: 4 iterations in 0.06 seconds (average 0.015000, setup 0.00)

yes = 360, no = 140732, maybe = 97076

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=238168, nc=161701, nnz=436727, k=4] [5.4 MB]
Creating vector for yes... [1.8 MB]
Allocating iteration vectors... [2 x 1.8 MB]
TOTAL: [10.8 MB]

Starting iterations...

Iterative method: 98 iterations in 0.51 seconds (average 0.002041, setup 0.31)

Value in the initial state: 1.6928179079169118E-4

Time for model checking: 1.359 seconds.

Result: 1.6928179079169118E-4 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmin=? [ F "MissionTimeUp" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=3,Objy1=1,Objx2=4,Objy2=3

Prob0E: 87 iterations in 0.88 seconds (average 0.010161, setup 0.00)

Prob1A: 3 iterations in 0.04 seconds (average 0.014667, setup 0.00)

yes = 360, no = 141022, maybe = 96786

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=238168, nc=161192, nnz=435655, k=4] [5.4 MB]
Creating vector for yes... [1.8 MB]
Allocating iteration vectors... [2 x 1.8 MB]
TOTAL: [10.8 MB]

Starting iterations...

Iterative method: 114 iterations in 0.52 seconds (average 0.002140, setup 0.28)

Value in the initial state: 2.0512990402273744E-7

Time for model checking: 1.476 seconds.

Result: 2.0512990402273744E-7 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmax=? [ F "Crash" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=3,Objy1=1,Objx2=4,Objy2=3

Prob0A: 3 iterations in 0.08 seconds (average 0.026667, setup 0.00)

Prob1E: 200 iterations in 2.66 seconds (average 0.013280, setup 0.00)

yes = 116809, no = 3202, maybe = 118157

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=238168, nc=195886, nnz=508989, k=4] [6.2 MB]
Creating vector for yes... [1.8 MB]
Allocating iteration vectors... [2 x 1.8 MB]
TOTAL: [11.7 MB]

Starting iterations...

Iterative method: 94 iterations in 0.54 seconds (average 0.002213, setup 0.34)

Value in the initial state: 0.029881345531927728

Time for model checking: 3.318 seconds.

Result: 0.029881345531927728 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmin=? [ F "Crash" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=3,Objy1=1,Objx2=4,Objy2=3

Prob0E: 3 iterations in 0.06 seconds (average 0.018667, setup 0.00)

Prob1A: 50 iterations in 0.46 seconds (average 0.009200, setup 0.00)

yes = 116809, no = 3202, maybe = 118157

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=238168, nc=195886, nnz=508989, k=4] [6.2 MB]
Creating vector for yes... [1.8 MB]
Allocating iteration vectors... [2 x 1.8 MB]
TOTAL: [11.7 MB]

Starting iterations...

Iterative method: 88 iterations in 0.45 seconds (average 0.002318, setup 0.25)

Value in the initial state: 0.016586403030449847

Time for model checking: 0.987 seconds.

Result: 0.016586403030449847 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmax=? [ F "AllAreaSearched" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=3,Objy1=1,Objx2=4,Objy2=3

Prob0A: 53 iterations in 0.39 seconds (average 0.007321, setup 0.00)

Prob1E: 4 iterations in 0.05 seconds (average 0.012000, setup 0.00)

yes = 1684, no = 175256, maybe = 61228

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=238168, nc=99713, nnz=268467, k=2] [3.4 MB]
Creating vector for yes... [1.8 MB]
Allocating iteration vectors... [2 x 1.8 MB]
TOTAL: [8.8 MB]

Starting iterations...

Iterative method: 87 iterations in 0.39 seconds (average 0.001885, setup 0.23)

Value in the initial state: 0.13752075866558097

Time for model checking: 0.841 seconds.

Result: 0.13752075866558097 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmin=? [ F "AllAreaSearched" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=3,Objy1=1,Objx2=4,Objy2=3

Prob0E: 62 iterations in 0.47 seconds (average 0.007613, setup 0.00)

Prob1A: 3 iterations in 0.04 seconds (average 0.012000, setup 0.00)

yes = 1684, no = 182946, maybe = 53538

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=238168, nc=86733, nnz=231629, k=2] [3.0 MB]
Creating vector for yes... [1.8 MB]
Allocating iteration vectors... [2 x 1.8 MB]
TOTAL: [8.4 MB]

Starting iterations...

Iterative method: 96 iterations in 0.34 seconds (average 0.001750, setup 0.17)

Value in the initial state: 0.0018038101739498756

Time for model checking: 0.859 seconds.

Result: 0.0018038101739498756 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmax=? [ F "MissionTimeUp"|"AllAreaSearched"|"Crash" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=3,Objy1=1,Objx2=4,Objy2=3

Prob0A: 3 iterations in 0.08 seconds (average 0.026667, setup 0.00)

Prob1E: 204 iterations in 3.00 seconds (average 0.014725, setup 0.00)

yes = 202848, no = 1158, maybe = 34162

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=238168, nc=58562, nnz=162462, k=2] [2.1 MB]
Creating vector for yes... [1.8 MB]
Allocating iteration vectors... [2 x 1.8 MB]
TOTAL: [7.6 MB]

Starting iterations...

Iterative method: 86 iterations in 0.33 seconds (average 0.001581, setup 0.19)

Value in the initial state: 0.16238072993471434

Time for model checking: 3.452 seconds.

Result: 0.16238072993471434 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmin=? [ F "MissionTimeUp"|"AllAreaSearched"|"Crash" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=3,Objy1=1,Objx2=4,Objy2=3

Prob0E: 3 iterations in 0.07 seconds (average 0.022667, setup 0.00)

Prob1A: 52 iterations in 0.53 seconds (average 0.010154, setup 0.00)

yes = 197445, no = 1158, maybe = 39565

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=238168, nc=67928, nnz=188995, k=2] [2.5 MB]
Creating vector for yes... [1.8 MB]
Allocating iteration vectors... [2 x 1.8 MB]
TOTAL: [7.9 MB]

Starting iterations...

Iterative method: 95 iterations in 0.33 seconds (average 0.001684, setup 0.17)

Value in the initial state: 0.01863834660354314

Time for model checking: 0.943 seconds.

Result: 0.01863834660354314 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"drop"}max=? [ F "deadlock" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=3,Objy1=1,Objx2=4,Objy2=3

Prob0E: 4 iterations in 0.07 seconds (average 0.017000, setup 0.00)

Prob1A: 1 iterations in 0.01 seconds (average 0.008000, setup 0.00)

goal = 1, inf = 0, maybe = 238167

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=238168, nc=315896, nnz=628999, k=4] [9.3 MB]
Building sparse matrix (transition rewards)... [n=238168, nc=315896, nnz=0, k=4] [2.1 MB]
Creating vector for state rewards... [1.8 MB]
Creating vector for inf... [1.8 MB]
Allocating iteration vectors... [2 x 1.8 MB]
TOTAL: [18.7 MB]

Starting iterations...

Iterative method: 93 iterations in 0.74 seconds (average 0.003441, setup 0.42)

Value in the initial state: 0.18261843851735884

Time for model checking: 0.829 seconds.

Result: 0.18261843851735884 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"drop"}min=? [ F "deadlock" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=3,Objy1=1,Objx2=4,Objy2=3

Prob0A: 4 iterations in 0.05 seconds (average 0.012000, setup 0.00)

Prob1E: 5 iterations in 0.06 seconds (average 0.011200, setup 0.00)

Warning: PRISM hasn't checked for zero-reward loops.
Your minimum rewards may be too low...

goal = 1, inf = 0, maybe = 238167

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=238168, nc=315896, nnz=628999, k=4] [9.3 MB]
Building sparse matrix (transition rewards)... [n=238168, nc=315896, nnz=0, k=4] [2.1 MB]
Creating vector for state rewards... [1.8 MB]
Creating vector for inf... [1.8 MB]
Allocating iteration vectors... [2 x 1.8 MB]
TOTAL: [18.7 MB]

Starting iterations...

Iterative method: 85 iterations in 0.62 seconds (average 0.003435, setup 0.33)

Value in the initial state: 0.1592772403433395

Time for model checking: 0.739 seconds.

Result: 0.1592772403433395 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"charge"}max=? [ F "deadlock" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=3,Objy1=1,Objx2=4,Objy2=3

Prob0E: 4 iterations in 0.03 seconds (average 0.007000, setup 0.00)

Prob1A: 1 iterations in 0.01 seconds (average 0.008000, setup 0.00)

goal = 1, inf = 0, maybe = 238167

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=238168, nc=315896, nnz=628999, k=4] [9.3 MB]
Building sparse matrix (transition rewards)... [n=238168, nc=315896, nnz=6908, k=4] [2.2 MB]
Creating vector for state rewards... [1.8 MB]
Creating vector for inf... [1.8 MB]
Allocating iteration vectors... [2 x 1.8 MB]
TOTAL: [18.8 MB]

Starting iterations...

Iterative method: 92 iterations in 0.65 seconds (average 0.003435, setup 0.33)

Value in the initial state: 0.27336645147039884

Time for model checking: 0.699 seconds.

Result: 0.27336645147039884 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"charge"}min=? [ F "deadlock" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=3,Objy1=1,Objx2=4,Objy2=3

Prob0A: 4 iterations in 0.03 seconds (average 0.007000, setup 0.00)

Prob1E: 5 iterations in 0.05 seconds (average 0.010400, setup 0.00)

Warning: PRISM hasn't checked for zero-reward loops.
Your minimum rewards may be too low...

goal = 1, inf = 0, maybe = 238167

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=238168, nc=315896, nnz=628999, k=4] [9.3 MB]
Building sparse matrix (transition rewards)... [n=238168, nc=315896, nnz=6908, k=4] [2.2 MB]
Creating vector for state rewards... [1.8 MB]
Creating vector for inf... [1.8 MB]
Allocating iteration vectors... [2 x 1.8 MB]
TOTAL: [18.8 MB]

Starting iterations...

Iterative method: 99 iterations in 0.67 seconds (average 0.003475, setup 0.32)

Value in the initial state: 0.039921605719268664

Time for model checking: 0.767 seconds.

Result: 0.039921605719268664 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"time"}max=? [ F "deadlock" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=3,Objy1=1,Objx2=4,Objy2=3

Prob0E: 4 iterations in 0.02 seconds (average 0.006000, setup 0.00)

Prob1A: 1 iterations in 0.01 seconds (average 0.008000, setup 0.00)

goal = 1, inf = 0, maybe = 238167

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=238168, nc=315896, nnz=628999, k=4] [9.3 MB]
Building sparse matrix (transition rewards)... [n=238168, nc=315896, nnz=120009, k=4] [3.5 MB]
Creating vector for state rewards... [1.8 MB]
Creating vector for inf... [1.8 MB]
Allocating iteration vectors... [2 x 1.8 MB]
TOTAL: [20.1 MB]

Starting iterations...

Iterative method: 97 iterations in 0.71 seconds (average 0.003546, setup 0.37)

Value in the initial state: 164.94301493243228

Time for model checking: 0.76 seconds.

Result: 164.94301493243228 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"time"}min=? [ F "deadlock" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=3,Objy1=1,Objx2=4,Objy2=3

Prob0A: 4 iterations in 0.02 seconds (average 0.006000, setup 0.00)

Prob1E: 5 iterations in 0.05 seconds (average 0.010400, setup 0.00)

Warning: PRISM hasn't checked for zero-reward loops.
Your minimum rewards may be too low...

goal = 1, inf = 0, maybe = 238167

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=238168, nc=315896, nnz=628999, k=4] [9.3 MB]
Building sparse matrix (transition rewards)... [n=238168, nc=315896, nnz=120009, k=4] [3.5 MB]
Creating vector for state rewards... [1.8 MB]
Creating vector for inf... [1.8 MB]
Allocating iteration vectors... [2 x 1.8 MB]
TOTAL: [20.1 MB]

Starting iterations...

Iterative method: 101 iterations in 0.73 seconds (average 0.003604, setup 0.36)

Value in the initial state: 91.96834287491373

Time for model checking: 0.818 seconds.

Result: 91.96834287491373 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"success"}max=? [ F "deadlock" ]/Pmin=? [ F "MissionSuccessful" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=3,Objy1=1,Objx2=4,Objy2=3

Prob0E: 4 iterations in 0.03 seconds (average 0.008000, setup 0.00)

Prob1A: 1 iterations in 0.01 seconds (average 0.008000, setup 0.00)

goal = 1, inf = 0, maybe = 238167

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=238168, nc=315896, nnz=628999, k=4] [9.3 MB]
Building sparse matrix (transition rewards)... [n=238168, nc=315896, nnz=2280, k=4] [2.1 MB]
Creating vector for state rewards... [1.8 MB]
Creating vector for inf... [1.8 MB]
Allocating iteration vectors... [2 x 1.8 MB]
TOTAL: [18.7 MB]

Starting iterations...

Iterative method: 99 iterations in 0.64 seconds (average 0.003313, setup 0.31)

Prob0E: 53 iterations in 0.39 seconds (average 0.007396, setup 0.00)

Prob1A: 3 iterations in 0.05 seconds (average 0.016000, setup 0.00)

yes = 1157, no = 202849, maybe = 34162

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=238168, nc=58562, nnz=162462, k=2] [2.1 MB]
Creating vector for yes... [1.8 MB]
Allocating iteration vectors... [2 x 1.8 MB]
TOTAL: [7.6 MB]

Starting iterations...

Iterative method: 92 iterations in 0.30 seconds (average 0.001522, setup 0.16)

Value in the initial state: 193.1800816264632

Time for model checking: 1.476 seconds.

Result: 193.1800816264632 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"success"}min=? [ F "deadlock" ]/Pmax=? [ F "MissionSuccessful" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=3,Objy1=1,Objx2=4,Objy2=3

Prob0A: 4 iterations in 0.06 seconds (average 0.015000, setup 0.00)

Prob1E: 5 iterations in 0.06 seconds (average 0.012800, setup 0.00)

Warning: PRISM hasn't checked for zero-reward loops.
Your minimum rewards may be too low...

goal = 1, inf = 0, maybe = 238167

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=238168, nc=315896, nnz=628999, k=4] [9.3 MB]
Building sparse matrix (transition rewards)... [n=238168, nc=315896, nnz=2280, k=4] [2.1 MB]
Creating vector for state rewards... [1.8 MB]
Creating vector for inf... [1.8 MB]
Allocating iteration vectors... [2 x 1.8 MB]
TOTAL: [18.7 MB]

Starting iterations...

Iterative method: 97 iterations in 0.71 seconds (average 0.003464, setup 0.38)

Prob0A: 52 iterations in 0.36 seconds (average 0.006923, setup 0.00)

Prob1E: 4 iterations in 0.04 seconds (average 0.010000, setup 0.00)

yes = 1157, no = 197446, maybe = 39565

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=238168, nc=67928, nnz=188995, k=2] [2.5 MB]
Creating vector for yes... [1.8 MB]
Allocating iteration vectors... [2 x 1.8 MB]
TOTAL: [7.9 MB]

Starting iterations...

Iterative method: 97 iterations in 0.31 seconds (average 0.001567, setup 0.16)

Value in the initial state: 79.70071707078904

Time for model checking: 1.577 seconds.

Result: 79.70071707078904 (value in the initial state)

---------------------------------------------------------------------

Note: There were 16 warnings during computation.

