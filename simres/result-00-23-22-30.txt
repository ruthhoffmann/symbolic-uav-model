PRISM
=====

Version: 4.3
Date: Wed Aug 03 15:57:54 BST 2016
Hostname: eglinton
Memory limits: cudd=1g, java(heap)=981.5m
Command line: prism uav_sim_mdp.prism -s uav_mdp.props -const 'Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=2,Objy1=2,Objx2=3,Objy2=0,'

Parsing model file "uav_sim_mdp.prism"...

Parsing properties file "uav_mdp.props"...

18 properties:
(1) Pmax=? [ F "MissionSuccessful" ]
(2) Pmin=? [ F "MissionSuccessful" ]
(3) Pmax=? [ F "MissionTimeUp" ]
(4) Pmin=? [ F "MissionTimeUp" ]
(5) Pmax=? [ F "Crash" ]
(6) Pmin=? [ F "Crash" ]
(7) Pmax=? [ F "AllAreaSearched" ]
(8) Pmin=? [ F "AllAreaSearched" ]
(9) Pmax=? [ F "MissionTimeUp"|"AllAreaSearched"|"Crash" ]
(10) Pmin=? [ F "MissionTimeUp"|"AllAreaSearched"|"Crash" ]
(11) R{"drop"}max=? [ F "deadlock" ]
(12) R{"drop"}min=? [ F "deadlock" ]
(13) R{"charge"}max=? [ F "deadlock" ]
(14) R{"charge"}min=? [ F "deadlock" ]
(15) R{"time"}max=? [ F "deadlock" ]
(16) R{"time"}min=? [ F "deadlock" ]
(17) R{"success"}max=? [ F "deadlock" ]/Pmin=? [ F "MissionSuccessful" ]
(18) R{"success"}min=? [ F "deadlock" ]/Pmax=? [ F "MissionSuccessful" ]

Type:        MDP
Modules:     Obj Move Robot T 
Variables:   NoOfObjs posx posy s1 retx rety b t c 

---------------------------------------------------------------------

Model checking: Pmax=? [ F "MissionSuccessful" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=2,Objy1=2,Objx2=3,Objy2=0

Building model...
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=2,Objy1=2,Objx2=3,Objy2=0

Warning: Guard for command 7 of module "Move" is never satisfied.

Warning: Guard for command 8 of module "Move" is never satisfied.

Warning: Guard for command 9 of module "Move" is never satisfied.

Warning: Guard for command 7 of module "Robot" is never satisfied.

Warning: Update 2 of command 3 of module "Robot" doesn't do anything ("(s1'=13)", line 210, column 44)

Warning: Update 2 of command 27 of module "T" doesn't do anything ("(c'=1)", line 323, column 108)

Warning: Guard for command 28 of module "T" is never satisfied.

Warning: Update 2 of command 9 of module "T" doesn't do anything ("(c'=1)", line 300, column 112)

Warning: Guard for command 10 of module "T" is never satisfied.

Warning: Update 2 of command 51 of module "T" doesn't do anything ("(c'=1)", line 354, column 113)

Warning: Guard for command 52 of module "T" is never satisfied.

Computing reachable states...

Reachability (BFS): 89 iterations in 1.49 seconds (average 0.016719, setup 0.00)

Time for model construction: 3.578 seconds.

Warning: Deadlocks detected and fixed in 1 states

Type:        MDP
States:      243265 (1 initial)
Transitions: 611699
Choices:     315096

Transition matrix: 216357 nodes (91 terminal), 611699 minterms, vars: 34r/34c/18nd

Prob0A: 52 iterations in 0.42 seconds (average 0.008000, setup 0.00)

Prob1E: 4 iterations in 0.06 seconds (average 0.014000, setup 0.00)

yes = 1132, no = 205892, maybe = 36241

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=243265, nc=62009, nnz=171003, k=2] [2.2 MB]
Creating vector for yes... [1.9 MB]
Allocating iteration vectors... [2 x 1.9 MB]
TOTAL: [7.8 MB]

Starting iterations...

Iterative method: 91 iterations in 0.41 seconds (average 0.001758, setup 0.25)

Value in the initial state: 0.983666061081481

Time for model checking: 0.91 seconds.

Result: 0.983666061081481 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmin=? [ F "MissionSuccessful" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=2,Objy1=2,Objx2=3,Objy2=0

Prob0E: 54 iterations in 0.44 seconds (average 0.008148, setup 0.00)

Prob1A: 3 iterations in 0.06 seconds (average 0.018667, setup 0.00)

yes = 1132, no = 212131, maybe = 30002

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=243265, nc=51186, nnz=139843, k=2] [1.9 MB]
Creating vector for yes... [1.9 MB]
Allocating iteration vectors... [2 x 1.9 MB]
TOTAL: [7.4 MB]

Starting iterations...

Iterative method: 88 iterations in 0.35 seconds (average 0.001591, setup 0.21)

Value in the initial state: 0.8465499231449127

Time for model checking: 0.872 seconds.

Result: 0.8465499231449127 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmax=? [ F "MissionTimeUp" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=2,Objy1=2,Objx2=3,Objy2=0

Prob0A: 67 iterations in 0.82 seconds (average 0.012179, setup 0.00)

Prob1E: 4 iterations in 0.14 seconds (average 0.034000, setup 0.00)

yes = 420, no = 151411, maybe = 91434

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=243265, nc=151008, nnz=402950, k=4] [5.0 MB]
Creating vector for yes... [1.9 MB]
Allocating iteration vectors... [2 x 1.9 MB]
TOTAL: [10.6 MB]

Starting iterations...

Iterative method: 95 iterations in 0.50 seconds (average 0.002063, setup 0.30)

Value in the initial state: 1.1106844656051974E-4

Time for model checking: 1.463 seconds.

Result: 1.1106844656051974E-4 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmin=? [ F "MissionTimeUp" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=2,Objy1=2,Objx2=3,Objy2=0

Prob0E: 83 iterations in 0.80 seconds (average 0.009687, setup 0.00)

Prob1A: 3 iterations in 0.05 seconds (average 0.017333, setup 0.00)

yes = 420, no = 151757, maybe = 91088

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=243265, nc=150406, nnz=401552, k=4] [5.0 MB]
Creating vector for yes... [1.9 MB]
Allocating iteration vectors... [2 x 1.9 MB]
TOTAL: [10.5 MB]

Starting iterations...

Iterative method: 113 iterations in 0.53 seconds (average 0.002159, setup 0.28)

Value in the initial state: 1.0656288511440022E-7

Time for model checking: 1.403 seconds.

Result: 1.0656288511440022E-7 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmax=? [ F "Crash" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=2,Objy1=2,Objx2=3,Objy2=0

Prob0A: 3 iterations in 0.08 seconds (average 0.026667, setup 0.00)

Prob1E: 199 iterations in 2.82 seconds (average 0.014171, setup 0.00)

yes = 119486, no = 3071, maybe = 120708

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=243265, nc=192539, nnz=489142, k=4] [6.0 MB]
Creating vector for yes... [1.9 MB]
Allocating iteration vectors... [2 x 1.9 MB]
TOTAL: [11.6 MB]

Starting iterations...

Iterative method: 90 iterations in 0.56 seconds (average 0.002267, setup 0.35)

Value in the initial state: 0.024483894299848187

Time for model checking: 3.485 seconds.

Result: 0.024483894299848187 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmin=? [ F "Crash" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=2,Objy1=2,Objx2=3,Objy2=0

Prob0E: 4 iterations in 0.08 seconds (average 0.019000, setup 0.00)

Prob1A: 48 iterations in 0.51 seconds (average 0.010667, setup 0.00)

yes = 119486, no = 3071, maybe = 120708

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=243265, nc=192539, nnz=489142, k=4] [6.0 MB]
Creating vector for yes... [1.9 MB]
Allocating iteration vectors... [2 x 1.9 MB]
TOTAL: [11.6 MB]

Starting iterations...

Iterative method: 82 iterations in 0.45 seconds (average 0.002390, setup 0.25)

Value in the initial state: 0.014289946748745317

Time for model checking: 1.063 seconds.

Result: 0.014289946748745317 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmax=? [ F "AllAreaSearched" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=2,Objy1=2,Objx2=3,Objy2=0

Prob0A: 60 iterations in 0.50 seconds (average 0.008267, setup 0.00)

Prob1E: 4 iterations in 0.05 seconds (average 0.012000, setup 0.00)

yes = 1518, no = 188183, maybe = 53564

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=243265, nc=80033, nnz=201342, k=2] [2.6 MB]
Creating vector for yes... [1.9 MB]
Allocating iteration vectors... [2 x 1.9 MB]
TOTAL: [8.2 MB]

Starting iterations...

Iterative method: 86 iterations in 0.36 seconds (average 0.001767, setup 0.20)

Value in the initial state: 0.13057366641745657

Time for model checking: 0.934 seconds.

Result: 0.13057366641745657 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmin=? [ F "AllAreaSearched" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=2,Objy1=2,Objx2=3,Objy2=0

Prob0E: 63 iterations in 0.51 seconds (average 0.008063, setup 0.00)

Prob1A: 3 iterations in 0.05 seconds (average 0.017333, setup 0.00)

yes = 1518, no = 194874, maybe = 46873

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=243265, nc=69554, nnz=173480, k=2] [2.3 MB]
Creating vector for yes... [1.9 MB]
Allocating iteration vectors... [2 x 1.9 MB]
TOTAL: [7.9 MB]

Starting iterations...

Iterative method: 94 iterations in 0.32 seconds (average 0.001617, setup 0.16)

Value in the initial state: 0.0018445601056075033

Time for model checking: 0.897 seconds.

Result: 0.0018445601056075033 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmax=? [ F "MissionTimeUp"|"AllAreaSearched"|"Crash" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=2,Objy1=2,Objx2=3,Objy2=0

Prob0A: 3 iterations in 0.08 seconds (average 0.028000, setup 0.00)

Prob1E: 208 iterations in 2.82 seconds (average 0.013558, setup 0.00)

yes = 212130, no = 1133, maybe = 30002

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=243265, nc=51186, nnz=139843, k=2] [1.9 MB]
Creating vector for yes... [1.9 MB]
Allocating iteration vectors... [2 x 1.9 MB]
TOTAL: [7.4 MB]

Starting iterations...

Iterative method: 81 iterations in 0.34 seconds (average 0.001679, setup 0.20)

Value in the initial state: 0.15344989952137184

Time for model checking: 3.267 seconds.

Result: 0.15344989952137184 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmin=? [ F "MissionTimeUp"|"AllAreaSearched"|"Crash" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=2,Objy1=2,Objx2=3,Objy2=0

Prob0E: 4 iterations in 0.07 seconds (average 0.018000, setup 0.00)

Prob1A: 52 iterations in 0.55 seconds (average 0.010538, setup 0.00)

yes = 205891, no = 1133, maybe = 36241

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=243265, nc=62009, nnz=171003, k=2] [2.2 MB]
Creating vector for yes... [1.9 MB]
Allocating iteration vectors... [2 x 1.9 MB]
TOTAL: [7.8 MB]

Starting iterations...

Iterative method: 91 iterations in 0.32 seconds (average 0.001626, setup 0.17)

Value in the initial state: 0.01633243914035309

Time for model checking: 0.971 seconds.

Result: 0.01633243914035309 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"drop"}max=? [ F "deadlock" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=2,Objy1=2,Objx2=3,Objy2=0

Prob0E: 5 iterations in 0.08 seconds (average 0.016800, setup 0.00)

Prob1A: 1 iterations in 0.01 seconds (average 0.008000, setup 0.00)

goal = 1, inf = 0, maybe = 243264

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=243265, nc=315095, nnz=611698, k=4] [9.1 MB]
Building sparse matrix (transition rewards)... [n=243265, nc=315095, nnz=0, k=4] [2.1 MB]
Creating vector for state rewards... [1.9 MB]
Creating vector for inf... [1.9 MB]
Allocating iteration vectors... [2 x 1.9 MB]
TOTAL: [18.7 MB]

Starting iterations...

Iterative method: 87 iterations in 0.76 seconds (average 0.003448, setup 0.46)

Value in the initial state: 0.17236193999002872

Time for model checking: 0.876 seconds.

Result: 0.17236193999002872 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"drop"}min=? [ F "deadlock" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=2,Objy1=2,Objx2=3,Objy2=0

Prob0A: 4 iterations in 0.04 seconds (average 0.011000, setup 0.00)

Prob1E: 5 iterations in 0.05 seconds (average 0.010400, setup 0.00)

Warning: PRISM hasn't checked for zero-reward loops.
Your minimum rewards may be too low...

goal = 1, inf = 0, maybe = 243264

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=243265, nc=315095, nnz=611698, k=4] [9.1 MB]
Building sparse matrix (transition rewards)... [n=243265, nc=315095, nnz=0, k=4] [2.1 MB]
Creating vector for state rewards... [1.9 MB]
Creating vector for inf... [1.9 MB]
Allocating iteration vectors... [2 x 1.9 MB]
TOTAL: [18.7 MB]

Starting iterations...

Iterative method: 79 iterations in 0.60 seconds (average 0.003494, setup 0.33)

Value in the initial state: 0.16382885240440603

Time for model checking: 0.714 seconds.

Result: 0.16382885240440603 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"charge"}max=? [ F "deadlock" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=2,Objy1=2,Objx2=3,Objy2=0

Prob0E: 5 iterations in 0.04 seconds (average 0.007200, setup 0.00)

Prob1A: 1 iterations in 0.01 seconds (average 0.008000, setup 0.00)

goal = 1, inf = 0, maybe = 243264

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=243265, nc=315095, nnz=611698, k=4] [9.1 MB]
Building sparse matrix (transition rewards)... [n=243265, nc=315095, nnz=6061, k=4] [2.2 MB]
Creating vector for state rewards... [1.9 MB]
Creating vector for inf... [1.9 MB]
Allocating iteration vectors... [2 x 1.9 MB]
TOTAL: [18.8 MB]

Starting iterations...

Iterative method: 89 iterations in 0.64 seconds (average 0.003416, setup 0.34)

Value in the initial state: 0.17299427088266725

Time for model checking: 0.702 seconds.

Result: 0.17299427088266725 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"charge"}min=? [ F "deadlock" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=2,Objy1=2,Objx2=3,Objy2=0

Prob0A: 4 iterations in 0.03 seconds (average 0.007000, setup 0.00)

Prob1E: 5 iterations in 0.05 seconds (average 0.009600, setup 0.00)

Warning: PRISM hasn't checked for zero-reward loops.
Your minimum rewards may be too low...

goal = 1, inf = 0, maybe = 243264

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=243265, nc=315095, nnz=611698, k=4] [9.1 MB]
Building sparse matrix (transition rewards)... [n=243265, nc=315095, nnz=6061, k=4] [2.2 MB]
Creating vector for state rewards... [1.9 MB]
Creating vector for inf... [1.9 MB]
Allocating iteration vectors... [2 x 1.9 MB]
TOTAL: [18.8 MB]

Starting iterations...

Iterative method: 96 iterations in 0.66 seconds (average 0.003458, setup 0.33)

Value in the initial state: 0.012421970516639442

Time for model checking: 0.763 seconds.

Result: 0.012421970516639442 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"time"}max=? [ F "deadlock" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=2,Objy1=2,Objx2=3,Objy2=0

Prob0E: 5 iterations in 0.03 seconds (average 0.005600, setup 0.00)

Prob1A: 1 iterations in 0.00 seconds (average 0.004000, setup 0.00)

goal = 1, inf = 0, maybe = 243264

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=243265, nc=315095, nnz=611698, k=4] [9.1 MB]
Building sparse matrix (transition rewards)... [n=243265, nc=315095, nnz=122555, k=4] [3.5 MB]
Creating vector for state rewards... [1.9 MB]
Creating vector for inf... [1.9 MB]
Allocating iteration vectors... [2 x 1.9 MB]
TOTAL: [20.1 MB]

Starting iterations...

Iterative method: 93 iterations in 0.71 seconds (average 0.003484, setup 0.38)

Value in the initial state: 135.33956222017454

Time for model checking: 0.752 seconds.

Result: 135.33956222017454 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"time"}min=? [ F "deadlock" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=2,Objy1=2,Objx2=3,Objy2=0

Prob0A: 4 iterations in 0.02 seconds (average 0.006000, setup 0.00)

Prob1E: 5 iterations in 0.05 seconds (average 0.009600, setup 0.00)

Warning: PRISM hasn't checked for zero-reward loops.
Your minimum rewards may be too low...

goal = 1, inf = 0, maybe = 243264

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=243265, nc=315095, nnz=611698, k=4] [9.1 MB]
Building sparse matrix (transition rewards)... [n=243265, nc=315095, nnz=122555, k=4] [3.5 MB]
Creating vector for state rewards... [1.9 MB]
Creating vector for inf... [1.9 MB]
Allocating iteration vectors... [2 x 1.9 MB]
TOTAL: [20.1 MB]

Starting iterations...

Iterative method: 97 iterations in 0.71 seconds (average 0.003546, setup 0.37)

Value in the initial state: 79.30827059881064

Time for model checking: 0.799 seconds.

Result: 79.30827059881064 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"success"}max=? [ F "deadlock" ]/Pmin=? [ F "MissionSuccessful" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=2,Objy1=2,Objx2=3,Objy2=0

Prob0E: 5 iterations in 0.03 seconds (average 0.006400, setup 0.00)

Prob1A: 1 iterations in 0.01 seconds (average 0.008000, setup 0.00)

goal = 1, inf = 0, maybe = 243264

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=243265, nc=315095, nnz=611698, k=4] [9.1 MB]
Building sparse matrix (transition rewards)... [n=243265, nc=315095, nnz=2232, k=4] [2.2 MB]
Creating vector for state rewards... [1.9 MB]
Creating vector for inf... [1.9 MB]
Allocating iteration vectors... [2 x 1.9 MB]
TOTAL: [18.7 MB]

Starting iterations...

Iterative method: 96 iterations in 0.66 seconds (average 0.003417, setup 0.33)

Prob0E: 54 iterations in 0.43 seconds (average 0.007926, setup 0.00)

Prob1A: 3 iterations in 0.06 seconds (average 0.020000, setup 0.00)

yes = 1132, no = 212131, maybe = 30002

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=243265, nc=51186, nnz=139843, k=2] [1.9 MB]
Creating vector for yes... [1.9 MB]
Allocating iteration vectors... [2 x 1.9 MB]
TOTAL: [7.4 MB]

Starting iterations...

Iterative method: 88 iterations in 0.30 seconds (average 0.001545, setup 0.16)

Value in the initial state: 157.0694785114803

Time for model checking: 1.54 seconds.

Result: 157.0694785114803 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"success"}min=? [ F "deadlock" ]/Pmax=? [ F "MissionSuccessful" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=2,Objy1=2,Objx2=3,Objy2=0

Prob0A: 4 iterations in 0.07 seconds (average 0.017000, setup 0.00)

Prob1E: 5 iterations in 0.05 seconds (average 0.009600, setup 0.00)

Warning: PRISM hasn't checked for zero-reward loops.
Your minimum rewards may be too low...

goal = 1, inf = 0, maybe = 243264

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=243265, nc=315095, nnz=611698, k=4] [9.1 MB]
Building sparse matrix (transition rewards)... [n=243265, nc=315095, nnz=2232, k=4] [2.2 MB]
Creating vector for state rewards... [1.9 MB]
Creating vector for inf... [1.9 MB]
Allocating iteration vectors... [2 x 1.9 MB]
TOTAL: [18.7 MB]

Starting iterations...

Iterative method: 92 iterations in 0.68 seconds (average 0.003435, setup 0.36)

Prob0A: 52 iterations in 0.48 seconds (average 0.009308, setup 0.00)

Prob1E: 4 iterations in 0.07 seconds (average 0.017000, setup 0.00)

yes = 1132, no = 205892, maybe = 36241

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=243265, nc=62009, nnz=171003, k=2] [2.2 MB]
Creating vector for yes... [1.9 MB]
Allocating iteration vectors... [2 x 1.9 MB]
TOTAL: [7.8 MB]

Starting iterations...

Iterative method: 91 iterations in 0.34 seconds (average 0.001582, setup 0.19)

Value in the initial state: 75.55635017574772

Time for model checking: 1.733 seconds.

Result: 75.55635017574772 (value in the initial state)

---------------------------------------------------------------------

Note: There were 16 warnings during computation.

