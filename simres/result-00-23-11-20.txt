PRISM
=====

Version: 4.3
Date: Wed Aug 03 15:54:56 BST 2016
Hostname: eglinton
Memory limits: cudd=1g, java(heap)=981.5m
Command line: prism uav_sim_mdp.prism -s uav_mdp.props -const 'Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=1,Objy1=1,Objx2=2,Objy2=0,'

Parsing model file "uav_sim_mdp.prism"...

Parsing properties file "uav_mdp.props"...

18 properties:
(1) Pmax=? [ F "MissionSuccessful" ]
(2) Pmin=? [ F "MissionSuccessful" ]
(3) Pmax=? [ F "MissionTimeUp" ]
(4) Pmin=? [ F "MissionTimeUp" ]
(5) Pmax=? [ F "Crash" ]
(6) Pmin=? [ F "Crash" ]
(7) Pmax=? [ F "AllAreaSearched" ]
(8) Pmin=? [ F "AllAreaSearched" ]
(9) Pmax=? [ F "MissionTimeUp"|"AllAreaSearched"|"Crash" ]
(10) Pmin=? [ F "MissionTimeUp"|"AllAreaSearched"|"Crash" ]
(11) R{"drop"}max=? [ F "deadlock" ]
(12) R{"drop"}min=? [ F "deadlock" ]
(13) R{"charge"}max=? [ F "deadlock" ]
(14) R{"charge"}min=? [ F "deadlock" ]
(15) R{"time"}max=? [ F "deadlock" ]
(16) R{"time"}min=? [ F "deadlock" ]
(17) R{"success"}max=? [ F "deadlock" ]/Pmin=? [ F "MissionSuccessful" ]
(18) R{"success"}min=? [ F "deadlock" ]/Pmax=? [ F "MissionSuccessful" ]

Type:        MDP
Modules:     Obj Move Robot T 
Variables:   NoOfObjs posx posy s1 retx rety b t c 

---------------------------------------------------------------------

Model checking: Pmax=? [ F "MissionSuccessful" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=1,Objy1=1,Objx2=2,Objy2=0

Building model...
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=1,Objy1=1,Objx2=2,Objy2=0

Warning: Guard for command 7 of module "Move" is never satisfied.

Warning: Guard for command 8 of module "Move" is never satisfied.

Warning: Guard for command 9 of module "Move" is never satisfied.

Warning: Guard for command 7 of module "Robot" is never satisfied.

Warning: Update 2 of command 3 of module "Robot" doesn't do anything ("(s1'=13)", line 210, column 44)

Warning: Update 2 of command 27 of module "T" doesn't do anything ("(c'=1)", line 323, column 108)

Warning: Guard for command 28 of module "T" is never satisfied.

Warning: Update 2 of command 9 of module "T" doesn't do anything ("(c'=1)", line 300, column 112)

Warning: Guard for command 10 of module "T" is never satisfied.

Warning: Update 2 of command 51 of module "T" doesn't do anything ("(c'=1)", line 354, column 113)

Warning: Guard for command 52 of module "T" is never satisfied.

Computing reachable states...

Reachability (BFS): 91 iterations in 1.72 seconds (average 0.018857, setup 0.00)

Time for model construction: 4.488 seconds.

Warning: Deadlocks detected and fixed in 1 states

Type:        MDP
States:      275666 (1 initial)
Transitions: 711233
Choices:     359846

Transition matrix: 220069 nodes (91 terminal), 711233 minterms, vars: 34r/34c/18nd

Prob0A: 50 iterations in 0.49 seconds (average 0.009840, setup 0.00)

Prob1E: 4 iterations in 0.07 seconds (average 0.018000, setup 0.00)

yes = 1140, no = 232925, maybe = 41601

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=275666, nc=72671, nnz=205579, k=2] [2.7 MB]
Creating vector for yes... [2.1 MB]
Allocating iteration vectors... [2 x 2.1 MB]
TOTAL: [9.0 MB]

Starting iterations...

Iterative method: 89 iterations in 0.46 seconds (average 0.001978, setup 0.28)

Value in the initial state: 0.9842395516104221

Time for model checking: 1.174 seconds.

Result: 0.9842395516104221 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmin=? [ F "MissionSuccessful" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=1,Objy1=1,Objx2=2,Objy2=0

Prob0E: 52 iterations in 0.53 seconds (average 0.010231, setup 0.00)

Prob1A: 3 iterations in 0.05 seconds (average 0.016000, setup 0.00)

yes = 1140, no = 239516, maybe = 35010

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=275666, nc=60968, nnz=171786, k=2] [2.3 MB]
Creating vector for yes... [2.1 MB]
Allocating iteration vectors... [2 x 2.1 MB]
TOTAL: [8.6 MB]

Starting iterations...

Iterative method: 85 iterations in 0.34 seconds (average 0.001929, setup 0.18)

Value in the initial state: 0.9206264667686761

Time for model checking: 1.007 seconds.

Result: 0.9206264667686761 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmax=? [ F "MissionTimeUp" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=1,Objy1=1,Objx2=2,Objy2=0

Prob0A: 66 iterations in 0.72 seconds (average 0.010848, setup 0.00)

Prob1E: 4 iterations in 0.04 seconds (average 0.011000, setup 0.00)

yes = 472, no = 171195, maybe = 103999

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=275666, nc=176127, nnz=479466, k=4] [5.9 MB]
Creating vector for yes... [2.1 MB]
Allocating iteration vectors... [2 x 2.1 MB]
TOTAL: [12.2 MB]

Starting iterations...

Iterative method: 96 iterations in 0.57 seconds (average 0.002417, setup 0.34)

Value in the initial state: 1.241616070964581E-4

Time for model checking: 1.573 seconds.

Result: 1.241616070964581E-4 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmin=? [ F "MissionTimeUp" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=1,Objy1=1,Objx2=2,Objy2=0

Prob0E: 82 iterations in 1.04 seconds (average 0.012634, setup 0.00)

Prob1A: 3 iterations in 0.06 seconds (average 0.020000, setup 0.00)

yes = 472, no = 171598, maybe = 103596

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=275666, nc=175411, nnz=477951, k=4] [5.9 MB]
Creating vector for yes... [2.1 MB]
Allocating iteration vectors... [2 x 2.1 MB]
TOTAL: [12.2 MB]

Starting iterations...

Iterative method: 113 iterations in 0.60 seconds (average 0.002478, setup 0.32)

Value in the initial state: 9.648469860950114E-8

Time for model checking: 1.814 seconds.

Result: 9.648469860950114E-8 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmax=? [ F "Crash" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=1,Objy1=1,Objx2=2,Objy2=0

Prob0A: 3 iterations in 0.10 seconds (average 0.033333, setup 0.00)

Prob1E: 194 iterations in 3.43 seconds (average 0.017670, setup 0.00)

yes = 135253, no = 3065, maybe = 137348

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=275666, nc=221528, nnz=572915, k=4] [7.0 MB]
Creating vector for yes... [2.1 MB]
Allocating iteration vectors... [2 x 2.1 MB]
TOTAL: [13.3 MB]

Starting iterations...

Iterative method: 89 iterations in 0.66 seconds (average 0.002652, setup 0.42)

Value in the initial state: 0.024554529051742977

Time for model checking: 4.475 seconds.

Result: 0.024554529051742977 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmin=? [ F "Crash" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=1,Objy1=1,Objx2=2,Objy2=0

Prob0E: 4 iterations in 0.12 seconds (average 0.029000, setup 0.00)

Prob1A: 50 iterations in 0.78 seconds (average 0.015520, setup 0.00)

yes = 135253, no = 3065, maybe = 137348

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=275666, nc=221528, nnz=572915, k=4] [7.0 MB]
Creating vector for yes... [2.1 MB]
Allocating iteration vectors... [2 x 2.1 MB]
TOTAL: [13.3 MB]

Starting iterations...

Iterative method: 81 iterations in 0.52 seconds (average 0.002617, setup 0.31)

Value in the initial state: 0.01372441447584906

Time for model checking: 1.691 seconds.

Result: 0.01372441447584906 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmax=? [ F "AllAreaSearched" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=1,Objy1=1,Objx2=2,Objy2=0

Prob0A: 62 iterations in 0.64 seconds (average 0.010323, setup 0.00)

Prob1E: 4 iterations in 0.06 seconds (average 0.015000, setup 0.00)

yes = 1452, no = 214011, maybe = 60203

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=275666, nc=90254, nnz=232436, k=2] [3.0 MB]
Creating vector for yes... [2.1 MB]
Allocating iteration vectors... [2 x 2.1 MB]
TOTAL: [9.3 MB]

Starting iterations...

Iterative method: 91 iterations in 0.40 seconds (average 0.002110, setup 0.20)

Value in the initial state: 0.0574707653941579

Time for model checking: 1.208 seconds.

Result: 0.0574707653941579 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmin=? [ F "AllAreaSearched" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=1,Objy1=1,Objx2=2,Objy2=0

Prob0E: 65 iterations in 0.47 seconds (average 0.007262, setup 0.00)

Prob1A: 3 iterations in 0.04 seconds (average 0.014667, setup 0.00)

yes = 1452, no = 221118, maybe = 53096

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=275666, nc=78524, nnz=200185, k=2] [2.6 MB]
Creating vector for yes... [2.1 MB]
Allocating iteration vectors... [2 x 2.1 MB]
TOTAL: [8.9 MB]

Starting iterations...

Iterative method: 95 iterations in 0.35 seconds (average 0.002147, setup 0.14)

Value in the initial state: 0.0018315621964965908

Time for model checking: 0.879 seconds.

Result: 0.0018315621964965908 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmax=? [ F "MissionTimeUp"|"AllAreaSearched"|"Crash" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=1,Objy1=1,Objx2=2,Objy2=0

Prob0A: 3 iterations in 0.11 seconds (average 0.036000, setup 0.00)

Prob1E: 200 iterations in 2.98 seconds (average 0.014920, setup 0.00)

yes = 239515, no = 1141, maybe = 35010

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=275666, nc=60968, nnz=171786, k=2] [2.3 MB]
Creating vector for yes... [2.1 MB]
Allocating iteration vectors... [2 x 2.1 MB]
TOTAL: [8.6 MB]

Starting iterations...

Iterative method: 80 iterations in 0.40 seconds (average 0.001850, setup 0.25)

Value in the initial state: 0.07937324344262513

Time for model checking: 3.51 seconds.

Result: 0.07937324344262513 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmin=? [ F "MissionTimeUp"|"AllAreaSearched"|"Crash" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=1,Objy1=1,Objx2=2,Objy2=0

Prob0E: 4 iterations in 0.07 seconds (average 0.017000, setup 0.00)

Prob1A: 50 iterations in 0.57 seconds (average 0.011360, setup 0.00)

yes = 232924, no = 1141, maybe = 41601

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=275666, nc=72671, nnz=205579, k=2] [2.7 MB]
Creating vector for yes... [2.1 MB]
Allocating iteration vectors... [2 x 2.1 MB]
TOTAL: [9.0 MB]

Starting iterations...

Iterative method: 90 iterations in 0.38 seconds (average 0.001911, setup 0.20)

Value in the initial state: 0.01575845929558561

Time for model checking: 1.052 seconds.

Result: 0.01575845929558561 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"drop"}max=? [ F "deadlock" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=1,Objy1=1,Objx2=2,Objy2=0

Prob0E: 5 iterations in 0.08 seconds (average 0.016800, setup 0.00)

Prob1A: 1 iterations in 0.01 seconds (average 0.008000, setup 0.00)

goal = 1, inf = 0, maybe = 275665

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=275666, nc=359845, nnz=711232, k=4] [10.6 MB]
Building sparse matrix (transition rewards)... [n=275666, nc=359845, nnz=0, k=4] [2.4 MB]
Creating vector for state rewards... [2.1 MB]
Creating vector for inf... [2.1 MB]
Allocating iteration vectors... [2 x 2.1 MB]
TOTAL: [21.4 MB]

Starting iterations...

Iterative method: 85 iterations in 0.85 seconds (average 0.004000, setup 0.51)

Value in the initial state: 0.2047138922367043

Time for model checking: 0.981 seconds.

Result: 0.2047138922367043 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"drop"}min=? [ F "deadlock" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=1,Objy1=1,Objx2=2,Objy2=0

Prob0A: 4 iterations in 0.08 seconds (average 0.019000, setup 0.00)

Prob1E: 5 iterations in 0.06 seconds (average 0.011200, setup 0.00)

Warning: PRISM hasn't checked for zero-reward loops.
Your minimum rewards may be too low...

goal = 1, inf = 0, maybe = 275665

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=275666, nc=359845, nnz=711232, k=4] [10.6 MB]
Building sparse matrix (transition rewards)... [n=275666, nc=359845, nnz=0, k=4] [2.4 MB]
Creating vector for state rewards... [2.1 MB]
Creating vector for inf... [2.1 MB]
Allocating iteration vectors... [2 x 2.1 MB]
TOTAL: [21.4 MB]

Starting iterations...

Iterative method: 78 iterations in 0.70 seconds (average 0.004051, setup 0.38)

Value in the initial state: 0.18993992694107167

Time for model checking: 0.852 seconds.

Result: 0.18993992694107167 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"charge"}max=? [ F "deadlock" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=1,Objy1=1,Objx2=2,Objy2=0

Prob0E: 5 iterations in 0.04 seconds (average 0.007200, setup 0.00)

Prob1A: 1 iterations in 0.01 seconds (average 0.008000, setup 0.00)

goal = 1, inf = 0, maybe = 275665

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=275666, nc=359845, nnz=711232, k=4] [10.6 MB]
Building sparse matrix (transition rewards)... [n=275666, nc=359845, nnz=7309, k=4] [2.5 MB]
Creating vector for state rewards... [2.1 MB]
Creating vector for inf... [2.1 MB]
Allocating iteration vectors... [2 x 2.1 MB]
TOTAL: [21.5 MB]

Starting iterations...

Iterative method: 87 iterations in 0.73 seconds (average 0.004000, setup 0.38)

Value in the initial state: 0.18361334194309253

Time for model checking: 0.794 seconds.

Result: 0.18361334194309253 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"charge"}min=? [ F "deadlock" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=1,Objy1=1,Objx2=2,Objy2=0

Prob0A: 4 iterations in 0.03 seconds (average 0.008000, setup 0.00)

Prob1E: 5 iterations in 0.05 seconds (average 0.010400, setup 0.00)

Warning: PRISM hasn't checked for zero-reward loops.
Your minimum rewards may be too low...

goal = 1, inf = 0, maybe = 275665

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=275666, nc=359845, nnz=711232, k=4] [10.6 MB]
Building sparse matrix (transition rewards)... [n=275666, nc=359845, nnz=7309, k=4] [2.5 MB]
Creating vector for state rewards... [2.1 MB]
Creating vector for inf... [2.1 MB]
Allocating iteration vectors... [2 x 2.1 MB]
TOTAL: [21.5 MB]

Starting iterations...

Iterative method: 95 iterations in 0.76 seconds (average 0.004084, setup 0.38)

Value in the initial state: 0.010675512659870181

Time for model checking: 0.863 seconds.

Result: 0.010675512659870181 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"time"}max=? [ F "deadlock" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=1,Objy1=1,Objx2=2,Objy2=0

Prob0E: 5 iterations in 0.04 seconds (average 0.007200, setup 0.00)

Prob1A: 1 iterations in 0.01 seconds (average 0.008000, setup 0.00)

goal = 1, inf = 0, maybe = 275665

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=275666, nc=359845, nnz=711232, k=4] [10.6 MB]
Building sparse matrix (transition rewards)... [n=275666, nc=359845, nnz=138316, k=4] [4.0 MB]
Creating vector for state rewards... [2.1 MB]
Creating vector for inf... [2.1 MB]
Allocating iteration vectors... [2 x 2.1 MB]
TOTAL: [23.0 MB]

Starting iterations...

Iterative method: 93 iterations in 0.80 seconds (average 0.004086, setup 0.42)

Value in the initial state: 135.69182036177415

Time for model checking: 0.865 seconds.

Result: 135.69182036177415 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"time"}min=? [ F "deadlock" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=1,Objy1=1,Objx2=2,Objy2=0

Prob0A: 4 iterations in 0.03 seconds (average 0.008000, setup 0.00)

Prob1E: 5 iterations in 0.05 seconds (average 0.010400, setup 0.00)

Warning: PRISM hasn't checked for zero-reward loops.
Your minimum rewards may be too low...

goal = 1, inf = 0, maybe = 275665

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=275666, nc=359845, nnz=711232, k=4] [10.6 MB]
Building sparse matrix (transition rewards)... [n=275666, nc=359845, nnz=138316, k=4] [4.0 MB]
Creating vector for state rewards... [2.1 MB]
Creating vector for inf... [2.1 MB]
Allocating iteration vectors... [2 x 2.1 MB]
TOTAL: [23.0 MB]

Starting iterations...

Iterative method: 95 iterations in 0.82 seconds (average 0.004126, setup 0.42)

Value in the initial state: 76.17647520445617

Time for model checking: 0.916 seconds.

Result: 76.17647520445617 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"success"}max=? [ F "deadlock" ]/Pmin=? [ F "MissionSuccessful" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=1,Objy1=1,Objx2=2,Objy2=0

Prob0E: 5 iterations in 0.04 seconds (average 0.007200, setup 0.00)

Prob1A: 1 iterations in 0.01 seconds (average 0.008000, setup 0.00)

goal = 1, inf = 0, maybe = 275665

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=275666, nc=359845, nnz=711232, k=4] [10.6 MB]
Building sparse matrix (transition rewards)... [n=275666, nc=359845, nnz=2248, k=4] [2.5 MB]
Creating vector for state rewards... [2.1 MB]
Creating vector for inf... [2.1 MB]
Allocating iteration vectors... [2 x 2.1 MB]
TOTAL: [21.4 MB]

Starting iterations...

Iterative method: 93 iterations in 0.75 seconds (average 0.003957, setup 0.38)

Prob0E: 52 iterations in 0.45 seconds (average 0.008692, setup 0.00)

Prob1A: 3 iterations in 0.06 seconds (average 0.018667, setup 0.00)

yes = 1140, no = 239516, maybe = 35010

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=275666, nc=60968, nnz=171786, k=2] [2.3 MB]
Creating vector for yes... [2.1 MB]
Allocating iteration vectors... [2 x 2.1 MB]
TOTAL: [8.6 MB]

Starting iterations...

Iterative method: 85 iterations in 0.32 seconds (average 0.001741, setup 0.18)

Value in the initial state: 144.76624790895008

Time for model checking: 1.649 seconds.

Result: 144.76624790895008 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"success"}min=? [ F "deadlock" ]/Pmax=? [ F "MissionSuccessful" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=1,Objy1=1,Objx2=2,Objy2=0

Prob0A: 4 iterations in 0.08 seconds (average 0.019000, setup 0.00)

Prob1E: 5 iterations in 0.06 seconds (average 0.012800, setup 0.00)

Warning: PRISM hasn't checked for zero-reward loops.
Your minimum rewards may be too low...

goal = 1, inf = 0, maybe = 275665

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=275666, nc=359845, nnz=711232, k=4] [10.6 MB]
Building sparse matrix (transition rewards)... [n=275666, nc=359845, nnz=2248, k=4] [2.5 MB]
Creating vector for state rewards... [2.1 MB]
Creating vector for inf... [2.1 MB]
Allocating iteration vectors... [2 x 2.1 MB]
TOTAL: [21.4 MB]

Starting iterations...

Iterative method: 91 iterations in 0.80 seconds (average 0.004000, setup 0.44)

Prob0A: 50 iterations in 0.48 seconds (average 0.009680, setup 0.00)

Prob1E: 4 iterations in 0.05 seconds (average 0.013000, setup 0.00)

yes = 1140, no = 232925, maybe = 41601

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=275666, nc=72671, nnz=205579, k=2] [2.7 MB]
Creating vector for yes... [2.1 MB]
Allocating iteration vectors... [2 x 2.1 MB]
TOTAL: [9.0 MB]

Starting iterations...

Iterative method: 89 iterations in 0.36 seconds (average 0.001843, setup 0.20)

Value in the initial state: 72.8576344250835

Time for model checking: 1.848 seconds.

Result: 72.8576344250835 (value in the initial state)

---------------------------------------------------------------------

Note: There were 16 warnings during computation.

