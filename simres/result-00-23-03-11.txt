PRISM
=====

Version: 4.3
Date: Wed Aug 03 15:53:27 BST 2016
Hostname: eglinton
Memory limits: cudd=1g, java(heap)=981.5m
Command line: prism uav_sim_mdp.prism -s uav_mdp.props -const 'Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=0,Objy1=3,Objx2=1,Objy2=1,'

Parsing model file "uav_sim_mdp.prism"...

Parsing properties file "uav_mdp.props"...

18 properties:
(1) Pmax=? [ F "MissionSuccessful" ]
(2) Pmin=? [ F "MissionSuccessful" ]
(3) Pmax=? [ F "MissionTimeUp" ]
(4) Pmin=? [ F "MissionTimeUp" ]
(5) Pmax=? [ F "Crash" ]
(6) Pmin=? [ F "Crash" ]
(7) Pmax=? [ F "AllAreaSearched" ]
(8) Pmin=? [ F "AllAreaSearched" ]
(9) Pmax=? [ F "MissionTimeUp"|"AllAreaSearched"|"Crash" ]
(10) Pmin=? [ F "MissionTimeUp"|"AllAreaSearched"|"Crash" ]
(11) R{"drop"}max=? [ F "deadlock" ]
(12) R{"drop"}min=? [ F "deadlock" ]
(13) R{"charge"}max=? [ F "deadlock" ]
(14) R{"charge"}min=? [ F "deadlock" ]
(15) R{"time"}max=? [ F "deadlock" ]
(16) R{"time"}min=? [ F "deadlock" ]
(17) R{"success"}max=? [ F "deadlock" ]/Pmin=? [ F "MissionSuccessful" ]
(18) R{"success"}min=? [ F "deadlock" ]/Pmax=? [ F "MissionSuccessful" ]

Type:        MDP
Modules:     Obj Move Robot T 
Variables:   NoOfObjs posx posy s1 retx rety b t c 

---------------------------------------------------------------------

Model checking: Pmax=? [ F "MissionSuccessful" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=0,Objy1=3,Objx2=1,Objy2=1

Building model...
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=0,Objy1=3,Objx2=1,Objy2=1

Warning: Guard for command 7 of module "Move" is never satisfied.

Warning: Guard for command 8 of module "Move" is never satisfied.

Warning: Guard for command 9 of module "Move" is never satisfied.

Warning: Guard for command 6 of module "Robot" is never satisfied.

Warning: Update 2 of command 3 of module "Robot" doesn't do anything ("(s1'=13)", line 210, column 44)

Warning: Update 2 of command 27 of module "T" doesn't do anything ("(c'=1)", line 323, column 108)

Warning: Guard for command 28 of module "T" is never satisfied.

Warning: Update 2 of command 9 of module "T" doesn't do anything ("(c'=1)", line 300, column 112)

Warning: Guard for command 10 of module "T" is never satisfied.

Warning: Update 2 of command 51 of module "T" doesn't do anything ("(c'=1)", line 354, column 113)

Warning: Guard for command 52 of module "T" is never satisfied.

Computing reachable states...

Reachability (BFS): 89 iterations in 1.19 seconds (average 0.013393, setup 0.00)

Time for model construction: 4.033 seconds.

Warning: Deadlocks detected and fixed in 1 states

Type:        MDP
States:      206972 (1 initial)
Transitions: 558978
Choices:     276370

Transition matrix: 216014 nodes (91 terminal), 558978 minterms, vars: 34r/34c/18nd

Prob0A: 56 iterations in 0.56 seconds (average 0.010071, setup 0.00)

Prob1E: 4 iterations in 0.08 seconds (average 0.020000, setup 0.00)

yes = 929, no = 160641, maybe = 45402

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=206972, nc=78004, nnz=215900, k=2] [2.7 MB]
Creating vector for yes... [1.6 MB]
Allocating iteration vectors... [2 x 1.6 MB]
TOTAL: [7.5 MB]

Starting iterations...

Iterative method: 97 iterations in 0.42 seconds (average 0.001485, setup 0.28)

Value in the initial state: 0.9354457680943756

Time for model checking: 1.339 seconds.

Result: 0.9354457680943756 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmin=? [ F "MissionSuccessful" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=0,Objy1=3,Objx2=1,Objy2=1

Prob0E: 59 iterations in 0.70 seconds (average 0.011864, setup 0.00)

Prob1A: 3 iterations in 0.06 seconds (average 0.018667, setup 0.00)

yes = 929, no = 169964, maybe = 36079

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=206972, nc=61448, nnz=168573, k=2] [2.2 MB]
Creating vector for yes... [1.6 MB]
Allocating iteration vectors... [2 x 1.6 MB]
TOTAL: [6.9 MB]

Starting iterations...

Iterative method: 98 iterations in 0.33 seconds (average 0.001429, setup 0.19)

Value in the initial state: 0.708900439069154

Time for model checking: 1.401 seconds.

Result: 0.708900439069154 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmax=? [ F "MissionTimeUp" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=0,Objy1=3,Objx2=1,Objy2=1

Prob0A: 74 iterations in 0.46 seconds (average 0.006216, setup 0.00)

Prob1E: 4 iterations in 0.04 seconds (average 0.009000, setup 0.00)

yes = 288, no = 172211, maybe = 34473

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=206972, nc=60143, nnz=164196, k=4] [2.1 MB]
Creating vector for yes... [1.6 MB]
Allocating iteration vectors... [2 x 1.6 MB]
TOTAL: [6.9 MB]

Starting iterations...

Iterative method: 98 iterations in 0.30 seconds (average 0.001347, setup 0.16)

Value in the initial state: 2.2464768922859935E-6

Time for model checking: 0.936 seconds.

Result: 2.2464768922859935E-6 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmin=? [ F "MissionTimeUp" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=0,Objy1=3,Objx2=1,Objy2=1

Prob0E: 88 iterations in 0.63 seconds (average 0.007136, setup 0.00)

Prob1A: 3 iterations in 0.04 seconds (average 0.012000, setup 0.00)

yes = 288, no = 172478, maybe = 34206

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=206972, nc=59669, nnz=163125, k=4] [2.1 MB]
Creating vector for yes... [1.6 MB]
Allocating iteration vectors... [2 x 1.6 MB]
TOTAL: [6.9 MB]

Starting iterations...

Iterative method: 116 iterations in 0.27 seconds (average 0.001379, setup 0.11)

Value in the initial state: 9.90738316391208E-10

Time for model checking: 1.016 seconds.

Result: 9.90738316391208E-10 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmax=? [ F "Crash" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=0,Objy1=3,Objx2=1,Objy2=1

Prob0A: 3 iterations in 0.10 seconds (average 0.034667, setup 0.00)

Prob1E: 200 iterations in 3.30 seconds (average 0.016500, setup 0.00)

yes = 101376, no = 1478, maybe = 104118

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=206972, nc=173516, nnz=456124, k=4] [5.6 MB]
Creating vector for yes... [1.6 MB]
Allocating iteration vectors... [2 x 1.6 MB]
TOTAL: [10.3 MB]

Starting iterations...

Iterative method: 94 iterations in 0.54 seconds (average 0.002000, setup 0.35)

Value in the initial state: 0.018624123614269277

Time for model checking: 4.78 seconds.

Result: 0.018624123614269277 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmin=? [ F "Crash" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=0,Objy1=3,Objx2=1,Objy2=1

Prob0E: 4 iterations in 0.08 seconds (average 0.021000, setup 0.00)

Prob1A: 53 iterations in 0.56 seconds (average 0.010566, setup 0.00)

yes = 101376, no = 1478, maybe = 104118

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=206972, nc=173516, nnz=456124, k=4] [5.6 MB]
Creating vector for yes... [1.6 MB]
Allocating iteration vectors... [2 x 1.6 MB]
TOTAL: [10.3 MB]

Starting iterations...

Iterative method: 91 iterations in 0.46 seconds (average 0.002066, setup 0.28)

Value in the initial state: 0.015415742882882126

Time for model checking: 1.493 seconds.

Result: 0.015415742882882126 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmax=? [ F "AllAreaSearched" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=0,Objy1=3,Objx2=1,Objy2=1

Prob0A: 53 iterations in 0.50 seconds (average 0.009358, setup 0.00)

Prob1E: 4 iterations in 0.06 seconds (average 0.015000, setup 0.00)

yes = 260, no = 126384, maybe = 80328

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=206972, nc=132252, nnz=357501, k=2] [4.4 MB]
Creating vector for yes... [1.6 MB]
Allocating iteration vectors... [2 x 1.6 MB]
TOTAL: [9.2 MB]

Starting iterations...

Iterative method: 97 iterations in 0.48 seconds (average 0.001856, setup 0.30)

Value in the initial state: 0.27379272661864906

Time for model checking: 1.287 seconds.

Result: 0.27379272661864906 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmin=? [ F "AllAreaSearched" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=0,Objy1=3,Objx2=1,Objy2=1

Prob0E: 67 iterations in 0.74 seconds (average 0.011045, setup 0.00)

Prob1A: 3 iterations in 0.06 seconds (average 0.018667, setup 0.00)

yes = 260, no = 128472, maybe = 78240

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=206972, nc=128316, nnz=345960, k=2] [4.3 MB]
Creating vector for yes... [1.6 MB]
Allocating iteration vectors... [2 x 1.6 MB]
TOTAL: [9.0 MB]

Starting iterations...

Iterative method: 104 iterations in 0.46 seconds (average 0.001846, setup 0.27)

Value in the initial state: 0.04888383165092807

Time for model checking: 1.339 seconds.

Result: 0.04888383165092807 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmax=? [ F "MissionTimeUp"|"AllAreaSearched"|"Crash" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=0,Objy1=3,Objx2=1,Objy2=1

Prob0A: 3 iterations in 0.11 seconds (average 0.037333, setup 0.00)

Prob1E: 217 iterations in 3.45 seconds (average 0.015908, setup 0.00)

yes = 169963, no = 930, maybe = 36079

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=206972, nc=61448, nnz=168573, k=2] [2.2 MB]
Creating vector for yes... [1.6 MB]
Allocating iteration vectors... [2 x 1.6 MB]
TOTAL: [6.9 MB]

Starting iterations...

Iterative method: 89 iterations in 0.36 seconds (average 0.001438, setup 0.23)

Value in the initial state: 0.29109908133299056

Time for model checking: 4.315 seconds.

Result: 0.29109908133299056 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmin=? [ F "MissionTimeUp"|"AllAreaSearched"|"Crash" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=0,Objy1=3,Objx2=1,Objy2=1

Prob0E: 4 iterations in 0.08 seconds (average 0.020000, setup 0.00)

Prob1A: 56 iterations in 0.61 seconds (average 0.010857, setup 0.00)

yes = 160640, no = 930, maybe = 45402

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=206972, nc=78004, nnz=215900, k=2] [2.7 MB]
Creating vector for yes... [1.6 MB]
Allocating iteration vectors... [2 x 1.6 MB]
TOTAL: [7.5 MB]

Starting iterations...

Iterative method: 91 iterations in 0.35 seconds (average 0.001582, setup 0.21)

Value in the initial state: 0.0645526718959927

Time for model checking: 1.169 seconds.

Result: 0.0645526718959927 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"drop"}max=? [ F "deadlock" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=0,Objy1=3,Objx2=1,Objy2=1

Prob0E: 5 iterations in 0.10 seconds (average 0.020000, setup 0.00)

Prob1A: 1 iterations in 0.01 seconds (average 0.008000, setup 0.00)

goal = 1, inf = 0, maybe = 206971

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=206972, nc=276369, nnz=558977, k=4] [8.2 MB]
Building sparse matrix (transition rewards)... [n=206972, nc=276369, nnz=0, k=4] [1.8 MB]
Creating vector for state rewards... [1.6 MB]
Creating vector for inf... [1.6 MB]
Allocating iteration vectors... [2 x 1.6 MB]
TOTAL: [16.4 MB]

Starting iterations...

Iterative method: 93 iterations in 0.69 seconds (average 0.003097, setup 0.40)

Value in the initial state: 0.16205270476230657

Time for model checking: 0.818 seconds.

Result: 0.16205270476230657 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"drop"}min=? [ F "deadlock" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=0,Objy1=3,Objx2=1,Objy2=1

Prob0A: 4 iterations in 0.05 seconds (average 0.012000, setup 0.00)

Prob1E: 5 iterations in 0.06 seconds (average 0.012000, setup 0.00)

Warning: PRISM hasn't checked for zero-reward loops.
Your minimum rewards may be too low...

goal = 1, inf = 0, maybe = 206971

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=206972, nc=276369, nnz=558977, k=4] [8.2 MB]
Building sparse matrix (transition rewards)... [n=206972, nc=276369, nnz=0, k=4] [1.8 MB]
Creating vector for state rewards... [1.6 MB]
Creating vector for inf... [1.6 MB]
Allocating iteration vectors... [2 x 1.6 MB]
TOTAL: [16.4 MB]

Starting iterations...

Iterative method: 89 iterations in 0.58 seconds (average 0.003101, setup 0.30)

Value in the initial state: 0.14513386059880487

Time for model checking: 0.703 seconds.

Result: 0.14513386059880487 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"charge"}max=? [ F "deadlock" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=0,Objy1=3,Objx2=1,Objy2=1

Prob0E: 5 iterations in 0.04 seconds (average 0.007200, setup 0.00)

Prob1A: 1 iterations in 0.00 seconds (average 0.004000, setup 0.00)

goal = 1, inf = 0, maybe = 206971

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=206972, nc=276369, nnz=558977, k=4] [8.2 MB]
Building sparse matrix (transition rewards)... [n=206972, nc=276369, nnz=6624, k=4] [1.9 MB]
Creating vector for state rewards... [1.6 MB]
Creating vector for inf... [1.6 MB]
Allocating iteration vectors... [2 x 1.6 MB]
TOTAL: [16.5 MB]

Starting iterations...

Iterative method: 95 iterations in 0.60 seconds (average 0.003116, setup 0.30)

Value in the initial state: 0.2743367906830312

Time for model checking: 0.655 seconds.

Result: 0.2743367906830312 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"charge"}min=? [ F "deadlock" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=0,Objy1=3,Objx2=1,Objy2=1

Prob0A: 4 iterations in 0.03 seconds (average 0.007000, setup 0.00)

Prob1E: 5 iterations in 0.06 seconds (average 0.011200, setup 0.00)

Warning: PRISM hasn't checked for zero-reward loops.
Your minimum rewards may be too low...

goal = 1, inf = 0, maybe = 206971

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=206972, nc=276369, nnz=558977, k=4] [8.2 MB]
Building sparse matrix (transition rewards)... [n=206972, nc=276369, nnz=6624, k=4] [1.9 MB]
Creating vector for state rewards... [1.6 MB]
Creating vector for inf... [1.6 MB]
Allocating iteration vectors... [2 x 1.6 MB]
TOTAL: [16.5 MB]

Starting iterations...

Iterative method: 102 iterations in 0.72 seconds (average 0.003098, setup 0.40)

Value in the initial state: 0.04761361851521437

Time for model checking: 0.821 seconds.

Result: 0.04761361851521437 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"time"}max=? [ F "deadlock" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=0,Objy1=3,Objx2=1,Objy2=1

Prob0E: 5 iterations in 0.04 seconds (average 0.007200, setup 0.00)

Prob1A: 1 iterations in 0.01 seconds (average 0.008000, setup 0.00)

goal = 1, inf = 0, maybe = 206971

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=206972, nc=276369, nnz=558977, k=4] [8.2 MB]
Building sparse matrix (transition rewards)... [n=206972, nc=276369, nnz=102852, k=4] [3.0 MB]
Creating vector for state rewards... [1.6 MB]
Creating vector for inf... [1.6 MB]
Allocating iteration vectors... [2 x 1.6 MB]
TOTAL: [17.6 MB]

Starting iterations...

Iterative method: 97 iterations in 0.64 seconds (average 0.003134, setup 0.34)

Value in the initial state: 103.38770253904129

Time for model checking: 0.695 seconds.

Result: 103.38770253904129 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"time"}min=? [ F "deadlock" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=0,Objy1=3,Objx2=1,Objy2=1

Prob0A: 4 iterations in 0.03 seconds (average 0.007000, setup 0.00)

Prob1E: 5 iterations in 0.05 seconds (average 0.010400, setup 0.00)

Warning: PRISM hasn't checked for zero-reward loops.
Your minimum rewards may be too low...

goal = 1, inf = 0, maybe = 206971

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=206972, nc=276369, nnz=558977, k=4] [8.2 MB]
Building sparse matrix (transition rewards)... [n=206972, nc=276369, nnz=102852, k=4] [3.0 MB]
Creating vector for state rewards... [1.6 MB]
Creating vector for inf... [1.6 MB]
Allocating iteration vectors... [2 x 1.6 MB]
TOTAL: [17.6 MB]

Starting iterations...

Iterative method: 105 iterations in 0.67 seconds (average 0.003162, setup 0.34)

Value in the initial state: 85.60944129455832

Time for model checking: 0.764 seconds.

Result: 85.60944129455832 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"success"}max=? [ F "deadlock" ]/Pmin=? [ F "MissionSuccessful" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=0,Objy1=3,Objx2=1,Objy2=1

Prob0E: 5 iterations in 0.04 seconds (average 0.007200, setup 0.00)

Prob1A: 1 iterations in 0.00 seconds (average 0.004000, setup 0.00)

goal = 1, inf = 0, maybe = 206971

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=206972, nc=276369, nnz=558977, k=4] [8.2 MB]
Building sparse matrix (transition rewards)... [n=206972, nc=276369, nnz=1839, k=4] [1.9 MB]
Creating vector for state rewards... [1.6 MB]
Creating vector for inf... [1.6 MB]
Allocating iteration vectors... [2 x 1.6 MB]
TOTAL: [16.4 MB]

Starting iterations...

Iterative method: 104 iterations in 0.62 seconds (average 0.003077, setup 0.30)

Prob0E: 59 iterations in 0.46 seconds (average 0.007797, setup 0.00)

Prob1A: 3 iterations in 0.07 seconds (average 0.024000, setup 0.00)

yes = 929, no = 169964, maybe = 36079

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=206972, nc=61448, nnz=168573, k=2] [2.2 MB]
Creating vector for yes... [1.6 MB]
Allocating iteration vectors... [2 x 1.6 MB]
TOTAL: [6.9 MB]

Starting iterations...

Iterative method: 98 iterations in 0.34 seconds (average 0.001388, setup 0.20)

Value in the initial state: 124.3478502249496

Time for model checking: 1.568 seconds.

Result: 124.3478502249496 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"success"}min=? [ F "deadlock" ]/Pmax=? [ F "MissionSuccessful" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=0,Objy1=3,Objx2=1,Objy2=1

Prob0A: 4 iterations in 0.12 seconds (average 0.029000, setup 0.00)

Prob1E: 5 iterations in 0.06 seconds (average 0.012800, setup 0.00)

Warning: PRISM hasn't checked for zero-reward loops.
Your minimum rewards may be too low...

goal = 1, inf = 0, maybe = 206971

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=206972, nc=276369, nnz=558977, k=4] [8.2 MB]
Building sparse matrix (transition rewards)... [n=206972, nc=276369, nnz=1839, k=4] [1.9 MB]
Creating vector for state rewards... [1.6 MB]
Creating vector for inf... [1.6 MB]
Allocating iteration vectors... [2 x 1.6 MB]
TOTAL: [16.4 MB]

Starting iterations...

Iterative method: 102 iterations in 0.66 seconds (average 0.003020, setup 0.35)

Prob0A: 56 iterations in 0.42 seconds (average 0.007500, setup 0.00)

Prob1E: 4 iterations in 0.05 seconds (average 0.012000, setup 0.00)

yes = 929, no = 160641, maybe = 45402

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=206972, nc=78004, nnz=215900, k=2] [2.7 MB]
Creating vector for yes... [1.6 MB]
Allocating iteration vectors... [2 x 1.6 MB]
TOTAL: [7.5 MB]

Starting iterations...

Iterative method: 97 iterations in 0.32 seconds (average 0.001485, setup 0.18)

Value in the initial state: 66.29241634894726

Time for model checking: 1.644 seconds.

Result: 66.29241634894726 (value in the initial state)

---------------------------------------------------------------------

Note: There were 16 warnings during computation.

