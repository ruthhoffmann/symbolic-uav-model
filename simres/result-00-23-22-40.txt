PRISM
=====

Version: 4.3
Date: Wed Aug 03 15:57:54 BST 2016
Hostname: eglinton
Memory limits: cudd=1g, java(heap)=981.5m
Command line: prism uav_sim_mdp.prism -s uav_mdp.props -const 'Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=2,Objy1=2,Objx2=4,Objy2=0,'

Parsing model file "uav_sim_mdp.prism"...

Parsing properties file "uav_mdp.props"...

18 properties:
(1) Pmax=? [ F "MissionSuccessful" ]
(2) Pmin=? [ F "MissionSuccessful" ]
(3) Pmax=? [ F "MissionTimeUp" ]
(4) Pmin=? [ F "MissionTimeUp" ]
(5) Pmax=? [ F "Crash" ]
(6) Pmin=? [ F "Crash" ]
(7) Pmax=? [ F "AllAreaSearched" ]
(8) Pmin=? [ F "AllAreaSearched" ]
(9) Pmax=? [ F "MissionTimeUp"|"AllAreaSearched"|"Crash" ]
(10) Pmin=? [ F "MissionTimeUp"|"AllAreaSearched"|"Crash" ]
(11) R{"drop"}max=? [ F "deadlock" ]
(12) R{"drop"}min=? [ F "deadlock" ]
(13) R{"charge"}max=? [ F "deadlock" ]
(14) R{"charge"}min=? [ F "deadlock" ]
(15) R{"time"}max=? [ F "deadlock" ]
(16) R{"time"}min=? [ F "deadlock" ]
(17) R{"success"}max=? [ F "deadlock" ]/Pmin=? [ F "MissionSuccessful" ]
(18) R{"success"}min=? [ F "deadlock" ]/Pmax=? [ F "MissionSuccessful" ]

Type:        MDP
Modules:     Obj Move Robot T 
Variables:   NoOfObjs posx posy s1 retx rety b t c 

---------------------------------------------------------------------

Model checking: Pmax=? [ F "MissionSuccessful" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=2,Objy1=2,Objx2=4,Objy2=0

Building model...
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=2,Objy1=2,Objx2=4,Objy2=0

Warning: Guard for command 7 of module "Move" is never satisfied.

Warning: Guard for command 8 of module "Move" is never satisfied.

Warning: Guard for command 9 of module "Move" is never satisfied.

Warning: Guard for command 7 of module "Robot" is never satisfied.

Warning: Update 2 of command 3 of module "Robot" doesn't do anything ("(s1'=13)", line 210, column 44)

Warning: Update 2 of command 27 of module "T" doesn't do anything ("(c'=1)", line 323, column 108)

Warning: Guard for command 28 of module "T" is never satisfied.

Warning: Update 2 of command 9 of module "T" doesn't do anything ("(c'=1)", line 300, column 112)

Warning: Guard for command 10 of module "T" is never satisfied.

Warning: Update 2 of command 51 of module "T" doesn't do anything ("(c'=1)", line 354, column 113)

Warning: Guard for command 52 of module "T" is never satisfied.

Computing reachable states...

Reachability (BFS): 87 iterations in 1.56 seconds (average 0.017885, setup 0.00)

Time for model construction: 3.77 seconds.

Warning: Deadlocks detected and fixed in 1 states

Type:        MDP
States:      238660 (1 initial)
Transitions: 602719
Choices:     309954

Transition matrix: 219323 nodes (91 terminal), 602719 minterms, vars: 34r/34c/18nd

Prob0A: 51 iterations in 0.42 seconds (average 0.008235, setup 0.00)

Prob1E: 4 iterations in 0.06 seconds (average 0.015000, setup 0.00)

yes = 1128, no = 202340, maybe = 35192

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=238660, nc=60500, nnz=167047, k=2] [2.2 MB]
Creating vector for yes... [1.8 MB]
Allocating iteration vectors... [2 x 1.8 MB]
TOTAL: [7.7 MB]

Starting iterations...

Iterative method: 91 iterations in 0.40 seconds (average 0.001626, setup 0.25)

Value in the initial state: 0.9832539584592502

Time for model checking: 0.901 seconds.

Result: 0.9832539584592502 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmin=? [ F "MissionSuccessful" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=2,Objy1=2,Objx2=4,Objy2=0

Prob0E: 53 iterations in 0.45 seconds (average 0.008453, setup 0.00)

Prob1A: 3 iterations in 0.06 seconds (average 0.021333, setup 0.00)

yes = 1128, no = 208395, maybe = 29137

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=238660, nc=49952, nnz=136551, k=2] [1.8 MB]
Creating vector for yes... [1.8 MB]
Allocating iteration vectors... [2 x 1.8 MB]
TOTAL: [7.3 MB]

Starting iterations...

Iterative method: 88 iterations in 0.34 seconds (average 0.001591, setup 0.20)

Value in the initial state: 0.8465495091459604

Time for model checking: 0.889 seconds.

Result: 0.8465495091459604 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmax=? [ F "MissionTimeUp" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=2,Objy1=2,Objx2=4,Objy2=0

Prob0A: 68 iterations in 0.79 seconds (average 0.011588, setup 0.00)

Prob1E: 4 iterations in 0.04 seconds (average 0.011000, setup 0.00)

yes = 416, no = 149035, maybe = 89209

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=238660, nc=148273, nnz=396458, k=4] [4.9 MB]
Creating vector for yes... [1.8 MB]
Allocating iteration vectors... [2 x 1.8 MB]
TOTAL: [10.4 MB]

Starting iterations...

Iterative method: 94 iterations in 0.55 seconds (average 0.002723, setup 0.29)

Value in the initial state: 1.1873701412093038E-4

Time for model checking: 1.407 seconds.

Result: 1.1873701412093038E-4 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmin=? [ F "MissionTimeUp" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=2,Objy1=2,Objx2=4,Objy2=0

Prob0E: 82 iterations in 1.12 seconds (average 0.013610, setup 0.00)

Prob1A: 3 iterations in 0.08 seconds (average 0.028000, setup 0.00)

yes = 416, no = 149381, maybe = 88863

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=238660, nc=147671, nnz=395060, k=4] [4.9 MB]
Creating vector for yes... [1.8 MB]
Allocating iteration vectors... [2 x 1.8 MB]
TOTAL: [10.4 MB]

Starting iterations...

Iterative method: 111 iterations in 0.57 seconds (average 0.002162, setup 0.33)

Value in the initial state: 1.7356105956091785E-7

Time for model checking: 1.797 seconds.

Result: 1.7356105956091785E-7 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmax=? [ F "Crash" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=2,Objy1=2,Objx2=4,Objy2=0

Prob0A: 3 iterations in 0.15 seconds (average 0.049333, setup 0.00)

Prob1E: 195 iterations in 2.96 seconds (average 0.015159, setup 0.00)

yes = 117167, no = 3061, maybe = 118432

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=238660, nc=189726, nnz=482491, k=4] [5.9 MB]
Creating vector for yes... [1.8 MB]
Allocating iteration vectors... [2 x 1.8 MB]
TOTAL: [11.4 MB]

Starting iterations...

Iterative method: 90 iterations in 0.55 seconds (average 0.002222, setup 0.35)

Value in the initial state: 0.024639305391375918

Time for model checking: 3.686 seconds.

Result: 0.024639305391375918 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmin=? [ F "Crash" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=2,Objy1=2,Objx2=4,Objy2=0

Prob0E: 4 iterations in 0.09 seconds (average 0.023000, setup 0.00)

Prob1A: 47 iterations in 0.55 seconds (average 0.011745, setup 0.00)

yes = 117167, no = 3061, maybe = 118432

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=238660, nc=189726, nnz=482491, k=4] [5.9 MB]
Creating vector for yes... [1.8 MB]
Allocating iteration vectors... [2 x 1.8 MB]
TOTAL: [11.4 MB]

Starting iterations...

Iterative method: 82 iterations in 0.44 seconds (average 0.002244, setup 0.26)

Value in the initial state: 0.014713087719184828

Time for model checking: 1.111 seconds.

Result: 0.014713087719184828 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmax=? [ F "AllAreaSearched" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=2,Objy1=2,Objx2=4,Objy2=0

Prob0A: 59 iterations in 0.50 seconds (average 0.008407, setup 0.00)

Prob1E: 4 iterations in 0.05 seconds (average 0.012000, setup 0.00)

yes = 1516, no = 184558, maybe = 52586

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=238660, nc=78808, nnz=198429, k=2] [2.6 MB]
Creating vector for yes... [1.8 MB]
Allocating iteration vectors... [2 x 1.8 MB]
TOTAL: [8.0 MB]

Starting iterations...

Iterative method: 86 iterations in 0.36 seconds (average 0.001767, setup 0.20)

Value in the initial state: 0.13057359125343398

Time for model checking: 0.92 seconds.

Result: 0.13057359125343398 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmin=? [ F "AllAreaSearched" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=2,Objy1=2,Objx2=4,Objy2=0

Prob0E: 62 iterations in 0.51 seconds (average 0.008194, setup 0.00)

Prob1A: 3 iterations in 0.05 seconds (average 0.016000, setup 0.00)

yes = 1516, no = 191039, maybe = 46105

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=238660, nc=68581, nnz=171155, k=2] [2.3 MB]
Creating vector for yes... [1.8 MB]
Allocating iteration vectors... [2 x 1.8 MB]
TOTAL: [7.7 MB]

Starting iterations...

Iterative method: 94 iterations in 0.31 seconds (average 0.001702, setup 0.15)

Value in the initial state: 0.0018445587891636826

Time for model checking: 0.868 seconds.

Result: 0.0018445587891636826 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmax=? [ F "MissionTimeUp"|"AllAreaSearched"|"Crash" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=2,Objy1=2,Objx2=4,Objy2=0

Prob0A: 3 iterations in 0.08 seconds (average 0.026667, setup 0.00)

Prob1E: 204 iterations in 2.80 seconds (average 0.013725, setup 0.00)

yes = 208394, no = 1129, maybe = 29137

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=238660, nc=49952, nnz=136551, k=2] [1.8 MB]
Creating vector for yes... [1.8 MB]
Allocating iteration vectors... [2 x 1.8 MB]
TOTAL: [7.3 MB]

Starting iterations...

Iterative method: 81 iterations in 0.31 seconds (average 0.001481, setup 0.19)

Value in the initial state: 0.1534503135316127

Time for model checking: 3.237 seconds.

Result: 0.1534503135316127 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmin=? [ F "MissionTimeUp"|"AllAreaSearched"|"Crash" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=2,Objy1=2,Objx2=4,Objy2=0

Prob0E: 4 iterations in 0.11 seconds (average 0.027000, setup 0.00)

Prob1A: 51 iterations in 0.50 seconds (average 0.009804, setup 0.00)

yes = 202339, no = 1129, maybe = 35192

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=238660, nc=60500, nnz=167047, k=2] [2.2 MB]
Creating vector for yes... [1.8 MB]
Allocating iteration vectors... [2 x 1.8 MB]
TOTAL: [7.7 MB]

Starting iterations...

Iterative method: 90 iterations in 0.32 seconds (average 0.001600, setup 0.18)

Value in the initial state: 0.01674478008455267

Time for model checking: 0.95 seconds.

Result: 0.01674478008455267 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"drop"}max=? [ F "deadlock" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=2,Objy1=2,Objx2=4,Objy2=0

Prob0E: 5 iterations in 0.08 seconds (average 0.016800, setup 0.00)

Prob1A: 1 iterations in 0.01 seconds (average 0.008000, setup 0.00)

goal = 1, inf = 0, maybe = 238659

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=238660, nc=309953, nnz=602718, k=4] [9.0 MB]
Building sparse matrix (transition rewards)... [n=238660, nc=309953, nnz=0, k=4] [2.1 MB]
Creating vector for state rewards... [1.8 MB]
Creating vector for inf... [1.8 MB]
Allocating iteration vectors... [2 x 1.8 MB]
TOTAL: [18.4 MB]

Starting iterations...

Iterative method: 87 iterations in 0.73 seconds (average 0.003402, setup 0.43)

Value in the initial state: 0.17236192456401433

Time for model checking: 0.839 seconds.

Result: 0.17236192456401433 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"drop"}min=? [ F "deadlock" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=2,Objy1=2,Objx2=4,Objy2=0

Prob0A: 4 iterations in 0.04 seconds (average 0.011000, setup 0.00)

Prob1E: 5 iterations in 0.06 seconds (average 0.011200, setup 0.00)

Warning: PRISM hasn't checked for zero-reward loops.
Your minimum rewards may be too low...

goal = 1, inf = 0, maybe = 238659

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=238660, nc=309953, nnz=602718, k=4] [9.0 MB]
Building sparse matrix (transition rewards)... [n=238660, nc=309953, nnz=0, k=4] [2.1 MB]
Creating vector for state rewards... [1.8 MB]
Creating vector for inf... [1.8 MB]
Allocating iteration vectors... [2 x 1.8 MB]
TOTAL: [18.4 MB]

Starting iterations...

Iterative method: 79 iterations in 0.59 seconds (average 0.003342, setup 0.33)

Value in the initial state: 0.16379329548977725

Time for model checking: 0.714 seconds.

Result: 0.16379329548977725 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"charge"}max=? [ F "deadlock" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=2,Objy1=2,Objx2=4,Objy2=0

Prob0E: 5 iterations in 0.03 seconds (average 0.006400, setup 0.00)

Prob1A: 1 iterations in 0.01 seconds (average 0.008000, setup 0.00)

goal = 1, inf = 0, maybe = 238659

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=238660, nc=309953, nnz=602718, k=4] [9.0 MB]
Building sparse matrix (transition rewards)... [n=238660, nc=309953, nnz=6061, k=4] [2.2 MB]
Creating vector for state rewards... [1.8 MB]
Creating vector for inf... [1.8 MB]
Allocating iteration vectors... [2 x 1.8 MB]
TOTAL: [18.4 MB]

Starting iterations...

Iterative method: 88 iterations in 0.65 seconds (average 0.003727, setup 0.32)

Value in the initial state: 0.17299420683614913

Time for model checking: 0.716 seconds.

Result: 0.17299420683614913 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"charge"}min=? [ F "deadlock" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=2,Objy1=2,Objx2=4,Objy2=0

Prob0A: 4 iterations in 0.05 seconds (average 0.013000, setup 0.00)

Prob1E: 5 iterations in 0.08 seconds (average 0.016000, setup 0.00)

Warning: PRISM hasn't checked for zero-reward loops.
Your minimum rewards may be too low...

goal = 1, inf = 0, maybe = 238659

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=238660, nc=309953, nnz=602718, k=4] [9.0 MB]
Building sparse matrix (transition rewards)... [n=238660, nc=309953, nnz=6061, k=4] [2.2 MB]
Creating vector for state rewards... [1.8 MB]
Creating vector for inf... [1.8 MB]
Allocating iteration vectors... [2 x 1.8 MB]
TOTAL: [18.4 MB]

Starting iterations...

Iterative method: 95 iterations in 0.68 seconds (average 0.003495, setup 0.35)

Value in the initial state: 0.01861782010847213

Time for model checking: 0.829 seconds.

Result: 0.01861782010847213 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"time"}max=? [ F "deadlock" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=2,Objy1=2,Objx2=4,Objy2=0

Prob0E: 5 iterations in 0.06 seconds (average 0.012000, setup 0.00)

Prob1A: 1 iterations in 0.01 seconds (average 0.012000, setup 0.00)

goal = 1, inf = 0, maybe = 238659

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=238660, nc=309953, nnz=602718, k=4] [9.0 MB]
Building sparse matrix (transition rewards)... [n=238660, nc=309953, nnz=120226, k=4] [3.5 MB]
Creating vector for state rewards... [1.8 MB]
Creating vector for inf... [1.8 MB]
Allocating iteration vectors... [2 x 1.8 MB]
TOTAL: [19.7 MB]

Starting iterations...

Iterative method: 93 iterations in 0.73 seconds (average 0.003570, setup 0.40)

Value in the initial state: 136.20057814464863

Time for model checking: 0.832 seconds.

Result: 136.20057814464863 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"time"}min=? [ F "deadlock" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=2,Objy1=2,Objx2=4,Objy2=0

Prob0A: 4 iterations in 0.03 seconds (average 0.007000, setup 0.00)

Prob1E: 5 iterations in 0.05 seconds (average 0.010400, setup 0.00)

Warning: PRISM hasn't checked for zero-reward loops.
Your minimum rewards may be too low...

goal = 1, inf = 0, maybe = 238659

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=238660, nc=309953, nnz=602718, k=4] [9.0 MB]
Building sparse matrix (transition rewards)... [n=238660, nc=309953, nnz=120226, k=4] [3.5 MB]
Creating vector for state rewards... [1.8 MB]
Creating vector for inf... [1.8 MB]
Allocating iteration vectors... [2 x 1.8 MB]
TOTAL: [19.7 MB]

Starting iterations...

Iterative method: 97 iterations in 0.70 seconds (average 0.003505, setup 0.36)

Value in the initial state: 81.63610306472596

Time for model checking: 0.794 seconds.

Result: 81.63610306472596 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"success"}max=? [ F "deadlock" ]/Pmin=? [ F "MissionSuccessful" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=2,Objy1=2,Objx2=4,Objy2=0

Prob0E: 5 iterations in 0.04 seconds (average 0.007200, setup 0.00)

Prob1A: 1 iterations in 0.01 seconds (average 0.008000, setup 0.00)

goal = 1, inf = 0, maybe = 238659

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=238660, nc=309953, nnz=602718, k=4] [9.0 MB]
Building sparse matrix (transition rewards)... [n=238660, nc=309953, nnz=2224, k=4] [2.1 MB]
Creating vector for state rewards... [1.8 MB]
Creating vector for inf... [1.8 MB]
Allocating iteration vectors... [2 x 1.8 MB]
TOTAL: [18.4 MB]

Starting iterations...

Iterative method: 95 iterations in 0.64 seconds (average 0.003368, setup 0.32)

Prob0E: 53 iterations in 0.41 seconds (average 0.007698, setup 0.00)

Prob1A: 3 iterations in 0.05 seconds (average 0.016000, setup 0.00)

yes = 1128, no = 208395, maybe = 29137

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=238660, nc=49952, nnz=136551, k=2] [1.8 MB]
Creating vector for yes... [1.8 MB]
Allocating iteration vectors... [2 x 1.8 MB]
TOTAL: [7.3 MB]

Starting iterations...

Iterative method: 88 iterations in 0.28 seconds (average 0.001500, setup 0.15)

Value in the initial state: 158.05772409762957

Time for model checking: 1.468 seconds.

Result: 158.05772409762957 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"success"}min=? [ F "deadlock" ]/Pmax=? [ F "MissionSuccessful" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=2,Objy1=2,Objx2=4,Objy2=0

Prob0A: 4 iterations in 0.08 seconds (average 0.020000, setup 0.00)

Prob1E: 5 iterations in 0.06 seconds (average 0.011200, setup 0.00)

Warning: PRISM hasn't checked for zero-reward loops.
Your minimum rewards may be too low...

goal = 1, inf = 0, maybe = 238659

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=238660, nc=309953, nnz=602718, k=4] [9.0 MB]
Building sparse matrix (transition rewards)... [n=238660, nc=309953, nnz=2224, k=4] [2.1 MB]
Creating vector for state rewards... [1.8 MB]
Creating vector for inf... [1.8 MB]
Allocating iteration vectors... [2 x 1.8 MB]
TOTAL: [18.4 MB]

Starting iterations...

Iterative method: 92 iterations in 0.68 seconds (average 0.003391, setup 0.37)

Prob0A: 51 iterations in 0.40 seconds (average 0.007843, setup 0.00)

Prob1E: 4 iterations in 0.04 seconds (average 0.009000, setup 0.00)

yes = 1128, no = 202340, maybe = 35192

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=238660, nc=60500, nnz=167047, k=2] [2.2 MB]
Creating vector for yes... [1.8 MB]
Allocating iteration vectors... [2 x 1.8 MB]
TOTAL: [7.7 MB]

Starting iterations...

Iterative method: 91 iterations in 0.30 seconds (average 0.001538, setup 0.16)

Value in the initial state: 76.30742547163415

Time for model checking: 1.584 seconds.

Result: 76.30742547163415 (value in the initial state)

---------------------------------------------------------------------

Note: There were 16 warnings during computation.

