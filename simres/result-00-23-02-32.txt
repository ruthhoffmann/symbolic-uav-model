PRISM
=====

Version: 4.3
Date: Wed Aug 03 15:52:48 BST 2016
Hostname: eglinton
Memory limits: cudd=1g, java(heap)=981.5m
Command line: prism uav_sim_mdp.prism -s uav_mdp.props -const 'Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=0,Objy1=2,Objx2=3,Objy2=2,'

Parsing model file "uav_sim_mdp.prism"...

Parsing properties file "uav_mdp.props"...

18 properties:
(1) Pmax=? [ F "MissionSuccessful" ]
(2) Pmin=? [ F "MissionSuccessful" ]
(3) Pmax=? [ F "MissionTimeUp" ]
(4) Pmin=? [ F "MissionTimeUp" ]
(5) Pmax=? [ F "Crash" ]
(6) Pmin=? [ F "Crash" ]
(7) Pmax=? [ F "AllAreaSearched" ]
(8) Pmin=? [ F "AllAreaSearched" ]
(9) Pmax=? [ F "MissionTimeUp"|"AllAreaSearched"|"Crash" ]
(10) Pmin=? [ F "MissionTimeUp"|"AllAreaSearched"|"Crash" ]
(11) R{"drop"}max=? [ F "deadlock" ]
(12) R{"drop"}min=? [ F "deadlock" ]
(13) R{"charge"}max=? [ F "deadlock" ]
(14) R{"charge"}min=? [ F "deadlock" ]
(15) R{"time"}max=? [ F "deadlock" ]
(16) R{"time"}min=? [ F "deadlock" ]
(17) R{"success"}max=? [ F "deadlock" ]/Pmin=? [ F "MissionSuccessful" ]
(18) R{"success"}min=? [ F "deadlock" ]/Pmax=? [ F "MissionSuccessful" ]

Type:        MDP
Modules:     Obj Move Robot T 
Variables:   NoOfObjs posx posy s1 retx rety b t c 

---------------------------------------------------------------------

Model checking: Pmax=? [ F "MissionSuccessful" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=0,Objy1=2,Objx2=3,Objy2=2

Building model...
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=0,Objy1=2,Objx2=3,Objy2=2

Warning: Guard for command 7 of module "Move" is never satisfied.

Warning: Guard for command 8 of module "Move" is never satisfied.

Warning: Guard for command 9 of module "Move" is never satisfied.

Warning: Guard for command 7 of module "Robot" is never satisfied.

Warning: Update 2 of command 3 of module "Robot" doesn't do anything ("(s1'=13)", line 210, column 44)

Warning: Update 2 of command 27 of module "T" doesn't do anything ("(c'=1)", line 323, column 108)

Warning: Guard for command 28 of module "T" is never satisfied.

Warning: Update 2 of command 9 of module "T" doesn't do anything ("(c'=1)", line 300, column 112)

Warning: Guard for command 10 of module "T" is never satisfied.

Warning: Update 2 of command 51 of module "T" doesn't do anything ("(c'=1)", line 354, column 113)

Warning: Guard for command 52 of module "T" is never satisfied.

Computing reachable states...

Reachability (BFS): 87 iterations in 0.88 seconds (average 0.010115, setup 0.00)

Time for model construction: 3.403 seconds.

Warning: Deadlocks detected and fixed in 1 states

Type:        MDP
States:      184437 (1 initial)
Transitions: 484595
Choices:     245690

Transition matrix: 178030 nodes (89 terminal), 484595 minterms, vars: 34r/34c/18nd

Prob0A: 45 iterations in 0.31 seconds (average 0.006844, setup 0.00)

Prob1E: 4 iterations in 0.05 seconds (average 0.012000, setup 0.00)

yes = 1057, no = 151429, maybe = 31951

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=184437, nc=57517, nnz=160820, k=2] [2.1 MB]
Creating vector for yes... [1.4 MB]
Allocating iteration vectors... [2 x 1.4 MB]
TOTAL: [6.3 MB]

Starting iterations...

Iterative method: 88 iterations in 0.34 seconds (average 0.001273, setup 0.23)

Value in the initial state: 0.9843103843588047

Time for model checking: 0.868 seconds.

Result: 0.9843103843588047 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmin=? [ F "MissionSuccessful" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=0,Objy1=2,Objx2=3,Objy2=2

Prob0E: 45 iterations in 0.34 seconds (average 0.007644, setup 0.00)

Prob1A: 3 iterations in 0.04 seconds (average 0.013333, setup 0.00)

yes = 1057, no = 156054, maybe = 27326

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=184437, nc=49021, nnz=136329, k=2] [1.8 MB]
Creating vector for yes... [1.4 MB]
Allocating iteration vectors... [2 x 1.4 MB]
TOTAL: [6.0 MB]

Starting iterations...

Iterative method: 87 iterations in 0.28 seconds (average 0.001195, setup 0.18)

Value in the initial state: 0.8625696460750314

Time for model checking: 0.858 seconds.

Result: 0.8625696460750314 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmax=? [ F "MissionTimeUp" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=0,Objy1=2,Objx2=3,Objy2=2

Prob0A: 76 iterations in 0.54 seconds (average 0.007105, setup 0.00)

Prob1E: 4 iterations in 0.04 seconds (average 0.009000, setup 0.00)

yes = 304, no = 118554, maybe = 65579

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=184437, nc=115478, nnz=314573, k=4] [3.9 MB]
Creating vector for yes... [1.4 MB]
Allocating iteration vectors... [2 x 1.4 MB]
TOTAL: [8.1 MB]

Starting iterations...

Iterative method: 94 iterations in 0.37 seconds (average 0.001574, setup 0.22)

Value in the initial state: 4.522015708796772E-5

Time for model checking: 1.305 seconds.

Result: 4.522015708796772E-5 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmin=? [ F "MissionTimeUp" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=0,Objy1=2,Objx2=3,Objy2=2

Prob0E: 90 iterations in 0.67 seconds (average 0.007467, setup 0.00)

Prob1A: 3 iterations in 0.04 seconds (average 0.012000, setup 0.00)

yes = 304, no = 118896, maybe = 65237

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=184437, nc=114877, nnz=313239, k=4] [3.9 MB]
Creating vector for yes... [1.4 MB]
Allocating iteration vectors... [2 x 1.4 MB]
TOTAL: [8.1 MB]

Starting iterations...

Iterative method: 112 iterations in 0.37 seconds (average 0.001607, setup 0.19)

Value in the initial state: 1.7426562523492742E-8

Time for model checking: 1.104 seconds.

Result: 1.7426562523492742E-8 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmax=? [ F "Crash" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=0,Objy1=2,Objx2=3,Objy2=2

Prob0A: 3 iterations in 0.07 seconds (average 0.024000, setup 0.00)

Prob1E: 168 iterations in 2.16 seconds (average 0.012881, setup 0.00)

yes = 90435, no = 2765, maybe = 91237

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=184437, nc=152490, nnz=391395, k=4] [4.8 MB]
Creating vector for yes... [1.4 MB]
Allocating iteration vectors... [2 x 1.4 MB]
TOTAL: [9.0 MB]

Starting iterations...

Iterative method: 89 iterations in 0.44 seconds (average 0.001753, setup 0.29)

Value in the initial state: 0.022723276646750234

Time for model checking: 3.759 seconds.

Result: 0.022723276646750234 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmin=? [ F "Crash" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=0,Objy1=2,Objx2=3,Objy2=2

Prob0E: 4 iterations in 0.06 seconds (average 0.015000, setup 0.00)

Prob1A: 45 iterations in 0.48 seconds (average 0.010578, setup 0.00)

yes = 90435, no = 2765, maybe = 91237

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=184437, nc=152490, nnz=391395, k=4] [4.8 MB]
Creating vector for yes... [1.4 MB]
Allocating iteration vectors... [2 x 1.4 MB]
TOTAL: [9.0 MB]

Starting iterations...

Iterative method: 80 iterations in 0.38 seconds (average 0.001750, setup 0.24)

Value in the initial state: 0.013635080768240396

Time for model checking: 0.962 seconds.

Result: 0.013635080768240396 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmax=? [ F "AllAreaSearched" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=0,Objy1=2,Objx2=3,Objy2=2

Prob0A: 51 iterations in 0.28 seconds (average 0.005490, setup 0.00)

Prob1E: 4 iterations in 0.04 seconds (average 0.010000, setup 0.00)

yes = 1403, no = 135068, maybe = 47966

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=184437, nc=75733, nnz=196321, k=2] [2.5 MB]
Creating vector for yes... [1.4 MB]
Allocating iteration vectors... [2 x 1.4 MB]
TOTAL: [6.7 MB]

Starting iterations...

Iterative method: 85 iterations in 0.30 seconds (average 0.001412, setup 0.18)

Value in the initial state: 0.11582928201371818

Time for model checking: 0.822 seconds.

Result: 0.11582928201371818 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmin=? [ F "AllAreaSearched" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=0,Objy1=2,Objx2=3,Objy2=2

Prob0E: 61 iterations in 0.34 seconds (average 0.005639, setup 0.00)

Prob1A: 3 iterations in 0.03 seconds (average 0.009333, setup 0.00)

yes = 1403, no = 141839, maybe = 41195

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=184437, nc=63401, nnz=160536, k=2] [2.1 MB]
Creating vector for yes... [1.4 MB]
Allocating iteration vectors... [2 x 1.4 MB]
TOTAL: [6.3 MB]

Starting iterations...

Iterative method: 96 iterations in 0.24 seconds (average 0.001333, setup 0.11)

Value in the initial state: 0.0018695834031212453

Time for model checking: 0.621 seconds.

Result: 0.0018695834031212453 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmax=? [ F "MissionTimeUp"|"AllAreaSearched"|"Crash" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=0,Objy1=2,Objx2=3,Objy2=2

Prob0A: 3 iterations in 0.07 seconds (average 0.022667, setup 0.00)

Prob1E: 172 iterations in 2.00 seconds (average 0.011628, setup 0.00)

yes = 156053, no = 1058, maybe = 27326

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=184437, nc=49021, nnz=136329, k=2] [1.8 MB]
Creating vector for yes... [1.4 MB]
Allocating iteration vectors... [2 x 1.4 MB]
TOTAL: [6.0 MB]

Starting iterations...

Iterative method: 79 iterations in 0.28 seconds (average 0.001215, setup 0.19)

Value in the initial state: 0.13742999923864335

Time for model checking: 2.739 seconds.

Result: 0.13742999923864335 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmin=? [ F "MissionTimeUp"|"AllAreaSearched"|"Crash" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=0,Objy1=2,Objx2=3,Objy2=2

Prob0E: 4 iterations in 0.04 seconds (average 0.011000, setup 0.00)

Prob1A: 45 iterations in 0.40 seconds (average 0.008889, setup 0.00)

yes = 151428, no = 1058, maybe = 31951

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=184437, nc=57517, nnz=160820, k=2] [2.1 MB]
Creating vector for yes... [1.4 MB]
Allocating iteration vectors... [2 x 1.4 MB]
TOTAL: [6.3 MB]

Starting iterations...

Iterative method: 85 iterations in 0.25 seconds (average 0.001318, setup 0.14)

Value in the initial state: 0.01568819016551943

Time for model checking: 0.944 seconds.

Result: 0.01568819016551943 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"drop"}max=? [ F "deadlock" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=0,Objy1=2,Objx2=3,Objy2=2

Prob0E: 5 iterations in 0.06 seconds (average 0.012000, setup 0.00)

Prob1A: 1 iterations in 0.01 seconds (average 0.008000, setup 0.00)

goal = 1, inf = 0, maybe = 184436

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=184437, nc=245689, nnz=484594, k=4] [7.2 MB]
Building sparse matrix (transition rewards)... [n=184437, nc=245689, nnz=0, k=4] [1.6 MB]
Creating vector for state rewards... [1.4 MB]
Creating vector for inf... [1.4 MB]
Allocating iteration vectors... [2 x 1.4 MB]
TOTAL: [14.5 MB]

Starting iterations...

Iterative method: 84 iterations in 0.57 seconds (average 0.002667, setup 0.35)

Value in the initial state: 0.14308171827743268

Time for model checking: 0.751 seconds.

Result: 0.14308171827743268 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"drop"}min=? [ F "deadlock" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=0,Objy1=2,Objx2=3,Objy2=2

Prob0A: 4 iterations in 0.04 seconds (average 0.009000, setup 0.00)

Prob1E: 5 iterations in 0.05 seconds (average 0.009600, setup 0.00)

Warning: PRISM hasn't checked for zero-reward loops.
Your minimum rewards may be too low...

goal = 1, inf = 0, maybe = 184436

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=184437, nc=245689, nnz=484594, k=4] [7.2 MB]
Building sparse matrix (transition rewards)... [n=184437, nc=245689, nnz=0, k=4] [1.6 MB]
Creating vector for state rewards... [1.4 MB]
Creating vector for inf... [1.4 MB]
Allocating iteration vectors... [2 x 1.4 MB]
TOTAL: [14.5 MB]

Starting iterations...

Iterative method: 78 iterations in 0.46 seconds (average 0.002564, setup 0.26)

Value in the initial state: 0.13596297828100112

Time for model checking: 0.623 seconds.

Result: 0.13596297828100112 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"charge"}max=? [ F "deadlock" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=0,Objy1=2,Objx2=3,Objy2=2

Prob0E: 5 iterations in 0.03 seconds (average 0.005600, setup 0.00)

Prob1A: 1 iterations in 0.00 seconds (average 0.004000, setup 0.00)

goal = 1, inf = 0, maybe = 184436

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=184437, nc=245689, nnz=484594, k=4] [7.2 MB]
Building sparse matrix (transition rewards)... [n=184437, nc=245689, nnz=5118, k=4] [1.7 MB]
Creating vector for state rewards... [1.4 MB]
Creating vector for inf... [1.4 MB]
Allocating iteration vectors... [2 x 1.4 MB]
TOTAL: [14.5 MB]

Starting iterations...

Iterative method: 85 iterations in 0.50 seconds (average 0.002729, setup 0.27)

Value in the initial state: 0.15116687681669772

Time for model checking: 0.566 seconds.

Result: 0.15116687681669772 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"charge"}min=? [ F "deadlock" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=0,Objy1=2,Objx2=3,Objy2=2

Prob0A: 4 iterations in 0.03 seconds (average 0.007000, setup 0.00)

Prob1E: 5 iterations in 0.05 seconds (average 0.009600, setup 0.00)

Warning: PRISM hasn't checked for zero-reward loops.
Your minimum rewards may be too low...

goal = 1, inf = 0, maybe = 184436

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=184437, nc=245689, nnz=484594, k=4] [7.2 MB]
Building sparse matrix (transition rewards)... [n=184437, nc=245689, nnz=5118, k=4] [1.7 MB]
Creating vector for state rewards... [1.4 MB]
Creating vector for inf... [1.4 MB]
Allocating iteration vectors... [2 x 1.4 MB]
TOTAL: [14.5 MB]

Starting iterations...

Iterative method: 96 iterations in 0.52 seconds (average 0.002708, setup 0.26)

Value in the initial state: 0.007917327620867033

Time for model checking: 0.628 seconds.

Result: 0.007917327620867033 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"time"}max=? [ F "deadlock" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=0,Objy1=2,Objx2=3,Objy2=2

Prob0E: 5 iterations in 0.03 seconds (average 0.005600, setup 0.00)

Prob1A: 1 iterations in 0.01 seconds (average 0.008000, setup 0.00)

goal = 1, inf = 0, maybe = 184436

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=184437, nc=245689, nnz=484594, k=4] [7.2 MB]
Building sparse matrix (transition rewards)... [n=184437, nc=245689, nnz=93198, k=4] [2.7 MB]
Creating vector for state rewards... [1.4 MB]
Creating vector for inf... [1.4 MB]
Allocating iteration vectors... [2 x 1.4 MB]
TOTAL: [15.5 MB]

Starting iterations...

Iterative method: 92 iterations in 0.55 seconds (average 0.002783, setup 0.30)

Value in the initial state: 125.68309627731834

Time for model checking: 0.716 seconds.

Result: 125.68309627731834 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"time"}min=? [ F "deadlock" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=0,Objy1=2,Objx2=3,Objy2=2

Prob0A: 4 iterations in 0.03 seconds (average 0.007000, setup 0.00)

Prob1E: 5 iterations in 0.05 seconds (average 0.010400, setup 0.00)

Warning: PRISM hasn't checked for zero-reward loops.
Your minimum rewards may be too low...

goal = 1, inf = 0, maybe = 184436

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=184437, nc=245689, nnz=484594, k=4] [7.2 MB]
Building sparse matrix (transition rewards)... [n=184437, nc=245689, nnz=93198, k=4] [2.7 MB]
Creating vector for state rewards... [1.4 MB]
Creating vector for inf... [1.4 MB]
Allocating iteration vectors... [2 x 1.4 MB]
TOTAL: [15.5 MB]

Starting iterations...

Iterative method: 91 iterations in 0.54 seconds (average 0.002769, setup 0.29)

Value in the initial state: 75.69167661073428

Time for model checking: 0.735 seconds.

Result: 75.69167661073428 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"success"}max=? [ F "deadlock" ]/Pmin=? [ F "MissionSuccessful" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=0,Objy1=2,Objx2=3,Objy2=2

Prob0E: 5 iterations in 0.03 seconds (average 0.005600, setup 0.00)

Prob1A: 1 iterations in 0.01 seconds (average 0.008000, setup 0.00)

goal = 1, inf = 0, maybe = 184436

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=184437, nc=245689, nnz=484594, k=4] [7.2 MB]
Building sparse matrix (transition rewards)... [n=184437, nc=245689, nnz=2084, k=4] [1.7 MB]
Creating vector for state rewards... [1.4 MB]
Creating vector for inf... [1.4 MB]
Allocating iteration vectors... [2 x 1.4 MB]
TOTAL: [14.5 MB]

Starting iterations...

Iterative method: 93 iterations in 0.51 seconds (average 0.002624, setup 0.26)

Prob0E: 45 iterations in 0.30 seconds (average 0.006756, setup 0.00)

Prob1A: 3 iterations in 0.04 seconds (average 0.014667, setup 0.00)

yes = 1057, no = 156054, maybe = 27326

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=184437, nc=49021, nnz=136329, k=2] [1.8 MB]
Creating vector for yes... [1.4 MB]
Allocating iteration vectors... [2 x 1.4 MB]
TOTAL: [6.0 MB]

Starting iterations...

Iterative method: 87 iterations in 0.20 seconds (average 0.001195, setup 0.10)

Value in the initial state: 143.22345958727064

Time for model checking: 1.183 seconds.

Result: 143.22345958727064 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"success"}min=? [ F "deadlock" ]/Pmax=? [ F "MissionSuccessful" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=0,Objy1=2,Objx2=3,Objy2=2

Prob0A: 4 iterations in 0.03 seconds (average 0.007000, setup 0.00)

Prob1E: 5 iterations in 0.04 seconds (average 0.008800, setup 0.00)

Warning: PRISM hasn't checked for zero-reward loops.
Your minimum rewards may be too low...

goal = 1, inf = 0, maybe = 184436

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=184437, nc=245689, nnz=484594, k=4] [7.2 MB]
Building sparse matrix (transition rewards)... [n=184437, nc=245689, nnz=2084, k=4] [1.7 MB]
Creating vector for state rewards... [1.4 MB]
Creating vector for inf... [1.4 MB]
Allocating iteration vectors... [2 x 1.4 MB]
TOTAL: [14.5 MB]

Starting iterations...

Iterative method: 90 iterations in 0.52 seconds (average 0.002756, setup 0.27)

Prob0A: 45 iterations in 0.29 seconds (average 0.006400, setup 0.00)

Prob1E: 4 iterations in 0.03 seconds (average 0.008000, setup 0.00)

yes = 1057, no = 151429, maybe = 31951

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=184437, nc=57517, nnz=160820, k=2] [2.1 MB]
Creating vector for yes... [1.4 MB]
Allocating iteration vectors... [2 x 1.4 MB]
TOTAL: [6.3 MB]

Starting iterations...

Iterative method: 88 iterations in 0.25 seconds (average 0.001318, setup 0.13)

Value in the initial state: 73.35315295601657

Time for model checking: 1.262 seconds.

Result: 73.35315295601657 (value in the initial state)

---------------------------------------------------------------------

Note: There were 16 warnings during computation.

