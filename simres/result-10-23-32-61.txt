PRISM
=====

Version: 4.3
Date: Wed Aug 03 16:14:17 BST 2016
Hostname: eglinton
Memory limits: cudd=1g, java(heap)=981.5m
Command line: prism uav_sim_mdp.prism -s uav_mdp.props -const 'Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=3,Objy1=2,Objx2=6,Objy2=1,'

Parsing model file "uav_sim_mdp.prism"...

Parsing properties file "uav_mdp.props"...

18 properties:
(1) Pmax=? [ F "MissionSuccessful" ]
(2) Pmin=? [ F "MissionSuccessful" ]
(3) Pmax=? [ F "MissionTimeUp" ]
(4) Pmin=? [ F "MissionTimeUp" ]
(5) Pmax=? [ F "Crash" ]
(6) Pmin=? [ F "Crash" ]
(7) Pmax=? [ F "AllAreaSearched" ]
(8) Pmin=? [ F "AllAreaSearched" ]
(9) Pmax=? [ F "MissionTimeUp"|"AllAreaSearched"|"Crash" ]
(10) Pmin=? [ F "MissionTimeUp"|"AllAreaSearched"|"Crash" ]
(11) R{"drop"}max=? [ F "deadlock" ]
(12) R{"drop"}min=? [ F "deadlock" ]
(13) R{"charge"}max=? [ F "deadlock" ]
(14) R{"charge"}min=? [ F "deadlock" ]
(15) R{"time"}max=? [ F "deadlock" ]
(16) R{"time"}min=? [ F "deadlock" ]
(17) R{"success"}max=? [ F "deadlock" ]/Pmin=? [ F "MissionSuccessful" ]
(18) R{"success"}min=? [ F "deadlock" ]/Pmax=? [ F "MissionSuccessful" ]

Type:        MDP
Modules:     Obj Move Robot T 
Variables:   NoOfObjs posx posy s1 retx rety b t c 

---------------------------------------------------------------------

Model checking: Pmax=? [ F "MissionSuccessful" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=3,Objy1=2,Objx2=6,Objy2=1

Building model...
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=3,Objy1=2,Objx2=6,Objy2=1

Warning: Guard for command 7 of module "Move" is never satisfied.

Warning: Guard for command 8 of module "Move" is never satisfied.

Warning: Guard for command 9 of module "Move" is never satisfied.

Warning: Guard for command 7 of module "Robot" is never satisfied.

Warning: Update 2 of command 3 of module "Robot" doesn't do anything ("(s1'=13)", line 210, column 44)

Warning: Update 2 of command 27 of module "T" doesn't do anything ("(c'=1)", line 323, column 108)

Warning: Guard for command 28 of module "T" is never satisfied.

Warning: Update 2 of command 9 of module "T" doesn't do anything ("(c'=1)", line 300, column 112)

Warning: Guard for command 10 of module "T" is never satisfied.

Warning: Update 2 of command 51 of module "T" doesn't do anything ("(c'=1)", line 354, column 113)

Warning: Guard for command 52 of module "T" is never satisfied.

Computing reachable states...

Reachability (BFS): 89 iterations in 1.43 seconds (average 0.016045, setup 0.00)

Time for model construction: 3.619 seconds.

Warning: Deadlocks detected and fixed in 1 states

Type:        MDP
States:      259595 (1 initial)
Transitions: 676029
Choices:     341722

Transition matrix: 216875 nodes (93 terminal), 676029 minterms, vars: 34r/34c/18nd

Prob0A: 50 iterations in 0.45 seconds (average 0.009040, setup 0.00)

Prob1E: 4 iterations in 0.08 seconds (average 0.020000, setup 0.00)

yes = 1185, no = 219418, maybe = 38992

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=259595, nc=67668, nnz=189937, k=2] [2.5 MB]
Creating vector for yes... [2.0 MB]
Allocating iteration vectors... [2 x 2.0 MB]
TOTAL: [8.4 MB]

Starting iterations...

Iterative method: 96 iterations in 0.44 seconds (average 0.001792, setup 0.27)

Value in the initial state: 0.9823918018674949

Time for model checking: 1.004 seconds.

Result: 0.9823918018674949 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmin=? [ F "MissionSuccessful" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=3,Objy1=2,Objx2=6,Objy2=1

Prob0E: 52 iterations in 0.50 seconds (average 0.009692, setup 0.00)

Prob1A: 3 iterations in 0.06 seconds (average 0.021333, setup 0.00)

yes = 1185, no = 226915, maybe = 31495

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=259595, nc=54275, nnz=150497, k=2] [2.0 MB]
Creating vector for yes... [2.0 MB]
Allocating iteration vectors... [2 x 2.0 MB]
TOTAL: [8.0 MB]

Starting iterations...

Iterative method: 89 iterations in 0.35 seconds (average 0.001573, setup 0.21)

Value in the initial state: 0.8230943208048793

Time for model checking: 0.951 seconds.

Result: 0.8230943208048793 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmax=? [ F "MissionTimeUp" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=3,Objy1=2,Objx2=6,Objy2=1

Prob0A: 69 iterations in 0.74 seconds (average 0.010667, setup 0.00)

Prob1E: 4 iterations in 0.05 seconds (average 0.013000, setup 0.00)

yes = 436, no = 159279, maybe = 99880

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=259595, nc=169144, nnz=458207, k=4] [5.7 MB]
Creating vector for yes... [2.0 MB]
Allocating iteration vectors... [2 x 2.0 MB]
TOTAL: [11.6 MB]

Starting iterations...

Iterative method: 95 iterations in 0.52 seconds (average 0.002189, setup 0.31)

Value in the initial state: 4.6350668771090263E-4

Time for model checking: 1.335 seconds.

Result: 4.6350668771090263E-4 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmin=? [ F "MissionTimeUp" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=3,Objy1=2,Objx2=6,Objy2=1

Prob0E: 84 iterations in 1.10 seconds (average 0.013048, setup 0.00)

Prob1A: 3 iterations in 0.06 seconds (average 0.018667, setup 0.00)

yes = 436, no = 159728, maybe = 99431

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=259595, nc=168350, nnz=456486, k=4] [5.6 MB]
Creating vector for yes... [2.0 MB]
Allocating iteration vectors... [2 x 2.0 MB]
TOTAL: [11.6 MB]

Starting iterations...

Iterative method: 112 iterations in 0.56 seconds (average 0.002250, setup 0.31)

Value in the initial state: 7.576567058072049E-7

Time for model checking: 1.742 seconds.

Result: 7.576567058072049E-7 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmax=? [ F "Crash" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=3,Objy1=2,Objx2=6,Objy2=1

Prob0A: 3 iterations in 0.09 seconds (average 0.029333, setup 0.00)

Prob1E: 191 iterations in 3.14 seconds (average 0.016419, setup 0.00)

yes = 127230, no = 3265, maybe = 129100

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=259595, nc=211227, nnz=545534, k=4] [6.7 MB]
Creating vector for yes... [2.0 MB]
Allocating iteration vectors... [2 x 2.0 MB]
TOTAL: [12.6 MB]

Starting iterations...

Iterative method: 92 iterations in 0.58 seconds (average 0.002435, setup 0.36)

Value in the initial state: 0.029960112757168696

Time for model checking: 3.852 seconds.

Result: 0.029960112757168696 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmin=? [ F "Crash" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=3,Objy1=2,Objx2=6,Objy2=1

Prob0E: 3 iterations in 0.08 seconds (average 0.026667, setup 0.00)

Prob1A: 47 iterations in 0.68 seconds (average 0.014553, setup 0.00)

yes = 127230, no = 3265, maybe = 129100

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=259595, nc=211227, nnz=545534, k=4] [6.7 MB]
Creating vector for yes... [2.0 MB]
Allocating iteration vectors... [2 x 2.0 MB]
TOTAL: [12.6 MB]

Starting iterations...

Iterative method: 87 iterations in 0.52 seconds (average 0.002483, setup 0.30)

Value in the initial state: 0.015570056642707691

Time for model checking: 1.311 seconds.

Result: 0.015570056642707691 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmax=? [ F "AllAreaSearched" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=3,Objy1=2,Objx2=6,Objy2=1

Prob0A: 57 iterations in 0.58 seconds (average 0.010175, setup 0.00)

Prob1E: 4 iterations in 0.05 seconds (average 0.013000, setup 0.00)

yes = 1643, no = 205103, maybe = 52849

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=259595, nc=80119, nnz=203379, k=2] [2.7 MB]
Creating vector for yes... [2.0 MB]
Allocating iteration vectors... [2 x 2.0 MB]
TOTAL: [8.6 MB]

Starting iterations...

Iterative method: 87 iterations in 0.35 seconds (average 0.001839, setup 0.19)

Value in the initial state: 0.15027423717963623

Time for model checking: 0.982 seconds.

Result: 0.15027423717963623 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmin=? [ F "AllAreaSearched" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=3,Objy1=2,Objx2=6,Objy2=1

Prob0E: 63 iterations in 0.41 seconds (average 0.006476, setup 0.00)

Prob1A: 3 iterations in 0.04 seconds (average 0.013333, setup 0.00)

yes = 1643, no = 212300, maybe = 45652

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=259595, nc=68637, nnz=172220, k=2] [2.3 MB]
Creating vector for yes... [2.0 MB]
Allocating iteration vectors... [2 x 2.0 MB]
TOTAL: [8.2 MB]

Starting iterations...

Iterative method: 93 iterations in 0.28 seconds (average 0.001677, setup 0.13)

Value in the initial state: 0.0017562915781867256

Time for model checking: 0.757 seconds.

Result: 0.0017562915781867256 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmax=? [ F "MissionTimeUp"|"AllAreaSearched"|"Crash" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=3,Objy1=2,Objx2=6,Objy2=1

Prob0A: 3 iterations in 0.10 seconds (average 0.032000, setup 0.00)

Prob1E: 200 iterations in 2.84 seconds (average 0.014220, setup 0.00)

yes = 226914, no = 1186, maybe = 31495

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=259595, nc=54275, nnz=150497, k=2] [2.0 MB]
Creating vector for yes... [2.0 MB]
Allocating iteration vectors... [2 x 2.0 MB]
TOTAL: [8.0 MB]

Starting iterations...

Iterative method: 84 iterations in 0.36 seconds (average 0.001714, setup 0.22)

Value in the initial state: 0.1769053244715495

Time for model checking: 3.344 seconds.

Result: 0.1769053244715495 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmin=? [ F "MissionTimeUp"|"AllAreaSearched"|"Crash" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=3,Objy1=2,Objx2=6,Objy2=1

Prob0E: 3 iterations in 0.09 seconds (average 0.029333, setup 0.00)

Prob1A: 50 iterations in 0.51 seconds (average 0.010240, setup 0.00)

yes = 219417, no = 1186, maybe = 38992

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=259595, nc=67668, nnz=189937, k=2] [2.5 MB]
Creating vector for yes... [2.0 MB]
Allocating iteration vectors... [2 x 2.0 MB]
TOTAL: [8.4 MB]

Starting iterations...

Iterative method: 95 iterations in 0.36 seconds (average 0.001684, setup 0.20)

Value in the initial state: 0.017607025491083585

Time for model checking: 1.0 seconds.

Result: 0.017607025491083585 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"drop"}max=? [ F "deadlock" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=3,Objy1=2,Objx2=6,Objy2=1

Prob0E: 4 iterations in 0.07 seconds (average 0.018000, setup 0.00)

Prob1A: 1 iterations in 0.01 seconds (average 0.008000, setup 0.00)

goal = 1, inf = 0, maybe = 259594

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=259595, nc=341721, nnz=676028, k=4] [10.0 MB]
Building sparse matrix (transition rewards)... [n=259595, nc=341721, nnz=0, k=4] [2.3 MB]
Creating vector for state rewards... [2.0 MB]
Creating vector for inf... [2.0 MB]
Allocating iteration vectors... [2 x 2.0 MB]
TOTAL: [20.2 MB]

Starting iterations...

Iterative method: 92 iterations in 0.82 seconds (average 0.003783, setup 0.48)

Value in the initial state: 0.205460041001005

Time for model checking: 0.913 seconds.

Result: 0.205460041001005 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"drop"}min=? [ F "deadlock" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=3,Objy1=2,Objx2=6,Objy2=1

Prob0A: 4 iterations in 0.06 seconds (average 0.016000, setup 0.00)

Prob1E: 5 iterations in 0.06 seconds (average 0.011200, setup 0.00)

Warning: PRISM hasn't checked for zero-reward loops.
Your minimum rewards may be too low...

goal = 1, inf = 0, maybe = 259594

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=259595, nc=341721, nnz=676028, k=4] [10.0 MB]
Building sparse matrix (transition rewards)... [n=259595, nc=341721, nnz=0, k=4] [2.3 MB]
Creating vector for state rewards... [2.0 MB]
Creating vector for inf... [2.0 MB]
Allocating iteration vectors... [2 x 2.0 MB]
TOTAL: [20.2 MB]

Starting iterations...

Iterative method: 82 iterations in 0.67 seconds (average 0.003756, setup 0.36)

Value in the initial state: 0.19109231427833495

Time for model checking: 0.808 seconds.

Result: 0.19109231427833495 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"charge"}max=? [ F "deadlock" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=3,Objy1=2,Objx2=6,Objy2=1

Prob0E: 4 iterations in 0.04 seconds (average 0.009000, setup 0.00)

Prob1A: 1 iterations in 0.00 seconds (average 0.004000, setup 0.00)

goal = 1, inf = 0, maybe = 259594

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=259595, nc=341721, nnz=676028, k=4] [10.0 MB]
Building sparse matrix (transition rewards)... [n=259595, nc=341721, nnz=7488, k=4] [2.4 MB]
Creating vector for state rewards... [2.0 MB]
Creating vector for inf... [2.0 MB]
Allocating iteration vectors... [2 x 2.0 MB]
TOTAL: [20.3 MB]

Starting iterations...

Iterative method: 91 iterations in 0.70 seconds (average 0.003736, setup 0.36)

Value in the initial state: 0.2922518416888728

Time for model checking: 0.756 seconds.

Result: 0.2922518416888728 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"charge"}min=? [ F "deadlock" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=3,Objy1=2,Objx2=6,Objy2=1

Prob0A: 4 iterations in 0.03 seconds (average 0.007000, setup 0.00)

Prob1E: 5 iterations in 0.05 seconds (average 0.010400, setup 0.00)

Warning: PRISM hasn't checked for zero-reward loops.
Your minimum rewards may be too low...

goal = 1, inf = 0, maybe = 259594

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=259595, nc=341721, nnz=676028, k=4] [10.0 MB]
Building sparse matrix (transition rewards)... [n=259595, nc=341721, nnz=7488, k=4] [2.4 MB]
Creating vector for state rewards... [2.0 MB]
Creating vector for inf... [2.0 MB]
Allocating iteration vectors... [2 x 2.0 MB]
TOTAL: [20.3 MB]

Starting iterations...

Iterative method: 98 iterations in 0.79 seconds (average 0.004000, setup 0.40)

Value in the initial state: 0.026804657865123437

Time for model checking: 0.887 seconds.

Result: 0.026804657865123437 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"time"}max=? [ F "deadlock" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=3,Objy1=2,Objx2=6,Objy2=1

Prob0E: 4 iterations in 0.03 seconds (average 0.007000, setup 0.00)

Prob1A: 1 iterations in 0.01 seconds (average 0.008000, setup 0.00)

goal = 1, inf = 0, maybe = 259594

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=259595, nc=341721, nnz=676028, k=4] [10.0 MB]
Building sparse matrix (transition rewards)... [n=259595, nc=341721, nnz=130493, k=4] [3.8 MB]
Creating vector for state rewards... [2.0 MB]
Creating vector for inf... [2.0 MB]
Allocating iteration vectors... [2 x 2.0 MB]
TOTAL: [21.7 MB]

Starting iterations...

Iterative method: 94 iterations in 0.75 seconds (average 0.003787, setup 0.40)

Value in the initial state: 165.31177822788752

Time for model checking: 0.797 seconds.

Result: 165.31177822788752 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"time"}min=? [ F "deadlock" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=3,Objy1=2,Objx2=6,Objy2=1

Prob0A: 4 iterations in 0.03 seconds (average 0.007000, setup 0.00)

Prob1E: 5 iterations in 0.04 seconds (average 0.008000, setup 0.00)

Warning: PRISM hasn't checked for zero-reward loops.
Your minimum rewards may be too low...

goal = 1, inf = 0, maybe = 259594

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=259595, nc=341721, nnz=676028, k=4] [10.0 MB]
Building sparse matrix (transition rewards)... [n=259595, nc=341721, nnz=130493, k=4] [3.8 MB]
Creating vector for state rewards... [2.0 MB]
Creating vector for inf... [2.0 MB]
Allocating iteration vectors... [2 x 2.0 MB]
TOTAL: [21.7 MB]

Starting iterations...

Iterative method: 102 iterations in 0.76 seconds (average 0.003725, setup 0.38)

Value in the initial state: 86.3587759764832

Time for model checking: 0.851 seconds.

Result: 86.3587759764832 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"success"}max=? [ F "deadlock" ]/Pmin=? [ F "MissionSuccessful" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=3,Objy1=2,Objx2=6,Objy2=1

Prob0E: 4 iterations in 0.03 seconds (average 0.007000, setup 0.00)

Prob1A: 1 iterations in 0.00 seconds (average 0.004000, setup 0.00)

goal = 1, inf = 0, maybe = 259594

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=259595, nc=341721, nnz=676028, k=4] [10.0 MB]
Building sparse matrix (transition rewards)... [n=259595, nc=341721, nnz=2334, k=4] [2.3 MB]
Creating vector for state rewards... [2.0 MB]
Creating vector for inf... [2.0 MB]
Allocating iteration vectors... [2 x 2.0 MB]
TOTAL: [20.3 MB]

Starting iterations...

Iterative method: 98 iterations in 0.70 seconds (average 0.003633, setup 0.35)

Prob0E: 52 iterations in 0.38 seconds (average 0.007308, setup 0.00)

Prob1A: 3 iterations in 0.05 seconds (average 0.016000, setup 0.00)

yes = 1185, no = 226915, maybe = 31495

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=259595, nc=54275, nnz=150497, k=2] [2.0 MB]
Creating vector for yes... [2.0 MB]
Allocating iteration vectors... [2 x 2.0 MB]
TOTAL: [8.0 MB]

Starting iterations...

Iterative method: 89 iterations in 0.39 seconds (average 0.002337, setup 0.18)

Value in the initial state: 197.0224355553289

Time for model checking: 1.601 seconds.

Result: 197.0224355553289 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"success"}min=? [ F "deadlock" ]/Pmax=? [ F "MissionSuccessful" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=3,Objy1=2,Objx2=6,Objy2=1

Prob0A: 4 iterations in 0.09 seconds (average 0.022000, setup 0.00)

Prob1E: 5 iterations in 0.05 seconds (average 0.009600, setup 0.00)

Warning: PRISM hasn't checked for zero-reward loops.
Your minimum rewards may be too low...

goal = 1, inf = 0, maybe = 259594

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=259595, nc=341721, nnz=676028, k=4] [10.0 MB]
Building sparse matrix (transition rewards)... [n=259595, nc=341721, nnz=2334, k=4] [2.3 MB]
Creating vector for state rewards... [2.0 MB]
Creating vector for inf... [2.0 MB]
Allocating iteration vectors... [2 x 2.0 MB]
TOTAL: [20.3 MB]

Starting iterations...

Iterative method: 93 iterations in 1.09 seconds (average 0.004903, setup 0.63)

Prob0A: 50 iterations in 0.38 seconds (average 0.007680, setup 0.00)

Prob1E: 4 iterations in 0.05 seconds (average 0.012000, setup 0.00)

yes = 1185, no = 219418, maybe = 38992

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=259595, nc=67668, nnz=189937, k=2] [2.5 MB]
Creating vector for yes... [2.0 MB]
Allocating iteration vectors... [2 x 2.0 MB]
TOTAL: [8.4 MB]

Starting iterations...

Iterative method: 96 iterations in 0.40 seconds (average 0.001750, setup 0.23)

Value in the initial state: 76.91311807856984

Time for model checking: 2.076 seconds.

Result: 76.91311807856984 (value in the initial state)

---------------------------------------------------------------------

Note: There were 16 warnings during computation.

