PRISM
=====

Version: 4.3
Date: Wed Aug 03 15:57:21 BST 2016
Hostname: eglinton
Memory limits: cudd=1g, java(heap)=981.5m
Command line: prism uav_sim_mdp.prism -s uav_mdp.props -const 'Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=2,Objy1=1,Objx2=5,Objy2=0,'

Parsing model file "uav_sim_mdp.prism"...

Parsing properties file "uav_mdp.props"...

18 properties:
(1) Pmax=? [ F "MissionSuccessful" ]
(2) Pmin=? [ F "MissionSuccessful" ]
(3) Pmax=? [ F "MissionTimeUp" ]
(4) Pmin=? [ F "MissionTimeUp" ]
(5) Pmax=? [ F "Crash" ]
(6) Pmin=? [ F "Crash" ]
(7) Pmax=? [ F "AllAreaSearched" ]
(8) Pmin=? [ F "AllAreaSearched" ]
(9) Pmax=? [ F "MissionTimeUp"|"AllAreaSearched"|"Crash" ]
(10) Pmin=? [ F "MissionTimeUp"|"AllAreaSearched"|"Crash" ]
(11) R{"drop"}max=? [ F "deadlock" ]
(12) R{"drop"}min=? [ F "deadlock" ]
(13) R{"charge"}max=? [ F "deadlock" ]
(14) R{"charge"}min=? [ F "deadlock" ]
(15) R{"time"}max=? [ F "deadlock" ]
(16) R{"time"}min=? [ F "deadlock" ]
(17) R{"success"}max=? [ F "deadlock" ]/Pmin=? [ F "MissionSuccessful" ]
(18) R{"success"}min=? [ F "deadlock" ]/Pmax=? [ F "MissionSuccessful" ]

Type:        MDP
Modules:     Obj Move Robot T 
Variables:   NoOfObjs posx posy s1 retx rety b t c 

---------------------------------------------------------------------

Model checking: Pmax=? [ F "MissionSuccessful" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=2,Objy1=1,Objx2=5,Objy2=0

Building model...
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=2,Objy1=1,Objx2=5,Objy2=0

Warning: Guard for command 7 of module "Move" is never satisfied.

Warning: Guard for command 8 of module "Move" is never satisfied.

Warning: Guard for command 9 of module "Move" is never satisfied.

Warning: Guard for command 7 of module "Robot" is never satisfied.

Warning: Update 2 of command 3 of module "Robot" doesn't do anything ("(s1'=13)", line 210, column 44)

Warning: Update 2 of command 27 of module "T" doesn't do anything ("(c'=1)", line 323, column 108)

Warning: Guard for command 28 of module "T" is never satisfied.

Warning: Update 2 of command 9 of module "T" doesn't do anything ("(c'=1)", line 300, column 112)

Warning: Guard for command 10 of module "T" is never satisfied.

Warning: Update 2 of command 51 of module "T" doesn't do anything ("(c'=1)", line 354, column 113)

Warning: Guard for command 52 of module "T" is never satisfied.

Computing reachable states...

Reachability (BFS): 87 iterations in 1.48 seconds (average 0.017057, setup 0.00)

Time for model construction: 3.734 seconds.

Warning: Deadlocks detected and fixed in 1 states

Type:        MDP
States:      259506 (1 initial)
Transitions: 675834
Choices:     340819

Transition matrix: 234667 nodes (93 terminal), 675834 minterms, vars: 34r/34c/18nd

Prob0A: 46 iterations in 0.42 seconds (average 0.009043, setup 0.00)

Prob1E: 4 iterations in 0.07 seconds (average 0.017000, setup 0.00)

yes = 1080, no = 220868, maybe = 37558

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=259506, nc=66719, nnz=190081, k=2] [2.5 MB]
Creating vector for yes... [2.0 MB]
Allocating iteration vectors... [2 x 2.0 MB]
TOTAL: [8.4 MB]

Starting iterations...

Iterative method: 88 iterations in 0.44 seconds (average 0.001864, setup 0.28)

Value in the initial state: 0.9839154843542938

Time for model checking: 0.955 seconds.

Result: 0.9839154843542938 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmin=? [ F "MissionSuccessful" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=2,Objy1=1,Objx2=5,Objy2=0

Prob0E: 48 iterations in 0.48 seconds (average 0.009917, setup 0.00)

Prob1A: 3 iterations in 0.06 seconds (average 0.021333, setup 0.00)

yes = 1080, no = 226717, maybe = 31709

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=259506, nc=56105, nnz=159156, k=2] [2.1 MB]
Creating vector for yes... [2.0 MB]
Allocating iteration vectors... [2 x 2.0 MB]
TOTAL: [8.1 MB]

Starting iterations...

Iterative method: 83 iterations in 0.37 seconds (average 0.001831, setup 0.22)

Value in the initial state: 0.9209142627421785

Time for model checking: 0.936 seconds.

Result: 0.9209142627421785 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmax=? [ F "MissionTimeUp" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=2,Objy1=1,Objx2=5,Objy2=0

Prob0A: 69 iterations in 0.82 seconds (average 0.011942, setup 0.00)

Prob1E: 4 iterations in 0.05 seconds (average 0.013000, setup 0.00)

yes = 476, no = 163397, maybe = 95633

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=259506, nc=165327, nnz=452656, k=4] [5.6 MB]
Creating vector for yes... [2.0 MB]
Allocating iteration vectors... [2 x 2.0 MB]
TOTAL: [11.5 MB]

Starting iterations...

Iterative method: 95 iterations in 0.56 seconds (average 0.002274, setup 0.34)

Value in the initial state: 1.631922315297491E-4

Time for model checking: 1.459 seconds.

Result: 1.631922315297491E-4 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmin=? [ F "MissionTimeUp" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=2,Objy1=1,Objx2=5,Objy2=0

Prob0E: 83 iterations in 1.10 seconds (average 0.013301, setup 0.00)

Prob1A: 3 iterations in 0.06 seconds (average 0.021333, setup 0.00)

yes = 476, no = 163800, maybe = 95230

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=259506, nc=164611, nnz=451141, k=4] [5.6 MB]
Creating vector for yes... [2.0 MB]
Allocating iteration vectors... [2 x 2.0 MB]
TOTAL: [11.5 MB]

Starting iterations...

Iterative method: 112 iterations in 0.60 seconds (average 0.002321, setup 0.34)

Value in the initial state: 1.9806553261909272E-7

Time for model checking: 1.778 seconds.

Result: 1.9806553261909272E-7 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmax=? [ F "Crash" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=2,Objy1=1,Objx2=5,Objy2=0

Prob0A: 3 iterations in 0.13 seconds (average 0.042667, setup 0.00)

Prob1E: 178 iterations in 3.30 seconds (average 0.018562, setup 0.00)

yes = 127160, no = 2990, maybe = 129356

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=259506, nc=210669, nnz=545684, k=4] [6.7 MB]
Creating vector for yes... [2.0 MB]
Allocating iteration vectors... [2 x 2.0 MB]
TOTAL: [12.6 MB]

Starting iterations...

Iterative method: 88 iterations in 0.62 seconds (average 0.002500, setup 0.40)

Value in the initial state: 0.024606639239812215

Time for model checking: 4.079 seconds.

Result: 0.024606639239812215 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmin=? [ F "Crash" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=2,Objy1=1,Objx2=5,Objy2=0

Prob0E: 4 iterations in 0.12 seconds (average 0.029000, setup 0.00)

Prob1A: 47 iterations in 0.72 seconds (average 0.015404, setup 0.00)

yes = 127160, no = 2990, maybe = 129356

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=259506, nc=210669, nnz=545684, k=4] [6.7 MB]
Creating vector for yes... [2.0 MB]
Allocating iteration vectors... [2 x 2.0 MB]
TOTAL: [12.6 MB]

Starting iterations...

Iterative method: 81 iterations in 0.54 seconds (average 0.002519, setup 0.34)

Value in the initial state: 0.014037909522484005

Time for model checking: 1.418 seconds.

Result: 0.014037909522484005 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmax=? [ F "AllAreaSearched" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=2,Objy1=1,Objx2=5,Objy2=0

Prob0A: 59 iterations in 0.62 seconds (average 0.010576, setup 0.00)

Prob1E: 4 iterations in 0.06 seconds (average 0.016000, setup 0.00)

yes = 1433, no = 201543, maybe = 56530

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=259506, nc=84339, nnz=216024, k=2] [2.8 MB]
Creating vector for yes... [2.0 MB]
Allocating iteration vectors... [2 x 2.0 MB]
TOTAL: [8.7 MB]

Starting iterations...

Iterative method: 92 iterations in 0.41 seconds (average 0.001957, setup 0.23)

Value in the initial state: 0.05717979118474704

Time for model checking: 1.123 seconds.

Result: 0.05717979118474704 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmin=? [ F "AllAreaSearched" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=2,Objy1=1,Objx2=5,Objy2=0

Prob0E: 63 iterations in 0.61 seconds (average 0.009651, setup 0.00)

Prob1A: 3 iterations in 0.06 seconds (average 0.020000, setup 0.00)

yes = 1433, no = 207611, maybe = 50462

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=259506, nc=73989, nnz=187078, k=2] [2.5 MB]
Creating vector for yes... [2.0 MB]
Allocating iteration vectors... [2 x 2.0 MB]
TOTAL: [8.4 MB]

Starting iterations...

Iterative method: 95 iterations in 0.34 seconds (average 0.001853, setup 0.17)

Value in the initial state: 0.0018303974819535362

Time for model checking: 1.034 seconds.

Result: 0.0018303974819535362 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmax=? [ F "MissionTimeUp"|"AllAreaSearched"|"Crash" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=2,Objy1=1,Objx2=5,Objy2=0

Prob0A: 3 iterations in 0.10 seconds (average 0.033333, setup 0.00)

Prob1E: 184 iterations in 2.80 seconds (average 0.015217, setup 0.00)

yes = 226716, no = 1081, maybe = 31709

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=259506, nc=56105, nnz=159156, k=2] [2.1 MB]
Creating vector for yes... [2.0 MB]
Allocating iteration vectors... [2 x 2.0 MB]
TOTAL: [8.1 MB]

Starting iterations...

Iterative method: 78 iterations in 0.34 seconds (average 0.001692, setup 0.21)

Value in the initial state: 0.07908544731584016

Time for model checking: 3.289 seconds.

Result: 0.07908544731584016 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmin=? [ F "MissionTimeUp"|"AllAreaSearched"|"Crash" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=2,Objy1=1,Objx2=5,Objy2=0

Prob0E: 4 iterations in 0.08 seconds (average 0.021000, setup 0.00)

Prob1A: 46 iterations in 0.52 seconds (average 0.011217, setup 0.00)

yes = 220867, no = 1081, maybe = 37558

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=259506, nc=66719, nnz=190081, k=2] [2.5 MB]
Creating vector for yes... [2.0 MB]
Allocating iteration vectors... [2 x 2.0 MB]
TOTAL: [8.4 MB]

Starting iterations...

Iterative method: 88 iterations in 0.35 seconds (average 0.001773, setup 0.19)

Value in the initial state: 0.016082812702757555

Time for model checking: 0.984 seconds.

Result: 0.016082812702757555 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"drop"}max=? [ F "deadlock" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=2,Objy1=1,Objx2=5,Objy2=0

Prob0E: 5 iterations in 0.10 seconds (average 0.019200, setup 0.00)

Prob1A: 1 iterations in 0.01 seconds (average 0.008000, setup 0.00)

goal = 1, inf = 0, maybe = 259505

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=259506, nc=340818, nnz=675833, k=4] [10.0 MB]
Building sparse matrix (transition rewards)... [n=259506, nc=340818, nnz=0, k=4] [2.3 MB]
Creating vector for state rewards... [2.0 MB]
Creating vector for inf... [2.0 MB]
Allocating iteration vectors... [2 x 2.0 MB]
TOTAL: [20.2 MB]

Starting iterations...

Iterative method: 84 iterations in 0.83 seconds (average 0.003714, setup 0.52)

Value in the initial state: 0.2045258216245771

Time for model checking: 0.951 seconds.

Result: 0.2045258216245771 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"drop"}min=? [ F "deadlock" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=2,Objy1=1,Objx2=5,Objy2=0

Prob0A: 4 iterations in 0.06 seconds (average 0.016000, setup 0.00)

Prob1E: 5 iterations in 0.06 seconds (average 0.012000, setup 0.00)

Warning: PRISM hasn't checked for zero-reward loops.
Your minimum rewards may be too low...

goal = 1, inf = 0, maybe = 259505

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=259506, nc=340818, nnz=675833, k=4] [10.0 MB]
Building sparse matrix (transition rewards)... [n=259506, nc=340818, nnz=0, k=4] [2.3 MB]
Creating vector for state rewards... [2.0 MB]
Creating vector for inf... [2.0 MB]
Allocating iteration vectors... [2 x 2.0 MB]
TOTAL: [20.2 MB]

Starting iterations...

Iterative method: 77 iterations in 0.66 seconds (average 0.003792, setup 0.37)

Value in the initial state: 0.18988192420283967

Time for model checking: 0.8 seconds.

Result: 0.18988192420283967 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"charge"}max=? [ F "deadlock" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=2,Objy1=1,Objx2=5,Objy2=0

Prob0E: 5 iterations in 0.04 seconds (average 0.007200, setup 0.00)

Prob1A: 1 iterations in 0.00 seconds (average 0.004000, setup 0.00)

goal = 1, inf = 0, maybe = 259505

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=259506, nc=340818, nnz=675833, k=4] [10.0 MB]
Building sparse matrix (transition rewards)... [n=259506, nc=340818, nnz=7309, k=4] [2.4 MB]
Creating vector for state rewards... [2.0 MB]
Creating vector for inf... [2.0 MB]
Allocating iteration vectors... [2 x 2.0 MB]
TOTAL: [20.3 MB]

Starting iterations...

Iterative method: 84 iterations in 0.69 seconds (average 0.003714, setup 0.38)

Value in the initial state: 0.18377710794267607

Time for model checking: 0.747 seconds.

Result: 0.18377710794267607 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"charge"}min=? [ F "deadlock" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=2,Objy1=1,Objx2=5,Objy2=0

Prob0A: 4 iterations in 0.03 seconds (average 0.008000, setup 0.00)

Prob1E: 5 iterations in 0.06 seconds (average 0.012000, setup 0.00)

Warning: PRISM hasn't checked for zero-reward loops.
Your minimum rewards may be too low...

goal = 1, inf = 0, maybe = 259505

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=259506, nc=340818, nnz=675833, k=4] [10.0 MB]
Building sparse matrix (transition rewards)... [n=259506, nc=340818, nnz=7309, k=4] [2.4 MB]
Creating vector for state rewards... [2.0 MB]
Creating vector for inf... [2.0 MB]
Allocating iteration vectors... [2 x 2.0 MB]
TOTAL: [20.3 MB]

Starting iterations...

Iterative method: 93 iterations in 0.72 seconds (average 0.003828, setup 0.37)

Value in the initial state: 0.014059158738358564

Time for model checking: 0.835 seconds.

Result: 0.014059158738358564 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"time"}max=? [ F "deadlock" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=2,Objy1=1,Objx2=5,Objy2=0

Prob0E: 5 iterations in 0.07 seconds (average 0.014400, setup 0.00)

Prob1A: 1 iterations in 0.01 seconds (average 0.008000, setup 0.00)

goal = 1, inf = 0, maybe = 259505

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=259506, nc=340818, nnz=675833, k=4] [10.0 MB]
Building sparse matrix (transition rewards)... [n=259506, nc=340818, nnz=130148, k=4] [3.8 MB]
Creating vector for state rewards... [2.0 MB]
Creating vector for inf... [2.0 MB]
Allocating iteration vectors... [2 x 2.0 MB]
TOTAL: [21.7 MB]

Starting iterations...

Iterative method: 92 iterations in 0.82 seconds (average 0.003957, setup 0.45)

Value in the initial state: 135.97371372671967

Time for model checking: 0.914 seconds.

Result: 135.97371372671967 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"time"}min=? [ F "deadlock" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=2,Objy1=1,Objx2=5,Objy2=0

Prob0A: 4 iterations in 0.03 seconds (average 0.007000, setup 0.00)

Prob1E: 5 iterations in 0.06 seconds (average 0.011200, setup 0.00)

Warning: PRISM hasn't checked for zero-reward loops.
Your minimum rewards may be too low...

goal = 1, inf = 0, maybe = 259505

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=259506, nc=340818, nnz=675833, k=4] [10.0 MB]
Building sparse matrix (transition rewards)... [n=259506, nc=340818, nnz=130148, k=4] [3.8 MB]
Creating vector for state rewards... [2.0 MB]
Creating vector for inf... [2.0 MB]
Allocating iteration vectors... [2 x 2.0 MB]
TOTAL: [21.7 MB]

Starting iterations...

Iterative method: 94 iterations in 0.76 seconds (average 0.003872, setup 0.40)

Value in the initial state: 77.90444152485296

Time for model checking: 0.872 seconds.

Result: 77.90444152485296 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"success"}max=? [ F "deadlock" ]/Pmin=? [ F "MissionSuccessful" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=2,Objy1=1,Objx2=5,Objy2=0

Prob0E: 5 iterations in 0.06 seconds (average 0.011200, setup 0.00)

Prob1A: 1 iterations in 0.00 seconds (average 0.004000, setup 0.00)

goal = 1, inf = 0, maybe = 259505

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=259506, nc=340818, nnz=675833, k=4] [10.0 MB]
Building sparse matrix (transition rewards)... [n=259506, nc=340818, nnz=2128, k=4] [2.3 MB]
Creating vector for state rewards... [2.0 MB]
Creating vector for inf... [2.0 MB]
Allocating iteration vectors... [2 x 2.0 MB]
TOTAL: [20.3 MB]

Starting iterations...

Iterative method: 91 iterations in 0.80 seconds (average 0.004835, setup 0.36)

Prob0E: 48 iterations in 0.38 seconds (average 0.007917, setup 0.00)

Prob1A: 3 iterations in 0.06 seconds (average 0.021333, setup 0.00)

yes = 1080, no = 226717, maybe = 31709

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=259506, nc=56105, nnz=159156, k=2] [2.1 MB]
Creating vector for yes... [2.0 MB]
Allocating iteration vectors... [2 x 2.0 MB]
TOTAL: [8.1 MB]

Starting iterations...

Iterative method: 83 iterations in 0.31 seconds (average 0.001639, setup 0.18)

Value in the initial state: 145.00086325911644

Time for model checking: 1.67 seconds.

Result: 145.00086325911644 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"success"}min=? [ F "deadlock" ]/Pmax=? [ F "MissionSuccessful" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=2,Objy1=1,Objx2=5,Objy2=0

Prob0A: 4 iterations in 0.09 seconds (average 0.022000, setup 0.00)

Prob1E: 5 iterations in 0.04 seconds (average 0.008800, setup 0.00)

Warning: PRISM hasn't checked for zero-reward loops.
Your minimum rewards may be too low...

goal = 1, inf = 0, maybe = 259505

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=259506, nc=340818, nnz=675833, k=4] [10.0 MB]
Building sparse matrix (transition rewards)... [n=259506, nc=340818, nnz=2128, k=4] [2.3 MB]
Creating vector for state rewards... [2.0 MB]
Creating vector for inf... [2.0 MB]
Allocating iteration vectors... [2 x 2.0 MB]
TOTAL: [20.3 MB]

Starting iterations...

Iterative method: 89 iterations in 0.86 seconds (average 0.004090, setup 0.49)

Prob0A: 46 iterations in 0.31 seconds (average 0.006783, setup 0.00)

Prob1E: 4 iterations in 0.04 seconds (average 0.011000, setup 0.00)

yes = 1080, no = 220868, maybe = 37558

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=259506, nc=66719, nnz=190081, k=2] [2.5 MB]
Creating vector for yes... [2.0 MB]
Allocating iteration vectors... [2 x 2.0 MB]
TOTAL: [8.4 MB]

Starting iterations...

Iterative method: 88 iterations in 0.32 seconds (average 0.001636, setup 0.18)

Value in the initial state: 73.75820839013211

Time for model checking: 1.721 seconds.

Result: 73.75820839013211 (value in the initial state)

---------------------------------------------------------------------

Note: There were 16 warnings during computation.

