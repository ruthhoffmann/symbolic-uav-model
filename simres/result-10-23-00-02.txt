PRISM
=====

Version: 4.3
Date: Wed Aug 03 16:06:23 BST 2016
Hostname: eglinton
Memory limits: cudd=1g, java(heap)=981.5m
Command line: prism uav_sim_mdp.prism -s uav_mdp.props -const 'Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=0,Objy1=0,Objx2=0,Objy2=2,'

Parsing model file "uav_sim_mdp.prism"...

Parsing properties file "uav_mdp.props"...

18 properties:
(1) Pmax=? [ F "MissionSuccessful" ]
(2) Pmin=? [ F "MissionSuccessful" ]
(3) Pmax=? [ F "MissionTimeUp" ]
(4) Pmin=? [ F "MissionTimeUp" ]
(5) Pmax=? [ F "Crash" ]
(6) Pmin=? [ F "Crash" ]
(7) Pmax=? [ F "AllAreaSearched" ]
(8) Pmin=? [ F "AllAreaSearched" ]
(9) Pmax=? [ F "MissionTimeUp"|"AllAreaSearched"|"Crash" ]
(10) Pmin=? [ F "MissionTimeUp"|"AllAreaSearched"|"Crash" ]
(11) R{"drop"}max=? [ F "deadlock" ]
(12) R{"drop"}min=? [ F "deadlock" ]
(13) R{"charge"}max=? [ F "deadlock" ]
(14) R{"charge"}min=? [ F "deadlock" ]
(15) R{"time"}max=? [ F "deadlock" ]
(16) R{"time"}min=? [ F "deadlock" ]
(17) R{"success"}max=? [ F "deadlock" ]/Pmin=? [ F "MissionSuccessful" ]
(18) R{"success"}min=? [ F "deadlock" ]/Pmax=? [ F "MissionSuccessful" ]

Type:        MDP
Modules:     Obj Move Robot T 
Variables:   NoOfObjs posx posy s1 retx rety b t c 

---------------------------------------------------------------------

Model checking: Pmax=? [ F "MissionSuccessful" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=0,Objy1=0,Objx2=0,Objy2=2

Building model...
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=0,Objy1=0,Objx2=0,Objy2=2

Warning: Guard for command 7 of module "Move" is never satisfied.

Warning: Guard for command 8 of module "Move" is never satisfied.

Warning: Guard for command 9 of module "Move" is never satisfied.

Warning: Guard for command 7 of module "Robot" is never satisfied.

Warning: Update 2 of command 3 of module "Robot" doesn't do anything ("(s1'=13)", line 210, column 44)

Warning: Update 2 of command 27 of module "T" doesn't do anything ("(c'=1)", line 323, column 108)

Warning: Guard for command 28 of module "T" is never satisfied.

Warning: Update 2 of command 9 of module "T" doesn't do anything ("(c'=1)", line 300, column 112)

Warning: Guard for command 10 of module "T" is never satisfied.

Warning: Update 2 of command 51 of module "T" doesn't do anything ("(c'=1)", line 354, column 113)

Warning: Guard for command 52 of module "T" is never satisfied.

Computing reachable states...

Reachability (BFS): 90 iterations in 1.82 seconds (average 0.020267, setup 0.00)

Time for model construction: 5.226 seconds.

Warning: Deadlocks detected and fixed in 1 states

Type:        MDP
States:      300835 (1 initial)
Transitions: 768301
Choices:     390843

Transition matrix: 224186 nodes (91 terminal), 768301 minterms, vars: 34r/34c/18nd

Prob0A: 54 iterations in 0.59 seconds (average 0.010889, setup 0.00)

Prob1E: 4 iterations in 0.09 seconds (average 0.022000, setup 0.00)

yes = 1190, no = 255677, maybe = 43968

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=300835, nc=75466, nnz=211661, k=2] [2.8 MB]
Creating vector for yes... [2.3 MB]
Allocating iteration vectors... [2 x 2.3 MB]
TOTAL: [9.7 MB]

Starting iterations...

Iterative method: 91 iterations in 0.50 seconds (average 0.002198, setup 0.30)

Value in the initial state: 0.9834737231795709

Time for model checking: 1.874 seconds.

Result: 0.9834737231795709 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmin=? [ F "MissionSuccessful" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=0,Objy1=0,Objx2=0,Objy2=2

Prob0E: 56 iterations in 0.56 seconds (average 0.010071, setup 0.00)

Prob1A: 4 iterations in 0.06 seconds (average 0.014000, setup 0.00)

yes = 1190, no = 262919, maybe = 36726

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=300835, nc=62867, nnz=175366, k=2] [2.4 MB]
Creating vector for yes... [2.3 MB]
Allocating iteration vectors... [2 x 2.3 MB]
TOTAL: [9.2 MB]

Starting iterations...

Iterative method: 87 iterations in 0.38 seconds (average 0.001977, setup 0.20)

Value in the initial state: 0.8273530541264281

Time for model checking: 1.168 seconds.

Result: 0.8273530541264281 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmax=? [ F "MissionTimeUp" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=0,Objy1=0,Objx2=0,Objy2=2

Prob0A: 66 iterations in 0.76 seconds (average 0.011455, setup 0.00)

Prob1E: 4 iterations in 0.06 seconds (average 0.014000, setup 0.00)

yes = 520, no = 183487, maybe = 116828

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=300835, nc=193953, nnz=522408, k=4] [6.5 MB]
Creating vector for yes... [2.3 MB]
Allocating iteration vectors... [2 x 2.3 MB]
TOTAL: [13.3 MB]

Starting iterations...

Iterative method: 96 iterations in 0.62 seconds (average 0.002625, setup 0.37)

Value in the initial state: 1.397101512454973E-4

Time for model checking: 1.902 seconds.

Result: 1.397101512454973E-4 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmin=? [ F "MissionTimeUp" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=0,Objy1=0,Objx2=0,Objy2=2

Prob0E: 80 iterations in 1.14 seconds (average 0.014200, setup 0.00)

Prob1A: 4 iterations in 0.06 seconds (average 0.016000, setup 0.00)

yes = 520, no = 183913, maybe = 116402

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=300835, nc=193198, nnz=520790, k=4] [6.4 MB]
Creating vector for yes... [2.3 MB]
Allocating iteration vectors... [2 x 2.3 MB]
TOTAL: [13.3 MB]

Starting iterations...

Iterative method: 113 iterations in 0.66 seconds (average 0.002655, setup 0.36)

Value in the initial state: 1.3724492656975148E-7

Time for model checking: 2.236 seconds.

Result: 1.3724492656975148E-7 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmax=? [ F "Crash" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=0,Objy1=0,Objx2=0,Objy2=2

Prob0A: 4 iterations in 0.12 seconds (average 0.029000, setup 0.00)

Prob1E: 249 iterations in 4.55 seconds (average 0.018281, setup 0.00)

yes = 147570, no = 3331, maybe = 149934

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=300835, nc=239942, nnz=617400, k=4] [7.6 MB]
Creating vector for yes... [2.3 MB]
Allocating iteration vectors... [2 x 2.3 MB]
TOTAL: [14.5 MB]

Starting iterations...

Iterative method: 90 iterations in 0.68 seconds (average 0.002800, setup 0.42)

Value in the initial state: 0.025503334241435426

Time for model checking: 7.186 seconds.

Result: 0.025503334241435426 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmin=? [ F "Crash" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=0,Objy1=0,Objx2=0,Objy2=2

Prob0E: 4 iterations in 0.13 seconds (average 0.032000, setup 0.00)

Prob1A: 52 iterations in 0.99 seconds (average 0.019000, setup 0.00)

yes = 147570, no = 3331, maybe = 149934

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=300835, nc=239942, nnz=617400, k=4] [7.6 MB]
Creating vector for yes... [2.3 MB]
Allocating iteration vectors... [2 x 2.3 MB]
TOTAL: [14.5 MB]

Starting iterations...

Iterative method: 85 iterations in 0.67 seconds (average 0.002965, setup 0.42)

Value in the initial state: 0.014476754061812097

Time for model checking: 2.467 seconds.

Result: 0.014476754061812097 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmax=? [ F "AllAreaSearched" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=0,Objy1=0,Objx2=0,Objy2=2

Prob0A: 64 iterations in 0.53 seconds (average 0.008250, setup 0.00)

Prob1E: 4 iterations in 0.06 seconds (average 0.016000, setup 0.00)

yes = 1620, no = 234722, maybe = 64493

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=300835, nc=97252, nnz=251008, k=2] [3.3 MB]
Creating vector for yes... [2.3 MB]
Allocating iteration vectors... [2 x 2.3 MB]
TOTAL: [10.1 MB]

Starting iterations...

Iterative method: 88 iterations in 0.45 seconds (average 0.002500, setup 0.23)

Value in the initial state: 0.14894776396655637

Time for model checking: 1.436 seconds.

Result: 0.14894776396655637 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmin=? [ F "AllAreaSearched" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=0,Objy1=0,Objx2=0,Objy2=2

Prob0E: 67 iterations in 0.56 seconds (average 0.008358, setup 0.00)

Prob1A: 4 iterations in 0.06 seconds (average 0.014000, setup 0.00)

yes = 1620, no = 243040, maybe = 56175

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=300835, nc=83974, nnz=215019, k=2] [2.8 MB]
Creating vector for yes... [2.3 MB]
Allocating iteration vectors... [2 x 2.3 MB]
TOTAL: [9.7 MB]

Starting iterations...

Iterative method: 96 iterations in 0.39 seconds (average 0.002417, setup 0.16)

Value in the initial state: 0.0018302626037802671

Time for model checking: 1.229 seconds.

Result: 0.0018302626037802671 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmax=? [ F "MissionTimeUp"|"AllAreaSearched"|"Crash" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=0,Objy1=0,Objx2=0,Objy2=2

Prob0A: 4 iterations in 0.12 seconds (average 0.030000, setup 0.00)

Prob1E: 270 iterations in 4.71 seconds (average 0.017452, setup 0.00)

yes = 262918, no = 1191, maybe = 36726

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=300835, nc=62867, nnz=175366, k=2] [2.4 MB]
Creating vector for yes... [2.3 MB]
Allocating iteration vectors... [2 x 2.3 MB]
TOTAL: [9.2 MB]

Starting iterations...

Iterative method: 80 iterations in 0.42 seconds (average 0.002150, setup 0.25)

Value in the initial state: 0.17264667254527669

Time for model checking: 6.484 seconds.

Result: 0.17264667254527669 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmin=? [ F "MissionTimeUp"|"AllAreaSearched"|"Crash" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=0,Objy1=0,Objx2=0,Objy2=2

Prob0E: 4 iterations in 0.11 seconds (average 0.028000, setup 0.00)

Prob1A: 54 iterations in 0.68 seconds (average 0.012519, setup 0.00)

yes = 255676, no = 1191, maybe = 43968

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=300835, nc=75466, nnz=211661, k=2] [2.8 MB]
Creating vector for yes... [2.3 MB]
Allocating iteration vectors... [2 x 2.3 MB]
TOTAL: [9.7 MB]

Starting iterations...

Iterative method: 91 iterations in 0.46 seconds (average 0.002198, setup 0.26)

Value in the initial state: 0.016524685414121

Time for model checking: 1.468 seconds.

Result: 0.016524685414121 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"drop"}max=? [ F "deadlock" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=0,Objy1=0,Objx2=0,Objy2=2

Prob0E: 5 iterations in 0.11 seconds (average 0.022400, setup 0.00)

Prob1A: 1 iterations in 0.01 seconds (average 0.008000, setup 0.00)

goal = 1, inf = 0, maybe = 300834

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=300835, nc=390842, nnz=768300, k=4] [11.4 MB]
Building sparse matrix (transition rewards)... [n=300835, nc=390842, nnz=0, k=4] [2.6 MB]
Creating vector for state rewards... [2.3 MB]
Creating vector for inf... [2.3 MB]
Allocating iteration vectors... [2 x 2.3 MB]
TOTAL: [23.3 MB]

Starting iterations...

Iterative method: 87 iterations in 0.92 seconds (average 0.004276, setup 0.54)

Value in the initial state: 0.205215674645935

Time for model checking: 1.163 seconds.

Result: 0.205215674645935 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"drop"}min=? [ F "deadlock" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=0,Objy1=0,Objx2=0,Objy2=2

Prob0A: 5 iterations in 0.07 seconds (average 0.013600, setup 0.00)

Prob1E: 6 iterations in 0.08 seconds (average 0.013333, setup 0.00)

Warning: PRISM hasn't checked for zero-reward loops.
Your minimum rewards may be too low...

goal = 1, inf = 0, maybe = 300834

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=300835, nc=390842, nnz=768300, k=4] [11.4 MB]
Building sparse matrix (transition rewards)... [n=300835, nc=390842, nnz=0, k=4] [2.6 MB]
Creating vector for state rewards... [2.3 MB]
Creating vector for inf... [2.3 MB]
Allocating iteration vectors... [2 x 2.3 MB]
TOTAL: [23.3 MB]

Starting iterations...

Iterative method: 79 iterations in 0.75 seconds (average 0.004405, setup 0.40)

Value in the initial state: 0.18766167061441716

Time for model checking: 1.017 seconds.

Result: 0.18766167061441716 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"charge"}max=? [ F "deadlock" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=0,Objy1=0,Objx2=0,Objy2=2

Prob0E: 5 iterations in 0.04 seconds (average 0.008800, setup 0.00)

Prob1A: 1 iterations in 0.00 seconds (average 0.004000, setup 0.00)

goal = 1, inf = 0, maybe = 300834

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=300835, nc=390842, nnz=768300, k=4] [11.4 MB]
Building sparse matrix (transition rewards)... [n=300835, nc=390842, nnz=7841, k=4] [2.7 MB]
Creating vector for state rewards... [2.3 MB]
Creating vector for inf... [2.3 MB]
Allocating iteration vectors... [2 x 2.3 MB]
TOTAL: [23.3 MB]

Starting iterations...

Iterative method: 88 iterations in 0.79 seconds (average 0.004409, setup 0.40)

Value in the initial state: 0.19454098181340176

Time for model checking: 1.051 seconds.

Result: 0.19454098181340176 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"charge"}min=? [ F "deadlock" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=0,Objy1=0,Objx2=0,Objy2=2

Prob0A: 5 iterations in 0.04 seconds (average 0.008800, setup 0.00)

Prob1E: 6 iterations in 0.08 seconds (average 0.012667, setup 0.00)

Warning: PRISM hasn't checked for zero-reward loops.
Your minimum rewards may be too low...

goal = 1, inf = 0, maybe = 300834

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=300835, nc=390842, nnz=768300, k=4] [11.4 MB]
Building sparse matrix (transition rewards)... [n=300835, nc=390842, nnz=7841, k=4] [2.7 MB]
Creating vector for state rewards... [2.3 MB]
Creating vector for inf... [2.3 MB]
Allocating iteration vectors... [2 x 2.3 MB]
TOTAL: [23.3 MB]

Starting iterations...

Iterative method: 96 iterations in 0.82 seconds (average 0.004417, setup 0.40)

Value in the initial state: 0.015817758642236415

Time for model checking: 1.354 seconds.

Result: 0.015817758642236415 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"time"}max=? [ F "deadlock" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=0,Objy1=0,Objx2=0,Objy2=2

Prob0E: 5 iterations in 0.04 seconds (average 0.008000, setup 0.00)

Prob1A: 1 iterations in 0.01 seconds (average 0.008000, setup 0.00)

goal = 1, inf = 0, maybe = 300834

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=300835, nc=390842, nnz=768300, k=4] [11.4 MB]
Building sparse matrix (transition rewards)... [n=300835, nc=390842, nnz=150899, k=4] [4.4 MB]
Creating vector for state rewards... [2.3 MB]
Creating vector for inf... [2.3 MB]
Allocating iteration vectors... [2 x 2.3 MB]
TOTAL: [25.0 MB]

Starting iterations...

Iterative method: 93 iterations in 0.88 seconds (average 0.004645, setup 0.45)

Value in the initial state: 140.92178958275966

Time for model checking: 1.127 seconds.

Result: 140.92178958275966 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"time"}min=? [ F "deadlock" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=0,Objy1=0,Objx2=0,Objy2=2

Prob0A: 5 iterations in 0.05 seconds (average 0.009600, setup 0.00)

Prob1E: 6 iterations in 0.08 seconds (average 0.012667, setup 0.00)

Warning: PRISM hasn't checked for zero-reward loops.
Your minimum rewards may be too low...

goal = 1, inf = 0, maybe = 300834

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=300835, nc=390842, nnz=768300, k=4] [11.4 MB]
Building sparse matrix (transition rewards)... [n=300835, nc=390842, nnz=150899, k=4] [4.4 MB]
Creating vector for state rewards... [2.3 MB]
Creating vector for inf... [2.3 MB]
Allocating iteration vectors... [2 x 2.3 MB]
TOTAL: [25.0 MB]

Starting iterations...

Iterative method: 98 iterations in 0.89 seconds (average 0.004653, setup 0.44)

Value in the initial state: 80.33495725283679

Time for model checking: 1.145 seconds.

Result: 80.33495725283679 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"success"}max=? [ F "deadlock" ]/Pmin=? [ F "MissionSuccessful" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=0,Objy1=0,Objx2=0,Objy2=2

Prob0E: 5 iterations in 0.05 seconds (average 0.009600, setup 0.00)

Prob1A: 1 iterations in 0.01 seconds (average 0.008000, setup 0.00)

goal = 1, inf = 0, maybe = 300834

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=300835, nc=390842, nnz=768300, k=4] [11.4 MB]
Building sparse matrix (transition rewards)... [n=300835, nc=390842, nnz=2346, k=4] [2.7 MB]
Creating vector for state rewards... [2.3 MB]
Creating vector for inf... [2.3 MB]
Allocating iteration vectors... [2 x 2.3 MB]
TOTAL: [23.3 MB]

Starting iterations...

Iterative method: 95 iterations in 0.81 seconds (average 0.004337, setup 0.40)

Prob0E: 56 iterations in 0.64 seconds (average 0.011357, setup 0.00)

Prob1A: 4 iterations in 0.06 seconds (average 0.014000, setup 0.00)

yes = 1190, no = 262919, maybe = 36726

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=300835, nc=62867, nnz=175366, k=2] [2.4 MB]
Creating vector for yes... [2.3 MB]
Allocating iteration vectors... [2 x 2.3 MB]
TOTAL: [9.2 MB]

Starting iterations...

Iterative method: 87 iterations in 0.39 seconds (average 0.002023, setup 0.22)

Value in the initial state: 167.26655063354391

Time for model checking: 2.529 seconds.

Result: 167.26655063354391 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"success"}min=? [ F "deadlock" ]/Pmax=? [ F "MissionSuccessful" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=0,Objy1=0,Objx2=0,Objy2=2

Prob0A: 5 iterations in 0.14 seconds (average 0.028000, setup 0.00)

Prob1E: 6 iterations in 0.09 seconds (average 0.014667, setup 0.00)

Warning: PRISM hasn't checked for zero-reward loops.
Your minimum rewards may be too low...

goal = 1, inf = 0, maybe = 300834

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=300835, nc=390842, nnz=768300, k=4] [11.4 MB]
Building sparse matrix (transition rewards)... [n=300835, nc=390842, nnz=2346, k=4] [2.7 MB]
Creating vector for state rewards... [2.3 MB]
Creating vector for inf... [2.3 MB]
Allocating iteration vectors... [2 x 2.3 MB]
TOTAL: [23.3 MB]

Starting iterations...

Iterative method: 92 iterations in 0.92 seconds (average 0.004435, setup 0.51)

Prob0A: 54 iterations in 0.46 seconds (average 0.008519, setup 0.00)

Prob1E: 4 iterations in 0.06 seconds (average 0.014000, setup 0.00)

yes = 1190, no = 255677, maybe = 43968

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=300835, nc=75466, nnz=211661, k=2] [2.8 MB]
Creating vector for yes... [2.3 MB]
Allocating iteration vectors... [2 x 2.3 MB]
TOTAL: [9.7 MB]

Starting iterations...

Iterative method: 91 iterations in 0.43 seconds (average 0.002110, setup 0.24)

Value in the initial state: 75.34260120457868

Time for model checking: 2.165 seconds.

Result: 75.34260120457868 (value in the initial state)

---------------------------------------------------------------------

Note: There were 16 warnings during computation.

