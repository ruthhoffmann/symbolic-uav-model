PRISM
=====

Version: 4.3
Date: Wed Aug 03 15:54:56 BST 2016
Hostname: eglinton
Memory limits: cudd=1g, java(heap)=981.5m
Command line: prism uav_sim_mdp.prism -s uav_mdp.props -const 'Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=1,Objy1=1,Objx2=2,Objy2=3,'

Parsing model file "uav_sim_mdp.prism"...

Parsing properties file "uav_mdp.props"...

18 properties:
(1) Pmax=? [ F "MissionSuccessful" ]
(2) Pmin=? [ F "MissionSuccessful" ]
(3) Pmax=? [ F "MissionTimeUp" ]
(4) Pmin=? [ F "MissionTimeUp" ]
(5) Pmax=? [ F "Crash" ]
(6) Pmin=? [ F "Crash" ]
(7) Pmax=? [ F "AllAreaSearched" ]
(8) Pmin=? [ F "AllAreaSearched" ]
(9) Pmax=? [ F "MissionTimeUp"|"AllAreaSearched"|"Crash" ]
(10) Pmin=? [ F "MissionTimeUp"|"AllAreaSearched"|"Crash" ]
(11) R{"drop"}max=? [ F "deadlock" ]
(12) R{"drop"}min=? [ F "deadlock" ]
(13) R{"charge"}max=? [ F "deadlock" ]
(14) R{"charge"}min=? [ F "deadlock" ]
(15) R{"time"}max=? [ F "deadlock" ]
(16) R{"time"}min=? [ F "deadlock" ]
(17) R{"success"}max=? [ F "deadlock" ]/Pmin=? [ F "MissionSuccessful" ]
(18) R{"success"}min=? [ F "deadlock" ]/Pmax=? [ F "MissionSuccessful" ]

Type:        MDP
Modules:     Obj Move Robot T 
Variables:   NoOfObjs posx posy s1 retx rety b t c 

---------------------------------------------------------------------

Model checking: Pmax=? [ F "MissionSuccessful" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=1,Objy1=1,Objx2=2,Objy2=3

Building model...
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=1,Objy1=1,Objx2=2,Objy2=3

Warning: Guard for command 7 of module "Move" is never satisfied.

Warning: Guard for command 8 of module "Move" is never satisfied.

Warning: Guard for command 9 of module "Move" is never satisfied.

Warning: Guard for command 7 of module "Robot" is never satisfied.

Warning: Update 2 of command 3 of module "Robot" doesn't do anything ("(s1'=13)", line 210, column 44)

Warning: Update 2 of command 27 of module "T" doesn't do anything ("(c'=1)", line 323, column 108)

Warning: Guard for command 28 of module "T" is never satisfied.

Warning: Update 2 of command 9 of module "T" doesn't do anything ("(c'=1)", line 300, column 112)

Warning: Guard for command 10 of module "T" is never satisfied.

Warning: Update 2 of command 51 of module "T" doesn't do anything ("(c'=1)", line 354, column 113)

Warning: Guard for command 52 of module "T" is never satisfied.

Computing reachable states...

Reachability (BFS): 89 iterations in 0.80 seconds (average 0.008989, setup 0.00)

Time for model construction: 3.324 seconds.

Warning: Deadlocks detected and fixed in 1 states

Type:        MDP
States:      130119 (1 initial)
Transitions: 307888
Choices:     164045

Transition matrix: 172217 nodes (91 terminal), 307888 minterms, vars: 34r/34c/18nd

Prob0A: 61 iterations in 0.25 seconds (average 0.004131, setup 0.00)

Prob1E: 4 iterations in 0.04 seconds (average 0.009000, setup 0.00)

yes = 222, no = 124023, maybe = 5874

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=130119, nc=10758, nnz=31015, k=2] [501.0 KB]
Creating vector for yes... [1016.6 KB]
Allocating iteration vectors... [2 x 1016.6 KB]
TOTAL: [3.5 MB]

Starting iterations...

Iterative method: 92 iterations in 0.16 seconds (average 0.000696, setup 0.10)

Value in the initial state: 6.848449803166106E-4

Time for model checking: 0.481 seconds.

Result: 6.848449803166106E-4 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmin=? [ F "MissionSuccessful" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=1,Objy1=1,Objx2=2,Objy2=3

Prob0E: 21 iterations in 0.07 seconds (average 0.003238, setup 0.00)

Prob1A: 3 iterations in 0.01 seconds (average 0.004000, setup 0.00)

yes = 222, no = 125198, maybe = 4699

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=130119, nc=8611, nnz=24583, k=2] [423.6 KB]
Creating vector for yes... [1016.6 KB]
Allocating iteration vectors... [2 x 1016.6 KB]
TOTAL: [3.4 MB]

Starting iterations...

Iterative method: 42 iterations in 0.05 seconds (average 0.000571, setup 0.03)

Value in the initial state: 0.0

Time for model checking: 0.147 seconds.

Result: 0.0 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmax=? [ F "MissionTimeUp" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=1,Objy1=1,Objx2=2,Objy2=3

Prob0A: 74 iterations in 0.52 seconds (average 0.006973, setup 0.00)

Prob1E: 4 iterations in 0.04 seconds (average 0.011000, setup 0.00)

yes = 288, no = 95358, maybe = 34473

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=130119, nc=60143, nnz=164196, k=4] [2.1 MB]
Creating vector for yes... [1016.6 KB]
Allocating iteration vectors... [2 x 1016.6 KB]
TOTAL: [5.0 MB]

Starting iterations...

Iterative method: 98 iterations in 0.29 seconds (average 0.000980, setup 0.19)

Value in the initial state: 2.2464768922859935E-6

Time for model checking: 1.0 seconds.

Result: 2.2464768922859935E-6 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmin=? [ F "MissionTimeUp" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=1,Objy1=1,Objx2=2,Objy2=3

Prob0E: 88 iterations in 0.70 seconds (average 0.008000, setup 0.00)

Prob1A: 3 iterations in 0.03 seconds (average 0.010667, setup 0.00)

yes = 288, no = 95625, maybe = 34206

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=130119, nc=59669, nnz=163125, k=4] [2.0 MB]
Creating vector for yes... [1016.6 KB]
Allocating iteration vectors... [2 x 1016.6 KB]
TOTAL: [5.0 MB]

Starting iterations...

Iterative method: 116 iterations in 0.28 seconds (average 0.000966, setup 0.16)

Value in the initial state: 9.90738316391208E-10

Time for model checking: 1.094 seconds.

Result: 9.90738316391208E-10 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmax=? [ F "Crash" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=1,Objy1=1,Objx2=2,Objy2=3

Prob0A: 3 iterations in 0.06 seconds (average 0.020000, setup 0.00)

Prob1E: 168 iterations in 1.81 seconds (average 0.010786, setup 0.00)

yes = 63972, no = 1973, maybe = 64174

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=130119, nc=98100, nnz=241943, k=4] [3.0 MB]
Creating vector for yes... [1016.6 KB]
Allocating iteration vectors... [2 x 1016.6 KB]
TOTAL: [6.0 MB]

Starting iterations...

Iterative method: 88 iterations in 0.37 seconds (average 0.001136, setup 0.27)

Value in the initial state: 0.013539937489765775

Time for model checking: 2.315 seconds.

Result: 0.013539937489765775 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmin=? [ F "Crash" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=1,Objy1=1,Objx2=2,Objy2=3

Prob0E: 4 iterations in 0.06 seconds (average 0.015000, setup 0.00)

Prob1A: 45 iterations in 0.41 seconds (average 0.009156, setup 0.00)

yes = 63972, no = 1973, maybe = 64174

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=130119, nc=98100, nnz=241943, k=4] [3.0 MB]
Creating vector for yes... [1016.6 KB]
Allocating iteration vectors... [2 x 1016.6 KB]
TOTAL: [6.0 MB]

Starting iterations...

Iterative method: 75 iterations in 0.32 seconds (average 0.001173, setup 0.23)

Value in the initial state: 0.011110030766017048

Time for model checking: 0.87 seconds.

Result: 0.011110030766017048 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmax=? [ F "AllAreaSearched" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=1,Objy1=1,Objx2=2,Objy2=3

Prob0A: 45 iterations in 0.30 seconds (average 0.006667, setup 0.00)

Prob1E: 4 iterations in 0.04 seconds (average 0.011000, setup 0.00)

yes = 1462, no = 85549, maybe = 43108

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=130119, nc=62271, nnz=153509, k=2] [1.9 MB]
Creating vector for yes... [1016.6 KB]
Allocating iteration vectors... [2 x 1016.6 KB]
TOTAL: [4.9 MB]

Starting iterations...

Iterative method: 83 iterations in 0.25 seconds (average 0.000964, setup 0.17)

Value in the initial state: 0.9888879210933086

Time for model checking: 0.76 seconds.

Result: 0.9888879210933086 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmin=? [ F "AllAreaSearched" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=1,Objy1=1,Objx2=2,Objy2=3

Prob0E: 45 iterations in 0.30 seconds (average 0.006667, setup 0.00)

Prob1A: 3 iterations in 0.03 seconds (average 0.009333, setup 0.00)

yes = 1462, no = 87637, maybe = 41020

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=130119, nc=58335, nnz=141968, k=2] [1.8 MB]
Creating vector for yes... [1016.6 KB]
Allocating iteration vectors... [2 x 1016.6 KB]
TOTAL: [4.8 MB]

Starting iterations...

Iterative method: 89 iterations in 0.18 seconds (average 0.000944, setup 0.10)

Value in the initial state: 0.9857824155007269

Time for model checking: 0.545 seconds.

Result: 0.9857824155007269 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmax=? [ F "MissionTimeUp"|"AllAreaSearched"|"Crash" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=1,Objy1=1,Objx2=2,Objy2=3

Prob0A: 3 iterations in 0.06 seconds (average 0.021333, setup 0.00)

Prob1E: 80 iterations in 0.79 seconds (average 0.009900, setup 0.00)

yes = 125197, no = 223, maybe = 4699

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=130119, nc=8611, nnz=24583, k=2] [423.6 KB]
Creating vector for yes... [1016.6 KB]
Allocating iteration vectors... [2 x 1016.6 KB]
TOTAL: [3.4 MB]

Starting iterations...

Iterative method: 39 iterations in 0.08 seconds (average 0.000718, setup 0.05)

Value in the initial state: 1.0

Time for model checking: 1.12 seconds.

Result: 1.0 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmin=? [ F "MissionTimeUp"|"AllAreaSearched"|"Crash" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=1,Objy1=1,Objx2=2,Objy2=3

Prob0E: 4 iterations in 0.04 seconds (average 0.009000, setup 0.00)

Prob1A: 61 iterations in 0.39 seconds (average 0.006361, setup 0.00)

yes = 124022, no = 223, maybe = 5874

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=130119, nc=10758, nnz=31015, k=2] [501.0 KB]
Creating vector for yes... [1016.6 KB]
Allocating iteration vectors... [2 x 1016.6 KB]
TOTAL: [3.5 MB]

Starting iterations...

Iterative method: 52 iterations in 0.10 seconds (average 0.000692, setup 0.06)

Value in the initial state: 0.9993114653445655

Time for model checking: 0.585 seconds.

Result: 0.9993114653445655 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"drop"}max=? [ F "deadlock" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=1,Objy1=1,Objx2=2,Objy2=3

Prob0E: 5 iterations in 0.04 seconds (average 0.008800, setup 0.00)

Prob1A: 1 iterations in 0.01 seconds (average 0.008000, setup 0.00)

goal = 1, inf = 0, maybe = 130118

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=130119, nc=164044, nnz=307887, k=4] [4.6 MB]
Building sparse matrix (transition rewards)... [n=130119, nc=164044, nnz=0, k=4] [1.1 MB]
Creating vector for state rewards... [1016.6 KB]
Creating vector for inf... [1016.6 KB]
Allocating iteration vectors... [2 x 1016.6 KB]
TOTAL: [9.7 MB]

Starting iterations...

Iterative method: 69 iterations in 0.40 seconds (average 0.001797, setup 0.28)

Value in the initial state: 0.08218710926696665

Time for model checking: 0.489 seconds.

Result: 0.08218710926696665 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"drop"}min=? [ F "deadlock" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=1,Objy1=1,Objx2=2,Objy2=3

Prob0A: 4 iterations in 0.03 seconds (average 0.007000, setup 0.00)

Prob1E: 5 iterations in 0.04 seconds (average 0.008800, setup 0.00)

Warning: PRISM hasn't checked for zero-reward loops.
Your minimum rewards may be too low...

goal = 1, inf = 0, maybe = 130118

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=130119, nc=164044, nnz=307887, k=4] [4.6 MB]
Building sparse matrix (transition rewards)... [n=130119, nc=164044, nnz=0, k=4] [1.1 MB]
Creating vector for state rewards... [1016.6 KB]
Creating vector for inf... [1016.6 KB]
Allocating iteration vectors... [2 x 1016.6 KB]
TOTAL: [9.7 MB]

Starting iterations...

Iterative method: 62 iterations in 0.29 seconds (average 0.001806, setup 0.18)

Value in the initial state: 0.08206694059607286

Time for model checking: 0.401 seconds.

Result: 0.08206694059607286 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"charge"}max=? [ F "deadlock" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=1,Objy1=1,Objx2=2,Objy2=3

Prob0E: 5 iterations in 0.03 seconds (average 0.005600, setup 0.00)

Prob1A: 1 iterations in 0.00 seconds (average 0.004000, setup 0.00)

goal = 1, inf = 0, maybe = 130118

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=130119, nc=164044, nnz=307887, k=4] [4.6 MB]
Building sparse matrix (transition rewards)... [n=130119, nc=164044, nnz=2607, k=4] [1.2 MB]
Creating vector for state rewards... [1016.6 KB]
Creating vector for inf... [1016.6 KB]
Allocating iteration vectors... [2 x 1016.6 KB]
TOTAL: [9.8 MB]

Starting iterations...

Iterative method: 83 iterations in 0.33 seconds (average 0.001783, setup 0.18)

Value in the initial state: 0.0015228262133692894

Time for model checking: 0.442 seconds.

Result: 0.0015228262133692894 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"charge"}min=? [ F "deadlock" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=1,Objy1=1,Objx2=2,Objy2=3

Prob0A: 4 iterations in 0.02 seconds (average 0.005000, setup 0.00)

Prob1E: 5 iterations in 0.04 seconds (average 0.008000, setup 0.00)

Warning: PRISM hasn't checked for zero-reward loops.
Your minimum rewards may be too low...

goal = 1, inf = 0, maybe = 130118

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=130119, nc=164044, nnz=307887, k=4] [4.6 MB]
Building sparse matrix (transition rewards)... [n=130119, nc=164044, nnz=2607, k=4] [1.2 MB]
Creating vector for state rewards... [1016.6 KB]
Creating vector for inf... [1016.6 KB]
Allocating iteration vectors... [2 x 1016.6 KB]
TOTAL: [9.8 MB]

Starting iterations...

Iterative method: 94 iterations in 0.35 seconds (average 0.001830, setup 0.18)

Value in the initial state: 5.870251718186365E-5

Time for model checking: 0.426 seconds.

Result: 5.870251718186365E-5 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"time"}max=? [ F "deadlock" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=1,Objy1=1,Objx2=2,Objy2=3

Prob0E: 5 iterations in 0.03 seconds (average 0.005600, setup 0.00)

Prob1A: 1 iterations in 0.01 seconds (average 0.008000, setup 0.00)

goal = 1, inf = 0, maybe = 130118

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=130119, nc=164044, nnz=307887, k=4] [4.6 MB]
Building sparse matrix (transition rewards)... [n=130119, nc=164044, nnz=65943, k=4] [1.9 MB]
Creating vector for state rewards... [1016.6 KB]
Creating vector for inf... [1016.6 KB]
Allocating iteration vectors... [2 x 1016.6 KB]
TOTAL: [10.5 MB]

Starting iterations...

Iterative method: 94 iterations in 0.38 seconds (average 0.001830, setup 0.21)

Value in the initial state: 75.1573415955869

Time for model checking: 0.455 seconds.

Result: 75.1573415955869 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"time"}min=? [ F "deadlock" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=1,Objy1=1,Objx2=2,Objy2=3

Prob0A: 4 iterations in 0.04 seconds (average 0.009000, setup 0.00)

Prob1E: 5 iterations in 0.04 seconds (average 0.008000, setup 0.00)

Warning: PRISM hasn't checked for zero-reward loops.
Your minimum rewards may be too low...

goal = 1, inf = 0, maybe = 130118

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=130119, nc=164044, nnz=307887, k=4] [4.6 MB]
Building sparse matrix (transition rewards)... [n=130119, nc=164044, nnz=65943, k=4] [1.9 MB]
Creating vector for state rewards... [1016.6 KB]
Creating vector for inf... [1016.6 KB]
Allocating iteration vectors... [2 x 1016.6 KB]
TOTAL: [10.5 MB]

Starting iterations...

Iterative method: 92 iterations in 0.37 seconds (average 0.001870, setup 0.20)

Value in the initial state: 61.70021856889474

Time for model checking: 0.47 seconds.

Result: 61.70021856889474 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"success"}max=? [ F "deadlock" ]/Pmin=? [ F "MissionSuccessful" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=1,Objy1=1,Objx2=2,Objy2=3

Prob0E: 5 iterations in 0.03 seconds (average 0.005600, setup 0.00)

Prob1A: 1 iterations in 0.00 seconds (average 0.004000, setup 0.00)

goal = 1, inf = 0, maybe = 130118

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=130119, nc=164044, nnz=307887, k=4] [4.6 MB]
Building sparse matrix (transition rewards)... [n=130119, nc=164044, nnz=438, k=4] [1.1 MB]
Creating vector for state rewards... [1016.6 KB]
Creating vector for inf... [1016.6 KB]
Allocating iteration vectors... [2 x 1016.6 KB]
TOTAL: [9.7 MB]

Starting iterations...

Iterative method: 90 iterations in 0.35 seconds (average 0.001822, setup 0.18)

Prob0E: 21 iterations in 0.06 seconds (average 0.003048, setup 0.00)

Prob1A: 3 iterations in 0.02 seconds (average 0.006667, setup 0.00)

yes = 222, no = 125198, maybe = 4699

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=130119, nc=8611, nnz=24583, k=2] [423.6 KB]
Creating vector for yes... [1016.6 KB]
Allocating iteration vectors... [2 x 1016.6 KB]
TOTAL: [3.4 MB]

Starting iterations...

Iterative method: 42 iterations in 0.05 seconds (average 0.000762, setup 0.02)

Value in the initial state: Infinity

Time for model checking: 0.559 seconds.

Result: Infinity (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"success"}min=? [ F "deadlock" ]/Pmax=? [ F "MissionSuccessful" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=1,Objy1=1,Objx2=2,Objy2=3

Prob0A: 4 iterations in 0.02 seconds (average 0.006000, setup 0.00)

Prob1E: 5 iterations in 0.04 seconds (average 0.008800, setup 0.00)

Warning: PRISM hasn't checked for zero-reward loops.
Your minimum rewards may be too low...

goal = 1, inf = 0, maybe = 130118

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=130119, nc=164044, nnz=307887, k=4] [4.6 MB]
Building sparse matrix (transition rewards)... [n=130119, nc=164044, nnz=438, k=4] [1.1 MB]
Creating vector for state rewards... [1016.6 KB]
Creating vector for inf... [1016.6 KB]
Allocating iteration vectors... [2 x 1016.6 KB]
TOTAL: [9.7 MB]

Starting iterations...

Iterative method: 48 iterations in 0.27 seconds (average 0.001833, setup 0.18)

Prob0A: 61 iterations in 0.28 seconds (average 0.004656, setup 0.00)

Prob1E: 4 iterations in 0.03 seconds (average 0.008000, setup 0.00)

yes = 222, no = 124023, maybe = 5874

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=130119, nc=10758, nnz=31015, k=2] [501.0 KB]
Creating vector for yes... [1016.6 KB]
Allocating iteration vectors... [2 x 1016.6 KB]
TOTAL: [3.5 MB]

Starting iterations...

Iterative method: 92 iterations in 0.14 seconds (average 0.000696, setup 0.07)

Value in the initial state: 0.0

Time for model checking: 0.811 seconds.

Result: 0.0 (value in the initial state)

---------------------------------------------------------------------

Note: There were 16 warnings during computation.

