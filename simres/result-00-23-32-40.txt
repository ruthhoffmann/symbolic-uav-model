PRISM
=====

Version: 4.3
Date: Wed Aug 03 15:59:56 BST 2016
Hostname: eglinton
Memory limits: cudd=1g, java(heap)=981.5m
Command line: prism uav_sim_mdp.prism -s uav_mdp.props -const 'Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=3,Objy1=2,Objx2=4,Objy2=0,'

Parsing model file "uav_sim_mdp.prism"...

Parsing properties file "uav_mdp.props"...

18 properties:
(1) Pmax=? [ F "MissionSuccessful" ]
(2) Pmin=? [ F "MissionSuccessful" ]
(3) Pmax=? [ F "MissionTimeUp" ]
(4) Pmin=? [ F "MissionTimeUp" ]
(5) Pmax=? [ F "Crash" ]
(6) Pmin=? [ F "Crash" ]
(7) Pmax=? [ F "AllAreaSearched" ]
(8) Pmin=? [ F "AllAreaSearched" ]
(9) Pmax=? [ F "MissionTimeUp"|"AllAreaSearched"|"Crash" ]
(10) Pmin=? [ F "MissionTimeUp"|"AllAreaSearched"|"Crash" ]
(11) R{"drop"}max=? [ F "deadlock" ]
(12) R{"drop"}min=? [ F "deadlock" ]
(13) R{"charge"}max=? [ F "deadlock" ]
(14) R{"charge"}min=? [ F "deadlock" ]
(15) R{"time"}max=? [ F "deadlock" ]
(16) R{"time"}min=? [ F "deadlock" ]
(17) R{"success"}max=? [ F "deadlock" ]/Pmin=? [ F "MissionSuccessful" ]
(18) R{"success"}min=? [ F "deadlock" ]/Pmax=? [ F "MissionSuccessful" ]

Type:        MDP
Modules:     Obj Move Robot T 
Variables:   NoOfObjs posx posy s1 retx rety b t c 

---------------------------------------------------------------------

Model checking: Pmax=? [ F "MissionSuccessful" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=3,Objy1=2,Objx2=4,Objy2=0

Building model...
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=3,Objy1=2,Objx2=4,Objy2=0

Warning: Guard for command 7 of module "Move" is never satisfied.

Warning: Guard for command 8 of module "Move" is never satisfied.

Warning: Guard for command 9 of module "Move" is never satisfied.

Warning: Guard for command 7 of module "Robot" is never satisfied.

Warning: Update 2 of command 3 of module "Robot" doesn't do anything ("(s1'=13)", line 210, column 44)

Warning: Update 2 of command 27 of module "T" doesn't do anything ("(c'=1)", line 323, column 108)

Warning: Guard for command 28 of module "T" is never satisfied.

Warning: Update 2 of command 9 of module "T" doesn't do anything ("(c'=1)", line 300, column 112)

Warning: Guard for command 10 of module "T" is never satisfied.

Warning: Update 2 of command 51 of module "T" doesn't do anything ("(c'=1)", line 354, column 113)

Warning: Guard for command 52 of module "T" is never satisfied.

Computing reachable states...

Reachability (BFS): 87 iterations in 1.21 seconds (average 0.013931, setup 0.00)

Time for model construction: 3.278 seconds.

Warning: Deadlocks detected and fixed in 1 states

Type:        MDP
States:      236971 (1 initial)
Transitions: 598938
Choices:     308040

Transition matrix: 219588 nodes (91 terminal), 598938 minterms, vars: 34r/34c/18nd

Prob0A: 52 iterations in 0.65 seconds (average 0.012538, setup 0.00)

Prob1E: 4 iterations in 0.08 seconds (average 0.020000, setup 0.00)

yes = 1115, no = 200992, maybe = 34864

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=236971, nc=59647, nnz=164290, k=2] [2.2 MB]
Creating vector for yes... [1.8 MB]
Allocating iteration vectors... [2 x 1.8 MB]
TOTAL: [7.6 MB]

Starting iterations...

Iterative method: 92 iterations in 0.40 seconds (average 0.001565, setup 0.26)

Value in the initial state: 0.9830274745226646

Time for model checking: 1.175 seconds.

Result: 0.9830274745226646 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmin=? [ F "MissionSuccessful" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=3,Objy1=2,Objx2=4,Objy2=0

Prob0E: 54 iterations in 0.57 seconds (average 0.010593, setup 0.00)

Prob1A: 3 iterations in 0.04 seconds (average 0.014667, setup 0.00)

yes = 1115, no = 207004, maybe = 28852

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=236971, nc=49250, nnz=134335, k=2] [1.8 MB]
Creating vector for yes... [1.8 MB]
Allocating iteration vectors... [2 x 1.8 MB]
TOTAL: [7.2 MB]

Starting iterations...

Iterative method: 89 iterations in 0.45 seconds (average 0.002427, setup 0.24)

Value in the initial state: 0.8465461219347067

Time for model checking: 1.064 seconds.

Result: 0.8465461219347067 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmax=? [ F "MissionTimeUp" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=3,Objy1=2,Objx2=4,Objy2=0

Prob0A: 68 iterations in 0.69 seconds (average 0.010176, setup 0.00)

Prob1E: 4 iterations in 0.04 seconds (average 0.011000, setup 0.00)

yes = 440, no = 146185, maybe = 90346

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=236971, nc=149420, nnz=397956, k=4] [4.9 MB]
Creating vector for yes... [1.8 MB]
Allocating iteration vectors... [2 x 1.8 MB]
TOTAL: [10.3 MB]

Starting iterations...

Iterative method: 94 iterations in 0.48 seconds (average 0.002000, setup 0.29)

Value in the initial state: 1.205743247358883E-4

Time for model checking: 1.244 seconds.

Result: 1.205743247358883E-4 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmin=? [ F "MissionTimeUp" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=3,Objy1=2,Objx2=4,Objy2=0

Prob0E: 82 iterations in 0.98 seconds (average 0.011951, setup 0.00)

Prob1A: 3 iterations in 0.05 seconds (average 0.017333, setup 0.00)

yes = 440, no = 146531, maybe = 90000

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=236971, nc=148818, nnz=396558, k=4] [4.9 MB]
Creating vector for yes... [1.8 MB]
Allocating iteration vectors... [2 x 1.8 MB]
TOTAL: [10.3 MB]

Starting iterations...

Iterative method: 111 iterations in 0.52 seconds (average 0.002018, setup 0.29)

Value in the initial state: 1.8376996867250056E-7

Time for model checking: 1.571 seconds.

Result: 1.8376996867250056E-7 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmax=? [ F "Crash" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=3,Objy1=2,Objx2=4,Objy2=0

Prob0A: 3 iterations in 0.11 seconds (average 0.037333, setup 0.00)

Prob1E: 199 iterations in 3.10 seconds (average 0.015598, setup 0.00)

yes = 116273, no = 3046, maybe = 117652

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=236971, nc=188721, nnz=479619, k=4] [5.9 MB]
Creating vector for yes... [1.8 MB]
Allocating iteration vectors... [2 x 1.8 MB]
TOTAL: [11.3 MB]

Starting iterations...

Iterative method: 90 iterations in 0.55 seconds (average 0.002178, setup 0.35)

Value in the initial state: 0.02482416894935344

Time for model checking: 3.795 seconds.

Result: 0.02482416894935344 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmin=? [ F "Crash" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=3,Objy1=2,Objx2=4,Objy2=0

Prob0E: 4 iterations in 0.10 seconds (average 0.024000, setup 0.00)

Prob1A: 47 iterations in 0.62 seconds (average 0.013277, setup 0.00)

yes = 116273, no = 3046, maybe = 117652

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=236971, nc=188721, nnz=479619, k=4] [5.9 MB]
Creating vector for yes... [1.8 MB]
Allocating iteration vectors... [2 x 1.8 MB]
TOTAL: [11.3 MB]

Starting iterations...

Iterative method: 83 iterations in 0.46 seconds (average 0.002217, setup 0.28)

Value in the initial state: 0.014932228075366238

Time for model checking: 1.217 seconds.

Result: 0.014932228075366238 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmax=? [ F "AllAreaSearched" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=3,Objy1=2,Objx2=4,Objy2=0

Prob0A: 59 iterations in 0.63 seconds (average 0.010644, setup 0.00)

Prob1E: 4 iterations in 0.06 seconds (average 0.014000, setup 0.00)

yes = 1490, no = 184934, maybe = 50547

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=236971, nc=76219, nnz=192215, k=2] [2.5 MB]
Creating vector for yes... [1.8 MB]
Allocating iteration vectors... [2 x 1.8 MB]
TOTAL: [7.9 MB]

Starting iterations...

Iterative method: 87 iterations in 0.41 seconds (average 0.001701, setup 0.26)

Value in the initial state: 0.1305649331398421

Time for model checking: 1.125 seconds.

Result: 0.1305649331398421 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmin=? [ F "AllAreaSearched" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=3,Objy1=2,Objx2=4,Objy2=0

Prob0E: 62 iterations in 0.43 seconds (average 0.006903, setup 0.00)

Prob1A: 3 iterations in 0.05 seconds (average 0.017333, setup 0.00)

yes = 1490, no = 191523, maybe = 43958

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=236971, nc=65916, nnz=164859, k=2] [2.2 MB]
Creating vector for yes... [1.8 MB]
Allocating iteration vectors... [2 x 1.8 MB]
TOTAL: [7.6 MB]

Starting iterations...

Iterative method: 94 iterations in 0.29 seconds (average 0.001617, setup 0.14)

Value in the initial state: 0.0018445422051452819

Time for model checking: 0.785 seconds.

Result: 0.0018445422051452819 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmax=? [ F "MissionTimeUp"|"AllAreaSearched"|"Crash" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=3,Objy1=2,Objx2=4,Objy2=0

Prob0A: 3 iterations in 0.08 seconds (average 0.026667, setup 0.00)

Prob1E: 208 iterations in 2.72 seconds (average 0.013058, setup 0.00)

yes = 207003, no = 1116, maybe = 28852

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=236971, nc=49250, nnz=134335, k=2] [1.8 MB]
Creating vector for yes... [1.8 MB]
Allocating iteration vectors... [2 x 1.8 MB]
TOTAL: [7.2 MB]

Starting iterations...

Iterative method: 82 iterations in 0.32 seconds (average 0.001610, setup 0.19)

Value in the initial state: 0.15345366963440493

Time for model checking: 3.155 seconds.

Result: 0.15345366963440493 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmin=? [ F "MissionTimeUp"|"AllAreaSearched"|"Crash" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=3,Objy1=2,Objx2=4,Objy2=0

Prob0E: 4 iterations in 0.08 seconds (average 0.021000, setup 0.00)

Prob1A: 52 iterations in 0.47 seconds (average 0.009077, setup 0.00)

yes = 200991, no = 1116, maybe = 34864

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=236971, nc=59647, nnz=164290, k=2] [2.2 MB]
Creating vector for yes... [1.8 MB]
Allocating iteration vectors... [2 x 1.8 MB]
TOTAL: [7.6 MB]

Starting iterations...

Iterative method: 91 iterations in 0.32 seconds (average 0.001495, setup 0.18)

Value in the initial state: 0.01697135869073804

Time for model checking: 0.925 seconds.

Result: 0.01697135869073804 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"drop"}max=? [ F "deadlock" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=3,Objy1=2,Objx2=4,Objy2=0

Prob0E: 5 iterations in 0.07 seconds (average 0.014400, setup 0.00)

Prob1A: 1 iterations in 0.01 seconds (average 0.008000, setup 0.00)

goal = 1, inf = 0, maybe = 236970

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=236971, nc=308039, nnz=598937, k=4] [8.9 MB]
Building sparse matrix (transition rewards)... [n=236971, nc=308039, nnz=0, k=4] [2.1 MB]
Creating vector for state rewards... [1.8 MB]
Creating vector for inf... [1.8 MB]
Allocating iteration vectors... [2 x 1.8 MB]
TOTAL: [18.2 MB]

Starting iterations...

Iterative method: 88 iterations in 0.72 seconds (average 0.003364, setup 0.43)

Value in the initial state: 0.17237720567817666

Time for model checking: 0.815 seconds.

Result: 0.17237720567817666 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"drop"}min=? [ F "deadlock" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=3,Objy1=2,Objx2=4,Objy2=0

Prob0A: 4 iterations in 0.05 seconds (average 0.012000, setup 0.00)

Prob1E: 5 iterations in 0.05 seconds (average 0.010400, setup 0.00)

Warning: PRISM hasn't checked for zero-reward loops.
Your minimum rewards may be too low...

goal = 1, inf = 0, maybe = 236970

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=236971, nc=308039, nnz=598937, k=4] [8.9 MB]
Building sparse matrix (transition rewards)... [n=236971, nc=308039, nnz=0, k=4] [2.1 MB]
Creating vector for state rewards... [1.8 MB]
Creating vector for inf... [1.8 MB]
Allocating iteration vectors... [2 x 1.8 MB]
TOTAL: [18.2 MB]

Starting iterations...

Iterative method: 80 iterations in 0.60 seconds (average 0.003350, setup 0.33)

Value in the initial state: 0.16378295493005104

Time for model checking: 0.712 seconds.

Result: 0.16378295493005104 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"charge"}max=? [ F "deadlock" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=3,Objy1=2,Objx2=4,Objy2=0

Prob0E: 5 iterations in 0.04 seconds (average 0.007200, setup 0.00)

Prob1A: 1 iterations in 0.00 seconds (average 0.004000, setup 0.00)

goal = 1, inf = 0, maybe = 236970

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=236971, nc=308039, nnz=598937, k=4] [8.9 MB]
Building sparse matrix (transition rewards)... [n=236971, nc=308039, nnz=6061, k=4] [2.1 MB]
Creating vector for state rewards... [1.8 MB]
Creating vector for inf... [1.8 MB]
Allocating iteration vectors... [2 x 1.8 MB]
TOTAL: [18.3 MB]

Starting iterations...

Iterative method: 88 iterations in 0.62 seconds (average 0.003364, setup 0.33)

Value in the initial state: 0.27259910064663156

Time for model checking: 0.676 seconds.

Result: 0.27259910064663156 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"charge"}min=? [ F "deadlock" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=3,Objy1=2,Objx2=4,Objy2=0

Prob0A: 4 iterations in 0.03 seconds (average 0.007000, setup 0.00)

Prob1E: 5 iterations in 0.04 seconds (average 0.008800, setup 0.00)

Warning: PRISM hasn't checked for zero-reward loops.
Your minimum rewards may be too low...

goal = 1, inf = 0, maybe = 236970

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=236971, nc=308039, nnz=598937, k=4] [8.9 MB]
Building sparse matrix (transition rewards)... [n=236971, nc=308039, nnz=6061, k=4] [2.1 MB]
Creating vector for state rewards... [1.8 MB]
Creating vector for inf... [1.8 MB]
Allocating iteration vectors... [2 x 1.8 MB]
TOTAL: [18.3 MB]

Starting iterations...

Iterative method: 96 iterations in 0.66 seconds (average 0.003417, setup 0.33)

Value in the initial state: 0.019563316441124422

Time for model checking: 0.738 seconds.

Result: 0.019563316441124422 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"time"}max=? [ F "deadlock" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=3,Objy1=2,Objx2=4,Objy2=0

Prob0E: 5 iterations in 0.03 seconds (average 0.005600, setup 0.00)

Prob1A: 1 iterations in 0.00 seconds (average 0.004000, setup 0.00)

goal = 1, inf = 0, maybe = 236970

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=236971, nc=308039, nnz=598937, k=4] [8.9 MB]
Building sparse matrix (transition rewards)... [n=236971, nc=308039, nnz=119317, k=4] [3.4 MB]
Creating vector for state rewards... [1.8 MB]
Creating vector for inf... [1.8 MB]
Allocating iteration vectors... [2 x 1.8 MB]
TOTAL: [19.6 MB]

Starting iterations...

Iterative method: 93 iterations in 0.68 seconds (average 0.003441, setup 0.36)

Value in the initial state: 137.22235347455938

Time for model checking: 0.732 seconds.

Result: 137.22235347455938 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"time"}min=? [ F "deadlock" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=3,Objy1=2,Objx2=4,Objy2=0

Prob0A: 4 iterations in 0.03 seconds (average 0.008000, setup 0.00)

Prob1E: 5 iterations in 0.05 seconds (average 0.009600, setup 0.00)

Warning: PRISM hasn't checked for zero-reward loops.
Your minimum rewards may be too low...

goal = 1, inf = 0, maybe = 236970

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=236971, nc=308039, nnz=598937, k=4] [8.9 MB]
Building sparse matrix (transition rewards)... [n=236971, nc=308039, nnz=119317, k=4] [3.4 MB]
Creating vector for state rewards... [1.8 MB]
Creating vector for inf... [1.8 MB]
Allocating iteration vectors... [2 x 1.8 MB]
TOTAL: [19.6 MB]

Starting iterations...

Iterative method: 98 iterations in 0.70 seconds (average 0.003469, setup 0.36)

Value in the initial state: 82.84931294857405

Time for model checking: 0.785 seconds.

Result: 82.84931294857405 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"success"}max=? [ F "deadlock" ]/Pmin=? [ F "MissionSuccessful" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=3,Objy1=2,Objx2=4,Objy2=0

Prob0E: 5 iterations in 0.03 seconds (average 0.006400, setup 0.00)

Prob1A: 1 iterations in 0.00 seconds (average 0.004000, setup 0.00)

goal = 1, inf = 0, maybe = 236970

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=236971, nc=308039, nnz=598937, k=4] [8.9 MB]
Building sparse matrix (transition rewards)... [n=236971, nc=308039, nnz=2198, k=4] [2.1 MB]
Creating vector for state rewards... [1.8 MB]
Creating vector for inf... [1.8 MB]
Allocating iteration vectors... [2 x 1.8 MB]
TOTAL: [18.3 MB]

Starting iterations...

Iterative method: 96 iterations in 0.63 seconds (average 0.003250, setup 0.32)

Prob0E: 54 iterations in 0.33 seconds (average 0.006074, setup 0.00)

Prob1A: 3 iterations in 0.05 seconds (average 0.017333, setup 0.00)

yes = 1115, no = 207004, maybe = 28852

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=236971, nc=49250, nnz=134335, k=2] [1.8 MB]
Creating vector for yes... [1.8 MB]
Allocating iteration vectors... [2 x 1.8 MB]
TOTAL: [7.2 MB]

Starting iterations...

Iterative method: 89 iterations in 0.26 seconds (average 0.001483, setup 0.12)

Value in the initial state: 159.22477810928814

Time for model checking: 1.336 seconds.

Result: 159.22477810928814 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"success"}min=? [ F "deadlock" ]/Pmax=? [ F "MissionSuccessful" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=3,Objy1=2,Objx2=4,Objy2=0

Prob0A: 4 iterations in 0.07 seconds (average 0.017000, setup 0.00)

Prob1E: 5 iterations in 0.04 seconds (average 0.007200, setup 0.00)

Warning: PRISM hasn't checked for zero-reward loops.
Your minimum rewards may be too low...

goal = 1, inf = 0, maybe = 236970

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=236971, nc=308039, nnz=598937, k=4] [8.9 MB]
Building sparse matrix (transition rewards)... [n=236971, nc=308039, nnz=2198, k=4] [2.1 MB]
Creating vector for state rewards... [1.8 MB]
Creating vector for inf... [1.8 MB]
Allocating iteration vectors... [2 x 1.8 MB]
TOTAL: [18.3 MB]

Starting iterations...

Iterative method: 92 iterations in 0.62 seconds (average 0.003130, setup 0.34)

Prob0A: 52 iterations in 0.33 seconds (average 0.006308, setup 0.00)

Prob1E: 4 iterations in 0.04 seconds (average 0.009000, setup 0.00)

yes = 1115, no = 200992, maybe = 34864

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=236971, nc=59647, nnz=164290, k=2] [2.2 MB]
Creating vector for yes... [1.8 MB]
Allocating iteration vectors... [2 x 1.8 MB]
TOTAL: [7.6 MB]

Starting iterations...

Iterative method: 92 iterations in 0.28 seconds (average 0.001391, setup 0.16)

Value in the initial state: 76.55340921926366

Time for model checking: 1.402 seconds.

Result: 76.55340921926366 (value in the initial state)

---------------------------------------------------------------------

Note: There were 16 warnings during computation.

