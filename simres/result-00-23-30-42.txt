PRISM
=====

Version: 4.3
Date: Wed Aug 03 15:58:45 BST 2016
Hostname: eglinton
Memory limits: cudd=1g, java(heap)=981.5m
Command line: prism uav_sim_mdp.prism -s uav_mdp.props -const 'Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=3,Objy1=0,Objx2=4,Objy2=2,'

Parsing model file "uav_sim_mdp.prism"...

Parsing properties file "uav_mdp.props"...

18 properties:
(1) Pmax=? [ F "MissionSuccessful" ]
(2) Pmin=? [ F "MissionSuccessful" ]
(3) Pmax=? [ F "MissionTimeUp" ]
(4) Pmin=? [ F "MissionTimeUp" ]
(5) Pmax=? [ F "Crash" ]
(6) Pmin=? [ F "Crash" ]
(7) Pmax=? [ F "AllAreaSearched" ]
(8) Pmin=? [ F "AllAreaSearched" ]
(9) Pmax=? [ F "MissionTimeUp"|"AllAreaSearched"|"Crash" ]
(10) Pmin=? [ F "MissionTimeUp"|"AllAreaSearched"|"Crash" ]
(11) R{"drop"}max=? [ F "deadlock" ]
(12) R{"drop"}min=? [ F "deadlock" ]
(13) R{"charge"}max=? [ F "deadlock" ]
(14) R{"charge"}min=? [ F "deadlock" ]
(15) R{"time"}max=? [ F "deadlock" ]
(16) R{"time"}min=? [ F "deadlock" ]
(17) R{"success"}max=? [ F "deadlock" ]/Pmin=? [ F "MissionSuccessful" ]
(18) R{"success"}min=? [ F "deadlock" ]/Pmax=? [ F "MissionSuccessful" ]

Type:        MDP
Modules:     Obj Move Robot T 
Variables:   NoOfObjs posx posy s1 retx rety b t c 

---------------------------------------------------------------------

Model checking: Pmax=? [ F "MissionSuccessful" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=3,Objy1=0,Objx2=4,Objy2=2

Building model...
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=3,Objy1=0,Objx2=4,Objy2=2

Warning: Guard for command 7 of module "Move" is never satisfied.

Warning: Guard for command 8 of module "Move" is never satisfied.

Warning: Guard for command 9 of module "Move" is never satisfied.

Warning: Guard for command 7 of module "Robot" is never satisfied.

Warning: Update 2 of command 3 of module "Robot" doesn't do anything ("(s1'=13)", line 210, column 44)

Warning: Update 2 of command 27 of module "T" doesn't do anything ("(c'=1)", line 323, column 108)

Warning: Guard for command 28 of module "T" is never satisfied.

Warning: Update 2 of command 9 of module "T" doesn't do anything ("(c'=1)", line 300, column 112)

Warning: Guard for command 10 of module "T" is never satisfied.

Warning: Update 2 of command 51 of module "T" doesn't do anything ("(c'=1)", line 354, column 113)

Warning: Guard for command 52 of module "T" is never satisfied.

Computing reachable states...

Reachability (BFS): 88 iterations in 1.51 seconds (average 0.017136, setup 0.00)

Time for model construction: 3.405 seconds.

Warning: Deadlocks detected and fixed in 1 states

Type:        MDP
States:      275637 (1 initial)
Transitions: 714706
Choices:     361134

Transition matrix: 222786 nodes (93 terminal), 714706 minterms, vars: 34r/34c/18nd

Prob0A: 55 iterations in 0.56 seconds (average 0.010109, setup 0.00)

Prob1E: 4 iterations in 0.07 seconds (average 0.018000, setup 0.00)

yes = 1164, no = 233422, maybe = 41051

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=275637, nc=70431, nnz=197254, k=2] [2.6 MB]
Creating vector for yes... [2.1 MB]
Allocating iteration vectors... [2 x 2.1 MB]
TOTAL: [8.9 MB]

Starting iterations...

Iterative method: 95 iterations in 0.43 seconds (average 0.001853, setup 0.26)

Value in the initial state: 0.9823756125373581

Time for model checking: 1.085 seconds.

Result: 0.9823756125373581 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmin=? [ F "MissionSuccessful" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=3,Objy1=0,Objx2=4,Objy2=2

Prob0E: 57 iterations in 0.48 seconds (average 0.008421, setup 0.00)

Prob1A: 3 iterations in 0.05 seconds (average 0.016000, setup 0.00)

yes = 1164, no = 239897, maybe = 34576

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=275637, nc=59281, nnz=165284, k=2] [2.2 MB]
Creating vector for yes... [2.1 MB]
Allocating iteration vectors... [2 x 2.1 MB]
TOTAL: [8.5 MB]

Starting iterations...

Iterative method: 89 iterations in 0.35 seconds (average 0.001753, setup 0.20)

Value in the initial state: 0.8213893738141339

Time for model checking: 0.9 seconds.

Result: 0.8213893738141339 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmax=? [ F "MissionTimeUp" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=3,Objy1=0,Objx2=4,Objy2=2

Prob0A: 67 iterations in 0.76 seconds (average 0.011403, setup 0.00)

Prob1E: 4 iterations in 0.05 seconds (average 0.012000, setup 0.00)

yes = 480, no = 165159, maybe = 109998

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=275637, nc=182894, nnz=493423, k=4] [6.1 MB]
Creating vector for yes... [2.1 MB]
Allocating iteration vectors... [2 x 2.1 MB]
TOTAL: [12.4 MB]

Starting iterations...

Iterative method: 96 iterations in 0.60 seconds (average 0.002458, setup 0.36)

Value in the initial state: 2.581920069981414E-4

Time for model checking: 1.409 seconds.

Result: 2.581920069981414E-4 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmin=? [ F "MissionTimeUp" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=3,Objy1=0,Objx2=4,Objy2=2

Prob0E: 83 iterations in 1.07 seconds (average 0.012916, setup 0.00)

Prob1A: 3 iterations in 0.06 seconds (average 0.021333, setup 0.00)

yes = 480, no = 165566, maybe = 109591

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=275637, nc=182177, nnz=491844, k=4] [6.1 MB]
Creating vector for yes... [2.1 MB]
Allocating iteration vectors... [2 x 2.1 MB]
TOTAL: [12.4 MB]

Starting iterations...

Iterative method: 112 iterations in 0.62 seconds (average 0.002429, setup 0.34)

Value in the initial state: 3.0698421978876213E-7

Time for model checking: 1.793 seconds.

Result: 3.0698421978876213E-7 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmax=? [ F "Crash" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=3,Objy1=0,Objx2=4,Objy2=2

Prob0A: 3 iterations in 0.11 seconds (average 0.036000, setup 0.00)

Prob1E: 204 iterations in 3.60 seconds (average 0.017647, setup 0.00)

yes = 135122, no = 3227, maybe = 137288

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=275637, nc=222785, nnz=576357, k=4] [7.1 MB]
Creating vector for yes... [2.1 MB]
Allocating iteration vectors... [2 x 2.1 MB]
TOTAL: [13.4 MB]

Starting iterations...

Iterative method: 91 iterations in 0.64 seconds (average 0.002637, setup 0.40)

Value in the initial state: 0.03018628229425363

Time for model checking: 4.372 seconds.

Result: 0.03018628229425363 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmin=? [ F "Crash" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=3,Objy1=0,Objx2=4,Objy2=2

Prob0E: 4 iterations in 0.11 seconds (average 0.027000, setup 0.00)

Prob1A: 49 iterations in 0.76 seconds (average 0.015429, setup 0.00)

yes = 135122, no = 3227, maybe = 137288

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=275637, nc=222785, nnz=576357, k=4] [7.1 MB]
Creating vector for yes... [2.1 MB]
Allocating iteration vectors... [2 x 2.1 MB]
TOTAL: [13.4 MB]

Starting iterations...

Iterative method: 87 iterations in 0.53 seconds (average 0.002713, setup 0.30)

Value in the initial state: 0.015565227873569301

Time for model checking: 1.428 seconds.

Result: 0.015565227873569301 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmax=? [ F "AllAreaSearched" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=3,Objy1=0,Objx2=4,Objy2=2

Prob0A: 61 iterations in 0.64 seconds (average 0.010426, setup 0.00)

Prob1E: 4 iterations in 0.06 seconds (average 0.015000, setup 0.00)

yes = 1582, no = 215137, maybe = 58918

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=275637, nc=91556, nnz=239977, k=2] [3.1 MB]
Creating vector for yes... [2.1 MB]
Allocating iteration vectors... [2 x 2.1 MB]
TOTAL: [9.4 MB]

Starting iterations...

Iterative method: 88 iterations in 0.38 seconds (average 0.002045, setup 0.20)

Value in the initial state: 0.15288456174516069

Time for model checking: 1.105 seconds.

Result: 0.15288456174516069 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmin=? [ F "AllAreaSearched" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=3,Objy1=0,Objx2=4,Objy2=2

Prob0E: 64 iterations in 0.46 seconds (average 0.007188, setup 0.00)

Prob1A: 3 iterations in 0.04 seconds (average 0.013333, setup 0.00)

yes = 1582, no = 223207, maybe = 50848

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=275637, nc=78689, nnz=205088, k=2] [2.7 MB]
Creating vector for yes... [2.1 MB]
Allocating iteration vectors... [2 x 2.1 MB]
TOTAL: [9.0 MB]

Starting iterations...

Iterative method: 94 iterations in 0.32 seconds (average 0.001872, setup 0.14)

Value in the initial state: 0.0017582196904960166

Time for model checking: 0.844 seconds.

Result: 0.0017582196904960166 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmax=? [ F "MissionTimeUp"|"AllAreaSearched"|"Crash" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=3,Objy1=0,Objx2=4,Objy2=2

Prob0A: 3 iterations in 0.09 seconds (average 0.029333, setup 0.00)

Prob1E: 220 iterations in 3.23 seconds (average 0.014691, setup 0.00)

yes = 239896, no = 1165, maybe = 34576

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=275637, nc=59281, nnz=165284, k=2] [2.2 MB]
Creating vector for yes... [2.1 MB]
Allocating iteration vectors... [2 x 2.1 MB]
TOTAL: [8.5 MB]

Starting iterations...

Iterative method: 83 iterations in 0.38 seconds (average 0.001735, setup 0.24)

Value in the initial state: 0.1786104040711892

Time for model checking: 3.728 seconds.

Result: 0.1786104040711892 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmin=? [ F "MissionTimeUp"|"AllAreaSearched"|"Crash" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=3,Objy1=0,Objx2=4,Objy2=2

Prob0E: 4 iterations in 0.08 seconds (average 0.019000, setup 0.00)

Prob1A: 55 iterations in 0.64 seconds (average 0.011709, setup 0.00)

yes = 233421, no = 1165, maybe = 41051

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=275637, nc=70431, nnz=197254, k=2] [2.6 MB]
Creating vector for yes... [2.1 MB]
Allocating iteration vectors... [2 x 2.1 MB]
TOTAL: [8.9 MB]

Starting iterations...

Iterative method: 94 iterations in 0.39 seconds (average 0.001872, setup 0.22)

Value in the initial state: 0.017623166040922934

Time for model checking: 1.141 seconds.

Result: 0.017623166040922934 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"drop"}max=? [ F "deadlock" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=3,Objy1=0,Objx2=4,Objy2=2

Prob0E: 5 iterations in 0.11 seconds (average 0.021600, setup 0.00)

Prob1A: 1 iterations in 0.01 seconds (average 0.012000, setup 0.00)

goal = 1, inf = 0, maybe = 275636

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=275637, nc=361133, nnz=714705, k=4] [10.6 MB]
Building sparse matrix (transition rewards)... [n=275637, nc=361133, nnz=0, k=4] [2.4 MB]
Creating vector for state rewards... [2.1 MB]
Creating vector for inf... [2.1 MB]
Allocating iteration vectors... [2 x 2.1 MB]
TOTAL: [21.4 MB]

Starting iterations...

Iterative method: 91 iterations in 0.88 seconds (average 0.004044, setup 0.51)

Value in the initial state: 0.2117000288569924

Time for model checking: 1.009 seconds.

Result: 0.2117000288569924 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"drop"}min=? [ F "deadlock" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=3,Objy1=0,Objx2=4,Objy2=2

Prob0A: 4 iterations in 0.05 seconds (average 0.012000, setup 0.00)

Prob1E: 5 iterations in 0.06 seconds (average 0.012000, setup 0.00)

Warning: PRISM hasn't checked for zero-reward loops.
Your minimum rewards may be too low...

goal = 1, inf = 0, maybe = 275636

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=275637, nc=361133, nnz=714705, k=4] [10.6 MB]
Building sparse matrix (transition rewards)... [n=275637, nc=361133, nnz=0, k=4] [2.4 MB]
Creating vector for state rewards... [2.1 MB]
Creating vector for inf... [2.1 MB]
Allocating iteration vectors... [2 x 2.1 MB]
TOTAL: [21.4 MB]

Starting iterations...

Iterative method: 83 iterations in 0.70 seconds (average 0.004000, setup 0.37)

Value in the initial state: 0.18736825600153614

Time for model checking: 0.833 seconds.

Result: 0.18736825600153614 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"charge"}max=? [ F "deadlock" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=3,Objy1=0,Objx2=4,Objy2=2

Prob0E: 5 iterations in 0.04 seconds (average 0.008800, setup 0.00)

Prob1A: 1 iterations in 0.01 seconds (average 0.008000, setup 0.00)

goal = 1, inf = 0, maybe = 275636

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=275637, nc=361133, nnz=714705, k=4] [10.6 MB]
Building sparse matrix (transition rewards)... [n=275637, nc=361133, nnz=7628, k=4] [2.5 MB]
Creating vector for state rewards... [2.1 MB]
Creating vector for inf... [2.1 MB]
Allocating iteration vectors... [2 x 2.1 MB]
TOTAL: [21.5 MB]

Starting iterations...

Iterative method: 90 iterations in 0.73 seconds (average 0.003956, setup 0.38)

Value in the initial state: 0.29161000895488837

Time for model checking: 0.807 seconds.

Result: 0.29161000895488837 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"charge"}min=? [ F "deadlock" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=3,Objy1=0,Objx2=4,Objy2=2

Prob0A: 4 iterations in 0.04 seconds (average 0.009000, setup 0.00)

Prob1E: 5 iterations in 0.06 seconds (average 0.012000, setup 0.00)

Warning: PRISM hasn't checked for zero-reward loops.
Your minimum rewards may be too low...

goal = 1, inf = 0, maybe = 275636

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=275637, nc=361133, nnz=714705, k=4] [10.6 MB]
Building sparse matrix (transition rewards)... [n=275637, nc=361133, nnz=7628, k=4] [2.5 MB]
Creating vector for state rewards... [2.1 MB]
Creating vector for inf... [2.1 MB]
Allocating iteration vectors... [2 x 2.1 MB]
TOTAL: [21.5 MB]

Starting iterations...

Iterative method: 98 iterations in 0.77 seconds (average 0.004000, setup 0.38)

Value in the initial state: 0.029551760044980085

Time for model checking: 0.893 seconds.

Result: 0.029551760044980085 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"time"}max=? [ F "deadlock" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=3,Objy1=0,Objx2=4,Objy2=2

Prob0E: 5 iterations in 0.05 seconds (average 0.009600, setup 0.00)

Prob1A: 1 iterations in 0.00 seconds (average 0.004000, setup 0.00)

goal = 1, inf = 0, maybe = 275636

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=275637, nc=361133, nnz=714705, k=4] [10.6 MB]
Building sparse matrix (transition rewards)... [n=275637, nc=361133, nnz=138347, k=4] [4.0 MB]
Creating vector for state rewards... [2.1 MB]
Creating vector for inf... [2.1 MB]
Allocating iteration vectors... [2 x 2.1 MB]
TOTAL: [23.0 MB]

Starting iterations...

Iterative method: 94 iterations in 0.81 seconds (average 0.004043, setup 0.43)

Value in the initial state: 166.58202532655287

Time for model checking: 0.878 seconds.

Result: 166.58202532655287 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"time"}min=? [ F "deadlock" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=3,Objy1=0,Objx2=4,Objy2=2

Prob0A: 4 iterations in 0.03 seconds (average 0.008000, setup 0.00)

Prob1E: 5 iterations in 0.06 seconds (average 0.012000, setup 0.00)

Warning: PRISM hasn't checked for zero-reward loops.
Your minimum rewards may be too low...

goal = 1, inf = 0, maybe = 275636

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=275637, nc=361133, nnz=714705, k=4] [10.6 MB]
Building sparse matrix (transition rewards)... [n=275637, nc=361133, nnz=138347, k=4] [4.0 MB]
Creating vector for state rewards... [2.1 MB]
Creating vector for inf... [2.1 MB]
Allocating iteration vectors... [2 x 2.1 MB]
TOTAL: [23.0 MB]

Starting iterations...

Iterative method: 101 iterations in 0.84 seconds (average 0.004119, setup 0.42)

Value in the initial state: 86.33045086214999

Time for model checking: 0.951 seconds.

Result: 86.33045086214999 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"success"}max=? [ F "deadlock" ]/Pmin=? [ F "MissionSuccessful" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=3,Objy1=0,Objx2=4,Objy2=2

Prob0E: 5 iterations in 0.04 seconds (average 0.008800, setup 0.00)

Prob1A: 1 iterations in 0.01 seconds (average 0.008000, setup 0.00)

goal = 1, inf = 0, maybe = 275636

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=275637, nc=361133, nnz=714705, k=4] [10.6 MB]
Building sparse matrix (transition rewards)... [n=275637, nc=361133, nnz=2294, k=4] [2.5 MB]
Creating vector for state rewards... [2.1 MB]
Creating vector for inf... [2.1 MB]
Allocating iteration vectors... [2 x 2.1 MB]
TOTAL: [21.5 MB]

Starting iterations...

Iterative method: 98 iterations in 0.76 seconds (average 0.003918, setup 0.38)

Prob0E: 57 iterations in 0.48 seconds (average 0.008491, setup 0.00)

Prob1A: 3 iterations in 0.07 seconds (average 0.022667, setup 0.00)

yes = 1164, no = 239897, maybe = 34576

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=275637, nc=59281, nnz=165284, k=2] [2.2 MB]
Creating vector for yes... [2.1 MB]
Allocating iteration vectors... [2 x 2.1 MB]
TOTAL: [8.5 MB]

Starting iterations...

Iterative method: 89 iterations in 0.35 seconds (average 0.001753, setup 0.19)

Value in the initial state: 198.9312733871134

Time for model checking: 1.753 seconds.

Result: 198.9312733871134 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"success"}min=? [ F "deadlock" ]/Pmax=? [ F "MissionSuccessful" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=3,Objy1=0,Objx2=4,Objy2=2

Prob0A: 4 iterations in 0.06 seconds (average 0.016000, setup 0.00)

Prob1E: 5 iterations in 0.05 seconds (average 0.009600, setup 0.00)

Warning: PRISM hasn't checked for zero-reward loops.
Your minimum rewards may be too low...

goal = 1, inf = 0, maybe = 275636

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=275637, nc=361133, nnz=714705, k=4] [10.6 MB]
Building sparse matrix (transition rewards)... [n=275637, nc=361133, nnz=2294, k=4] [2.5 MB]
Creating vector for state rewards... [2.1 MB]
Creating vector for inf... [2.1 MB]
Allocating iteration vectors... [2 x 2.1 MB]
TOTAL: [21.5 MB]

Starting iterations...

Iterative method: 94 iterations in 0.83 seconds (average 0.003957, setup 0.46)

Prob0A: 55 iterations in 0.51 seconds (average 0.009236, setup 0.00)

Prob1E: 4 iterations in 0.06 seconds (average 0.014000, setup 0.00)

yes = 1164, no = 233422, maybe = 41051

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=275637, nc=70431, nnz=197254, k=2] [2.6 MB]
Creating vector for yes... [2.1 MB]
Allocating iteration vectors... [2 x 2.1 MB]
TOTAL: [8.9 MB]

Starting iterations...

Iterative method: 95 iterations in 0.40 seconds (average 0.001811, setup 0.23)

Value in the initial state: 76.88777123041739

Time for model checking: 1.936 seconds.

Result: 76.88777123041739 (value in the initial state)

---------------------------------------------------------------------

Note: There were 16 warnings during computation.

