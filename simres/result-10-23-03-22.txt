PRISM
=====

Version: 4.3
Date: Wed Aug 03 16:08:38 BST 2016
Hostname: eglinton
Memory limits: cudd=1g, java(heap)=981.5m
Command line: prism uav_sim_mdp.prism -s uav_mdp.props -const 'Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=0,Objy1=3,Objx2=2,Objy2=2,'

Parsing model file "uav_sim_mdp.prism"...

Parsing properties file "uav_mdp.props"...

18 properties:
(1) Pmax=? [ F "MissionSuccessful" ]
(2) Pmin=? [ F "MissionSuccessful" ]
(3) Pmax=? [ F "MissionTimeUp" ]
(4) Pmin=? [ F "MissionTimeUp" ]
(5) Pmax=? [ F "Crash" ]
(6) Pmin=? [ F "Crash" ]
(7) Pmax=? [ F "AllAreaSearched" ]
(8) Pmin=? [ F "AllAreaSearched" ]
(9) Pmax=? [ F "MissionTimeUp"|"AllAreaSearched"|"Crash" ]
(10) Pmin=? [ F "MissionTimeUp"|"AllAreaSearched"|"Crash" ]
(11) R{"drop"}max=? [ F "deadlock" ]
(12) R{"drop"}min=? [ F "deadlock" ]
(13) R{"charge"}max=? [ F "deadlock" ]
(14) R{"charge"}min=? [ F "deadlock" ]
(15) R{"time"}max=? [ F "deadlock" ]
(16) R{"time"}min=? [ F "deadlock" ]
(17) R{"success"}max=? [ F "deadlock" ]/Pmin=? [ F "MissionSuccessful" ]
(18) R{"success"}min=? [ F "deadlock" ]/Pmax=? [ F "MissionSuccessful" ]

Type:        MDP
Modules:     Obj Move Robot T 
Variables:   NoOfObjs posx posy s1 retx rety b t c 

---------------------------------------------------------------------

Model checking: Pmax=? [ F "MissionSuccessful" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=0,Objy1=3,Objx2=2,Objy2=2

Building model...
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=0,Objy1=3,Objx2=2,Objy2=2

Warning: Guard for command 7 of module "Move" is never satisfied.

Warning: Guard for command 8 of module "Move" is never satisfied.

Warning: Guard for command 9 of module "Move" is never satisfied.

Warning: Guard for command 6 of module "Robot" is never satisfied.

Warning: Update 2 of command 3 of module "Robot" doesn't do anything ("(s1'=13)", line 210, column 44)

Warning: Update 2 of command 27 of module "T" doesn't do anything ("(c'=1)", line 323, column 108)

Warning: Guard for command 28 of module "T" is never satisfied.

Warning: Update 2 of command 9 of module "T" doesn't do anything ("(c'=1)", line 300, column 112)

Warning: Guard for command 10 of module "T" is never satisfied.

Warning: Update 2 of command 51 of module "T" doesn't do anything ("(c'=1)", line 354, column 113)

Warning: Guard for command 52 of module "T" is never satisfied.

Computing reachable states...

Reachability (BFS): 87 iterations in 0.83 seconds (average 0.009563, setup 0.00)

Time for model construction: 3.32 seconds.

Warning: Deadlocks detected and fixed in 1 states

Type:        MDP
States:      164105 (1 initial)
Transitions: 447763
Choices:     221222

Transition matrix: 198849 nodes (87 terminal), 447763 minterms, vars: 34r/34c/18nd

Prob0A: 55 iterations in 0.40 seconds (average 0.007273, setup 0.00)

Prob1E: 4 iterations in 0.05 seconds (average 0.012000, setup 0.00)

yes = 874, no = 123472, maybe = 39759

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=164105, nc=68858, nnz=189450, k=2] [2.4 MB]
Creating vector for yes... [1.3 MB]
Allocating iteration vectors... [2 x 1.3 MB]
TOTAL: [6.1 MB]

Starting iterations...

Iterative method: 92 iterations in 0.35 seconds (average 0.001217, setup 0.24)

Value in the initial state: 0.9436611288270593

Time for model checking: 0.858 seconds.

Result: 0.9436611288270593 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmin=? [ F "MissionSuccessful" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=0,Objy1=3,Objx2=2,Objy2=2

Prob0E: 55 iterations in 0.46 seconds (average 0.008364, setup 0.00)

Prob1A: 3 iterations in 0.06 seconds (average 0.018667, setup 0.00)

yes = 874, no = 131465, maybe = 31766

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=164105, nc=54917, nnz=150554, k=2] [1.9 MB]
Creating vector for yes... [1.3 MB]
Allocating iteration vectors... [2 x 1.3 MB]
TOTAL: [5.7 MB]

Starting iterations...

Iterative method: 93 iterations in 0.32 seconds (average 0.001118, setup 0.22)

Value in the initial state: 0.7275217823499556

Time for model checking: 1.196 seconds.

Result: 0.7275217823499556 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmax=? [ F "MissionTimeUp" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=0,Objy1=3,Objx2=2,Objy2=2

Prob0A: 77 iterations in 0.56 seconds (average 0.007221, setup 0.00)

Prob1E: 4 iterations in 0.04 seconds (average 0.010000, setup 0.00)

yes = 204, no = 142199, maybe = 21702

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=164105, nc=38078, nnz=102390, k=4] [1.4 MB]
Creating vector for yes... [1.3 MB]
Allocating iteration vectors... [2 x 1.3 MB]
TOTAL: [5.1 MB]

Starting iterations...

Iterative method: 97 iterations in 0.23 seconds (average 0.001072, setup 0.12)

Value in the initial state: 3.3366338152379826E-7

Time for model checking: 1.123 seconds.

Result: 3.3366338152379826E-7 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmin=? [ F "MissionTimeUp" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=0,Objy1=3,Objx2=2,Objy2=2

Prob0E: 91 iterations in 0.43 seconds (average 0.004703, setup 0.00)

Prob1A: 3 iterations in 0.02 seconds (average 0.008000, setup 0.00)

yes = 204, no = 142430, maybe = 21471

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=164105, nc=37672, nnz=101431, k=4] [1.4 MB]
Creating vector for yes... [1.3 MB]
Allocating iteration vectors... [2 x 1.3 MB]
TOTAL: [5.1 MB]

Starting iterations...

Iterative method: 115 iterations in 0.19 seconds (average 0.001078, setup 0.06)

Value in the initial state: 6.456679974111881E-11

Time for model checking: 0.654 seconds.

Result: 6.456679974111881E-11 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmax=? [ F "Crash" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=0,Objy1=3,Objx2=2,Objy2=2

Prob0A: 3 iterations in 0.08 seconds (average 0.025333, setup 0.00)

Prob1E: 204 iterations in 2.52 seconds (average 0.012333, setup 0.00)

yes = 80379, no = 1319, maybe = 82407

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=164105, nc=139524, nnz=366065, k=4] [4.5 MB]
Creating vector for yes... [1.3 MB]
Allocating iteration vectors... [2 x 1.3 MB]
TOTAL: [8.2 MB]

Starting iterations...

Iterative method: 91 iterations in 0.44 seconds (average 0.001538, setup 0.30)

Value in the initial state: 0.018277642394567195

Time for model checking: 3.615 seconds.

Result: 0.018277642394567195 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmin=? [ F "Crash" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=0,Objy1=3,Objx2=2,Objy2=2

Prob0E: 3 iterations in 0.06 seconds (average 0.018667, setup 0.00)

Prob1A: 52 iterations in 0.49 seconds (average 0.009385, setup 0.00)

yes = 80379, no = 1319, maybe = 82407

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=164105, nc=139524, nnz=366065, k=4] [4.5 MB]
Creating vector for yes... [1.3 MB]
Allocating iteration vectors... [2 x 1.3 MB]
TOTAL: [8.2 MB]

Starting iterations...

Iterative method: 82 iterations in 0.31 seconds (average 0.001610, setup 0.18)

Value in the initial state: 0.015178211142935483

Time for model checking: 1.006 seconds.

Result: 0.015178211142935483 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmax=? [ F "AllAreaSearched" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=0,Objy1=3,Objx2=2,Objy2=2

Prob0A: 52 iterations in 0.54 seconds (average 0.010385, setup 0.00)

Prob1E: 4 iterations in 0.05 seconds (average 0.012000, setup 0.00)

yes = 240, no = 96277, maybe = 67588

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=164105, nc=113491, nnz=306852, k=2] [3.8 MB]
Creating vector for yes... [1.3 MB]
Allocating iteration vectors... [2 x 1.3 MB]
TOTAL: [7.5 MB]

Starting iterations...

Iterative method: 96 iterations in 0.37 seconds (average 0.001458, setup 0.23)

Value in the initial state: 0.25500293434632454

Time for model checking: 1.164 seconds.

Result: 0.25500293434632454 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmin=? [ F "AllAreaSearched" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=0,Objy1=3,Objx2=2,Objy2=2

Prob0E: 62 iterations in 0.49 seconds (average 0.007871, setup 0.00)

Prob1A: 3 iterations in 0.04 seconds (average 0.014667, setup 0.00)

yes = 240, no = 97636, maybe = 66229

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=164105, nc=110955, nnz=299545, k=2] [3.7 MB]
Creating vector for yes... [1.3 MB]
Allocating iteration vectors... [2 x 1.3 MB]
TOTAL: [7.4 MB]

Starting iterations...

Iterative method: 101 iterations in 0.36 seconds (average 0.001505, setup 0.21)

Value in the initial state: 0.04096496563301318

Time for model checking: 1.138 seconds.

Result: 0.04096496563301318 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmax=? [ F "MissionTimeUp"|"AllAreaSearched"|"Crash" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=0,Objy1=3,Objx2=2,Objy2=2

Prob0A: 3 iterations in 0.08 seconds (average 0.025333, setup 0.00)

Prob1E: 217 iterations in 2.78 seconds (average 0.012793, setup 0.00)

yes = 131464, no = 875, maybe = 31766

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=164105, nc=54917, nnz=150554, k=2] [1.9 MB]
Creating vector for yes... [1.3 MB]
Allocating iteration vectors... [2 x 1.3 MB]
TOTAL: [5.7 MB]

Starting iterations...

Iterative method: 84 iterations in 0.31 seconds (average 0.001143, setup 0.21)

Value in the initial state: 0.2724776019848023

Time for model checking: 3.976 seconds.

Result: 0.2724776019848023 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmin=? [ F "MissionTimeUp"|"AllAreaSearched"|"Crash" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=0,Objy1=3,Objx2=2,Objy2=2

Prob0E: 3 iterations in 0.06 seconds (average 0.021333, setup 0.00)

Prob1A: 55 iterations in 0.51 seconds (average 0.009236, setup 0.00)

yes = 123471, no = 875, maybe = 39759

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=164105, nc=68858, nnz=189450, k=2] [2.4 MB]
Creating vector for yes... [1.3 MB]
Allocating iteration vectors... [2 x 1.3 MB]
TOTAL: [6.1 MB]

Starting iterations...

Iterative method: 81 iterations in 0.27 seconds (average 0.001284, setup 0.16)

Value in the initial state: 0.05633656445519887

Time for model checking: 0.908 seconds.

Result: 0.05633656445519887 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"drop"}max=? [ F "deadlock" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=0,Objy1=3,Objx2=2,Objy2=2

Prob0E: 4 iterations in 0.06 seconds (average 0.014000, setup 0.00)

Prob1A: 1 iterations in 0.01 seconds (average 0.008000, setup 0.00)

goal = 1, inf = 0, maybe = 164104

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=164105, nc=221221, nnz=447762, k=4] [6.6 MB]
Building sparse matrix (transition rewards)... [n=164105, nc=221221, nnz=0, k=4] [1.5 MB]
Creating vector for state rewards... [1.3 MB]
Creating vector for inf... [1.3 MB]
Allocating iteration vectors... [2 x 1.3 MB]
TOTAL: [13.1 MB]

Starting iterations...

Iterative method: 88 iterations in 0.55 seconds (average 0.002409, setup 0.34)

Value in the initial state: 0.13494472158334972

Time for model checking: 0.625 seconds.

Result: 0.13494472158334972 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"drop"}min=? [ F "deadlock" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=0,Objy1=3,Objx2=2,Objy2=2

Prob0A: 4 iterations in 0.04 seconds (average 0.010000, setup 0.00)

Prob1E: 5 iterations in 0.06 seconds (average 0.012000, setup 0.00)

Warning: PRISM hasn't checked for zero-reward loops.
Your minimum rewards may be too low...

goal = 1, inf = 0, maybe = 164104

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=164105, nc=221221, nnz=447762, k=4] [6.6 MB]
Building sparse matrix (transition rewards)... [n=164105, nc=221221, nnz=0, k=4] [1.5 MB]
Creating vector for state rewards... [1.3 MB]
Creating vector for inf... [1.3 MB]
Allocating iteration vectors... [2 x 1.3 MB]
TOTAL: [13.1 MB]

Starting iterations...

Iterative method: 86 iterations in 0.46 seconds (average 0.002419, setup 0.26)

Value in the initial state: 0.12241167940977311

Time for model checking: 0.675 seconds.

Result: 0.12241167940977311 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"charge"}max=? [ F "deadlock" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=0,Objy1=3,Objx2=2,Objy2=2

Prob0E: 4 iterations in 0.03 seconds (average 0.007000, setup 0.00)

Prob1A: 1 iterations in 0.00 seconds (average 0.004000, setup 0.00)

goal = 1, inf = 0, maybe = 164104

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=164105, nc=221221, nnz=447762, k=4] [6.6 MB]
Building sparse matrix (transition rewards)... [n=164105, nc=221221, nnz=5430, k=4] [1.5 MB]
Creating vector for state rewards... [1.3 MB]
Creating vector for inf... [1.3 MB]
Allocating iteration vectors... [2 x 1.3 MB]
TOTAL: [13.1 MB]

Starting iterations...

Iterative method: 93 iterations in 0.47 seconds (average 0.002409, setup 0.25)

Value in the initial state: 0.2555458891842519

Time for model checking: 0.571 seconds.

Result: 0.2555458891842519 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"charge"}min=? [ F "deadlock" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=0,Objy1=3,Objx2=2,Objy2=2

Prob0A: 4 iterations in 0.02 seconds (average 0.006000, setup 0.00)

Prob1E: 5 iterations in 0.05 seconds (average 0.009600, setup 0.00)

Warning: PRISM hasn't checked for zero-reward loops.
Your minimum rewards may be too low...

goal = 1, inf = 0, maybe = 164104

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=164105, nc=221221, nnz=447762, k=4] [6.6 MB]
Building sparse matrix (transition rewards)... [n=164105, nc=221221, nnz=5430, k=4] [1.5 MB]
Creating vector for state rewards... [1.3 MB]
Creating vector for inf... [1.3 MB]
Allocating iteration vectors... [2 x 1.3 MB]
TOTAL: [13.1 MB]

Starting iterations...

Iterative method: 99 iterations in 0.49 seconds (average 0.002465, setup 0.24)

Value in the initial state: 0.040158095969691866

Time for model checking: 0.6 seconds.

Result: 0.040158095969691866 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"time"}max=? [ F "deadlock" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=0,Objy1=3,Objx2=2,Objy2=2

Prob0E: 4 iterations in 0.03 seconds (average 0.007000, setup 0.00)

Prob1A: 1 iterations in 0.00 seconds (average 0.004000, setup 0.00)

goal = 1, inf = 0, maybe = 164104

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=164105, nc=221221, nnz=447762, k=4] [6.6 MB]
Building sparse matrix (transition rewards)... [n=164105, nc=221221, nnz=81696, k=4] [2.4 MB]
Creating vector for state rewards... [1.3 MB]
Creating vector for inf... [1.3 MB]
Allocating iteration vectors... [2 x 1.3 MB]
TOTAL: [14.0 MB]

Starting iterations...

Iterative method: 95 iterations in 0.52 seconds (average 0.002442, setup 0.28)

Value in the initial state: 101.48190981539317

Time for model checking: 0.56 seconds.

Result: 101.48190981539317 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"time"}min=? [ F "deadlock" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=0,Objy1=3,Objx2=2,Objy2=2

Prob0A: 4 iterations in 0.03 seconds (average 0.007000, setup 0.00)

Prob1E: 5 iterations in 0.05 seconds (average 0.009600, setup 0.00)

Warning: PRISM hasn't checked for zero-reward loops.
Your minimum rewards may be too low...

goal = 1, inf = 0, maybe = 164104

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=164105, nc=221221, nnz=447762, k=4] [6.6 MB]
Building sparse matrix (transition rewards)... [n=164105, nc=221221, nnz=81696, k=4] [2.4 MB]
Creating vector for state rewards... [1.3 MB]
Creating vector for inf... [1.3 MB]
Allocating iteration vectors... [2 x 1.3 MB]
TOTAL: [14.0 MB]

Starting iterations...

Iterative method: 100 iterations in 0.52 seconds (average 0.002520, setup 0.27)

Value in the initial state: 84.2924440010842

Time for model checking: 0.615 seconds.

Result: 84.2924440010842 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"success"}max=? [ F "deadlock" ]/Pmin=? [ F "MissionSuccessful" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=0,Objy1=3,Objx2=2,Objy2=2

Prob0E: 4 iterations in 0.02 seconds (average 0.006000, setup 0.00)

Prob1A: 1 iterations in 0.00 seconds (average 0.004000, setup 0.00)

goal = 1, inf = 0, maybe = 164104

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=164105, nc=221221, nnz=447762, k=4] [6.6 MB]
Building sparse matrix (transition rewards)... [n=164105, nc=221221, nnz=1730, k=4] [1.5 MB]
Creating vector for state rewards... [1.3 MB]
Creating vector for inf... [1.3 MB]
Allocating iteration vectors... [2 x 1.3 MB]
TOTAL: [13.1 MB]

Starting iterations...

Iterative method: 99 iterations in 0.48 seconds (average 0.002384, setup 0.25)

Prob0E: 55 iterations in 0.46 seconds (average 0.008436, setup 0.00)

Prob1A: 3 iterations in 0.06 seconds (average 0.020000, setup 0.00)

yes = 874, no = 131465, maybe = 31766

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=164105, nc=54917, nnz=150554, k=2] [1.9 MB]
Creating vector for yes... [1.3 MB]
Allocating iteration vectors... [2 x 1.3 MB]
TOTAL: [5.7 MB]

Starting iterations...

Iterative method: 93 iterations in 0.25 seconds (average 0.001204, setup 0.14)

Value in the initial state: 120.8783951172294

Time for model checking: 1.332 seconds.

Result: 120.8783951172294 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"success"}min=? [ F "deadlock" ]/Pmax=? [ F "MissionSuccessful" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=0,Objy1=3,Objx2=2,Objy2=2

Prob0A: 4 iterations in 0.05 seconds (average 0.012000, setup 0.00)

Prob1E: 5 iterations in 0.05 seconds (average 0.009600, setup 0.00)

Warning: PRISM hasn't checked for zero-reward loops.
Your minimum rewards may be too low...

goal = 1, inf = 0, maybe = 164104

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=164105, nc=221221, nnz=447762, k=4] [6.6 MB]
Building sparse matrix (transition rewards)... [n=164105, nc=221221, nnz=1730, k=4] [1.5 MB]
Creating vector for state rewards... [1.3 MB]
Creating vector for inf... [1.3 MB]
Allocating iteration vectors... [2 x 1.3 MB]
TOTAL: [13.1 MB]

Starting iterations...

Iterative method: 98 iterations in 0.50 seconds (average 0.002449, setup 0.26)

Prob0A: 55 iterations in 0.41 seconds (average 0.007418, setup 0.00)

Prob1E: 4 iterations in 0.04 seconds (average 0.009000, setup 0.00)

yes = 874, no = 123472, maybe = 39759

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=164105, nc=68858, nnz=189450, k=2] [2.4 MB]
Creating vector for yes... [1.3 MB]
Allocating iteration vectors... [2 x 1.3 MB]
TOTAL: [6.1 MB]

Starting iterations...

Iterative method: 92 iterations in 0.25 seconds (average 0.001261, setup 0.14)

Value in the initial state: 67.3111947654603

Time for model checking: 1.333 seconds.

Result: 67.3111947654603 (value in the initial state)

---------------------------------------------------------------------

Note: There were 16 warnings during computation.

