PRISM
=====

Version: 4.3
Date: Wed Aug 03 15:57:21 BST 2016
Hostname: eglinton
Memory limits: cudd=1g, java(heap)=981.5m
Command line: prism uav_sim_mdp.prism -s uav_mdp.props -const 'Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=2,Objy1=1,Objx2=4,Objy2=1,'

Parsing model file "uav_sim_mdp.prism"...

Parsing properties file "uav_mdp.props"...

18 properties:
(1) Pmax=? [ F "MissionSuccessful" ]
(2) Pmin=? [ F "MissionSuccessful" ]
(3) Pmax=? [ F "MissionTimeUp" ]
(4) Pmin=? [ F "MissionTimeUp" ]
(5) Pmax=? [ F "Crash" ]
(6) Pmin=? [ F "Crash" ]
(7) Pmax=? [ F "AllAreaSearched" ]
(8) Pmin=? [ F "AllAreaSearched" ]
(9) Pmax=? [ F "MissionTimeUp"|"AllAreaSearched"|"Crash" ]
(10) Pmin=? [ F "MissionTimeUp"|"AllAreaSearched"|"Crash" ]
(11) R{"drop"}max=? [ F "deadlock" ]
(12) R{"drop"}min=? [ F "deadlock" ]
(13) R{"charge"}max=? [ F "deadlock" ]
(14) R{"charge"}min=? [ F "deadlock" ]
(15) R{"time"}max=? [ F "deadlock" ]
(16) R{"time"}min=? [ F "deadlock" ]
(17) R{"success"}max=? [ F "deadlock" ]/Pmin=? [ F "MissionSuccessful" ]
(18) R{"success"}min=? [ F "deadlock" ]/Pmax=? [ F "MissionSuccessful" ]

Type:        MDP
Modules:     Obj Move Robot T 
Variables:   NoOfObjs posx posy s1 retx rety b t c 

---------------------------------------------------------------------

Model checking: Pmax=? [ F "MissionSuccessful" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=2,Objy1=1,Objx2=4,Objy2=1

Building model...
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=2,Objy1=1,Objx2=4,Objy2=1

Warning: Guard for command 7 of module "Move" is never satisfied.

Warning: Guard for command 8 of module "Move" is never satisfied.

Warning: Guard for command 9 of module "Move" is never satisfied.

Warning: Guard for command 7 of module "Robot" is never satisfied.

Warning: Update 2 of command 3 of module "Robot" doesn't do anything ("(s1'=13)", line 210, column 44)

Warning: Update 2 of command 27 of module "T" doesn't do anything ("(c'=1)", line 323, column 108)

Warning: Guard for command 28 of module "T" is never satisfied.

Warning: Update 2 of command 9 of module "T" doesn't do anything ("(c'=1)", line 300, column 112)

Warning: Guard for command 10 of module "T" is never satisfied.

Warning: Update 2 of command 51 of module "T" doesn't do anything ("(c'=1)", line 354, column 113)

Warning: Guard for command 52 of module "T" is never satisfied.

Computing reachable states...

Reachability (BFS): 89 iterations in 1.29 seconds (average 0.014472, setup 0.00)

Time for model construction: 3.538 seconds.

Warning: Deadlocks detected and fixed in 1 states

Type:        MDP
States:      231157 (1 initial)
Transitions: 606686
Choices:     305612

Transition matrix: 209920 nodes (93 terminal), 606686 minterms, vars: 34r/34c/18nd

Prob0A: 42 iterations in 0.36 seconds (average 0.008667, setup 0.00)

Prob1E: 4 iterations in 0.06 seconds (average 0.015000, setup 0.00)

yes = 1099, no = 193811, maybe = 36247

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=231157, nc=65808, nnz=187733, k=2] [2.4 MB]
Creating vector for yes... [1.8 MB]
Allocating iteration vectors... [2 x 1.8 MB]
TOTAL: [7.7 MB]

Starting iterations...

Iterative method: 85 iterations in 0.40 seconds (average 0.001600, setup 0.26)

Value in the initial state: 0.9850423286084845

Time for model checking: 0.849 seconds.

Result: 0.9850423286084845 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmin=? [ F "MissionSuccessful" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=2,Objy1=1,Objx2=4,Objy2=1

Prob0E: 42 iterations in 0.50 seconds (average 0.011810, setup 0.00)

Prob1A: 3 iterations in 0.04 seconds (average 0.014667, setup 0.00)

yes = 1099, no = 198901, maybe = 31157

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=231157, nc=56353, nnz=160278, k=2] [2.1 MB]
Creating vector for yes... [1.8 MB]
Allocating iteration vectors... [2 x 1.8 MB]
TOTAL: [7.4 MB]

Starting iterations...

Iterative method: 83 iterations in 0.29 seconds (average 0.001494, setup 0.16)

Value in the initial state: 0.9352414032417486

Time for model checking: 0.849 seconds.

Result: 0.9352414032417486 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmax=? [ F "MissionTimeUp" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=2,Objy1=1,Objx2=4,Objy2=1

Prob0A: 72 iterations in 0.58 seconds (average 0.008111, setup 0.00)

Prob1E: 4 iterations in 0.04 seconds (average 0.010000, setup 0.00)

yes = 392, no = 148722, maybe = 82043

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=231157, nc=145000, nnz=399340, k=4] [4.9 MB]
Creating vector for yes... [1.8 MB]
Allocating iteration vectors... [2 x 1.8 MB]
TOTAL: [10.2 MB]

Starting iterations...

Iterative method: 96 iterations in 0.47 seconds (average 0.001958, setup 0.28)

Value in the initial state: 6.012737499193916E-5

Time for model checking: 1.116 seconds.

Result: 6.012737499193916E-5 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmin=? [ F "MissionTimeUp" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=2,Objy1=1,Objx2=4,Objy2=1

Prob0E: 86 iterations in 0.87 seconds (average 0.010093, setup 0.00)

Prob1A: 3 iterations in 0.05 seconds (average 0.017333, setup 0.00)

yes = 392, no = 149125, maybe = 81640

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=231157, nc=144284, nnz=397825, k=4] [4.9 MB]
Creating vector for yes... [1.8 MB]
Allocating iteration vectors... [2 x 1.8 MB]
TOTAL: [10.2 MB]

Starting iterations...

Iterative method: 113 iterations in 0.48 seconds (average 0.001982, setup 0.26)

Value in the initial state: 2.7937749045074102E-8

Time for model checking: 1.427 seconds.

Result: 2.7937749045074102E-8 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmax=? [ F "Crash" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=2,Objy1=1,Objx2=4,Objy2=1

Prob0A: 3 iterations in 0.09 seconds (average 0.030667, setup 0.00)

Prob1E: 161 iterations in 2.52 seconds (average 0.015677, setup 0.00)

yes = 113309, no = 2883, maybe = 114965

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=231157, nc=189420, nnz=490494, k=4] [6.0 MB]
Creating vector for yes... [1.8 MB]
Allocating iteration vectors... [2 x 1.8 MB]
TOTAL: [11.3 MB]

Starting iterations...

Iterative method: 86 iterations in 0.53 seconds (average 0.002140, setup 0.35)

Value in the initial state: 0.022985844048963464

Time for model checking: 3.179 seconds.

Result: 0.022985844048963464 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmin=? [ F "Crash" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=2,Objy1=1,Objx2=4,Objy2=1

Prob0E: 4 iterations in 0.08 seconds (average 0.021000, setup 0.00)

Prob1A: 43 iterations in 0.50 seconds (average 0.011628, setup 0.00)

yes = 113309, no = 2883, maybe = 114965

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=231157, nc=189420, nnz=490494, k=4] [6.0 MB]
Creating vector for yes... [1.8 MB]
Allocating iteration vectors... [2 x 1.8 MB]
TOTAL: [11.3 MB]

Starting iterations...

Iterative method: 77 iterations in 0.43 seconds (average 0.002286, setup 0.25)

Value in the initial state: 0.01291427018849788

Time for model checking: 1.035 seconds.

Result: 0.01291427018849788 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmax=? [ F "AllAreaSearched" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=2,Objy1=1,Objx2=4,Objy2=1

Prob0A: 54 iterations in 0.44 seconds (average 0.008222, setup 0.00)

Prob1E: 4 iterations in 0.06 seconds (average 0.015000, setup 0.00)

yes = 1391, no = 174092, maybe = 55674

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=231157, nc=84422, nnz=218666, k=2] [2.8 MB]
Creating vector for yes... [1.8 MB]
Allocating iteration vectors... [2 x 1.8 MB]
TOTAL: [8.1 MB]

Starting iterations...

Iterative method: 90 iterations in 0.38 seconds (average 0.001778, setup 0.22)

Value in the initial state: 0.0450284238325436

Time for model checking: 0.905 seconds.

Result: 0.0450284238325436 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmin=? [ F "AllAreaSearched" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=2,Objy1=1,Objx2=4,Objy2=1

Prob0E: 62 iterations in 0.45 seconds (average 0.007226, setup 0.00)

Prob1A: 3 iterations in 0.04 seconds (average 0.013333, setup 0.00)

yes = 1391, no = 180102, maybe = 49664

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=231157, nc=73500, nnz=186784, k=2] [2.4 MB]
Creating vector for yes... [1.8 MB]
Allocating iteration vectors... [2 x 1.8 MB]
TOTAL: [7.7 MB]

Starting iterations...

Iterative method: 94 iterations in 0.29 seconds (average 0.001660, setup 0.14)

Value in the initial state: 0.0018466128386096234

Time for model checking: 0.795 seconds.

Result: 0.0018466128386096234 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmax=? [ F "MissionTimeUp"|"AllAreaSearched"|"Crash" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=2,Objy1=1,Objx2=4,Objy2=1

Prob0A: 3 iterations in 0.10 seconds (average 0.034667, setup 0.00)

Prob1E: 160 iterations in 2.72 seconds (average 0.017025, setup 0.00)

yes = 198900, no = 1100, maybe = 31157

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=231157, nc=56353, nnz=160278, k=2] [2.1 MB]
Creating vector for yes... [1.8 MB]
Allocating iteration vectors... [2 x 1.8 MB]
TOTAL: [7.4 MB]

Starting iterations...

Iterative method: 77 iterations in 0.44 seconds (average 0.001714, setup 0.30)

Value in the initial state: 0.06475841398639774

Time for model checking: 3.3 seconds.

Result: 0.06475841398639774 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmin=? [ F "MissionTimeUp"|"AllAreaSearched"|"Crash" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=2,Objy1=1,Objx2=4,Objy2=1

Prob0E: 4 iterations in 0.07 seconds (average 0.018000, setup 0.00)

Prob1A: 42 iterations in 0.39 seconds (average 0.009333, setup 0.00)

yes = 193810, no = 1100, maybe = 36247

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=231157, nc=65808, nnz=187733, k=2] [2.4 MB]
Creating vector for yes... [1.8 MB]
Allocating iteration vectors... [2 x 1.8 MB]
TOTAL: [7.7 MB]

Starting iterations...

Iterative method: 84 iterations in 0.30 seconds (average 0.001619, setup 0.17)

Value in the initial state: 0.01495603378145358

Time for model checking: 0.771 seconds.

Result: 0.01495603378145358 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"drop"}max=? [ F "deadlock" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=2,Objy1=1,Objx2=4,Objy2=1

Prob0E: 5 iterations in 0.06 seconds (average 0.011200, setup 0.00)

Prob1A: 1 iterations in 0.01 seconds (average 0.008000, setup 0.00)

goal = 1, inf = 0, maybe = 231156

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=231157, nc=305611, nnz=606685, k=4] [9.0 MB]
Building sparse matrix (transition rewards)... [n=231157, nc=305611, nnz=0, k=4] [2.0 MB]
Creating vector for state rewards... [1.8 MB]
Creating vector for inf... [1.8 MB]
Allocating iteration vectors... [2 x 1.8 MB]
TOTAL: [18.1 MB]

Starting iterations...

Iterative method: 81 iterations in 0.70 seconds (average 0.003358, setup 0.42)

Value in the initial state: 0.17533565228336906

Time for model checking: 0.766 seconds.

Result: 0.17533565228336906 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"drop"}min=? [ F "deadlock" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=2,Objy1=1,Objx2=4,Objy2=1

Prob0A: 4 iterations in 0.05 seconds (average 0.012000, setup 0.00)

Prob1E: 5 iterations in 0.06 seconds (average 0.011200, setup 0.00)

Warning: PRISM hasn't checked for zero-reward loops.
Your minimum rewards may be too low...

goal = 1, inf = 0, maybe = 231156

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=231157, nc=305611, nnz=606685, k=4] [9.0 MB]
Building sparse matrix (transition rewards)... [n=231157, nc=305611, nnz=0, k=4] [2.0 MB]
Creating vector for state rewards... [1.8 MB]
Creating vector for inf... [1.8 MB]
Allocating iteration vectors... [2 x 1.8 MB]
TOTAL: [18.1 MB]

Starting iterations...

Iterative method: 74 iterations in 0.56 seconds (average 0.003405, setup 0.31)

Value in the initial state: 0.16261339296702287

Time for model checking: 0.689 seconds.

Result: 0.16261339296702287 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"charge"}max=? [ F "deadlock" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=2,Objy1=1,Objx2=4,Objy2=1

Prob0E: 5 iterations in 0.04 seconds (average 0.007200, setup 0.00)

Prob1A: 1 iterations in 0.00 seconds (average 0.004000, setup 0.00)

goal = 1, inf = 0, maybe = 231156

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=231157, nc=305611, nnz=606685, k=4] [9.0 MB]
Building sparse matrix (transition rewards)... [n=231157, nc=305611, nnz=6589, k=4] [2.1 MB]
Creating vector for state rewards... [1.8 MB]
Creating vector for inf... [1.8 MB]
Allocating iteration vectors... [2 x 1.8 MB]
TOTAL: [18.2 MB]

Starting iterations...

Iterative method: 82 iterations in 0.59 seconds (average 0.003366, setup 0.31)

Value in the initial state: 0.16181721169411054

Time for model checking: 0.653 seconds.

Result: 0.16181721169411054 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"charge"}min=? [ F "deadlock" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=2,Objy1=1,Objx2=4,Objy2=1

Prob0A: 4 iterations in 0.02 seconds (average 0.006000, setup 0.00)

Prob1E: 5 iterations in 0.05 seconds (average 0.010400, setup 0.00)

Warning: PRISM hasn't checked for zero-reward loops.
Your minimum rewards may be too low...

goal = 1, inf = 0, maybe = 231156

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=231157, nc=305611, nnz=606685, k=4] [9.0 MB]
Building sparse matrix (transition rewards)... [n=231157, nc=305611, nnz=6589, k=4] [2.1 MB]
Creating vector for state rewards... [1.8 MB]
Creating vector for inf... [1.8 MB]
Allocating iteration vectors... [2 x 1.8 MB]
TOTAL: [18.2 MB]

Starting iterations...

Iterative method: 92 iterations in 0.64 seconds (average 0.003435, setup 0.32)

Value in the initial state: 0.00604367144142493

Time for model checking: 0.732 seconds.

Result: 0.00604367144142493 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"time"}max=? [ F "deadlock" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=2,Objy1=1,Objx2=4,Objy2=1

Prob0E: 5 iterations in 0.04 seconds (average 0.007200, setup 0.00)

Prob1A: 1 iterations in 0.00 seconds (average 0.004000, setup 0.00)

goal = 1, inf = 0, maybe = 231156

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=231157, nc=305611, nnz=606685, k=4] [9.0 MB]
Building sparse matrix (transition rewards)... [n=231157, nc=305611, nnz=116190, k=4] [3.4 MB]
Creating vector for state rewards... [1.8 MB]
Creating vector for inf... [1.8 MB]
Allocating iteration vectors... [2 x 1.8 MB]
TOTAL: [19.4 MB]

Starting iterations...

Iterative method: 92 iterations in 0.68 seconds (average 0.003391, setup 0.37)

Value in the initial state: 127.08380573811004

Time for model checking: 0.735 seconds.

Result: 127.08380573811004 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"time"}min=? [ F "deadlock" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=2,Objy1=1,Objx2=4,Objy2=1

Prob0A: 4 iterations in 0.03 seconds (average 0.007000, setup 0.00)

Prob1E: 5 iterations in 0.05 seconds (average 0.010400, setup 0.00)

Warning: PRISM hasn't checked for zero-reward loops.
Your minimum rewards may be too low...

goal = 1, inf = 0, maybe = 231156

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=231157, nc=305611, nnz=606685, k=4] [9.0 MB]
Building sparse matrix (transition rewards)... [n=231157, nc=305611, nnz=116190, k=4] [3.4 MB]
Creating vector for state rewards... [1.8 MB]
Creating vector for inf... [1.8 MB]
Allocating iteration vectors... [2 x 1.8 MB]
TOTAL: [19.4 MB]

Starting iterations...

Iterative method: 90 iterations in 0.68 seconds (average 0.003467, setup 0.37)

Value in the initial state: 71.69521964781758

Time for model checking: 0.77 seconds.

Result: 71.69521964781758 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"success"}max=? [ F "deadlock" ]/Pmin=? [ F "MissionSuccessful" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=2,Objy1=1,Objx2=4,Objy2=1

Prob0E: 5 iterations in 0.04 seconds (average 0.007200, setup 0.00)

Prob1A: 1 iterations in 0.01 seconds (average 0.008000, setup 0.00)

goal = 1, inf = 0, maybe = 231156

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=231157, nc=305611, nnz=606685, k=4] [9.0 MB]
Building sparse matrix (transition rewards)... [n=231157, nc=305611, nnz=2166, k=4] [2.1 MB]
Creating vector for state rewards... [1.8 MB]
Creating vector for inf... [1.8 MB]
Allocating iteration vectors... [2 x 1.8 MB]
TOTAL: [18.1 MB]

Starting iterations...

Iterative method: 89 iterations in 0.63 seconds (average 0.003371, setup 0.33)

Prob0E: 42 iterations in 0.28 seconds (average 0.006667, setup 0.00)

Prob1A: 3 iterations in 0.05 seconds (average 0.017333, setup 0.00)

yes = 1099, no = 198901, maybe = 31157

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=231157, nc=56353, nnz=160278, k=2] [2.1 MB]
Creating vector for yes... [1.8 MB]
Allocating iteration vectors... [2 x 1.8 MB]
TOTAL: [7.4 MB]

Starting iterations...

Iterative method: 83 iterations in 0.28 seconds (average 0.001446, setup 0.16)

Value in the initial state: 133.52889237523823

Time for model checking: 1.319 seconds.

Result: 133.52889237523823 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"success"}min=? [ F "deadlock" ]/Pmax=? [ F "MissionSuccessful" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=2,Objy1=1,Objx2=4,Objy2=1

Prob0A: 4 iterations in 0.08 seconds (average 0.020000, setup 0.00)

Prob1E: 5 iterations in 0.06 seconds (average 0.012000, setup 0.00)

Warning: PRISM hasn't checked for zero-reward loops.
Your minimum rewards may be too low...

goal = 1, inf = 0, maybe = 231156

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=231157, nc=305611, nnz=606685, k=4] [9.0 MB]
Building sparse matrix (transition rewards)... [n=231157, nc=305611, nnz=2166, k=4] [2.1 MB]
Creating vector for state rewards... [1.8 MB]
Creating vector for inf... [1.8 MB]
Allocating iteration vectors... [2 x 1.8 MB]
TOTAL: [18.1 MB]

Starting iterations...

Iterative method: 87 iterations in 0.66 seconds (average 0.003402, setup 0.36)

Prob0A: 42 iterations in 0.27 seconds (average 0.006381, setup 0.00)

Prob1E: 4 iterations in 0.04 seconds (average 0.010000, setup 0.00)

yes = 1099, no = 193811, maybe = 36247

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=231157, nc=65808, nnz=187733, k=2] [2.4 MB]
Creating vector for yes... [1.8 MB]
Allocating iteration vectors... [2 x 1.8 MB]
TOTAL: [7.7 MB]

Starting iterations...

Iterative method: 85 iterations in 0.30 seconds (average 0.001553, setup 0.16)

Value in the initial state: 70.08951987706263

Time for model checking: 1.404 seconds.

Result: 70.08951987706263 (value in the initial state)

---------------------------------------------------------------------

Note: There were 16 warnings during computation.

