PRISM
=====

Version: 4.3
Date: Wed Aug 03 16:06:23 BST 2016
Hostname: eglinton
Memory limits: cudd=1g, java(heap)=981.5m
Command line: prism uav_sim_mdp.prism -s uav_mdp.props -const 'Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=0,Objy1=0,Objx2=2,Objy2=3,'

Parsing model file "uav_sim_mdp.prism"...

Parsing properties file "uav_mdp.props"...

18 properties:
(1) Pmax=? [ F "MissionSuccessful" ]
(2) Pmin=? [ F "MissionSuccessful" ]
(3) Pmax=? [ F "MissionTimeUp" ]
(4) Pmin=? [ F "MissionTimeUp" ]
(5) Pmax=? [ F "Crash" ]
(6) Pmin=? [ F "Crash" ]
(7) Pmax=? [ F "AllAreaSearched" ]
(8) Pmin=? [ F "AllAreaSearched" ]
(9) Pmax=? [ F "MissionTimeUp"|"AllAreaSearched"|"Crash" ]
(10) Pmin=? [ F "MissionTimeUp"|"AllAreaSearched"|"Crash" ]
(11) R{"drop"}max=? [ F "deadlock" ]
(12) R{"drop"}min=? [ F "deadlock" ]
(13) R{"charge"}max=? [ F "deadlock" ]
(14) R{"charge"}min=? [ F "deadlock" ]
(15) R{"time"}max=? [ F "deadlock" ]
(16) R{"time"}min=? [ F "deadlock" ]
(17) R{"success"}max=? [ F "deadlock" ]/Pmin=? [ F "MissionSuccessful" ]
(18) R{"success"}min=? [ F "deadlock" ]/Pmax=? [ F "MissionSuccessful" ]

Type:        MDP
Modules:     Obj Move Robot T 
Variables:   NoOfObjs posx posy s1 retx rety b t c 

---------------------------------------------------------------------

Model checking: Pmax=? [ F "MissionSuccessful" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=0,Objy1=0,Objx2=2,Objy2=3

Building model...
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=0,Objy1=0,Objx2=2,Objy2=3

Warning: Guard for command 7 of module "Move" is never satisfied.

Warning: Guard for command 8 of module "Move" is never satisfied.

Warning: Guard for command 9 of module "Move" is never satisfied.

Warning: Guard for command 7 of module "Robot" is never satisfied.

Warning: Update 2 of command 3 of module "Robot" doesn't do anything ("(s1'=13)", line 210, column 44)

Warning: Update 2 of command 27 of module "T" doesn't do anything ("(c'=1)", line 323, column 108)

Warning: Guard for command 28 of module "T" is never satisfied.

Warning: Update 2 of command 9 of module "T" doesn't do anything ("(c'=1)", line 300, column 112)

Warning: Guard for command 10 of module "T" is never satisfied.

Warning: Update 2 of command 51 of module "T" doesn't do anything ("(c'=1)", line 354, column 113)

Warning: Guard for command 52 of module "T" is never satisfied.

Computing reachable states...

Reachability (BFS): 90 iterations in 1.24 seconds (average 0.013733, setup 0.00)

Time for model construction: 4.337 seconds.

Warning: Deadlocks detected and fixed in 1 states

Type:        MDP
States:      179448 (1 initial)
Transitions: 413046
Choices:     221559

Transition matrix: 182525 nodes (91 terminal), 413046 minterms, vars: 34r/34c/18nd

Prob0A: 55 iterations in 0.25 seconds (average 0.004509, setup 0.00)

Prob1E: 4 iterations in 0.04 seconds (average 0.010000, setup 0.00)

yes = 219, no = 171816, maybe = 7413

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=179448, nc=13613, nnz=40149, k=2] [659.0 KB]
Creating vector for yes... [1.4 MB]
Allocating iteration vectors... [2 x 1.4 MB]
TOTAL: [4.8 MB]

Starting iterations...

Iterative method: 88 iterations in 0.19 seconds (average 0.001000, setup 0.10)

Value in the initial state: 3.772618114059628E-4

Time for model checking: 0.553 seconds.

Result: 3.772618114059628E-4 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmin=? [ F "MissionSuccessful" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=0,Objy1=0,Objx2=2,Objy2=3

Prob0E: 22 iterations in 0.07 seconds (average 0.003273, setup 0.00)

Prob1A: 3 iterations in 0.02 seconds (average 0.006667, setup 0.00)

yes = 219, no = 173582, maybe = 5647

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=179448, nc=10336, nnz=30157, k=2] [538.7 KB]
Creating vector for yes... [1.4 MB]
Allocating iteration vectors... [2 x 1.4 MB]
TOTAL: [4.6 MB]

Starting iterations...

Iterative method: 44 iterations in 0.07 seconds (average 0.001000, setup 0.03)

Value in the initial state: 0.0

Time for model checking: 0.264 seconds.

Result: 0.0 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmax=? [ F "MissionTimeUp" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=0,Objy1=0,Objx2=2,Objy2=3

Prob0A: 66 iterations in 0.54 seconds (average 0.008242, setup 0.00)

Prob1E: 4 iterations in 0.05 seconds (average 0.013000, setup 0.00)

yes = 436, no = 127341, maybe = 51671

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=179448, nc=86443, nnz=232058, k=4] [2.9 MB]
Creating vector for yes... [1.4 MB]
Allocating iteration vectors... [2 x 1.4 MB]
TOTAL: [7.0 MB]

Starting iterations...

Iterative method: 99 iterations in 0.36 seconds (average 0.001293, setup 0.23)

Value in the initial state: 1.1983655051213816E-5

Time for model checking: 1.167 seconds.

Result: 1.1983655051213816E-5 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmin=? [ F "MissionTimeUp" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=0,Objy1=0,Objx2=2,Objy2=3

Prob0E: 80 iterations in 0.90 seconds (average 0.011200, setup 0.00)

Prob1A: 4 iterations in 0.04 seconds (average 0.010000, setup 0.00)

yes = 436, no = 127627, maybe = 51385

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=179448, nc=85931, nnz=230948, k=4] [2.9 MB]
Creating vector for yes... [1.4 MB]
Allocating iteration vectors... [2 x 1.4 MB]
TOTAL: [7.0 MB]

Starting iterations...

Iterative method: 117 iterations in 0.35 seconds (average 0.001436, setup 0.18)

Value in the initial state: 1.1269148570989252E-8

Time for model checking: 1.611 seconds.

Result: 1.1269148570989252E-8 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmax=? [ F "Crash" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=0,Objy1=0,Objx2=2,Objy2=3

Prob0A: 4 iterations in 0.08 seconds (average 0.019000, setup 0.00)

Prob1E: 223 iterations in 2.90 seconds (average 0.012987, setup 0.00)

yes = 88106, no = 1976, maybe = 89366

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=179448, nc=131477, nnz=322964, k=4] [4.0 MB]
Creating vector for yes... [1.4 MB]
Allocating iteration vectors... [2 x 1.4 MB]
TOTAL: [8.1 MB]

Starting iterations...

Iterative method: 94 iterations in 0.42 seconds (average 0.001617, setup 0.27)

Value in the initial state: 0.014890701142109445

Time for model checking: 4.032 seconds.

Result: 0.014890701142109445 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmin=? [ F "Crash" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=0,Objy1=0,Objx2=2,Objy2=3

Prob0E: 4 iterations in 0.08 seconds (average 0.019000, setup 0.00)

Prob1A: 43 iterations in 0.52 seconds (average 0.012000, setup 0.00)

yes = 88106, no = 1976, maybe = 89366

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=179448, nc=131477, nnz=322964, k=4] [4.0 MB]
Creating vector for yes... [1.4 MB]
Allocating iteration vectors... [2 x 1.4 MB]
TOTAL: [8.1 MB]

Starting iterations...

Iterative method: 81 iterations in 0.32 seconds (average 0.001728, setup 0.18)

Value in the initial state: 0.011775542258934757

Time for model checking: 1.346 seconds.

Result: 0.011775542258934757 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmax=? [ F "AllAreaSearched" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=0,Objy1=0,Objx2=2,Objy2=3

Prob0A: 54 iterations in 0.45 seconds (average 0.008370, setup 0.00)

Prob1E: 4 iterations in 0.04 seconds (average 0.011000, setup 0.00)

yes = 1320, no = 128501, maybe = 49627

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=179448, nc=66482, nnz=158701, k=2] [2.1 MB]
Creating vector for yes... [1.4 MB]
Allocating iteration vectors... [2 x 1.4 MB]
TOTAL: [6.2 MB]

Starting iterations...

Iterative method: 86 iterations in 0.28 seconds (average 0.001302, setup 0.17)

Value in the initial state: 0.9882206848161377

Time for model checking: 0.794 seconds.

Result: 0.9882206848161377 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmin=? [ F "AllAreaSearched" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=0,Objy1=0,Objx2=2,Objy2=3

Prob0E: 58 iterations in 0.33 seconds (average 0.005724, setup 0.00)

Prob1A: 4 iterations in 0.03 seconds (average 0.008000, setup 0.00)

yes = 1320, no = 131204, maybe = 46924

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=179448, nc=61361, nnz=143515, k=2] [1.9 MB]
Creating vector for yes... [1.4 MB]
Allocating iteration vectors... [2 x 1.4 MB]
TOTAL: [6.0 MB]

Starting iterations...

Iterative method: 93 iterations in 0.23 seconds (average 0.001290, setup 0.11)

Value in the initial state: 0.9847356424272767

Time for model checking: 0.993 seconds.

Result: 0.9847356424272767 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmax=? [ F "MissionTimeUp"|"AllAreaSearched"|"Crash" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=0,Objy1=0,Objx2=2,Objy2=3

Prob0A: 4 iterations in 0.08 seconds (average 0.020000, setup 0.00)

Prob1E: 105 iterations in 1.17 seconds (average 0.011124, setup 0.00)

yes = 173581, no = 220, maybe = 5647

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=179448, nc=10336, nnz=30157, k=2] [538.7 KB]
Creating vector for yes... [1.4 MB]
Allocating iteration vectors... [2 x 1.4 MB]
TOTAL: [4.6 MB]

Starting iterations...

Iterative method: 42 iterations in 0.10 seconds (average 0.000952, setup 0.06)

Value in the initial state: 1.0

Time for model checking: 1.911 seconds.

Result: 1.0 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmin=? [ F "MissionTimeUp"|"AllAreaSearched"|"Crash" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=0,Objy1=0,Objx2=2,Objy2=3

Prob0E: 4 iterations in 0.05 seconds (average 0.012000, setup 0.00)

Prob1A: 55 iterations in 0.41 seconds (average 0.007491, setup 0.00)

yes = 171815, no = 220, maybe = 7413

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=179448, nc=13613, nnz=40149, k=2] [659.0 KB]
Creating vector for yes... [1.4 MB]
Allocating iteration vectors... [2 x 1.4 MB]
TOTAL: [4.8 MB]

Starting iterations...

Iterative method: 56 iterations in 0.12 seconds (average 0.001000, setup 0.06)

Value in the initial state: 0.9996224095287995

Time for model checking: 0.879 seconds.

Result: 0.9996224095287995 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"drop"}max=? [ F "deadlock" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=0,Objy1=0,Objx2=2,Objy2=3

Prob0E: 5 iterations in 0.05 seconds (average 0.010400, setup 0.00)

Prob1A: 1 iterations in 0.00 seconds (average 0.004000, setup 0.00)

goal = 1, inf = 0, maybe = 179447

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=179448, nc=221558, nnz=413045, k=4] [6.3 MB]
Building sparse matrix (transition rewards)... [n=179448, nc=221558, nnz=0, k=4] [1.5 MB]
Creating vector for state rewards... [1.4 MB]
Creating vector for inf... [1.4 MB]
Allocating iteration vectors... [2 x 1.4 MB]
TOTAL: [13.3 MB]

Starting iterations...

Iterative method: 62 iterations in 0.48 seconds (average 0.002516, setup 0.32)

Value in the initial state: 0.11013925638558711

Time for model checking: 0.692 seconds.

Result: 0.11013925638558711 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"drop"}min=? [ F "deadlock" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=0,Objy1=0,Objx2=2,Objy2=3

Prob0A: 5 iterations in 0.05 seconds (average 0.010400, setup 0.00)

Prob1E: 6 iterations in 0.06 seconds (average 0.010000, setup 0.00)

Warning: PRISM hasn't checked for zero-reward loops.
Your minimum rewards may be too low...

goal = 1, inf = 0, maybe = 179447

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=179448, nc=221558, nnz=413045, k=4] [6.3 MB]
Building sparse matrix (transition rewards)... [n=179448, nc=221558, nnz=0, k=4] [1.5 MB]
Creating vector for state rewards... [1.4 MB]
Creating vector for inf... [1.4 MB]
Allocating iteration vectors... [2 x 1.4 MB]
TOTAL: [13.3 MB]

Starting iterations...

Iterative method: 54 iterations in 0.38 seconds (average 0.002519, setup 0.24)

Value in the initial state: 0.10998751751574319

Time for model checking: 0.566 seconds.

Result: 0.10998751751574319 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"charge"}max=? [ F "deadlock" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=0,Objy1=0,Objx2=2,Objy2=3

Prob0E: 5 iterations in 0.03 seconds (average 0.006400, setup 0.00)

Prob1A: 1 iterations in 0.00 seconds (average 0.004000, setup 0.00)

goal = 1, inf = 0, maybe = 179447

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=179448, nc=221558, nnz=413045, k=4] [6.3 MB]
Building sparse matrix (transition rewards)... [n=179448, nc=221558, nnz=3375, k=4] [1.6 MB]
Creating vector for state rewards... [1.4 MB]
Creating vector for inf... [1.4 MB]
Allocating iteration vectors... [2 x 1.4 MB]
TOTAL: [13.3 MB]

Starting iterations...

Iterative method: 77 iterations in 0.44 seconds (average 0.002545, setup 0.24)

Value in the initial state: 9.915422930473002E-4

Time for model checking: 0.566 seconds.

Result: 9.915422930473002E-4 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"charge"}min=? [ F "deadlock" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=0,Objy1=0,Objx2=2,Objy2=3

Prob0A: 5 iterations in 0.03 seconds (average 0.006400, setup 0.00)

Prob1E: 6 iterations in 0.06 seconds (average 0.010667, setup 0.00)

Warning: PRISM hasn't checked for zero-reward loops.
Your minimum rewards may be too low...

goal = 1, inf = 0, maybe = 179447

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=179448, nc=221558, nnz=413045, k=4] [6.3 MB]
Building sparse matrix (transition rewards)... [n=179448, nc=221558, nnz=3375, k=4] [1.6 MB]
Creating vector for state rewards... [1.4 MB]
Creating vector for inf... [1.4 MB]
Allocating iteration vectors... [2 x 1.4 MB]
TOTAL: [13.3 MB]

Starting iterations...

Iterative method: 89 iterations in 0.46 seconds (average 0.002427, setup 0.24)

Value in the initial state: 3.7691265491348526E-5

Time for model checking: 0.855 seconds.

Result: 3.7691265491348526E-5 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"time"}max=? [ F "deadlock" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=0,Objy1=0,Objx2=2,Objy2=3

Prob0E: 5 iterations in 0.04 seconds (average 0.007200, setup 0.00)

Prob1A: 1 iterations in 0.01 seconds (average 0.008000, setup 0.00)

goal = 1, inf = 0, maybe = 179447

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=179448, nc=221558, nnz=413045, k=4] [6.3 MB]
Building sparse matrix (transition rewards)... [n=179448, nc=221558, nnz=90080, k=4] [2.6 MB]
Creating vector for state rewards... [1.4 MB]
Creating vector for inf... [1.4 MB]
Allocating iteration vectors... [2 x 1.4 MB]
TOTAL: [14.3 MB]

Starting iterations...

Iterative method: 98 iterations in 0.52 seconds (average 0.002490, setup 0.28)

Value in the initial state: 82.60875721433139

Time for model checking: 0.701 seconds.

Result: 82.60875721433139 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"time"}min=? [ F "deadlock" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=0,Objy1=0,Objx2=2,Objy2=3

Prob0A: 5 iterations in 0.03 seconds (average 0.005600, setup 0.00)

Prob1E: 6 iterations in 0.06 seconds (average 0.010000, setup 0.00)

Warning: PRISM hasn't checked for zero-reward loops.
Your minimum rewards may be too low...

goal = 1, inf = 0, maybe = 179447

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=179448, nc=221558, nnz=413045, k=4] [6.3 MB]
Building sparse matrix (transition rewards)... [n=179448, nc=221558, nnz=90080, k=4] [2.6 MB]
Creating vector for state rewards... [1.4 MB]
Creating vector for inf... [1.4 MB]
Allocating iteration vectors... [2 x 1.4 MB]
TOTAL: [14.3 MB]

Starting iterations...

Iterative method: 98 iterations in 0.54 seconds (average 0.002571, setup 0.28)

Value in the initial state: 65.39128799546815

Time for model checking: 0.932 seconds.

Result: 65.39128799546815 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"success"}max=? [ F "deadlock" ]/Pmin=? [ F "MissionSuccessful" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=0,Objy1=0,Objx2=2,Objy2=3

Prob0E: 5 iterations in 0.04 seconds (average 0.007200, setup 0.00)

Prob1A: 1 iterations in 0.00 seconds (average 0.004000, setup 0.00)

goal = 1, inf = 0, maybe = 179447

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=179448, nc=221558, nnz=413045, k=4] [6.3 MB]
Building sparse matrix (transition rewards)... [n=179448, nc=221558, nnz=432, k=4] [1.5 MB]
Creating vector for state rewards... [1.4 MB]
Creating vector for inf... [1.4 MB]
Allocating iteration vectors... [2 x 1.4 MB]
TOTAL: [13.3 MB]

Starting iterations...

Iterative method: 85 iterations in 0.46 seconds (average 0.002447, setup 0.25)

Prob0E: 22 iterations in 0.06 seconds (average 0.002909, setup 0.00)

Prob1A: 3 iterations in 0.02 seconds (average 0.006667, setup 0.00)

yes = 219, no = 173582, maybe = 5647

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=179448, nc=10336, nnz=30157, k=2] [538.7 KB]
Creating vector for yes... [1.4 MB]
Allocating iteration vectors... [2 x 1.4 MB]
TOTAL: [4.6 MB]

Starting iterations...

Iterative method: 44 iterations in 0.07 seconds (average 0.000909, setup 0.03)

Value in the initial state: Infinity

Time for model checking: 0.9 seconds.

Result: Infinity (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"success"}min=? [ F "deadlock" ]/Pmax=? [ F "MissionSuccessful" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=0,Objy1=0,Objx2=2,Objy2=3

Prob0A: 5 iterations in 0.04 seconds (average 0.007200, setup 0.00)

Prob1E: 6 iterations in 0.07 seconds (average 0.011333, setup 0.00)

Warning: PRISM hasn't checked for zero-reward loops.
Your minimum rewards may be too low...

goal = 1, inf = 0, maybe = 179447

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=179448, nc=221558, nnz=413045, k=4] [6.3 MB]
Building sparse matrix (transition rewards)... [n=179448, nc=221558, nnz=432, k=4] [1.5 MB]
Creating vector for state rewards... [1.4 MB]
Creating vector for inf... [1.4 MB]
Allocating iteration vectors... [2 x 1.4 MB]
TOTAL: [13.3 MB]

Starting iterations...

Iterative method: 50 iterations in 0.38 seconds (average 0.002560, setup 0.25)

Prob0A: 55 iterations in 0.23 seconds (average 0.004218, setup 0.00)

Prob1E: 4 iterations in 0.02 seconds (average 0.006000, setup 0.00)

yes = 219, no = 171816, maybe = 7413

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=179448, nc=13613, nnz=40149, k=2] [659.0 KB]
Creating vector for yes... [1.4 MB]
Allocating iteration vectors... [2 x 1.4 MB]
TOTAL: [4.8 MB]

Starting iterations...

Iterative method: 88 iterations in 0.14 seconds (average 0.001045, setup 0.05)

Value in the initial state: 0.0

Time for model checking: 1.089 seconds.

Result: 0.0 (value in the initial state)

---------------------------------------------------------------------

Note: There were 16 warnings during computation.

