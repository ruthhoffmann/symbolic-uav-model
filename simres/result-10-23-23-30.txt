PRISM
=====

Version: 4.3
Date: Wed Aug 03 16:12:46 BST 2016
Hostname: eglinton
Memory limits: cudd=1g, java(heap)=981.5m
Command line: prism uav_sim_mdp.prism -s uav_mdp.props -const 'Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=2,Objy1=3,Objx2=3,Objy2=0,'

Parsing model file "uav_sim_mdp.prism"...

Parsing properties file "uav_mdp.props"...

18 properties:
(1) Pmax=? [ F "MissionSuccessful" ]
(2) Pmin=? [ F "MissionSuccessful" ]
(3) Pmax=? [ F "MissionTimeUp" ]
(4) Pmin=? [ F "MissionTimeUp" ]
(5) Pmax=? [ F "Crash" ]
(6) Pmin=? [ F "Crash" ]
(7) Pmax=? [ F "AllAreaSearched" ]
(8) Pmin=? [ F "AllAreaSearched" ]
(9) Pmax=? [ F "MissionTimeUp"|"AllAreaSearched"|"Crash" ]
(10) Pmin=? [ F "MissionTimeUp"|"AllAreaSearched"|"Crash" ]
(11) R{"drop"}max=? [ F "deadlock" ]
(12) R{"drop"}min=? [ F "deadlock" ]
(13) R{"charge"}max=? [ F "deadlock" ]
(14) R{"charge"}min=? [ F "deadlock" ]
(15) R{"time"}max=? [ F "deadlock" ]
(16) R{"time"}min=? [ F "deadlock" ]
(17) R{"success"}max=? [ F "deadlock" ]/Pmin=? [ F "MissionSuccessful" ]
(18) R{"success"}min=? [ F "deadlock" ]/Pmax=? [ F "MissionSuccessful" ]

Type:        MDP
Modules:     Obj Move Robot T 
Variables:   NoOfObjs posx posy s1 retx rety b t c 

---------------------------------------------------------------------

Model checking: Pmax=? [ F "MissionSuccessful" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=2,Objy1=3,Objx2=3,Objy2=0

Building model...
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=2,Objy1=3,Objx2=3,Objy2=0

Warning: Guard for command 7 of module "Move" is never satisfied.

Warning: Guard for command 8 of module "Move" is never satisfied.

Warning: Guard for command 9 of module "Move" is never satisfied.

Warning: Guard for command 7 of module "Robot" is never satisfied.

Warning: Update 2 of command 3 of module "Robot" doesn't do anything ("(s1'=13)", line 210, column 44)

Warning: Update 2 of command 27 of module "T" doesn't do anything ("(c'=1)", line 323, column 108)

Warning: Guard for command 28 of module "T" is never satisfied.

Warning: Update 2 of command 9 of module "T" doesn't do anything ("(c'=1)", line 300, column 112)

Warning: Guard for command 10 of module "T" is never satisfied.

Warning: Update 2 of command 51 of module "T" doesn't do anything ("(c'=1)", line 354, column 113)

Warning: Guard for command 52 of module "T" is never satisfied.

Computing reachable states...

Reachability (BFS): 90 iterations in 1.12 seconds (average 0.012489, setup 0.00)

Time for model construction: 3.031 seconds.

Warning: Deadlocks detected and fixed in 1 states

Type:        MDP
States:      169465 (1 initial)
Transitions: 395713
Choices:     210706

Transition matrix: 182963 nodes (89 terminal), 395713 minterms, vars: 34r/34c/18nd

Prob0A: 57 iterations in 0.31 seconds (average 0.005474, setup 0.00)

Prob1E: 4 iterations in 0.04 seconds (average 0.011000, setup 0.00)

yes = 216, no = 162013, maybe = 7236

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=169465, nc=13277, nnz=39132, k=2] [637.0 KB]
Creating vector for yes... [1.3 MB]
Allocating iteration vectors... [2 x 1.3 MB]
TOTAL: [4.5 MB]

Starting iterations...

Iterative method: 90 iterations in 0.19 seconds (average 0.000933, setup 0.11)

Value in the initial state: 4.819626599430695E-4

Time for model checking: 0.59 seconds.

Result: 4.819626599430695E-4 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmin=? [ F "MissionSuccessful" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=2,Objy1=3,Objx2=3,Objy2=0

Prob0E: 22 iterations in 0.07 seconds (average 0.003273, setup 0.00)

Prob1A: 3 iterations in 0.02 seconds (average 0.006667, setup 0.00)

yes = 216, no = 163704, maybe = 5545

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=169465, nc=10147, nnz=29602, k=2] [522.3 KB]
Creating vector for yes... [1.3 MB]
Allocating iteration vectors... [2 x 1.3 MB]
TOTAL: [4.4 MB]

Starting iterations...

Iterative method: 43 iterations in 0.07 seconds (average 0.000930, setup 0.03)

Value in the initial state: 0.0

Time for model checking: 0.175 seconds.

Result: 0.0 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmax=? [ F "MissionTimeUp" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=2,Objy1=3,Objx2=3,Objy2=0

Prob0A: 67 iterations in 0.53 seconds (average 0.007881, setup 0.00)

Prob1E: 4 iterations in 0.04 seconds (average 0.011000, setup 0.00)

yes = 400, no = 120427, maybe = 48638

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=169465, nc=82465, nnz=223286, k=4] [2.8 MB]
Creating vector for yes... [1.3 MB]
Allocating iteration vectors... [2 x 1.3 MB]
TOTAL: [6.7 MB]

Starting iterations...

Iterative method: 98 iterations in 0.33 seconds (average 0.001265, setup 0.20)

Value in the initial state: 1.3589126912702208E-5

Time for model checking: 0.927 seconds.

Result: 1.3589126912702208E-5 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmin=? [ F "MissionTimeUp" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=2,Objy1=3,Objx2=3,Objy2=0

Prob0E: 82 iterations in 0.70 seconds (average 0.008488, setup 0.00)

Prob1A: 3 iterations in 0.05 seconds (average 0.017333, setup 0.00)

yes = 400, no = 120713, maybe = 48352

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=169465, nc=81953, nnz=222176, k=4] [2.8 MB]
Creating vector for yes... [1.3 MB]
Allocating iteration vectors... [2 x 1.3 MB]
TOTAL: [6.7 MB]

Starting iterations...

Iterative method: 116 iterations in 0.44 seconds (average 0.001310, setup 0.28)

Value in the initial state: 1.3992453596476174E-8

Time for model checking: 1.211 seconds.

Result: 1.3992453596476174E-8 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmax=? [ F "Crash" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=2,Objy1=3,Objx2=3,Objy2=0

Prob0A: 3 iterations in 0.08 seconds (average 0.026667, setup 0.00)

Prob1E: 179 iterations in 2.08 seconds (average 0.011598, setup 0.00)

yes = 83194, no = 1967, maybe = 84304

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=169465, nc=125545, nnz=310552, k=4] [3.8 MB]
Creating vector for yes... [1.3 MB]
Allocating iteration vectors... [2 x 1.3 MB]
TOTAL: [7.7 MB]

Starting iterations...

Iterative method: 93 iterations in 0.39 seconds (average 0.001505, setup 0.25)

Value in the initial state: 0.014891515061846015

Time for model checking: 2.583 seconds.

Result: 0.014891515061846015 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmin=? [ F "Crash" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=2,Objy1=3,Objx2=3,Objy2=0

Prob0E: 3 iterations in 0.05 seconds (average 0.017333, setup 0.00)

Prob1A: 45 iterations in 0.48 seconds (average 0.010667, setup 0.00)

yes = 83194, no = 1967, maybe = 84304

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=169465, nc=125545, nnz=310552, k=4] [3.8 MB]
Creating vector for yes... [1.3 MB]
Allocating iteration vectors... [2 x 1.3 MB]
TOTAL: [7.7 MB]

Starting iterations...

Iterative method: 80 iterations in 0.28 seconds (average 0.001550, setup 0.16)

Value in the initial state: 0.01177574111694195

Time for model checking: 0.83 seconds.

Result: 0.01177574111694195 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmax=? [ F "AllAreaSearched" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=2,Objy1=3,Objx2=3,Objy2=0

Prob0A: 51 iterations in 0.41 seconds (average 0.008000, setup 0.00)

Prob1E: 4 iterations in 0.05 seconds (average 0.012000, setup 0.00)

yes = 1350, no = 119740, maybe = 48375

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=169465, nc=65965, nnz=159444, k=2] [2.0 MB]
Creating vector for yes... [1.3 MB]
Allocating iteration vectors... [2 x 1.3 MB]
TOTAL: [5.9 MB]

Starting iterations...

Iterative method: 88 iterations in 0.28 seconds (average 0.001182, setup 0.17)

Value in the initial state: 0.9882222307112443

Time for model checking: 0.749 seconds.

Result: 0.9882222307112443 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmin=? [ F "AllAreaSearched" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=2,Objy1=3,Objx2=3,Objy2=0

Prob0E: 55 iterations in 0.42 seconds (average 0.007636, setup 0.00)

Prob1A: 3 iterations in 0.04 seconds (average 0.014667, setup 0.00)

yes = 1350, no = 122443, maybe = 45672

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=169465, nc=60844, nnz=144258, k=2] [1.9 MB]
Creating vector for yes... [1.3 MB]
Allocating iteration vectors... [2 x 1.3 MB]
TOTAL: [5.7 MB]

Starting iterations...

Iterative method: 93 iterations in 0.26 seconds (average 0.001204, setup 0.14)

Value in the initial state: 0.9846329274048843

Time for model checking: 0.726 seconds.

Result: 0.9846329274048843 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmax=? [ F "MissionTimeUp"|"AllAreaSearched"|"Crash" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=2,Objy1=3,Objx2=3,Objy2=0

Prob0A: 3 iterations in 0.07 seconds (average 0.024000, setup 0.00)

Prob1E: 84 iterations in 0.80 seconds (average 0.009476, setup 0.00)

yes = 163703, no = 217, maybe = 5545

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=169465, nc=10147, nnz=29602, k=2] [522.3 KB]
Creating vector for yes... [1.3 MB]
Allocating iteration vectors... [2 x 1.3 MB]
TOTAL: [4.4 MB]

Starting iterations...

Iterative method: 41 iterations in 0.09 seconds (average 0.000878, setup 0.06)

Value in the initial state: 1.0

Time for model checking: 0.968 seconds.

Result: 1.0 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmin=? [ F "MissionTimeUp"|"AllAreaSearched"|"Crash" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=2,Objy1=3,Objx2=3,Objy2=0

Prob0E: 3 iterations in 0.02 seconds (average 0.008000, setup 0.00)

Prob1A: 57 iterations in 0.35 seconds (average 0.006105, setup 0.00)

yes = 162012, no = 217, maybe = 7236

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=169465, nc=13277, nnz=39132, k=2] [637.0 KB]
Creating vector for yes... [1.3 MB]
Allocating iteration vectors... [2 x 1.3 MB]
TOTAL: [4.5 MB]

Starting iterations...

Iterative method: 56 iterations in 0.12 seconds (average 0.001000, setup 0.06)

Value in the initial state: 0.9995171351122214

Time for model checking: 0.502 seconds.

Result: 0.9995171351122214 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"drop"}max=? [ F "deadlock" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=2,Objy1=3,Objx2=3,Objy2=0

Prob0E: 4 iterations in 0.04 seconds (average 0.009000, setup 0.00)

Prob1A: 1 iterations in 0.00 seconds (average 0.004000, setup 0.00)

goal = 1, inf = 0, maybe = 169464

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=169465, nc=210705, nnz=395712, k=4] [6.0 MB]
Building sparse matrix (transition rewards)... [n=169465, nc=210705, nnz=0, k=4] [1.5 MB]
Creating vector for state rewards... [1.3 MB]
Creating vector for inf... [1.3 MB]
Allocating iteration vectors... [2 x 1.3 MB]
TOTAL: [12.6 MB]

Starting iterations...

Iterative method: 65 iterations in 0.44 seconds (average 0.002277, setup 0.29)

Value in the initial state: 0.11009046525691153

Time for model checking: 0.505 seconds.

Result: 0.11009046525691153 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"drop"}min=? [ F "deadlock" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=2,Objy1=3,Objx2=3,Objy2=0

Prob0A: 4 iterations in 0.02 seconds (average 0.006000, setup 0.00)

Prob1E: 5 iterations in 0.04 seconds (average 0.007200, setup 0.00)

Warning: PRISM hasn't checked for zero-reward loops.
Your minimum rewards may be too low...

goal = 1, inf = 0, maybe = 169464

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=169465, nc=210705, nnz=395712, k=4] [6.0 MB]
Building sparse matrix (transition rewards)... [n=169465, nc=210705, nnz=0, k=4] [1.5 MB]
Creating vector for state rewards... [1.3 MB]
Creating vector for inf... [1.3 MB]
Allocating iteration vectors... [2 x 1.3 MB]
TOTAL: [12.6 MB]

Starting iterations...

Iterative method: 57 iterations in 0.36 seconds (average 0.002386, setup 0.22)

Value in the initial state: 0.10992521063557575

Time for model checking: 0.424 seconds.

Result: 0.10992521063557575 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"charge"}max=? [ F "deadlock" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=2,Objy1=3,Objx2=3,Objy2=0

Prob0E: 4 iterations in 0.02 seconds (average 0.005000, setup 0.00)

Prob1A: 1 iterations in 0.00 seconds (average 0.004000, setup 0.00)

goal = 1, inf = 0, maybe = 169464

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=169465, nc=210705, nnz=395712, k=4] [6.0 MB]
Building sparse matrix (transition rewards)... [n=169465, nc=210705, nnz=3375, k=4] [1.5 MB]
Creating vector for state rewards... [1.3 MB]
Creating vector for inf... [1.3 MB]
Allocating iteration vectors... [2 x 1.3 MB]
TOTAL: [12.6 MB]

Starting iterations...

Iterative method: 79 iterations in 0.41 seconds (average 0.002278, setup 0.23)

Value in the initial state: 0.0014400432329116858

Time for model checking: 0.441 seconds.

Result: 0.0014400432329116858 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"charge"}min=? [ F "deadlock" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=2,Objy1=3,Objx2=3,Objy2=0

Prob0A: 4 iterations in 0.02 seconds (average 0.005000, setup 0.00)

Prob1E: 5 iterations in 0.03 seconds (average 0.006400, setup 0.00)

Warning: PRISM hasn't checked for zero-reward loops.
Your minimum rewards may be too low...

goal = 1, inf = 0, maybe = 169464

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=169465, nc=210705, nnz=395712, k=4] [6.0 MB]
Building sparse matrix (transition rewards)... [n=169465, nc=210705, nnz=3375, k=4] [1.5 MB]
Creating vector for state rewards... [1.3 MB]
Creating vector for inf... [1.3 MB]
Allocating iteration vectors... [2 x 1.3 MB]
TOTAL: [12.6 MB]

Starting iterations...

Iterative method: 91 iterations in 0.43 seconds (average 0.002330, setup 0.22)

Value in the initial state: 5.937872863839176E-5

Time for model checking: 0.498 seconds.

Result: 5.937872863839176E-5 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"time"}max=? [ F "deadlock" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=2,Objy1=3,Objx2=3,Objy2=0

Prob0E: 4 iterations in 0.02 seconds (average 0.004000, setup 0.00)

Prob1A: 1 iterations in 0.00 seconds (average 0.004000, setup 0.00)

goal = 1, inf = 0, maybe = 169464

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=169465, nc=210705, nnz=395712, k=4] [6.0 MB]
Building sparse matrix (transition rewards)... [n=169465, nc=210705, nnz=85159, k=4] [2.4 MB]
Creating vector for state rewards... [1.3 MB]
Creating vector for inf... [1.3 MB]
Allocating iteration vectors... [2 x 1.3 MB]
TOTAL: [13.6 MB]

Starting iterations...

Iterative method: 97 iterations in 0.46 seconds (average 0.002268, setup 0.24)

Value in the initial state: 82.61316840434655

Time for model checking: 0.515 seconds.

Result: 82.61316840434655 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"time"}min=? [ F "deadlock" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=2,Objy1=3,Objx2=3,Objy2=0

Prob0A: 4 iterations in 0.02 seconds (average 0.006000, setup 0.00)

Prob1E: 5 iterations in 0.04 seconds (average 0.007200, setup 0.00)

Warning: PRISM hasn't checked for zero-reward loops.
Your minimum rewards may be too low...

goal = 1, inf = 0, maybe = 169464

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=169465, nc=210705, nnz=395712, k=4] [6.0 MB]
Building sparse matrix (transition rewards)... [n=169465, nc=210705, nnz=85159, k=4] [2.4 MB]
Creating vector for state rewards... [1.3 MB]
Creating vector for inf... [1.3 MB]
Allocating iteration vectors... [2 x 1.3 MB]
TOTAL: [13.6 MB]

Starting iterations...

Iterative method: 97 iterations in 0.60 seconds (average 0.003423, setup 0.27)

Value in the initial state: 65.39238640483988

Time for model checking: 0.681 seconds.

Result: 65.39238640483988 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"success"}max=? [ F "deadlock" ]/Pmin=? [ F "MissionSuccessful" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=2,Objy1=3,Objx2=3,Objy2=0

Prob0E: 4 iterations in 0.04 seconds (average 0.011000, setup 0.00)

Prob1A: 1 iterations in 0.01 seconds (average 0.008000, setup 0.00)

goal = 1, inf = 0, maybe = 169464

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=169465, nc=210705, nnz=395712, k=4] [6.0 MB]
Building sparse matrix (transition rewards)... [n=169465, nc=210705, nnz=426, k=4] [1.5 MB]
Creating vector for state rewards... [1.3 MB]
Creating vector for inf... [1.3 MB]
Allocating iteration vectors... [2 x 1.3 MB]
TOTAL: [12.6 MB]

Starting iterations...

Iterative method: 87 iterations in 0.49 seconds (average 0.002391, setup 0.28)

Prob0E: 22 iterations in 0.05 seconds (average 0.002364, setup 0.00)

Prob1A: 3 iterations in 0.01 seconds (average 0.004000, setup 0.00)

yes = 216, no = 163704, maybe = 5545

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=169465, nc=10147, nnz=29602, k=2] [522.3 KB]
Creating vector for yes... [1.3 MB]
Allocating iteration vectors... [2 x 1.3 MB]
TOTAL: [4.4 MB]

Starting iterations...

Iterative method: 43 iterations in 0.06 seconds (average 0.000930, setup 0.02)

Value in the initial state: Infinity

Time for model checking: 0.703 seconds.

Result: Infinity (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"success"}min=? [ F "deadlock" ]/Pmax=? [ F "MissionSuccessful" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=2,Objy1=3,Objx2=3,Objy2=0

Prob0A: 4 iterations in 0.03 seconds (average 0.008000, setup 0.00)

Prob1E: 5 iterations in 0.03 seconds (average 0.006400, setup 0.00)

Warning: PRISM hasn't checked for zero-reward loops.
Your minimum rewards may be too low...

goal = 1, inf = 0, maybe = 169464

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=169465, nc=210705, nnz=395712, k=4] [6.0 MB]
Building sparse matrix (transition rewards)... [n=169465, nc=210705, nnz=426, k=4] [1.5 MB]
Creating vector for state rewards... [1.3 MB]
Creating vector for inf... [1.3 MB]
Allocating iteration vectors... [2 x 1.3 MB]
TOTAL: [12.6 MB]

Starting iterations...

Iterative method: 49 iterations in 0.31 seconds (average 0.002204, setup 0.20)

Prob0A: 57 iterations in 0.18 seconds (average 0.003158, setup 0.00)

Prob1E: 4 iterations in 0.02 seconds (average 0.005000, setup 0.00)

yes = 216, no = 162013, maybe = 7236

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=169465, nc=13277, nnz=39132, k=2] [637.0 KB]
Creating vector for yes... [1.3 MB]
Allocating iteration vectors... [2 x 1.3 MB]
TOTAL: [4.5 MB]

Starting iterations...

Iterative method: 90 iterations in 0.13 seconds (average 0.000933, setup 0.05)

Value in the initial state: 0.0

Time for model checking: 0.718 seconds.

Result: 0.0 (value in the initial state)

---------------------------------------------------------------------

Note: There were 16 warnings during computation.

