PRISM
=====

Version: 4.3
Date: Wed Aug 03 16:15:53 BST 2016
Hostname: eglinton
Memory limits: cudd=1g, java(heap)=981.5m
Command line: prism uav_sim_mdp.prism -s uav_mdp.props -const 'Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=4,Objy1=1,Objx2=6,Objy2=2,'

Parsing model file "uav_sim_mdp.prism"...

Parsing properties file "uav_mdp.props"...

18 properties:
(1) Pmax=? [ F "MissionSuccessful" ]
(2) Pmin=? [ F "MissionSuccessful" ]
(3) Pmax=? [ F "MissionTimeUp" ]
(4) Pmin=? [ F "MissionTimeUp" ]
(5) Pmax=? [ F "Crash" ]
(6) Pmin=? [ F "Crash" ]
(7) Pmax=? [ F "AllAreaSearched" ]
(8) Pmin=? [ F "AllAreaSearched" ]
(9) Pmax=? [ F "MissionTimeUp"|"AllAreaSearched"|"Crash" ]
(10) Pmin=? [ F "MissionTimeUp"|"AllAreaSearched"|"Crash" ]
(11) R{"drop"}max=? [ F "deadlock" ]
(12) R{"drop"}min=? [ F "deadlock" ]
(13) R{"charge"}max=? [ F "deadlock" ]
(14) R{"charge"}min=? [ F "deadlock" ]
(15) R{"time"}max=? [ F "deadlock" ]
(16) R{"time"}min=? [ F "deadlock" ]
(17) R{"success"}max=? [ F "deadlock" ]/Pmin=? [ F "MissionSuccessful" ]
(18) R{"success"}min=? [ F "deadlock" ]/Pmax=? [ F "MissionSuccessful" ]

Type:        MDP
Modules:     Obj Move Robot T 
Variables:   NoOfObjs posx posy s1 retx rety b t c 

---------------------------------------------------------------------

Model checking: Pmax=? [ F "MissionSuccessful" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=4,Objy1=1,Objx2=6,Objy2=2

Building model...
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=4,Objy1=1,Objx2=6,Objy2=2

Warning: Guard for command 7 of module "Move" is never satisfied.

Warning: Guard for command 8 of module "Move" is never satisfied.

Warning: Guard for command 9 of module "Move" is never satisfied.

Warning: Guard for command 7 of module "Robot" is never satisfied.

Warning: Update 2 of command 3 of module "Robot" doesn't do anything ("(s1'=13)", line 210, column 44)

Warning: Update 2 of command 27 of module "T" doesn't do anything ("(c'=1)", line 323, column 108)

Warning: Guard for command 28 of module "T" is never satisfied.

Warning: Update 2 of command 9 of module "T" doesn't do anything ("(c'=1)", line 300, column 112)

Warning: Guard for command 10 of module "T" is never satisfied.

Warning: Update 2 of command 51 of module "T" doesn't do anything ("(c'=1)", line 354, column 113)

Warning: Guard for command 52 of module "T" is never satisfied.

Computing reachable states...

Reachability (BFS): 88 iterations in 1.20 seconds (average 0.013591, setup 0.00)

Time for model construction: 2.905 seconds.

Warning: Deadlocks detected and fixed in 1 states

Type:        MDP
States:      305922 (1 initial)
Transitions: 840604
Choices:     410684

Transition matrix: 223014 nodes (93 terminal), 840604 minterms, vars: 34r/34c/18nd

Prob0A: 52 iterations in 0.43 seconds (average 0.008231, setup 0.00)

Prob1E: 4 iterations in 0.06 seconds (average 0.014000, setup 0.00)

yes = 1192, no = 255648, maybe = 49082

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=305922, nc=86037, nnz=247691, k=2] [3.2 MB]
Creating vector for yes... [2.3 MB]
Allocating iteration vectors... [2 x 2.3 MB]
TOTAL: [10.2 MB]

Starting iterations...

Iterative method: 102 iterations in 0.48 seconds (average 0.002118, setup 0.27)

Value in the initial state: 0.9805928265108923

Time for model checking: 0.961 seconds.

Result: 0.9805928265108923 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmin=? [ F "MissionSuccessful" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=4,Objy1=1,Objx2=6,Objy2=2

Prob0E: 54 iterations in 0.46 seconds (average 0.008444, setup 0.00)

Prob1A: 3 iterations in 0.05 seconds (average 0.017333, setup 0.00)

yes = 1192, no = 262151, maybe = 42579

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=305922, nc=74437, nnz=214392, k=2] [2.8 MB]
Creating vector for yes... [2.3 MB]
Allocating iteration vectors... [2 x 2.3 MB]
TOTAL: [9.8 MB]

Starting iterations...

Iterative method: 92 iterations in 0.39 seconds (average 0.002000, setup 0.21)

Value in the initial state: 0.8009614639679241

Time for model checking: 0.929 seconds.

Result: 0.8009614639679241 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmax=? [ F "MissionTimeUp" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=4,Objy1=1,Objx2=6,Objy2=2

Prob0A: 72 iterations in 0.65 seconds (average 0.009000, setup 0.00)

Prob1E: 4 iterations in 0.05 seconds (average 0.012000, setup 0.00)

yes = 448, no = 177903, maybe = 127571

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=305922, nc=219175, nnz=607205, k=4] [7.4 MB]
Creating vector for yes... [2.3 MB]
Allocating iteration vectors... [2 x 2.3 MB]
TOTAL: [14.5 MB]

Starting iterations...

Iterative method: 99 iterations in 0.62 seconds (average 0.002707, setup 0.35)

Value in the initial state: 7.032950803475997E-4

Time for model checking: 1.334 seconds.

Result: 7.032950803475997E-4 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmin=? [ F "MissionTimeUp" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=4,Objy1=1,Objx2=6,Objy2=2

Prob0E: 86 iterations in 0.89 seconds (average 0.010326, setup 0.00)

Prob1A: 3 iterations in 0.05 seconds (average 0.016000, setup 0.00)

yes = 448, no = 178283, maybe = 127191

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=305922, nc=218491, nnz=605913, k=4] [7.4 MB]
Creating vector for yes... [2.3 MB]
Allocating iteration vectors... [2 x 2.3 MB]
TOTAL: [14.4 MB]

Starting iterations...

Iterative method: 114 iterations in 0.65 seconds (average 0.002807, setup 0.33)

Value in the initial state: 1.7344249622983E-6

Time for model checking: 1.605 seconds.

Result: 1.7344249622983E-6 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmax=? [ F "Crash" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=4,Objy1=1,Objx2=6,Objy2=2

Prob0A: 3 iterations in 0.10 seconds (average 0.033333, setup 0.00)

Prob1E: 200 iterations in 2.93 seconds (average 0.014640, setup 0.00)

yes = 149625, no = 3303, maybe = 152994

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=305922, nc=257756, nnz=687676, k=4] [8.4 MB]
Creating vector for yes... [2.3 MB]
Allocating iteration vectors... [2 x 2.3 MB]
TOTAL: [15.4 MB]

Starting iterations...

Iterative method: 94 iterations in 0.66 seconds (average 0.002936, setup 0.38)

Value in the initial state: 0.03188249771379999

Time for model checking: 3.721 seconds.

Result: 0.03188249771379999 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmin=? [ F "Crash" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=4,Objy1=1,Objx2=6,Objy2=2

Prob0E: 3 iterations in 0.08 seconds (average 0.025333, setup 0.00)

Prob1A: 49 iterations in 0.60 seconds (average 0.012245, setup 0.00)

yes = 149625, no = 3303, maybe = 152994

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=305922, nc=257756, nnz=687676, k=4] [8.4 MB]
Creating vector for yes... [2.3 MB]
Allocating iteration vectors... [2 x 2.3 MB]
TOTAL: [15.4 MB]

Starting iterations...

Iterative method: 92 iterations in 0.62 seconds (average 0.003000, setup 0.34)

Value in the initial state: 0.017271371199577893

Time for model checking: 1.329 seconds.

Result: 0.017271371199577893 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmax=? [ F "AllAreaSearched" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=4,Objy1=1,Objx2=6,Objy2=2

Prob0A: 56 iterations in 0.42 seconds (average 0.007571, setup 0.00)

Prob1E: 4 iterations in 0.06 seconds (average 0.014000, setup 0.00)

yes = 1662, no = 232223, maybe = 72037

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=305922, nc=117914, nnz=327087, k=2] [4.1 MB]
Creating vector for yes... [2.3 MB]
Allocating iteration vectors... [2 x 2.3 MB]
TOTAL: [11.1 MB]

Starting iterations...

Iterative method: 91 iterations in 0.43 seconds (average 0.002242, setup 0.23)

Value in the initial state: 0.17170148293957876

Time for model checking: 0.943 seconds.

Result: 0.17170148293957876 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmin=? [ F "AllAreaSearched" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=4,Objy1=1,Objx2=6,Objy2=2

Prob0E: 64 iterations in 0.52 seconds (average 0.008188, setup 0.00)

Prob1A: 3 iterations in 0.05 seconds (average 0.016000, setup 0.00)

yes = 1662, no = 241125, maybe = 63135

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=305922, nc=102487, nnz=283310, k=2] [3.6 MB]
Creating vector for yes... [2.3 MB]
Allocating iteration vectors... [2 x 2.3 MB]
TOTAL: [10.6 MB]

Starting iterations...

Iterative method: 98 iterations in 0.41 seconds (average 0.002245, setup 0.19)

Value in the initial state: 0.001749790864418637

Time for model checking: 0.995 seconds.

Result: 0.001749790864418637 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmax=? [ F "MissionTimeUp"|"AllAreaSearched"|"Crash" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=4,Objy1=1,Objx2=6,Objy2=2

Prob0A: 3 iterations in 0.08 seconds (average 0.028000, setup 0.00)

Prob1E: 208 iterations in 2.68 seconds (average 0.012885, setup 0.00)

yes = 262150, no = 1193, maybe = 42579

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=305922, nc=74437, nnz=214392, k=2] [2.8 MB]
Creating vector for yes... [2.3 MB]
Allocating iteration vectors... [2 x 2.3 MB]
TOTAL: [9.8 MB]

Starting iterations...

Iterative method: 87 iterations in 0.40 seconds (average 0.001931, setup 0.23)

Value in the initial state: 0.19903820846210085

Time for model checking: 3.188 seconds.

Result: 0.19903820846210085 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmin=? [ F "MissionTimeUp"|"AllAreaSearched"|"Crash" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=4,Objy1=1,Objx2=6,Objy2=2

Prob0E: 3 iterations in 0.06 seconds (average 0.020000, setup 0.00)

Prob1A: 52 iterations in 0.54 seconds (average 0.010385, setup 0.00)

yes = 255647, no = 1193, maybe = 49082

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=305922, nc=86037, nnz=247691, k=2] [3.2 MB]
Creating vector for yes... [2.3 MB]
Allocating iteration vectors... [2 x 2.3 MB]
TOTAL: [10.2 MB]

Starting iterations...

Iterative method: 98 iterations in 0.40 seconds (average 0.002041, setup 0.20)

Value in the initial state: 0.01940665944217918

Time for model checking: 1.039 seconds.

Result: 0.01940665944217918 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"drop"}max=? [ F "deadlock" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=4,Objy1=1,Objx2=6,Objy2=2

Prob0E: 4 iterations in 0.08 seconds (average 0.021000, setup 0.00)

Prob1A: 1 iterations in 0.01 seconds (average 0.008000, setup 0.00)

goal = 1, inf = 0, maybe = 305921

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=305922, nc=410683, nnz=840603, k=4] [12.4 MB]
Building sparse matrix (transition rewards)... [n=305922, nc=410683, nnz=0, k=4] [2.7 MB]
Creating vector for state rewards... [2.3 MB]
Creating vector for inf... [2.3 MB]
Allocating iteration vectors... [2 x 2.3 MB]
TOTAL: [24.4 MB]

Starting iterations...

Iterative method: 96 iterations in 0.96 seconds (average 0.004500, setup 0.52)

Value in the initial state: 0.2539723308532282

Time for model checking: 1.064 seconds.

Result: 0.2539723308532282 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"drop"}min=? [ F "deadlock" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=4,Objy1=1,Objx2=6,Objy2=2

Prob0A: 4 iterations in 0.05 seconds (average 0.013000, setup 0.00)

Prob1E: 5 iterations in 0.05 seconds (average 0.009600, setup 0.00)

Warning: PRISM hasn't checked for zero-reward loops.
Your minimum rewards may be too low...

goal = 1, inf = 0, maybe = 305921

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=305922, nc=410683, nnz=840603, k=4] [12.4 MB]
Building sparse matrix (transition rewards)... [n=305922, nc=410683, nnz=0, k=4] [2.7 MB]
Creating vector for state rewards... [2.3 MB]
Creating vector for inf... [2.3 MB]
Allocating iteration vectors... [2 x 2.3 MB]
TOTAL: [24.4 MB]

Starting iterations...

Iterative method: 86 iterations in 0.79 seconds (average 0.004419, setup 0.41)

Value in the initial state: 0.20568023441872502

Time for model checking: 0.918 seconds.

Result: 0.20568023441872502 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"charge"}max=? [ F "deadlock" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=4,Objy1=1,Objx2=6,Objy2=2

Prob0E: 4 iterations in 0.03 seconds (average 0.008000, setup 0.00)

Prob1A: 1 iterations in 0.00 seconds (average 0.004000, setup 0.00)

goal = 1, inf = 0, maybe = 305921

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=305922, nc=410683, nnz=840603, k=4] [12.4 MB]
Building sparse matrix (transition rewards)... [n=305922, nc=410683, nnz=10107, k=4] [2.8 MB]
Creating vector for state rewards... [2.3 MB]
Creating vector for inf... [2.3 MB]
Allocating iteration vectors... [2 x 2.3 MB]
TOTAL: [24.5 MB]

Starting iterations...

Iterative method: 91 iterations in 0.82 seconds (average 0.004484, setup 0.41)

Value in the initial state: 0.31135817399762034

Time for model checking: 0.868 seconds.

Result: 0.31135817399762034 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"charge"}min=? [ F "deadlock" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=4,Objy1=1,Objx2=6,Objy2=2

Prob0A: 4 iterations in 0.03 seconds (average 0.007000, setup 0.00)

Prob1E: 5 iterations in 0.05 seconds (average 0.010400, setup 0.00)

Warning: PRISM hasn't checked for zero-reward loops.
Your minimum rewards may be too low...

goal = 1, inf = 0, maybe = 305921

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=305922, nc=410683, nnz=840603, k=4] [12.4 MB]
Building sparse matrix (transition rewards)... [n=305922, nc=410683, nnz=10107, k=4] [2.8 MB]
Creating vector for state rewards... [2.3 MB]
Creating vector for inf... [2.3 MB]
Allocating iteration vectors... [2 x 2.3 MB]
TOTAL: [24.5 MB]

Starting iterations...

Iterative method: 103 iterations in 0.88 seconds (average 0.004544, setup 0.41)

Value in the initial state: 0.060426190930655085

Time for model checking: 0.975 seconds.

Result: 0.060426190930655085 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"time"}max=? [ F "deadlock" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=4,Objy1=1,Objx2=6,Objy2=2

Prob0E: 4 iterations in 0.03 seconds (average 0.007000, setup 0.00)

Prob1A: 1 iterations in 0.01 seconds (average 0.008000, setup 0.00)

goal = 1, inf = 0, maybe = 305921

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=305922, nc=410683, nnz=840603, k=4] [12.4 MB]
Building sparse matrix (transition rewards)... [n=305922, nc=410683, nnz=152926, k=4] [4.5 MB]
Creating vector for state rewards... [2.3 MB]
Creating vector for inf... [2.3 MB]
Allocating iteration vectors... [2 x 2.3 MB]
TOTAL: [26.2 MB]

Starting iterations...

Iterative method: 97 iterations in 0.90 seconds (average 0.004536, setup 0.46)

Value in the initial state: 175.84918651071524

Time for model checking: 0.948 seconds.

Result: 175.84918651071524 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"time"}min=? [ F "deadlock" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=4,Objy1=1,Objx2=6,Objy2=2

Prob0A: 4 iterations in 0.03 seconds (average 0.007000, setup 0.00)

Prob1E: 5 iterations in 0.05 seconds (average 0.010400, setup 0.00)

Warning: PRISM hasn't checked for zero-reward loops.
Your minimum rewards may be too low...

goal = 1, inf = 0, maybe = 305921

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=305922, nc=410683, nnz=840603, k=4] [12.4 MB]
Building sparse matrix (transition rewards)... [n=305922, nc=410683, nnz=152926, k=4] [4.5 MB]
Creating vector for state rewards... [2.3 MB]
Creating vector for inf... [2.3 MB]
Allocating iteration vectors... [2 x 2.3 MB]
TOTAL: [26.2 MB]

Starting iterations...

Iterative method: 106 iterations in 0.93 seconds (average 0.004528, setup 0.45)

Value in the initial state: 95.70730776396813

Time for model checking: 1.035 seconds.

Result: 95.70730776396813 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"success"}max=? [ F "deadlock" ]/Pmin=? [ F "MissionSuccessful" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=4,Objy1=1,Objx2=6,Objy2=2

Prob0E: 4 iterations in 0.03 seconds (average 0.007000, setup 0.00)

Prob1A: 1 iterations in 0.01 seconds (average 0.008000, setup 0.00)

goal = 1, inf = 0, maybe = 305921

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=305922, nc=410683, nnz=840603, k=4] [12.4 MB]
Building sparse matrix (transition rewards)... [n=305922, nc=410683, nnz=2348, k=4] [2.8 MB]
Creating vector for state rewards... [2.3 MB]
Creating vector for inf... [2.3 MB]
Allocating iteration vectors... [2 x 2.3 MB]
TOTAL: [24.5 MB]

Starting iterations...

Iterative method: 98 iterations in 0.82 seconds (average 0.004286, setup 0.40)

Prob0E: 54 iterations in 0.39 seconds (average 0.007259, setup 0.00)

Prob1A: 3 iterations in 0.04 seconds (average 0.014667, setup 0.00)

yes = 1192, no = 262151, maybe = 42579

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=305922, nc=74437, nnz=214392, k=2] [2.8 MB]
Creating vector for yes... [2.3 MB]
Allocating iteration vectors... [2 x 2.3 MB]
TOTAL: [9.8 MB]

Starting iterations...

Iterative method: 92 iterations in 0.34 seconds (average 0.001870, setup 0.16)

Value in the initial state: 215.041738700983

Time for model checking: 1.673 seconds.

Result: 215.041738700983 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"success"}min=? [ F "deadlock" ]/Pmax=? [ F "MissionSuccessful" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=4,Objy1=1,Objx2=6,Objy2=2

Prob0A: 4 iterations in 0.08 seconds (average 0.020000, setup 0.00)

Prob1E: 5 iterations in 0.04 seconds (average 0.008000, setup 0.00)

Warning: PRISM hasn't checked for zero-reward loops.
Your minimum rewards may be too low...

goal = 1, inf = 0, maybe = 305921

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=305922, nc=410683, nnz=840603, k=4] [12.4 MB]
Building sparse matrix (transition rewards)... [n=305922, nc=410683, nnz=2348, k=4] [2.8 MB]
Creating vector for state rewards... [2.3 MB]
Creating vector for inf... [2.3 MB]
Allocating iteration vectors... [2 x 2.3 MB]
TOTAL: [24.5 MB]

Starting iterations...

Iterative method: 99 iterations in 0.93 seconds (average 0.005212, setup 0.42)

Prob0A: 52 iterations in 0.32 seconds (average 0.006154, setup 0.00)

Prob1E: 4 iterations in 0.04 seconds (average 0.011000, setup 0.00)

yes = 1192, no = 255648, maybe = 49082

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=305922, nc=86037, nnz=247691, k=2] [3.2 MB]
Creating vector for yes... [2.3 MB]
Allocating iteration vectors... [2 x 2.3 MB]
TOTAL: [10.2 MB]

Starting iterations...

Iterative method: 102 iterations in 0.42 seconds (average 0.002039, setup 0.21)

Value in the initial state: 78.55562883703602

Time for model checking: 1.878 seconds.

Result: 78.55562883703602 (value in the initial state)

---------------------------------------------------------------------

Note: There were 16 warnings during computation.

