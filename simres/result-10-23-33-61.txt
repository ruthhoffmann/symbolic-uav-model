PRISM
=====

Version: 4.3
Date: Wed Aug 03 16:14:48 BST 2016
Hostname: eglinton
Memory limits: cudd=1g, java(heap)=981.5m
Command line: prism uav_sim_mdp.prism -s uav_mdp.props -const 'Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=3,Objy1=3,Objx2=6,Objy2=1,'

Parsing model file "uav_sim_mdp.prism"...

Parsing properties file "uav_mdp.props"...

18 properties:
(1) Pmax=? [ F "MissionSuccessful" ]
(2) Pmin=? [ F "MissionSuccessful" ]
(3) Pmax=? [ F "MissionTimeUp" ]
(4) Pmin=? [ F "MissionTimeUp" ]
(5) Pmax=? [ F "Crash" ]
(6) Pmin=? [ F "Crash" ]
(7) Pmax=? [ F "AllAreaSearched" ]
(8) Pmin=? [ F "AllAreaSearched" ]
(9) Pmax=? [ F "MissionTimeUp"|"AllAreaSearched"|"Crash" ]
(10) Pmin=? [ F "MissionTimeUp"|"AllAreaSearched"|"Crash" ]
(11) R{"drop"}max=? [ F "deadlock" ]
(12) R{"drop"}min=? [ F "deadlock" ]
(13) R{"charge"}max=? [ F "deadlock" ]
(14) R{"charge"}min=? [ F "deadlock" ]
(15) R{"time"}max=? [ F "deadlock" ]
(16) R{"time"}min=? [ F "deadlock" ]
(17) R{"success"}max=? [ F "deadlock" ]/Pmin=? [ F "MissionSuccessful" ]
(18) R{"success"}min=? [ F "deadlock" ]/Pmax=? [ F "MissionSuccessful" ]

Type:        MDP
Modules:     Obj Move Robot T 
Variables:   NoOfObjs posx posy s1 retx rety b t c 

---------------------------------------------------------------------

Model checking: Pmax=? [ F "MissionSuccessful" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=3,Objy1=3,Objx2=6,Objy2=1

Building model...
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=3,Objy1=3,Objx2=6,Objy2=1

Warning: Guard for command 7 of module "Move" is never satisfied.

Warning: Guard for command 8 of module "Move" is never satisfied.

Warning: Guard for command 9 of module "Move" is never satisfied.

Warning: Guard for command 7 of module "Robot" is never satisfied.

Warning: Update 2 of command 3 of module "Robot" doesn't do anything ("(s1'=13)", line 210, column 44)

Warning: Update 2 of command 27 of module "T" doesn't do anything ("(c'=1)", line 323, column 108)

Warning: Guard for command 28 of module "T" is never satisfied.

Warning: Update 2 of command 9 of module "T" doesn't do anything ("(c'=1)", line 300, column 112)

Warning: Guard for command 10 of module "T" is never satisfied.

Warning: Update 2 of command 51 of module "T" doesn't do anything ("(c'=1)", line 354, column 113)

Warning: Guard for command 52 of module "T" is never satisfied.

Computing reachable states...

Reachability (BFS): 89 iterations in 1.32 seconds (average 0.014876, setup 0.00)

Time for model construction: 3.083 seconds.

Warning: Deadlocks detected and fixed in 1 states

Type:        MDP
States:      251428 (1 initial)
Transitions: 657919
Choices:     332335

Transition matrix: 238322 nodes (93 terminal), 657919 minterms, vars: 34r/34c/18nd

Prob0A: 57 iterations in 0.50 seconds (average 0.008842, setup 0.00)

Prob1E: 4 iterations in 0.07 seconds (average 0.017000, setup 0.00)

yes = 1140, no = 213616, maybe = 36672

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=251428, nc=61837, nnz=170663, k=2] [2.3 MB]
Creating vector for yes... [1.9 MB]
Allocating iteration vectors... [2 x 1.9 MB]
TOTAL: [8.0 MB]

Starting iterations...

Iterative method: 101 iterations in 0.42 seconds (average 0.001663, setup 0.25)

Value in the initial state: 0.9801169586922253

Time for model checking: 1.021 seconds.

Result: 0.9801169586922253 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmin=? [ F "MissionSuccessful" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=3,Objy1=3,Objx2=6,Objy2=1

Prob0E: 59 iterations in 0.62 seconds (average 0.010576, setup 0.00)

Prob1A: 3 iterations in 0.05 seconds (average 0.016000, setup 0.00)

yes = 1140, no = 219840, maybe = 30448

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=251428, nc=51426, nnz=140668, k=2] [1.9 MB]
Creating vector for yes... [1.9 MB]
Allocating iteration vectors... [2 x 1.9 MB]
TOTAL: [7.7 MB]

Starting iterations...

Iterative method: 95 iterations in 0.33 seconds (average 0.001600, setup 0.18)

Value in the initial state: 0.7538157648214626

Time for model checking: 1.032 seconds.

Result: 0.7538157648214626 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmax=? [ F "MissionTimeUp" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=3,Objy1=3,Objx2=6,Objy2=1

Prob0A: 69 iterations in 0.69 seconds (average 0.010029, setup 0.00)

Prob1E: 4 iterations in 0.04 seconds (average 0.011000, setup 0.00)

yes = 388, no = 145522, maybe = 105518

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=251428, nc=173021, nnz=464177, k=4] [5.7 MB]
Creating vector for yes... [1.9 MB]
Allocating iteration vectors... [2 x 1.9 MB]
TOTAL: [11.5 MB]

Starting iterations...

Iterative method: 100 iterations in 0.54 seconds (average 0.002160, setup 0.33)

Value in the initial state: 6.880459269779872E-4

Time for model checking: 1.281 seconds.

Result: 6.880459269779872E-4 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmin=? [ F "MissionTimeUp" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=3,Objy1=3,Objx2=6,Objy2=1

Prob0E: 84 iterations in 1.08 seconds (average 0.012857, setup 0.00)

Prob1A: 3 iterations in 0.05 seconds (average 0.016000, setup 0.00)

yes = 388, no = 145788, maybe = 105252

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=251428, nc=172565, nnz=463119, k=4] [5.7 MB]
Creating vector for yes... [1.9 MB]
Allocating iteration vectors... [2 x 1.9 MB]
TOTAL: [11.5 MB]

Starting iterations...

Iterative method: 116 iterations in 0.54 seconds (average 0.002241, setup 0.28)

Value in the initial state: 2.1580429271027496E-6

Time for model checking: 1.724 seconds.

Result: 2.1580429271027496E-6 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmax=? [ F "Crash" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=3,Objy1=3,Objx2=6,Objy2=1

Prob0A: 3 iterations in 0.09 seconds (average 0.029333, setup 0.00)

Prob1E: 208 iterations in 2.90 seconds (average 0.013962, setup 0.00)

yes = 122467, no = 3315, maybe = 125646

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=251428, nc=206553, nnz=532137, k=4] [6.5 MB]
Creating vector for yes... [1.9 MB]
Allocating iteration vectors... [2 x 1.9 MB]
TOTAL: [12.3 MB]

Starting iterations...

Iterative method: 97 iterations in 0.62 seconds (average 0.002351, setup 0.40)

Value in the initial state: 0.03109350453110065

Time for model checking: 3.647 seconds.

Result: 0.03109350453110065 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmin=? [ F "Crash" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=3,Objy1=3,Objx2=6,Objy2=1

Prob0E: 3 iterations in 0.08 seconds (average 0.028000, setup 0.00)

Prob1A: 49 iterations in 0.72 seconds (average 0.014612, setup 0.00)

yes = 122467, no = 3315, maybe = 125646

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=251428, nc=206553, nnz=532137, k=4] [6.5 MB]
Creating vector for yes... [1.9 MB]
Allocating iteration vectors... [2 x 1.9 MB]
TOTAL: [12.3 MB]

Starting iterations...

Iterative method: 102 iterations in 0.58 seconds (average 0.002431, setup 0.34)

Value in the initial state: 0.01789122625955444

Time for model checking: 1.408 seconds.

Result: 0.01789122625955444 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmax=? [ F "AllAreaSearched" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=3,Objy1=3,Objx2=6,Objy2=1

Prob0A: 57 iterations in 0.44 seconds (average 0.007649, setup 0.00)

Prob1E: 4 iterations in 0.05 seconds (average 0.013000, setup 0.00)

yes = 1786, no = 196457, maybe = 53185

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=251428, nc=84964, nnz=218455, k=2] [2.8 MB]
Creating vector for yes... [1.9 MB]
Allocating iteration vectors... [2 x 1.9 MB]
TOTAL: [8.6 MB]

Starting iterations...

Iterative method: 86 iterations in 0.37 seconds (average 0.001814, setup 0.22)

Value in the initial state: 0.21770204927675482

Time for model checking: 0.88 seconds.

Result: 0.21770204927675482 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmin=? [ F "AllAreaSearched" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=3,Objy1=3,Objx2=6,Objy2=1

Prob0E: 63 iterations in 0.48 seconds (average 0.007619, setup 0.00)

Prob1A: 3 iterations in 0.05 seconds (average 0.016000, setup 0.00)

yes = 1786, no = 204464, maybe = 45178

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=251428, nc=72495, nnz=184647, k=2] [2.4 MB]
Creating vector for yes... [1.9 MB]
Allocating iteration vectors... [2 x 1.9 MB]
TOTAL: [8.2 MB]

Starting iterations...

Iterative method: 95 iterations in 0.32 seconds (average 0.001684, setup 0.16)

Value in the initial state: 0.0017935637309186472

Time for model checking: 0.882 seconds.

Result: 0.0017935637309186472 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmax=? [ F "MissionTimeUp"|"AllAreaSearched"|"Crash" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=3,Objy1=3,Objx2=6,Objy2=1

Prob0A: 3 iterations in 0.10 seconds (average 0.032000, setup 0.00)

Prob1E: 228 iterations in 3.41 seconds (average 0.014965, setup 0.00)

yes = 219839, no = 1141, maybe = 30448

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=251428, nc=51426, nnz=140668, k=2] [1.9 MB]
Creating vector for yes... [1.9 MB]
Allocating iteration vectors... [2 x 1.9 MB]
TOTAL: [7.7 MB]

Starting iterations...

Iterative method: 89 iterations in 0.35 seconds (average 0.001528, setup 0.22)

Value in the initial state: 0.24618387063962302

Time for model checking: 3.921 seconds.

Result: 0.24618387063962302 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmin=? [ F "MissionTimeUp"|"AllAreaSearched"|"Crash" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=3,Objy1=3,Objx2=6,Objy2=1

Prob0E: 3 iterations in 0.09 seconds (average 0.030667, setup 0.00)

Prob1A: 57 iterations in 0.76 seconds (average 0.013263, setup 0.00)

yes = 213615, no = 1141, maybe = 36672

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=251428, nc=61837, nnz=170663, k=2] [2.3 MB]
Creating vector for yes... [1.9 MB]
Allocating iteration vectors... [2 x 1.9 MB]
TOTAL: [8.0 MB]

Starting iterations...

Iterative method: 100 iterations in 0.36 seconds (average 0.001600, setup 0.20)

Value in the initial state: 0.019882185914203675

Time for model checking: 1.267 seconds.

Result: 0.019882185914203675 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"drop"}max=? [ F "deadlock" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=3,Objy1=3,Objx2=6,Objy2=1

Prob0E: 4 iterations in 0.10 seconds (average 0.025000, setup 0.00)

Prob1A: 1 iterations in 0.02 seconds (average 0.016000, setup 0.00)

goal = 1, inf = 0, maybe = 251427

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=251428, nc=332334, nnz=657918, k=4] [9.8 MB]
Building sparse matrix (transition rewards)... [n=251428, nc=332334, nnz=0, k=4] [2.2 MB]
Creating vector for state rewards... [1.9 MB]
Creating vector for inf... [1.9 MB]
Allocating iteration vectors... [2 x 1.9 MB]
TOTAL: [19.7 MB]

Starting iterations...

Iterative method: 98 iterations in 0.82 seconds (average 0.003633, setup 0.46)

Value in the initial state: 0.20536751005928508

Time for model checking: 0.948 seconds.

Result: 0.20536751005928508 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"drop"}min=? [ F "deadlock" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=3,Objy1=3,Objx2=6,Objy2=1

Prob0A: 4 iterations in 0.04 seconds (average 0.009000, setup 0.00)

Prob1E: 5 iterations in 0.05 seconds (average 0.010400, setup 0.00)

Warning: PRISM hasn't checked for zero-reward loops.
Your minimum rewards may be too low...

goal = 1, inf = 0, maybe = 251427

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=251428, nc=332334, nnz=657918, k=4] [9.8 MB]
Building sparse matrix (transition rewards)... [n=251428, nc=332334, nnz=0, k=4] [2.2 MB]
Creating vector for state rewards... [1.9 MB]
Creating vector for inf... [1.9 MB]
Allocating iteration vectors... [2 x 1.9 MB]
TOTAL: [19.7 MB]

Starting iterations...

Iterative method: 88 iterations in 0.64 seconds (average 0.003500, setup 0.34)

Value in the initial state: 0.19086646737145505

Time for model checking: 0.752 seconds.

Result: 0.19086646737145505 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"charge"}max=? [ F "deadlock" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=3,Objy1=3,Objx2=6,Objy2=1

Prob0E: 4 iterations in 0.03 seconds (average 0.007000, setup 0.00)

Prob1A: 1 iterations in 0.01 seconds (average 0.008000, setup 0.00)

goal = 1, inf = 0, maybe = 251427

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=251428, nc=332334, nnz=657918, k=4] [9.8 MB]
Building sparse matrix (transition rewards)... [n=251428, nc=332334, nnz=7488, k=4] [2.3 MB]
Creating vector for state rewards... [1.9 MB]
Creating vector for inf... [1.9 MB]
Allocating iteration vectors... [2 x 1.9 MB]
TOTAL: [19.7 MB]

Starting iterations...

Iterative method: 97 iterations in 0.69 seconds (average 0.003588, setup 0.34)

Value in the initial state: 0.2911140582229838

Time for model checking: 0.734 seconds.

Result: 0.2911140582229838 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"charge"}min=? [ F "deadlock" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=3,Objy1=3,Objx2=6,Objy2=1

Prob0A: 4 iterations in 0.02 seconds (average 0.006000, setup 0.00)

Prob1E: 5 iterations in 0.04 seconds (average 0.008000, setup 0.00)

Warning: PRISM hasn't checked for zero-reward loops.
Your minimum rewards may be too low...

goal = 1, inf = 0, maybe = 251427

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=251428, nc=332334, nnz=657918, k=4] [9.8 MB]
Building sparse matrix (transition rewards)... [n=251428, nc=332334, nnz=7488, k=4] [2.3 MB]
Creating vector for state rewards... [1.9 MB]
Creating vector for inf... [1.9 MB]
Allocating iteration vectors... [2 x 1.9 MB]
TOTAL: [19.7 MB]

Starting iterations...

Iterative method: 103 iterations in 0.68 seconds (average 0.003534, setup 0.32)

Value in the initial state: 0.04534149965524261

Time for model checking: 0.776 seconds.

Result: 0.04534149965524261 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"time"}max=? [ F "deadlock" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=3,Objy1=3,Objx2=6,Objy2=1

Prob0E: 4 iterations in 0.02 seconds (average 0.006000, setup 0.00)

Prob1A: 1 iterations in 0.00 seconds (average 0.004000, setup 0.00)

goal = 1, inf = 0, maybe = 251427

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=251428, nc=332334, nnz=657918, k=4] [9.8 MB]
Building sparse matrix (transition rewards)... [n=251428, nc=332334, nnz=125780, k=4] [3.7 MB]
Creating vector for state rewards... [1.9 MB]
Creating vector for inf... [1.9 MB]
Allocating iteration vectors... [2 x 1.9 MB]
TOTAL: [21.1 MB]

Starting iterations...

Iterative method: 100 iterations in 0.73 seconds (average 0.003600, setup 0.37)

Value in the initial state: 171.56305612963985

Time for model checking: 0.778 seconds.

Result: 171.56305612963985 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"time"}min=? [ F "deadlock" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=3,Objy1=3,Objx2=6,Objy2=1

Prob0A: 4 iterations in 0.02 seconds (average 0.006000, setup 0.00)

Prob1E: 5 iterations in 0.03 seconds (average 0.006400, setup 0.00)

Warning: PRISM hasn't checked for zero-reward loops.
Your minimum rewards may be too low...

goal = 1, inf = 0, maybe = 251427

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=251428, nc=332334, nnz=657918, k=4] [9.8 MB]
Building sparse matrix (transition rewards)... [n=251428, nc=332334, nnz=125780, k=4] [3.7 MB]
Creating vector for state rewards... [1.9 MB]
Creating vector for inf... [1.9 MB]
Allocating iteration vectors... [2 x 1.9 MB]
TOTAL: [21.1 MB]

Starting iterations...

Iterative method: 108 iterations in 0.72 seconds (average 0.003481, setup 0.34)

Value in the initial state: 99.15418085344251

Time for model checking: 0.794 seconds.

Result: 99.15418085344251 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"success"}max=? [ F "deadlock" ]/Pmin=? [ F "MissionSuccessful" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=3,Objy1=3,Objx2=6,Objy2=1

Prob0E: 4 iterations in 0.02 seconds (average 0.006000, setup 0.00)

Prob1A: 1 iterations in 0.00 seconds (average 0.004000, setup 0.00)

goal = 1, inf = 0, maybe = 251427

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=251428, nc=332334, nnz=657918, k=4] [9.8 MB]
Building sparse matrix (transition rewards)... [n=251428, nc=332334, nnz=2244, k=4] [2.3 MB]
Creating vector for state rewards... [1.9 MB]
Creating vector for inf... [1.9 MB]
Allocating iteration vectors... [2 x 1.9 MB]
TOTAL: [19.7 MB]

Starting iterations...

Iterative method: 102 iterations in 0.67 seconds (average 0.003412, setup 0.32)

Prob0E: 59 iterations in 0.28 seconds (average 0.004678, setup 0.00)

Prob1A: 3 iterations in 0.05 seconds (average 0.017333, setup 0.00)

yes = 1140, no = 219840, maybe = 30448

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=251428, nc=51426, nnz=140668, k=2] [1.9 MB]
Creating vector for yes... [1.9 MB]
Allocating iteration vectors... [2 x 1.9 MB]
TOTAL: [7.7 MB]

Starting iterations...

Iterative method: 95 iterations in 0.35 seconds (average 0.002147, setup 0.14)

Value in the initial state: 223.16475559633835

Time for model checking: 1.446 seconds.

Result: 223.16475559633835 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"success"}min=? [ F "deadlock" ]/Pmax=? [ F "MissionSuccessful" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=3,Objy1=3,Objx2=6,Objy2=1

Prob0A: 4 iterations in 0.12 seconds (average 0.030000, setup 0.00)

Prob1E: 5 iterations in 0.08 seconds (average 0.015200, setup 0.00)

Warning: PRISM hasn't checked for zero-reward loops.
Your minimum rewards may be too low...

goal = 1, inf = 0, maybe = 251427

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=251428, nc=332334, nnz=657918, k=4] [9.8 MB]
Building sparse matrix (transition rewards)... [n=251428, nc=332334, nnz=2244, k=4] [2.3 MB]
Creating vector for state rewards... [1.9 MB]
Creating vector for inf... [1.9 MB]
Allocating iteration vectors... [2 x 1.9 MB]
TOTAL: [19.7 MB]

Starting iterations...

Iterative method: 99 iterations in 1.00 seconds (average 0.003717, setup 0.64)

Prob0A: 57 iterations in 0.30 seconds (average 0.005263, setup 0.00)

Prob1E: 4 iterations in 0.04 seconds (average 0.011000, setup 0.00)

yes = 1140, no = 213616, maybe = 36672

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=251428, nc=61837, nnz=170663, k=2] [2.3 MB]
Creating vector for yes... [1.9 MB]
Allocating iteration vectors... [2 x 1.9 MB]
TOTAL: [8.0 MB]

Starting iterations...

Iterative method: 101 iterations in 0.31 seconds (average 0.001465, setup 0.16)

Value in the initial state: 81.56920082478246

Time for model checking: 1.906 seconds.

Result: 81.56920082478246 (value in the initial state)

---------------------------------------------------------------------

Note: There were 16 warnings during computation.

