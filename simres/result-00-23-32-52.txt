PRISM
=====

Version: 4.3
Date: Wed Aug 03 15:59:56 BST 2016
Hostname: eglinton
Memory limits: cudd=1g, java(heap)=981.5m
Command line: prism uav_sim_mdp.prism -s uav_mdp.props -const 'Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=3,Objy1=2,Objx2=5,Objy2=2,'

Parsing model file "uav_sim_mdp.prism"...

Parsing properties file "uav_mdp.props"...

18 properties:
(1) Pmax=? [ F "MissionSuccessful" ]
(2) Pmin=? [ F "MissionSuccessful" ]
(3) Pmax=? [ F "MissionTimeUp" ]
(4) Pmin=? [ F "MissionTimeUp" ]
(5) Pmax=? [ F "Crash" ]
(6) Pmin=? [ F "Crash" ]
(7) Pmax=? [ F "AllAreaSearched" ]
(8) Pmin=? [ F "AllAreaSearched" ]
(9) Pmax=? [ F "MissionTimeUp"|"AllAreaSearched"|"Crash" ]
(10) Pmin=? [ F "MissionTimeUp"|"AllAreaSearched"|"Crash" ]
(11) R{"drop"}max=? [ F "deadlock" ]
(12) R{"drop"}min=? [ F "deadlock" ]
(13) R{"charge"}max=? [ F "deadlock" ]
(14) R{"charge"}min=? [ F "deadlock" ]
(15) R{"time"}max=? [ F "deadlock" ]
(16) R{"time"}min=? [ F "deadlock" ]
(17) R{"success"}max=? [ F "deadlock" ]/Pmin=? [ F "MissionSuccessful" ]
(18) R{"success"}min=? [ F "deadlock" ]/Pmax=? [ F "MissionSuccessful" ]

Type:        MDP
Modules:     Obj Move Robot T 
Variables:   NoOfObjs posx posy s1 retx rety b t c 

---------------------------------------------------------------------

Model checking: Pmax=? [ F "MissionSuccessful" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=3,Objy1=2,Objx2=5,Objy2=2

Building model...
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=3,Objy1=2,Objx2=5,Objy2=2

Warning: Guard for command 7 of module "Move" is never satisfied.

Warning: Guard for command 8 of module "Move" is never satisfied.

Warning: Guard for command 9 of module "Move" is never satisfied.

Warning: Guard for command 7 of module "Robot" is never satisfied.

Warning: Update 2 of command 3 of module "Robot" doesn't do anything ("(s1'=13)", line 210, column 44)

Warning: Update 2 of command 27 of module "T" doesn't do anything ("(c'=1)", line 323, column 108)

Warning: Guard for command 28 of module "T" is never satisfied.

Warning: Update 2 of command 9 of module "T" doesn't do anything ("(c'=1)", line 300, column 112)

Warning: Guard for command 10 of module "T" is never satisfied.

Warning: Update 2 of command 51 of module "T" doesn't do anything ("(c'=1)", line 354, column 113)

Warning: Guard for command 52 of module "T" is never satisfied.

Computing reachable states...

Reachability (BFS): 87 iterations in 0.87 seconds (average 0.009977, setup 0.00)

Time for model construction: 3.142 seconds.

Warning: Deadlocks detected and fixed in 1 states

Type:        MDP
States:      210292 (1 initial)
Transitions: 583338
Choices:     285947

Transition matrix: 206484 nodes (91 terminal), 583338 minterms, vars: 34r/34c/18nd

Prob0A: 48 iterations in 0.36 seconds (average 0.007500, setup 0.00)

Prob1E: 4 iterations in 0.06 seconds (average 0.016000, setup 0.00)

yes = 1078, no = 173439, maybe = 35775

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=210292, nc=64991, nnz=185889, k=2] [2.4 MB]
Creating vector for yes... [1.6 MB]
Allocating iteration vectors... [2 x 1.6 MB]
TOTAL: [7.2 MB]

Starting iterations...

Iterative method: 96 iterations in 0.36 seconds (average 0.001417, setup 0.22)

Value in the initial state: 0.983023336000495

Time for model checking: 0.816 seconds.

Result: 0.983023336000495 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmin=? [ F "MissionSuccessful" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=3,Objy1=2,Objx2=5,Objy2=2

Prob0E: 48 iterations in 0.54 seconds (average 0.011167, setup 0.00)

Prob1A: 3 iterations in 0.04 seconds (average 0.012000, setup 0.00)

yes = 1078, no = 177536, maybe = 31678

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=210292, nc=57378, nnz=164092, k=2] [2.1 MB]
Creating vector for yes... [1.6 MB]
Allocating iteration vectors... [2 x 1.6 MB]
TOTAL: [6.9 MB]

Starting iterations...

Iterative method: 89 iterations in 0.30 seconds (average 0.001438, setup 0.17)

Value in the initial state: 0.8406045061960338

Time for model checking: 0.892 seconds.

Result: 0.8406045061960338 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmax=? [ F "MissionTimeUp" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=3,Objy1=2,Objx2=5,Objy2=2

Prob0A: 78 iterations in 0.47 seconds (average 0.006000, setup 0.00)

Prob1E: 4 iterations in 0.03 seconds (average 0.008000, setup 0.00)

yes = 308, no = 129186, maybe = 80798

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=210292, nc=144794, nnz=403930, k=4] [5.0 MB]
Creating vector for yes... [1.6 MB]
Allocating iteration vectors... [2 x 1.6 MB]
TOTAL: [9.8 MB]

Starting iterations...

Iterative method: 97 iterations in 0.61 seconds (average 0.002557, setup 0.36)

Value in the initial state: 1.4933169796393724E-4

Time for model checking: 1.136 seconds.

Result: 1.4933169796393724E-4 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmin=? [ F "MissionTimeUp" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=3,Objy1=2,Objx2=5,Objy2=2

Prob0E: 92 iterations in 0.87 seconds (average 0.009478, setup 0.00)

Prob1A: 3 iterations in 0.04 seconds (average 0.013333, setup 0.00)

yes = 308, no = 129608, maybe = 80376

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=210292, nc=144040, nnz=402376, k=4] [4.9 MB]
Creating vector for yes... [1.6 MB]
Allocating iteration vectors... [2 x 1.6 MB]
TOTAL: [9.8 MB]

Starting iterations...

Iterative method: 112 iterations in 0.57 seconds (average 0.001929, setup 0.36)

Value in the initial state: 2.1625256573472245E-7

Time for model checking: 1.51 seconds.

Result: 2.1625256573472245E-7 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmax=? [ F "Crash" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=3,Objy1=2,Objx2=5,Objy2=2

Prob0A: 3 iterations in 0.08 seconds (average 0.025333, setup 0.00)

Prob1E: 180 iterations in 2.68 seconds (average 0.014867, setup 0.00)

yes = 102775, no = 2855, maybe = 104662

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=210292, nc=180317, nnz=477708, k=4] [5.8 MB]
Creating vector for yes... [1.6 MB]
Allocating iteration vectors... [2 x 1.6 MB]
TOTAL: [10.7 MB]

Starting iterations...

Iterative method: 91 iterations in 0.57 seconds (average 0.002505, setup 0.34)

Value in the initial state: 0.02500887537513271

Time for model checking: 3.356 seconds.

Result: 0.02500887537513271 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmin=? [ F "Crash" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=3,Objy1=2,Objx2=5,Objy2=2

Prob0E: 4 iterations in 0.06 seconds (average 0.016000, setup 0.00)

Prob1A: 48 iterations in 0.51 seconds (average 0.010667, setup 0.00)

yes = 102775, no = 2855, maybe = 104662

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=210292, nc=180317, nnz=477708, k=4] [5.8 MB]
Creating vector for yes... [1.6 MB]
Allocating iteration vectors... [2 x 1.6 MB]
TOTAL: [10.7 MB]

Starting iterations...

Iterative method: 86 iterations in 0.59 seconds (average 0.003442, setup 0.30)

Value in the initial state: 0.01493746146588309

Time for model checking: 1.194 seconds.

Result: 0.01493746146588309 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmax=? [ F "AllAreaSearched" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=3,Objy1=2,Objx2=5,Objy2=2

Prob0A: 55 iterations in 0.42 seconds (average 0.007636, setup 0.00)

Prob1E: 4 iterations in 0.04 seconds (average 0.010000, setup 0.00)

yes = 1468, no = 153276, maybe = 55548

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=210292, nc=91899, nnz=251570, k=2] [3.2 MB]
Creating vector for yes... [1.6 MB]
Allocating iteration vectors... [2 x 1.6 MB]
TOTAL: [8.0 MB]

Starting iterations...

Iterative method: 87 iterations in 0.33 seconds (average 0.001609, setup 0.19)

Value in the initial state: 0.13648231053955873

Time for model checking: 0.776 seconds.

Result: 0.13648231053955873 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmin=? [ F "AllAreaSearched" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=3,Objy1=2,Objx2=5,Objy2=2

Prob0E: 62 iterations in 0.36 seconds (average 0.005806, setup 0.00)

Prob1A: 3 iterations in 0.04 seconds (average 0.012000, setup 0.00)

yes = 1468, no = 160560, maybe = 48264

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=210292, nc=78369, nnz=212154, k=2] [2.7 MB]
Creating vector for yes... [1.6 MB]
Allocating iteration vectors... [2 x 1.6 MB]
TOTAL: [7.5 MB]

Starting iterations...

Iterative method: 95 iterations in 0.27 seconds (average 0.001516, setup 0.13)

Value in the initial state: 0.0018495121097118414

Time for model checking: 0.683 seconds.

Result: 0.0018495121097118414 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmax=? [ F "MissionTimeUp"|"AllAreaSearched"|"Crash" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=3,Objy1=2,Objx2=5,Objy2=2

Prob0A: 3 iterations in 0.07 seconds (average 0.024000, setup 0.00)

Prob1E: 184 iterations in 2.25 seconds (average 0.012217, setup 0.00)

yes = 177535, no = 1079, maybe = 31678

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=210292, nc=57378, nnz=164092, k=2] [2.1 MB]
Creating vector for yes... [1.6 MB]
Allocating iteration vectors... [2 x 1.6 MB]
TOTAL: [6.9 MB]

Starting iterations...

Iterative method: 82 iterations in 0.34 seconds (average 0.001805, setup 0.19)

Value in the initial state: 0.15939519359988272

Time for model checking: 2.68 seconds.

Result: 0.15939519359988272 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmin=? [ F "MissionTimeUp"|"AllAreaSearched"|"Crash" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=3,Objy1=2,Objx2=5,Objy2=2

Prob0E: 4 iterations in 0.07 seconds (average 0.017000, setup 0.00)

Prob1A: 48 iterations in 0.45 seconds (average 0.009333, setup 0.00)

yes = 173438, no = 1079, maybe = 35775

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=210292, nc=64991, nnz=185889, k=2] [2.4 MB]
Creating vector for yes... [1.6 MB]
Allocating iteration vectors... [2 x 1.6 MB]
TOTAL: [7.2 MB]

Starting iterations...

Iterative method: 94 iterations in 0.29 seconds (average 0.001404, setup 0.16)

Value in the initial state: 0.01697513757086261

Time for model checking: 0.851 seconds.

Result: 0.01697513757086261 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"drop"}max=? [ F "deadlock" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=3,Objy1=2,Objx2=5,Objy2=2

Prob0E: 5 iterations in 0.06 seconds (average 0.012800, setup 0.00)

Prob1A: 1 iterations in 0.01 seconds (average 0.008000, setup 0.00)

goal = 1, inf = 0, maybe = 210291

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=210292, nc=285946, nnz=583337, k=4] [8.6 MB]
Building sparse matrix (transition rewards)... [n=210292, nc=285946, nnz=0, k=4] [1.9 MB]
Creating vector for state rewards... [1.6 MB]
Creating vector for inf... [1.6 MB]
Allocating iteration vectors... [2 x 1.6 MB]
TOTAL: [16.9 MB]

Starting iterations...

Iterative method: 91 iterations in 0.68 seconds (average 0.003033, setup 0.40)

Value in the initial state: 0.17987427405175255

Time for model checking: 0.761 seconds.

Result: 0.17987427405175255 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"drop"}min=? [ F "deadlock" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=3,Objy1=2,Objx2=5,Objy2=2

Prob0A: 4 iterations in 0.04 seconds (average 0.009000, setup 0.00)

Prob1E: 5 iterations in 0.04 seconds (average 0.008800, setup 0.00)

Warning: PRISM hasn't checked for zero-reward loops.
Your minimum rewards may be too low...

goal = 1, inf = 0, maybe = 210291

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=210292, nc=285946, nnz=583337, k=4] [8.6 MB]
Building sparse matrix (transition rewards)... [n=210292, nc=285946, nnz=0, k=4] [1.9 MB]
Creating vector for state rewards... [1.6 MB]
Creating vector for inf... [1.6 MB]
Allocating iteration vectors... [2 x 1.6 MB]
TOTAL: [16.9 MB]

Starting iterations...

Iterative method: 82 iterations in 0.55 seconds (average 0.003122, setup 0.30)

Value in the initial state: 0.15722956046954104

Time for model checking: 0.642 seconds.

Result: 0.15722956046954104 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"charge"}max=? [ F "deadlock" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=3,Objy1=2,Objx2=5,Objy2=2

Prob0E: 5 iterations in 0.02 seconds (average 0.004800, setup 0.00)

Prob1A: 1 iterations in 0.00 seconds (average 0.004000, setup 0.00)

goal = 1, inf = 0, maybe = 210291

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=210292, nc=285946, nnz=583337, k=4] [8.6 MB]
Building sparse matrix (transition rewards)... [n=210292, nc=285946, nnz=7000, k=4] [2.0 MB]
Creating vector for state rewards... [1.6 MB]
Creating vector for inf... [1.6 MB]
Allocating iteration vectors... [2 x 1.6 MB]
TOTAL: [17.0 MB]

Starting iterations...

Iterative method: 87 iterations in 0.57 seconds (average 0.003126, setup 0.30)

Value in the initial state: 0.27361831190292046

Time for model checking: 0.611 seconds.

Result: 0.27361831190292046 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"charge"}min=? [ F "deadlock" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=3,Objy1=2,Objx2=5,Objy2=2

Prob0A: 4 iterations in 0.02 seconds (average 0.006000, setup 0.00)

Prob1E: 5 iterations in 0.04 seconds (average 0.008000, setup 0.00)

Warning: PRISM hasn't checked for zero-reward loops.
Your minimum rewards may be too low...

goal = 1, inf = 0, maybe = 210291

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=210292, nc=285946, nnz=583337, k=4] [8.6 MB]
Building sparse matrix (transition rewards)... [n=210292, nc=285946, nnz=7000, k=4] [2.0 MB]
Creating vector for state rewards... [1.6 MB]
Creating vector for inf... [1.6 MB]
Allocating iteration vectors... [2 x 1.6 MB]
TOTAL: [17.0 MB]

Starting iterations...

Iterative method: 99 iterations in 0.60 seconds (average 0.003111, setup 0.30)

Value in the initial state: 0.0222725449882487

Time for model checking: 0.68 seconds.

Result: 0.0222725449882487 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"time"}max=? [ F "deadlock" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=3,Objy1=2,Objx2=5,Objy2=2

Prob0E: 5 iterations in 0.02 seconds (average 0.004800, setup 0.00)

Prob1A: 1 iterations in 0.00 seconds (average 0.004000, setup 0.00)

goal = 1, inf = 0, maybe = 210291

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=210292, nc=285946, nnz=583337, k=4] [8.6 MB]
Building sparse matrix (transition rewards)... [n=210292, nc=285946, nnz=105628, k=4] [3.1 MB]
Creating vector for state rewards... [1.6 MB]
Creating vector for inf... [1.6 MB]
Allocating iteration vectors... [2 x 1.6 MB]
TOTAL: [18.1 MB]

Starting iterations...

Iterative method: 94 iterations in 0.63 seconds (average 0.003149, setup 0.33)

Value in the initial state: 138.23829326383353

Time for model checking: 0.679 seconds.

Result: 138.23829326383353 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"time"}min=? [ F "deadlock" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=3,Objy1=2,Objx2=5,Objy2=2

Prob0A: 4 iterations in 0.03 seconds (average 0.007000, setup 0.00)

Prob1E: 5 iterations in 0.04 seconds (average 0.008800, setup 0.00)

Warning: PRISM hasn't checked for zero-reward loops.
Your minimum rewards may be too low...

goal = 1, inf = 0, maybe = 210291

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=210292, nc=285946, nnz=583337, k=4] [8.6 MB]
Building sparse matrix (transition rewards)... [n=210292, nc=285946, nnz=105628, k=4] [3.1 MB]
Creating vector for state rewards... [1.6 MB]
Creating vector for inf... [1.6 MB]
Allocating iteration vectors... [2 x 1.6 MB]
TOTAL: [18.1 MB]

Starting iterations...

Iterative method: 98 iterations in 0.64 seconds (average 0.003184, setup 0.32)

Value in the initial state: 82.87776091680396

Time for model checking: 0.721 seconds.

Result: 82.87776091680396 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"success"}max=? [ F "deadlock" ]/Pmin=? [ F "MissionSuccessful" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=3,Objy1=2,Objx2=5,Objy2=2

Prob0E: 5 iterations in 0.02 seconds (average 0.004000, setup 0.00)

Prob1A: 1 iterations in 0.00 seconds (average 0.004000, setup 0.00)

goal = 1, inf = 0, maybe = 210291

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=210292, nc=285946, nnz=583337, k=4] [8.6 MB]
Building sparse matrix (transition rewards)... [n=210292, nc=285946, nnz=2124, k=4] [1.9 MB]
Creating vector for state rewards... [1.6 MB]
Creating vector for inf... [1.6 MB]
Allocating iteration vectors... [2 x 1.6 MB]
TOTAL: [16.9 MB]

Starting iterations...

Iterative method: 94 iterations in 0.58 seconds (average 0.003021, setup 0.30)

Prob0E: 48 iterations in 0.36 seconds (average 0.007583, setup 0.00)

Prob1A: 3 iterations in 0.04 seconds (average 0.013333, setup 0.00)

yes = 1078, no = 177536, maybe = 31678

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=210292, nc=57378, nnz=164092, k=2] [2.1 MB]
Creating vector for yes... [1.6 MB]
Allocating iteration vectors... [2 x 1.6 MB]
TOTAL: [6.9 MB]

Starting iterations...

Iterative method: 89 iterations in 0.28 seconds (average 0.001393, setup 0.16)

Value in the initial state: 161.46320307228325

Time for model checking: 1.327 seconds.

Result: 161.46320307228325 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"success"}min=? [ F "deadlock" ]/Pmax=? [ F "MissionSuccessful" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=3,Objy1=2,Objx2=5,Objy2=2

Prob0A: 4 iterations in 0.09 seconds (average 0.022000, setup 0.00)

Prob1E: 5 iterations in 0.04 seconds (average 0.008000, setup 0.00)

Warning: PRISM hasn't checked for zero-reward loops.
Your minimum rewards may be too low...

goal = 1, inf = 0, maybe = 210291

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=210292, nc=285946, nnz=583337, k=4] [8.6 MB]
Building sparse matrix (transition rewards)... [n=210292, nc=285946, nnz=2124, k=4] [1.9 MB]
Creating vector for state rewards... [1.6 MB]
Creating vector for inf... [1.6 MB]
Allocating iteration vectors... [2 x 1.6 MB]
TOTAL: [16.9 MB]

Starting iterations...

Iterative method: 96 iterations in 0.64 seconds (average 0.003125, setup 0.34)

Prob0A: 48 iterations in 0.19 seconds (average 0.003917, setup 0.00)

Prob1E: 4 iterations in 0.03 seconds (average 0.008000, setup 0.00)

yes = 1078, no = 173439, maybe = 35775

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=210292, nc=64991, nnz=185889, k=2] [2.4 MB]
Creating vector for yes... [1.6 MB]
Allocating iteration vectors... [2 x 1.6 MB]
TOTAL: [7.2 MB]

Starting iterations...

Iterative method: 96 iterations in 0.30 seconds (average 0.001375, setup 0.17)

Value in the initial state: 76.55260452686157

Time for model checking: 1.293 seconds.

Result: 76.55260452686157 (value in the initial state)

---------------------------------------------------------------------

Note: There were 16 warnings during computation.

