PRISM
=====

Version: 4.3
Date: Wed Aug 03 16:07:57 BST 2016
Hostname: eglinton
Memory limits: cudd=1g, java(heap)=981.5m
Command line: prism uav_sim_mdp.prism -s uav_mdp.props -const 'Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=0,Objy1=2,Objx2=6,Objy2=0,'

Parsing model file "uav_sim_mdp.prism"...

Parsing properties file "uav_mdp.props"...

18 properties:
(1) Pmax=? [ F "MissionSuccessful" ]
(2) Pmin=? [ F "MissionSuccessful" ]
(3) Pmax=? [ F "MissionTimeUp" ]
(4) Pmin=? [ F "MissionTimeUp" ]
(5) Pmax=? [ F "Crash" ]
(6) Pmin=? [ F "Crash" ]
(7) Pmax=? [ F "AllAreaSearched" ]
(8) Pmin=? [ F "AllAreaSearched" ]
(9) Pmax=? [ F "MissionTimeUp"|"AllAreaSearched"|"Crash" ]
(10) Pmin=? [ F "MissionTimeUp"|"AllAreaSearched"|"Crash" ]
(11) R{"drop"}max=? [ F "deadlock" ]
(12) R{"drop"}min=? [ F "deadlock" ]
(13) R{"charge"}max=? [ F "deadlock" ]
(14) R{"charge"}min=? [ F "deadlock" ]
(15) R{"time"}max=? [ F "deadlock" ]
(16) R{"time"}min=? [ F "deadlock" ]
(17) R{"success"}max=? [ F "deadlock" ]/Pmin=? [ F "MissionSuccessful" ]
(18) R{"success"}min=? [ F "deadlock" ]/Pmax=? [ F "MissionSuccessful" ]

Type:        MDP
Modules:     Obj Move Robot T 
Variables:   NoOfObjs posx posy s1 retx rety b t c 

---------------------------------------------------------------------

Model checking: Pmax=? [ F "MissionSuccessful" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=0,Objy1=2,Objx2=6,Objy2=0

Building model...
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=0,Objy1=2,Objx2=6,Objy2=0

Warning: Guard for command 7 of module "Move" is never satisfied.

Warning: Guard for command 8 of module "Move" is never satisfied.

Warning: Guard for command 9 of module "Move" is never satisfied.

Warning: Guard for command 7 of module "Robot" is never satisfied.

Warning: Update 2 of command 3 of module "Robot" doesn't do anything ("(s1'=13)", line 210, column 44)

Warning: Update 2 of command 27 of module "T" doesn't do anything ("(c'=1)", line 323, column 108)

Warning: Guard for command 28 of module "T" is never satisfied.

Warning: Update 2 of command 9 of module "T" doesn't do anything ("(c'=1)", line 300, column 112)

Warning: Guard for command 10 of module "T" is never satisfied.

Warning: Update 2 of command 51 of module "T" doesn't do anything ("(c'=1)", line 354, column 113)

Warning: Guard for command 52 of module "T" is never satisfied.

Computing reachable states...

Reachability (BFS): 88 iterations in 1.67 seconds (average 0.019000, setup 0.00)

Time for model construction: 5.529 seconds.

Warning: Deadlocks detected and fixed in 1 states

Type:        MDP
States:      281937 (1 initial)
Transitions: 746441
Choices:     372650

Transition matrix: 249454 nodes (91 terminal), 746441 minterms, vars: 34r/34c/18nd

Prob0A: 49 iterations in 0.58 seconds (average 0.011755, setup 0.00)

Prob1E: 4 iterations in 0.08 seconds (average 0.020000, setup 0.00)

yes = 1144, no = 239342, maybe = 41451

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=281937, nc=73063, nnz=208996, k=2] [2.7 MB]
Creating vector for yes... [2.2 MB]
Allocating iteration vectors... [2 x 2.2 MB]
TOTAL: [9.2 MB]

Starting iterations...

Iterative method: 95 iterations in 0.50 seconds (average 0.001979, setup 0.31)

Value in the initial state: 0.982388354295845

Time for model checking: 1.975 seconds.

Result: 0.982388354295845 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmin=? [ F "MissionSuccessful" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=0,Objy1=2,Objx2=6,Objy2=0

Prob0E: 51 iterations in 0.57 seconds (average 0.011137, setup 0.00)

Prob1A: 3 iterations in 0.06 seconds (average 0.020000, setup 0.00)

yes = 1144, no = 246509, maybe = 34284

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=281937, nc=60102, nnz=170690, k=2] [2.3 MB]
Creating vector for yes... [2.2 MB]
Allocating iteration vectors... [2 x 2.2 MB]
TOTAL: [8.7 MB]

Starting iterations...

Iterative method: 88 iterations in 0.36 seconds (average 0.001909, setup 0.19)

Value in the initial state: 0.8125243244314669

Time for model checking: 1.505 seconds.

Result: 0.8125243244314669 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmax=? [ F "MissionTimeUp" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=0,Objy1=2,Objx2=6,Objy2=0

Prob0A: 69 iterations in 0.90 seconds (average 0.012986, setup 0.00)

Prob1E: 4 iterations in 0.07 seconds (average 0.018000, setup 0.00)

yes = 488, no = 173891, maybe = 107558

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=281937, nc=185510, nnz=510537, k=4] [6.3 MB]
Creating vector for yes... [2.2 MB]
Allocating iteration vectors... [2 x 2.2 MB]
TOTAL: [12.7 MB]

Starting iterations...

Iterative method: 95 iterations in 0.64 seconds (average 0.002568, setup 0.40)

Value in the initial state: 5.319091431244135E-4

Time for model checking: 2.212 seconds.

Result: 5.319091431244135E-4 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmin=? [ F "MissionTimeUp" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=0,Objy1=2,Objx2=6,Objy2=0

Prob0E: 83 iterations in 1.28 seconds (average 0.015422, setup 0.00)

Prob1A: 3 iterations in 0.06 seconds (average 0.021333, setup 0.00)

yes = 488, no = 174336, maybe = 107113

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=281937, nc=184717, nnz=508880, k=4] [6.3 MB]
Creating vector for yes... [2.2 MB]
Allocating iteration vectors... [2 x 2.2 MB]
TOTAL: [12.7 MB]

Starting iterations...

Iterative method: 112 iterations in 0.64 seconds (average 0.002571, setup 0.35)

Value in the initial state: 9.76347772260271E-7

Time for model checking: 2.73 seconds.

Result: 9.76347772260271E-7 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmax=? [ F "Crash" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=0,Objy1=2,Objx2=6,Objy2=0

Prob0A: 3 iterations in 0.10 seconds (average 0.034667, setup 0.00)

Prob1E: 187 iterations in 3.76 seconds (average 0.020128, setup 0.00)

yes = 138041, no = 3268, maybe = 140628

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=281937, nc=231341, nnz=605132, k=4] [7.4 MB]
Creating vector for yes... [2.2 MB]
Allocating iteration vectors... [2 x 2.2 MB]
TOTAL: [13.9 MB]

Starting iterations...

Iterative method: 91 iterations in 0.68 seconds (average 0.002725, setup 0.44)

Value in the initial state: 0.02675730635648083

Time for model checking: 6.444 seconds.

Result: 0.02675730635648083 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmin=? [ F "Crash" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=0,Objy1=2,Objx2=6,Objy2=0

Prob0E: 3 iterations in 0.09 seconds (average 0.029333, setup 0.00)

Prob1A: 46 iterations in 0.70 seconds (average 0.015217, setup 0.00)

yes = 138041, no = 3268, maybe = 140628

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=281937, nc=231341, nnz=605132, k=4] [7.4 MB]
Creating vector for yes... [2.2 MB]
Allocating iteration vectors... [2 x 2.2 MB]
TOTAL: [13.9 MB]

Starting iterations...

Iterative method: 86 iterations in 0.64 seconds (average 0.002837, setup 0.39)

Value in the initial state: 0.015562183548094759

Time for model checking: 1.974 seconds.

Result: 0.015562183548094759 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmax=? [ F "AllAreaSearched" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=0,Objy1=2,Objx2=6,Objy2=0

Prob0A: 59 iterations in 0.62 seconds (average 0.010576, setup 0.00)

Prob1E: 4 iterations in 0.07 seconds (average 0.018000, setup 0.00)

yes = 1635, no = 218922, maybe = 61380

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=281937, nc=93616, nnz=243322, k=2] [3.1 MB]
Creating vector for yes... [2.2 MB]
Allocating iteration vectors... [2 x 2.2 MB]
TOTAL: [9.6 MB]

Starting iterations...

Iterative method: 92 iterations in 0.47 seconds (average 0.002304, setup 0.26)

Value in the initial state: 0.16287986805461013

Time for model checking: 1.535 seconds.

Result: 0.16287986805461013 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmin=? [ F "AllAreaSearched" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=0,Objy1=2,Objx2=6,Objy2=0

Prob0E: 64 iterations in 0.63 seconds (average 0.009813, setup 0.00)

Prob1A: 3 iterations in 0.06 seconds (average 0.021333, setup 0.00)

yes = 1635, no = 226220, maybe = 54082

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=281937, nc=81378, nnz=209244, k=2] [2.7 MB]
Creating vector for yes... [2.2 MB]
Allocating iteration vectors... [2 x 2.2 MB]
TOTAL: [9.2 MB]

Starting iterations...

Iterative method: 98 iterations in 0.40 seconds (average 0.002163, setup 0.18)

Value in the initial state: 0.0018150413458747341

Time for model checking: 1.253 seconds.

Result: 0.0018150413458747341 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmax=? [ F "MissionTimeUp"|"AllAreaSearched"|"Crash" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=0,Objy1=2,Objx2=6,Objy2=0

Prob0A: 3 iterations in 0.14 seconds (average 0.048000, setup 0.00)

Prob1E: 196 iterations in 4.26 seconds (average 0.021714, setup 0.00)

yes = 246508, no = 1145, maybe = 34284

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=281937, nc=60102, nnz=170690, k=2] [2.3 MB]
Creating vector for yes... [2.2 MB]
Allocating iteration vectors... [2 x 2.2 MB]
TOTAL: [8.7 MB]

Starting iterations...

Iterative method: 81 iterations in 0.35 seconds (average 0.001778, setup 0.21)

Value in the initial state: 0.1874753809848192

Time for model checking: 5.015 seconds.

Result: 0.1874753809848192 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmin=? [ F "MissionTimeUp"|"AllAreaSearched"|"Crash" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=0,Objy1=2,Objx2=6,Objy2=0

Prob0E: 3 iterations in 0.09 seconds (average 0.030667, setup 0.00)

Prob1A: 49 iterations in 0.62 seconds (average 0.012735, setup 0.00)

yes = 239341, no = 1145, maybe = 41451

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=281937, nc=73063, nnz=208996, k=2] [2.7 MB]
Creating vector for yes... [2.2 MB]
Allocating iteration vectors... [2 x 2.2 MB]
TOTAL: [9.2 MB]

Starting iterations...

Iterative method: 93 iterations in 0.35 seconds (average 0.001849, setup 0.18)

Value in the initial state: 0.0176109429683404

Time for model checking: 1.112 seconds.

Result: 0.0176109429683404 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"drop"}max=? [ F "deadlock" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=0,Objy1=2,Objx2=6,Objy2=0

Prob0E: 4 iterations in 0.08 seconds (average 0.021000, setup 0.00)

Prob1A: 1 iterations in 0.01 seconds (average 0.008000, setup 0.00)

goal = 1, inf = 0, maybe = 281936

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=281937, nc=372649, nnz=746440, k=4] [11.0 MB]
Building sparse matrix (transition rewards)... [n=281937, nc=372649, nnz=0, k=4] [2.5 MB]
Creating vector for state rewards... [2.2 MB]
Creating vector for inf... [2.2 MB]
Allocating iteration vectors... [2 x 2.2 MB]
TOTAL: [22.1 MB]

Starting iterations...

Iterative method: 91 iterations in 0.86 seconds (average 0.004044, setup 0.49)

Value in the initial state: 0.2343605747001899

Time for model checking: 0.969 seconds.

Result: 0.2343605747001899 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"drop"}min=? [ F "deadlock" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=0,Objy1=2,Objx2=6,Objy2=0

Prob0A: 4 iterations in 0.08 seconds (average 0.019000, setup 0.00)

Prob1E: 5 iterations in 0.05 seconds (average 0.010400, setup 0.00)

Warning: PRISM hasn't checked for zero-reward loops.
Your minimum rewards may be too low...

goal = 1, inf = 0, maybe = 281936

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=281937, nc=372649, nnz=746440, k=4] [11.0 MB]
Building sparse matrix (transition rewards)... [n=281937, nc=372649, nnz=0, k=4] [2.5 MB]
Creating vector for state rewards... [2.2 MB]
Creating vector for inf... [2.2 MB]
Allocating iteration vectors... [2 x 2.2 MB]
TOTAL: [22.1 MB]

Starting iterations...

Iterative method: 81 iterations in 0.78 seconds (average 0.004790, setup 0.39)

Value in the initial state: 0.2149467418128548

Time for model checking: 0.917 seconds.

Result: 0.2149467418128548 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"charge"}max=? [ F "deadlock" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=0,Objy1=2,Objx2=6,Objy2=0

Prob0E: 4 iterations in 0.04 seconds (average 0.009000, setup 0.00)

Prob1A: 1 iterations in 0.00 seconds (average 0.004000, setup 0.00)

goal = 1, inf = 0, maybe = 281936

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=281937, nc=372649, nnz=746440, k=4] [11.0 MB]
Building sparse matrix (transition rewards)... [n=281937, nc=372649, nnz=8348, k=4] [2.6 MB]
Creating vector for state rewards... [2.2 MB]
Creating vector for inf... [2.2 MB]
Allocating iteration vectors... [2 x 2.2 MB]
TOTAL: [22.2 MB]

Starting iterations...

Iterative method: 88 iterations in 0.89 seconds (average 0.005045, setup 0.44)

Value in the initial state: 0.3108796127665955

Time for model checking: 0.953 seconds.

Result: 0.3108796127665955 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"charge"}min=? [ F "deadlock" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=0,Objy1=2,Objx2=6,Objy2=0

Prob0A: 4 iterations in 0.03 seconds (average 0.008000, setup 0.00)

Prob1E: 5 iterations in 0.05 seconds (average 0.009600, setup 0.00)

Warning: PRISM hasn't checked for zero-reward loops.
Your minimum rewards may be too low...

goal = 1, inf = 0, maybe = 281936

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=281937, nc=372649, nnz=746440, k=4] [11.0 MB]
Building sparse matrix (transition rewards)... [n=281937, nc=372649, nnz=8348, k=4] [2.6 MB]
Creating vector for state rewards... [2.2 MB]
Creating vector for inf... [2.2 MB]
Allocating iteration vectors... [2 x 2.2 MB]
TOTAL: [22.2 MB]

Starting iterations...

Iterative method: 96 iterations in 0.76 seconds (average 0.003750, setup 0.40)

Value in the initial state: 0.031194103931278493

Time for model checking: 0.854 seconds.

Result: 0.031194103931278493 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"time"}max=? [ F "deadlock" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=0,Objy1=2,Objx2=6,Objy2=0

Prob0E: 4 iterations in 0.03 seconds (average 0.007000, setup 0.00)

Prob1A: 1 iterations in 0.01 seconds (average 0.008000, setup 0.00)

goal = 1, inf = 0, maybe = 281936

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=281937, nc=372649, nnz=746440, k=4] [11.0 MB]
Building sparse matrix (transition rewards)... [n=281937, nc=372649, nnz=141307, k=4] [4.1 MB]
Creating vector for state rewards... [2.2 MB]
Creating vector for inf... [2.2 MB]
Allocating iteration vectors... [2 x 2.2 MB]
TOTAL: [23.8 MB]

Starting iterations...

Iterative method: 94 iterations in 0.89 seconds (average 0.004255, setup 0.49)

Value in the initial state: 147.76756537526552

Time for model checking: 0.939 seconds.

Result: 147.76756537526552 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"time"}min=? [ F "deadlock" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=0,Objy1=2,Objx2=6,Objy2=0

Prob0A: 4 iterations in 0.05 seconds (average 0.013000, setup 0.00)

Prob1E: 5 iterations in 0.06 seconds (average 0.012800, setup 0.00)

Warning: PRISM hasn't checked for zero-reward loops.
Your minimum rewards may be too low...

goal = 1, inf = 0, maybe = 281936

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=281937, nc=372649, nnz=746440, k=4] [11.0 MB]
Building sparse matrix (transition rewards)... [n=281937, nc=372649, nnz=141307, k=4] [4.1 MB]
Creating vector for state rewards... [2.2 MB]
Creating vector for inf... [2.2 MB]
Allocating iteration vectors... [2 x 2.2 MB]
TOTAL: [23.8 MB]

Starting iterations...

Iterative method: 101 iterations in 0.86 seconds (average 0.004119, setup 0.44)

Value in the initial state: 86.30491886108882

Time for model checking: 0.99 seconds.

Result: 86.30491886108882 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"success"}max=? [ F "deadlock" ]/Pmin=? [ F "MissionSuccessful" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=0,Objy1=2,Objx2=6,Objy2=0

Prob0E: 4 iterations in 0.02 seconds (average 0.006000, setup 0.00)

Prob1A: 1 iterations in 0.01 seconds (average 0.008000, setup 0.00)

goal = 1, inf = 0, maybe = 281936

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=281937, nc=372649, nnz=746440, k=4] [11.0 MB]
Building sparse matrix (transition rewards)... [n=281937, nc=372649, nnz=2254, k=4] [2.5 MB]
Creating vector for state rewards... [2.2 MB]
Creating vector for inf... [2.2 MB]
Allocating iteration vectors... [2 x 2.2 MB]
TOTAL: [22.2 MB]

Starting iterations...

Iterative method: 97 iterations in 0.69 seconds (average 0.003588, setup 0.34)

Prob0E: 51 iterations in 0.27 seconds (average 0.005333, setup 0.00)

Prob1A: 3 iterations in 0.04 seconds (average 0.014667, setup 0.00)

yes = 1144, no = 246509, maybe = 34284

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=281937, nc=60102, nnz=170690, k=2] [2.3 MB]
Creating vector for yes... [2.2 MB]
Allocating iteration vectors... [2 x 2.2 MB]
TOTAL: [8.7 MB]

Starting iterations...

Iterative method: 88 iterations in 0.29 seconds (average 0.001591, setup 0.15)

Value in the initial state: 178.4586341874587

Time for model checking: 1.377 seconds.

Result: 178.4586341874587 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"success"}min=? [ F "deadlock" ]/Pmax=? [ F "MissionSuccessful" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=0,Objy1=2,Objx2=6,Objy2=0

Prob0A: 4 iterations in 0.10 seconds (average 0.025000, setup 0.00)

Prob1E: 5 iterations in 0.04 seconds (average 0.008000, setup 0.00)

Warning: PRISM hasn't checked for zero-reward loops.
Your minimum rewards may be too low...

goal = 1, inf = 0, maybe = 281936

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=281937, nc=372649, nnz=746440, k=4] [11.0 MB]
Building sparse matrix (transition rewards)... [n=281937, nc=372649, nnz=2254, k=4] [2.5 MB]
Creating vector for state rewards... [2.2 MB]
Creating vector for inf... [2.2 MB]
Allocating iteration vectors... [2 x 2.2 MB]
TOTAL: [22.2 MB]

Starting iterations...

Iterative method: 92 iterations in 0.74 seconds (average 0.003696, setup 0.40)

Prob0A: 49 iterations in 0.27 seconds (average 0.005469, setup 0.00)

Prob1E: 4 iterations in 0.04 seconds (average 0.009000, setup 0.00)

yes = 1144, no = 239342, maybe = 41451

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=281937, nc=73063, nnz=208996, k=2] [2.7 MB]
Creating vector for yes... [2.2 MB]
Allocating iteration vectors... [2 x 2.2 MB]
TOTAL: [9.2 MB]

Starting iterations...

Iterative method: 95 iterations in 0.33 seconds (average 0.001642, setup 0.18)

Value in the initial state: 77.05356972043953

Time for model checking: 1.553 seconds.

Result: 77.05356972043953 (value in the initial state)

---------------------------------------------------------------------

Note: There were 16 warnings during computation.

