PRISM
=====

Version: 4.3
Date: Wed Aug 03 15:55:32 BST 2016
Hostname: eglinton
Memory limits: cudd=1g, java(heap)=981.5m
Command line: prism uav_sim_mdp.prism -s uav_mdp.props -const 'Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=1,Objy1=2,Objx2=2,Objy2=2,'

Parsing model file "uav_sim_mdp.prism"...

Parsing properties file "uav_mdp.props"...

18 properties:
(1) Pmax=? [ F "MissionSuccessful" ]
(2) Pmin=? [ F "MissionSuccessful" ]
(3) Pmax=? [ F "MissionTimeUp" ]
(4) Pmin=? [ F "MissionTimeUp" ]
(5) Pmax=? [ F "Crash" ]
(6) Pmin=? [ F "Crash" ]
(7) Pmax=? [ F "AllAreaSearched" ]
(8) Pmin=? [ F "AllAreaSearched" ]
(9) Pmax=? [ F "MissionTimeUp"|"AllAreaSearched"|"Crash" ]
(10) Pmin=? [ F "MissionTimeUp"|"AllAreaSearched"|"Crash" ]
(11) R{"drop"}max=? [ F "deadlock" ]
(12) R{"drop"}min=? [ F "deadlock" ]
(13) R{"charge"}max=? [ F "deadlock" ]
(14) R{"charge"}min=? [ F "deadlock" ]
(15) R{"time"}max=? [ F "deadlock" ]
(16) R{"time"}min=? [ F "deadlock" ]
(17) R{"success"}max=? [ F "deadlock" ]/Pmin=? [ F "MissionSuccessful" ]
(18) R{"success"}min=? [ F "deadlock" ]/Pmax=? [ F "MissionSuccessful" ]

Type:        MDP
Modules:     Obj Move Robot T 
Variables:   NoOfObjs posx posy s1 retx rety b t c 

---------------------------------------------------------------------

Model checking: Pmax=? [ F "MissionSuccessful" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=1,Objy1=2,Objx2=2,Objy2=2

Building model...
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=1,Objy1=2,Objx2=2,Objy2=2

Warning: Guard for command 7 of module "Move" is never satisfied.

Warning: Guard for command 8 of module "Move" is never satisfied.

Warning: Guard for command 9 of module "Move" is never satisfied.

Warning: Guard for command 7 of module "Robot" is never satisfied.

Warning: Update 2 of command 3 of module "Robot" doesn't do anything ("(s1'=13)", line 210, column 44)

Warning: Update 2 of command 27 of module "T" doesn't do anything ("(c'=1)", line 323, column 108)

Warning: Guard for command 28 of module "T" is never satisfied.

Warning: Update 2 of command 9 of module "T" doesn't do anything ("(c'=1)", line 300, column 112)

Warning: Guard for command 10 of module "T" is never satisfied.

Warning: Update 2 of command 51 of module "T" doesn't do anything ("(c'=1)", line 354, column 113)

Warning: Guard for command 52 of module "T" is never satisfied.

Computing reachable states...

Reachability (BFS): 88 iterations in 0.85 seconds (average 0.009636, setup 0.00)

Time for model construction: 2.891 seconds.

Warning: Deadlocks detected and fixed in 1 states

Type:        MDP
States:      163420 (1 initial)
Transitions: 423927
Choices:     216710

Transition matrix: 171783 nodes (89 terminal), 423927 minterms, vars: 34r/34c/18nd

Prob0A: 43 iterations in 0.21 seconds (average 0.004930, setup 0.00)

Prob1E: 4 iterations in 0.04 seconds (average 0.009000, setup 0.00)

yes = 1064, no = 134896, maybe = 27460

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=163420, nc=50200, nnz=139734, k=2] [1.8 MB]
Creating vector for yes... [1.2 MB]
Allocating iteration vectors... [2 x 1.2 MB]
TOTAL: [5.5 MB]

Starting iterations...

Iterative method: 83 iterations in 0.26 seconds (average 0.001108, setup 0.17)

Value in the initial state: 0.9849233735313982

Time for model checking: 0.538 seconds.

Result: 0.9849233735313982 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmin=? [ F "MissionSuccessful" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=1,Objy1=2,Objx2=2,Objy2=2

Prob0E: 43 iterations in 0.22 seconds (average 0.005116, setup 0.00)

Prob1A: 3 iterations in 0.03 seconds (average 0.010667, setup 0.00)

yes = 1064, no = 138085, maybe = 24271

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=163420, nc=44281, nnz=122717, k=2] [1.6 MB]
Creating vector for yes... [1.2 MB]
Allocating iteration vectors... [2 x 1.2 MB]
TOTAL: [5.3 MB]

Starting iterations...

Iterative method: 83 iterations in 0.22 seconds (average 0.001108, setup 0.12)

Value in the initial state: 0.9497929485595552

Time for model checking: 0.485 seconds.

Result: 0.9497929485595552 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmax=? [ F "MissionTimeUp" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=1,Objy1=2,Objx2=2,Objy2=2

Prob0A: 76 iterations in 0.46 seconds (average 0.006000, setup 0.00)

Prob1E: 4 iterations in 0.05 seconds (average 0.013000, setup 0.00)

yes = 248, no = 110151, maybe = 53021

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=163420, nc=94789, nnz=260216, k=4] [3.2 MB]
Creating vector for yes... [1.2 MB]
Allocating iteration vectors... [2 x 1.2 MB]
TOTAL: [7.0 MB]

Starting iterations...

Iterative method: 94 iterations in 0.34 seconds (average 0.001319, setup 0.22)

Value in the initial state: 1.257775906787271E-5

Time for model checking: 0.866 seconds.

Result: 1.257775906787271E-5 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmin=? [ F "MissionTimeUp" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=1,Objy1=2,Objx2=2,Objy2=2

Prob0E: 90 iterations in 0.58 seconds (average 0.006400, setup 0.00)

Prob1A: 3 iterations in 0.03 seconds (average 0.010667, setup 0.00)

yes = 248, no = 110474, maybe = 52698

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=163420, nc=94226, nnz=258921, k=4] [3.2 MB]
Creating vector for yes... [1.2 MB]
Allocating iteration vectors... [2 x 1.2 MB]
TOTAL: [6.9 MB]

Starting iterations...

Iterative method: 112 iterations in 0.35 seconds (average 0.001357, setup 0.20)

Value in the initial state: 3.003578686542101E-9

Time for model checking: 0.974 seconds.

Result: 3.003578686542101E-9 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmax=? [ F "Crash" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=1,Objy1=2,Objx2=2,Objy2=2

Prob0A: 3 iterations in 0.06 seconds (average 0.021333, setup 0.00)

Prob1E: 160 iterations in 1.71 seconds (average 0.010700, setup 0.00)

yes = 80271, no = 2735, maybe = 80414

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=163420, nc=133704, nnz=340921, k=4] [4.2 MB]
Creating vector for yes... [1.2 MB]
Allocating iteration vectors... [2 x 1.2 MB]
TOTAL: [7.9 MB]

Starting iterations...

Iterative method: 85 iterations in 0.42 seconds (average 0.001506, setup 0.29)

Value in the initial state: 0.021027596922817245

Time for model checking: 2.378 seconds.

Result: 0.021027596922817245 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmin=? [ F "Crash" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=1,Objy1=2,Objx2=2,Objy2=2

Prob0E: 4 iterations in 0.06 seconds (average 0.015000, setup 0.00)

Prob1A: 43 iterations in 0.35 seconds (average 0.008186, setup 0.00)

yes = 80271, no = 2735, maybe = 80414

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=163420, nc=133704, nnz=340921, k=4] [4.2 MB]
Creating vector for yes... [1.2 MB]
Allocating iteration vectors... [2 x 1.2 MB]
TOTAL: [7.9 MB]

Starting iterations...

Iterative method: 76 iterations in 0.33 seconds (average 0.001474, setup 0.22)

Value in the initial state: 0.013020596498960748

Time for model checking: 0.885 seconds.

Result: 0.013020596498960748 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmax=? [ F "AllAreaSearched" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=1,Objy1=2,Objx2=2,Objy2=2

Prob0A: 53 iterations in 0.33 seconds (average 0.006189, setup 0.00)

Prob1E: 4 iterations in 0.03 seconds (average 0.008000, setup 0.00)

yes = 1422, no = 115422, maybe = 46576

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=163420, nc=73175, nnz=189327, k=2] [2.4 MB]
Creating vector for yes... [1.2 MB]
Allocating iteration vectors... [2 x 1.2 MB]
TOTAL: [6.1 MB]

Starting iterations...

Iterative method: 85 iterations in 0.27 seconds (average 0.001224, setup 0.17)

Value in the initial state: 0.03116366869212947

Time for model checking: 0.676 seconds.

Result: 0.03116366869212947 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmin=? [ F "AllAreaSearched" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=1,Objy1=2,Objx2=2,Objy2=2

Prob0E: 53 iterations in 0.34 seconds (average 0.006340, setup 0.00)

Prob1A: 3 iterations in 0.03 seconds (average 0.010667, setup 0.00)

yes = 1422, no = 120394, maybe = 41604

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=163420, nc=63951, nnz=162659, k=2] [2.1 MB]
Creating vector for yes... [1.2 MB]
Allocating iteration vectors... [2 x 1.2 MB]
TOTAL: [5.8 MB]

Starting iterations...

Iterative method: 93 iterations in 0.24 seconds (average 0.001161, setup 0.13)

Value in the initial state: 0.0019438703583251626

Time for model checking: 0.669 seconds.

Result: 0.0019438703583251626 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmax=? [ F "MissionTimeUp"|"AllAreaSearched"|"Crash" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=1,Objy1=2,Objx2=2,Objy2=2

Prob0A: 3 iterations in 0.06 seconds (average 0.018667, setup 0.00)

Prob1E: 164 iterations in 1.58 seconds (average 0.009634, setup 0.00)

yes = 138084, no = 1065, maybe = 24271

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=163420, nc=44281, nnz=122717, k=2] [1.6 MB]
Creating vector for yes... [1.2 MB]
Allocating iteration vectors... [2 x 1.2 MB]
TOTAL: [5.3 MB]

Starting iterations...

Iterative method: 78 iterations in 0.24 seconds (average 0.001026, setup 0.16)

Value in the initial state: 0.05020644187598494

Time for model checking: 2.212 seconds.

Result: 0.05020644187598494 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmin=? [ F "MissionTimeUp"|"AllAreaSearched"|"Crash" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=1,Objy1=2,Objx2=2,Objy2=2

Prob0E: 4 iterations in 0.04 seconds (average 0.010000, setup 0.00)

Prob1A: 43 iterations in 0.30 seconds (average 0.006884, setup 0.00)

yes = 134895, no = 1065, maybe = 27460

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=163420, nc=50200, nnz=139734, k=2] [1.8 MB]
Creating vector for yes... [1.2 MB]
Allocating iteration vectors... [2 x 1.2 MB]
TOTAL: [5.5 MB]

Starting iterations...

Iterative method: 78 iterations in 0.20 seconds (average 0.001128, setup 0.11)

Value in the initial state: 0.01507529252593881

Time for model checking: 0.603 seconds.

Result: 0.01507529252593881 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"drop"}max=? [ F "deadlock" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=1,Objy1=2,Objx2=2,Objy2=2

Prob0E: 5 iterations in 0.05 seconds (average 0.009600, setup 0.00)

Prob1A: 1 iterations in 0.00 seconds (average 0.004000, setup 0.00)

goal = 1, inf = 0, maybe = 163419

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=163420, nc=216709, nnz=423926, k=4] [6.3 MB]
Building sparse matrix (transition rewards)... [n=163420, nc=216709, nnz=0, k=4] [1.5 MB]
Creating vector for state rewards... [1.2 MB]
Creating vector for inf... [1.2 MB]
Allocating iteration vectors... [2 x 1.2 MB]
TOTAL: [12.7 MB]

Starting iterations...

Iterative method: 79 iterations in 0.50 seconds (average 0.002278, setup 0.32)

Value in the initial state: 0.11437487106982315

Time for model checking: 0.572 seconds.

Result: 0.11437487106982315 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"drop"}min=? [ F "deadlock" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=1,Objy1=2,Objx2=2,Objy2=2

Prob0A: 4 iterations in 0.03 seconds (average 0.008000, setup 0.00)

Prob1E: 5 iterations in 0.04 seconds (average 0.008000, setup 0.00)

Warning: PRISM hasn't checked for zero-reward loops.
Your minimum rewards may be too low...

goal = 1, inf = 0, maybe = 163419

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=163420, nc=216709, nnz=423926, k=4] [6.3 MB]
Building sparse matrix (transition rewards)... [n=163420, nc=216709, nnz=0, k=4] [1.5 MB]
Creating vector for state rewards... [1.2 MB]
Creating vector for inf... [1.2 MB]
Allocating iteration vectors... [2 x 1.2 MB]
TOTAL: [12.7 MB]

Starting iterations...

Iterative method: 74 iterations in 0.41 seconds (average 0.002378, setup 0.23)

Value in the initial state: 0.10853035592500138

Time for model checking: 0.489 seconds.

Result: 0.10853035592500138 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"charge"}max=? [ F "deadlock" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=1,Objy1=2,Objx2=2,Objy2=2

Prob0E: 5 iterations in 0.02 seconds (average 0.003200, setup 0.00)

Prob1A: 1 iterations in 0.01 seconds (average 0.008000, setup 0.00)

goal = 1, inf = 0, maybe = 163419

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=163420, nc=216709, nnz=423926, k=4] [6.3 MB]
Building sparse matrix (transition rewards)... [n=163420, nc=216709, nnz=4278, k=4] [1.5 MB]
Creating vector for state rewards... [1.2 MB]
Creating vector for inf... [1.2 MB]
Allocating iteration vectors... [2 x 1.2 MB]
TOTAL: [12.8 MB]

Starting iterations...

Iterative method: 83 iterations in 0.42 seconds (average 0.002313, setup 0.23)

Value in the initial state: 0.11618977423074352

Time for model checking: 0.466 seconds.

Result: 0.11618977423074352 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"charge"}min=? [ F "deadlock" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=1,Objy1=2,Objx2=2,Objy2=2

Prob0A: 4 iterations in 0.02 seconds (average 0.006000, setup 0.00)

Prob1E: 5 iterations in 0.04 seconds (average 0.008000, setup 0.00)

Warning: PRISM hasn't checked for zero-reward loops.
Your minimum rewards may be too low...

goal = 1, inf = 0, maybe = 163419

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=163420, nc=216709, nnz=423926, k=4] [6.3 MB]
Building sparse matrix (transition rewards)... [n=163420, nc=216709, nnz=4278, k=4] [1.5 MB]
Creating vector for state rewards... [1.2 MB]
Creating vector for inf... [1.2 MB]
Allocating iteration vectors... [2 x 1.2 MB]
TOTAL: [12.8 MB]

Starting iterations...

Iterative method: 70 iterations in 0.40 seconds (average 0.002400, setup 0.23)

Value in the initial state: 0.003734758543776971

Time for model checking: 0.47 seconds.

Result: 0.003734758543776971 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"time"}max=? [ F "deadlock" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=1,Objy1=2,Objx2=2,Objy2=2

Prob0E: 5 iterations in 0.02 seconds (average 0.004800, setup 0.00)

Prob1A: 1 iterations in 0.00 seconds (average 0.004000, setup 0.00)

goal = 1, inf = 0, maybe = 163419

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=163420, nc=216709, nnz=423926, k=4] [6.3 MB]
Building sparse matrix (transition rewards)... [n=163420, nc=216709, nnz=83004, k=4] [2.4 MB]
Creating vector for state rewards... [1.2 MB]
Creating vector for inf... [1.2 MB]
Allocating iteration vectors... [2 x 1.2 MB]
TOTAL: [13.7 MB]

Starting iterations...

Iterative method: 89 iterations in 0.47 seconds (average 0.002427, setup 0.26)

Value in the initial state: 116.36863621843523

Time for model checking: 0.514 seconds.

Result: 116.36863621843523 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"time"}min=? [ F "deadlock" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=1,Objy1=2,Objx2=2,Objy2=2

Prob0A: 4 iterations in 0.02 seconds (average 0.005000, setup 0.00)

Prob1E: 5 iterations in 0.04 seconds (average 0.007200, setup 0.00)

Warning: PRISM hasn't checked for zero-reward loops.
Your minimum rewards may be too low...

goal = 1, inf = 0, maybe = 163419

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=163420, nc=216709, nnz=423926, k=4] [6.3 MB]
Building sparse matrix (transition rewards)... [n=163420, nc=216709, nnz=83004, k=4] [2.4 MB]
Creating vector for state rewards... [1.2 MB]
Creating vector for inf... [1.2 MB]
Allocating iteration vectors... [2 x 1.2 MB]
TOTAL: [13.7 MB]

Starting iterations...

Iterative method: 87 iterations in 0.46 seconds (average 0.002437, setup 0.24)

Value in the initial state: 72.29541186936129

Time for model checking: 0.536 seconds.

Result: 72.29541186936129 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"success"}max=? [ F "deadlock" ]/Pmin=? [ F "MissionSuccessful" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=1,Objy1=2,Objx2=2,Objy2=2

Prob0E: 5 iterations in 0.02 seconds (average 0.004800, setup 0.00)

Prob1A: 1 iterations in 0.00 seconds (average 0.004000, setup 0.00)

goal = 1, inf = 0, maybe = 163419

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=163420, nc=216709, nnz=423926, k=4] [6.3 MB]
Building sparse matrix (transition rewards)... [n=163420, nc=216709, nnz=2101, k=4] [1.5 MB]
Creating vector for state rewards... [1.2 MB]
Creating vector for inf... [1.2 MB]
Allocating iteration vectors... [2 x 1.2 MB]
TOTAL: [12.8 MB]

Starting iterations...

Iterative method: 88 iterations in 0.44 seconds (average 0.002364, setup 0.23)

Prob0E: 43 iterations in 0.21 seconds (average 0.004930, setup 0.00)

Prob1A: 3 iterations in 0.03 seconds (average 0.009333, setup 0.00)

yes = 1064, no = 138085, maybe = 24271

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=163420, nc=44281, nnz=122717, k=2] [1.6 MB]
Creating vector for yes... [1.2 MB]
Allocating iteration vectors... [2 x 1.2 MB]
TOTAL: [5.3 MB]

Starting iterations...

Iterative method: 83 iterations in 0.17 seconds (average 0.001060, setup 0.08)

Value in the initial state: 120.8424733720457

Time for model checking: 0.907 seconds.

Result: 120.8424733720457 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"success"}min=? [ F "deadlock" ]/Pmax=? [ F "MissionSuccessful" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=1,Objy1=2,Objx2=2,Objy2=2

Prob0A: 4 iterations in 0.02 seconds (average 0.006000, setup 0.00)

Prob1E: 5 iterations in 0.04 seconds (average 0.008000, setup 0.00)

Warning: PRISM hasn't checked for zero-reward loops.
Your minimum rewards may be too low...

goal = 1, inf = 0, maybe = 163419

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=163420, nc=216709, nnz=423926, k=4] [6.3 MB]
Building sparse matrix (transition rewards)... [n=163420, nc=216709, nnz=2101, k=4] [1.5 MB]
Creating vector for state rewards... [1.2 MB]
Creating vector for inf... [1.2 MB]
Allocating iteration vectors... [2 x 1.2 MB]
TOTAL: [12.8 MB]

Starting iterations...

Iterative method: 86 iterations in 0.44 seconds (average 0.002326, setup 0.24)

Prob0A: 43 iterations in 0.20 seconds (average 0.004744, setup 0.00)

Prob1E: 4 iterations in 0.02 seconds (average 0.006000, setup 0.00)

yes = 1064, no = 134896, maybe = 27460

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=163420, nc=50200, nnz=139734, k=2] [1.8 MB]
Creating vector for yes... [1.2 MB]
Allocating iteration vectors... [2 x 1.2 MB]
TOTAL: [5.5 MB]

Starting iterations...

Iterative method: 83 iterations in 0.19 seconds (average 0.001108, setup 0.10)

Value in the initial state: 71.4498575239171

Time for model checking: 0.948 seconds.

Result: 71.4498575239171 (value in the initial state)

---------------------------------------------------------------------

Note: There were 16 warnings during computation.

