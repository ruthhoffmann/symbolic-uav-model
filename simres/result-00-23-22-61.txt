PRISM
=====

Version: 4.3
Date: Wed Aug 03 15:57:54 BST 2016
Hostname: eglinton
Memory limits: cudd=1g, java(heap)=981.5m
Command line: prism uav_sim_mdp.prism -s uav_mdp.props -const 'Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=2,Objy1=2,Objx2=6,Objy2=1,'

Parsing model file "uav_sim_mdp.prism"...

Parsing properties file "uav_mdp.props"...

18 properties:
(1) Pmax=? [ F "MissionSuccessful" ]
(2) Pmin=? [ F "MissionSuccessful" ]
(3) Pmax=? [ F "MissionTimeUp" ]
(4) Pmin=? [ F "MissionTimeUp" ]
(5) Pmax=? [ F "Crash" ]
(6) Pmin=? [ F "Crash" ]
(7) Pmax=? [ F "AllAreaSearched" ]
(8) Pmin=? [ F "AllAreaSearched" ]
(9) Pmax=? [ F "MissionTimeUp"|"AllAreaSearched"|"Crash" ]
(10) Pmin=? [ F "MissionTimeUp"|"AllAreaSearched"|"Crash" ]
(11) R{"drop"}max=? [ F "deadlock" ]
(12) R{"drop"}min=? [ F "deadlock" ]
(13) R{"charge"}max=? [ F "deadlock" ]
(14) R{"charge"}min=? [ F "deadlock" ]
(15) R{"time"}max=? [ F "deadlock" ]
(16) R{"time"}min=? [ F "deadlock" ]
(17) R{"success"}max=? [ F "deadlock" ]/Pmin=? [ F "MissionSuccessful" ]
(18) R{"success"}min=? [ F "deadlock" ]/Pmax=? [ F "MissionSuccessful" ]

Type:        MDP
Modules:     Obj Move Robot T 
Variables:   NoOfObjs posx posy s1 retx rety b t c 

---------------------------------------------------------------------

Model checking: Pmax=? [ F "MissionSuccessful" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=2,Objy1=2,Objx2=6,Objy2=1

Building model...
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=2,Objy1=2,Objx2=6,Objy2=1

Warning: Guard for command 7 of module "Move" is never satisfied.

Warning: Guard for command 8 of module "Move" is never satisfied.

Warning: Guard for command 9 of module "Move" is never satisfied.

Warning: Guard for command 7 of module "Robot" is never satisfied.

Warning: Update 2 of command 3 of module "Robot" doesn't do anything ("(s1'=13)", line 210, column 44)

Warning: Update 2 of command 27 of module "T" doesn't do anything ("(c'=1)", line 323, column 108)

Warning: Guard for command 28 of module "T" is never satisfied.

Warning: Update 2 of command 9 of module "T" doesn't do anything ("(c'=1)", line 300, column 112)

Warning: Guard for command 10 of module "T" is never satisfied.

Warning: Update 2 of command 51 of module "T" doesn't do anything ("(c'=1)", line 354, column 113)

Warning: Guard for command 52 of module "T" is never satisfied.

Computing reachable states...

Reachability (BFS): 89 iterations in 1.50 seconds (average 0.016854, setup 0.00)

Time for model construction: 3.641 seconds.

Warning: Deadlocks detected and fixed in 1 states

Type:        MDP
States:      271772 (1 initial)
Transitions: 706729
Choices:     357480

Transition matrix: 229278 nodes (97 terminal), 706729 minterms, vars: 34r/34c/18nd

Prob0A: 49 iterations in 0.47 seconds (average 0.009633, setup 0.00)

Prob1E: 4 iterations in 0.07 seconds (average 0.018000, setup 0.00)

yes = 1218, no = 230583, maybe = 39971

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=271772, nc=69556, nnz=195577, k=2] [2.6 MB]
Creating vector for yes... [2.1 MB]
Allocating iteration vectors... [2 x 2.1 MB]
TOTAL: [8.8 MB]

Starting iterations...

Iterative method: 96 iterations in 0.46 seconds (average 0.001833, setup 0.28)

Value in the initial state: 0.9829251215179268

Time for model checking: 1.032 seconds.

Result: 0.9829251215179268 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmin=? [ F "MissionSuccessful" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=2,Objy1=2,Objx2=6,Objy2=1

Prob0E: 51 iterations in 0.52 seconds (average 0.010118, setup 0.00)

Prob1A: 3 iterations in 0.08 seconds (average 0.026667, setup 0.00)

yes = 1218, no = 238556, maybe = 31998

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=271772, nc=55254, nnz=153320, k=2] [2.1 MB]
Creating vector for yes... [2.1 MB]
Allocating iteration vectors... [2 x 2.1 MB]
TOTAL: [8.3 MB]

Starting iterations...

Iterative method: 89 iterations in 0.38 seconds (average 0.001708, setup 0.23)

Value in the initial state: 0.8311055228159139

Time for model checking: 0.993 seconds.

Result: 0.8311055228159139 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmax=? [ F "MissionTimeUp" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=2,Objy1=2,Objx2=6,Objy2=1

Prob0A: 69 iterations in 0.83 seconds (average 0.012000, setup 0.00)

Prob1E: 4 iterations in 0.06 seconds (average 0.016000, setup 0.00)

yes = 448, no = 167487, maybe = 103837

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=271772, nc=176390, nnz=477900, k=4] [5.9 MB]
Creating vector for yes... [2.1 MB]
Allocating iteration vectors... [2 x 2.1 MB]
TOTAL: [12.1 MB]

Starting iterations...

Iterative method: 95 iterations in 0.58 seconds (average 0.002316, setup 0.36)

Value in the initial state: 4.0562214297853284E-4

Time for model checking: 1.509 seconds.

Result: 4.0562214297853284E-4 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmin=? [ F "MissionTimeUp" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=2,Objy1=2,Objx2=6,Objy2=1

Prob0E: 84 iterations in 0.98 seconds (average 0.011667, setup 0.00)

Prob1A: 3 iterations in 0.06 seconds (average 0.020000, setup 0.00)

yes = 448, no = 167959, maybe = 103365

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=271772, nc=175557, nnz=476076, k=4] [5.9 MB]
Creating vector for yes... [2.1 MB]
Allocating iteration vectors... [2 x 2.1 MB]
TOTAL: [12.1 MB]

Starting iterations...

Iterative method: 113 iterations in 0.58 seconds (average 0.002407, setup 0.31)

Value in the initial state: 6.634439551490912E-7

Time for model checking: 1.636 seconds.

Result: 6.634439551490912E-7 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmax=? [ F "Crash" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=2,Objy1=2,Objx2=6,Objy2=1

Prob0A: 3 iterations in 0.11 seconds (average 0.037333, setup 0.00)

Prob1E: 187 iterations in 3.18 seconds (average 0.017005, setup 0.00)

yes = 133247, no = 3306, maybe = 135219

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=271772, nc=220927, nnz=570176, k=4] [7.0 MB]
Creating vector for yes... [2.1 MB]
Allocating iteration vectors... [2 x 2.1 MB]
TOTAL: [13.2 MB]

Starting iterations...

Iterative method: 92 iterations in 0.62 seconds (average 0.002609, setup 0.38)

Value in the initial state: 0.02563361011319301

Time for model checking: 3.932 seconds.

Result: 0.02563361011319301 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmin=? [ F "Crash" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=2,Objy1=2,Objx2=6,Objy2=1

Prob0E: 4 iterations in 0.10 seconds (average 0.025000, setup 0.00)

Prob1A: 46 iterations in 0.64 seconds (average 0.013913, setup 0.00)

yes = 133247, no = 3306, maybe = 135219

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=271772, nc=220927, nnz=570176, k=4] [7.0 MB]
Creating vector for yes... [2.1 MB]
Allocating iteration vectors... [2 x 2.1 MB]
TOTAL: [13.2 MB]

Starting iterations...

Iterative method: 86 iterations in 0.54 seconds (average 0.002651, setup 0.31)

Value in the initial state: 0.015032635461459994

Time for model checking: 1.304 seconds.

Result: 0.015032635461459994 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmax=? [ F "AllAreaSearched" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=2,Objy1=2,Objx2=6,Objy2=1

Prob0A: 57 iterations in 0.56 seconds (average 0.009754, setup 0.00)

Prob1E: 4 iterations in 0.06 seconds (average 0.015000, setup 0.00)

yes = 1639, no = 214961, maybe = 55172

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=271772, nc=82964, nnz=209969, k=2] [2.7 MB]
Creating vector for yes... [2.1 MB]
Allocating iteration vectors... [2 x 2.1 MB]
TOTAL: [9.0 MB]

Starting iterations...

Iterative method: 87 iterations in 0.37 seconds (average 0.001885, setup 0.20)

Value in the initial state: 0.14510631607619623

Time for model checking: 1.008 seconds.

Result: 0.14510631607619623 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmin=? [ F "AllAreaSearched" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=2,Objy1=2,Objx2=6,Objy2=1

Prob0E: 63 iterations in 0.52 seconds (average 0.008317, setup 0.00)

Prob1A: 3 iterations in 0.06 seconds (average 0.018667, setup 0.00)

yes = 1639, no = 222338, maybe = 47795

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=271772, nc=71152, nnz=177807, k=2] [2.4 MB]
Creating vector for yes... [2.1 MB]
Allocating iteration vectors... [2 x 2.1 MB]
TOTAL: [8.6 MB]

Starting iterations...

Iterative method: 95 iterations in 0.33 seconds (average 0.001853, setup 0.16)

Value in the initial state: 0.001830808781527636

Time for model checking: 0.935 seconds.

Result: 0.001830808781527636 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmax=? [ F "MissionTimeUp"|"AllAreaSearched"|"Crash" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=2,Objy1=2,Objx2=6,Objy2=1

Prob0A: 3 iterations in 0.09 seconds (average 0.029333, setup 0.00)

Prob1E: 196 iterations in 2.84 seconds (average 0.014490, setup 0.00)

yes = 238555, no = 1219, maybe = 31998

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=271772, nc=55254, nnz=153320, k=2] [2.1 MB]
Creating vector for yes... [2.1 MB]
Allocating iteration vectors... [2 x 2.1 MB]
TOTAL: [8.3 MB]

Starting iterations...

Iterative method: 83 iterations in 0.35 seconds (average 0.001735, setup 0.21)

Value in the initial state: 0.16889428311711596

Time for model checking: 3.329 seconds.

Result: 0.16889428311711596 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmin=? [ F "MissionTimeUp"|"AllAreaSearched"|"Crash" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=2,Objy1=2,Objx2=6,Objy2=1

Prob0E: 4 iterations in 0.10 seconds (average 0.024000, setup 0.00)

Prob1A: 49 iterations in 0.53 seconds (average 0.010776, setup 0.00)

yes = 230582, no = 1219, maybe = 39971

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=271772, nc=69556, nnz=195577, k=2] [2.6 MB]
Creating vector for yes... [2.1 MB]
Allocating iteration vectors... [2 x 2.1 MB]
TOTAL: [8.8 MB]

Starting iterations...

Iterative method: 95 iterations in 0.38 seconds (average 0.001853, setup 0.20)

Value in the initial state: 0.0170741432803486

Time for model checking: 1.04 seconds.

Result: 0.0170741432803486 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"drop"}max=? [ F "deadlock" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=2,Objy1=2,Objx2=6,Objy2=1

Prob0E: 5 iterations in 0.08 seconds (average 0.016800, setup 0.00)

Prob1A: 1 iterations in 0.01 seconds (average 0.008000, setup 0.00)

goal = 1, inf = 0, maybe = 271771

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=271772, nc=357479, nnz=706728, k=4] [10.5 MB]
Building sparse matrix (transition rewards)... [n=271772, nc=357479, nnz=0, k=4] [2.4 MB]
Creating vector for state rewards... [2.1 MB]
Creating vector for inf... [2.1 MB]
Allocating iteration vectors... [2 x 2.1 MB]
TOTAL: [21.2 MB]

Starting iterations...

Iterative method: 91 iterations in 0.87 seconds (average 0.003956, setup 0.51)

Value in the initial state: 0.2011907697247105

Time for model checking: 0.973 seconds.

Result: 0.2011907697247105 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"drop"}min=? [ F "deadlock" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=2,Objy1=2,Objx2=6,Objy2=1

Prob0A: 4 iterations in 0.05 seconds (average 0.013000, setup 0.00)

Prob1E: 5 iterations in 0.05 seconds (average 0.010400, setup 0.00)

Warning: PRISM hasn't checked for zero-reward loops.
Your minimum rewards may be too low...

goal = 1, inf = 0, maybe = 271771

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=271772, nc=357479, nnz=706728, k=4] [10.5 MB]
Building sparse matrix (transition rewards)... [n=271772, nc=357479, nnz=0, k=4] [2.4 MB]
Creating vector for state rewards... [2.1 MB]
Creating vector for inf... [2.1 MB]
Allocating iteration vectors... [2 x 2.1 MB]
TOTAL: [21.2 MB]

Starting iterations...

Iterative method: 81 iterations in 0.69 seconds (average 0.003951, setup 0.37)

Value in the initial state: 0.19152650804316806

Time for model checking: 0.804 seconds.

Result: 0.19152650804316806 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"charge"}max=? [ F "deadlock" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=2,Objy1=2,Objx2=6,Objy2=1

Prob0E: 5 iterations in 0.04 seconds (average 0.007200, setup 0.00)

Prob1A: 1 iterations in 0.00 seconds (average 0.004000, setup 0.00)

goal = 1, inf = 0, maybe = 271771

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=271772, nc=357479, nnz=706728, k=4] [10.5 MB]
Building sparse matrix (transition rewards)... [n=271772, nc=357479, nnz=7972, k=4] [2.5 MB]
Creating vector for state rewards... [2.1 MB]
Creating vector for inf... [2.1 MB]
Allocating iteration vectors... [2 x 2.1 MB]
TOTAL: [21.3 MB]

Starting iterations...

Iterative method: 91 iterations in 0.72 seconds (average 0.003912, setup 0.37)

Value in the initial state: 0.19498685126343163

Time for model checking: 0.783 seconds.

Result: 0.19498685126343163 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"charge"}min=? [ F "deadlock" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=2,Objy1=2,Objx2=6,Objy2=1

Prob0A: 4 iterations in 0.03 seconds (average 0.007000, setup 0.00)

Prob1E: 5 iterations in 0.05 seconds (average 0.010400, setup 0.00)

Warning: PRISM hasn't checked for zero-reward loops.
Your minimum rewards may be too low...

goal = 1, inf = 0, maybe = 271771

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=271772, nc=357479, nnz=706728, k=4] [10.5 MB]
Building sparse matrix (transition rewards)... [n=271772, nc=357479, nnz=7972, k=4] [2.5 MB]
Creating vector for state rewards... [2.1 MB]
Creating vector for inf... [2.1 MB]
Allocating iteration vectors... [2 x 2.1 MB]
TOTAL: [21.3 MB]

Starting iterations...

Iterative method: 98 iterations in 0.76 seconds (average 0.004000, setup 0.37)

Value in the initial state: 0.023298740077317668

Time for model checking: 0.85 seconds.

Result: 0.023298740077317668 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"time"}max=? [ F "deadlock" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=2,Objy1=2,Objx2=6,Objy2=1

Prob0E: 5 iterations in 0.04 seconds (average 0.007200, setup 0.00)

Prob1A: 1 iterations in 0.00 seconds (average 0.004000, setup 0.00)

goal = 1, inf = 0, maybe = 271771

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=271772, nc=357479, nnz=706728, k=4] [10.5 MB]
Building sparse matrix (transition rewards)... [n=271772, nc=357479, nnz=136551, k=4] [4.0 MB]
Creating vector for state rewards... [2.1 MB]
Creating vector for inf... [2.1 MB]
Allocating iteration vectors... [2 x 2.1 MB]
TOTAL: [22.7 MB]

Starting iterations...

Iterative method: 94 iterations in 0.78 seconds (average 0.004000, setup 0.41)

Value in the initial state: 141.61559740820587

Time for model checking: 0.849 seconds.

Result: 141.61559740820587 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"time"}min=? [ F "deadlock" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=2,Objy1=2,Objx2=6,Objy2=1

Prob0A: 4 iterations in 0.02 seconds (average 0.005000, setup 0.00)

Prob1E: 5 iterations in 0.05 seconds (average 0.009600, setup 0.00)

Warning: PRISM hasn't checked for zero-reward loops.
Your minimum rewards may be too low...

goal = 1, inf = 0, maybe = 271771

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=271772, nc=357479, nnz=706728, k=4] [10.5 MB]
Building sparse matrix (transition rewards)... [n=271772, nc=357479, nnz=136551, k=4] [4.0 MB]
Creating vector for state rewards... [2.1 MB]
Creating vector for inf... [2.1 MB]
Allocating iteration vectors... [2 x 2.1 MB]
TOTAL: [22.7 MB]

Starting iterations...

Iterative method: 102 iterations in 0.82 seconds (average 0.004039, setup 0.40)

Value in the initial state: 83.38987521087255

Time for model checking: 0.91 seconds.

Result: 83.38987521087255 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"success"}max=? [ F "deadlock" ]/Pmin=? [ F "MissionSuccessful" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=2,Objy1=2,Objx2=6,Objy2=1

Prob0E: 5 iterations in 0.03 seconds (average 0.006400, setup 0.00)

Prob1A: 1 iterations in 0.00 seconds (average 0.004000, setup 0.00)

goal = 1, inf = 0, maybe = 271771

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=271772, nc=357479, nnz=706728, k=4] [10.5 MB]
Building sparse matrix (transition rewards)... [n=271772, nc=357479, nnz=2398, k=4] [2.4 MB]
Creating vector for state rewards... [2.1 MB]
Creating vector for inf... [2.1 MB]
Allocating iteration vectors... [2 x 2.1 MB]
TOTAL: [21.2 MB]

Starting iterations...

Iterative method: 98 iterations in 0.74 seconds (average 0.003918, setup 0.36)

Prob0E: 51 iterations in 0.38 seconds (average 0.007451, setup 0.00)

Prob1A: 3 iterations in 0.05 seconds (average 0.017333, setup 0.00)

yes = 1218, no = 238556, maybe = 31998

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=271772, nc=55254, nnz=153320, k=2] [2.1 MB]
Creating vector for yes... [2.1 MB]
Allocating iteration vectors... [2 x 2.1 MB]
TOTAL: [8.3 MB]

Starting iterations...

Iterative method: 89 iterations in 0.30 seconds (average 0.001618, setup 0.16)

Value in the initial state: 167.28512812644252

Time for model checking: 1.566 seconds.

Result: 167.28512812644252 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"success"}min=? [ F "deadlock" ]/Pmax=? [ F "MissionSuccessful" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=2,Objy1=2,Objx2=6,Objy2=1

Prob0A: 4 iterations in 0.08 seconds (average 0.019000, setup 0.00)

Prob1E: 5 iterations in 0.04 seconds (average 0.008000, setup 0.00)

Warning: PRISM hasn't checked for zero-reward loops.
Your minimum rewards may be too low...

goal = 1, inf = 0, maybe = 271771

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=271772, nc=357479, nnz=706728, k=4] [10.5 MB]
Building sparse matrix (transition rewards)... [n=271772, nc=357479, nnz=2398, k=4] [2.4 MB]
Creating vector for state rewards... [2.1 MB]
Creating vector for inf... [2.1 MB]
Allocating iteration vectors... [2 x 2.1 MB]
TOTAL: [21.2 MB]

Starting iterations...

Iterative method: 93 iterations in 0.71 seconds (average 0.003527, setup 0.38)

Prob0A: 49 iterations in 0.28 seconds (average 0.005714, setup 0.00)

Prob1E: 4 iterations in 0.04 seconds (average 0.009000, setup 0.00)

yes = 1218, no = 230583, maybe = 39971

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=271772, nc=69556, nnz=195577, k=2] [2.6 MB]
Creating vector for yes... [2.1 MB]
Allocating iteration vectors... [2 x 2.1 MB]
TOTAL: [8.8 MB]

Starting iterations...

Iterative method: 96 iterations in 0.31 seconds (average 0.001625, setup 0.15)

Value in the initial state: 76.74353201242407

Time for model checking: 1.457 seconds.

Result: 76.74353201242407 (value in the initial state)

---------------------------------------------------------------------

Note: There were 16 warnings during computation.

