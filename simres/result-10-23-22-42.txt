PRISM
=====

Version: 4.3
Date: Wed Aug 03 16:12:14 BST 2016
Hostname: eglinton
Memory limits: cudd=1g, java(heap)=981.5m
Command line: prism uav_sim_mdp.prism -s uav_mdp.props -const 'Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=2,Objy1=2,Objx2=4,Objy2=2,'

Parsing model file "uav_sim_mdp.prism"...

Parsing properties file "uav_mdp.props"...

18 properties:
(1) Pmax=? [ F "MissionSuccessful" ]
(2) Pmin=? [ F "MissionSuccessful" ]
(3) Pmax=? [ F "MissionTimeUp" ]
(4) Pmin=? [ F "MissionTimeUp" ]
(5) Pmax=? [ F "Crash" ]
(6) Pmin=? [ F "Crash" ]
(7) Pmax=? [ F "AllAreaSearched" ]
(8) Pmin=? [ F "AllAreaSearched" ]
(9) Pmax=? [ F "MissionTimeUp"|"AllAreaSearched"|"Crash" ]
(10) Pmin=? [ F "MissionTimeUp"|"AllAreaSearched"|"Crash" ]
(11) R{"drop"}max=? [ F "deadlock" ]
(12) R{"drop"}min=? [ F "deadlock" ]
(13) R{"charge"}max=? [ F "deadlock" ]
(14) R{"charge"}min=? [ F "deadlock" ]
(15) R{"time"}max=? [ F "deadlock" ]
(16) R{"time"}min=? [ F "deadlock" ]
(17) R{"success"}max=? [ F "deadlock" ]/Pmin=? [ F "MissionSuccessful" ]
(18) R{"success"}min=? [ F "deadlock" ]/Pmax=? [ F "MissionSuccessful" ]

Type:        MDP
Modules:     Obj Move Robot T 
Variables:   NoOfObjs posx posy s1 retx rety b t c 

---------------------------------------------------------------------

Model checking: Pmax=? [ F "MissionSuccessful" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=2,Objy1=2,Objx2=4,Objy2=2

Building model...
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=2,Objy1=2,Objx2=4,Objy2=2

Warning: Guard for command 7 of module "Move" is never satisfied.

Warning: Guard for command 8 of module "Move" is never satisfied.

Warning: Guard for command 9 of module "Move" is never satisfied.

Warning: Guard for command 7 of module "Robot" is never satisfied.

Warning: Update 2 of command 3 of module "Robot" doesn't do anything ("(s1'=13)", line 210, column 44)

Warning: Update 2 of command 27 of module "T" doesn't do anything ("(c'=1)", line 323, column 108)

Warning: Guard for command 28 of module "T" is never satisfied.

Warning: Update 2 of command 9 of module "T" doesn't do anything ("(c'=1)", line 300, column 112)

Warning: Guard for command 10 of module "T" is never satisfied.

Warning: Update 2 of command 51 of module "T" doesn't do anything ("(c'=1)", line 354, column 113)

Warning: Guard for command 52 of module "T" is never satisfied.

Computing reachable states...

Reachability (BFS): 88 iterations in 0.84 seconds (average 0.009500, setup 0.00)

Time for model construction: 2.811 seconds.

Warning: Deadlocks detected and fixed in 1 states

Type:        MDP
States:      185237 (1 initial)
Transitions: 499498
Choices:     249568

Transition matrix: 172591 nodes (87 terminal), 499498 minterms, vars: 34r/34c/18nd

Prob0A: 46 iterations in 0.28 seconds (average 0.006087, setup 0.00)

Prob1E: 4 iterations in 0.04 seconds (average 0.011000, setup 0.00)

yes = 1047, no = 152113, maybe = 32077

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=185237, nc=58219, nnz=163747, k=2] [2.1 MB]
Creating vector for yes... [1.4 MB]
Allocating iteration vectors... [2 x 1.4 MB]
TOTAL: [6.3 MB]

Starting iterations...

Iterative method: 89 iterations in 0.33 seconds (average 0.001258, setup 0.22)

Value in the initial state: 0.9839043348317181

Time for model checking: 0.68 seconds.

Result: 0.9839043348317181 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmin=? [ F "MissionSuccessful" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=2,Objy1=2,Objx2=4,Objy2=2

Prob0E: 46 iterations in 0.29 seconds (average 0.006348, setup 0.00)

Prob1A: 3 iterations in 0.04 seconds (average 0.012000, setup 0.00)

yes = 1047, no = 155972, maybe = 28218

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=185237, nc=51092, nnz=143404, k=2] [1.9 MB]
Creating vector for yes... [1.4 MB]
Allocating iteration vectors... [2 x 1.4 MB]
TOTAL: [6.1 MB]

Starting iterations...

Iterative method: 86 iterations in 0.27 seconds (average 0.001256, setup 0.16)

Value in the initial state: 0.8601760946733482

Time for model checking: 0.612 seconds.

Result: 0.8601760946733482 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmax=? [ F "MissionTimeUp" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=2,Objy1=2,Objx2=4,Objy2=2

Prob0A: 77 iterations in 0.49 seconds (average 0.006338, setup 0.00)

Prob1E: 4 iterations in 0.03 seconds (average 0.008000, setup 0.00)

yes = 276, no = 117228, maybe = 67733

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=185237, nc=120845, nnz=332797, k=4] [4.1 MB]
Creating vector for yes... [1.4 MB]
Allocating iteration vectors... [2 x 1.4 MB]
TOTAL: [8.3 MB]

Starting iterations...

Iterative method: 94 iterations in 0.38 seconds (average 0.001574, setup 0.23)

Value in the initial state: 5.743790685136143E-5

Time for model checking: 0.91 seconds.

Result: 5.743790685136143E-5 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmin=? [ F "MissionTimeUp" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=2,Objy1=2,Objx2=4,Objy2=2

Prob0E: 91 iterations in 0.57 seconds (average 0.006286, setup 0.00)

Prob1A: 3 iterations in 0.03 seconds (average 0.010667, setup 0.00)

yes = 276, no = 117589, maybe = 67372

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=185237, nc=120206, nnz=331424, k=4] [4.1 MB]
Creating vector for yes... [1.4 MB]
Allocating iteration vectors... [2 x 1.4 MB]
TOTAL: [8.3 MB]

Starting iterations...

Iterative method: 112 iterations in 0.40 seconds (average 0.001571, setup 0.23)

Value in the initial state: 2.7790444094646712E-8

Time for model checking: 1.031 seconds.

Result: 2.7790444094646712E-8 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmax=? [ F "Crash" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=2,Objy1=2,Objx2=4,Objy2=2

Prob0A: 3 iterations in 0.07 seconds (average 0.024000, setup 0.00)

Prob1E: 176 iterations in 1.95 seconds (average 0.011068, setup 0.00)

yes = 90718, no = 2746, maybe = 91773

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=185237, nc=156104, nnz=406034, k=4] [5.0 MB]
Creating vector for yes... [1.4 MB]
Allocating iteration vectors... [2 x 1.4 MB]
TOTAL: [9.2 MB]

Starting iterations...

Iterative method: 89 iterations in 0.43 seconds (average 0.001753, setup 0.27)

Value in the initial state: 0.023554201898868588

Time for model checking: 2.475 seconds.

Result: 0.023554201898868588 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmin=? [ F "Crash" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=2,Objy1=2,Objx2=4,Objy2=2

Prob0E: 3 iterations in 0.08 seconds (average 0.025333, setup 0.00)

Prob1A: 46 iterations in 0.40 seconds (average 0.008609, setup 0.00)

yes = 90718, no = 2746, maybe = 91773

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=185237, nc=156104, nnz=406034, k=4] [5.0 MB]
Creating vector for yes... [1.4 MB]
Allocating iteration vectors... [2 x 1.4 MB]
TOTAL: [9.2 MB]

Starting iterations...

Iterative method: 81 iterations in 0.36 seconds (average 0.001827, setup 0.21)

Value in the initial state: 0.014042157757226231

Time for model checking: 0.838 seconds.

Result: 0.014042157757226231 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmax=? [ F "AllAreaSearched" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=2,Objy1=2,Objx2=4,Objy2=2

Prob0A: 54 iterations in 0.25 seconds (average 0.004667, setup 0.00)

Prob1E: 4 iterations in 0.03 seconds (average 0.007000, setup 0.00)

yes = 1422, no = 133507, maybe = 50308

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=185237, nc=81837, nnz=218418, k=2] [2.8 MB]
Creating vector for yes... [1.4 MB]
Allocating iteration vectors... [2 x 1.4 MB]
TOTAL: [7.0 MB]

Starting iterations...

Iterative method: 85 iterations in 0.28 seconds (average 0.001412, setup 0.16)

Value in the initial state: 0.11787866490556215

Time for model checking: 0.585 seconds.

Result: 0.11787866490556215 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmin=? [ F "AllAreaSearched" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=2,Objy1=2,Objx2=4,Objy2=2

Prob0E: 61 iterations in 0.29 seconds (average 0.004787, setup 0.00)

Prob1A: 3 iterations in 0.02 seconds (average 0.008000, setup 0.00)

yes = 1422, no = 139948, maybe = 43867

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=185237, nc=69937, nnz=184119, k=2] [2.4 MB]
Creating vector for yes... [1.4 MB]
Allocating iteration vectors... [2 x 1.4 MB]
TOTAL: [6.6 MB]

Starting iterations...

Iterative method: 94 iterations in 0.24 seconds (average 0.001362, setup 0.11)

Value in the initial state: 0.0018629002565870832

Time for model checking: 0.559 seconds.

Result: 0.0018629002565870832 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmax=? [ F "MissionTimeUp"|"AllAreaSearched"|"Crash" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=2,Objy1=2,Objx2=4,Objy2=2

Prob0A: 3 iterations in 0.06 seconds (average 0.020000, setup 0.00)

Prob1E: 180 iterations in 1.88 seconds (average 0.010467, setup 0.00)

yes = 155971, no = 1048, maybe = 28218

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=185237, nc=51092, nnz=143404, k=2] [1.9 MB]
Creating vector for yes... [1.4 MB]
Allocating iteration vectors... [2 x 1.4 MB]
TOTAL: [6.1 MB]

Starting iterations...

Iterative method: 79 iterations in 0.27 seconds (average 0.001215, setup 0.17)

Value in the initial state: 0.13982354350980963

Time for model checking: 2.237 seconds.

Result: 0.13982354350980963 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmin=? [ F "MissionTimeUp"|"AllAreaSearched"|"Crash" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=2,Objy1=2,Objx2=4,Objy2=2

Prob0E: 3 iterations in 0.05 seconds (average 0.016000, setup 0.00)

Prob1A: 46 iterations in 0.35 seconds (average 0.007652, setup 0.00)

yes = 152112, no = 1048, maybe = 32077

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=185237, nc=58219, nnz=163747, k=2] [2.1 MB]
Creating vector for yes... [1.4 MB]
Allocating iteration vectors... [2 x 1.4 MB]
TOTAL: [6.3 MB]

Starting iterations...

Iterative method: 87 iterations in 0.24 seconds (average 0.001241, setup 0.13)

Value in the initial state: 0.016093948363520558

Time for model checking: 0.648 seconds.

Result: 0.016093948363520558 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"drop"}max=? [ F "deadlock" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=2,Objy1=2,Objx2=4,Objy2=2

Prob0E: 4 iterations in 0.05 seconds (average 0.012000, setup 0.00)

Prob1A: 1 iterations in 0.00 seconds (average 0.000000, setup 0.00)

goal = 1, inf = 0, maybe = 185236

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=185237, nc=249567, nnz=499497, k=4] [7.4 MB]
Building sparse matrix (transition rewards)... [n=185237, nc=249567, nnz=0, k=4] [1.7 MB]
Creating vector for state rewards... [1.4 MB]
Creating vector for inf... [1.4 MB]
Allocating iteration vectors... [2 x 1.4 MB]
TOTAL: [14.7 MB]

Starting iterations...

Iterative method: 86 iterations in 0.57 seconds (average 0.002651, setup 0.34)

Value in the initial state: 0.14682081005585318

Time for model checking: 0.636 seconds.

Result: 0.14682081005585318 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"drop"}min=? [ F "deadlock" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=2,Objy1=2,Objx2=4,Objy2=2

Prob0A: 4 iterations in 0.03 seconds (average 0.007000, setup 0.00)

Prob1E: 5 iterations in 0.04 seconds (average 0.007200, setup 0.00)

Warning: PRISM hasn't checked for zero-reward loops.
Your minimum rewards may be too low...

goal = 1, inf = 0, maybe = 185236

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=185237, nc=249567, nnz=499497, k=4] [7.4 MB]
Building sparse matrix (transition rewards)... [n=185237, nc=249567, nnz=0, k=4] [1.7 MB]
Creating vector for state rewards... [1.4 MB]
Creating vector for inf... [1.4 MB]
Allocating iteration vectors... [2 x 1.4 MB]
TOTAL: [14.7 MB]

Starting iterations...

Iterative method: 79 iterations in 0.47 seconds (average 0.002684, setup 0.26)

Value in the initial state: 0.13306795686574277

Time for model checking: 0.551 seconds.

Result: 0.13306795686574277 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"charge"}max=? [ F "deadlock" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=2,Objy1=2,Objx2=4,Objy2=2

Prob0E: 4 iterations in 0.02 seconds (average 0.005000, setup 0.00)

Prob1A: 1 iterations in 0.00 seconds (average 0.004000, setup 0.00)

goal = 1, inf = 0, maybe = 185236

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=185237, nc=249567, nnz=499497, k=4] [7.4 MB]
Building sparse matrix (transition rewards)... [n=185237, nc=249567, nnz=5550, k=4] [1.7 MB]
Creating vector for state rewards... [1.4 MB]
Creating vector for inf... [1.4 MB]
Allocating iteration vectors... [2 x 1.4 MB]
TOTAL: [14.8 MB]

Starting iterations...

Iterative method: 85 iterations in 0.49 seconds (average 0.002682, setup 0.26)

Value in the initial state: 0.15117914791211612

Time for model checking: 0.525 seconds.

Result: 0.15117914791211612 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"charge"}min=? [ F "deadlock" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=2,Objy1=2,Objx2=4,Objy2=2

Prob0A: 4 iterations in 0.02 seconds (average 0.006000, setup 0.00)

Prob1E: 5 iterations in 0.04 seconds (average 0.007200, setup 0.00)

Warning: PRISM hasn't checked for zero-reward loops.
Your minimum rewards may be too low...

goal = 1, inf = 0, maybe = 185236

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=185237, nc=249567, nnz=499497, k=4] [7.4 MB]
Building sparse matrix (transition rewards)... [n=185237, nc=249567, nnz=5550, k=4] [1.7 MB]
Creating vector for state rewards... [1.4 MB]
Creating vector for inf... [1.4 MB]
Allocating iteration vectors... [2 x 1.4 MB]
TOTAL: [14.8 MB]

Starting iterations...

Iterative method: 95 iterations in 0.52 seconds (average 0.002695, setup 0.26)

Value in the initial state: 0.00993394635541225

Time for model checking: 0.584 seconds.

Result: 0.00993394635541225 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"time"}max=? [ F "deadlock" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=2,Objy1=2,Objx2=4,Objy2=2

Prob0E: 4 iterations in 0.02 seconds (average 0.005000, setup 0.00)

Prob1A: 1 iterations in 0.00 seconds (average 0.004000, setup 0.00)

goal = 1, inf = 0, maybe = 185236

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=185237, nc=249567, nnz=499497, k=4] [7.4 MB]
Building sparse matrix (transition rewards)... [n=185237, nc=249567, nnz=93462, k=4] [2.7 MB]
Creating vector for state rewards... [1.4 MB]
Creating vector for inf... [1.4 MB]
Allocating iteration vectors... [2 x 1.4 MB]
TOTAL: [15.8 MB]

Starting iterations...

Iterative method: 92 iterations in 0.54 seconds (average 0.002739, setup 0.29)

Value in the initial state: 130.2574845467725

Time for model checking: 0.578 seconds.

Result: 130.2574845467725 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"time"}min=? [ F "deadlock" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=2,Objy1=2,Objx2=4,Objy2=2

Prob0A: 4 iterations in 0.02 seconds (average 0.006000, setup 0.00)

Prob1E: 5 iterations in 0.04 seconds (average 0.008000, setup 0.00)

Warning: PRISM hasn't checked for zero-reward loops.
Your minimum rewards may be too low...

goal = 1, inf = 0, maybe = 185236

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=185237, nc=249567, nnz=499497, k=4] [7.4 MB]
Building sparse matrix (transition rewards)... [n=185237, nc=249567, nnz=93462, k=4] [2.7 MB]
Creating vector for state rewards... [1.4 MB]
Creating vector for inf... [1.4 MB]
Allocating iteration vectors... [2 x 1.4 MB]
TOTAL: [15.8 MB]

Starting iterations...

Iterative method: 92 iterations in 0.54 seconds (average 0.002739, setup 0.28)

Value in the initial state: 77.94758122746694

Time for model checking: 0.611 seconds.

Result: 77.94758122746694 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"success"}max=? [ F "deadlock" ]/Pmin=? [ F "MissionSuccessful" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=2,Objy1=2,Objx2=4,Objy2=2

Prob0E: 4 iterations in 0.02 seconds (average 0.005000, setup 0.00)

Prob1A: 1 iterations in 0.00 seconds (average 0.004000, setup 0.00)

goal = 1, inf = 0, maybe = 185236

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=185237, nc=249567, nnz=499497, k=4] [7.4 MB]
Building sparse matrix (transition rewards)... [n=185237, nc=249567, nnz=2064, k=4] [1.7 MB]
Creating vector for state rewards... [1.4 MB]
Creating vector for inf... [1.4 MB]
Allocating iteration vectors... [2 x 1.4 MB]
TOTAL: [14.7 MB]

Starting iterations...

Iterative method: 92 iterations in 0.50 seconds (average 0.002696, setup 0.26)

Prob0E: 46 iterations in 0.24 seconds (average 0.005130, setup 0.00)

Prob1A: 3 iterations in 0.03 seconds (average 0.009333, setup 0.00)

yes = 1047, no = 155972, maybe = 28218

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=185237, nc=51092, nnz=143404, k=2] [1.9 MB]
Creating vector for yes... [1.4 MB]
Allocating iteration vectors... [2 x 1.4 MB]
TOTAL: [6.1 MB]

Starting iterations...

Iterative method: 86 iterations in 0.20 seconds (average 0.001302, setup 0.09)

Value in the initial state: 148.8074749238595

Time for model checking: 1.02 seconds.

Result: 148.8074749238595 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"success"}min=? [ F "deadlock" ]/Pmax=? [ F "MissionSuccessful" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=2,Objy1=2,Objx2=4,Objy2=2

Prob0A: 4 iterations in 0.05 seconds (average 0.013000, setup 0.00)

Prob1E: 5 iterations in 0.04 seconds (average 0.008800, setup 0.00)

Warning: PRISM hasn't checked for zero-reward loops.
Your minimum rewards may be too low...

goal = 1, inf = 0, maybe = 185236

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=185237, nc=249567, nnz=499497, k=4] [7.4 MB]
Building sparse matrix (transition rewards)... [n=185237, nc=249567, nnz=2064, k=4] [1.7 MB]
Creating vector for state rewards... [1.4 MB]
Creating vector for inf... [1.4 MB]
Allocating iteration vectors... [2 x 1.4 MB]
TOTAL: [14.7 MB]

Starting iterations...

Iterative method: 91 iterations in 0.51 seconds (average 0.002725, setup 0.26)

Prob0A: 46 iterations in 0.26 seconds (average 0.005739, setup 0.00)

Prob1E: 4 iterations in 0.03 seconds (average 0.007000, setup 0.00)

yes = 1047, no = 152113, maybe = 32077

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=185237, nc=58219, nnz=163747, k=2] [2.1 MB]
Creating vector for yes... [1.4 MB]
Allocating iteration vectors... [2 x 1.4 MB]
TOTAL: [6.3 MB]

Starting iterations...

Iterative method: 89 iterations in 0.23 seconds (average 0.001258, setup 0.12)

Value in the initial state: 75.03325104425038

Time for model checking: 1.121 seconds.

Result: 75.03325104425038 (value in the initial state)

---------------------------------------------------------------------

Note: There were 16 warnings during computation.

