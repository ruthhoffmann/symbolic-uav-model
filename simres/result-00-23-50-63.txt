PRISM
=====

Version: 4.3
Date: Wed Aug 03 16:03:05 BST 2016
Hostname: eglinton
Memory limits: cudd=1g, java(heap)=981.5m
Command line: prism uav_sim_mdp.prism -s uav_mdp.props -const 'Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=5,Objy1=0,Objx2=6,Objy2=3,'

Parsing model file "uav_sim_mdp.prism"...

Parsing properties file "uav_mdp.props"...

18 properties:
(1) Pmax=? [ F "MissionSuccessful" ]
(2) Pmin=? [ F "MissionSuccessful" ]
(3) Pmax=? [ F "MissionTimeUp" ]
(4) Pmin=? [ F "MissionTimeUp" ]
(5) Pmax=? [ F "Crash" ]
(6) Pmin=? [ F "Crash" ]
(7) Pmax=? [ F "AllAreaSearched" ]
(8) Pmin=? [ F "AllAreaSearched" ]
(9) Pmax=? [ F "MissionTimeUp"|"AllAreaSearched"|"Crash" ]
(10) Pmin=? [ F "MissionTimeUp"|"AllAreaSearched"|"Crash" ]
(11) R{"drop"}max=? [ F "deadlock" ]
(12) R{"drop"}min=? [ F "deadlock" ]
(13) R{"charge"}max=? [ F "deadlock" ]
(14) R{"charge"}min=? [ F "deadlock" ]
(15) R{"time"}max=? [ F "deadlock" ]
(16) R{"time"}min=? [ F "deadlock" ]
(17) R{"success"}max=? [ F "deadlock" ]/Pmin=? [ F "MissionSuccessful" ]
(18) R{"success"}min=? [ F "deadlock" ]/Pmax=? [ F "MissionSuccessful" ]

Type:        MDP
Modules:     Obj Move Robot T 
Variables:   NoOfObjs posx posy s1 retx rety b t c 

---------------------------------------------------------------------

Model checking: Pmax=? [ F "MissionSuccessful" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=5,Objy1=0,Objx2=6,Objy2=3

Building model...
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=5,Objy1=0,Objx2=6,Objy2=3

Warning: Guard for command 7 of module "Move" is never satisfied.

Warning: Guard for command 8 of module "Move" is never satisfied.

Warning: Guard for command 9 of module "Move" is never satisfied.

Warning: Guard for command 7 of module "Robot" is never satisfied.

Warning: Update 2 of command 3 of module "Robot" doesn't do anything ("(s1'=13)", line 210, column 44)

Warning: Update 2 of command 27 of module "T" doesn't do anything ("(c'=1)", line 323, column 108)

Warning: Guard for command 28 of module "T" is never satisfied.

Warning: Update 2 of command 9 of module "T" doesn't do anything ("(c'=1)", line 300, column 112)

Warning: Guard for command 10 of module "T" is never satisfied.

Warning: Update 2 of command 51 of module "T" doesn't do anything ("(c'=1)", line 354, column 113)

Warning: Guard for command 52 of module "T" is never satisfied.

Computing reachable states...

Reachability (BFS): 89 iterations in 1.59 seconds (average 0.017888, setup 0.00)

Time for model construction: 3.507 seconds.

Warning: Deadlocks detected and fixed in 1 states

Type:        MDP
States:      337959 (1 initial)
Transitions: 926475
Choices:     452550

Transition matrix: 230130 nodes (97 terminal), 926475 minterms, vars: 34r/34c/18nd

Prob0A: 58 iterations in 0.48 seconds (average 0.008345, setup 0.00)

Prob1E: 4 iterations in 0.07 seconds (average 0.017000, setup 0.00)

yes = 1231, no = 287244, maybe = 49484

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=337959, nc=86255, nnz=248650, k=2] [3.3 MB]
Creating vector for yes... [2.6 MB]
Allocating iteration vectors... [2 x 2.6 MB]
TOTAL: [11.0 MB]

Starting iterations...

Iterative method: 103 iterations in 0.50 seconds (average 0.002136, setup 0.28)

Value in the initial state: 0.9789532783278581

Time for model checking: 1.092 seconds.

Result: 0.9789532783278581 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmin=? [ F "MissionSuccessful" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=5,Objy1=0,Objx2=6,Objy2=3

Prob0E: 60 iterations in 0.53 seconds (average 0.008800, setup 0.00)

Prob1A: 3 iterations in 0.05 seconds (average 0.016000, setup 0.00)

yes = 1231, no = 292961, maybe = 43767

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=337959, nc=76542, nnz=220983, k=2] [2.9 MB]
Creating vector for yes... [2.6 MB]
Allocating iteration vectors... [2 x 2.6 MB]
TOTAL: [10.7 MB]

Starting iterations...

Iterative method: 94 iterations in 0.40 seconds (average 0.002213, setup 0.20)

Value in the initial state: 0.7187690139181754

Time for model checking: 1.006 seconds.

Result: 0.7187690139181754 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmax=? [ F "MissionTimeUp" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=5,Objy1=0,Objx2=6,Objy2=3

Prob0A: 69 iterations in 0.72 seconds (average 0.010435, setup 0.00)

Prob1E: 4 iterations in 0.05 seconds (average 0.012000, setup 0.00)

yes = 524, no = 193587, maybe = 143848

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=337959, nc=244625, nnz=675629, k=4] [8.3 MB]
Creating vector for yes... [2.6 MB]
Allocating iteration vectors... [2 x 2.6 MB]
TOTAL: [16.0 MB]

Starting iterations...

Iterative method: 100 iterations in 0.68 seconds (average 0.002880, setup 0.39)

Value in the initial state: 8.515725651501452E-4

Time for model checking: 1.449 seconds.

Result: 8.515725651501452E-4 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmin=? [ F "MissionTimeUp" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=5,Objy1=0,Objx2=6,Objy2=3

Prob0E: 83 iterations in 1.09 seconds (average 0.013157, setup 0.00)

Prob1A: 3 iterations in 0.05 seconds (average 0.016000, setup 0.00)

yes = 524, no = 193981, maybe = 143454

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=337959, nc=243918, nnz=674273, k=4] [8.3 MB]
Creating vector for yes... [2.6 MB]
Allocating iteration vectors... [2 x 2.6 MB]
TOTAL: [16.0 MB]

Starting iterations...

Iterative method: 114 iterations in 0.69 seconds (average 0.002982, setup 0.35)

Value in the initial state: 4.832127424988574E-6

Time for model checking: 1.876 seconds.

Result: 4.832127424988574E-6 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmax=? [ F "Crash" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=5,Objy1=0,Objx2=6,Objy2=3

Prob0A: 3 iterations in 0.08 seconds (average 0.025333, setup 0.00)

Prob1E: 204 iterations in 3.06 seconds (average 0.014980, setup 0.00)

yes = 165204, no = 3492, maybe = 169263

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=337959, nc=283854, nnz=757779, k=4] [9.3 MB]
Creating vector for yes... [2.6 MB]
Allocating iteration vectors... [2 x 2.6 MB]
TOTAL: [17.0 MB]

Starting iterations...

Iterative method: 96 iterations in 0.72 seconds (average 0.003083, setup 0.42)

Value in the initial state: 0.03325298772347292

Time for model checking: 3.892 seconds.

Result: 0.03325298772347292 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmin=? [ F "Crash" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=5,Objy1=0,Objx2=6,Objy2=3

Prob0E: 4 iterations in 0.09 seconds (average 0.022000, setup 0.00)

Prob1A: 49 iterations in 0.74 seconds (average 0.015020, setup 0.00)

yes = 165204, no = 3492, maybe = 169263

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=337959, nc=283854, nnz=757779, k=4] [9.3 MB]
Creating vector for yes... [2.6 MB]
Allocating iteration vectors... [2 x 2.6 MB]
TOTAL: [17.0 MB]

Starting iterations...

Iterative method: 101 iterations in 0.72 seconds (average 0.003248, setup 0.40)

Value in the initial state: 0.01888183954900267

Time for model checking: 1.563 seconds.

Result: 0.01888183954900267 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmax=? [ F "AllAreaSearched" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=5,Objy1=0,Objx2=6,Objy2=3

Prob0A: 61 iterations in 0.39 seconds (average 0.006361, setup 0.00)

Prob1E: 4 iterations in 0.04 seconds (average 0.011000, setup 0.00)

yes = 1736, no = 261447, maybe = 74776

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=337959, nc=123325, nnz=343749, k=2] [4.4 MB]
Creating vector for yes... [2.6 MB]
Allocating iteration vectors... [2 x 2.6 MB]
TOTAL: [12.1 MB]

Starting iterations...

Iterative method: 93 iterations in 0.43 seconds (average 0.002366, setup 0.21)

Value in the initial state: 0.2510079207935705

Time for model checking: 0.895 seconds.

Result: 0.2510079207935705 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmin=? [ F "AllAreaSearched" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=5,Objy1=0,Objx2=6,Objy2=3

Prob0E: 65 iterations in 0.37 seconds (average 0.005723, setup 0.00)

Prob1A: 3 iterations in 0.04 seconds (average 0.013333, setup 0.00)

yes = 1736, no = 270512, maybe = 65711

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=337959, nc=108161, nnz=301048, k=2] [3.9 MB]
Creating vector for yes... [2.6 MB]
Allocating iteration vectors... [2 x 2.6 MB]
TOTAL: [11.6 MB]

Starting iterations...

Iterative method: 99 iterations in 0.41 seconds (average 0.002303, setup 0.18)

Value in the initial state: 0.0017441078766884733

Time for model checking: 0.839 seconds.

Result: 0.0017441078766884733 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmax=? [ F "MissionTimeUp"|"AllAreaSearched"|"Crash" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=5,Objy1=0,Objx2=6,Objy2=3

Prob0A: 3 iterations in 0.09 seconds (average 0.030667, setup 0.00)

Prob1E: 232 iterations in 3.42 seconds (average 0.014741, setup 0.00)

yes = 292960, no = 1232, maybe = 43767

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=337959, nc=76542, nnz=220983, k=2] [2.9 MB]
Creating vector for yes... [2.6 MB]
Allocating iteration vectors... [2 x 2.6 MB]
TOTAL: [10.7 MB]

Starting iterations...

Iterative method: 87 iterations in 0.42 seconds (average 0.002069, setup 0.24)

Value in the initial state: 0.2812305847624419

Time for model checking: 3.974 seconds.

Result: 0.2812305847624419 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmin=? [ F "MissionTimeUp"|"AllAreaSearched"|"Crash" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=5,Objy1=0,Objx2=6,Objy2=3

Prob0E: 4 iterations in 0.09 seconds (average 0.022000, setup 0.00)

Prob1A: 58 iterations in 0.68 seconds (average 0.011655, setup 0.00)

yes = 287243, no = 1232, maybe = 49484

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=337959, nc=86255, nnz=248650, k=2] [3.3 MB]
Creating vector for yes... [2.6 MB]
Allocating iteration vectors... [2 x 2.6 MB]
TOTAL: [11.0 MB]

Starting iterations...

Iterative method: 99 iterations in 0.42 seconds (average 0.002182, setup 0.20)

Value in the initial state: 0.021046092424908174

Time for model checking: 1.225 seconds.

Result: 0.021046092424908174 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"drop"}max=? [ F "deadlock" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=5,Objy1=0,Objx2=6,Objy2=3

Prob0E: 5 iterations in 0.11 seconds (average 0.022400, setup 0.00)

Prob1A: 1 iterations in 0.01 seconds (average 0.012000, setup 0.00)

goal = 1, inf = 0, maybe = 337958

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=337959, nc=452549, nnz=926474, k=4] [13.6 MB]
Building sparse matrix (transition rewards)... [n=337959, nc=452549, nnz=0, k=4] [3.0 MB]
Creating vector for state rewards... [2.6 MB]
Creating vector for inf... [2.6 MB]
Allocating iteration vectors... [2 x 2.6 MB]
TOTAL: [26.9 MB]

Starting iterations...

Iterative method: 98 iterations in 1.02 seconds (average 0.004857, setup 0.54)

Value in the initial state: 0.28309243132381023

Time for model checking: 1.158 seconds.

Result: 0.28309243132381023 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"drop"}min=? [ F "deadlock" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=5,Objy1=0,Objx2=6,Objy2=3

Prob0A: 4 iterations in 0.04 seconds (average 0.009000, setup 0.00)

Prob1E: 5 iterations in 0.05 seconds (average 0.009600, setup 0.00)

Warning: PRISM hasn't checked for zero-reward loops.
Your minimum rewards may be too low...

goal = 1, inf = 0, maybe = 337958

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=337959, nc=452549, nnz=926474, k=4] [13.6 MB]
Building sparse matrix (transition rewards)... [n=337959, nc=452549, nnz=0, k=4] [3.0 MB]
Creating vector for state rewards... [2.6 MB]
Creating vector for inf... [2.6 MB]
Allocating iteration vectors... [2 x 2.6 MB]
TOTAL: [26.9 MB]

Starting iterations...

Iterative method: 88 iterations in 0.86 seconds (average 0.004909, setup 0.43)

Value in the initial state: 0.23081828083212702

Time for model checking: 0.969 seconds.

Result: 0.23081828083212702 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"charge"}max=? [ F "deadlock" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=5,Objy1=0,Objx2=6,Objy2=3

Prob0E: 5 iterations in 0.04 seconds (average 0.008000, setup 0.00)

Prob1A: 1 iterations in 0.01 seconds (average 0.008000, setup 0.00)

goal = 1, inf = 0, maybe = 337958

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=337959, nc=452549, nnz=926474, k=4] [13.6 MB]
Building sparse matrix (transition rewards)... [n=337959, nc=452549, nnz=11338, k=4] [3.1 MB]
Creating vector for state rewards... [2.6 MB]
Creating vector for inf... [2.6 MB]
Allocating iteration vectors... [2 x 2.6 MB]
TOTAL: [27.1 MB]

Starting iterations...

Iterative method: 93 iterations in 0.90 seconds (average 0.004903, setup 0.44)

Value in the initial state: 0.32880610746776945

Time for model checking: 0.952 seconds.

Result: 0.32880610746776945 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"charge"}min=? [ F "deadlock" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=5,Objy1=0,Objx2=6,Objy2=3

Prob0A: 4 iterations in 0.03 seconds (average 0.007000, setup 0.00)

Prob1E: 5 iterations in 0.05 seconds (average 0.010400, setup 0.00)

Warning: PRISM hasn't checked for zero-reward loops.
Your minimum rewards may be too low...

goal = 1, inf = 0, maybe = 337958

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=337959, nc=452549, nnz=926474, k=4] [13.6 MB]
Building sparse matrix (transition rewards)... [n=337959, nc=452549, nnz=11338, k=4] [3.1 MB]
Creating vector for state rewards... [2.6 MB]
Creating vector for inf... [2.6 MB]
Allocating iteration vectors... [2 x 2.6 MB]
TOTAL: [27.1 MB]

Starting iterations...

Iterative method: 102 iterations in 0.94 seconds (average 0.004863, setup 0.44)

Value in the initial state: 0.0794327950812384

Time for model checking: 1.036 seconds.

Result: 0.0794327950812384 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"time"}max=? [ F "deadlock" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=5,Objy1=0,Objx2=6,Objy2=3

Prob0E: 5 iterations in 0.03 seconds (average 0.006400, setup 0.00)

Prob1A: 1 iterations in 0.00 seconds (average 0.004000, setup 0.00)

goal = 1, inf = 0, maybe = 337958

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=337959, nc=452549, nnz=926474, k=4] [13.6 MB]
Building sparse matrix (transition rewards)... [n=337959, nc=452549, nnz=168694, k=4] [4.9 MB]
Creating vector for state rewards... [2.6 MB]
Creating vector for inf... [2.6 MB]
Allocating iteration vectors... [2 x 2.6 MB]
TOTAL: [28.9 MB]

Starting iterations...

Iterative method: 99 iterations in 0.98 seconds (average 0.004889, setup 0.49)

Value in the initial state: 183.3624786630393

Time for model checking: 1.037 seconds.

Result: 183.3624786630393 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"time"}min=? [ F "deadlock" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=5,Objy1=0,Objx2=6,Objy2=3

Prob0A: 4 iterations in 0.04 seconds (average 0.010000, setup 0.00)

Prob1E: 5 iterations in 0.06 seconds (average 0.011200, setup 0.00)

Warning: PRISM hasn't checked for zero-reward loops.
Your minimum rewards may be too low...

goal = 1, inf = 0, maybe = 337958

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=337959, nc=452549, nnz=926474, k=4] [13.6 MB]
Building sparse matrix (transition rewards)... [n=337959, nc=452549, nnz=168694, k=4] [4.9 MB]
Creating vector for state rewards... [2.6 MB]
Creating vector for inf... [2.6 MB]
Allocating iteration vectors... [2 x 2.6 MB]
TOTAL: [28.9 MB]

Starting iterations...

Iterative method: 107 iterations in 1.00 seconds (average 0.004897, setup 0.47)

Value in the initial state: 104.57422558303853

Time for model checking: 1.107 seconds.

Result: 104.57422558303853 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"success"}max=? [ F "deadlock" ]/Pmin=? [ F "MissionSuccessful" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=5,Objy1=0,Objx2=6,Objy2=3

Prob0E: 5 iterations in 0.02 seconds (average 0.004800, setup 0.00)

Prob1A: 1 iterations in 0.00 seconds (average 0.004000, setup 0.00)

goal = 1, inf = 0, maybe = 337958

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=337959, nc=452549, nnz=926474, k=4] [13.6 MB]
Building sparse matrix (transition rewards)... [n=337959, nc=452549, nnz=2424, k=4] [3.0 MB]
Creating vector for state rewards... [2.6 MB]
Creating vector for inf... [2.6 MB]
Allocating iteration vectors... [2 x 2.6 MB]
TOTAL: [27.0 MB]

Starting iterations...

Iterative method: 99 iterations in 0.86 seconds (average 0.004485, setup 0.42)

Prob0E: 60 iterations in 0.32 seconds (average 0.005267, setup 0.00)

Prob1A: 3 iterations in 0.04 seconds (average 0.014667, setup 0.00)

yes = 1231, no = 292961, maybe = 43767

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=337959, nc=76542, nnz=220983, k=2] [2.9 MB]
Creating vector for yes... [2.6 MB]
Allocating iteration vectors... [2 x 2.6 MB]
TOTAL: [10.7 MB]

Starting iterations...

Iterative method: 94 iterations in 0.36 seconds (average 0.001915, setup 0.18)

Value in the initial state: 249.73522711228708

Time for model checking: 1.665 seconds.

Result: 249.73522711228708 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"success"}min=? [ F "deadlock" ]/Pmax=? [ F "MissionSuccessful" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=5,Objy1=0,Objx2=6,Objy2=3

Prob0A: 4 iterations in 0.10 seconds (average 0.024000, setup 0.00)

Prob1E: 5 iterations in 0.04 seconds (average 0.008800, setup 0.00)

Warning: PRISM hasn't checked for zero-reward loops.
Your minimum rewards may be too low...

goal = 1, inf = 0, maybe = 337958

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=337959, nc=452549, nnz=926474, k=4] [13.6 MB]
Building sparse matrix (transition rewards)... [n=337959, nc=452549, nnz=2424, k=4] [3.0 MB]
Creating vector for state rewards... [2.6 MB]
Creating vector for inf... [2.6 MB]
Allocating iteration vectors... [2 x 2.6 MB]
TOTAL: [27.0 MB]

Starting iterations...

Iterative method: 100 iterations in 0.90 seconds (average 0.004440, setup 0.46)

Prob0A: 58 iterations in 0.29 seconds (average 0.005034, setup 0.00)

Prob1E: 4 iterations in 0.04 seconds (average 0.010000, setup 0.00)

yes = 1231, no = 287244, maybe = 49484

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=337959, nc=86255, nnz=248650, k=2] [3.3 MB]
Creating vector for yes... [2.6 MB]
Allocating iteration vectors... [2 x 2.6 MB]
TOTAL: [11.0 MB]

Starting iterations...

Iterative method: 103 iterations in 0.39 seconds (average 0.001981, setup 0.18)

Value in the initial state: 79.15162553332755

Time for model checking: 1.794 seconds.

Result: 79.15162553332755 (value in the initial state)

---------------------------------------------------------------------

Note: There were 16 warnings during computation.

