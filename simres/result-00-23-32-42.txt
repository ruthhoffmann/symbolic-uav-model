PRISM
=====

Version: 4.3
Date: Wed Aug 03 15:59:56 BST 2016
Hostname: eglinton
Memory limits: cudd=1g, java(heap)=981.5m
Command line: prism uav_sim_mdp.prism -s uav_mdp.props -const 'Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=3,Objy1=2,Objx2=4,Objy2=2,'

Parsing model file "uav_sim_mdp.prism"...

Parsing properties file "uav_mdp.props"...

18 properties:
(1) Pmax=? [ F "MissionSuccessful" ]
(2) Pmin=? [ F "MissionSuccessful" ]
(3) Pmax=? [ F "MissionTimeUp" ]
(4) Pmin=? [ F "MissionTimeUp" ]
(5) Pmax=? [ F "Crash" ]
(6) Pmin=? [ F "Crash" ]
(7) Pmax=? [ F "AllAreaSearched" ]
(8) Pmin=? [ F "AllAreaSearched" ]
(9) Pmax=? [ F "MissionTimeUp"|"AllAreaSearched"|"Crash" ]
(10) Pmin=? [ F "MissionTimeUp"|"AllAreaSearched"|"Crash" ]
(11) R{"drop"}max=? [ F "deadlock" ]
(12) R{"drop"}min=? [ F "deadlock" ]
(13) R{"charge"}max=? [ F "deadlock" ]
(14) R{"charge"}min=? [ F "deadlock" ]
(15) R{"time"}max=? [ F "deadlock" ]
(16) R{"time"}min=? [ F "deadlock" ]
(17) R{"success"}max=? [ F "deadlock" ]/Pmin=? [ F "MissionSuccessful" ]
(18) R{"success"}min=? [ F "deadlock" ]/Pmax=? [ F "MissionSuccessful" ]

Type:        MDP
Modules:     Obj Move Robot T 
Variables:   NoOfObjs posx posy s1 retx rety b t c 

---------------------------------------------------------------------

Model checking: Pmax=? [ F "MissionSuccessful" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=3,Objy1=2,Objx2=4,Objy2=2

Building model...
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=3,Objy1=2,Objx2=4,Objy2=2

Warning: Guard for command 7 of module "Move" is never satisfied.

Warning: Guard for command 8 of module "Move" is never satisfied.

Warning: Guard for command 9 of module "Move" is never satisfied.

Warning: Guard for command 7 of module "Robot" is never satisfied.

Warning: Update 2 of command 3 of module "Robot" doesn't do anything ("(s1'=13)", line 210, column 44)

Warning: Update 2 of command 27 of module "T" doesn't do anything ("(c'=1)", line 323, column 108)

Warning: Guard for command 28 of module "T" is never satisfied.

Warning: Update 2 of command 9 of module "T" doesn't do anything ("(c'=1)", line 300, column 112)

Warning: Guard for command 10 of module "T" is never satisfied.

Warning: Update 2 of command 51 of module "T" doesn't do anything ("(c'=1)", line 354, column 113)

Warning: Guard for command 52 of module "T" is never satisfied.

Computing reachable states...

Reachability (BFS): 87 iterations in 0.73 seconds (average 0.008368, setup 0.00)

Time for model construction: 2.513 seconds.

Warning: Deadlocks detected and fixed in 1 states

Type:        MDP
States:      173122 (1 initial)
Transitions: 470530
Choices:     233621

Transition matrix: 200635 nodes (89 terminal), 470530 minterms, vars: 34r/34c/18nd

Prob0A: 46 iterations in 0.27 seconds (average 0.005826, setup 0.00)

Prob1E: 4 iterations in 0.04 seconds (average 0.011000, setup 0.00)

yes = 1024, no = 142348, maybe = 29750

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=173122, nc=54348, nnz=154151, k=2] [2.0 MB]
Creating vector for yes... [1.3 MB]
Allocating iteration vectors... [2 x 1.3 MB]
TOTAL: [5.9 MB]

Starting iterations...

Iterative method: 90 iterations in 0.30 seconds (average 0.001156, setup 0.20)

Value in the initial state: 0.9838999541980178

Time for model checking: 0.636 seconds.

Result: 0.9838999541980178 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmin=? [ F "MissionSuccessful" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=3,Objy1=2,Objx2=4,Objy2=2

Prob0E: 46 iterations in 0.27 seconds (average 0.005826, setup 0.00)

Prob1A: 3 iterations in 0.04 seconds (average 0.012000, setup 0.00)

yes = 1024, no = 145865, maybe = 26233

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=173122, nc=47794, nnz=135288, k=2] [1.8 MB]
Creating vector for yes... [1.3 MB]
Allocating iteration vectors... [2 x 1.3 MB]
TOTAL: [5.7 MB]

Starting iterations...

Iterative method: 86 iterations in 0.25 seconds (average 0.001116, setup 0.16)

Value in the initial state: 0.8601494756131065

Time for model checking: 0.569 seconds.

Result: 0.8601494756131065 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmax=? [ F "MissionTimeUp" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=3,Objy1=2,Objx2=4,Objy2=2

Prob0A: 78 iterations in 0.56 seconds (average 0.007128, setup 0.00)

Prob1E: 4 iterations in 0.05 seconds (average 0.012000, setup 0.00)

yes = 252, no = 111019, maybe = 61851

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=173122, nc=111273, nnz=310619, k=4] [3.8 MB]
Creating vector for yes... [1.3 MB]
Allocating iteration vectors... [2 x 1.3 MB]
TOTAL: [7.8 MB]

Starting iterations...

Iterative method: 95 iterations in 0.40 seconds (average 0.001389, setup 0.27)

Value in the initial state: 6.114205366374797E-5

Time for model checking: 1.035 seconds.

Result: 6.114205366374797E-5 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmin=? [ F "MissionTimeUp" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=3,Objy1=2,Objx2=4,Objy2=2

Prob0E: 92 iterations in 0.65 seconds (average 0.007087, setup 0.00)

Prob1A: 3 iterations in 0.04 seconds (average 0.014667, setup 0.00)

yes = 252, no = 111380, maybe = 61490

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=173122, nc=110634, nnz=309246, k=4] [3.8 MB]
Creating vector for yes... [1.3 MB]
Allocating iteration vectors... [2 x 1.3 MB]
TOTAL: [7.8 MB]

Starting iterations...

Iterative method: 111 iterations in 0.37 seconds (average 0.001477, setup 0.21)

Value in the initial state: 3.3235285293118525E-8

Time for model checking: 1.087 seconds.

Result: 3.3235285293118525E-8 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmax=? [ F "Crash" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=3,Objy1=2,Objx2=4,Objy2=2

Prob0A: 3 iterations in 0.07 seconds (average 0.022667, setup 0.00)

Prob1E: 172 iterations in 2.04 seconds (average 0.011860, setup 0.00)

yes = 84768, no = 2665, maybe = 85689

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=173122, nc=146188, nnz=383097, k=4] [4.7 MB]
Creating vector for yes... [1.3 MB]
Allocating iteration vectors... [2 x 1.3 MB]
TOTAL: [8.7 MB]

Starting iterations...

Iterative method: 88 iterations in 0.42 seconds (average 0.001591, setup 0.28)

Value in the initial state: 0.02360329915386268

Time for model checking: 2.539 seconds.

Result: 0.02360329915386268 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmin=? [ F "Crash" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=3,Objy1=2,Objx2=4,Objy2=2

Prob0E: 4 iterations in 0.06 seconds (average 0.014000, setup 0.00)

Prob1A: 46 iterations in 0.38 seconds (average 0.008348, setup 0.00)

yes = 84768, no = 2665, maybe = 85689

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=173122, nc=146188, nnz=383097, k=4] [4.7 MB]
Creating vector for yes... [1.3 MB]
Allocating iteration vectors... [2 x 1.3 MB]
TOTAL: [8.7 MB]

Starting iterations...

Iterative method: 81 iterations in 0.31 seconds (average 0.001630, setup 0.18)

Value in the initial state: 0.014045407473204503

Time for model checking: 0.775 seconds.

Result: 0.014045407473204503 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmax=? [ F "AllAreaSearched" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=3,Objy1=2,Objx2=4,Objy2=2

Prob0A: 54 iterations in 0.42 seconds (average 0.007852, setup 0.00)

Prob1E: 4 iterations in 0.07 seconds (average 0.017000, setup 0.00)

yes = 1388, no = 124275, maybe = 47459

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=173122, nc=77185, nnz=207181, k=2] [2.6 MB]
Creating vector for yes... [1.3 MB]
Allocating iteration vectors... [2 x 1.3 MB]
TOTAL: [6.6 MB]

Starting iterations...

Iterative method: 85 iterations in 0.31 seconds (average 0.001365, setup 0.20)

Value in the initial state: 0.11788248243882614

Time for model checking: 0.813 seconds.

Result: 0.11788248243882614 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmin=? [ F "AllAreaSearched" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=3,Objy1=2,Objx2=4,Objy2=2

Prob0E: 61 iterations in 0.30 seconds (average 0.004852, setup 0.00)

Prob1A: 3 iterations in 0.03 seconds (average 0.009333, setup 0.00)

yes = 1388, no = 130119, maybe = 41615

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=173122, nc=66291, nnz=175308, k=2] [2.2 MB]
Creating vector for yes... [1.3 MB]
Allocating iteration vectors... [2 x 1.3 MB]
TOTAL: [6.2 MB]

Starting iterations...

Iterative method: 93 iterations in 0.24 seconds (average 0.001290, setup 0.12)

Value in the initial state: 0.001862929285853868

Time for model checking: 0.549 seconds.

Result: 0.001862929285853868 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmax=? [ F "MissionTimeUp"|"AllAreaSearched"|"Crash" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=3,Objy1=2,Objx2=4,Objy2=2

Prob0A: 3 iterations in 0.06 seconds (average 0.021333, setup 0.00)

Prob1E: 176 iterations in 1.75 seconds (average 0.009955, setup 0.00)

yes = 145864, no = 1025, maybe = 26233

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=173122, nc=47794, nnz=135288, k=2] [1.8 MB]
Creating vector for yes... [1.3 MB]
Allocating iteration vectors... [2 x 1.3 MB]
TOTAL: [5.7 MB]

Starting iterations...

Iterative method: 79 iterations in 0.25 seconds (average 0.001063, setup 0.17)

Value in the initial state: 0.13985019129232854

Time for model checking: 2.088 seconds.

Result: 0.13985019129232854 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmin=? [ F "MissionTimeUp"|"AllAreaSearched"|"Crash" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=3,Objy1=2,Objx2=4,Objy2=2

Prob0E: 4 iterations in 0.06 seconds (average 0.014000, setup 0.00)

Prob1A: 46 iterations in 0.35 seconds (average 0.007652, setup 0.00)

yes = 142347, no = 1025, maybe = 29750

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=173122, nc=54348, nnz=154151, k=2] [2.0 MB]
Creating vector for yes... [1.3 MB]
Allocating iteration vectors... [2 x 1.3 MB]
TOTAL: [5.9 MB]

Starting iterations...

Iterative method: 88 iterations in 0.36 seconds (average 0.001409, setup 0.23)

Value in the initial state: 0.016098448408133473

Time for model checking: 0.765 seconds.

Result: 0.016098448408133473 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"drop"}max=? [ F "deadlock" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=3,Objy1=2,Objx2=4,Objy2=2

Prob0E: 5 iterations in 0.05 seconds (average 0.010400, setup 0.00)

Prob1A: 1 iterations in 0.00 seconds (average 0.004000, setup 0.00)

goal = 1, inf = 0, maybe = 173121

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=173122, nc=233620, nnz=470529, k=4] [6.9 MB]
Building sparse matrix (transition rewards)... [n=173122, nc=233620, nnz=0, k=4] [1.6 MB]
Creating vector for state rewards... [1.3 MB]
Creating vector for inf... [1.3 MB]
Allocating iteration vectors... [2 x 1.3 MB]
TOTAL: [13.8 MB]

Starting iterations...

Iterative method: 86 iterations in 0.64 seconds (average 0.002605, setup 0.41)

Value in the initial state: 0.14681805579142998

Time for model checking: 0.704 seconds.

Result: 0.14681805579142998 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"drop"}min=? [ F "deadlock" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=3,Objy1=2,Objx2=4,Objy2=2

Prob0A: 4 iterations in 0.03 seconds (average 0.008000, setup 0.00)

Prob1E: 5 iterations in 0.04 seconds (average 0.008000, setup 0.00)

Warning: PRISM hasn't checked for zero-reward loops.
Your minimum rewards may be too low...

goal = 1, inf = 0, maybe = 173121

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=173122, nc=233620, nnz=470529, k=4] [6.9 MB]
Building sparse matrix (transition rewards)... [n=173122, nc=233620, nnz=0, k=4] [1.6 MB]
Creating vector for state rewards... [1.3 MB]
Creating vector for inf... [1.3 MB]
Allocating iteration vectors... [2 x 1.3 MB]
TOTAL: [13.8 MB]

Starting iterations...

Iterative method: 79 iterations in 0.46 seconds (average 0.002582, setup 0.26)

Value in the initial state: 0.1330665909241777

Time for model checking: 0.543 seconds.

Result: 0.1330665909241777 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"charge"}max=? [ F "deadlock" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=3,Objy1=2,Objx2=4,Objy2=2

Prob0E: 5 iterations in 0.02 seconds (average 0.004800, setup 0.00)

Prob1A: 1 iterations in 0.00 seconds (average 0.004000, setup 0.00)

goal = 1, inf = 0, maybe = 173121

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=173122, nc=233620, nnz=470529, k=4] [6.9 MB]
Building sparse matrix (transition rewards)... [n=173122, nc=233620, nnz=5323, k=4] [1.6 MB]
Creating vector for state rewards... [1.3 MB]
Creating vector for inf... [1.3 MB]
Allocating iteration vectors... [2 x 1.3 MB]
TOTAL: [13.8 MB]

Starting iterations...

Iterative method: 85 iterations in 0.47 seconds (average 0.002541, setup 0.25)

Value in the initial state: 0.15118270384010374

Time for model checking: 0.508 seconds.

Result: 0.15118270384010374 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"charge"}min=? [ F "deadlock" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=3,Objy1=2,Objx2=4,Objy2=2

Prob0A: 4 iterations in 0.02 seconds (average 0.005000, setup 0.00)

Prob1E: 5 iterations in 0.04 seconds (average 0.007200, setup 0.00)

Warning: PRISM hasn't checked for zero-reward loops.
Your minimum rewards may be too low...

goal = 1, inf = 0, maybe = 173121

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=173122, nc=233620, nnz=470529, k=4] [6.9 MB]
Building sparse matrix (transition rewards)... [n=173122, nc=233620, nnz=5323, k=4] [1.6 MB]
Creating vector for state rewards... [1.3 MB]
Creating vector for inf... [1.3 MB]
Allocating iteration vectors... [2 x 1.3 MB]
TOTAL: [13.8 MB]

Starting iterations...

Iterative method: 95 iterations in 0.50 seconds (average 0.002568, setup 0.26)

Value in the initial state: 0.009922573998395853

Time for model checking: 0.563 seconds.

Result: 0.009922573998395853 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"time"}max=? [ F "deadlock" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=3,Objy1=2,Objx2=4,Objy2=2

Prob0E: 5 iterations in 0.02 seconds (average 0.004800, setup 0.00)

Prob1A: 1 iterations in 0.00 seconds (average 0.004000, setup 0.00)

goal = 1, inf = 0, maybe = 173121

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=173122, nc=233620, nnz=470529, k=4] [6.9 MB]
Building sparse matrix (transition rewards)... [n=173122, nc=233620, nnz=87431, k=4] [2.6 MB]
Creating vector for state rewards... [1.3 MB]
Creating vector for inf... [1.3 MB]
Allocating iteration vectors... [2 x 1.3 MB]
TOTAL: [14.8 MB]

Starting iterations...

Iterative method: 91 iterations in 0.51 seconds (average 0.002549, setup 0.28)

Value in the initial state: 130.52596733469292

Time for model checking: 0.557 seconds.

Result: 130.52596733469292 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"time"}min=? [ F "deadlock" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=3,Objy1=2,Objx2=4,Objy2=2

Prob0A: 4 iterations in 0.02 seconds (average 0.004000, setup 0.00)

Prob1E: 5 iterations in 0.04 seconds (average 0.008000, setup 0.00)

Warning: PRISM hasn't checked for zero-reward loops.
Your minimum rewards may be too low...

goal = 1, inf = 0, maybe = 173121

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=173122, nc=233620, nnz=470529, k=4] [6.9 MB]
Building sparse matrix (transition rewards)... [n=173122, nc=233620, nnz=87431, k=4] [2.6 MB]
Creating vector for state rewards... [1.3 MB]
Creating vector for inf... [1.3 MB]
Allocating iteration vectors... [2 x 1.3 MB]
TOTAL: [14.8 MB]

Starting iterations...

Iterative method: 92 iterations in 0.52 seconds (average 0.002609, setup 0.28)

Value in the initial state: 77.96513357328594

Time for model checking: 0.59 seconds.

Result: 77.96513357328594 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"success"}max=? [ F "deadlock" ]/Pmin=? [ F "MissionSuccessful" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=3,Objy1=2,Objx2=4,Objy2=2

Prob0E: 5 iterations in 0.02 seconds (average 0.004800, setup 0.00)

Prob1A: 1 iterations in 0.01 seconds (average 0.008000, setup 0.00)

goal = 1, inf = 0, maybe = 173121

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=173122, nc=233620, nnz=470529, k=4] [6.9 MB]
Building sparse matrix (transition rewards)... [n=173122, nc=233620, nnz=2021, k=4] [1.6 MB]
Creating vector for state rewards... [1.3 MB]
Creating vector for inf... [1.3 MB]
Allocating iteration vectors... [2 x 1.3 MB]
TOTAL: [13.8 MB]

Starting iterations...

Iterative method: 91 iterations in 0.48 seconds (average 0.002505, setup 0.25)

Prob0E: 46 iterations in 0.25 seconds (average 0.005478, setup 0.00)

Prob1A: 3 iterations in 0.03 seconds (average 0.010667, setup 0.00)

yes = 1024, no = 145865, maybe = 26233

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=173122, nc=47794, nnz=135288, k=2] [1.8 MB]
Creating vector for yes... [1.3 MB]
Allocating iteration vectors... [2 x 1.3 MB]
TOTAL: [5.7 MB]

Starting iterations...

Iterative method: 86 iterations in 0.21 seconds (average 0.001116, setup 0.11)

Value in the initial state: 149.09972307881583

Time for model checking: 1.025 seconds.

Result: 149.09972307881583 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"success"}min=? [ F "deadlock" ]/Pmax=? [ F "MissionSuccessful" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=3,Objy1=2,Objx2=4,Objy2=2

Prob0A: 4 iterations in 0.05 seconds (average 0.013000, setup 0.00)

Prob1E: 5 iterations in 0.04 seconds (average 0.008000, setup 0.00)

Warning: PRISM hasn't checked for zero-reward loops.
Your minimum rewards may be too low...

goal = 1, inf = 0, maybe = 173121

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=173122, nc=233620, nnz=470529, k=4] [6.9 MB]
Building sparse matrix (transition rewards)... [n=173122, nc=233620, nnz=2021, k=4] [1.6 MB]
Creating vector for state rewards... [1.3 MB]
Creating vector for inf... [1.3 MB]
Allocating iteration vectors... [2 x 1.3 MB]
TOTAL: [13.8 MB]

Starting iterations...

Iterative method: 91 iterations in 0.58 seconds (average 0.002637, setup 0.34)

Prob0A: 46 iterations in 0.20 seconds (average 0.004435, setup 0.00)

Prob1E: 4 iterations in 0.02 seconds (average 0.006000, setup 0.00)

yes = 1024, no = 142348, maybe = 29750

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=173122, nc=54348, nnz=154151, k=2] [2.0 MB]
Creating vector for yes... [1.3 MB]
Allocating iteration vectors... [2 x 1.3 MB]
TOTAL: [5.9 MB]

Starting iterations...

Iterative method: 90 iterations in 0.23 seconds (average 0.001200, setup 0.12)

Value in the initial state: 74.9401562753942

Time for model checking: 1.138 seconds.

Result: 74.9401562753942 (value in the initial state)

---------------------------------------------------------------------

Note: There were 16 warnings during computation.

