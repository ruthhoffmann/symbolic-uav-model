PRISM
=====

Version: 4.3
Date: Wed Aug 03 16:02:35 BST 2016
Hostname: eglinton
Memory limits: cudd=1g, java(heap)=981.5m
Command line: prism uav_sim_mdp.prism -s uav_mdp.props -const 'Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=4,Objy1=3,Objx2=6,Objy2=0,'

Parsing model file "uav_sim_mdp.prism"...

Parsing properties file "uav_mdp.props"...

18 properties:
(1) Pmax=? [ F "MissionSuccessful" ]
(2) Pmin=? [ F "MissionSuccessful" ]
(3) Pmax=? [ F "MissionTimeUp" ]
(4) Pmin=? [ F "MissionTimeUp" ]
(5) Pmax=? [ F "Crash" ]
(6) Pmin=? [ F "Crash" ]
(7) Pmax=? [ F "AllAreaSearched" ]
(8) Pmin=? [ F "AllAreaSearched" ]
(9) Pmax=? [ F "MissionTimeUp"|"AllAreaSearched"|"Crash" ]
(10) Pmin=? [ F "MissionTimeUp"|"AllAreaSearched"|"Crash" ]
(11) R{"drop"}max=? [ F "deadlock" ]
(12) R{"drop"}min=? [ F "deadlock" ]
(13) R{"charge"}max=? [ F "deadlock" ]
(14) R{"charge"}min=? [ F "deadlock" ]
(15) R{"time"}max=? [ F "deadlock" ]
(16) R{"time"}min=? [ F "deadlock" ]
(17) R{"success"}max=? [ F "deadlock" ]/Pmin=? [ F "MissionSuccessful" ]
(18) R{"success"}min=? [ F "deadlock" ]/Pmax=? [ F "MissionSuccessful" ]

Type:        MDP
Modules:     Obj Move Robot T 
Variables:   NoOfObjs posx posy s1 retx rety b t c 

---------------------------------------------------------------------

Model checking: Pmax=? [ F "MissionSuccessful" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=4,Objy1=3,Objx2=6,Objy2=0

Building model...
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=4,Objy1=3,Objx2=6,Objy2=0

Warning: Guard for command 7 of module "Move" is never satisfied.

Warning: Guard for command 8 of module "Move" is never satisfied.

Warning: Guard for command 9 of module "Move" is never satisfied.

Warning: Guard for command 7 of module "Robot" is never satisfied.

Warning: Update 2 of command 3 of module "Robot" doesn't do anything ("(s1'=13)", line 210, column 44)

Warning: Update 2 of command 27 of module "T" doesn't do anything ("(c'=1)", line 323, column 108)

Warning: Guard for command 28 of module "T" is never satisfied.

Warning: Update 2 of command 9 of module "T" doesn't do anything ("(c'=1)", line 300, column 112)

Warning: Guard for command 10 of module "T" is never satisfied.

Warning: Update 2 of command 51 of module "T" doesn't do anything ("(c'=1)", line 354, column 113)

Warning: Guard for command 52 of module "T" is never satisfied.

Computing reachable states...

Reachability (BFS): 89 iterations in 1.49 seconds (average 0.016764, setup 0.00)

Time for model construction: 3.058 seconds.

Warning: Deadlocks detected and fixed in 1 states

Type:        MDP
States:      284913 (1 initial)
Transitions: 759309
Choices:     377915

Transition matrix: 245089 nodes (95 terminal), 759309 minterms, vars: 34r/34c/18nd

Prob0A: 58 iterations in 0.68 seconds (average 0.011724, setup 0.00)

Prob1E: 4 iterations in 0.07 seconds (average 0.017000, setup 0.00)

yes = 1147, no = 243851, maybe = 39915

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=284913, nc=68001, nnz=190542, k=2] [2.5 MB]
Creating vector for yes... [2.2 MB]
Allocating iteration vectors... [2 x 2.2 MB]
TOTAL: [9.0 MB]

Starting iterations...

Iterative method: 102 iterations in 0.46 seconds (average 0.001922, setup 0.27)

Value in the initial state: 0.9795575187084324

Time for model checking: 1.286 seconds.

Result: 0.9795575187084324 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmin=? [ F "MissionSuccessful" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=4,Objy1=3,Objx2=6,Objy2=0

Prob0E: 60 iterations in 0.71 seconds (average 0.011800, setup 0.00)

Prob1A: 3 iterations in 0.05 seconds (average 0.017333, setup 0.00)

yes = 1147, no = 249469, maybe = 34297

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=284913, nc=58786, nnz=164350, k=2] [2.2 MB]
Creating vector for yes... [2.2 MB]
Allocating iteration vectors... [2 x 2.2 MB]
TOTAL: [8.7 MB]

Starting iterations...

Iterative method: 96 iterations in 0.36 seconds (average 0.001833, setup 0.18)

Value in the initial state: 0.7379711841610475

Time for model checking: 1.15 seconds.

Result: 0.7379711841610475 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmax=? [ F "MissionTimeUp" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=4,Objy1=3,Objx2=6,Objy2=0

Prob0A: 68 iterations in 0.83 seconds (average 0.012176, setup 0.00)

Prob1E: 4 iterations in 0.05 seconds (average 0.013000, setup 0.00)

yes = 456, no = 163922, maybe = 120535

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=284913, nc=200204, nnz=542706, k=4] [6.7 MB]
Creating vector for yes... [2.2 MB]
Allocating iteration vectors... [2 x 2.2 MB]
TOTAL: [13.2 MB]

Starting iterations...

Iterative method: 102 iterations in 0.60 seconds (average 0.002471, setup 0.35)

Value in the initial state: 7.201810603701493E-4

Time for model checking: 1.49 seconds.

Result: 7.201810603701493E-4 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmin=? [ F "MissionTimeUp" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=4,Objy1=3,Objx2=6,Objy2=0

Prob0E: 84 iterations in 1.15 seconds (average 0.013714, setup 0.00)

Prob1A: 3 iterations in 0.05 seconds (average 0.017333, setup 0.00)

yes = 456, no = 164245, maybe = 120212

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=284913, nc=199634, nnz=541531, k=4] [6.7 MB]
Creating vector for yes... [2.2 MB]
Allocating iteration vectors... [2 x 2.2 MB]
TOTAL: [13.2 MB]

Starting iterations...

Iterative method: 114 iterations in 0.64 seconds (average 0.002526, setup 0.35)

Value in the initial state: 2.9024699447724157E-6

Time for model checking: 1.889 seconds.

Result: 2.9024699447724157E-6 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmax=? [ F "Crash" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=4,Objy1=3,Objx2=6,Objy2=0

Prob0A: 3 iterations in 0.11 seconds (average 0.037333, setup 0.00)

Prob1E: 204 iterations in 3.32 seconds (average 0.016255, setup 0.00)

yes = 139413, no = 3326, maybe = 142174

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=284913, nc=235176, nnz=616570, k=4] [7.6 MB]
Creating vector for yes... [2.2 MB]
Allocating iteration vectors... [2 x 2.2 MB]
TOTAL: [14.1 MB]

Starting iterations...

Iterative method: 96 iterations in 0.64 seconds (average 0.002667, setup 0.38)

Value in the initial state: 0.03213090648356137

Time for model checking: 4.117 seconds.

Result: 0.03213090648356137 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmin=? [ F "Crash" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=4,Objy1=3,Objx2=6,Objy2=0

Prob0E: 4 iterations in 0.08 seconds (average 0.019000, setup 0.00)

Prob1A: 49 iterations in 0.55 seconds (average 0.011184, setup 0.00)

yes = 139413, no = 3326, maybe = 142174

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=284913, nc=235176, nnz=616570, k=4] [7.6 MB]
Creating vector for yes... [2.2 MB]
Allocating iteration vectors... [2 x 2.2 MB]
TOTAL: [14.1 MB]

Starting iterations...

Iterative method: 101 iterations in 0.54 seconds (average 0.002733, setup 0.26)

Value in the initial state: 0.01840700421640604

Time for model checking: 1.187 seconds.

Result: 0.01840700421640604 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmax=? [ F "AllAreaSearched" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=4,Objy1=3,Objx2=6,Objy2=0

Prob0A: 59 iterations in 0.45 seconds (average 0.007593, setup 0.00)

Prob1E: 4 iterations in 0.05 seconds (average 0.012000, setup 0.00)

yes = 1722, no = 223510, maybe = 59681

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=284913, nc=96487, nnz=258275, k=2] [3.3 MB]
Creating vector for yes... [2.2 MB]
Allocating iteration vectors... [2 x 2.2 MB]
TOTAL: [9.8 MB]

Starting iterations...

Iterative method: 91 iterations in 0.40 seconds (average 0.001978, setup 0.22)

Value in the initial state: 0.23272827086084502

Time for model checking: 0.914 seconds.

Result: 0.23272827086084502 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmin=? [ F "AllAreaSearched" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=4,Objy1=3,Objx2=6,Objy2=0

Prob0E: 64 iterations in 0.45 seconds (average 0.007063, setup 0.00)

Prob1A: 3 iterations in 0.04 seconds (average 0.012000, setup 0.00)

yes = 1722, no = 231457, maybe = 51734

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=284913, nc=84059, nnz=224677, k=2] [2.9 MB]
Creating vector for yes... [2.2 MB]
Allocating iteration vectors... [2 x 2.2 MB]
TOTAL: [9.4 MB]

Starting iterations...

Iterative method: 97 iterations in 0.32 seconds (average 0.001938, setup 0.14)

Value in the initial state: 0.001781193048803562

Time for model checking: 0.826 seconds.

Result: 0.001781193048803562 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmax=? [ F "MissionTimeUp"|"AllAreaSearched"|"Crash" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=4,Objy1=3,Objx2=6,Objy2=0

Prob0A: 3 iterations in 0.08 seconds (average 0.028000, setup 0.00)

Prob1E: 232 iterations in 3.94 seconds (average 0.016966, setup 0.00)

yes = 249468, no = 1148, maybe = 34297

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=284913, nc=58786, nnz=164350, k=2] [2.2 MB]
Creating vector for yes... [2.2 MB]
Allocating iteration vectors... [2 x 2.2 MB]
TOTAL: [8.7 MB]

Starting iterations...

Iterative method: 89 iterations in 0.36 seconds (average 0.001753, setup 0.20)

Value in the initial state: 0.2620285672519846

Time for model checking: 4.457 seconds.

Result: 0.2620285672519846 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmin=? [ F "MissionTimeUp"|"AllAreaSearched"|"Crash" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=4,Objy1=3,Objx2=6,Objy2=0

Prob0E: 4 iterations in 0.06 seconds (average 0.014000, setup 0.00)

Prob1A: 58 iterations in 0.54 seconds (average 0.009310, setup 0.00)

yes = 243850, no = 1148, maybe = 39915

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=284913, nc=68001, nnz=190542, k=2] [2.5 MB]
Creating vector for yes... [2.2 MB]
Allocating iteration vectors... [2 x 2.2 MB]
TOTAL: [9.0 MB]

Starting iterations...

Iterative method: 100 iterations in 0.33 seconds (average 0.001840, setup 0.15)

Value in the initial state: 0.020442118406920905

Time for model checking: 0.95 seconds.

Result: 0.020442118406920905 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"drop"}max=? [ F "deadlock" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=4,Objy1=3,Objx2=6,Objy2=0

Prob0E: 5 iterations in 0.05 seconds (average 0.009600, setup 0.00)

Prob1A: 1 iterations in 0.00 seconds (average 0.004000, setup 0.00)

goal = 1, inf = 0, maybe = 284912

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=284913, nc=377914, nnz=759308, k=4] [11.2 MB]
Building sparse matrix (transition rewards)... [n=284913, nc=377914, nnz=0, k=4] [2.5 MB]
Creating vector for state rewards... [2.2 MB]
Creating vector for inf... [2.2 MB]
Allocating iteration vectors... [2 x 2.2 MB]
TOTAL: [22.4 MB]

Starting iterations...

Iterative method: 98 iterations in 0.84 seconds (average 0.003959, setup 0.45)

Value in the initial state: 0.24030383803142677

Time for model checking: 0.911 seconds.

Result: 0.24030383803142677 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"drop"}min=? [ F "deadlock" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=4,Objy1=3,Objx2=6,Objy2=0

Prob0A: 4 iterations in 0.04 seconds (average 0.009000, setup 0.00)

Prob1E: 5 iterations in 0.05 seconds (average 0.009600, setup 0.00)

Warning: PRISM hasn't checked for zero-reward loops.
Your minimum rewards may be too low...

goal = 1, inf = 0, maybe = 284912

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=284913, nc=377914, nnz=759308, k=4] [11.2 MB]
Building sparse matrix (transition rewards)... [n=284913, nc=377914, nnz=0, k=4] [2.5 MB]
Creating vector for state rewards... [2.2 MB]
Creating vector for inf... [2.2 MB]
Allocating iteration vectors... [2 x 2.2 MB]
TOTAL: [22.4 MB]

Starting iterations...

Iterative method: 88 iterations in 0.73 seconds (average 0.004000, setup 0.38)

Value in the initial state: 0.21246293257245777

Time for model checking: 0.828 seconds.

Result: 0.21246293257245777 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"charge"}max=? [ F "deadlock" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=4,Objy1=3,Objx2=6,Objy2=0

Prob0E: 5 iterations in 0.03 seconds (average 0.005600, setup 0.00)

Prob1A: 1 iterations in 0.01 seconds (average 0.008000, setup 0.00)

goal = 1, inf = 0, maybe = 284912

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=284913, nc=377914, nnz=759308, k=4] [11.2 MB]
Building sparse matrix (transition rewards)... [n=284913, nc=377914, nnz=8928, k=4] [2.6 MB]
Creating vector for state rewards... [2.2 MB]
Creating vector for inf... [2.2 MB]
Allocating iteration vectors... [2 x 2.2 MB]
TOTAL: [22.5 MB]

Starting iterations...

Iterative method: 96 iterations in 0.76 seconds (average 0.003917, setup 0.39)

Value in the initial state: 0.3093836260884585

Time for model checking: 0.817 seconds.

Result: 0.3093836260884585 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"charge"}min=? [ F "deadlock" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=4,Objy1=3,Objx2=6,Objy2=0

Prob0A: 4 iterations in 0.03 seconds (average 0.007000, setup 0.00)

Prob1E: 5 iterations in 0.04 seconds (average 0.008000, setup 0.00)

Warning: PRISM hasn't checked for zero-reward loops.
Your minimum rewards may be too low...

goal = 1, inf = 0, maybe = 284912

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=284913, nc=377914, nnz=759308, k=4] [11.2 MB]
Building sparse matrix (transition rewards)... [n=284913, nc=377914, nnz=8928, k=4] [2.6 MB]
Creating vector for state rewards... [2.2 MB]
Creating vector for inf... [2.2 MB]
Allocating iteration vectors... [2 x 2.2 MB]
TOTAL: [22.5 MB]

Starting iterations...

Iterative method: 102 iterations in 0.74 seconds (average 0.003725, setup 0.36)

Value in the initial state: 0.05747063910668343

Time for model checking: 0.821 seconds.

Result: 0.05747063910668343 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"time"}max=? [ F "deadlock" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=4,Objy1=3,Objx2=6,Objy2=0

Prob0E: 5 iterations in 0.03 seconds (average 0.006400, setup 0.00)

Prob1A: 1 iterations in 0.01 seconds (average 0.008000, setup 0.00)

goal = 1, inf = 0, maybe = 284912

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=284913, nc=377914, nnz=759308, k=4] [11.2 MB]
Building sparse matrix (transition rewards)... [n=284913, nc=377914, nnz=142737, k=4] [4.2 MB]
Creating vector for state rewards... [2.2 MB]
Creating vector for inf... [2.2 MB]
Allocating iteration vectors... [2 x 2.2 MB]
TOTAL: [24.1 MB]

Starting iterations...

Iterative method: 99 iterations in 0.76 seconds (average 0.003798, setup 0.38)

Value in the initial state: 177.25079941483128

Time for model checking: 0.824 seconds.

Result: 177.25079941483128 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"time"}min=? [ F "deadlock" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=4,Objy1=3,Objx2=6,Objy2=0

Prob0A: 4 iterations in 0.03 seconds (average 0.007000, setup 0.00)

Prob1E: 5 iterations in 0.04 seconds (average 0.008800, setup 0.00)

Warning: PRISM hasn't checked for zero-reward loops.
Your minimum rewards may be too low...

goal = 1, inf = 0, maybe = 284912

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=284913, nc=377914, nnz=759308, k=4] [11.2 MB]
Building sparse matrix (transition rewards)... [n=284913, nc=377914, nnz=142737, k=4] [4.2 MB]
Creating vector for state rewards... [2.2 MB]
Creating vector for inf... [2.2 MB]
Allocating iteration vectors... [2 x 2.2 MB]
TOTAL: [24.1 MB]

Starting iterations...

Iterative method: 107 iterations in 0.81 seconds (average 0.003850, setup 0.40)

Value in the initial state: 101.97763785802731

Time for model checking: 0.89 seconds.

Result: 101.97763785802731 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"success"}max=? [ F "deadlock" ]/Pmin=? [ F "MissionSuccessful" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=4,Objy1=3,Objx2=6,Objy2=0

Prob0E: 5 iterations in 0.03 seconds (average 0.005600, setup 0.00)

Prob1A: 1 iterations in 0.00 seconds (average 0.004000, setup 0.00)

goal = 1, inf = 0, maybe = 284912

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=284913, nc=377914, nnz=759308, k=4] [11.2 MB]
Building sparse matrix (transition rewards)... [n=284913, nc=377914, nnz=2258, k=4] [2.6 MB]
Creating vector for state rewards... [2.2 MB]
Creating vector for inf... [2.2 MB]
Allocating iteration vectors... [2 x 2.2 MB]
TOTAL: [22.5 MB]

Starting iterations...

Iterative method: 101 iterations in 0.72 seconds (average 0.003683, setup 0.34)

Prob0E: 60 iterations in 0.29 seconds (average 0.004867, setup 0.00)

Prob1A: 3 iterations in 0.04 seconds (average 0.012000, setup 0.00)

yes = 1147, no = 249469, maybe = 34297

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=284913, nc=58786, nnz=164350, k=2] [2.2 MB]
Creating vector for yes... [2.2 MB]
Allocating iteration vectors... [2 x 2.2 MB]
TOTAL: [8.7 MB]

Starting iterations...

Iterative method: 96 iterations in 0.24 seconds (average 0.001625, setup 0.09)

Value in the initial state: 235.40727663972487

Time for model checking: 1.361 seconds.

Result: 235.40727663972487 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"success"}min=? [ F "deadlock" ]/Pmax=? [ F "MissionSuccessful" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=4,Objy1=3,Objx2=6,Objy2=0

Prob0A: 4 iterations in 0.04 seconds (average 0.011000, setup 0.00)

Prob1E: 5 iterations in 0.04 seconds (average 0.007200, setup 0.00)

Warning: PRISM hasn't checked for zero-reward loops.
Your minimum rewards may be too low...

goal = 1, inf = 0, maybe = 284912

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=284913, nc=377914, nnz=759308, k=4] [11.2 MB]
Building sparse matrix (transition rewards)... [n=284913, nc=377914, nnz=2258, k=4] [2.6 MB]
Creating vector for state rewards... [2.2 MB]
Creating vector for inf... [2.2 MB]
Allocating iteration vectors... [2 x 2.2 MB]
TOTAL: [22.5 MB]

Starting iterations...

Iterative method: 99 iterations in 0.71 seconds (average 0.003677, setup 0.34)

Prob0A: 58 iterations in 0.28 seconds (average 0.004759, setup 0.00)

Prob1E: 4 iterations in 0.02 seconds (average 0.006000, setup 0.00)

yes = 1147, no = 243851, maybe = 39915

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=284913, nc=68001, nnz=190542, k=2] [2.5 MB]
Creating vector for yes... [2.2 MB]
Allocating iteration vectors... [2 x 2.2 MB]
TOTAL: [9.0 MB]

Starting iterations...

Iterative method: 102 iterations in 0.28 seconds (average 0.001647, setup 0.11)

Value in the initial state: 81.18896079473602

Time for model checking: 1.387 seconds.

Result: 81.18896079473602 (value in the initial state)

---------------------------------------------------------------------

Note: There were 16 warnings during computation.

