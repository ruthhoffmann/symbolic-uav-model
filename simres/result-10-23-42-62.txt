PRISM
=====

Version: 4.3
Date: Wed Aug 03 16:16:23 BST 2016
Hostname: eglinton
Memory limits: cudd=1g, java(heap)=981.5m
Command line: prism uav_sim_mdp.prism -s uav_mdp.props -const 'Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=4,Objy1=2,Objx2=6,Objy2=2,'

Parsing model file "uav_sim_mdp.prism"...

Parsing properties file "uav_mdp.props"...

18 properties:
(1) Pmax=? [ F "MissionSuccessful" ]
(2) Pmin=? [ F "MissionSuccessful" ]
(3) Pmax=? [ F "MissionTimeUp" ]
(4) Pmin=? [ F "MissionTimeUp" ]
(5) Pmax=? [ F "Crash" ]
(6) Pmin=? [ F "Crash" ]
(7) Pmax=? [ F "AllAreaSearched" ]
(8) Pmin=? [ F "AllAreaSearched" ]
(9) Pmax=? [ F "MissionTimeUp"|"AllAreaSearched"|"Crash" ]
(10) Pmin=? [ F "MissionTimeUp"|"AllAreaSearched"|"Crash" ]
(11) R{"drop"}max=? [ F "deadlock" ]
(12) R{"drop"}min=? [ F "deadlock" ]
(13) R{"charge"}max=? [ F "deadlock" ]
(14) R{"charge"}min=? [ F "deadlock" ]
(15) R{"time"}max=? [ F "deadlock" ]
(16) R{"time"}min=? [ F "deadlock" ]
(17) R{"success"}max=? [ F "deadlock" ]/Pmin=? [ F "MissionSuccessful" ]
(18) R{"success"}min=? [ F "deadlock" ]/Pmax=? [ F "MissionSuccessful" ]

Type:        MDP
Modules:     Obj Move Robot T 
Variables:   NoOfObjs posx posy s1 retx rety b t c 

---------------------------------------------------------------------

Model checking: Pmax=? [ F "MissionSuccessful" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=4,Objy1=2,Objx2=6,Objy2=2

Building model...
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=4,Objy1=2,Objx2=6,Objy2=2

Warning: Guard for command 7 of module "Move" is never satisfied.

Warning: Guard for command 8 of module "Move" is never satisfied.

Warning: Guard for command 9 of module "Move" is never satisfied.

Warning: Guard for command 7 of module "Robot" is never satisfied.

Warning: Update 2 of command 3 of module "Robot" doesn't do anything ("(s1'=13)", line 210, column 44)

Warning: Update 2 of command 27 of module "T" doesn't do anything ("(c'=1)", line 323, column 108)

Warning: Guard for command 28 of module "T" is never satisfied.

Warning: Update 2 of command 9 of module "T" doesn't do anything ("(c'=1)", line 300, column 112)

Warning: Guard for command 10 of module "T" is never satisfied.

Warning: Update 2 of command 51 of module "T" doesn't do anything ("(c'=1)", line 354, column 113)

Warning: Guard for command 52 of module "T" is never satisfied.

Computing reachable states...

Reachability (BFS): 88 iterations in 1.17 seconds (average 0.013273, setup 0.00)

Time for model construction: 2.686 seconds.

Warning: Deadlocks detected and fixed in 1 states

Type:        MDP
States:      242446 (1 initial)
Transitions: 696693
Choices:     333334

Transition matrix: 209751 nodes (89 terminal), 696693 minterms, vars: 34r/34c/18nd

Prob0A: 51 iterations in 0.44 seconds (average 0.008706, setup 0.00)

Prob1E: 4 iterations in 0.06 seconds (average 0.015000, setup 0.00)

yes = 1058, no = 199366, maybe = 42022

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=242446, nc=76311, nnz=222791, k=2] [2.9 MB]
Creating vector for yes... [1.8 MB]
Allocating iteration vectors... [2 x 1.8 MB]
TOTAL: [8.4 MB]

Starting iterations...

Iterative method: 102 iterations in 0.39 seconds (average 0.001725, setup 0.22)

Value in the initial state: 0.9803093870564649

Time for model checking: 0.926 seconds.

Result: 0.9803093870564649 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmin=? [ F "MissionSuccessful" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=4,Objy1=2,Objx2=6,Objy2=2

Prob0E: 51 iterations in 0.35 seconds (average 0.006824, setup 0.00)

Prob1A: 3 iterations in 0.04 seconds (average 0.012000, setup 0.00)

yes = 1058, no = 204674, maybe = 36714

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=242446, nc=66365, nnz=193866, k=2] [2.5 MB]
Creating vector for yes... [1.8 MB]
Allocating iteration vectors... [2 x 1.8 MB]
TOTAL: [8.1 MB]

Starting iterations...

Iterative method: 92 iterations in 0.34 seconds (average 0.001609, setup 0.19)

Value in the initial state: 0.7254363224267948

Time for model checking: 0.738 seconds.

Result: 0.7254363224267948 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmax=? [ F "MissionTimeUp" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=4,Objy1=2,Objx2=6,Objy2=2

Prob0A: 78 iterations in 0.78 seconds (average 0.010051, setup 0.00)

Prob1E: 4 iterations in 0.04 seconds (average 0.009000, setup 0.00)

yes = 312, no = 143208, maybe = 98926

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=242446, nc=178190, nnz=504763, k=4] [6.2 MB]
Creating vector for yes... [1.8 MB]
Allocating iteration vectors... [2 x 1.8 MB]
TOTAL: [11.7 MB]

Starting iterations...

Iterative method: 99 iterations in 0.52 seconds (average 0.002141, setup 0.31)

Value in the initial state: 7.082502045573671E-4

Time for model checking: 1.335 seconds.

Result: 7.082502045573671E-4 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmin=? [ F "MissionTimeUp" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=4,Objy1=2,Objx2=6,Objy2=2

Prob0E: 92 iterations in 0.58 seconds (average 0.006304, setup 0.00)

Prob1A: 3 iterations in 0.04 seconds (average 0.012000, setup 0.00)

yes = 312, no = 143579, maybe = 98555

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=242446, nc=177514, nnz=503560, k=4] [6.2 MB]
Creating vector for yes... [1.8 MB]
Allocating iteration vectors... [2 x 1.8 MB]
TOTAL: [11.7 MB]

Starting iterations...

Iterative method: 114 iterations in 0.52 seconds (average 0.002140, setup 0.28)

Value in the initial state: 1.8880910164058668E-6

Time for model checking: 1.156 seconds.

Result: 1.8880910164058668E-6 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmax=? [ F "Crash" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=4,Objy1=2,Objx2=6,Objy2=2

Prob0A: 3 iterations in 0.06 seconds (average 0.021333, setup 0.00)

Prob1E: 196 iterations in 2.14 seconds (average 0.010898, setup 0.00)

yes = 118322, no = 2837, maybe = 121287

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=242446, nc=212175, nnz=575534, k=4] [7.0 MB]
Creating vector for yes... [1.8 MB]
Allocating iteration vectors... [2 x 1.8 MB]
TOTAL: [12.6 MB]

Starting iterations...

Iterative method: 94 iterations in 0.56 seconds (average 0.002298, setup 0.34)

Value in the initial state: 0.03200795238427392

Time for model checking: 2.78 seconds.

Result: 0.03200795238427392 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmin=? [ F "Crash" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=4,Objy1=2,Objx2=6,Objy2=2

Prob0E: 3 iterations in 0.05 seconds (average 0.016000, setup 0.00)

Prob1A: 51 iterations in 0.37 seconds (average 0.007216, setup 0.00)

yes = 118322, no = 2837, maybe = 121287

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=242446, nc=212175, nnz=575534, k=4] [7.0 MB]
Creating vector for yes... [1.8 MB]
Allocating iteration vectors... [2 x 1.8 MB]
TOTAL: [12.6 MB]

Starting iterations...

Iterative method: 95 iterations in 0.48 seconds (average 0.002358, setup 0.25)

Value in the initial state: 0.017547917621631055

Time for model checking: 0.903 seconds.

Result: 0.017547917621631055 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmax=? [ F "AllAreaSearched" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=4,Objy1=2,Objx2=6,Objy2=2

Prob0A: 55 iterations in 0.32 seconds (average 0.005745, setup 0.00)

Prob1E: 4 iterations in 0.03 seconds (average 0.008000, setup 0.00)

yes = 1466, no = 179884, maybe = 61096

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=242446, nc=103029, nnz=289613, k=2] [3.6 MB]
Creating vector for yes... [1.8 MB]
Allocating iteration vectors... [2 x 1.8 MB]
TOTAL: [9.2 MB]

Starting iterations...

Iterative method: 90 iterations in 0.37 seconds (average 0.001778, setup 0.21)

Value in the initial state: 0.2452291525699174

Time for model checking: 0.746 seconds.

Result: 0.2452291525699174 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmin=? [ F "AllAreaSearched" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=4,Objy1=2,Objx2=6,Objy2=2

Prob0E: 64 iterations in 0.38 seconds (average 0.006000, setup 0.00)

Prob1A: 3 iterations in 0.03 seconds (average 0.010667, setup 0.00)

yes = 1466, no = 187680, maybe = 53300

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=242446, nc=88461, nnz=246468, k=2] [3.1 MB]
Creating vector for yes... [1.8 MB]
Allocating iteration vectors... [2 x 1.8 MB]
TOTAL: [8.7 MB]

Starting iterations...

Iterative method: 98 iterations in 0.32 seconds (average 0.001714, setup 0.15)

Value in the initial state: 0.0017492885073894155

Time for model checking: 0.744 seconds.

Result: 0.0017492885073894155 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmax=? [ F "MissionTimeUp"|"AllAreaSearched"|"Crash" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=4,Objy1=2,Objx2=6,Objy2=2

Prob0A: 3 iterations in 0.06 seconds (average 0.020000, setup 0.00)

Prob1E: 200 iterations in 2.04 seconds (average 0.010200, setup 0.00)

yes = 204673, no = 1059, maybe = 36714

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=242446, nc=66365, nnz=193866, k=2] [2.5 MB]
Creating vector for yes... [1.8 MB]
Allocating iteration vectors... [2 x 1.8 MB]
TOTAL: [8.1 MB]

Starting iterations...

Iterative method: 85 iterations in 0.32 seconds (average 0.001647, setup 0.18)

Value in the initial state: 0.2745634833742787

Time for model checking: 2.462 seconds.

Result: 0.2745634833742787 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmin=? [ F "MissionTimeUp"|"AllAreaSearched"|"Crash" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=4,Objy1=2,Objx2=6,Objy2=2

Prob0E: 3 iterations in 0.04 seconds (average 0.013333, setup 0.00)

Prob1A: 51 iterations in 0.32 seconds (average 0.006275, setup 0.00)

yes = 199365, no = 1059, maybe = 42022

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=242446, nc=76311, nnz=222791, k=2] [2.9 MB]
Creating vector for yes... [1.8 MB]
Allocating iteration vectors... [2 x 1.8 MB]
TOTAL: [8.4 MB]

Starting iterations...

Iterative method: 98 iterations in 0.33 seconds (average 0.001673, setup 0.17)

Value in the initial state: 0.019690253704835653

Time for model checking: 0.713 seconds.

Result: 0.019690253704835653 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"drop"}max=? [ F "deadlock" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=4,Objy1=2,Objx2=6,Objy2=2

Prob0E: 4 iterations in 0.04 seconds (average 0.011000, setup 0.00)

Prob1A: 1 iterations in 0.00 seconds (average 0.004000, setup 0.00)

goal = 1, inf = 0, maybe = 242445

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=242446, nc=333333, nnz=696692, k=4] [10.2 MB]
Building sparse matrix (transition rewards)... [n=242446, nc=333333, nnz=0, k=4] [2.2 MB]
Creating vector for state rewards... [1.8 MB]
Creating vector for inf... [1.8 MB]
Allocating iteration vectors... [2 x 1.8 MB]
TOTAL: [19.8 MB]

Starting iterations...

Iterative method: 97 iterations in 0.77 seconds (average 0.003546, setup 0.42)

Value in the initial state: 0.25386903256064913

Time for model checking: 0.839 seconds.

Result: 0.25386903256064913 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"drop"}min=? [ F "deadlock" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=4,Objy1=2,Objx2=6,Objy2=2

Prob0A: 4 iterations in 0.03 seconds (average 0.008000, setup 0.00)

Prob1E: 5 iterations in 0.03 seconds (average 0.006400, setup 0.00)

Warning: PRISM hasn't checked for zero-reward loops.
Your minimum rewards may be too low...

goal = 1, inf = 0, maybe = 242445

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=242446, nc=333333, nnz=696692, k=4] [10.2 MB]
Building sparse matrix (transition rewards)... [n=242446, nc=333333, nnz=0, k=4] [2.2 MB]
Creating vector for state rewards... [1.8 MB]
Creating vector for inf... [1.8 MB]
Allocating iteration vectors... [2 x 1.8 MB]
TOTAL: [19.8 MB]

Starting iterations...

Iterative method: 86 iterations in 0.65 seconds (average 0.003581, setup 0.34)

Value in the initial state: 0.20514596256332873

Time for model checking: 0.726 seconds.

Result: 0.20514596256332873 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"charge"}max=? [ F "deadlock" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=4,Objy1=2,Objx2=6,Objy2=2

Prob0E: 4 iterations in 0.02 seconds (average 0.005000, setup 0.00)

Prob1A: 1 iterations in 0.00 seconds (average 0.004000, setup 0.00)

goal = 1, inf = 0, maybe = 242445

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=242446, nc=333333, nnz=696692, k=4] [10.2 MB]
Building sparse matrix (transition rewards)... [n=242446, nc=333333, nnz=8920, k=4] [2.3 MB]
Creating vector for state rewards... [1.8 MB]
Creating vector for inf... [1.8 MB]
Allocating iteration vectors... [2 x 1.8 MB]
TOTAL: [19.9 MB]

Starting iterations...

Iterative method: 91 iterations in 0.67 seconds (average 0.003560, setup 0.35)

Value in the initial state: 0.31222064384637727

Time for model checking: 0.705 seconds.

Result: 0.31222064384637727 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"charge"}min=? [ F "deadlock" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=4,Objy1=2,Objx2=6,Objy2=2

Prob0A: 4 iterations in 0.02 seconds (average 0.005000, setup 0.00)

Prob1E: 5 iterations in 0.03 seconds (average 0.005600, setup 0.00)

Warning: PRISM hasn't checked for zero-reward loops.
Your minimum rewards may be too low...

goal = 1, inf = 0, maybe = 242445

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=242446, nc=333333, nnz=696692, k=4] [10.2 MB]
Building sparse matrix (transition rewards)... [n=242446, nc=333333, nnz=8920, k=4] [2.3 MB]
Creating vector for state rewards... [1.8 MB]
Creating vector for inf... [1.8 MB]
Allocating iteration vectors... [2 x 1.8 MB]
TOTAL: [19.9 MB]

Starting iterations...

Iterative method: 102 iterations in 0.71 seconds (average 0.003608, setup 0.34)

Value in the initial state: 0.06332375333650025

Time for model checking: 0.772 seconds.

Result: 0.06332375333650025 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"time"}max=? [ F "deadlock" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=4,Objy1=2,Objx2=6,Objy2=2

Prob0E: 4 iterations in 0.02 seconds (average 0.004000, setup 0.00)

Prob1A: 1 iterations in 0.00 seconds (average 0.004000, setup 0.00)

goal = 1, inf = 0, maybe = 242445

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=242446, nc=333333, nnz=696692, k=4] [10.2 MB]
Building sparse matrix (transition rewards)... [n=242446, nc=333333, nnz=121157, k=4] [3.6 MB]
Creating vector for state rewards... [1.8 MB]
Creating vector for inf... [1.8 MB]
Allocating iteration vectors... [2 x 1.8 MB]
TOTAL: [21.2 MB]

Starting iterations...

Iterative method: 97 iterations in 0.74 seconds (average 0.003629, setup 0.39)

Value in the initial state: 176.5444472746866

Time for model checking: 0.772 seconds.

Result: 176.5444472746866 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"time"}min=? [ F "deadlock" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=4,Objy1=2,Objx2=6,Objy2=2

Prob0A: 4 iterations in 0.02 seconds (average 0.004000, setup 0.00)

Prob1E: 5 iterations in 0.03 seconds (average 0.006400, setup 0.00)

Warning: PRISM hasn't checked for zero-reward loops.
Your minimum rewards may be too low...

goal = 1, inf = 0, maybe = 242445

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=242446, nc=333333, nnz=696692, k=4] [10.2 MB]
Building sparse matrix (transition rewards)... [n=242446, nc=333333, nnz=121157, k=4] [3.6 MB]
Creating vector for state rewards... [1.8 MB]
Creating vector for inf... [1.8 MB]
Allocating iteration vectors... [2 x 1.8 MB]
TOTAL: [21.2 MB]

Starting iterations...

Iterative method: 106 iterations in 0.78 seconds (average 0.003774, setup 0.38)

Value in the initial state: 97.23321693492471

Time for model checking: 0.838 seconds.

Result: 97.23321693492471 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"success"}max=? [ F "deadlock" ]/Pmin=? [ F "MissionSuccessful" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=4,Objy1=2,Objx2=6,Objy2=2

Prob0E: 4 iterations in 0.02 seconds (average 0.005000, setup 0.00)

Prob1A: 1 iterations in 0.00 seconds (average 0.004000, setup 0.00)

goal = 1, inf = 0, maybe = 242445

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=242446, nc=333333, nnz=696692, k=4] [10.2 MB]
Building sparse matrix (transition rewards)... [n=242446, nc=333333, nnz=2084, k=4] [2.2 MB]
Creating vector for state rewards... [1.8 MB]
Creating vector for inf... [1.8 MB]
Allocating iteration vectors... [2 x 1.8 MB]
TOTAL: [19.8 MB]

Starting iterations...

Iterative method: 98 iterations in 0.69 seconds (average 0.003551, setup 0.34)

Prob0E: 51 iterations in 0.24 seconds (average 0.004706, setup 0.00)

Prob1A: 3 iterations in 0.05 seconds (average 0.016000, setup 0.00)

yes = 1058, no = 204674, maybe = 36714

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=242446, nc=66365, nnz=193866, k=2] [2.5 MB]
Creating vector for yes... [1.8 MB]
Allocating iteration vectors... [2 x 1.8 MB]
TOTAL: [8.1 MB]

Starting iterations...

Iterative method: 92 iterations in 0.28 seconds (average 0.001652, setup 0.13)

Value in the initial state: 238.32492463195155

Time for model checking: 1.319 seconds.

Result: 238.32492463195155 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"success"}min=? [ F "deadlock" ]/Pmax=? [ F "MissionSuccessful" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=4,Objy1=2,Objx2=6,Objy2=2

Prob0A: 4 iterations in 0.05 seconds (average 0.012000, setup 0.00)

Prob1E: 5 iterations in 0.03 seconds (average 0.006400, setup 0.00)

Warning: PRISM hasn't checked for zero-reward loops.
Your minimum rewards may be too low...

goal = 1, inf = 0, maybe = 242445

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=242446, nc=333333, nnz=696692, k=4] [10.2 MB]
Building sparse matrix (transition rewards)... [n=242446, nc=333333, nnz=2084, k=4] [2.2 MB]
Creating vector for state rewards... [1.8 MB]
Creating vector for inf... [1.8 MB]
Allocating iteration vectors... [2 x 1.8 MB]
TOTAL: [19.8 MB]

Starting iterations...

Iterative method: 98 iterations in 0.69 seconds (average 0.003551, setup 0.34)

Prob0A: 51 iterations in 0.31 seconds (average 0.006118, setup 0.00)

Prob1E: 4 iterations in 0.03 seconds (average 0.008000, setup 0.00)

yes = 1058, no = 199366, maybe = 42022

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=242446, nc=76311, nnz=222791, k=2] [2.9 MB]
Creating vector for yes... [1.8 MB]
Allocating iteration vectors... [2 x 1.8 MB]
TOTAL: [8.4 MB]

Starting iterations...

Iterative method: 102 iterations in 0.35 seconds (average 0.001647, setup 0.18)

Value in the initial state: 77.63160861669499

Time for model checking: 1.482 seconds.

Result: 77.63160861669499 (value in the initial state)

---------------------------------------------------------------------

Note: There were 16 warnings during computation.

