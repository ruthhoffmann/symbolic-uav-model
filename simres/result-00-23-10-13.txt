PRISM
=====

Version: 4.3
Date: Wed Aug 03 15:54:11 BST 2016
Hostname: eglinton
Memory limits: cudd=1g, java(heap)=981.5m
Command line: prism uav_sim_mdp.prism -s uav_mdp.props -const 'Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=1,Objy1=0,Objx2=1,Objy2=3,'

Parsing model file "uav_sim_mdp.prism"...

Parsing properties file "uav_mdp.props"...

18 properties:
(1) Pmax=? [ F "MissionSuccessful" ]
(2) Pmin=? [ F "MissionSuccessful" ]
(3) Pmax=? [ F "MissionTimeUp" ]
(4) Pmin=? [ F "MissionTimeUp" ]
(5) Pmax=? [ F "Crash" ]
(6) Pmin=? [ F "Crash" ]
(7) Pmax=? [ F "AllAreaSearched" ]
(8) Pmin=? [ F "AllAreaSearched" ]
(9) Pmax=? [ F "MissionTimeUp"|"AllAreaSearched"|"Crash" ]
(10) Pmin=? [ F "MissionTimeUp"|"AllAreaSearched"|"Crash" ]
(11) R{"drop"}max=? [ F "deadlock" ]
(12) R{"drop"}min=? [ F "deadlock" ]
(13) R{"charge"}max=? [ F "deadlock" ]
(14) R{"charge"}min=? [ F "deadlock" ]
(15) R{"time"}max=? [ F "deadlock" ]
(16) R{"time"}min=? [ F "deadlock" ]
(17) R{"success"}max=? [ F "deadlock" ]/Pmin=? [ F "MissionSuccessful" ]
(18) R{"success"}min=? [ F "deadlock" ]/Pmax=? [ F "MissionSuccessful" ]

Type:        MDP
Modules:     Obj Move Robot T 
Variables:   NoOfObjs posx posy s1 retx rety b t c 

---------------------------------------------------------------------

Model checking: Pmax=? [ F "MissionSuccessful" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=1,Objy1=0,Objx2=1,Objy2=3

Building model...
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=1,Objy1=0,Objx2=1,Objy2=3

Warning: Guard for command 7 of module "Move" is never satisfied.

Warning: Guard for command 8 of module "Move" is never satisfied.

Warning: Guard for command 9 of module "Move" is never satisfied.

Warning: Guard for command 7 of module "Robot" is never satisfied.

Warning: Update 2 of command 3 of module "Robot" doesn't do anything ("(s1'=13)", line 210, column 44)

Warning: Update 2 of command 27 of module "T" doesn't do anything ("(c'=1)", line 323, column 108)

Warning: Guard for command 28 of module "T" is never satisfied.

Warning: Update 2 of command 9 of module "T" doesn't do anything ("(c'=1)", line 300, column 112)

Warning: Guard for command 10 of module "T" is never satisfied.

Warning: Update 2 of command 51 of module "T" doesn't do anything ("(c'=1)", line 354, column 113)

Warning: Guard for command 52 of module "T" is never satisfied.

Computing reachable states...

Reachability (BFS): 90 iterations in 1.91 seconds (average 0.021200, setup 0.00)

Time for model construction: 5.121 seconds.

Warning: Deadlocks detected and fixed in 1 states

Type:        MDP
States:      253304 (1 initial)
Transitions: 633980
Choices:     327154

Transition matrix: 202359 nodes (91 terminal), 633980 minterms, vars: 34r/34c/18nd

Prob0A: 64 iterations in 0.72 seconds (average 0.011187, setup 0.00)

Prob1E: 4 iterations in 0.11 seconds (average 0.027000, setup 0.00)

yes = 1098, no = 216616, maybe = 35590

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=253304, nc=58254, nnz=156618, k=2] [2.1 MB]
Creating vector for yes... [1.9 MB]
Allocating iteration vectors... [2 x 1.9 MB]
TOTAL: [7.9 MB]

Starting iterations...

Iterative method: 100 iterations in 0.48 seconds (average 0.001680, setup 0.31)

Value in the initial state: 0.9805570379377204

Time for model checking: 1.724 seconds.

Result: 0.9805570379377204 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmin=? [ F "MissionSuccessful" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=1,Objy1=0,Objx2=1,Objy2=3

Prob0E: 66 iterations in 0.72 seconds (average 0.010970, setup 0.00)

Prob1A: 3 iterations in 0.06 seconds (average 0.018667, setup 0.00)

yes = 1098, no = 221763, maybe = 30443

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=253304, nc=50387, nnz=135136, k=2] [1.8 MB]
Creating vector for yes... [1.9 MB]
Allocating iteration vectors... [2 x 1.9 MB]
TOTAL: [7.6 MB]

Starting iterations...

Iterative method: 95 iterations in 0.34 seconds (average 0.001642, setup 0.18)

Value in the initial state: 0.9676141135798136

Time for model checking: 1.548 seconds.

Result: 0.9676141135798136 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmax=? [ F "MissionTimeUp" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=1,Objy1=0,Objx2=1,Objy2=3

Prob0A: 75 iterations in 0.80 seconds (average 0.010667, setup 0.00)

Prob1E: 4 iterations in 0.05 seconds (average 0.012000, setup 0.00)

yes = 452, no = 143084, maybe = 109768

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=253304, nc=170885, nnz=444774, k=4] [5.5 MB]
Creating vector for yes... [1.9 MB]
Allocating iteration vectors... [2 x 1.9 MB]
TOTAL: [11.3 MB]

Starting iterations...

Iterative method: 101 iterations in 0.56 seconds (average 0.002257, setup 0.33)

Value in the initial state: 1.4996628205996994E-4

Time for model checking: 1.803 seconds.

Result: 1.4996628205996994E-4 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmin=? [ F "MissionTimeUp" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=1,Objy1=0,Objx2=1,Objy2=3

Prob0E: 86 iterations in 1.05 seconds (average 0.012186, setup 0.00)

Prob1A: 3 iterations in 0.05 seconds (average 0.016000, setup 0.00)

yes = 452, no = 143175, maybe = 109677

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=253304, nc=170742, nnz=444345, k=4] [5.5 MB]
Creating vector for yes... [1.9 MB]
Allocating iteration vectors... [2 x 1.9 MB]
TOTAL: [11.3 MB]

Starting iterations...

Iterative method: 118 iterations in 0.56 seconds (average 0.002271, setup 0.30)

Value in the initial state: 3.1896388452801085E-7

Time for model checking: 1.985 seconds.

Result: 3.1896388452801085E-7 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmax=? [ F "Crash" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=1,Objy1=0,Objx2=1,Objy2=3

Prob0A: 3 iterations in 0.08 seconds (average 0.026667, setup 0.00)

Prob1E: 211 iterations in 3.43 seconds (average 0.016246, setup 0.00)

yes = 124443, no = 3153, maybe = 125708

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=253304, nc=199558, nnz=506384, k=4] [6.2 MB]
Creating vector for yes... [1.9 MB]
Allocating iteration vectors... [2 x 1.9 MB]
TOTAL: [12.0 MB]

Starting iterations...

Iterative method: 98 iterations in 0.59 seconds (average 0.002367, setup 0.36)

Value in the initial state: 0.030046596231279796

Time for model checking: 5.508 seconds.

Result: 0.030046596231279796 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmin=? [ F "Crash" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=1,Objy1=0,Objx2=1,Objy2=3

Prob0E: 4 iterations in 0.07 seconds (average 0.017000, setup 0.00)

Prob1A: 54 iterations in 0.62 seconds (average 0.011481, setup 0.00)

yes = 124443, no = 3153, maybe = 125708

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=253304, nc=199558, nnz=506384, k=4] [6.2 MB]
Creating vector for yes... [1.9 MB]
Allocating iteration vectors... [2 x 1.9 MB]
TOTAL: [12.0 MB]

Starting iterations...

Iterative method: 93 iterations in 0.52 seconds (average 0.002409, setup 0.30)

Value in the initial state: 0.017499423003604794

Time for model checking: 1.487 seconds.

Result: 0.017499423003604794 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmax=? [ F "AllAreaSearched" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=1,Objy1=0,Objx2=1,Objy2=3

Prob0A: 61 iterations in 0.50 seconds (average 0.008262, setup 0.00)

Prob1E: 4 iterations in 0.05 seconds (average 0.013000, setup 0.00)

yes = 1602, no = 195168, maybe = 56534

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=253304, nc=88872, nnz=230273, k=2] [3.0 MB]
Creating vector for yes... [1.9 MB]
Allocating iteration vectors... [2 x 1.9 MB]
TOTAL: [8.8 MB]

Starting iterations...

Iterative method: 97 iterations in 0.41 seconds (average 0.001938, setup 0.22)

Value in the initial state: 0.0022821853220927474

Time for model checking: 1.008 seconds.

Result: 0.0022821853220927474 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmin=? [ F "AllAreaSearched" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=1,Objy1=0,Objx2=1,Objy2=3

Prob0E: 66 iterations in 0.54 seconds (average 0.008121, setup 0.00)

Prob1A: 3 iterations in 0.05 seconds (average 0.016000, setup 0.00)

yes = 1602, no = 202095, maybe = 49607

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=253304, nc=78621, nnz=202754, k=2] [2.6 MB]
Creating vector for yes... [1.9 MB]
Allocating iteration vectors... [2 x 1.9 MB]
TOTAL: [8.4 MB]

Starting iterations...

Iterative method: 101 iterations in 0.36 seconds (average 0.001941, setup 0.17)

Value in the initial state: 0.0018304344680542448

Time for model checking: 1.131 seconds.

Result: 0.0018304344680542448 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmax=? [ F "MissionTimeUp"|"AllAreaSearched"|"Crash" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=1,Objy1=0,Objx2=1,Objy2=3

Prob0A: 3 iterations in 0.10 seconds (average 0.034667, setup 0.00)

Prob1E: 256 iterations in 4.41 seconds (average 0.017219, setup 0.00)

yes = 221762, no = 1099, maybe = 30443

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=253304, nc=50387, nnz=135136, k=2] [1.8 MB]
Creating vector for yes... [1.9 MB]
Allocating iteration vectors... [2 x 1.9 MB]
TOTAL: [7.6 MB]

Starting iterations...

Iterative method: 92 iterations in 0.36 seconds (average 0.001652, setup 0.21)

Value in the initial state: 0.0323855789505149

Time for model checking: 5.073 seconds.

Result: 0.0323855789505149 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmin=? [ F "MissionTimeUp"|"AllAreaSearched"|"Crash" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=1,Objy1=0,Objx2=1,Objy2=3

Prob0E: 4 iterations in 0.08 seconds (average 0.019000, setup 0.00)

Prob1A: 64 iterations in 0.74 seconds (average 0.011562, setup 0.00)

yes = 216615, no = 1099, maybe = 35590

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=253304, nc=58254, nnz=156618, k=2] [2.1 MB]
Creating vector for yes... [1.9 MB]
Allocating iteration vectors... [2 x 1.9 MB]
TOTAL: [7.9 MB]

Starting iterations...

Iterative method: 99 iterations in 0.37 seconds (average 0.001778, setup 0.19)

Value in the initial state: 0.01944167666935568

Time for model checking: 1.212 seconds.

Result: 0.01944167666935568 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"drop"}max=? [ F "deadlock" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=1,Objy1=0,Objx2=1,Objy2=3

Prob0E: 5 iterations in 0.09 seconds (average 0.017600, setup 0.00)

Prob1A: 1 iterations in 0.01 seconds (average 0.008000, setup 0.00)

goal = 1, inf = 0, maybe = 253303

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=253304, nc=327153, nnz=633979, k=4] [9.5 MB]
Building sparse matrix (transition rewards)... [n=253304, nc=327153, nnz=0, k=4] [2.2 MB]
Creating vector for state rewards... [1.9 MB]
Creating vector for inf... [1.9 MB]
Allocating iteration vectors... [2 x 1.9 MB]
TOTAL: [19.4 MB]

Starting iterations...

Iterative method: 96 iterations in 0.80 seconds (average 0.003625, setup 0.46)

Value in the initial state: 0.1768049909950472

Time for model checking: 0.921 seconds.

Result: 0.1768049909950472 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"drop"}min=? [ F "deadlock" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=1,Objy1=0,Objx2=1,Objy2=3

Prob0A: 4 iterations in 0.06 seconds (average 0.014000, setup 0.00)

Prob1E: 5 iterations in 0.08 seconds (average 0.015200, setup 0.00)

Warning: PRISM hasn't checked for zero-reward loops.
Your minimum rewards may be too low...

goal = 1, inf = 0, maybe = 253303

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=253304, nc=327153, nnz=633979, k=4] [9.5 MB]
Building sparse matrix (transition rewards)... [n=253304, nc=327153, nnz=0, k=4] [2.2 MB]
Creating vector for state rewards... [1.9 MB]
Creating vector for inf... [1.9 MB]
Allocating iteration vectors... [2 x 1.9 MB]
TOTAL: [19.4 MB]

Starting iterations...

Iterative method: 89 iterations in 0.66 seconds (average 0.003640, setup 0.34)

Value in the initial state: 0.1641825506894561

Time for model checking: 0.824 seconds.

Result: 0.1641825506894561 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"charge"}max=? [ F "deadlock" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=1,Objy1=0,Objx2=1,Objy2=3

Prob0E: 5 iterations in 0.04 seconds (average 0.008800, setup 0.00)

Prob1A: 1 iterations in 0.01 seconds (average 0.008000, setup 0.00)

goal = 1, inf = 0, maybe = 253303

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=253304, nc=327153, nnz=633979, k=4] [9.5 MB]
Building sparse matrix (transition rewards)... [n=253304, nc=327153, nnz=6274, k=4] [2.3 MB]
Creating vector for state rewards... [1.9 MB]
Creating vector for inf... [1.9 MB]
Allocating iteration vectors... [2 x 1.9 MB]
TOTAL: [19.5 MB]

Starting iterations...

Iterative method: 96 iterations in 0.69 seconds (average 0.003667, setup 0.34)

Value in the initial state: 0.27208251746706297

Time for model checking: 0.769 seconds.

Result: 0.27208251746706297 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"charge"}min=? [ F "deadlock" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=1,Objy1=0,Objx2=1,Objy2=3

Prob0A: 4 iterations in 0.04 seconds (average 0.009000, setup 0.00)

Prob1E: 5 iterations in 0.07 seconds (average 0.013600, setup 0.00)

Warning: PRISM hasn't checked for zero-reward loops.
Your minimum rewards may be too low...

goal = 1, inf = 0, maybe = 253303

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=253304, nc=327153, nnz=633979, k=4] [9.5 MB]
Building sparse matrix (transition rewards)... [n=253304, nc=327153, nnz=6274, k=4] [2.3 MB]
Creating vector for state rewards... [1.9 MB]
Creating vector for inf... [1.9 MB]
Allocating iteration vectors... [2 x 1.9 MB]
TOTAL: [19.5 MB]

Starting iterations...

Iterative method: 102 iterations in 0.72 seconds (average 0.003647, setup 0.34)

Value in the initial state: 0.041094183291610645

Time for model checking: 0.846 seconds.

Result: 0.041094183291610645 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"time"}max=? [ F "deadlock" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=1,Objy1=0,Objx2=1,Objy2=3

Prob0E: 5 iterations in 0.04 seconds (average 0.008800, setup 0.00)

Prob1A: 1 iterations in 0.01 seconds (average 0.008000, setup 0.00)

goal = 1, inf = 0, maybe = 253303

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=253304, nc=327153, nnz=633979, k=4] [9.5 MB]
Building sparse matrix (transition rewards)... [n=253304, nc=327153, nnz=127594, k=4] [3.7 MB]
Creating vector for state rewards... [1.9 MB]
Creating vector for inf... [1.9 MB]
Allocating iteration vectors... [2 x 1.9 MB]
TOTAL: [20.9 MB]

Starting iterations...

Iterative method: 101 iterations in 0.77 seconds (average 0.003723, setup 0.39)

Value in the initial state: 165.87556849051865

Time for model checking: 0.832 seconds.

Result: 165.87556849051865 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"time"}min=? [ F "deadlock" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=1,Objy1=0,Objx2=1,Objy2=3

Prob0A: 4 iterations in 0.03 seconds (average 0.008000, setup 0.00)

Prob1E: 5 iterations in 0.06 seconds (average 0.012000, setup 0.00)

Warning: PRISM hasn't checked for zero-reward loops.
Your minimum rewards may be too low...

goal = 1, inf = 0, maybe = 253303

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=253304, nc=327153, nnz=633979, k=4] [9.5 MB]
Building sparse matrix (transition rewards)... [n=253304, nc=327153, nnz=127594, k=4] [3.7 MB]
Creating vector for state rewards... [1.9 MB]
Creating vector for inf... [1.9 MB]
Allocating iteration vectors... [2 x 1.9 MB]
TOTAL: [20.9 MB]

Starting iterations...

Iterative method: 108 iterations in 0.79 seconds (average 0.003815, setup 0.38)

Value in the initial state: 97.01413353255046

Time for model checking: 0.911 seconds.

Result: 97.01413353255046 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"success"}max=? [ F "deadlock" ]/Pmin=? [ F "MissionSuccessful" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=1,Objy1=0,Objx2=1,Objy2=3

Prob0E: 5 iterations in 0.04 seconds (average 0.008800, setup 0.00)

Prob1A: 1 iterations in 0.01 seconds (average 0.008000, setup 0.00)

goal = 1, inf = 0, maybe = 253303

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=253304, nc=327153, nnz=633979, k=4] [9.5 MB]
Building sparse matrix (transition rewards)... [n=253304, nc=327153, nnz=2164, k=4] [2.2 MB]
Creating vector for state rewards... [1.9 MB]
Creating vector for inf... [1.9 MB]
Allocating iteration vectors... [2 x 1.9 MB]
TOTAL: [19.4 MB]

Starting iterations...

Iterative method: 102 iterations in 0.71 seconds (average 0.003569, setup 0.34)

Prob0E: 66 iterations in 0.47 seconds (average 0.007152, setup 0.00)

Prob1A: 3 iterations in 0.06 seconds (average 0.020000, setup 0.00)

yes = 1098, no = 221763, maybe = 30443

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=253304, nc=50387, nnz=135136, k=2] [1.8 MB]
Creating vector for yes... [1.9 MB]
Allocating iteration vectors... [2 x 1.9 MB]
TOTAL: [7.6 MB]

Starting iterations...

Iterative method: 95 iterations in 0.34 seconds (average 0.001600, setup 0.18)

Value in the initial state: 168.5246347850965

Time for model checking: 1.658 seconds.

Result: 168.5246347850965 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"success"}min=? [ F "deadlock" ]/Pmax=? [ F "MissionSuccessful" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=1,Objy1=0,Objx2=1,Objy2=3

Prob0A: 4 iterations in 0.09 seconds (average 0.022000, setup 0.00)

Prob1E: 5 iterations in 0.06 seconds (average 0.012000, setup 0.00)

Warning: PRISM hasn't checked for zero-reward loops.
Your minimum rewards may be too low...

goal = 1, inf = 0, maybe = 253303

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=253304, nc=327153, nnz=633979, k=4] [9.5 MB]
Building sparse matrix (transition rewards)... [n=253304, nc=327153, nnz=2164, k=4] [2.2 MB]
Creating vector for state rewards... [1.9 MB]
Creating vector for inf... [1.9 MB]
Allocating iteration vectors... [2 x 1.9 MB]
TOTAL: [19.4 MB]

Starting iterations...

Iterative method: 101 iterations in 0.75 seconds (average 0.003683, setup 0.38)

Prob0A: 64 iterations in 0.47 seconds (average 0.007375, setup 0.00)

Prob1E: 4 iterations in 0.05 seconds (average 0.012000, setup 0.00)

yes = 1098, no = 216616, maybe = 35590

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=253304, nc=58254, nnz=156618, k=2] [2.1 MB]
Creating vector for yes... [1.9 MB]
Allocating iteration vectors... [2 x 1.9 MB]
TOTAL: [7.9 MB]

Starting iterations...

Iterative method: 100 iterations in 0.36 seconds (average 0.001680, setup 0.20)

Value in the initial state: 97.82656249048729

Time for model checking: 1.797 seconds.

Result: 97.82656249048729 (value in the initial state)

---------------------------------------------------------------------

Note: There were 16 warnings during computation.

