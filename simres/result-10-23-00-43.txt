PRISM
=====

Version: 4.3
Date: Wed Aug 03 16:06:24 BST 2016
Hostname: eglinton
Memory limits: cudd=1g, java(heap)=981.5m
Command line: prism uav_sim_mdp.prism -s uav_mdp.props -const 'Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=0,Objy1=0,Objx2=4,Objy2=3,'

Parsing model file "uav_sim_mdp.prism"...

Parsing properties file "uav_mdp.props"...

18 properties:
(1) Pmax=? [ F "MissionSuccessful" ]
(2) Pmin=? [ F "MissionSuccessful" ]
(3) Pmax=? [ F "MissionTimeUp" ]
(4) Pmin=? [ F "MissionTimeUp" ]
(5) Pmax=? [ F "Crash" ]
(6) Pmin=? [ F "Crash" ]
(7) Pmax=? [ F "AllAreaSearched" ]
(8) Pmin=? [ F "AllAreaSearched" ]
(9) Pmax=? [ F "MissionTimeUp"|"AllAreaSearched"|"Crash" ]
(10) Pmin=? [ F "MissionTimeUp"|"AllAreaSearched"|"Crash" ]
(11) R{"drop"}max=? [ F "deadlock" ]
(12) R{"drop"}min=? [ F "deadlock" ]
(13) R{"charge"}max=? [ F "deadlock" ]
(14) R{"charge"}min=? [ F "deadlock" ]
(15) R{"time"}max=? [ F "deadlock" ]
(16) R{"time"}min=? [ F "deadlock" ]
(17) R{"success"}max=? [ F "deadlock" ]/Pmin=? [ F "MissionSuccessful" ]
(18) R{"success"}min=? [ F "deadlock" ]/Pmax=? [ F "MissionSuccessful" ]

Type:        MDP
Modules:     Obj Move Robot T 
Variables:   NoOfObjs posx posy s1 retx rety b t c 

---------------------------------------------------------------------

Model checking: Pmax=? [ F "MissionSuccessful" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=0,Objy1=0,Objx2=4,Objy2=3

Building model...
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=0,Objy1=0,Objx2=4,Objy2=3

Warning: Guard for command 7 of module "Move" is never satisfied.

Warning: Guard for command 8 of module "Move" is never satisfied.

Warning: Guard for command 9 of module "Move" is never satisfied.

Warning: Guard for command 7 of module "Robot" is never satisfied.

Warning: Update 2 of command 3 of module "Robot" doesn't do anything ("(s1'=13)", line 210, column 44)

Warning: Update 2 of command 27 of module "T" doesn't do anything ("(c'=1)", line 323, column 108)

Warning: Guard for command 28 of module "T" is never satisfied.

Warning: Update 2 of command 9 of module "T" doesn't do anything ("(c'=1)", line 300, column 112)

Warning: Guard for command 10 of module "T" is never satisfied.

Warning: Update 2 of command 51 of module "T" doesn't do anything ("(c'=1)", line 354, column 113)

Warning: Guard for command 52 of module "T" is never satisfied.

Computing reachable states...

Reachability (BFS): 88 iterations in 1.81 seconds (average 0.020591, setup 0.00)

Time for model construction: 5.783 seconds.

Warning: Deadlocks detected and fixed in 1 states

Type:        MDP
States:      299588 (1 initial)
Transitions: 769683
Choices:     390568

Transition matrix: 229131 nodes (93 terminal), 769683 minterms, vars: 34r/34c/18nd

Prob0A: 63 iterations in 0.78 seconds (average 0.012381, setup 0.00)

Prob1E: 4 iterations in 0.13 seconds (average 0.032000, setup 0.00)

yes = 1208, no = 255940, maybe = 42440

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=299588, nc=71151, nnz=196701, k=2] [2.6 MB]
Creating vector for yes... [2.3 MB]
Allocating iteration vectors... [2 x 2.3 MB]
TOTAL: [9.5 MB]

Starting iterations...

Iterative method: 100 iterations in 0.54 seconds (average 0.002160, setup 0.32)

Value in the initial state: 0.9802099269289629

Time for model checking: 1.655 seconds.

Result: 0.9802099269289629 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmin=? [ F "MissionSuccessful" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=0,Objy1=0,Objx2=4,Objy2=3

Prob0E: 65 iterations in 0.68 seconds (average 0.010462, setup 0.00)

Prob1A: 4 iterations in 0.08 seconds (average 0.019000, setup 0.00)

yes = 1208, no = 261896, maybe = 36484

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=299588, nc=61636, nnz=170222, k=2] [2.3 MB]
Creating vector for yes... [2.3 MB]
Allocating iteration vectors... [2 x 2.3 MB]
TOTAL: [9.1 MB]

Starting iterations...

Iterative method: 95 iterations in 0.39 seconds (average 0.001979, setup 0.20)

Value in the initial state: 0.746982534763657

Time for model checking: 1.424 seconds.

Result: 0.746982534763657 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmax=? [ F "MissionTimeUp" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=0,Objy1=0,Objx2=4,Objy2=3

Prob0A: 73 iterations in 0.95 seconds (average 0.012986, setup 0.00)

Prob1E: 4 iterations in 0.07 seconds (average 0.018000, setup 0.00)

yes = 496, no = 171926, maybe = 127166

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=299588, nc=204451, nnz=543741, k=4] [6.7 MB]
Creating vector for yes... [2.3 MB]
Allocating iteration vectors... [2 x 2.3 MB]
TOTAL: [13.6 MB]

Starting iterations...

Iterative method: 99 iterations in 0.64 seconds (average 0.002747, setup 0.37)

Value in the initial state: 2.2504730788256433E-4

Time for model checking: 2.448 seconds.

Result: 2.2504730788256433E-4 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmin=? [ F "MissionTimeUp" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=0,Objy1=0,Objx2=4,Objy2=3

Prob0E: 83 iterations in 1.34 seconds (average 0.016096, setup 0.00)

Prob1A: 4 iterations in 0.08 seconds (average 0.020000, setup 0.00)

yes = 496, no = 172230, maybe = 126862

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=299588, nc=203919, nnz=542605, k=4] [6.7 MB]
Creating vector for yes... [2.3 MB]
Allocating iteration vectors... [2 x 2.3 MB]
TOTAL: [13.5 MB]

Starting iterations...

Iterative method: 116 iterations in 0.67 seconds (average 0.002759, setup 0.35)

Value in the initial state: 5.482073551467297E-7

Time for model checking: 2.572 seconds.

Result: 5.482073551467297E-7 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmax=? [ F "Crash" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=0,Objy1=0,Objx2=4,Objy2=3

Prob0A: 4 iterations in 0.15 seconds (average 0.038000, setup 0.00)

Prob1E: 253 iterations in 5.08 seconds (average 0.020079, setup 0.00)

yes = 146909, no = 3467, maybe = 149212

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=299588, nc=240192, nnz=619307, k=4] [7.6 MB]
Creating vector for yes... [2.3 MB]
Allocating iteration vectors... [2 x 2.3 MB]
TOTAL: [14.5 MB]

Starting iterations...

Iterative method: 96 iterations in 0.69 seconds (average 0.002875, setup 0.42)

Value in the initial state: 0.03113349784663196

Time for model checking: 8.022 seconds.

Result: 0.03113349784663196 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmin=? [ F "Crash" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=0,Objy1=0,Objx2=4,Objy2=3

Prob0E: 4 iterations in 0.09 seconds (average 0.022000, setup 0.00)

Prob1A: 52 iterations in 0.72 seconds (average 0.013923, setup 0.00)

yes = 146909, no = 3467, maybe = 149212

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=299588, nc=240192, nnz=619307, k=4] [7.6 MB]
Creating vector for yes... [2.3 MB]
Allocating iteration vectors... [2 x 2.3 MB]
TOTAL: [14.5 MB]

Starting iterations...

Iterative method: 94 iterations in 0.55 seconds (average 0.002894, setup 0.28)

Value in the initial state: 0.017721386258365744

Time for model checking: 1.841 seconds.

Result: 0.017721386258365744 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmax=? [ F "AllAreaSearched" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=0,Objy1=0,Objx2=4,Objy2=3

Prob0A: 64 iterations in 0.56 seconds (average 0.008750, setup 0.00)

Prob1E: 4 iterations in 0.06 seconds (average 0.016000, setup 0.00)

yes = 1762, no = 234254, maybe = 63572

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=299588, nc=101497, nnz=269874, k=2] [3.5 MB]
Creating vector for yes... [2.3 MB]
Allocating iteration vectors... [2 x 2.3 MB]
TOTAL: [10.3 MB]

Starting iterations...

Iterative method: 89 iterations in 0.44 seconds (average 0.002472, setup 0.22)

Value in the initial state: 0.22461309425289816

Time for model checking: 1.416 seconds.

Result: 0.22461309425289816 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmin=? [ F "AllAreaSearched" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=0,Objy1=0,Objx2=4,Objy2=3

Prob0E: 67 iterations in 0.53 seconds (average 0.007940, setup 0.00)

Prob1A: 4 iterations in 0.04 seconds (average 0.010000, setup 0.00)

yes = 1762, no = 242909, maybe = 54917

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=299588, nc=88389, nnz=235178, k=2] [3.1 MB]
Creating vector for yes... [2.3 MB]
Allocating iteration vectors... [2 x 2.3 MB]
TOTAL: [9.9 MB]

Starting iterations...

Iterative method: 96 iterations in 0.37 seconds (average 0.002375, setup 0.14)

Value in the initial state: 0.0017917305945488295

Time for model checking: 1.043 seconds.

Result: 0.0017917305945488295 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmax=? [ F "MissionTimeUp"|"AllAreaSearched"|"Crash" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=0,Objy1=0,Objx2=4,Objy2=3

Prob0A: 4 iterations in 0.11 seconds (average 0.027000, setup 0.00)

Prob1E: 315 iterations in 6.72 seconds (average 0.021321, setup 0.00)

yes = 261895, no = 1209, maybe = 36484

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=299588, nc=61636, nnz=170222, k=2] [2.3 MB]
Creating vector for yes... [2.3 MB]
Allocating iteration vectors... [2 x 2.3 MB]
TOTAL: [9.1 MB]

Starting iterations...

Iterative method: 87 iterations in 0.37 seconds (average 0.002023, setup 0.20)

Value in the initial state: 0.25301715598349855

Time for model checking: 9.511 seconds.

Result: 0.25301715598349855 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmin=? [ F "MissionTimeUp"|"AllAreaSearched"|"Crash" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=0,Objy1=0,Objx2=4,Objy2=3

Prob0E: 4 iterations in 0.07 seconds (average 0.017000, setup 0.00)

Prob1A: 63 iterations in 0.92 seconds (average 0.014603, setup 0.00)

yes = 255939, no = 1209, maybe = 42440

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=299588, nc=71151, nnz=196701, k=2] [2.6 MB]
Creating vector for yes... [2.3 MB]
Allocating iteration vectors... [2 x 2.3 MB]
TOTAL: [9.5 MB]

Starting iterations...

Iterative method: 98 iterations in 0.40 seconds (average 0.002163, setup 0.18)

Value in the initial state: 0.019789429040587836

Time for model checking: 1.92 seconds.

Result: 0.019789429040587836 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"drop"}max=? [ F "deadlock" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=0,Objy1=0,Objx2=4,Objy2=3

Prob0E: 5 iterations in 0.10 seconds (average 0.020000, setup 0.00)

Prob1A: 1 iterations in 0.01 seconds (average 0.008000, setup 0.00)

goal = 1, inf = 0, maybe = 299587

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=299588, nc=390567, nnz=769682, k=4] [11.4 MB]
Building sparse matrix (transition rewards)... [n=299588, nc=390567, nnz=0, k=4] [2.6 MB]
Creating vector for state rewards... [2.3 MB]
Creating vector for inf... [2.3 MB]
Allocating iteration vectors... [2 x 2.3 MB]
TOTAL: [23.2 MB]

Starting iterations...

Iterative method: 95 iterations in 0.94 seconds (average 0.004379, setup 0.52)

Value in the initial state: 0.21177489001883137

Time for model checking: 1.408 seconds.

Result: 0.21177489001883137 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"drop"}min=? [ F "deadlock" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=0,Objy1=0,Objx2=4,Objy2=3

Prob0A: 5 iterations in 0.06 seconds (average 0.012800, setup 0.00)

Prob1E: 6 iterations in 0.10 seconds (average 0.016000, setup 0.00)

Warning: PRISM hasn't checked for zero-reward loops.
Your minimum rewards may be too low...

goal = 1, inf = 0, maybe = 299587

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=299588, nc=390567, nnz=769682, k=4] [11.4 MB]
Building sparse matrix (transition rewards)... [n=299588, nc=390567, nnz=0, k=4] [2.6 MB]
Creating vector for state rewards... [2.3 MB]
Creating vector for inf... [2.3 MB]
Allocating iteration vectors... [2 x 2.3 MB]
TOTAL: [23.2 MB]

Starting iterations...

Iterative method: 87 iterations in 0.77 seconds (average 0.004368, setup 0.39)

Value in the initial state: 0.18497140961724481

Time for model checking: 1.226 seconds.

Result: 0.18497140961724481 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"charge"}max=? [ F "deadlock" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=0,Objy1=0,Objx2=4,Objy2=3

Prob0E: 5 iterations in 0.06 seconds (average 0.012000, setup 0.00)

Prob1A: 1 iterations in 0.01 seconds (average 0.008000, setup 0.00)

goal = 1, inf = 0, maybe = 299587

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=299588, nc=390567, nnz=769682, k=4] [11.4 MB]
Building sparse matrix (transition rewards)... [n=299588, nc=390567, nnz=8160, k=4] [2.7 MB]
Creating vector for state rewards... [2.3 MB]
Creating vector for inf... [2.3 MB]
Allocating iteration vectors... [2 x 2.3 MB]
TOTAL: [23.3 MB]

Starting iterations...

Iterative method: 94 iterations in 0.82 seconds (average 0.004383, setup 0.40)

Value in the initial state: 0.29183096260678143

Time for model checking: 1.048 seconds.

Result: 0.29183096260678143 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"charge"}min=? [ F "deadlock" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=0,Objy1=0,Objx2=4,Objy2=3

Prob0A: 5 iterations in 0.05 seconds (average 0.009600, setup 0.00)

Prob1E: 6 iterations in 0.10 seconds (average 0.016000, setup 0.00)

Warning: PRISM hasn't checked for zero-reward loops.
Your minimum rewards may be too low...

goal = 1, inf = 0, maybe = 299587

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=299588, nc=390567, nnz=769682, k=4] [11.4 MB]
Building sparse matrix (transition rewards)... [n=299588, nc=390567, nnz=8160, k=4] [2.7 MB]
Creating vector for state rewards... [2.3 MB]
Creating vector for inf... [2.3 MB]
Allocating iteration vectors... [2 x 2.3 MB]
TOTAL: [23.3 MB]

Starting iterations...

Iterative method: 100 iterations in 0.85 seconds (average 0.004400, setup 0.41)

Value in the initial state: 0.051824198253417976

Time for model checking: 1.18 seconds.

Result: 0.051824198253417976 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"time"}max=? [ F "deadlock" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=0,Objy1=0,Objx2=4,Objy2=3

Prob0E: 5 iterations in 0.05 seconds (average 0.009600, setup 0.00)

Prob1A: 1 iterations in 0.01 seconds (average 0.008000, setup 0.00)

goal = 1, inf = 0, maybe = 299587

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=299588, nc=390567, nnz=769682, k=4] [11.4 MB]
Building sparse matrix (transition rewards)... [n=299588, nc=390567, nnz=150374, k=4] [4.4 MB]
Creating vector for state rewards... [2.3 MB]
Creating vector for inf... [2.3 MB]
Allocating iteration vectors... [2 x 2.3 MB]
TOTAL: [24.9 MB]

Starting iterations...

Iterative method: 99 iterations in 0.90 seconds (average 0.004525, setup 0.46)

Value in the initial state: 171.81951880746024

Time for model checking: 1.149 seconds.

Result: 171.81951880746024 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"time"}min=? [ F "deadlock" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=0,Objy1=0,Objx2=4,Objy2=3

Prob0A: 5 iterations in 0.05 seconds (average 0.009600, setup 0.00)

Prob1E: 6 iterations in 0.10 seconds (average 0.016667, setup 0.00)

Warning: PRISM hasn't checked for zero-reward loops.
Your minimum rewards may be too low...

goal = 1, inf = 0, maybe = 299587

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=299588, nc=390567, nnz=769682, k=4] [11.4 MB]
Building sparse matrix (transition rewards)... [n=299588, nc=390567, nnz=150374, k=4] [4.4 MB]
Creating vector for state rewards... [2.3 MB]
Creating vector for inf... [2.3 MB]
Allocating iteration vectors... [2 x 2.3 MB]
TOTAL: [24.9 MB]

Starting iterations...

Iterative method: 106 iterations in 0.93 seconds (average 0.004491, setup 0.45)

Value in the initial state: 98.20909317603493

Time for model checking: 1.096 seconds.

Result: 98.20909317603493 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"success"}max=? [ F "deadlock" ]/Pmin=? [ F "MissionSuccessful" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=0,Objy1=0,Objx2=4,Objy2=3

Prob0E: 5 iterations in 0.04 seconds (average 0.008000, setup 0.00)

Prob1A: 1 iterations in 0.01 seconds (average 0.008000, setup 0.00)

goal = 1, inf = 0, maybe = 299587

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=299588, nc=390567, nnz=769682, k=4] [11.4 MB]
Building sparse matrix (transition rewards)... [n=299588, nc=390567, nnz=2380, k=4] [2.7 MB]
Creating vector for state rewards... [2.3 MB]
Creating vector for inf... [2.3 MB]
Allocating iteration vectors... [2 x 2.3 MB]
TOTAL: [23.2 MB]

Starting iterations...

Iterative method: 100 iterations in 0.91 seconds (average 0.004720, setup 0.44)

Prob0E: 65 iterations in 0.50 seconds (average 0.007631, setup 0.00)

Prob1A: 4 iterations in 0.06 seconds (average 0.014000, setup 0.00)

yes = 1208, no = 261896, maybe = 36484

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=299588, nc=61636, nnz=170222, k=2] [2.3 MB]
Creating vector for yes... [2.3 MB]
Allocating iteration vectors... [2 x 2.3 MB]
TOTAL: [9.1 MB]

Starting iterations...

Iterative method: 95 iterations in 0.37 seconds (average 0.001937, setup 0.18)

Value in the initial state: 225.56953543869054

Time for model checking: 1.922 seconds.

Result: 225.56953543869054 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"success"}min=? [ F "deadlock" ]/Pmax=? [ F "MissionSuccessful" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=0,Objy1=0,Objx2=4,Objy2=3

Prob0A: 5 iterations in 0.13 seconds (average 0.025600, setup 0.00)

Prob1E: 6 iterations in 0.09 seconds (average 0.015333, setup 0.00)

Warning: PRISM hasn't checked for zero-reward loops.
Your minimum rewards may be too low...

goal = 1, inf = 0, maybe = 299587

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=299588, nc=390567, nnz=769682, k=4] [11.4 MB]
Building sparse matrix (transition rewards)... [n=299588, nc=390567, nnz=2380, k=4] [2.7 MB]
Creating vector for state rewards... [2.3 MB]
Creating vector for inf... [2.3 MB]
Allocating iteration vectors... [2 x 2.3 MB]
TOTAL: [23.2 MB]

Starting iterations...

Iterative method: 99 iterations in 0.84 seconds (average 0.004283, setup 0.42)

Prob0A: 63 iterations in 0.42 seconds (average 0.006667, setup 0.00)

Prob1E: 4 iterations in 0.03 seconds (average 0.008000, setup 0.00)

yes = 1208, no = 255940, maybe = 42440

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=299588, nc=71151, nnz=196701, k=2] [2.6 MB]
Creating vector for yes... [2.3 MB]
Allocating iteration vectors... [2 x 2.3 MB]
TOTAL: [9.5 MB]

Starting iterations...

Iterative method: 100 iterations in 0.34 seconds (average 0.001840, setup 0.15)

Value in the initial state: 79.33380899716701

Time for model checking: 1.876 seconds.

Result: 79.33380899716701 (value in the initial state)

---------------------------------------------------------------------

Note: There were 16 warnings during computation.

