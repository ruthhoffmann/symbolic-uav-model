PRISM
=====

Version: 4.3
Date: Wed Aug 03 16:13:05 BST 2016
Hostname: eglinton
Memory limits: cudd=1g, java(heap)=981.5m
Command line: prism uav_sim_mdp.prism -s uav_mdp.props -const 'Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=3,Objy1=0,Objx2=6,Objy2=2,'

Parsing model file "uav_sim_mdp.prism"...

Parsing properties file "uav_mdp.props"...

18 properties:
(1) Pmax=? [ F "MissionSuccessful" ]
(2) Pmin=? [ F "MissionSuccessful" ]
(3) Pmax=? [ F "MissionTimeUp" ]
(4) Pmin=? [ F "MissionTimeUp" ]
(5) Pmax=? [ F "Crash" ]
(6) Pmin=? [ F "Crash" ]
(7) Pmax=? [ F "AllAreaSearched" ]
(8) Pmin=? [ F "AllAreaSearched" ]
(9) Pmax=? [ F "MissionTimeUp"|"AllAreaSearched"|"Crash" ]
(10) Pmin=? [ F "MissionTimeUp"|"AllAreaSearched"|"Crash" ]
(11) R{"drop"}max=? [ F "deadlock" ]
(12) R{"drop"}min=? [ F "deadlock" ]
(13) R{"charge"}max=? [ F "deadlock" ]
(14) R{"charge"}min=? [ F "deadlock" ]
(15) R{"time"}max=? [ F "deadlock" ]
(16) R{"time"}min=? [ F "deadlock" ]
(17) R{"success"}max=? [ F "deadlock" ]/Pmin=? [ F "MissionSuccessful" ]
(18) R{"success"}min=? [ F "deadlock" ]/Pmax=? [ F "MissionSuccessful" ]

Type:        MDP
Modules:     Obj Move Robot T 
Variables:   NoOfObjs posx posy s1 retx rety b t c 

---------------------------------------------------------------------

Model checking: Pmax=? [ F "MissionSuccessful" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=3,Objy1=0,Objx2=6,Objy2=2

Building model...
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=3,Objy1=0,Objx2=6,Objy2=2

Warning: Guard for command 7 of module "Move" is never satisfied.

Warning: Guard for command 8 of module "Move" is never satisfied.

Warning: Guard for command 9 of module "Move" is never satisfied.

Warning: Guard for command 7 of module "Robot" is never satisfied.

Warning: Update 2 of command 3 of module "Robot" doesn't do anything ("(s1'=13)", line 210, column 44)

Warning: Update 2 of command 27 of module "T" doesn't do anything ("(c'=1)", line 323, column 108)

Warning: Guard for command 28 of module "T" is never satisfied.

Warning: Update 2 of command 9 of module "T" doesn't do anything ("(c'=1)", line 300, column 112)

Warning: Guard for command 10 of module "T" is never satisfied.

Warning: Update 2 of command 51 of module "T" doesn't do anything ("(c'=1)", line 354, column 113)

Warning: Guard for command 52 of module "T" is never satisfied.

Computing reachable states...

Reachability (BFS): 89 iterations in 1.90 seconds (average 0.021393, setup 0.00)

Time for model construction: 3.938 seconds.

Warning: Deadlocks detected and fixed in 1 states

Type:        MDP
States:      347586 (1 initial)
Transitions: 940576
Choices:     462076

Transition matrix: 233232 nodes (93 terminal), 940576 minterms, vars: 34r/34c/18nd

Prob0A: 59 iterations in 0.59 seconds (average 0.009966, setup 0.00)

Prob1E: 4 iterations in 0.08 seconds (average 0.020000, setup 0.00)

yes = 1190, no = 295044, maybe = 51352

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=347586, nc=88699, nnz=254833, k=2] [3.3 MB]
Creating vector for yes... [2.7 MB]
Allocating iteration vectors... [2 x 2.7 MB]
TOTAL: [11.3 MB]

Starting iterations...

Iterative method: 103 iterations in 0.54 seconds (average 0.002447, setup 0.29)

Value in the initial state: 0.9795500199789822

Time for model checking: 1.24 seconds.

Result: 0.9795500199789822 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmin=? [ F "MissionSuccessful" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=3,Objy1=0,Objx2=6,Objy2=2

Prob0E: 61 iterations in 0.60 seconds (average 0.009770, setup 0.00)

Prob1A: 3 iterations in 0.06 seconds (average 0.020000, setup 0.00)

yes = 1190, no = 302219, maybe = 44177

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=347586, nc=76389, nnz=219575, k=2] [2.9 MB]
Creating vector for yes... [2.7 MB]
Allocating iteration vectors... [2 x 2.7 MB]
TOTAL: [10.9 MB]

Starting iterations...

Iterative method: 93 iterations in 0.41 seconds (average 0.002323, setup 0.20)

Value in the initial state: 0.7151147186153025

Time for model checking: 1.1 seconds.

Result: 0.7151147186153025 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmax=? [ F "MissionTimeUp" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=3,Objy1=0,Objx2=6,Objy2=2

Prob0A: 67 iterations in 1.02 seconds (average 0.015164, setup 0.00)

Prob1E: 4 iterations in 0.05 seconds (average 0.013000, setup 0.00)

yes = 540, no = 198287, maybe = 148759

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=347586, nc=250121, nnz=686835, k=4] [8.4 MB]
Creating vector for yes... [2.7 MB]
Allocating iteration vectors... [2 x 2.7 MB]
TOTAL: [16.4 MB]

Starting iterations...

Iterative method: 100 iterations in 0.70 seconds (average 0.003160, setup 0.39)

Value in the initial state: 8.436165224113606E-4

Time for model checking: 1.809 seconds.

Result: 8.436165224113606E-4 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmin=? [ F "MissionTimeUp" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=3,Objy1=0,Objx2=6,Objy2=2

Prob0E: 82 iterations in 1.00 seconds (average 0.012244, setup 0.00)

Prob1A: 3 iterations in 0.07 seconds (average 0.022667, setup 0.00)

yes = 540, no = 198667, maybe = 148379

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=347586, nc=249437, nnz=685543, k=4] [8.4 MB]
Creating vector for yes... [2.7 MB]
Allocating iteration vectors... [2 x 2.7 MB]
TOTAL: [16.4 MB]

Starting iterations...

Iterative method: 114 iterations in 0.76 seconds (average 0.003228, setup 0.39)

Value in the initial state: 3.0849804722035746E-6

Time for model checking: 1.843 seconds.

Result: 3.0849804722035746E-6 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmax=? [ F "Crash" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=3,Objy1=0,Objx2=6,Objy2=2

Prob0A: 3 iterations in 0.11 seconds (average 0.036000, setup 0.00)

Prob1E: 211 iterations in 4.06 seconds (average 0.019223, setup 0.00)

yes = 170077, no = 3389, maybe = 174120

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=347586, nc=288610, nnz=767110, k=4] [9.4 MB]
Creating vector for yes... [2.7 MB]
Allocating iteration vectors... [2 x 2.7 MB]
TOTAL: [17.3 MB]

Starting iterations...

Iterative method: 95 iterations in 0.79 seconds (average 0.003326, setup 0.47)

Value in the initial state: 0.03297831560569528

Time for model checking: 4.975 seconds.

Result: 0.03297831560569528 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmin=? [ F "Crash" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=3,Objy1=0,Objx2=6,Objy2=2

Prob0E: 3 iterations in 0.10 seconds (average 0.033333, setup 0.00)

Prob1A: 51 iterations in 0.77 seconds (average 0.015137, setup 0.00)

yes = 170077, no = 3389, maybe = 174120

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=347586, nc=288610, nnz=767110, k=4] [9.4 MB]
Creating vector for yes... [2.7 MB]
Allocating iteration vectors... [2 x 2.7 MB]
TOTAL: [17.3 MB]

Starting iterations...

Iterative method: 101 iterations in 0.75 seconds (average 0.003446, setup 0.40)

Value in the initial state: 0.01829111392008092

Time for model checking: 1.658 seconds.

Result: 0.01829111392008092 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmax=? [ F "AllAreaSearched" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=3,Objy1=0,Objx2=6,Objy2=2

Prob0A: 63 iterations in 0.56 seconds (average 0.008889, setup 0.00)

Prob1E: 4 iterations in 0.07 seconds (average 0.017000, setup 0.00)

yes = 1658, no = 273441, maybe = 72487

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=347586, nc=117779, nnz=325348, k=2] [4.2 MB]
Creating vector for yes... [2.7 MB]
Allocating iteration vectors... [2 x 2.7 MB]
TOTAL: [12.1 MB]

Starting iterations...

Iterative method: 92 iterations in 0.50 seconds (average 0.002739, setup 0.24)

Value in the initial state: 0.25474413014580227

Time for model checking: 1.15 seconds.

Result: 0.25474413014580227 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmin=? [ F "AllAreaSearched" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=3,Objy1=0,Objx2=6,Objy2=2

Prob0E: 67 iterations in 0.65 seconds (average 0.009672, setup 0.00)

Prob1A: 3 iterations in 0.06 seconds (average 0.021333, setup 0.00)

yes = 1658, no = 282850, maybe = 63078

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=347586, nc=102306, nnz=282485, k=2] [3.7 MB]
Creating vector for yes... [2.7 MB]
Allocating iteration vectors... [2 x 2.7 MB]
TOTAL: [11.6 MB]

Starting iterations...

Iterative method: 99 iterations in 0.43 seconds (average 0.002505, setup 0.18)

Value in the initial state: 0.0017419007741941107

Time for model checking: 1.179 seconds.

Result: 0.0017419007741941107 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmax=? [ F "MissionTimeUp"|"AllAreaSearched"|"Crash" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=3,Objy1=0,Objx2=6,Objy2=2

Prob0A: 3 iterations in 0.12 seconds (average 0.038667, setup 0.00)

Prob1E: 236 iterations in 4.69 seconds (average 0.019881, setup 0.00)

yes = 302218, no = 1191, maybe = 44177

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=347586, nc=76389, nnz=219575, k=2] [2.9 MB]
Creating vector for yes... [2.7 MB]
Allocating iteration vectors... [2 x 2.7 MB]
TOTAL: [10.9 MB]

Starting iterations...

Iterative method: 86 iterations in 0.47 seconds (average 0.002419, setup 0.26)

Value in the initial state: 0.28488484467013936

Time for model checking: 5.326 seconds.

Result: 0.28488484467013936 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmin=? [ F "MissionTimeUp"|"AllAreaSearched"|"Crash" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=3,Objy1=0,Objx2=6,Objy2=2

Prob0E: 3 iterations in 0.07 seconds (average 0.022667, setup 0.00)

Prob1A: 59 iterations in 0.71 seconds (average 0.012000, setup 0.00)

yes = 295043, no = 1191, maybe = 51352

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=347586, nc=88699, nnz=254833, k=2] [3.3 MB]
Creating vector for yes... [2.7 MB]
Allocating iteration vectors... [2 x 2.7 MB]
TOTAL: [11.3 MB]

Starting iterations...

Iterative method: 99 iterations in 0.43 seconds (average 0.002424, setup 0.19)

Value in the initial state: 0.020449714937460763

Time for model checking: 1.219 seconds.

Result: 0.020449714937460763 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"drop"}max=? [ F "deadlock" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=3,Objy1=0,Objx2=6,Objy2=2

Prob0E: 4 iterations in 0.05 seconds (average 0.013000, setup 0.00)

Prob1A: 1 iterations in 0.01 seconds (average 0.008000, setup 0.00)

goal = 1, inf = 0, maybe = 347585

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=347586, nc=462075, nnz=940575, k=4] [13.9 MB]
Building sparse matrix (transition rewards)... [n=347586, nc=462075, nnz=0, k=4] [3.1 MB]
Creating vector for state rewards... [2.7 MB]
Creating vector for inf... [2.7 MB]
Allocating iteration vectors... [2 x 2.7 MB]
TOTAL: [27.5 MB]

Starting iterations...

Iterative method: 98 iterations in 1.06 seconds (average 0.005102, setup 0.56)

Value in the initial state: 0.2834031608491247

Time for model checking: 1.135 seconds.

Result: 0.2834031608491247 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"drop"}min=? [ F "deadlock" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=3,Objy1=0,Objx2=6,Objy2=2

Prob0A: 4 iterations in 0.05 seconds (average 0.012000, setup 0.00)

Prob1E: 5 iterations in 0.07 seconds (average 0.013600, setup 0.00)

Warning: PRISM hasn't checked for zero-reward loops.
Your minimum rewards may be too low...

goal = 1, inf = 0, maybe = 347585

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=347586, nc=462075, nnz=940575, k=4] [13.9 MB]
Building sparse matrix (transition rewards)... [n=347586, nc=462075, nnz=0, k=4] [3.1 MB]
Creating vector for state rewards... [2.7 MB]
Creating vector for inf... [2.7 MB]
Allocating iteration vectors... [2 x 2.7 MB]
TOTAL: [27.5 MB]

Starting iterations...

Iterative method: 87 iterations in 0.90 seconds (average 0.005103, setup 0.46)

Value in the initial state: 0.2320279205984377

Time for model checking: 1.038 seconds.

Result: 0.2320279205984377 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"charge"}max=? [ F "deadlock" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=3,Objy1=0,Objx2=6,Objy2=2

Prob0E: 4 iterations in 0.04 seconds (average 0.009000, setup 0.00)

Prob1A: 1 iterations in 0.01 seconds (average 0.008000, setup 0.00)

goal = 1, inf = 0, maybe = 347585

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=347586, nc=462075, nnz=940575, k=4] [13.9 MB]
Building sparse matrix (transition rewards)... [n=347586, nc=462075, nnz=11040, k=4] [3.2 MB]
Creating vector for state rewards... [2.7 MB]
Creating vector for inf... [2.7 MB]
Allocating iteration vectors... [2 x 2.7 MB]
TOTAL: [27.7 MB]

Starting iterations...

Iterative method: 94 iterations in 0.94 seconds (average 0.005021, setup 0.47)

Value in the initial state: 0.3288404750714439

Time for model checking: 1.01 seconds.

Result: 0.3288404750714439 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"charge"}min=? [ F "deadlock" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=3,Objy1=0,Objx2=6,Objy2=2

Prob0A: 4 iterations in 0.04 seconds (average 0.009000, setup 0.00)

Prob1E: 5 iterations in 0.06 seconds (average 0.012000, setup 0.00)

Warning: PRISM hasn't checked for zero-reward loops.
Your minimum rewards may be too low...

goal = 1, inf = 0, maybe = 347585

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=347586, nc=462075, nnz=940575, k=4] [13.9 MB]
Building sparse matrix (transition rewards)... [n=347586, nc=462075, nnz=11040, k=4] [3.2 MB]
Creating vector for state rewards... [2.7 MB]
Creating vector for inf... [2.7 MB]
Allocating iteration vectors... [2 x 2.7 MB]
TOTAL: [27.7 MB]

Starting iterations...

Iterative method: 103 iterations in 0.96 seconds (average 0.005010, setup 0.45)

Value in the initial state: 0.07401671596303408

Time for model checking: 1.094 seconds.

Result: 0.07401671596303408 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"time"}max=? [ F "deadlock" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=3,Objy1=0,Objx2=6,Objy2=2

Prob0E: 4 iterations in 0.03 seconds (average 0.008000, setup 0.00)

Prob1A: 1 iterations in 0.01 seconds (average 0.012000, setup 0.00)

goal = 1, inf = 0, maybe = 347585

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=347586, nc=462075, nnz=940575, k=4] [13.9 MB]
Building sparse matrix (transition rewards)... [n=347586, nc=462075, nnz=173464, k=4] [5.1 MB]
Creating vector for state rewards... [2.7 MB]
Creating vector for inf... [2.7 MB]
Allocating iteration vectors... [2 x 2.7 MB]
TOTAL: [29.5 MB]

Starting iterations...

Iterative method: 98 iterations in 1.01 seconds (average 0.005102, setup 0.51)

Value in the initial state: 181.85224830394276

Time for model checking: 1.072 seconds.

Result: 181.85224830394276 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"time"}min=? [ F "deadlock" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=3,Objy1=0,Objx2=6,Objy2=2

Prob0A: 4 iterations in 0.04 seconds (average 0.010000, setup 0.00)

Prob1E: 5 iterations in 0.06 seconds (average 0.012800, setup 0.00)

Warning: PRISM hasn't checked for zero-reward loops.
Your minimum rewards may be too low...

goal = 1, inf = 0, maybe = 347585

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=347586, nc=462075, nnz=940575, k=4] [13.9 MB]
Building sparse matrix (transition rewards)... [n=347586, nc=462075, nnz=173464, k=4] [5.1 MB]
Creating vector for state rewards... [2.7 MB]
Creating vector for inf... [2.7 MB]
Allocating iteration vectors... [2 x 2.7 MB]
TOTAL: [29.5 MB]

Starting iterations...

Iterative method: 107 iterations in 1.06 seconds (average 0.005196, setup 0.50)

Value in the initial state: 101.31614797041027

Time for model checking: 1.181 seconds.

Result: 101.31614797041027 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"success"}max=? [ F "deadlock" ]/Pmin=? [ F "MissionSuccessful" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=3,Objy1=0,Objx2=6,Objy2=2

Prob0E: 4 iterations in 0.04 seconds (average 0.009000, setup 0.00)

Prob1A: 1 iterations in 0.01 seconds (average 0.008000, setup 0.00)

goal = 1, inf = 0, maybe = 347585

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=347586, nc=462075, nnz=940575, k=4] [13.9 MB]
Building sparse matrix (transition rewards)... [n=347586, nc=462075, nnz=2344, k=4] [3.1 MB]
Creating vector for state rewards... [2.7 MB]
Creating vector for inf... [2.7 MB]
Allocating iteration vectors... [2 x 2.7 MB]
TOTAL: [27.6 MB]

Starting iterations...

Iterative method: 99 iterations in 0.92 seconds (average 0.004889, setup 0.44)

Prob0E: 61 iterations in 0.32 seconds (average 0.005246, setup 0.00)

Prob1A: 3 iterations in 0.04 seconds (average 0.013333, setup 0.00)

yes = 1190, no = 302219, maybe = 44177

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=347586, nc=76389, nnz=219575, k=2] [2.9 MB]
Creating vector for yes... [2.7 MB]
Allocating iteration vectors... [2 x 2.7 MB]
TOTAL: [10.9 MB]

Starting iterations...

Iterative method: 93 iterations in 0.31 seconds (average 0.002022, setup 0.12)

Value in the initial state: 248.98239339391327

Time for model checking: 1.687 seconds.

Result: 248.98239339391327 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"success"}min=? [ F "deadlock" ]/Pmax=? [ F "MissionSuccessful" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=3,Objy1=0,Objx2=6,Objy2=2

Prob0A: 4 iterations in 0.03 seconds (average 0.007000, setup 0.00)

Prob1E: 5 iterations in 0.04 seconds (average 0.008000, setup 0.00)

Warning: PRISM hasn't checked for zero-reward loops.
Your minimum rewards may be too low...

goal = 1, inf = 0, maybe = 347585

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=347586, nc=462075, nnz=940575, k=4] [13.9 MB]
Building sparse matrix (transition rewards)... [n=347586, nc=462075, nnz=2344, k=4] [3.1 MB]
Creating vector for state rewards... [2.7 MB]
Creating vector for inf... [2.7 MB]
Allocating iteration vectors... [2 x 2.7 MB]
TOTAL: [27.6 MB]

Starting iterations...

Iterative method: 99 iterations in 0.86 seconds (average 0.004606, setup 0.41)

Prob0A: 59 iterations in 0.28 seconds (average 0.004814, setup 0.00)

Prob1E: 4 iterations in 0.03 seconds (average 0.008000, setup 0.00)

yes = 1190, no = 295044, maybe = 51352

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=347586, nc=88699, nnz=254833, k=2] [3.3 MB]
Creating vector for yes... [2.7 MB]
Allocating iteration vectors... [2 x 2.7 MB]
TOTAL: [11.3 MB]

Starting iterations...

Iterative method: 103 iterations in 0.34 seconds (average 0.002058, setup 0.13)

Value in the initial state: 78.63940932497121

Time for model checking: 1.63 seconds.

Result: 78.63940932497121 (value in the initial state)

---------------------------------------------------------------------

Note: There were 16 warnings during computation.

