PRISM
=====

Version: 4.3
Date: Wed Aug 03 16:09:17 BST 2016
Hostname: eglinton
Memory limits: cudd=1g, java(heap)=981.5m
Command line: prism uav_sim_mdp.prism -s uav_mdp.props -const 'Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=1,Objy1=1,Objx2=2,Objy2=0,'

Parsing model file "uav_sim_mdp.prism"...

Parsing properties file "uav_mdp.props"...

18 properties:
(1) Pmax=? [ F "MissionSuccessful" ]
(2) Pmin=? [ F "MissionSuccessful" ]
(3) Pmax=? [ F "MissionTimeUp" ]
(4) Pmin=? [ F "MissionTimeUp" ]
(5) Pmax=? [ F "Crash" ]
(6) Pmin=? [ F "Crash" ]
(7) Pmax=? [ F "AllAreaSearched" ]
(8) Pmin=? [ F "AllAreaSearched" ]
(9) Pmax=? [ F "MissionTimeUp"|"AllAreaSearched"|"Crash" ]
(10) Pmin=? [ F "MissionTimeUp"|"AllAreaSearched"|"Crash" ]
(11) R{"drop"}max=? [ F "deadlock" ]
(12) R{"drop"}min=? [ F "deadlock" ]
(13) R{"charge"}max=? [ F "deadlock" ]
(14) R{"charge"}min=? [ F "deadlock" ]
(15) R{"time"}max=? [ F "deadlock" ]
(16) R{"time"}min=? [ F "deadlock" ]
(17) R{"success"}max=? [ F "deadlock" ]/Pmin=? [ F "MissionSuccessful" ]
(18) R{"success"}min=? [ F "deadlock" ]/Pmax=? [ F "MissionSuccessful" ]

Type:        MDP
Modules:     Obj Move Robot T 
Variables:   NoOfObjs posx posy s1 retx rety b t c 

---------------------------------------------------------------------

Model checking: Pmax=? [ F "MissionSuccessful" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=1,Objy1=1,Objx2=2,Objy2=0

Building model...
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=1,Objy1=1,Objx2=2,Objy2=0

Warning: Guard for command 7 of module "Move" is never satisfied.

Warning: Guard for command 8 of module "Move" is never satisfied.

Warning: Guard for command 9 of module "Move" is never satisfied.

Warning: Guard for command 7 of module "Robot" is never satisfied.

Warning: Update 2 of command 3 of module "Robot" doesn't do anything ("(s1'=13)", line 210, column 44)

Warning: Update 2 of command 27 of module "T" doesn't do anything ("(c'=1)", line 323, column 108)

Warning: Guard for command 28 of module "T" is never satisfied.

Warning: Update 2 of command 9 of module "T" doesn't do anything ("(c'=1)", line 300, column 112)

Warning: Guard for command 10 of module "T" is never satisfied.

Warning: Update 2 of command 51 of module "T" doesn't do anything ("(c'=1)", line 354, column 113)

Warning: Guard for command 52 of module "T" is never satisfied.

Computing reachable states...

Reachability (BFS): 90 iterations in 1.62 seconds (average 0.018044, setup 0.00)

Time for model construction: 4.014 seconds.

Warning: Deadlocks detected and fixed in 1 states

Type:        MDP
States:      285169 (1 initial)
Transitions: 735155
Choices:     372009

Transition matrix: 195468 nodes (89 terminal), 735155 minterms, vars: 34r/34c/18nd

Prob0A: 50 iterations in 0.38 seconds (average 0.007600, setup 0.00)

Prob1E: 4 iterations in 0.06 seconds (average 0.014000, setup 0.00)

yes = 1142, no = 240994, maybe = 43033

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=285169, nc=74968, nnz=211949, k=2] [2.8 MB]
Creating vector for yes... [2.2 MB]
Allocating iteration vectors... [2 x 2.2 MB]
TOTAL: [9.3 MB]

Starting iterations...

Iterative method: 89 iterations in 0.44 seconds (average 0.002022, setup 0.26)

Value in the initial state: 0.9839494116397528

Time for model checking: 1.108 seconds.

Result: 0.9839494116397528 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmin=? [ F "MissionSuccessful" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=1,Objy1=1,Objx2=2,Objy2=0

Prob0E: 52 iterations in 0.45 seconds (average 0.008692, setup 0.00)

Prob1A: 3 iterations in 0.06 seconds (average 0.020000, setup 0.00)

yes = 1142, no = 247985, maybe = 36042

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=285169, nc=62547, nnz=176085, k=2] [2.3 MB]
Creating vector for yes... [2.2 MB]
Allocating iteration vectors... [2 x 2.2 MB]
TOTAL: [8.9 MB]

Starting iterations...

Iterative method: 85 iterations in 0.38 seconds (average 0.001929, setup 0.22)

Value in the initial state: 0.8276361844028965

Time for model checking: 0.953 seconds.

Result: 0.8276361844028965 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmax=? [ F "MissionTimeUp" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=1,Objy1=1,Objx2=2,Objy2=0

Prob0A: 67 iterations in 0.74 seconds (average 0.011045, setup 0.00)

Prob1E: 4 iterations in 0.06 seconds (average 0.016000, setup 0.00)

yes = 476, no = 176292, maybe = 108401

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=285169, nc=183108, nnz=497763, k=4] [6.1 MB]
Creating vector for yes... [2.2 MB]
Allocating iteration vectors... [2 x 2.2 MB]
TOTAL: [12.7 MB]

Starting iterations...

Iterative method: 96 iterations in 0.58 seconds (average 0.002500, setup 0.34)

Value in the initial state: 1.259497268971511E-4

Time for model checking: 1.54 seconds.

Result: 1.259497268971511E-4 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmin=? [ F "MissionTimeUp" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=1,Objy1=1,Objx2=2,Objy2=0

Prob0E: 82 iterations in 0.91 seconds (average 0.011073, setup 0.00)

Prob1A: 3 iterations in 0.05 seconds (average 0.016000, setup 0.00)

yes = 476, no = 176695, maybe = 107998

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=285169, nc=182392, nnz=496248, k=4] [6.1 MB]
Creating vector for yes... [2.2 MB]
Allocating iteration vectors... [2 x 2.2 MB]
TOTAL: [12.7 MB]

Starting iterations...

Iterative method: 113 iterations in 0.59 seconds (average 0.002513, setup 0.30)

Value in the initial state: 1.0711224005398012E-7

Time for model checking: 1.783 seconds.

Result: 1.0711224005398012E-7 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmax=? [ F "Crash" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=1,Objy1=1,Objx2=2,Objy2=0

Prob0A: 3 iterations in 0.08 seconds (average 0.028000, setup 0.00)

Prob1E: 194 iterations in 2.94 seconds (average 0.015134, setup 0.00)

yes = 139869, no = 3151, maybe = 142149

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=285169, nc=228989, nnz=592135, k=4] [7.3 MB]
Creating vector for yes... [2.2 MB]
Allocating iteration vectors... [2 x 2.2 MB]
TOTAL: [13.8 MB]

Starting iterations...

Iterative method: 90 iterations in 0.63 seconds (average 0.002756, setup 0.38)

Value in the initial state: 0.024647770885498778

Time for model checking: 4.341 seconds.

Result: 0.024647770885498778 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmin=? [ F "Crash" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=1,Objy1=1,Objx2=2,Objy2=0

Prob0E: 3 iterations in 0.07 seconds (average 0.024000, setup 0.00)

Prob1A: 50 iterations in 0.58 seconds (average 0.011520, setup 0.00)

yes = 139869, no = 3151, maybe = 142149

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=285169, nc=228989, nnz=592135, k=4] [7.3 MB]
Creating vector for yes... [2.2 MB]
Allocating iteration vectors... [2 x 2.2 MB]
TOTAL: [13.8 MB]

Starting iterations...

Iterative method: 81 iterations in 0.52 seconds (average 0.002815, setup 0.30)

Value in the initial state: 0.014010929882590197

Time for model checking: 1.346 seconds.

Result: 0.014010929882590197 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmax=? [ F "AllAreaSearched" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=1,Objy1=1,Objx2=2,Objy2=0

Prob0A: 62 iterations in 0.56 seconds (average 0.008968, setup 0.00)

Prob1E: 4 iterations in 0.06 seconds (average 0.014000, setup 0.00)

yes = 1532, no = 222385, maybe = 61252

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=285169, nc=91503, nnz=234976, k=2] [3.0 MB]
Creating vector for yes... [2.2 MB]
Allocating iteration vectors... [2 x 2.2 MB]
TOTAL: [9.6 MB]

Starting iterations...

Iterative method: 89 iterations in 0.41 seconds (average 0.002247, setup 0.21)

Value in the initial state: 0.14899314400366334

Time for model checking: 1.071 seconds.

Result: 0.14899314400366334 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmin=? [ F "AllAreaSearched" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=1,Objy1=1,Objx2=2,Objy2=0

Prob0E: 65 iterations in 0.59 seconds (average 0.009046, setup 0.00)

Prob1A: 3 iterations in 0.06 seconds (average 0.018667, setup 0.00)

yes = 1532, no = 229829, maybe = 53808

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=285169, nc=79283, nnz=201518, k=2] [2.7 MB]
Creating vector for yes... [2.2 MB]
Allocating iteration vectors... [2 x 2.2 MB]
TOTAL: [9.2 MB]

Starting iterations...

Iterative method: 95 iterations in 0.37 seconds (average 0.002063, setup 0.17)

Value in the initial state: 0.0018324600437960006

Time for model checking: 1.022 seconds.

Result: 0.0018324600437960006 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmax=? [ F "MissionTimeUp"|"AllAreaSearched"|"Crash" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=1,Objy1=1,Objx2=2,Objy2=0

Prob0A: 3 iterations in 0.08 seconds (average 0.026667, setup 0.00)

Prob1E: 200 iterations in 2.80 seconds (average 0.014020, setup 0.00)

yes = 247984, no = 1143, maybe = 36042

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=285169, nc=62547, nnz=176085, k=2] [2.3 MB]
Creating vector for yes... [2.2 MB]
Allocating iteration vectors... [2 x 2.2 MB]
TOTAL: [8.9 MB]

Starting iterations...

Iterative method: 78 iterations in 0.38 seconds (average 0.001949, setup 0.22)

Value in the initial state: 0.1723636163930873

Time for model checking: 3.29 seconds.

Result: 0.1723636163930873 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmin=? [ F "MissionTimeUp"|"AllAreaSearched"|"Crash" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=1,Objy1=1,Objx2=2,Objy2=0

Prob0E: 3 iterations in 0.08 seconds (average 0.025333, setup 0.00)

Prob1A: 50 iterations in 0.52 seconds (average 0.010480, setup 0.00)

yes = 240993, no = 1143, maybe = 43033

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=285169, nc=74968, nnz=211949, k=2] [2.8 MB]
Creating vector for yes... [2.2 MB]
Allocating iteration vectors... [2 x 2.2 MB]
TOTAL: [9.3 MB]

Starting iterations...

Iterative method: 89 iterations in 0.38 seconds (average 0.002022, setup 0.20)

Value in the initial state: 0.016048571254403506

Time for model checking: 1.02 seconds.

Result: 0.016048571254403506 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"drop"}max=? [ F "deadlock" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=1,Objy1=1,Objx2=2,Objy2=0

Prob0E: 4 iterations in 0.07 seconds (average 0.018000, setup 0.00)

Prob1A: 1 iterations in 0.01 seconds (average 0.008000, setup 0.00)

goal = 1, inf = 0, maybe = 285168

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=285169, nc=372008, nnz=735154, k=4] [10.9 MB]
Building sparse matrix (transition rewards)... [n=285169, nc=372008, nnz=0, k=4] [2.5 MB]
Creating vector for state rewards... [2.2 MB]
Creating vector for inf... [2.2 MB]
Allocating iteration vectors... [2 x 2.2 MB]
TOTAL: [22.1 MB]

Starting iterations...

Iterative method: 85 iterations in 0.84 seconds (average 0.004141, setup 0.49)

Value in the initial state: 0.20471392367388666

Time for model checking: 0.945 seconds.

Result: 0.20471392367388666 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"drop"}min=? [ F "deadlock" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=1,Objy1=1,Objx2=2,Objy2=0

Prob0A: 4 iterations in 0.04 seconds (average 0.011000, setup 0.00)

Prob1E: 5 iterations in 0.06 seconds (average 0.011200, setup 0.00)

Warning: PRISM hasn't checked for zero-reward loops.
Your minimum rewards may be too low...

goal = 1, inf = 0, maybe = 285168

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=285169, nc=372008, nnz=735154, k=4] [10.9 MB]
Building sparse matrix (transition rewards)... [n=285169, nc=372008, nnz=0, k=4] [2.5 MB]
Creating vector for state rewards... [2.2 MB]
Creating vector for inf... [2.2 MB]
Allocating iteration vectors... [2 x 2.2 MB]
TOTAL: [22.1 MB]

Starting iterations...

Iterative method: 78 iterations in 0.71 seconds (average 0.004154, setup 0.39)

Value in the initial state: 0.1899248443701462

Time for model checking: 0.822 seconds.

Result: 0.1899248443701462 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"charge"}max=? [ F "deadlock" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=1,Objy1=1,Objx2=2,Objy2=0

Prob0E: 4 iterations in 0.04 seconds (average 0.009000, setup 0.00)

Prob1A: 1 iterations in 0.00 seconds (average 0.004000, setup 0.00)

goal = 1, inf = 0, maybe = 285168

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=285169, nc=372008, nnz=735154, k=4] [10.9 MB]
Building sparse matrix (transition rewards)... [n=285169, nc=372008, nnz=7522, k=4] [2.6 MB]
Creating vector for state rewards... [2.2 MB]
Creating vector for inf... [2.2 MB]
Allocating iteration vectors... [2 x 2.2 MB]
TOTAL: [22.2 MB]

Starting iterations...

Iterative method: 87 iterations in 0.75 seconds (average 0.004138, setup 0.39)

Value in the initial state: 0.19480901461816486

Time for model checking: 0.805 seconds.

Result: 0.19480901461816486 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"charge"}min=? [ F "deadlock" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=1,Objy1=1,Objx2=2,Objy2=0

Prob0A: 4 iterations in 0.03 seconds (average 0.008000, setup 0.00)

Prob1E: 5 iterations in 0.06 seconds (average 0.012000, setup 0.00)

Warning: PRISM hasn't checked for zero-reward loops.
Your minimum rewards may be too low...

goal = 1, inf = 0, maybe = 285168

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=285169, nc=372008, nnz=735154, k=4] [10.9 MB]
Building sparse matrix (transition rewards)... [n=285169, nc=372008, nnz=7522, k=4] [2.6 MB]
Creating vector for state rewards... [2.2 MB]
Creating vector for inf... [2.2 MB]
Allocating iteration vectors... [2 x 2.2 MB]
TOTAL: [22.2 MB]

Starting iterations...

Iterative method: 95 iterations in 0.78 seconds (average 0.004211, setup 0.38)

Value in the initial state: 0.013484974703452016

Time for model checking: 0.888 seconds.

Result: 0.013484974703452016 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"time"}max=? [ F "deadlock" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=1,Objy1=1,Objx2=2,Objy2=0

Prob0E: 4 iterations in 0.03 seconds (average 0.008000, setup 0.00)

Prob1A: 1 iterations in 0.01 seconds (average 0.008000, setup 0.00)

goal = 1, inf = 0, maybe = 285168

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=285169, nc=372008, nnz=735154, k=4] [10.9 MB]
Building sparse matrix (transition rewards)... [n=285169, nc=372008, nnz=143018, k=4] [4.1 MB]
Creating vector for state rewards... [2.2 MB]
Creating vector for inf... [2.2 MB]
Allocating iteration vectors... [2 x 2.2 MB]
TOTAL: [23.8 MB]

Starting iterations...

Iterative method: 93 iterations in 0.82 seconds (average 0.004215, setup 0.43)

Value in the initial state: 136.2125270974533

Time for model checking: 0.881 seconds.

Result: 136.2125270974533 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"time"}min=? [ F "deadlock" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=1,Objy1=1,Objx2=2,Objy2=0

Prob0A: 4 iterations in 0.03 seconds (average 0.008000, setup 0.00)

Prob1E: 5 iterations in 0.06 seconds (average 0.011200, setup 0.00)

Warning: PRISM hasn't checked for zero-reward loops.
Your minimum rewards may be too low...

goal = 1, inf = 0, maybe = 285168

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=285169, nc=372008, nnz=735154, k=4] [10.9 MB]
Building sparse matrix (transition rewards)... [n=285169, nc=372008, nnz=143018, k=4] [4.1 MB]
Creating vector for state rewards... [2.2 MB]
Creating vector for inf... [2.2 MB]
Allocating iteration vectors... [2 x 2.2 MB]
TOTAL: [23.8 MB]

Starting iterations...

Iterative method: 95 iterations in 0.83 seconds (average 0.004295, setup 0.42)

Value in the initial state: 77.75810370868672

Time for model checking: 0.93 seconds.

Result: 77.75810370868672 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"success"}max=? [ F "deadlock" ]/Pmin=? [ F "MissionSuccessful" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=1,Objy1=1,Objx2=2,Objy2=0

Prob0E: 4 iterations in 0.03 seconds (average 0.007000, setup 0.00)

Prob1A: 1 iterations in 0.01 seconds (average 0.008000, setup 0.00)

goal = 1, inf = 0, maybe = 285168

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=285169, nc=372008, nnz=735154, k=4] [10.9 MB]
Building sparse matrix (transition rewards)... [n=285169, nc=372008, nnz=2252, k=4] [2.5 MB]
Creating vector for state rewards... [2.2 MB]
Creating vector for inf... [2.2 MB]
Allocating iteration vectors... [2 x 2.2 MB]
TOTAL: [22.2 MB]

Starting iterations...

Iterative method: 93 iterations in 0.76 seconds (average 0.004086, setup 0.38)

Prob0E: 52 iterations in 0.37 seconds (average 0.007154, setup 0.00)

Prob1A: 3 iterations in 0.04 seconds (average 0.014667, setup 0.00)

yes = 1142, no = 247985, maybe = 36042

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=285169, nc=62547, nnz=176085, k=2] [2.3 MB]
Creating vector for yes... [2.2 MB]
Allocating iteration vectors... [2 x 2.2 MB]
TOTAL: [8.9 MB]

Starting iterations...

Iterative method: 85 iterations in 0.32 seconds (average 0.001882, setup 0.16)

Value in the initial state: 161.6659279282015

Time for model checking: 1.574 seconds.

Result: 161.6659279282015 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"success"}min=? [ F "deadlock" ]/Pmax=? [ F "MissionSuccessful" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=1,Objy1=1,Objx2=2,Objy2=0

Prob0A: 4 iterations in 0.08 seconds (average 0.021000, setup 0.00)

Prob1E: 5 iterations in 0.05 seconds (average 0.009600, setup 0.00)

Warning: PRISM hasn't checked for zero-reward loops.
Your minimum rewards may be too low...

goal = 1, inf = 0, maybe = 285168

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=285169, nc=372008, nnz=735154, k=4] [10.9 MB]
Building sparse matrix (transition rewards)... [n=285169, nc=372008, nnz=2252, k=4] [2.5 MB]
Creating vector for state rewards... [2.2 MB]
Creating vector for inf... [2.2 MB]
Allocating iteration vectors... [2 x 2.2 MB]
TOTAL: [22.2 MB]

Starting iterations...

Iterative method: 91 iterations in 0.78 seconds (average 0.004088, setup 0.40)

Prob0A: 50 iterations in 0.38 seconds (average 0.007600, setup 0.00)

Prob1E: 4 iterations in 0.04 seconds (average 0.011000, setup 0.00)

yes = 1142, no = 240994, maybe = 43033

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=285169, nc=74968, nnz=211949, k=2] [2.8 MB]
Creating vector for yes... [2.2 MB]
Allocating iteration vectors... [2 x 2.2 MB]
TOTAL: [9.3 MB]

Starting iterations...

Iterative method: 89 iterations in 0.34 seconds (average 0.001843, setup 0.18)

Value in the initial state: 73.63279882241022

Time for model checking: 1.691 seconds.

Result: 73.63279882241022 (value in the initial state)

---------------------------------------------------------------------

Note: There were 16 warnings during computation.

