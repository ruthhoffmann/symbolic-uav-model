PRISM
=====

Version: 4.3
Date: Wed Aug 03 16:00:57 BST 2016
Hostname: eglinton
Memory limits: cudd=1g, java(heap)=981.5m
Command line: prism uav_sim_mdp.prism -s uav_mdp.props -const 'Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=4,Objy1=0,Objx2=4,Objy2=3,'

Parsing model file "uav_sim_mdp.prism"...

Parsing properties file "uav_mdp.props"...

18 properties:
(1) Pmax=? [ F "MissionSuccessful" ]
(2) Pmin=? [ F "MissionSuccessful" ]
(3) Pmax=? [ F "MissionTimeUp" ]
(4) Pmin=? [ F "MissionTimeUp" ]
(5) Pmax=? [ F "Crash" ]
(6) Pmin=? [ F "Crash" ]
(7) Pmax=? [ F "AllAreaSearched" ]
(8) Pmin=? [ F "AllAreaSearched" ]
(9) Pmax=? [ F "MissionTimeUp"|"AllAreaSearched"|"Crash" ]
(10) Pmin=? [ F "MissionTimeUp"|"AllAreaSearched"|"Crash" ]
(11) R{"drop"}max=? [ F "deadlock" ]
(12) R{"drop"}min=? [ F "deadlock" ]
(13) R{"charge"}max=? [ F "deadlock" ]
(14) R{"charge"}min=? [ F "deadlock" ]
(15) R{"time"}max=? [ F "deadlock" ]
(16) R{"time"}min=? [ F "deadlock" ]
(17) R{"success"}max=? [ F "deadlock" ]/Pmin=? [ F "MissionSuccessful" ]
(18) R{"success"}min=? [ F "deadlock" ]/Pmax=? [ F "MissionSuccessful" ]

Type:        MDP
Modules:     Obj Move Robot T 
Variables:   NoOfObjs posx posy s1 retx rety b t c 

---------------------------------------------------------------------

Model checking: Pmax=? [ F "MissionSuccessful" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=4,Objy1=0,Objx2=4,Objy2=3

Building model...
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=4,Objy1=0,Objx2=4,Objy2=3

Warning: Guard for command 7 of module "Move" is never satisfied.

Warning: Guard for command 8 of module "Move" is never satisfied.

Warning: Guard for command 9 of module "Move" is never satisfied.

Warning: Guard for command 7 of module "Robot" is never satisfied.

Warning: Update 2 of command 3 of module "Robot" doesn't do anything ("(s1'=13)", line 210, column 44)

Warning: Update 2 of command 27 of module "T" doesn't do anything ("(c'=1)", line 323, column 108)

Warning: Guard for command 28 of module "T" is never satisfied.

Warning: Update 2 of command 9 of module "T" doesn't do anything ("(c'=1)", line 300, column 112)

Warning: Guard for command 10 of module "T" is never satisfied.

Warning: Update 2 of command 51 of module "T" doesn't do anything ("(c'=1)", line 354, column 113)

Warning: Guard for command 52 of module "T" is never satisfied.

Computing reachable states...

Reachability (BFS): 88 iterations in 1.43 seconds (average 0.016273, setup 0.00)

Time for model construction: 3.384 seconds.

Warning: Deadlocks detected and fixed in 1 states

Type:        MDP
States:      264565 (1 initial)
Transitions: 691096
Choices:     348154

Transition matrix: 223038 nodes (93 terminal), 691096 minterms, vars: 34r/34c/18nd

Prob0A: 59 iterations in 0.70 seconds (average 0.011864, setup 0.00)

Prob1E: 4 iterations in 0.06 seconds (average 0.015000, setup 0.00)

yes = 1134, no = 224870, maybe = 38561

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=264565, nc=65442, nnz=181864, k=2] [2.4 MB]
Creating vector for yes... [2.0 MB]
Allocating iteration vectors... [2 x 2.0 MB]
TOTAL: [8.5 MB]

Starting iterations...

Iterative method: 99 iterations in 0.44 seconds (average 0.001818, setup 0.26)

Value in the initial state: 0.980180494220358

Time for model checking: 1.225 seconds.

Result: 0.980180494220358 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmin=? [ F "MissionSuccessful" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=4,Objy1=0,Objx2=4,Objy2=3

Prob0E: 61 iterations in 0.47 seconds (average 0.007738, setup 0.00)

Prob1A: 3 iterations in 0.07 seconds (average 0.022667, setup 0.00)

yes = 1134, no = 230140, maybe = 33291

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=264565, nc=56851, nnz=157767, k=2] [2.1 MB]
Creating vector for yes... [2.0 MB]
Allocating iteration vectors... [2 x 2.0 MB]
TOTAL: [8.2 MB]

Starting iterations...

Iterative method: 94 iterations in 0.34 seconds (average 0.001787, setup 0.18)

Value in the initial state: 0.7468836179794404

Time for model checking: 0.912 seconds.

Result: 0.7468836179794404 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmax=? [ F "MissionTimeUp" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=4,Objy1=0,Objx2=4,Objy2=3

Prob0A: 69 iterations in 0.95 seconds (average 0.013797, setup 0.00)

Prob1E: 4 iterations in 0.05 seconds (average 0.012000, setup 0.00)

yes = 464, no = 153209, maybe = 110892

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=264565, nc=181590, nnz=487039, k=4] [6.0 MB]
Creating vector for yes... [2.0 MB]
Allocating iteration vectors... [2 x 2.0 MB]
TOTAL: [12.1 MB]

Starting iterations...

Iterative method: 101 iterations in 0.57 seconds (average 0.002257, setup 0.34)

Value in the initial state: 2.693324009783472E-4

Time for model checking: 1.586 seconds.

Result: 2.693324009783472E-4 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmin=? [ F "MissionTimeUp" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=4,Objy1=0,Objx2=4,Objy2=3

Prob0E: 82 iterations in 1.18 seconds (average 0.014341, setup 0.00)

Prob1A: 3 iterations in 0.05 seconds (average 0.016000, setup 0.00)

yes = 464, no = 153499, maybe = 110602

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=264565, nc=181081, nnz=485967, k=4] [6.0 MB]
Creating vector for yes... [2.0 MB]
Allocating iteration vectors... [2 x 2.0 MB]
TOTAL: [12.0 MB]

Starting iterations...

Iterative method: 115 iterations in 0.66 seconds (average 0.003026, setup 0.31)

Value in the initial state: 7.598174670724646E-7

Time for model checking: 1.928 seconds.

Result: 7.598174670724646E-7 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmax=? [ F "Crash" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=4,Objy1=0,Objx2=4,Objy2=3

Prob0A: 3 iterations in 0.14 seconds (average 0.048000, setup 0.00)

Prob1E: 208 iterations in 3.97 seconds (average 0.019096, setup 0.00)

yes = 129613, no = 3239, maybe = 131713

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=264565, nc=215302, nnz=558244, k=4] [6.8 MB]
Creating vector for yes... [2.0 MB]
Allocating iteration vectors... [2 x 2.0 MB]
TOTAL: [12.9 MB]

Starting iterations...

Iterative method: 95 iterations in 0.59 seconds (average 0.002484, setup 0.35)

Value in the initial state: 0.031147595054996886

Time for model checking: 4.755 seconds.

Result: 0.031147595054996886 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmin=? [ F "Crash" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=4,Objy1=0,Objx2=4,Objy2=3

Prob0E: 4 iterations in 0.09 seconds (average 0.023000, setup 0.00)

Prob1A: 48 iterations in 0.77 seconds (average 0.016000, setup 0.00)

yes = 129613, no = 3239, maybe = 131713

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=264565, nc=215302, nnz=558244, k=4] [6.8 MB]
Creating vector for yes... [2.0 MB]
Allocating iteration vectors... [2 x 2.0 MB]
TOTAL: [12.9 MB]

Starting iterations...

Iterative method: 90 iterations in 0.58 seconds (average 0.002622, setup 0.34)

Value in the initial state: 0.01774229158269082

Time for model checking: 1.462 seconds.

Result: 0.01774229158269082 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmax=? [ F "AllAreaSearched" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=4,Objy1=0,Objx2=4,Objy2=3

Prob0A: 60 iterations in 0.47 seconds (average 0.007867, setup 0.00)

Prob1E: 4 iterations in 0.06 seconds (average 0.014000, setup 0.00)

yes = 1640, no = 204643, maybe = 58282

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=264565, nc=93837, nnz=250389, k=2] [3.2 MB]
Creating vector for yes... [2.0 MB]
Allocating iteration vectors... [2 x 2.0 MB]
TOTAL: [9.3 MB]

Starting iterations...

Iterative method: 87 iterations in 0.37 seconds (average 0.001931, setup 0.20)

Value in the initial state: 0.22465530734088862

Time for model checking: 0.92 seconds.

Result: 0.22465530734088862 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmin=? [ F "AllAreaSearched" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=4,Objy1=0,Objx2=4,Objy2=3

Prob0E: 63 iterations in 0.48 seconds (average 0.007556, setup 0.00)

Prob1A: 3 iterations in 0.06 seconds (average 0.021333, setup 0.00)

yes = 1640, no = 212163, maybe = 50762

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=264565, nc=82144, nnz=219064, k=2] [2.8 MB]
Creating vector for yes... [2.0 MB]
Allocating iteration vectors... [2 x 2.0 MB]
TOTAL: [8.9 MB]

Starting iterations...

Iterative method: 96 iterations in 0.46 seconds (average 0.002500, setup 0.22)

Value in the initial state: 0.0017913354264924245

Time for model checking: 1.02 seconds.

Result: 0.0017913354264924245 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmax=? [ F "MissionTimeUp"|"AllAreaSearched"|"Crash" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=4,Objy1=0,Objx2=4,Objy2=3

Prob0A: 3 iterations in 0.16 seconds (average 0.053333, setup 0.00)

Prob1E: 236 iterations in 4.32 seconds (average 0.018305, setup 0.00)

yes = 230139, no = 1135, maybe = 33291

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=264565, nc=56851, nnz=157767, k=2] [2.1 MB]
Creating vector for yes... [2.0 MB]
Allocating iteration vectors... [2 x 2.0 MB]
TOTAL: [8.2 MB]

Starting iterations...

Iterative method: 87 iterations in 0.35 seconds (average 0.001747, setup 0.20)

Value in the initial state: 0.25311609760775555

Time for model checking: 4.863 seconds.

Result: 0.25311609760775555 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmin=? [ F "MissionTimeUp"|"AllAreaSearched"|"Crash" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=4,Objy1=0,Objx2=4,Objy2=3

Prob0E: 4 iterations in 0.08 seconds (average 0.021000, setup 0.00)

Prob1A: 59 iterations in 0.60 seconds (average 0.010102, setup 0.00)

yes = 224869, no = 1135, maybe = 38561

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=264565, nc=65442, nnz=181864, k=2] [2.4 MB]
Creating vector for yes... [2.0 MB]
Allocating iteration vectors... [2 x 2.0 MB]
TOTAL: [8.5 MB]

Starting iterations...

Iterative method: 97 iterations in 0.35 seconds (average 0.001773, setup 0.18)

Value in the initial state: 0.01981855571363813

Time for model checking: 1.043 seconds.

Result: 0.01981855571363813 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"drop"}max=? [ F "deadlock" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=4,Objy1=0,Objx2=4,Objy2=3

Prob0E: 5 iterations in 0.08 seconds (average 0.016000, setup 0.00)

Prob1A: 1 iterations in 0.01 seconds (average 0.008000, setup 0.00)

goal = 1, inf = 0, maybe = 264564

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=264565, nc=348153, nnz=691095, k=4] [10.2 MB]
Building sparse matrix (transition rewards)... [n=264565, nc=348153, nnz=0, k=4] [2.3 MB]
Creating vector for state rewards... [2.0 MB]
Creating vector for inf... [2.0 MB]
Allocating iteration vectors... [2 x 2.0 MB]
TOTAL: [20.7 MB]

Starting iterations...

Iterative method: 95 iterations in 0.83 seconds (average 0.003789, setup 0.47)

Value in the initial state: 0.2117143566508078

Time for model checking: 0.934 seconds.

Result: 0.2117143566508078 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"drop"}min=? [ F "deadlock" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=4,Objy1=0,Objx2=4,Objy2=3

Prob0A: 4 iterations in 0.05 seconds (average 0.013000, setup 0.00)

Prob1E: 5 iterations in 0.06 seconds (average 0.012000, setup 0.00)

Warning: PRISM hasn't checked for zero-reward loops.
Your minimum rewards may be too low...

goal = 1, inf = 0, maybe = 264564

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=264565, nc=348153, nnz=691095, k=4] [10.2 MB]
Building sparse matrix (transition rewards)... [n=264565, nc=348153, nnz=0, k=4] [2.3 MB]
Creating vector for state rewards... [2.0 MB]
Creating vector for inf... [2.0 MB]
Allocating iteration vectors... [2 x 2.0 MB]
TOTAL: [20.7 MB]

Starting iterations...

Iterative method: 87 iterations in 0.70 seconds (average 0.003816, setup 0.36)

Value in the initial state: 0.18490070144582754

Time for model checking: 0.818 seconds.

Result: 0.18490070144582754 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"charge"}max=? [ F "deadlock" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=4,Objy1=0,Objx2=4,Objy2=3

Prob0E: 5 iterations in 0.04 seconds (average 0.008000, setup 0.00)

Prob1A: 1 iterations in 0.01 seconds (average 0.008000, setup 0.00)

goal = 1, inf = 0, maybe = 264564

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=264565, nc=348153, nnz=691095, k=4] [10.2 MB]
Building sparse matrix (transition rewards)... [n=264565, nc=348153, nnz=7628, k=4] [2.4 MB]
Creating vector for state rewards... [2.0 MB]
Creating vector for inf... [2.0 MB]
Allocating iteration vectors... [2 x 2.0 MB]
TOTAL: [20.7 MB]

Starting iterations...

Iterative method: 93 iterations in 0.71 seconds (average 0.003785, setup 0.36)

Value in the initial state: 0.2919444563458905

Time for model checking: 0.782 seconds.

Result: 0.2919444563458905 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"charge"}min=? [ F "deadlock" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=4,Objy1=0,Objx2=4,Objy2=3

Prob0A: 4 iterations in 0.03 seconds (average 0.007000, setup 0.00)

Prob1E: 5 iterations in 0.05 seconds (average 0.010400, setup 0.00)

Warning: PRISM hasn't checked for zero-reward loops.
Your minimum rewards may be too low...

goal = 1, inf = 0, maybe = 264564

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=264565, nc=348153, nnz=691095, k=4] [10.2 MB]
Building sparse matrix (transition rewards)... [n=264565, nc=348153, nnz=7628, k=4] [2.4 MB]
Creating vector for state rewards... [2.0 MB]
Creating vector for inf... [2.0 MB]
Allocating iteration vectors... [2 x 2.0 MB]
TOTAL: [20.7 MB]

Starting iterations...

Iterative method: 100 iterations in 0.75 seconds (average 0.003840, setup 0.37)

Value in the initial state: 0.05188748569473182

Time for model checking: 0.843 seconds.

Result: 0.05188748569473182 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"time"}max=? [ F "deadlock" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=4,Objy1=0,Objx2=4,Objy2=3

Prob0E: 5 iterations in 0.04 seconds (average 0.008000, setup 0.00)

Prob1A: 1 iterations in 0.00 seconds (average 0.004000, setup 0.00)

goal = 1, inf = 0, maybe = 264564

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=264565, nc=348153, nnz=691095, k=4] [10.2 MB]
Building sparse matrix (transition rewards)... [n=264565, nc=348153, nnz=132850, k=4] [3.9 MB]
Creating vector for state rewards... [2.0 MB]
Creating vector for inf... [2.0 MB]
Allocating iteration vectors... [2 x 2.0 MB]
TOTAL: [22.2 MB]

Starting iterations...

Iterative method: 97 iterations in 0.79 seconds (average 0.003918, setup 0.41)

Value in the initial state: 171.89225556607767

Time for model checking: 0.843 seconds.

Result: 171.89225556607767 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"time"}min=? [ F "deadlock" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=4,Objy1=0,Objx2=4,Objy2=3

Prob0A: 4 iterations in 0.04 seconds (average 0.009000, setup 0.00)

Prob1E: 5 iterations in 0.06 seconds (average 0.011200, setup 0.00)

Warning: PRISM hasn't checked for zero-reward loops.
Your minimum rewards may be too low...

goal = 1, inf = 0, maybe = 264564

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=264565, nc=348153, nnz=691095, k=4] [10.2 MB]
Building sparse matrix (transition rewards)... [n=264565, nc=348153, nnz=132850, k=4] [3.9 MB]
Creating vector for state rewards... [2.0 MB]
Creating vector for inf... [2.0 MB]
Allocating iteration vectors... [2 x 2.0 MB]
TOTAL: [22.2 MB]

Starting iterations...

Iterative method: 105 iterations in 0.81 seconds (average 0.003924, setup 0.40)

Value in the initial state: 98.3228313229742

Time for model checking: 0.913 seconds.

Result: 98.3228313229742 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"success"}max=? [ F "deadlock" ]/Pmin=? [ F "MissionSuccessful" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=4,Objy1=0,Objx2=4,Objy2=3

Prob0E: 5 iterations in 0.03 seconds (average 0.006400, setup 0.00)

Prob1A: 1 iterations in 0.00 seconds (average 0.004000, setup 0.00)

goal = 1, inf = 0, maybe = 264564

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=264565, nc=348153, nnz=691095, k=4] [10.2 MB]
Building sparse matrix (transition rewards)... [n=264565, nc=348153, nnz=2234, k=4] [2.4 MB]
Creating vector for state rewards... [2.0 MB]
Creating vector for inf... [2.0 MB]
Allocating iteration vectors... [2 x 2.0 MB]
TOTAL: [20.7 MB]

Starting iterations...

Iterative method: 99 iterations in 0.74 seconds (average 0.003798, setup 0.36)

Prob0E: 61 iterations in 0.35 seconds (average 0.005770, setup 0.00)

Prob1A: 3 iterations in 0.06 seconds (average 0.018667, setup 0.00)

yes = 1134, no = 230140, maybe = 33291

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=264565, nc=56851, nnz=157767, k=2] [2.1 MB]
Creating vector for yes... [2.0 MB]
Allocating iteration vectors... [2 x 2.0 MB]
TOTAL: [8.2 MB]

Starting iterations...

Iterative method: 94 iterations in 0.34 seconds (average 0.001617, setup 0.18)

Value in the initial state: 225.68808807031263

Time for model checking: 1.576 seconds.

Result: 225.68808807031263 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"success"}min=? [ F "deadlock" ]/Pmax=? [ F "MissionSuccessful" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=4,Objy1=0,Objx2=4,Objy2=3

Prob0A: 4 iterations in 0.11 seconds (average 0.028000, setup 0.00)

Prob1E: 5 iterations in 0.06 seconds (average 0.011200, setup 0.00)

Warning: PRISM hasn't checked for zero-reward loops.
Your minimum rewards may be too low...

goal = 1, inf = 0, maybe = 264564

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=264565, nc=348153, nnz=691095, k=4] [10.2 MB]
Building sparse matrix (transition rewards)... [n=264565, nc=348153, nnz=2234, k=4] [2.4 MB]
Creating vector for state rewards... [2.0 MB]
Creating vector for inf... [2.0 MB]
Allocating iteration vectors... [2 x 2.0 MB]
TOTAL: [20.7 MB]

Starting iterations...

Iterative method: 98 iterations in 0.78 seconds (average 0.003714, setup 0.42)

Prob0A: 59 iterations in 0.37 seconds (average 0.006237, setup 0.00)

Prob1E: 4 iterations in 0.07 seconds (average 0.017000, setup 0.00)

yes = 1134, no = 224870, maybe = 38561

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=264565, nc=65442, nnz=181864, k=2] [2.4 MB]
Creating vector for yes... [2.0 MB]
Allocating iteration vectors... [2 x 2.0 MB]
TOTAL: [8.5 MB]

Starting iterations...

Iterative method: 99 iterations in 0.38 seconds (average 0.001778, setup 0.20)

Value in the initial state: 79.46462076315213

Time for model checking: 1.786 seconds.

Result: 79.46462076315213 (value in the initial state)

---------------------------------------------------------------------

Note: There were 16 warnings during computation.

