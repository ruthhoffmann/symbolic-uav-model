PRISM
=====

Version: 4.3
Date: Wed Aug 03 15:55:32 BST 2016
Hostname: eglinton
Memory limits: cudd=1g, java(heap)=981.5m
Command line: prism uav_sim_mdp.prism -s uav_mdp.props -const 'Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=1,Objy1=2,Objx2=1,Objy2=3,'

Parsing model file "uav_sim_mdp.prism"...

Parsing properties file "uav_mdp.props"...

18 properties:
(1) Pmax=? [ F "MissionSuccessful" ]
(2) Pmin=? [ F "MissionSuccessful" ]
(3) Pmax=? [ F "MissionTimeUp" ]
(4) Pmin=? [ F "MissionTimeUp" ]
(5) Pmax=? [ F "Crash" ]
(6) Pmin=? [ F "Crash" ]
(7) Pmax=? [ F "AllAreaSearched" ]
(8) Pmin=? [ F "AllAreaSearched" ]
(9) Pmax=? [ F "MissionTimeUp"|"AllAreaSearched"|"Crash" ]
(10) Pmin=? [ F "MissionTimeUp"|"AllAreaSearched"|"Crash" ]
(11) R{"drop"}max=? [ F "deadlock" ]
(12) R{"drop"}min=? [ F "deadlock" ]
(13) R{"charge"}max=? [ F "deadlock" ]
(14) R{"charge"}min=? [ F "deadlock" ]
(15) R{"time"}max=? [ F "deadlock" ]
(16) R{"time"}min=? [ F "deadlock" ]
(17) R{"success"}max=? [ F "deadlock" ]/Pmin=? [ F "MissionSuccessful" ]
(18) R{"success"}min=? [ F "deadlock" ]/Pmax=? [ F "MissionSuccessful" ]

Type:        MDP
Modules:     Obj Move Robot T 
Variables:   NoOfObjs posx posy s1 retx rety b t c 

---------------------------------------------------------------------

Model checking: Pmax=? [ F "MissionSuccessful" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=1,Objy1=2,Objx2=1,Objy2=3

Building model...
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=1,Objy1=2,Objx2=1,Objy2=3

Warning: Guard for command 7 of module "Move" is never satisfied.

Warning: Guard for command 8 of module "Move" is never satisfied.

Warning: Guard for command 9 of module "Move" is never satisfied.

Warning: Guard for command 7 of module "Robot" is never satisfied.

Warning: Update 2 of command 3 of module "Robot" doesn't do anything ("(s1'=13)", line 210, column 44)

Warning: Update 2 of command 27 of module "T" doesn't do anything ("(c'=1)", line 323, column 108)

Warning: Guard for command 28 of module "T" is never satisfied.

Warning: Update 2 of command 9 of module "T" doesn't do anything ("(c'=1)", line 300, column 112)

Warning: Guard for command 10 of module "T" is never satisfied.

Warning: Update 2 of command 51 of module "T" doesn't do anything ("(c'=1)", line 354, column 113)

Warning: Guard for command 52 of module "T" is never satisfied.

Computing reachable states...

Reachability (BFS): 88 iterations in 0.98 seconds (average 0.011182, setup 0.00)

Time for model construction: 3.116 seconds.

Warning: Deadlocks detected and fixed in 1 states

Type:        MDP
States:      165406 (1 initial)
Transitions: 426426
Choices:     219279

Transition matrix: 180013 nodes (89 terminal), 426426 minterms, vars: 34r/34c/18nd

Prob0A: 53 iterations in 0.33 seconds (average 0.006189, setup 0.00)

Prob1E: 4 iterations in 0.05 seconds (average 0.012000, setup 0.00)

yes = 1043, no = 134077, maybe = 30286

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=165406, nc=51452, nnz=137995, k=2] [1.8 MB]
Creating vector for yes... [1.3 MB]
Allocating iteration vectors... [2 x 1.3 MB]
TOTAL: [5.6 MB]

Starting iterations...

Iterative method: 93 iterations in 0.32 seconds (average 0.001075, setup 0.22)

Value in the initial state: 0.9822530814827558

Time for model checking: 0.869 seconds.

Result: 0.9822530814827558 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmin=? [ F "MissionSuccessful" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=1,Objy1=2,Objx2=1,Objy2=3

Prob0E: 53 iterations in 0.37 seconds (average 0.006943, setup 0.00)

Prob1A: 3 iterations in 0.05 seconds (average 0.016000, setup 0.00)

yes = 1043, no = 137912, maybe = 26451

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=165406, nc=44960, nnz=120010, k=2] [1.6 MB]
Creating vector for yes... [1.3 MB]
Allocating iteration vectors... [2 x 1.3 MB]
TOTAL: [5.4 MB]

Starting iterations...

Iterative method: 91 iterations in 0.28 seconds (average 0.001099, setup 0.18)

Value in the initial state: 0.9700738388883218

Time for model checking: 0.735 seconds.

Result: 0.9700738388883218 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmax=? [ F "MissionTimeUp" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=1,Objy1=2,Objx2=1,Objy2=3

Prob0A: 76 iterations in 0.66 seconds (average 0.008632, setup 0.00)

Prob1E: 4 iterations in 0.04 seconds (average 0.009000, setup 0.00)

yes = 248, no = 98816, maybe = 66342

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=165406, nc=108242, nnz=284469, k=4] [3.5 MB]
Creating vector for yes... [1.3 MB]
Allocating iteration vectors... [2 x 1.3 MB]
TOTAL: [7.3 MB]

Starting iterations...

Iterative method: 99 iterations in 0.36 seconds (average 0.001414, setup 0.22)

Value in the initial state: 3.152207534764809E-5

Time for model checking: 1.086 seconds.

Result: 3.152207534764809E-5 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmin=? [ F "MissionTimeUp" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=1,Objy1=2,Objx2=1,Objy2=3

Prob0E: 90 iterations in 0.61 seconds (average 0.006756, setup 0.00)

Prob1A: 3 iterations in 0.03 seconds (average 0.010667, setup 0.00)

yes = 248, no = 98900, maybe = 66258

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=165406, nc=108110, nnz=284073, k=4] [3.5 MB]
Creating vector for yes... [1.3 MB]
Allocating iteration vectors... [2 x 1.3 MB]
TOTAL: [7.3 MB]

Starting iterations...

Iterative method: 115 iterations in 0.38 seconds (average 0.001391, setup 0.22)

Value in the initial state: 4.039729635307162E-8

Time for model checking: 1.048 seconds.

Result: 4.039729635307162E-8 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmax=? [ F "Crash" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=1,Objy1=2,Objx2=1,Objy2=3

Prob0A: 3 iterations in 0.09 seconds (average 0.030667, setup 0.00)

Prob1E: 200 iterations in 2.29 seconds (average 0.011460, setup 0.00)

yes = 81302, no = 2790, maybe = 81314

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=165406, nc=135187, nnz=342334, k=4] [4.2 MB]
Creating vector for yes... [1.3 MB]
Allocating iteration vectors... [2 x 1.3 MB]
TOTAL: [8.0 MB]

Starting iterations...

Iterative method: 93 iterations in 0.41 seconds (average 0.001505, setup 0.27)

Value in the initial state: 0.027689181945980808

Time for model checking: 2.829 seconds.

Result: 0.027689181945980808 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmin=? [ F "Crash" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=1,Objy1=2,Objx2=1,Objy2=3

Prob0E: 4 iterations in 0.05 seconds (average 0.013000, setup 0.00)

Prob1A: 53 iterations in 0.43 seconds (average 0.008151, setup 0.00)

yes = 81302, no = 2790, maybe = 81314

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=165406, nc=135187, nnz=342334, k=4] [4.2 MB]
Creating vector for yes... [1.3 MB]
Allocating iteration vectors... [2 x 1.3 MB]
TOTAL: [8.0 MB]

Starting iterations...

Iterative method: 88 iterations in 0.30 seconds (average 0.001545, setup 0.16)

Value in the initial state: 0.01576303935177882

Time for model checking: 0.806 seconds.

Result: 0.01576303935177882 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmax=? [ F "AllAreaSearched" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=1,Objy1=2,Objx2=1,Objy2=3

Prob0A: 53 iterations in 0.40 seconds (average 0.007623, setup 0.00)

Prob1E: 4 iterations in 0.04 seconds (average 0.009000, setup 0.00)

yes = 1498, no = 114531, maybe = 49377

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=165406, nc=81690, nnz=215474, k=2] [2.7 MB]
Creating vector for yes... [1.3 MB]
Allocating iteration vectors... [2 x 1.3 MB]
TOTAL: [6.5 MB]

Starting iterations...

Iterative method: 92 iterations in 0.32 seconds (average 0.001348, setup 0.19)

Value in the initial state: 0.0022362272190872214

Time for model checking: 0.776 seconds.

Result: 0.0022362272190872214 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmin=? [ F "AllAreaSearched" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=1,Objy1=2,Objx2=1,Objy2=3

Prob0E: 53 iterations in 0.29 seconds (average 0.005509, setup 0.00)

Prob1A: 3 iterations in 0.03 seconds (average 0.009333, setup 0.00)

yes = 1498, no = 119407, maybe = 44501

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=165406, nc=73542, nnz=193074, k=2] [2.4 MB]
Creating vector for yes... [1.3 MB]
Allocating iteration vectors... [2 x 1.3 MB]
TOTAL: [6.2 MB]

Starting iterations...

Iterative method: 94 iterations in 0.25 seconds (average 0.001319, setup 0.12)

Value in the initial state: 0.0018528358607690489

Time for model checking: 0.562 seconds.

Result: 0.0018528358607690489 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmax=? [ F "MissionTimeUp"|"AllAreaSearched"|"Crash" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=1,Objy1=2,Objx2=1,Objy2=3

Prob0A: 3 iterations in 0.07 seconds (average 0.024000, setup 0.00)

Prob1E: 204 iterations in 2.21 seconds (average 0.010843, setup 0.00)

yes = 137911, no = 1044, maybe = 26451

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=165406, nc=44960, nnz=120010, k=2] [1.6 MB]
Creating vector for yes... [1.3 MB]
Allocating iteration vectors... [2 x 1.3 MB]
TOTAL: [5.4 MB]

Starting iterations...

Iterative method: 88 iterations in 0.26 seconds (average 0.001091, setup 0.16)

Value in the initial state: 0.029926032565061482

Time for model checking: 2.575 seconds.

Result: 0.029926032565061482 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmin=? [ F "MissionTimeUp"|"AllAreaSearched"|"Crash" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=1,Objy1=2,Objx2=1,Objy2=3

Prob0E: 4 iterations in 0.05 seconds (average 0.012000, setup 0.00)

Prob1A: 53 iterations in 0.41 seconds (average 0.007698, setup 0.00)

yes = 134076, no = 1044, maybe = 30286

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=165406, nc=51452, nnz=137995, k=2] [1.8 MB]
Creating vector for yes... [1.3 MB]
Allocating iteration vectors... [2 x 1.3 MB]
TOTAL: [5.6 MB]

Starting iterations...

Iterative method: 90 iterations in 0.24 seconds (average 0.001156, setup 0.14)

Value in the initial state: 0.01774555674089486

Time for model checking: 0.717 seconds.

Result: 0.01774555674089486 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"drop"}max=? [ F "deadlock" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=1,Objy1=2,Objx2=1,Objy2=3

Prob0E: 5 iterations in 0.05 seconds (average 0.009600, setup 0.00)

Prob1A: 1 iterations in 0.01 seconds (average 0.008000, setup 0.00)

goal = 1, inf = 0, maybe = 165405

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=165406, nc=219278, nnz=426425, k=4] [6.3 MB]
Building sparse matrix (transition rewards)... [n=165406, nc=219278, nnz=0, k=4] [1.5 MB]
Creating vector for state rewards... [1.3 MB]
Creating vector for inf... [1.3 MB]
Allocating iteration vectors... [2 x 1.3 MB]
TOTAL: [12.9 MB]

Starting iterations...

Iterative method: 89 iterations in 0.53 seconds (average 0.002337, setup 0.32)

Value in the initial state: 0.11983517968576035

Time for model checking: 0.598 seconds.

Result: 0.11983517968576035 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"drop"}min=? [ F "deadlock" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=1,Objy1=2,Objx2=1,Objy2=3

Prob0A: 4 iterations in 0.03 seconds (average 0.008000, setup 0.00)

Prob1E: 5 iterations in 0.04 seconds (average 0.008000, setup 0.00)

Warning: PRISM hasn't checked for zero-reward loops.
Your minimum rewards may be too low...

goal = 1, inf = 0, maybe = 165405

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=165406, nc=219278, nnz=426425, k=4] [6.3 MB]
Building sparse matrix (transition rewards)... [n=165406, nc=219278, nnz=0, k=4] [1.5 MB]
Creating vector for state rewards... [1.3 MB]
Creating vector for inf... [1.3 MB]
Allocating iteration vectors... [2 x 1.3 MB]
TOTAL: [12.9 MB]

Starting iterations...

Iterative method: 84 iterations in 0.43 seconds (average 0.002429, setup 0.22)

Value in the initial state: 0.10873396167687385

Time for model checking: 0.515 seconds.

Result: 0.10873396167687385 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"charge"}max=? [ F "deadlock" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=1,Objy1=2,Objx2=1,Objy2=3

Prob0E: 5 iterations in 0.03 seconds (average 0.005600, setup 0.00)

Prob1A: 1 iterations in 0.00 seconds (average 0.004000, setup 0.00)

goal = 1, inf = 0, maybe = 165405

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=165406, nc=219278, nnz=426425, k=4] [6.3 MB]
Building sparse matrix (transition rewards)... [n=165406, nc=219278, nnz=4398, k=4] [1.5 MB]
Creating vector for state rewards... [1.3 MB]
Creating vector for inf... [1.3 MB]
Allocating iteration vectors... [2 x 1.3 MB]
TOTAL: [12.9 MB]

Starting iterations...

Iterative method: 92 iterations in 0.46 seconds (average 0.002391, setup 0.24)

Value in the initial state: 0.2340721670450267

Time for model checking: 0.496 seconds.

Result: 0.2340721670450267 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"charge"}min=? [ F "deadlock" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=1,Objy1=2,Objx2=1,Objy2=3

Prob0A: 4 iterations in 0.02 seconds (average 0.005000, setup 0.00)

Prob1E: 5 iterations in 0.04 seconds (average 0.008000, setup 0.00)

Warning: PRISM hasn't checked for zero-reward loops.
Your minimum rewards may be too low...

goal = 1, inf = 0, maybe = 165405

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=165406, nc=219278, nnz=426425, k=4] [6.3 MB]
Building sparse matrix (transition rewards)... [n=165406, nc=219278, nnz=4398, k=4] [1.5 MB]
Creating vector for state rewards... [1.3 MB]
Creating vector for inf... [1.3 MB]
Allocating iteration vectors... [2 x 1.3 MB]
TOTAL: [12.9 MB]

Starting iterations...

Iterative method: 74 iterations in 0.41 seconds (average 0.002378, setup 0.24)

Value in the initial state: 0.02601673630529491

Time for model checking: 0.499 seconds.

Result: 0.02601673630529491 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"time"}max=? [ F "deadlock" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=1,Objy1=2,Objx2=1,Objy2=3

Prob0E: 5 iterations in 0.03 seconds (average 0.005600, setup 0.00)

Prob1A: 1 iterations in 0.00 seconds (average 0.004000, setup 0.00)

goal = 1, inf = 0, maybe = 165405

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=165406, nc=219278, nnz=426425, k=4] [6.3 MB]
Building sparse matrix (transition rewards)... [n=165406, nc=219278, nnz=84090, k=4] [2.4 MB]
Creating vector for state rewards... [1.3 MB]
Creating vector for inf... [1.3 MB]
Allocating iteration vectors... [2 x 1.3 MB]
TOTAL: [13.8 MB]

Starting iterations...

Iterative method: 96 iterations in 0.50 seconds (average 0.002458, setup 0.26)

Value in the initial state: 152.94223722031018

Time for model checking: 0.541 seconds.

Result: 152.94223722031018 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"time"}min=? [ F "deadlock" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=1,Objy1=2,Objx2=1,Objy2=3

Prob0A: 4 iterations in 0.02 seconds (average 0.006000, setup 0.00)

Prob1E: 5 iterations in 0.04 seconds (average 0.008000, setup 0.00)

Warning: PRISM hasn't checked for zero-reward loops.
Your minimum rewards may be too low...

goal = 1, inf = 0, maybe = 165405

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=165406, nc=219278, nnz=426425, k=4] [6.3 MB]
Building sparse matrix (transition rewards)... [n=165406, nc=219278, nnz=84090, k=4] [2.4 MB]
Creating vector for state rewards... [1.3 MB]
Creating vector for inf... [1.3 MB]
Allocating iteration vectors... [2 x 1.3 MB]
TOTAL: [13.8 MB]

Starting iterations...

Iterative method: 98 iterations in 0.50 seconds (average 0.002490, setup 0.26)

Value in the initial state: 87.44766285151246

Time for model checking: 0.575 seconds.

Result: 87.44766285151246 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"success"}max=? [ F "deadlock" ]/Pmin=? [ F "MissionSuccessful" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=1,Objy1=2,Objx2=1,Objy2=3

Prob0E: 5 iterations in 0.03 seconds (average 0.005600, setup 0.00)

Prob1A: 1 iterations in 0.00 seconds (average 0.000000, setup 0.00)

goal = 1, inf = 0, maybe = 165405

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=165406, nc=219278, nnz=426425, k=4] [6.3 MB]
Building sparse matrix (transition rewards)... [n=165406, nc=219278, nnz=2056, k=4] [1.5 MB]
Creating vector for state rewards... [1.3 MB]
Creating vector for inf... [1.3 MB]
Allocating iteration vectors... [2 x 1.3 MB]
TOTAL: [12.9 MB]

Starting iterations...

Iterative method: 96 iterations in 0.46 seconds (average 0.002333, setup 0.23)

Prob0E: 53 iterations in 0.33 seconds (average 0.006264, setup 0.00)

Prob1A: 3 iterations in 0.04 seconds (average 0.014667, setup 0.00)

yes = 1043, no = 137912, maybe = 26451

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=165406, nc=44960, nnz=120010, k=2] [1.6 MB]
Creating vector for yes... [1.3 MB]
Allocating iteration vectors... [2 x 1.3 MB]
TOTAL: [5.4 MB]

Starting iterations...

Iterative method: 91 iterations in 0.20 seconds (average 0.001055, setup 0.11)

Value in the initial state: 155.15148206128484

Time for model checking: 1.093 seconds.

Result: 155.15148206128484 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"success"}min=? [ F "deadlock" ]/Pmax=? [ F "MissionSuccessful" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=1,Objy1=2,Objx2=1,Objy2=3

Prob0A: 4 iterations in 0.06 seconds (average 0.014000, setup 0.00)

Prob1E: 5 iterations in 0.04 seconds (average 0.008800, setup 0.00)

Warning: PRISM hasn't checked for zero-reward loops.
Your minimum rewards may be too low...

goal = 1, inf = 0, maybe = 165405

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=165406, nc=219278, nnz=426425, k=4] [6.3 MB]
Building sparse matrix (transition rewards)... [n=165406, nc=219278, nnz=2056, k=4] [1.5 MB]
Creating vector for state rewards... [1.3 MB]
Creating vector for inf... [1.3 MB]
Allocating iteration vectors... [2 x 1.3 MB]
TOTAL: [12.9 MB]

Starting iterations...

Iterative method: 97 iterations in 0.47 seconds (average 0.002351, setup 0.24)

Prob0A: 53 iterations in 0.30 seconds (average 0.005660, setup 0.00)

Prob1E: 4 iterations in 0.03 seconds (average 0.008000, setup 0.00)

yes = 1043, no = 134077, maybe = 30286

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=165406, nc=51452, nnz=137995, k=2] [1.8 MB]
Creating vector for yes... [1.3 MB]
Allocating iteration vectors... [2 x 1.3 MB]
TOTAL: [5.6 MB]

Starting iterations...

Iterative method: 93 iterations in 0.22 seconds (average 0.001118, setup 0.12)

Value in the initial state: 88.1329472584508

Time for model checking: 1.128 seconds.

Result: 88.1329472584508 (value in the initial state)

---------------------------------------------------------------------

Note: There were 16 warnings during computation.

