PRISM
=====

Version: 4.3
Date: Wed Aug 03 15:53:27 BST 2016
Hostname: eglinton
Memory limits: cudd=1g, java(heap)=981.5m
Command line: prism uav_sim_mdp.prism -s uav_mdp.props -const 'Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=0,Objy1=3,Objx2=6,Objy2=0,'

Parsing model file "uav_sim_mdp.prism"...

Parsing properties file "uav_mdp.props"...

18 properties:
(1) Pmax=? [ F "MissionSuccessful" ]
(2) Pmin=? [ F "MissionSuccessful" ]
(3) Pmax=? [ F "MissionTimeUp" ]
(4) Pmin=? [ F "MissionTimeUp" ]
(5) Pmax=? [ F "Crash" ]
(6) Pmin=? [ F "Crash" ]
(7) Pmax=? [ F "AllAreaSearched" ]
(8) Pmin=? [ F "AllAreaSearched" ]
(9) Pmax=? [ F "MissionTimeUp"|"AllAreaSearched"|"Crash" ]
(10) Pmin=? [ F "MissionTimeUp"|"AllAreaSearched"|"Crash" ]
(11) R{"drop"}max=? [ F "deadlock" ]
(12) R{"drop"}min=? [ F "deadlock" ]
(13) R{"charge"}max=? [ F "deadlock" ]
(14) R{"charge"}min=? [ F "deadlock" ]
(15) R{"time"}max=? [ F "deadlock" ]
(16) R{"time"}min=? [ F "deadlock" ]
(17) R{"success"}max=? [ F "deadlock" ]/Pmin=? [ F "MissionSuccessful" ]
(18) R{"success"}min=? [ F "deadlock" ]/Pmax=? [ F "MissionSuccessful" ]

Type:        MDP
Modules:     Obj Move Robot T 
Variables:   NoOfObjs posx posy s1 retx rety b t c 

---------------------------------------------------------------------

Model checking: Pmax=? [ F "MissionSuccessful" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=0,Objy1=3,Objx2=6,Objy2=0

Building model...
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=0,Objy1=3,Objx2=6,Objy2=0

Warning: Guard for command 7 of module "Move" is never satisfied.

Warning: Guard for command 8 of module "Move" is never satisfied.

Warning: Guard for command 9 of module "Move" is never satisfied.

Warning: Guard for command 6 of module "Robot" is never satisfied.

Warning: Update 2 of command 3 of module "Robot" doesn't do anything ("(s1'=13)", line 210, column 44)

Warning: Update 2 of command 27 of module "T" doesn't do anything ("(c'=1)", line 323, column 108)

Warning: Guard for command 28 of module "T" is never satisfied.

Warning: Update 2 of command 9 of module "T" doesn't do anything ("(c'=1)", line 300, column 112)

Warning: Guard for command 10 of module "T" is never satisfied.

Warning: Update 2 of command 51 of module "T" doesn't do anything ("(c'=1)", line 354, column 113)

Warning: Guard for command 52 of module "T" is never satisfied.

Computing reachable states...

Reachability (BFS): 90 iterations in 1.68 seconds (average 0.018711, setup 0.00)

Time for model construction: 5.167 seconds.

Warning: Deadlocks detected and fixed in 1 states

Type:        MDP
States:      257451 (1 initial)
Transitions: 699606
Choices:     343458

Transition matrix: 259808 nodes (95 terminal), 699606 minterms, vars: 34r/34c/18nd

Prob0A: 60 iterations in 0.89 seconds (average 0.014867, setup 0.00)

Prob1E: 4 iterations in 0.10 seconds (average 0.026000, setup 0.00)

yes = 969, no = 209144, maybe = 47338

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=257451, nc=80345, nnz=221985, k=2] [2.9 MB]
Creating vector for yes... [2.0 MB]
Allocating iteration vectors... [2 x 2.0 MB]
TOTAL: [8.8 MB]

Starting iterations...

Iterative method: 102 iterations in 0.53 seconds (average 0.001922, setup 0.34)

Value in the initial state: 0.8052088956465904

Time for model checking: 1.94 seconds.

Result: 0.8052088956465904 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmin=? [ F "MissionSuccessful" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=0,Objy1=3,Objx2=6,Objy2=0

Prob0E: 77 iterations in 0.89 seconds (average 0.011584, setup 0.00)

Prob1A: 3 iterations in 0.08 seconds (average 0.026667, setup 0.00)

yes = 969, no = 218109, maybe = 38373

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=257451, nc=64861, nnz=178635, k=2] [2.4 MB]
Creating vector for yes... [2.0 MB]
Allocating iteration vectors... [2 x 2.0 MB]
TOTAL: [8.2 MB]

Starting iterations...

Iterative method: 99 iterations in 0.42 seconds (average 0.001939, setup 0.23)

Value in the initial state: 0.00682641416955537

Time for model checking: 1.469 seconds.

Result: 0.00682641416955537 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmax=? [ F "MissionTimeUp" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=0,Objy1=3,Objx2=6,Objy2=0

Prob0A: 68 iterations in 0.60 seconds (average 0.008824, setup 0.00)

Prob1E: 4 iterations in 0.06 seconds (average 0.015000, setup 0.00)

yes = 416, no = 204197, maybe = 52838

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=257451, nc=91429, nnz=251512, k=4] [3.2 MB]
Creating vector for yes... [2.0 MB]
Allocating iteration vectors... [2 x 2.0 MB]
TOTAL: [9.1 MB]

Starting iterations...

Iterative method: 100 iterations in 0.40 seconds (average 0.001840, setup 0.22)

Value in the initial state: 7.691886171980358E-5

Time for model checking: 1.133 seconds.

Result: 7.691886171980358E-5 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmin=? [ F "MissionTimeUp" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=0,Objy1=3,Objx2=6,Objy2=0

Prob0E: 84 iterations in 0.83 seconds (average 0.009905, setup 0.00)

Prob1A: 3 iterations in 0.06 seconds (average 0.020000, setup 0.00)

yes = 416, no = 204502, maybe = 52533

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=257451, nc=90879, nnz=250363, k=4] [3.2 MB]
Creating vector for yes... [2.0 MB]
Allocating iteration vectors... [2 x 2.0 MB]
TOTAL: [9.1 MB]

Starting iterations...

Iterative method: 115 iterations in 0.38 seconds (average 0.001843, setup 0.17)

Value in the initial state: 1.875492502055201E-7

Time for model checking: 1.379 seconds.

Result: 1.875492502055201E-7 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmax=? [ F "Crash" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=0,Objy1=3,Objx2=6,Objy2=0

Prob0A: 3 iterations in 0.18 seconds (average 0.061333, setup 0.00)

Prob1E: 207 iterations in 4.88 seconds (average 0.023556, setup 0.00)

yes = 125806, no = 1686, maybe = 129959

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=257451, nc=215966, nnz=572114, k=4] [7.0 MB]
Creating vector for yes... [2.0 MB]
Allocating iteration vectors... [2 x 2.0 MB]
TOTAL: [12.9 MB]

Starting iterations...

Iterative method: 98 iterations in 0.70 seconds (average 0.002449, setup 0.46)

Value in the initial state: 0.019872685659748295

Time for model checking: 6.932 seconds.

Result: 0.019872685659748295 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmin=? [ F "Crash" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=0,Objy1=3,Objx2=6,Objy2=0

Prob0E: 4 iterations in 0.15 seconds (average 0.037000, setup 0.00)

Prob1A: 55 iterations in 1.05 seconds (average 0.019127, setup 0.00)

yes = 125806, no = 1686, maybe = 129959

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=257451, nc=215966, nnz=572114, k=4] [7.0 MB]
Creating vector for yes... [2.0 MB]
Allocating iteration vectors... [2 x 2.0 MB]
TOTAL: [12.9 MB]

Starting iterations...

Iterative method: 98 iterations in 0.61 seconds (average 0.002571, setup 0.36)

Value in the initial state: 0.016267899206725164

Time for model checking: 2.156 seconds.

Result: 0.016267899206725164 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmax=? [ F "AllAreaSearched" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=0,Objy1=3,Objx2=6,Objy2=0

Prob0A: 57 iterations in 0.65 seconds (average 0.011368, setup 0.00)

Prob1E: 4 iterations in 0.07 seconds (average 0.018000, setup 0.00)

yes = 300, no = 168849, maybe = 88302

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=257451, nc=143987, nnz=389916, k=2] [4.8 MB]
Creating vector for yes... [2.0 MB]
Allocating iteration vectors... [2 x 2.0 MB]
TOTAL: [10.7 MB]

Starting iterations...

Iterative method: 100 iterations in 0.51 seconds (average 0.002160, setup 0.30)

Value in the initial state: 0.9756220159391785

Time for model checking: 1.445 seconds.

Result: 0.9756220159391785 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmin=? [ F "AllAreaSearched" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=0,Objy1=3,Objx2=6,Objy2=0

Prob0E: 75 iterations in 0.93 seconds (average 0.012427, setup 0.00)

Prob1A: 3 iterations in 0.05 seconds (average 0.017333, setup 0.00)

yes = 300, no = 172152, maybe = 84999

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=257451, nc=137711, nnz=371145, k=2] [4.6 MB]
Creating vector for yes... [2.0 MB]
Allocating iteration vectors... [2 x 2.0 MB]
TOTAL: [10.5 MB]

Starting iterations...

Iterative method: 105 iterations in 0.46 seconds (average 0.002248, setup 0.23)

Value in the initial state: 0.17682566654693221

Time for model checking: 1.492 seconds.

Result: 0.17682566654693221 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmax=? [ F "MissionTimeUp"|"AllAreaSearched"|"Crash" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=0,Objy1=3,Objx2=6,Objy2=0

Prob0A: 3 iterations in 0.13 seconds (average 0.042667, setup 0.00)

Prob1E: 285 iterations in 6.24 seconds (average 0.021881, setup 0.00)

yes = 218108, no = 970, maybe = 38373

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=257451, nc=64861, nnz=178635, k=2] [2.4 MB]
Creating vector for yes... [2.0 MB]
Allocating iteration vectors... [2 x 2.0 MB]
TOTAL: [8.2 MB]

Starting iterations...

Iterative method: 85 iterations in 0.40 seconds (average 0.001694, setup 0.25)

Value in the initial state: 0.9931725197881488

Time for model checking: 6.815 seconds.

Result: 0.9931725197881488 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmin=? [ F "MissionTimeUp"|"AllAreaSearched"|"Crash" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=0,Objy1=3,Objx2=6,Objy2=0

Prob0E: 4 iterations in 0.08 seconds (average 0.019000, setup 0.00)

Prob1A: 60 iterations in 0.85 seconds (average 0.014200, setup 0.00)

yes = 209143, no = 970, maybe = 47338

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=257451, nc=80345, nnz=221985, k=2] [2.9 MB]
Creating vector for yes... [2.0 MB]
Allocating iteration vectors... [2 x 2.0 MB]
TOTAL: [8.8 MB]

Starting iterations...

Iterative method: 95 iterations in 0.36 seconds (average 0.001811, setup 0.19)

Value in the initial state: 0.19479057552718476

Time for model checking: 1.327 seconds.

Result: 0.19479057552718476 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"drop"}max=? [ F "deadlock" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=0,Objy1=3,Objx2=6,Objy2=0

Prob0E: 5 iterations in 0.08 seconds (average 0.016800, setup 0.00)

Prob1A: 1 iterations in 0.01 seconds (average 0.012000, setup 0.00)

goal = 1, inf = 0, maybe = 257450

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=257451, nc=343457, nnz=699605, k=4] [10.3 MB]
Building sparse matrix (transition rewards)... [n=257451, nc=343457, nnz=0, k=4] [2.3 MB]
Creating vector for state rewards... [2.0 MB]
Creating vector for inf... [2.0 MB]
Allocating iteration vectors... [2 x 2.0 MB]
TOTAL: [20.4 MB]

Starting iterations...

Iterative method: 101 iterations in 0.86 seconds (average 0.003723, setup 0.48)

Value in the initial state: 0.2162892847447966

Time for model checking: 0.971 seconds.

Result: 0.2162892847447966 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"drop"}min=? [ F "deadlock" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=0,Objy1=3,Objx2=6,Objy2=0

Prob0A: 4 iterations in 0.05 seconds (average 0.012000, setup 0.00)

Prob1E: 5 iterations in 0.08 seconds (average 0.015200, setup 0.00)

Warning: PRISM hasn't checked for zero-reward loops.
Your minimum rewards may be too low...

goal = 1, inf = 0, maybe = 257450

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=257451, nc=343457, nnz=699605, k=4] [10.3 MB]
Building sparse matrix (transition rewards)... [n=257451, nc=343457, nnz=0, k=4] [2.3 MB]
Creating vector for state rewards... [2.0 MB]
Creating vector for inf... [2.0 MB]
Allocating iteration vectors... [2 x 2.0 MB]
TOTAL: [20.4 MB]

Starting iterations...

Iterative method: 92 iterations in 0.72 seconds (average 0.003739, setup 0.38)

Value in the initial state: 0.19548761660558364

Time for model checking: 0.861 seconds.

Result: 0.19548761660558364 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"charge"}max=? [ F "deadlock" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=0,Objy1=3,Objx2=6,Objy2=0

Prob0E: 5 iterations in 0.04 seconds (average 0.008800, setup 0.00)

Prob1A: 1 iterations in 0.01 seconds (average 0.012000, setup 0.00)

goal = 1, inf = 0, maybe = 257450

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=257451, nc=343457, nnz=699605, k=4] [10.3 MB]
Building sparse matrix (transition rewards)... [n=257451, nc=343457, nnz=8778, k=4] [2.4 MB]
Creating vector for state rewards... [2.0 MB]
Creating vector for inf... [2.0 MB]
Allocating iteration vectors... [2 x 2.0 MB]
TOTAL: [20.5 MB]

Starting iterations...

Iterative method: 98 iterations in 0.74 seconds (average 0.003755, setup 0.37)

Value in the initial state: 0.978120984292687

Time for model checking: 0.81 seconds.

Result: 0.978120984292687 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"charge"}min=? [ F "deadlock" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=0,Objy1=3,Objx2=6,Objy2=0

Prob0A: 4 iterations in 0.04 seconds (average 0.010000, setup 0.00)

Prob1E: 5 iterations in 0.08 seconds (average 0.015200, setup 0.00)

Warning: PRISM hasn't checked for zero-reward loops.
Your minimum rewards may be too low...

goal = 1, inf = 0, maybe = 257450

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=257451, nc=343457, nnz=699605, k=4] [10.3 MB]
Building sparse matrix (transition rewards)... [n=257451, nc=343457, nnz=8778, k=4] [2.4 MB]
Creating vector for state rewards... [2.0 MB]
Creating vector for inf... [2.0 MB]
Allocating iteration vectors... [2 x 2.0 MB]
TOTAL: [20.5 MB]

Starting iterations...

Iterative method: 104 iterations in 0.77 seconds (average 0.003769, setup 0.38)

Value in the initial state: 0.17424422378376933

Time for model checking: 0.896 seconds.

Result: 0.17424422378376933 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"time"}max=? [ F "deadlock" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=0,Objy1=3,Objx2=6,Objy2=0

Prob0E: 5 iterations in 0.05 seconds (average 0.010400, setup 0.00)

Prob1A: 1 iterations in 0.01 seconds (average 0.008000, setup 0.00)

goal = 1, inf = 0, maybe = 257450

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=257451, nc=343457, nnz=699605, k=4] [10.3 MB]
Building sparse matrix (transition rewards)... [n=257451, nc=343457, nnz=127490, k=4] [3.8 MB]
Creating vector for state rewards... [2.0 MB]
Creating vector for inf... [2.0 MB]
Allocating iteration vectors... [2 x 2.0 MB]
TOTAL: [21.9 MB]

Starting iterations...

Iterative method: 100 iterations in 0.79 seconds (average 0.003720, setup 0.42)

Value in the initial state: 110.22117304281299

Time for model checking: 0.874 seconds.

Result: 110.22117304281299 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"time"}min=? [ F "deadlock" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=0,Objy1=3,Objx2=6,Objy2=0

Prob0A: 4 iterations in 0.04 seconds (average 0.009000, setup 0.00)

Prob1E: 5 iterations in 0.08 seconds (average 0.015200, setup 0.00)

Warning: PRISM hasn't checked for zero-reward loops.
Your minimum rewards may be too low...

goal = 1, inf = 0, maybe = 257450

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=257451, nc=343457, nnz=699605, k=4] [10.3 MB]
Building sparse matrix (transition rewards)... [n=257451, nc=343457, nnz=127490, k=4] [3.8 MB]
Creating vector for state rewards... [2.0 MB]
Creating vector for inf... [2.0 MB]
Allocating iteration vectors... [2 x 2.0 MB]
TOTAL: [21.9 MB]

Starting iterations...

Iterative method: 109 iterations in 0.81 seconds (average 0.003780, setup 0.40)

Value in the initial state: 90.3260890872039

Time for model checking: 0.948 seconds.

Result: 90.3260890872039 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"success"}max=? [ F "deadlock" ]/Pmin=? [ F "MissionSuccessful" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=0,Objy1=3,Objx2=6,Objy2=0

Prob0E: 5 iterations in 0.04 seconds (average 0.007200, setup 0.00)

Prob1A: 1 iterations in 0.01 seconds (average 0.008000, setup 0.00)

goal = 1, inf = 0, maybe = 257450

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=257451, nc=343457, nnz=699605, k=4] [10.3 MB]
Building sparse matrix (transition rewards)... [n=257451, nc=343457, nnz=1917, k=4] [2.3 MB]
Creating vector for state rewards... [2.0 MB]
Creating vector for inf... [2.0 MB]
Allocating iteration vectors... [2 x 2.0 MB]
TOTAL: [20.5 MB]

Starting iterations...

Iterative method: 102 iterations in 0.70 seconds (average 0.003569, setup 0.34)

Prob0E: 77 iterations in 0.58 seconds (average 0.007532, setup 0.00)

Prob1A: 3 iterations in 0.05 seconds (average 0.016000, setup 0.00)

yes = 969, no = 218109, maybe = 38373

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=257451, nc=64861, nnz=178635, k=2] [2.4 MB]
Creating vector for yes... [2.0 MB]
Allocating iteration vectors... [2 x 2.0 MB]
TOTAL: [8.2 MB]

Starting iterations...

Iterative method: 99 iterations in 0.32 seconds (average 0.001535, setup 0.17)

Value in the initial state: 12956.909921249407

Time for model checking: 1.741 seconds.

Result: 12956.909921249407 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"success"}min=? [ F "deadlock" ]/Pmax=? [ F "MissionSuccessful" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=0,Objy1=3,Objx2=6,Objy2=0

Prob0A: 4 iterations in 0.12 seconds (average 0.029000, setup 0.00)

Prob1E: 5 iterations in 0.07 seconds (average 0.014400, setup 0.00)

Warning: PRISM hasn't checked for zero-reward loops.
Your minimum rewards may be too low...

goal = 1, inf = 0, maybe = 257450

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=257451, nc=343457, nnz=699605, k=4] [10.3 MB]
Building sparse matrix (transition rewards)... [n=257451, nc=343457, nnz=1917, k=4] [2.3 MB]
Creating vector for state rewards... [2.0 MB]
Creating vector for inf... [2.0 MB]
Allocating iteration vectors... [2 x 2.0 MB]
TOTAL: [20.5 MB]

Starting iterations...

Iterative method: 105 iterations in 0.88 seconds (average 0.003848, setup 0.47)

Prob0A: 60 iterations in 0.31 seconds (average 0.005133, setup 0.00)

Prob1E: 4 iterations in 0.03 seconds (average 0.008000, setup 0.00)

yes = 969, no = 209144, maybe = 47338

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=257451, nc=80345, nnz=221985, k=2] [2.9 MB]
Creating vector for yes... [2.0 MB]
Allocating iteration vectors... [2 x 2.0 MB]
TOTAL: [8.8 MB]

Starting iterations...

Iterative method: 102 iterations in 0.32 seconds (average 0.001608, setup 0.16)

Value in the initial state: 2.831016565278953

Time for model checking: 1.723 seconds.

Result: 2.831016565278953 (value in the initial state)

---------------------------------------------------------------------

Note: There were 16 warnings during computation.

