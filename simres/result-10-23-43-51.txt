PRISM
=====

Version: 4.3
Date: Wed Aug 03 16:16:54 BST 2016
Hostname: eglinton
Memory limits: cudd=1g, java(heap)=981.5m
Command line: prism uav_sim_mdp.prism -s uav_mdp.props -const 'Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=4,Objy1=3,Objx2=5,Objy2=1,'

Parsing model file "uav_sim_mdp.prism"...

Parsing properties file "uav_mdp.props"...

18 properties:
(1) Pmax=? [ F "MissionSuccessful" ]
(2) Pmin=? [ F "MissionSuccessful" ]
(3) Pmax=? [ F "MissionTimeUp" ]
(4) Pmin=? [ F "MissionTimeUp" ]
(5) Pmax=? [ F "Crash" ]
(6) Pmin=? [ F "Crash" ]
(7) Pmax=? [ F "AllAreaSearched" ]
(8) Pmin=? [ F "AllAreaSearched" ]
(9) Pmax=? [ F "MissionTimeUp"|"AllAreaSearched"|"Crash" ]
(10) Pmin=? [ F "MissionTimeUp"|"AllAreaSearched"|"Crash" ]
(11) R{"drop"}max=? [ F "deadlock" ]
(12) R{"drop"}min=? [ F "deadlock" ]
(13) R{"charge"}max=? [ F "deadlock" ]
(14) R{"charge"}min=? [ F "deadlock" ]
(15) R{"time"}max=? [ F "deadlock" ]
(16) R{"time"}min=? [ F "deadlock" ]
(17) R{"success"}max=? [ F "deadlock" ]/Pmin=? [ F "MissionSuccessful" ]
(18) R{"success"}min=? [ F "deadlock" ]/Pmax=? [ F "MissionSuccessful" ]

Type:        MDP
Modules:     Obj Move Robot T 
Variables:   NoOfObjs posx posy s1 retx rety b t c 

---------------------------------------------------------------------

Model checking: Pmax=? [ F "MissionSuccessful" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=4,Objy1=3,Objx2=5,Objy2=1

Building model...
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=4,Objy1=3,Objx2=5,Objy2=1

Warning: Guard for command 7 of module "Move" is never satisfied.

Warning: Guard for command 8 of module "Move" is never satisfied.

Warning: Guard for command 9 of module "Move" is never satisfied.

Warning: Guard for command 7 of module "Robot" is never satisfied.

Warning: Update 2 of command 3 of module "Robot" doesn't do anything ("(s1'=13)", line 210, column 44)

Warning: Update 2 of command 27 of module "T" doesn't do anything ("(c'=1)", line 323, column 108)

Warning: Guard for command 28 of module "T" is never satisfied.

Warning: Update 2 of command 9 of module "T" doesn't do anything ("(c'=1)", line 300, column 112)

Warning: Guard for command 10 of module "T" is never satisfied.

Warning: Update 2 of command 51 of module "T" doesn't do anything ("(c'=1)", line 354, column 113)

Warning: Guard for command 52 of module "T" is never satisfied.

Computing reachable states...

Reachability (BFS): 89 iterations in 1.13 seconds (average 0.012674, setup 0.00)

Time for model construction: 2.609 seconds.

Warning: Deadlocks detected and fixed in 1 states

Type:        MDP
States:      254646 (1 initial)
Transitions: 677316
Choices:     337907

Transition matrix: 215070 nodes (91 terminal), 677316 minterms, vars: 34r/34c/18nd

Prob0A: 55 iterations in 0.39 seconds (average 0.007127, setup 0.00)

Prob1E: 4 iterations in 0.06 seconds (average 0.014000, setup 0.00)

yes = 1147, no = 212955, maybe = 40544

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=254646, nc=69448, nnz=194540, k=2] [2.5 MB]
Creating vector for yes... [1.9 MB]
Allocating iteration vectors... [2 x 1.9 MB]
TOTAL: [8.4 MB]

Starting iterations...

Iterative method: 99 iterations in 0.40 seconds (average 0.001778, setup 0.22)

Value in the initial state: 0.9806114978592407

Time for model checking: 0.879 seconds.

Result: 0.9806114978592407 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmin=? [ F "MissionSuccessful" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=4,Objy1=3,Objx2=5,Objy2=1

Prob0E: 57 iterations in 0.57 seconds (average 0.009965, setup 0.00)

Prob1A: 3 iterations in 0.06 seconds (average 0.020000, setup 0.00)

yes = 1147, no = 219303, maybe = 34196

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=254646, nc=58467, nnz=162919, k=2] [2.2 MB]
Creating vector for yes... [1.9 MB]
Allocating iteration vectors... [2 x 1.9 MB]
TOTAL: [8.0 MB]

Starting iterations...

Iterative method: 92 iterations in 0.36 seconds (average 0.001652, setup 0.21)

Value in the initial state: 0.7465592156043828

Time for model checking: 1.007 seconds.

Result: 0.7465592156043828 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmax=? [ F "MissionTimeUp" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=4,Objy1=3,Objx2=5,Objy2=1

Prob0A: 71 iterations in 0.62 seconds (average 0.008732, setup 0.00)

Prob1E: 4 iterations in 0.04 seconds (average 0.010000, setup 0.00)

yes = 376, no = 148654, maybe = 105616

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=254646, nc=175824, nnz=477265, k=4] [5.9 MB]
Creating vector for yes... [1.9 MB]
Allocating iteration vectors... [2 x 1.9 MB]
TOTAL: [11.7 MB]

Starting iterations...

Iterative method: 99 iterations in 0.51 seconds (average 0.002182, setup 0.30)

Value in the initial state: 4.492480872355986E-4

Time for model checking: 1.191 seconds.

Result: 4.492480872355986E-4 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmin=? [ F "MissionTimeUp" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=4,Objy1=3,Objx2=5,Objy2=1

Prob0E: 85 iterations in 0.84 seconds (average 0.009929, setup 0.00)

Prob1A: 3 iterations in 0.05 seconds (average 0.016000, setup 0.00)

yes = 376, no = 148963, maybe = 105307

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=254646, nc=175277, nnz=476154, k=4] [5.9 MB]
Creating vector for yes... [1.9 MB]
Allocating iteration vectors... [2 x 1.9 MB]
TOTAL: [11.7 MB]

Starting iterations...

Iterative method: 115 iterations in 0.53 seconds (average 0.002191, setup 0.28)

Value in the initial state: 7.293517199802451E-7

Time for model checking: 1.454 seconds.

Result: 7.293517199802451E-7 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmax=? [ F "Crash" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=4,Objy1=3,Objx2=5,Objy2=1

Prob0A: 3 iterations in 0.08 seconds (average 0.026667, setup 0.00)

Prob1E: 204 iterations in 2.42 seconds (average 0.011882, setup 0.00)

yes = 124804, no = 3206, maybe = 126636

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=254646, nc=209897, nnz=549306, k=4] [6.7 MB]
Creating vector for yes... [1.9 MB]
Allocating iteration vectors... [2 x 1.9 MB]
TOTAL: [12.6 MB]

Starting iterations...

Iterative method: 95 iterations in 0.56 seconds (average 0.002316, setup 0.34)

Value in the initial state: 0.031006531544013952

Time for model checking: 3.091 seconds.

Result: 0.031006531544013952 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmin=? [ F "Crash" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=4,Objy1=3,Objx2=5,Objy2=1

Prob0E: 3 iterations in 0.07 seconds (average 0.024000, setup 0.00)

Prob1A: 49 iterations in 0.48 seconds (average 0.009878, setup 0.00)

yes = 124804, no = 3206, maybe = 126636

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=254646, nc=209897, nnz=549306, k=4] [6.7 MB]
Creating vector for yes... [1.9 MB]
Allocating iteration vectors... [2 x 1.9 MB]
TOTAL: [12.6 MB]

Starting iterations...

Iterative method: 91 iterations in 0.49 seconds (average 0.002418, setup 0.27)

Value in the initial state: 0.01732825311134309

Time for model checking: 1.07 seconds.

Result: 0.01732825311134309 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmax=? [ F "AllAreaSearched" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=4,Objy1=3,Objx2=5,Objy2=1

Prob0A: 56 iterations in 0.40 seconds (average 0.007143, setup 0.00)

Prob1E: 4 iterations in 0.15 seconds (average 0.037000, setup 0.00)

yes = 1682, no = 192641, maybe = 60323

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=254646, nc=97940, nnz=263886, k=2] [3.4 MB]
Creating vector for yes... [1.9 MB]
Allocating iteration vectors... [2 x 1.9 MB]
TOTAL: [9.2 MB]

Starting iterations...

Iterative method: 86 iterations in 0.36 seconds (average 0.001860, setup 0.20)

Value in the initial state: 0.2249289732120085

Time for model checking: 0.94 seconds.

Result: 0.2249289732120085 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmin=? [ F "AllAreaSearched" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=4,Objy1=3,Objx2=5,Objy2=1

Prob0E: 63 iterations in 0.39 seconds (average 0.006159, setup 0.00)

Prob1A: 3 iterations in 0.04 seconds (average 0.012000, setup 0.00)

yes = 1682, no = 200048, maybe = 52916

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=254646, nc=85916, nnz=230021, k=2] [3.0 MB]
Creating vector for yes... [1.9 MB]
Allocating iteration vectors... [2 x 1.9 MB]
TOTAL: [8.8 MB]

Starting iterations...

Iterative method: 96 iterations in 0.32 seconds (average 0.001792, setup 0.14)

Value in the initial state: 0.0017908422252018463

Time for model checking: 0.757 seconds.

Result: 0.0017908422252018463 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmax=? [ F "MissionTimeUp"|"AllAreaSearched"|"Crash" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=4,Objy1=3,Objx2=5,Objy2=1

Prob0A: 3 iterations in 0.08 seconds (average 0.028000, setup 0.00)

Prob1E: 220 iterations in 2.86 seconds (average 0.012982, setup 0.00)

yes = 219302, no = 1148, maybe = 34196

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=254646, nc=58467, nnz=162919, k=2] [2.2 MB]
Creating vector for yes... [1.9 MB]
Allocating iteration vectors... [2 x 1.9 MB]
TOTAL: [8.0 MB]

Starting iterations...

Iterative method: 86 iterations in 0.33 seconds (average 0.001581, setup 0.20)

Value in the initial state: 0.25344031329556166

Time for model checking: 3.294 seconds.

Result: 0.25344031329556166 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmin=? [ F "MissionTimeUp"|"AllAreaSearched"|"Crash" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=4,Objy1=3,Objx2=5,Objy2=1

Prob0E: 3 iterations in 0.08 seconds (average 0.025333, setup 0.00)

Prob1A: 55 iterations in 0.54 seconds (average 0.009891, setup 0.00)

yes = 212954, no = 1148, maybe = 40544

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=254646, nc=69448, nnz=194540, k=2] [2.5 MB]
Creating vector for yes... [1.9 MB]
Allocating iteration vectors... [2 x 1.9 MB]
TOTAL: [8.4 MB]

Starting iterations...

Iterative method: 98 iterations in 0.36 seconds (average 0.001714, setup 0.19)

Value in the initial state: 0.019387323541464385

Time for model checking: 1.002 seconds.

Result: 0.019387323541464385 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"drop"}max=? [ F "deadlock" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=4,Objy1=3,Objx2=5,Objy2=1

Prob0E: 4 iterations in 0.08 seconds (average 0.021000, setup 0.00)

Prob1A: 1 iterations in 0.00 seconds (average 0.004000, setup 0.00)

goal = 1, inf = 0, maybe = 254645

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=254646, nc=337906, nnz=677315, k=4] [10.0 MB]
Building sparse matrix (transition rewards)... [n=254646, nc=337906, nnz=0, k=4] [2.3 MB]
Creating vector for state rewards... [1.9 MB]
Creating vector for inf... [1.9 MB]
Allocating iteration vectors... [2 x 1.9 MB]
TOTAL: [20.0 MB]

Starting iterations...

Iterative method: 96 iterations in 0.79 seconds (average 0.003625, setup 0.44)

Value in the initial state: 0.2116574025450502

Time for model checking: 0.897 seconds.

Result: 0.2116574025450502 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"drop"}min=? [ F "deadlock" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=4,Objy1=3,Objx2=5,Objy2=1

Prob0A: 4 iterations in 0.04 seconds (average 0.011000, setup 0.00)

Prob1E: 5 iterations in 0.05 seconds (average 0.009600, setup 0.00)

Warning: PRISM hasn't checked for zero-reward loops.
Your minimum rewards may be too low...

goal = 1, inf = 0, maybe = 254645

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=254646, nc=337906, nnz=677315, k=4] [10.0 MB]
Building sparse matrix (transition rewards)... [n=254646, nc=337906, nnz=0, k=4] [2.3 MB]
Creating vector for state rewards... [1.9 MB]
Creating vector for inf... [1.9 MB]
Allocating iteration vectors... [2 x 1.9 MB]
TOTAL: [20.0 MB]

Starting iterations...

Iterative method: 87 iterations in 0.66 seconds (average 0.003632, setup 0.34)

Value in the initial state: 0.1866257963695104

Time for model checking: 0.763 seconds.

Result: 0.1866257963695104 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"charge"}max=? [ F "deadlock" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=4,Objy1=3,Objx2=5,Objy2=1

Prob0E: 4 iterations in 0.03 seconds (average 0.007000, setup 0.00)

Prob1A: 1 iterations in 0.00 seconds (average 0.004000, setup 0.00)

goal = 1, inf = 0, maybe = 254645

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=254646, nc=337906, nnz=677315, k=4] [10.0 MB]
Building sparse matrix (transition rewards)... [n=254646, nc=337906, nnz=7628, k=4] [2.3 MB]
Creating vector for state rewards... [1.9 MB]
Creating vector for inf... [1.9 MB]
Allocating iteration vectors... [2 x 1.9 MB]
TOTAL: [20.1 MB]

Starting iterations...

Iterative method: 93 iterations in 0.68 seconds (average 0.003570, setup 0.34)

Value in the initial state: 0.29276443061826973

Time for model checking: 0.722 seconds.

Result: 0.29276443061826973 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"charge"}min=? [ F "deadlock" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=4,Objy1=3,Objx2=5,Objy2=1

Prob0A: 4 iterations in 0.03 seconds (average 0.007000, setup 0.00)

Prob1E: 5 iterations in 0.04 seconds (average 0.007200, setup 0.00)

Warning: PRISM hasn't checked for zero-reward loops.
Your minimum rewards may be too low...

goal = 1, inf = 0, maybe = 254645

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=254646, nc=337906, nnz=677315, k=4] [10.0 MB]
Building sparse matrix (transition rewards)... [n=254646, nc=337906, nnz=7628, k=4] [2.3 MB]
Creating vector for state rewards... [1.9 MB]
Creating vector for inf... [1.9 MB]
Allocating iteration vectors... [2 x 1.9 MB]
TOTAL: [20.1 MB]

Starting iterations...

Iterative method: 101 iterations in 0.70 seconds (average 0.003604, setup 0.33)

Value in the initial state: 0.048087501356889606

Time for model checking: 0.775 seconds.

Result: 0.048087501356889606 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"time"}max=? [ F "deadlock" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=4,Objy1=3,Objx2=5,Objy2=1

Prob0E: 4 iterations in 0.02 seconds (average 0.006000, setup 0.00)

Prob1A: 1 iterations in 0.00 seconds (average 0.004000, setup 0.00)

goal = 1, inf = 0, maybe = 254645

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=254646, nc=337906, nnz=677315, k=4] [10.0 MB]
Building sparse matrix (transition rewards)... [n=254646, nc=337906, nnz=128008, k=4] [3.7 MB]
Creating vector for state rewards... [1.9 MB]
Creating vector for inf... [1.9 MB]
Allocating iteration vectors... [2 x 1.9 MB]
TOTAL: [21.5 MB]

Starting iterations...

Iterative method: 98 iterations in 0.82 seconds (average 0.004408, setup 0.39)

Value in the initial state: 171.10335601092146

Time for model checking: 0.866 seconds.

Result: 171.10335601092146 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"time"}min=? [ F "deadlock" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=4,Objy1=3,Objx2=5,Objy2=1

Prob0A: 4 iterations in 0.02 seconds (average 0.006000, setup 0.00)

Prob1E: 5 iterations in 0.04 seconds (average 0.008800, setup 0.00)

Warning: PRISM hasn't checked for zero-reward loops.
Your minimum rewards may be too low...

goal = 1, inf = 0, maybe = 254645

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=254646, nc=337906, nnz=677315, k=4] [10.0 MB]
Building sparse matrix (transition rewards)... [n=254646, nc=337906, nnz=128008, k=4] [3.7 MB]
Creating vector for state rewards... [1.9 MB]
Creating vector for inf... [1.9 MB]
Allocating iteration vectors... [2 x 1.9 MB]
TOTAL: [21.5 MB]

Starting iterations...

Iterative method: 105 iterations in 0.76 seconds (average 0.003467, setup 0.40)

Value in the initial state: 96.04503219335948

Time for model checking: 0.847 seconds.

Result: 96.04503219335948 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"success"}max=? [ F "deadlock" ]/Pmin=? [ F "MissionSuccessful" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=4,Objy1=3,Objx2=5,Objy2=1

Prob0E: 4 iterations in 0.02 seconds (average 0.006000, setup 0.00)

Prob1A: 1 iterations in 0.01 seconds (average 0.008000, setup 0.00)

goal = 1, inf = 0, maybe = 254645

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=254646, nc=337906, nnz=677315, k=4] [10.0 MB]
Building sparse matrix (transition rewards)... [n=254646, nc=337906, nnz=2260, k=4] [2.3 MB]
Creating vector for state rewards... [1.9 MB]
Creating vector for inf... [1.9 MB]
Allocating iteration vectors... [2 x 1.9 MB]
TOTAL: [20.1 MB]

Starting iterations...

Iterative method: 101 iterations in 0.65 seconds (average 0.003327, setup 0.32)

Prob0E: 57 iterations in 0.39 seconds (average 0.006807, setup 0.00)

Prob1A: 3 iterations in 0.04 seconds (average 0.013333, setup 0.00)

yes = 1147, no = 219303, maybe = 34196

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=254646, nc=58467, nnz=162919, k=2] [2.2 MB]
Creating vector for yes... [1.9 MB]
Allocating iteration vectors... [2 x 1.9 MB]
TOTAL: [8.0 MB]

Starting iterations...

Iterative method: 92 iterations in 0.29 seconds (average 0.001478, setup 0.15)

Value in the initial state: 224.74392006194222

Time for model checking: 1.426 seconds.

Result: 224.74392006194222 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"success"}min=? [ F "deadlock" ]/Pmax=? [ F "MissionSuccessful" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=4,Objy1=3,Objx2=5,Objy2=1

Prob0A: 4 iterations in 0.10 seconds (average 0.026000, setup 0.00)

Prob1E: 5 iterations in 0.04 seconds (average 0.008800, setup 0.00)

Warning: PRISM hasn't checked for zero-reward loops.
Your minimum rewards may be too low...

goal = 1, inf = 0, maybe = 254645

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=254646, nc=337906, nnz=677315, k=4] [10.0 MB]
Building sparse matrix (transition rewards)... [n=254646, nc=337906, nnz=2260, k=4] [2.3 MB]
Creating vector for state rewards... [1.9 MB]
Creating vector for inf... [1.9 MB]
Allocating iteration vectors... [2 x 1.9 MB]
TOTAL: [20.1 MB]

Starting iterations...

Iterative method: 98 iterations in 0.68 seconds (average 0.003347, setup 0.35)

Prob0A: 55 iterations in 0.25 seconds (average 0.004509, setup 0.00)

Prob1E: 4 iterations in 0.03 seconds (average 0.008000, setup 0.00)

yes = 1147, no = 212955, maybe = 40544

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=254646, nc=69448, nnz=194540, k=2] [2.5 MB]
Creating vector for yes... [1.9 MB]
Allocating iteration vectors... [2 x 1.9 MB]
TOTAL: [8.4 MB]

Starting iterations...

Iterative method: 99 iterations in 0.32 seconds (average 0.001657, setup 0.16)

Value in the initial state: 79.20257769236241

Time for model checking: 1.428 seconds.

Result: 79.20257769236241 (value in the initial state)

---------------------------------------------------------------------

Note: There were 16 warnings during computation.

