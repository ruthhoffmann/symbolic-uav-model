PRISM
=====

Version: 4.3
Date: Wed Aug 03 15:58:45 BST 2016
Hostname: eglinton
Memory limits: cudd=1g, java(heap)=981.5m
Command line: prism uav_sim_mdp.prism -s uav_mdp.props -const 'Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=3,Objy1=0,Objx2=6,Objy2=1,'

Parsing model file "uav_sim_mdp.prism"...

Parsing properties file "uav_mdp.props"...

18 properties:
(1) Pmax=? [ F "MissionSuccessful" ]
(2) Pmin=? [ F "MissionSuccessful" ]
(3) Pmax=? [ F "MissionTimeUp" ]
(4) Pmin=? [ F "MissionTimeUp" ]
(5) Pmax=? [ F "Crash" ]
(6) Pmin=? [ F "Crash" ]
(7) Pmax=? [ F "AllAreaSearched" ]
(8) Pmin=? [ F "AllAreaSearched" ]
(9) Pmax=? [ F "MissionTimeUp"|"AllAreaSearched"|"Crash" ]
(10) Pmin=? [ F "MissionTimeUp"|"AllAreaSearched"|"Crash" ]
(11) R{"drop"}max=? [ F "deadlock" ]
(12) R{"drop"}min=? [ F "deadlock" ]
(13) R{"charge"}max=? [ F "deadlock" ]
(14) R{"charge"}min=? [ F "deadlock" ]
(15) R{"time"}max=? [ F "deadlock" ]
(16) R{"time"}min=? [ F "deadlock" ]
(17) R{"success"}max=? [ F "deadlock" ]/Pmin=? [ F "MissionSuccessful" ]
(18) R{"success"}min=? [ F "deadlock" ]/Pmax=? [ F "MissionSuccessful" ]

Type:        MDP
Modules:     Obj Move Robot T 
Variables:   NoOfObjs posx posy s1 retx rety b t c 

---------------------------------------------------------------------

Model checking: Pmax=? [ F "MissionSuccessful" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=3,Objy1=0,Objx2=6,Objy2=1

Building model...
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=3,Objy1=0,Objx2=6,Objy2=1

Warning: Guard for command 7 of module "Move" is never satisfied.

Warning: Guard for command 8 of module "Move" is never satisfied.

Warning: Guard for command 9 of module "Move" is never satisfied.

Warning: Guard for command 7 of module "Robot" is never satisfied.

Warning: Update 2 of command 3 of module "Robot" doesn't do anything ("(s1'=13)", line 210, column 44)

Warning: Update 2 of command 27 of module "T" doesn't do anything ("(c'=1)", line 323, column 108)

Warning: Guard for command 28 of module "T" is never satisfied.

Warning: Update 2 of command 9 of module "T" doesn't do anything ("(c'=1)", line 300, column 112)

Warning: Guard for command 10 of module "T" is never satisfied.

Warning: Update 2 of command 51 of module "T" doesn't do anything ("(c'=1)", line 354, column 113)

Warning: Guard for command 52 of module "T" is never satisfied.

Computing reachable states...

Reachability (BFS): 90 iterations in 1.64 seconds (average 0.018267, setup 0.00)

Time for model construction: 3.703 seconds.

Warning: Deadlocks detected and fixed in 1 states

Type:        MDP
States:      333749 (1 initial)
Transitions: 901906
Choices:     443493

Transition matrix: 233257 nodes (97 terminal), 901906 minterms, vars: 34r/34c/18nd

Prob0A: 48 iterations in 0.52 seconds (average 0.010917, setup 0.00)

Prob1E: 4 iterations in 0.07 seconds (average 0.018000, setup 0.00)

yes = 1162, no = 282810, maybe = 49777

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=333749, nc=89631, nnz=262551, k=2] [3.4 MB]
Creating vector for yes... [2.5 MB]
Allocating iteration vectors... [2 x 2.5 MB]
TOTAL: [11.0 MB]

Starting iterations...

Iterative method: 93 iterations in 0.58 seconds (average 0.002882, setup 0.31)

Value in the initial state: 0.9844335366595791

Time for model checking: 1.225 seconds.

Result: 0.9844335366595791 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmin=? [ F "MissionSuccessful" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=3,Objy1=0,Objx2=6,Objy2=1

Prob0E: 48 iterations in 0.62 seconds (average 0.012917, setup 0.00)

Prob1A: 3 iterations in 0.06 seconds (average 0.020000, setup 0.00)

yes = 1162, no = 290216, maybe = 42371

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=333749, nc=75904, nnz=222389, k=2] [2.9 MB]
Creating vector for yes... [2.5 MB]
Allocating iteration vectors... [2 x 2.5 MB]
TOTAL: [10.6 MB]

Starting iterations...

Iterative method: 83 iterations in 0.38 seconds (average 0.002169, setup 0.20)

Value in the initial state: 0.906089491272516

Time for model checking: 1.078 seconds.

Result: 0.906089491272516 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmax=? [ F "MissionTimeUp" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=3,Objy1=0,Objx2=6,Objy2=1

Prob0A: 67 iterations in 0.76 seconds (average 0.011343, setup 0.00)

Prob1E: 4 iterations in 0.05 seconds (average 0.013000, setup 0.00)

yes = 544, no = 202897, maybe = 130308

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=333749, nc=228303, nnz=636115, k=4] [7.8 MB]
Creating vector for yes... [2.5 MB]
Allocating iteration vectors... [2 x 2.5 MB]
TOTAL: [15.5 MB]

Starting iterations...

Iterative method: 99 iterations in 0.67 seconds (average 0.002949, setup 0.38)

Value in the initial state: 5.933726707560165E-4

Time for model checking: 1.523 seconds.

Result: 5.933726707560165E-4 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmin=? [ F "MissionTimeUp" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=3,Objy1=0,Objx2=6,Objy2=1

Prob0E: 83 iterations in 1.16 seconds (average 0.014024, setup 0.00)

Prob1A: 3 iterations in 0.07 seconds (average 0.024000, setup 0.00)

yes = 544, no = 203441, maybe = 129764

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=333749, nc=227319, nnz=634199, k=4] [7.8 MB]
Creating vector for yes... [2.5 MB]
Allocating iteration vectors... [2 x 2.5 MB]
TOTAL: [15.4 MB]

Starting iterations...

Iterative method: 114 iterations in 0.72 seconds (average 0.003053, setup 0.37)

Value in the initial state: 6.269763430307966E-7

Time for model checking: 1.989 seconds.

Result: 6.269763430307966E-7 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmax=? [ F "Crash" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=3,Objy1=0,Objx2=6,Objy2=1

Prob0A: 3 iterations in 0.14 seconds (average 0.048000, setup 0.00)

Prob1E: 182 iterations in 3.87 seconds (average 0.021253, setup 0.00)

yes = 163115, no = 3144, maybe = 167490

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=333749, nc=277234, nnz=735647, k=4] [9.0 MB]
Creating vector for yes... [2.5 MB]
Allocating iteration vectors... [2 x 2.5 MB]
TOTAL: [16.6 MB]

Starting iterations...

Iterative method: 92 iterations in 0.77 seconds (average 0.003217, setup 0.47)

Value in the initial state: 0.02627288957568334

Time for model checking: 4.825 seconds.

Result: 0.02627288957568334 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmin=? [ F "Crash" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=3,Objy1=0,Objx2=6,Objy2=1

Prob0E: 4 iterations in 0.10 seconds (average 0.024000, setup 0.00)

Prob1A: 44 iterations in 0.64 seconds (average 0.014455, setup 0.00)

yes = 163115, no = 3144, maybe = 167490

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=333749, nc=277234, nnz=735647, k=4] [9.0 MB]
Creating vector for yes... [2.5 MB]
Allocating iteration vectors... [2 x 2.5 MB]
TOTAL: [16.6 MB]

Starting iterations...

Iterative method: 85 iterations in 0.59 seconds (average 0.003294, setup 0.31)

Value in the initial state: 0.013510016065127271

Time for model checking: 1.347 seconds.

Result: 0.013510016065127271 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmax=? [ F "AllAreaSearched" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=3,Objy1=0,Objx2=6,Objy2=1

Prob0A: 63 iterations in 0.55 seconds (average 0.008698, setup 0.00)

Prob1E: 4 iterations in 0.06 seconds (average 0.014000, setup 0.00)

yes = 1437, no = 267551, maybe = 64761

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=333749, nc=96826, nnz=254816, k=2] [3.3 MB]
Creating vector for yes... [2.5 MB]
Allocating iteration vectors... [2 x 2.5 MB]
TOTAL: [11.0 MB]

Starting iterations...

Iterative method: 96 iterations in 0.44 seconds (average 0.002500, setup 0.20)

Value in the initial state: 0.07177608129307268

Time for model checking: 1.059 seconds.

Result: 0.07177608129307268 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmin=? [ F "AllAreaSearched" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=3,Objy1=0,Objx2=6,Objy2=1

Prob0E: 67 iterations in 0.56 seconds (average 0.008358, setup 0.00)

Prob1A: 3 iterations in 0.04 seconds (average 0.012000, setup 0.00)

yes = 1437, no = 275810, maybe = 56502

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=333749, nc=81947, nnz=211798, k=2] [2.8 MB]
Creating vector for yes... [2.5 MB]
Allocating iteration vectors... [2 x 2.5 MB]
TOTAL: [10.5 MB]

Starting iterations...

Iterative method: 97 iterations in 0.36 seconds (average 0.002309, setup 0.14)

Value in the initial state: 0.0017970373284561306

Time for model checking: 0.948 seconds.

Result: 0.0017970373284561306 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmax=? [ F "MissionTimeUp"|"AllAreaSearched"|"Crash" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=3,Objy1=0,Objx2=6,Objy2=1

Prob0A: 3 iterations in 0.09 seconds (average 0.030667, setup 0.00)

Prob1E: 184 iterations in 3.92 seconds (average 0.021304, setup 0.00)

yes = 290215, no = 1163, maybe = 42371

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=333749, nc=75904, nnz=222389, k=2] [2.9 MB]
Creating vector for yes... [2.5 MB]
Allocating iteration vectors... [2 x 2.5 MB]
TOTAL: [10.6 MB]

Starting iterations...

Iterative method: 78 iterations in 0.42 seconds (average 0.002256, setup 0.24)

Value in the initial state: 0.09391033849666033

Time for model checking: 4.474 seconds.

Result: 0.09391033849666033 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmin=? [ F "MissionTimeUp"|"AllAreaSearched"|"Crash" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=3,Objy1=0,Objx2=6,Objy2=1

Prob0E: 4 iterations in 0.07 seconds (average 0.018000, setup 0.00)

Prob1A: 48 iterations in 0.54 seconds (average 0.011250, setup 0.00)

yes = 282809, no = 1163, maybe = 49777

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=333749, nc=89631, nnz=262551, k=2] [3.4 MB]
Creating vector for yes... [2.5 MB]
Allocating iteration vectors... [2 x 2.5 MB]
TOTAL: [11.0 MB]

Starting iterations...

Iterative method: 90 iterations in 0.40 seconds (average 0.002400, setup 0.18)

Value in the initial state: 0.01556545906338561

Time for model checking: 1.035 seconds.

Result: 0.01556545906338561 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"drop"}max=? [ F "deadlock" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=3,Objy1=0,Objx2=6,Objy2=1

Prob0E: 5 iterations in 0.06 seconds (average 0.012800, setup 0.00)

Prob1A: 1 iterations in 0.01 seconds (average 0.012000, setup 0.00)

goal = 1, inf = 0, maybe = 333748

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=333749, nc=443492, nnz=901905, k=4] [13.3 MB]
Building sparse matrix (transition rewards)... [n=333749, nc=443492, nnz=0, k=4] [3.0 MB]
Creating vector for state rewards... [2.5 MB]
Creating vector for inf... [2.5 MB]
Allocating iteration vectors... [2 x 2.5 MB]
TOTAL: [26.4 MB]

Starting iterations...

Iterative method: 86 iterations in 0.98 seconds (average 0.004930, setup 0.55)

Value in the initial state: 0.27301428717647674

Time for model checking: 1.068 seconds.

Result: 0.27301428717647674 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"drop"}min=? [ F "deadlock" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=3,Objy1=0,Objx2=6,Objy2=1

Prob0A: 4 iterations in 0.04 seconds (average 0.010000, setup 0.00)

Prob1E: 5 iterations in 0.07 seconds (average 0.013600, setup 0.00)

Warning: PRISM hasn't checked for zero-reward loops.
Your minimum rewards may be too low...

goal = 1, inf = 0, maybe = 333748

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=333749, nc=443492, nnz=901905, k=4] [13.3 MB]
Building sparse matrix (transition rewards)... [n=333749, nc=443492, nnz=0, k=4] [3.0 MB]
Creating vector for state rewards... [2.5 MB]
Creating vector for inf... [2.5 MB]
Allocating iteration vectors... [2 x 2.5 MB]
TOTAL: [26.4 MB]

Starting iterations...

Iterative method: 76 iterations in 0.84 seconds (average 0.004895, setup 0.46)

Value in the initial state: 0.24222470767525278

Time for model checking: 0.956 seconds.

Result: 0.24222470767525278 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"charge"}max=? [ F "deadlock" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=3,Objy1=0,Objx2=6,Objy2=1

Prob0E: 5 iterations in 0.04 seconds (average 0.008000, setup 0.00)

Prob1A: 1 iterations in 0.01 seconds (average 0.008000, setup 0.00)

goal = 1, inf = 0, maybe = 333748

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=333749, nc=443492, nnz=901905, k=4] [13.3 MB]
Building sparse matrix (transition rewards)... [n=333749, nc=443492, nnz=10827, k=4] [3.1 MB]
Creating vector for state rewards... [2.5 MB]
Creating vector for inf... [2.5 MB]
Allocating iteration vectors... [2 x 2.5 MB]
TOTAL: [26.6 MB]

Starting iterations...

Iterative method: 83 iterations in 0.87 seconds (average 0.004916, setup 0.46)

Value in the initial state: 0.22631406294927284

Time for model checking: 0.937 seconds.

Result: 0.22631406294927284 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"charge"}min=? [ F "deadlock" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=3,Objy1=0,Objx2=6,Objy2=1

Prob0A: 4 iterations in 0.04 seconds (average 0.010000, setup 0.00)

Prob1E: 5 iterations in 0.06 seconds (average 0.012800, setup 0.00)

Warning: PRISM hasn't checked for zero-reward loops.
Your minimum rewards may be too low...

goal = 1, inf = 0, maybe = 333748

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=333749, nc=443492, nnz=901905, k=4] [13.3 MB]
Building sparse matrix (transition rewards)... [n=333749, nc=443492, nnz=10827, k=4] [3.1 MB]
Creating vector for state rewards... [2.5 MB]
Creating vector for inf... [2.5 MB]
Allocating iteration vectors... [2 x 2.5 MB]
TOTAL: [26.6 MB]

Starting iterations...

Iterative method: 96 iterations in 0.94 seconds (average 0.004958, setup 0.46)

Value in the initial state: 0.0152737295793682

Time for model checking: 1.06 seconds.

Result: 0.0152737295793682 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"time"}max=? [ F "deadlock" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=3,Objy1=0,Objx2=6,Objy2=1

Prob0E: 5 iterations in 0.04 seconds (average 0.008800, setup 0.00)

Prob1A: 1 iterations in 0.01 seconds (average 0.008000, setup 0.00)

goal = 1, inf = 0, maybe = 333748

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=333749, nc=443492, nnz=901905, k=4] [13.3 MB]
Building sparse matrix (transition rewards)... [n=333749, nc=443492, nnz=166257, k=4] [4.9 MB]
Creating vector for state rewards... [2.5 MB]
Creating vector for inf... [2.5 MB]
Allocating iteration vectors... [2 x 2.5 MB]
TOTAL: [28.3 MB]

Starting iterations...

Iterative method: 96 iterations in 0.97 seconds (average 0.004917, setup 0.50)

Value in the initial state: 145.0512838309539

Time for model checking: 1.051 seconds.

Result: 145.0512838309539 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"time"}min=? [ F "deadlock" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=3,Objy1=0,Objx2=6,Objy2=1

Prob0A: 4 iterations in 0.03 seconds (average 0.007000, setup 0.00)

Prob1E: 5 iterations in 0.05 seconds (average 0.009600, setup 0.00)

Warning: PRISM hasn't checked for zero-reward loops.
Your minimum rewards may be too low...

goal = 1, inf = 0, maybe = 333748

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=333749, nc=443492, nnz=901905, k=4] [13.3 MB]
Building sparse matrix (transition rewards)... [n=333749, nc=443492, nnz=166257, k=4] [4.9 MB]
Creating vector for state rewards... [2.5 MB]
Creating vector for inf... [2.5 MB]
Allocating iteration vectors... [2 x 2.5 MB]
TOTAL: [28.3 MB]

Starting iterations...

Iterative method: 98 iterations in 1.00 seconds (average 0.005102, setup 0.50)

Value in the initial state: 74.97008433689112

Time for model checking: 1.092 seconds.

Result: 74.97008433689112 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"success"}max=? [ F "deadlock" ]/Pmin=? [ F "MissionSuccessful" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=3,Objy1=0,Objx2=6,Objy2=1

Prob0E: 5 iterations in 0.04 seconds (average 0.007200, setup 0.00)

Prob1A: 1 iterations in 0.01 seconds (average 0.008000, setup 0.00)

goal = 1, inf = 0, maybe = 333748

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=333749, nc=443492, nnz=901905, k=4] [13.3 MB]
Building sparse matrix (transition rewards)... [n=333749, nc=443492, nnz=2288, k=4] [3.0 MB]
Creating vector for state rewards... [2.5 MB]
Creating vector for inf... [2.5 MB]
Allocating iteration vectors... [2 x 2.5 MB]
TOTAL: [26.5 MB]

Starting iterations...

Iterative method: 91 iterations in 0.90 seconds (average 0.004835, setup 0.46)

Prob0E: 48 iterations in 0.25 seconds (average 0.005250, setup 0.00)

Prob1A: 3 iterations in 0.04 seconds (average 0.012000, setup 0.00)

yes = 1162, no = 290216, maybe = 42371

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=333749, nc=75904, nnz=222389, k=2] [2.9 MB]
Creating vector for yes... [2.5 MB]
Allocating iteration vectors... [2 x 2.5 MB]
TOTAL: [10.6 MB]

Starting iterations...

Iterative method: 83 iterations in 0.29 seconds (average 0.002169, setup 0.11)

Value in the initial state: 156.95811875920444

Time for model checking: 1.569 seconds.

Result: 156.95811875920444 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"success"}min=? [ F "deadlock" ]/Pmax=? [ F "MissionSuccessful" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=3,Objy1=0,Objx2=6,Objy2=1

Prob0A: 4 iterations in 0.04 seconds (average 0.009000, setup 0.00)

Prob1E: 5 iterations in 0.05 seconds (average 0.010400, setup 0.00)

Warning: PRISM hasn't checked for zero-reward loops.
Your minimum rewards may be too low...

goal = 1, inf = 0, maybe = 333748

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=333749, nc=443492, nnz=901905, k=4] [13.3 MB]
Building sparse matrix (transition rewards)... [n=333749, nc=443492, nnz=2288, k=4] [3.0 MB]
Creating vector for state rewards... [2.5 MB]
Creating vector for inf... [2.5 MB]
Allocating iteration vectors... [2 x 2.5 MB]
TOTAL: [26.5 MB]

Starting iterations...

Iterative method: 91 iterations in 0.86 seconds (average 0.004659, setup 0.44)

Prob0A: 48 iterations in 0.29 seconds (average 0.006083, setup 0.00)

Prob1E: 4 iterations in 0.03 seconds (average 0.008000, setup 0.00)

yes = 1162, no = 282810, maybe = 49777

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=333749, nc=89631, nnz=262551, k=2] [3.4 MB]
Creating vector for yes... [2.5 MB]
Allocating iteration vectors... [2 x 2.5 MB]
TOTAL: [11.0 MB]

Starting iterations...

Iterative method: 93 iterations in 0.35 seconds (average 0.002022, setup 0.16)

Value in the initial state: 70.85127857663515

Time for model checking: 1.673 seconds.

Result: 70.85127857663515 (value in the initial state)

---------------------------------------------------------------------

Note: There were 16 warnings during computation.

