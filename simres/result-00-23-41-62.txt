PRISM
=====

Version: 4.3
Date: Wed Aug 03 16:01:33 BST 2016
Hostname: eglinton
Memory limits: cudd=1g, java(heap)=981.5m
Command line: prism uav_sim_mdp.prism -s uav_mdp.props -const 'Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=4,Objy1=1,Objx2=6,Objy2=2,'

Parsing model file "uav_sim_mdp.prism"...

Parsing properties file "uav_mdp.props"...

18 properties:
(1) Pmax=? [ F "MissionSuccessful" ]
(2) Pmin=? [ F "MissionSuccessful" ]
(3) Pmax=? [ F "MissionTimeUp" ]
(4) Pmin=? [ F "MissionTimeUp" ]
(5) Pmax=? [ F "Crash" ]
(6) Pmin=? [ F "Crash" ]
(7) Pmax=? [ F "AllAreaSearched" ]
(8) Pmin=? [ F "AllAreaSearched" ]
(9) Pmax=? [ F "MissionTimeUp"|"AllAreaSearched"|"Crash" ]
(10) Pmin=? [ F "MissionTimeUp"|"AllAreaSearched"|"Crash" ]
(11) R{"drop"}max=? [ F "deadlock" ]
(12) R{"drop"}min=? [ F "deadlock" ]
(13) R{"charge"}max=? [ F "deadlock" ]
(14) R{"charge"}min=? [ F "deadlock" ]
(15) R{"time"}max=? [ F "deadlock" ]
(16) R{"time"}min=? [ F "deadlock" ]
(17) R{"success"}max=? [ F "deadlock" ]/Pmin=? [ F "MissionSuccessful" ]
(18) R{"success"}min=? [ F "deadlock" ]/Pmax=? [ F "MissionSuccessful" ]

Type:        MDP
Modules:     Obj Move Robot T 
Variables:   NoOfObjs posx posy s1 retx rety b t c 

---------------------------------------------------------------------

Model checking: Pmax=? [ F "MissionSuccessful" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=4,Objy1=1,Objx2=6,Objy2=2

Building model...
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=4,Objy1=1,Objx2=6,Objy2=2

Warning: Guard for command 7 of module "Move" is never satisfied.

Warning: Guard for command 8 of module "Move" is never satisfied.

Warning: Guard for command 9 of module "Move" is never satisfied.

Warning: Guard for command 7 of module "Robot" is never satisfied.

Warning: Update 2 of command 3 of module "Robot" doesn't do anything ("(s1'=13)", line 210, column 44)

Warning: Update 2 of command 27 of module "T" doesn't do anything ("(c'=1)", line 323, column 108)

Warning: Guard for command 28 of module "T" is never satisfied.

Warning: Update 2 of command 9 of module "T" doesn't do anything ("(c'=1)", line 300, column 112)

Warning: Guard for command 10 of module "T" is never satisfied.

Warning: Update 2 of command 51 of module "T" doesn't do anything ("(c'=1)", line 354, column 113)

Warning: Guard for command 52 of module "T" is never satisfied.

Computing reachable states...

Reachability (BFS): 89 iterations in 1.47 seconds (average 0.016494, setup 0.00)

Time for model construction: 3.848 seconds.

Warning: Deadlocks detected and fixed in 1 states

Type:        MDP
States:      316937 (1 initial)
Transitions: 871881
Choices:     425769

Transition matrix: 222717 nodes (97 terminal), 871881 minterms, vars: 34r/34c/18nd

Prob0A: 52 iterations in 0.37 seconds (average 0.007154, setup 0.00)

Prob1E: 4 iterations in 0.06 seconds (average 0.015000, setup 0.00)

yes = 1244, no = 265388, maybe = 50305

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=316937, nc=88336, nnz=254636, k=2] [3.3 MB]
Creating vector for yes... [2.4 MB]
Allocating iteration vectors... [2 x 2.4 MB]
TOTAL: [10.6 MB]

Starting iterations...

Iterative method: 102 iterations in 0.47 seconds (average 0.002078, setup 0.26)

Value in the initial state: 0.9814171226397403

Time for model checking: 0.934 seconds.

Result: 0.9814171226397403 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmin=? [ F "MissionSuccessful" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=4,Objy1=1,Objx2=6,Objy2=2

Prob0E: 54 iterations in 0.46 seconds (average 0.008593, setup 0.00)

Prob1A: 3 iterations in 0.06 seconds (average 0.018667, setup 0.00)

yes = 1244, no = 272037, maybe = 43656

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=316937, nc=76468, nnz=220511, k=2] [2.9 MB]
Creating vector for yes... [2.4 MB]
Allocating iteration vectors... [2 x 2.4 MB]
TOTAL: [10.2 MB]

Starting iterations...

Iterative method: 92 iterations in 0.41 seconds (average 0.002087, setup 0.22)

Value in the initial state: 0.8009011446280264

Time for model checking: 0.967 seconds.

Result: 0.8009011446280264 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmax=? [ F "MissionTimeUp" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=4,Objy1=1,Objx2=6,Objy2=2

Prob0A: 72 iterations in 0.73 seconds (average 0.010111, setup 0.00)

Prob1E: 4 iterations in 0.05 seconds (average 0.013000, setup 0.00)

yes = 456, no = 184712, maybe = 131769

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=316937, nc=226832, nnz=628935, k=4] [7.7 MB]
Creating vector for yes... [2.4 MB]
Allocating iteration vectors... [2 x 2.4 MB]
TOTAL: [15.0 MB]

Starting iterations...

Iterative method: 99 iterations in 0.65 seconds (average 0.002828, setup 0.37)

Value in the initial state: 7.322524828918155E-4

Time for model checking: 1.431 seconds.

Result: 7.322524828918155E-4 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmin=? [ F "MissionTimeUp" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=4,Objy1=1,Objx2=6,Objy2=2

Prob0E: 86 iterations in 0.86 seconds (average 0.009953, setup 0.00)

Prob1A: 3 iterations in 0.06 seconds (average 0.018667, setup 0.00)

yes = 456, no = 185106, maybe = 131375

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=316937, nc=226125, nnz=627579, k=4] [7.7 MB]
Creating vector for yes... [2.4 MB]
Allocating iteration vectors... [2 x 2.4 MB]
TOTAL: [15.0 MB]

Starting iterations...

Iterative method: 114 iterations in 0.67 seconds (average 0.002807, setup 0.35)

Value in the initial state: 1.5981585263378047E-6

Time for model checking: 1.612 seconds.

Result: 1.5981585263378047E-6 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmax=? [ F "Crash" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=4,Objy1=1,Objx2=6,Objy2=2

Prob0A: 3 iterations in 0.09 seconds (average 0.030667, setup 0.00)

Prob1E: 200 iterations in 2.87 seconds (average 0.014340, setup 0.00)

yes = 154995, no = 3419, maybe = 158523

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=316937, nc=267355, nnz=713467, k=4] [8.7 MB]
Creating vector for yes... [2.4 MB]
Allocating iteration vectors... [2 x 2.4 MB]
TOTAL: [16.0 MB]

Starting iterations...

Iterative method: 94 iterations in 0.68 seconds (average 0.002936, setup 0.40)

Value in the initial state: 0.031807534106377926

Time for model checking: 3.668 seconds.

Result: 0.031807534106377926 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmin=? [ F "Crash" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=4,Objy1=1,Objx2=6,Objy2=2

Prob0E: 4 iterations in 0.08 seconds (average 0.019000, setup 0.00)

Prob1A: 49 iterations in 0.56 seconds (average 0.011429, setup 0.00)

yes = 154995, no = 3419, maybe = 158523

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=316937, nc=267355, nnz=713467, k=4] [8.7 MB]
Creating vector for yes... [2.4 MB]
Allocating iteration vectors... [2 x 2.4 MB]
TOTAL: [16.0 MB]

Starting iterations...

Iterative method: 92 iterations in 0.64 seconds (average 0.003043, setup 0.36)

Value in the initial state: 0.016475591528366015

Time for model checking: 1.298 seconds.

Result: 0.016475591528366015 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmax=? [ F "AllAreaSearched" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=4,Objy1=1,Objx2=6,Objy2=2

Prob0A: 56 iterations in 0.42 seconds (average 0.007429, setup 0.00)

Prob1E: 4 iterations in 0.05 seconds (average 0.013000, setup 0.00)

yes = 1718, no = 240789, maybe = 74430

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=316937, nc=121888, nnz=338362, k=2] [4.3 MB]
Creating vector for yes... [2.4 MB]
Allocating iteration vectors... [2 x 2.4 MB]
TOTAL: [11.5 MB]

Starting iterations...

Iterative method: 91 iterations in 0.45 seconds (average 0.002374, setup 0.24)

Value in the initial state: 0.17169482006035033

Time for model checking: 0.95 seconds.

Result: 0.17169482006035033 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmin=? [ F "AllAreaSearched" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=4,Objy1=1,Objx2=6,Objy2=2

Prob0E: 64 iterations in 0.47 seconds (average 0.007313, setup 0.00)

Prob1A: 3 iterations in 0.04 seconds (average 0.014667, setup 0.00)

yes = 1718, no = 249895, maybe = 65324

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=316937, nc=106085, nnz=293515, k=2] [3.8 MB]
Creating vector for yes... [2.4 MB]
Allocating iteration vectors... [2 x 2.4 MB]
TOTAL: [11.0 MB]

Starting iterations...

Iterative method: 98 iterations in 0.40 seconds (average 0.002204, setup 0.18)

Value in the initial state: 0.0017489522842566948

Time for model checking: 0.925 seconds.

Result: 0.0017489522842566948 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmax=? [ F "MissionTimeUp"|"AllAreaSearched"|"Crash" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=4,Objy1=1,Objx2=6,Objy2=2

Prob0A: 3 iterations in 0.10 seconds (average 0.034667, setup 0.00)

Prob1E: 208 iterations in 3.03 seconds (average 0.014577, setup 0.00)

yes = 272036, no = 1245, maybe = 43656

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=316937, nc=76468, nnz=220511, k=2] [2.9 MB]
Creating vector for yes... [2.4 MB]
Allocating iteration vectors... [2 x 2.4 MB]
TOTAL: [10.2 MB]

Starting iterations...

Iterative method: 87 iterations in 0.38 seconds (average 0.001977, setup 0.20)

Value in the initial state: 0.19909858301562577

Time for model checking: 3.549 seconds.

Result: 0.19909858301562577 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmin=? [ F "MissionTimeUp"|"AllAreaSearched"|"Crash" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=4,Objy1=1,Objx2=6,Objy2=2

Prob0E: 4 iterations in 0.07 seconds (average 0.018000, setup 0.00)

Prob1A: 52 iterations in 0.57 seconds (average 0.011000, setup 0.00)

yes = 265387, no = 1245, maybe = 50305

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=316937, nc=88336, nnz=254636, k=2] [3.3 MB]
Creating vector for yes... [2.4 MB]
Allocating iteration vectors... [2 x 2.4 MB]
TOTAL: [10.6 MB]

Starting iterations...

Iterative method: 99 iterations in 0.42 seconds (average 0.002141, setup 0.21)

Value in the initial state: 0.01858196491956311

Time for model checking: 1.098 seconds.

Result: 0.01858196491956311 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"drop"}max=? [ F "deadlock" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=4,Objy1=1,Objx2=6,Objy2=2

Prob0E: 5 iterations in 0.09 seconds (average 0.017600, setup 0.00)

Prob1A: 1 iterations in 0.01 seconds (average 0.008000, setup 0.00)

goal = 1, inf = 0, maybe = 316936

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=316937, nc=425768, nnz=871880, k=4] [12.8 MB]
Building sparse matrix (transition rewards)... [n=316937, nc=425768, nnz=0, k=4] [2.8 MB]
Creating vector for state rewards... [2.4 MB]
Creating vector for inf... [2.4 MB]
Allocating iteration vectors... [2 x 2.4 MB]
TOTAL: [25.3 MB]

Starting iterations...

Iterative method: 96 iterations in 0.95 seconds (average 0.004542, setup 0.52)

Value in the initial state: 0.25396131631317925

Time for model checking: 1.064 seconds.

Result: 0.25396131631317925 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"drop"}min=? [ F "deadlock" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=4,Objy1=1,Objx2=6,Objy2=2

Prob0A: 4 iterations in 0.06 seconds (average 0.016000, setup 0.00)

Prob1E: 5 iterations in 0.07 seconds (average 0.013600, setup 0.00)

Warning: PRISM hasn't checked for zero-reward loops.
Your minimum rewards may be too low...

goal = 1, inf = 0, maybe = 316936

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=316937, nc=425768, nnz=871880, k=4] [12.8 MB]
Building sparse matrix (transition rewards)... [n=316937, nc=425768, nnz=0, k=4] [2.8 MB]
Creating vector for state rewards... [2.4 MB]
Creating vector for inf... [2.4 MB]
Allocating iteration vectors... [2 x 2.4 MB]
TOTAL: [25.3 MB]

Starting iterations...

Iterative method: 86 iterations in 0.82 seconds (average 0.004651, setup 0.42)

Value in the initial state: 0.20639876994612144

Time for model checking: 0.965 seconds.

Result: 0.20639876994612144 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"charge"}max=? [ F "deadlock" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=4,Objy1=1,Objx2=6,Objy2=2

Prob0E: 5 iterations in 0.04 seconds (average 0.008800, setup 0.00)

Prob1A: 1 iterations in 0.01 seconds (average 0.008000, setup 0.00)

goal = 1, inf = 0, maybe = 316936

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=316937, nc=425768, nnz=871880, k=4] [12.8 MB]
Building sparse matrix (transition rewards)... [n=316937, nc=425768, nnz=10618, k=4] [3.0 MB]
Creating vector for state rewards... [2.4 MB]
Creating vector for inf... [2.4 MB]
Allocating iteration vectors... [2 x 2.4 MB]
TOTAL: [25.4 MB]

Starting iterations...

Iterative method: 91 iterations in 0.84 seconds (average 0.004615, setup 0.42)

Value in the initial state: 0.3114412285257838

Time for model checking: 0.901 seconds.

Result: 0.3114412285257838 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"charge"}min=? [ F "deadlock" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=4,Objy1=1,Objx2=6,Objy2=2

Prob0A: 4 iterations in 0.04 seconds (average 0.009000, setup 0.00)

Prob1E: 5 iterations in 0.06 seconds (average 0.012000, setup 0.00)

Warning: PRISM hasn't checked for zero-reward loops.
Your minimum rewards may be too low...

goal = 1, inf = 0, maybe = 316936

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=316937, nc=425768, nnz=871880, k=4] [12.8 MB]
Building sparse matrix (transition rewards)... [n=316937, nc=425768, nnz=10618, k=4] [3.0 MB]
Creating vector for state rewards... [2.4 MB]
Creating vector for inf... [2.4 MB]
Allocating iteration vectors... [2 x 2.4 MB]
TOTAL: [25.4 MB]

Starting iterations...

Iterative method: 104 iterations in 0.88 seconds (average 0.004615, setup 0.40)

Value in the initial state: 0.04454198844175227

Time for model checking: 1.008 seconds.

Result: 0.04454198844175227 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"time"}max=? [ F "deadlock" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=4,Objy1=1,Objx2=6,Objy2=2

Prob0E: 5 iterations in 0.04 seconds (average 0.007200, setup 0.00)

Prob1A: 1 iterations in 0.00 seconds (average 0.004000, setup 0.00)

goal = 1, inf = 0, maybe = 316936

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=316937, nc=425768, nnz=871880, k=4] [12.8 MB]
Building sparse matrix (transition rewards)... [n=316937, nc=425768, nnz=158412, k=4] [4.6 MB]
Creating vector for state rewards... [2.4 MB]
Creating vector for inf... [2.4 MB]
Allocating iteration vectors... [2 x 2.4 MB]
TOTAL: [27.1 MB]

Starting iterations...

Iterative method: 97 iterations in 0.91 seconds (average 0.004660, setup 0.46)

Value in the initial state: 175.42868995223864

Time for model checking: 0.97 seconds.

Result: 175.42868995223864 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"time"}min=? [ F "deadlock" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=4,Objy1=1,Objx2=6,Objy2=2

Prob0A: 4 iterations in 0.03 seconds (average 0.008000, setup 0.00)

Prob1E: 5 iterations in 0.06 seconds (average 0.011200, setup 0.00)

Warning: PRISM hasn't checked for zero-reward loops.
Your minimum rewards may be too low...

goal = 1, inf = 0, maybe = 316936

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=316937, nc=425768, nnz=871880, k=4] [12.8 MB]
Building sparse matrix (transition rewards)... [n=316937, nc=425768, nnz=158412, k=4] [4.6 MB]
Creating vector for state rewards... [2.4 MB]
Creating vector for inf... [2.4 MB]
Allocating iteration vectors... [2 x 2.4 MB]
TOTAL: [27.1 MB]

Starting iterations...

Iterative method: 106 iterations in 1.00 seconds (average 0.005170, setup 0.45)

Value in the initial state: 91.34210744488465

Time for model checking: 1.104 seconds.

Result: 91.34210744488465 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"success"}max=? [ F "deadlock" ]/Pmin=? [ F "MissionSuccessful" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=4,Objy1=1,Objx2=6,Objy2=2

Prob0E: 5 iterations in 0.06 seconds (average 0.012800, setup 0.00)

Prob1A: 1 iterations in 0.01 seconds (average 0.012000, setup 0.00)

goal = 1, inf = 0, maybe = 316936

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=316937, nc=425768, nnz=871880, k=4] [12.8 MB]
Building sparse matrix (transition rewards)... [n=316937, nc=425768, nnz=2450, k=4] [2.9 MB]
Creating vector for state rewards... [2.4 MB]
Creating vector for inf... [2.4 MB]
Allocating iteration vectors... [2 x 2.4 MB]
TOTAL: [25.3 MB]

Starting iterations...

Iterative method: 98 iterations in 0.95 seconds (average 0.005143, setup 0.45)

Prob0E: 54 iterations in 0.37 seconds (average 0.006889, setup 0.00)

Prob1A: 3 iterations in 0.04 seconds (average 0.014667, setup 0.00)

yes = 1244, no = 272037, maybe = 43656

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=316937, nc=76468, nnz=220511, k=2] [2.9 MB]
Creating vector for yes... [2.4 MB]
Allocating iteration vectors... [2 x 2.4 MB]
TOTAL: [10.2 MB]

Starting iterations...

Iterative method: 92 iterations in 0.42 seconds (average 0.002826, setup 0.16)

Value in the initial state: 214.51266142726405

Time for model checking: 1.926 seconds.

Result: 214.51266142726405 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"success"}min=? [ F "deadlock" ]/Pmax=? [ F "MissionSuccessful" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=4,Objy1=1,Objx2=6,Objy2=2

Prob0A: 4 iterations in 0.11 seconds (average 0.027000, setup 0.00)

Prob1E: 5 iterations in 0.06 seconds (average 0.012800, setup 0.00)

Warning: PRISM hasn't checked for zero-reward loops.
Your minimum rewards may be too low...

goal = 1, inf = 0, maybe = 316936

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=316937, nc=425768, nnz=871880, k=4] [12.8 MB]
Building sparse matrix (transition rewards)... [n=316937, nc=425768, nnz=2450, k=4] [2.9 MB]
Creating vector for state rewards... [2.4 MB]
Creating vector for inf... [2.4 MB]
Allocating iteration vectors... [2 x 2.4 MB]
TOTAL: [25.3 MB]

Starting iterations...

Iterative method: 100 iterations in 1.01 seconds (average 0.004760, setup 0.54)

Prob0A: 52 iterations in 0.28 seconds (average 0.005385, setup 0.00)

Prob1E: 4 iterations in 0.04 seconds (average 0.010000, setup 0.00)

yes = 1244, no = 265388, maybe = 50305

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=316937, nc=88336, nnz=254636, k=2] [3.3 MB]
Creating vector for yes... [2.4 MB]
Allocating iteration vectors... [2 x 2.4 MB]
TOTAL: [10.6 MB]

Starting iterations...

Iterative method: 102 iterations in 0.36 seconds (average 0.001882, setup 0.16)

Value in the initial state: 77.73199695931855

Time for model checking: 1.893 seconds.

Result: 77.73199695931855 (value in the initial state)

---------------------------------------------------------------------

Note: There were 16 warnings during computation.

