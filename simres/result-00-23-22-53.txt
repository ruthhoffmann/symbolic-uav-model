PRISM
=====

Version: 4.3
Date: Wed Aug 03 15:57:54 BST 2016
Hostname: eglinton
Memory limits: cudd=1g, java(heap)=981.5m
Command line: prism uav_sim_mdp.prism -s uav_mdp.props -const 'Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=2,Objy1=2,Objx2=5,Objy2=3,'

Parsing model file "uav_sim_mdp.prism"...

Parsing properties file "uav_mdp.props"...

18 properties:
(1) Pmax=? [ F "MissionSuccessful" ]
(2) Pmin=? [ F "MissionSuccessful" ]
(3) Pmax=? [ F "MissionTimeUp" ]
(4) Pmin=? [ F "MissionTimeUp" ]
(5) Pmax=? [ F "Crash" ]
(6) Pmin=? [ F "Crash" ]
(7) Pmax=? [ F "AllAreaSearched" ]
(8) Pmin=? [ F "AllAreaSearched" ]
(9) Pmax=? [ F "MissionTimeUp"|"AllAreaSearched"|"Crash" ]
(10) Pmin=? [ F "MissionTimeUp"|"AllAreaSearched"|"Crash" ]
(11) R{"drop"}max=? [ F "deadlock" ]
(12) R{"drop"}min=? [ F "deadlock" ]
(13) R{"charge"}max=? [ F "deadlock" ]
(14) R{"charge"}min=? [ F "deadlock" ]
(15) R{"time"}max=? [ F "deadlock" ]
(16) R{"time"}min=? [ F "deadlock" ]
(17) R{"success"}max=? [ F "deadlock" ]/Pmin=? [ F "MissionSuccessful" ]
(18) R{"success"}min=? [ F "deadlock" ]/Pmax=? [ F "MissionSuccessful" ]

Type:        MDP
Modules:     Obj Move Robot T 
Variables:   NoOfObjs posx posy s1 retx rety b t c 

---------------------------------------------------------------------

Model checking: Pmax=? [ F "MissionSuccessful" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=2,Objy1=2,Objx2=5,Objy2=3

Building model...
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=2,Objy1=2,Objx2=5,Objy2=3

Warning: Guard for command 7 of module "Move" is never satisfied.

Warning: Guard for command 8 of module "Move" is never satisfied.

Warning: Guard for command 9 of module "Move" is never satisfied.

Warning: Guard for command 7 of module "Robot" is never satisfied.

Warning: Update 2 of command 3 of module "Robot" doesn't do anything ("(s1'=13)", line 210, column 44)

Warning: Update 2 of command 27 of module "T" doesn't do anything ("(c'=1)", line 323, column 108)

Warning: Guard for command 28 of module "T" is never satisfied.

Warning: Update 2 of command 9 of module "T" doesn't do anything ("(c'=1)", line 300, column 112)

Warning: Guard for command 10 of module "T" is never satisfied.

Warning: Update 2 of command 51 of module "T" doesn't do anything ("(c'=1)", line 354, column 113)

Warning: Guard for command 52 of module "T" is never satisfied.

Computing reachable states...

Reachability (BFS): 88 iterations in 1.09 seconds (average 0.012409, setup 0.00)

Time for model construction: 3.047 seconds.

Warning: Deadlocks detected and fixed in 1 states

Type:        MDP
States:      225632 (1 initial)
Transitions: 621842
Choices:     305592

Transition matrix: 209789 nodes (93 terminal), 621842 minterms, vars: 34r/34c/18nd

Prob0A: 51 iterations in 0.36 seconds (average 0.006980, setup 0.00)

Prob1E: 4 iterations in 0.05 seconds (average 0.013000, setup 0.00)

yes = 1122, no = 185345, maybe = 39165

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=225632, nc=69482, nnz=196919, k=2] [2.5 MB]
Creating vector for yes... [1.7 MB]
Allocating iteration vectors... [2 x 1.7 MB]
TOTAL: [7.7 MB]

Starting iterations...

Iterative method: 99 iterations in 0.42 seconds (average 0.001616, setup 0.26)

Value in the initial state: 0.9824469758291459

Time for model checking: 0.864 seconds.

Result: 0.9824469758291459 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmin=? [ F "MissionSuccessful" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=2,Objy1=2,Objx2=5,Objy2=3

Prob0E: 51 iterations in 0.39 seconds (average 0.007608, setup 0.00)

Prob1A: 3 iterations in 0.05 seconds (average 0.017333, setup 0.00)

yes = 1122, no = 189979, maybe = 34531

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=225632, nc=61134, nnz=173160, k=2] [2.3 MB]
Creating vector for yes... [1.7 MB]
Allocating iteration vectors... [2 x 1.7 MB]
TOTAL: [7.4 MB]

Starting iterations...

Iterative method: 91 iterations in 0.36 seconds (average 0.001495, setup 0.22)

Value in the initial state: 0.8328981263486429

Time for model checking: 0.821 seconds.

Result: 0.8328981263486429 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmax=? [ F "MissionTimeUp" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=2,Objy1=2,Objx2=5,Objy2=3

Prob0A: 77 iterations in 0.70 seconds (average 0.009039, setup 0.00)

Prob1E: 4 iterations in 0.05 seconds (average 0.012000, setup 0.00)

yes = 304, no = 134588, maybe = 90740

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=225632, nc=158210, nnz=437072, k=4] [5.4 MB]
Creating vector for yes... [1.7 MB]
Allocating iteration vectors... [2 x 1.7 MB]
TOTAL: [10.5 MB]

Starting iterations...

Iterative method: 99 iterations in 0.48 seconds (average 0.001980, setup 0.28)

Value in the initial state: 2.112211254873047E-4

Time for model checking: 1.26 seconds.

Result: 2.112211254873047E-4 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmin=? [ F "MissionTimeUp" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=2,Objy1=2,Objx2=5,Objy2=3

Prob0E: 91 iterations in 0.86 seconds (average 0.009407, setup 0.00)

Prob1A: 3 iterations in 0.04 seconds (average 0.012000, setup 0.00)

yes = 304, no = 134916, maybe = 90412

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=225632, nc=157625, nnz=435922, k=4] [5.4 MB]
Creating vector for yes... [1.7 MB]
Allocating iteration vectors... [2 x 1.7 MB]
TOTAL: [10.5 MB]

Starting iterations...

Iterative method: 114 iterations in 0.49 seconds (average 0.002035, setup 0.26)

Value in the initial state: 2.630444880420324E-7

Time for model checking: 1.403 seconds.

Result: 2.630444880420324E-7 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmax=? [ F "Crash" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=2,Objy1=2,Objx2=5,Objy2=3

Prob0A: 3 iterations in 0.06 seconds (average 0.021333, setup 0.00)

Prob1E: 192 iterations in 2.59 seconds (average 0.013479, setup 0.00)

yes = 110436, no = 2985, maybe = 112211

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=225632, nc=192171, nnz=508421, k=4] [6.2 MB]
Creating vector for yes... [1.7 MB]
Allocating iteration vectors... [2 x 1.7 MB]
TOTAL: [11.4 MB]

Starting iterations...

Iterative method: 93 iterations in 0.53 seconds (average 0.002194, setup 0.33)

Value in the initial state: 0.029653228254955463

Time for model checking: 3.208 seconds.

Result: 0.029653228254955463 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmin=? [ F "Crash" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=2,Objy1=2,Objx2=5,Objy2=3

Prob0E: 4 iterations in 0.08 seconds (average 0.021000, setup 0.00)

Prob1A: 51 iterations in 0.52 seconds (average 0.010196, setup 0.00)

yes = 110436, no = 2985, maybe = 112211

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=225632, nc=192171, nnz=508421, k=4] [6.2 MB]
Creating vector for yes... [1.7 MB]
Allocating iteration vectors... [2 x 1.7 MB]
TOTAL: [11.4 MB]

Starting iterations...

Iterative method: 88 iterations in 0.48 seconds (average 0.002182, setup 0.29)

Value in the initial state: 0.01548974834952059

Time for model checking: 1.121 seconds.

Result: 0.01548974834952059 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmax=? [ F "AllAreaSearched" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=2,Objy1=2,Objx2=5,Objy2=3

Prob0A: 54 iterations in 0.33 seconds (average 0.006074, setup 0.00)

Prob1E: 4 iterations in 0.04 seconds (average 0.009000, setup 0.00)

yes = 1558, no = 162077, maybe = 61997

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=225632, nc=103900, nnz=286921, k=2] [3.6 MB]
Creating vector for yes... [1.7 MB]
Allocating iteration vectors... [2 x 1.7 MB]
TOTAL: [8.8 MB]

Starting iterations...

Iterative method: 87 iterations in 0.37 seconds (average 0.001839, setup 0.21)

Value in the initial state: 0.14158822785039019

Time for model checking: 0.75 seconds.

Result: 0.14158822785039019 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmin=? [ F "AllAreaSearched" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=2,Objy1=2,Objx2=5,Objy2=3

Prob0E: 62 iterations in 0.38 seconds (average 0.006194, setup 0.00)

Prob1A: 3 iterations in 0.04 seconds (average 0.012000, setup 0.00)

yes = 1558, no = 169448, maybe = 54626

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=225632, nc=90616, nnz=248618, k=2] [3.1 MB]
Creating vector for yes... [1.7 MB]
Allocating iteration vectors... [2 x 1.7 MB]
TOTAL: [8.3 MB]

Starting iterations...

Iterative method: 96 iterations in 0.32 seconds (average 0.001667, setup 0.16)

Value in the initial state: 0.0017993942758397564

Time for model checking: 0.752 seconds.

Result: 0.0017993942758397564 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmax=? [ F "MissionTimeUp"|"AllAreaSearched"|"Crash" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=2,Objy1=2,Objx2=5,Objy2=3

Prob0A: 3 iterations in 0.07 seconds (average 0.024000, setup 0.00)

Prob1E: 196 iterations in 2.62 seconds (average 0.013367, setup 0.00)

yes = 189978, no = 1123, maybe = 34531

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=225632, nc=61134, nnz=173160, k=2] [2.3 MB]
Creating vector for yes... [1.7 MB]
Allocating iteration vectors... [2 x 1.7 MB]
TOTAL: [7.4 MB]

Starting iterations...

Iterative method: 86 iterations in 0.32 seconds (average 0.001535, setup 0.19)

Value in the initial state: 0.1671014590536342

Time for model checking: 3.05 seconds.

Result: 0.1671014590536342 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmin=? [ F "MissionTimeUp"|"AllAreaSearched"|"Crash" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=2,Objy1=2,Objx2=5,Objy2=3

Prob0E: 4 iterations in 0.10 seconds (average 0.024000, setup 0.00)

Prob1A: 51 iterations in 0.48 seconds (average 0.009490, setup 0.00)

yes = 185344, no = 1123, maybe = 39165

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=225632, nc=69482, nnz=196919, k=2] [2.5 MB]
Creating vector for yes... [1.7 MB]
Allocating iteration vectors... [2 x 1.7 MB]
TOTAL: [7.7 MB]

Starting iterations...

Iterative method: 96 iterations in 0.33 seconds (average 0.001583, setup 0.18)

Value in the initial state: 0.017551766386546278

Time for model checking: 0.894 seconds.

Result: 0.017551766386546278 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"drop"}max=? [ F "deadlock" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=2,Objy1=2,Objx2=5,Objy2=3

Prob0E: 5 iterations in 0.07 seconds (average 0.014400, setup 0.00)

Prob1A: 1 iterations in 0.01 seconds (average 0.008000, setup 0.00)

goal = 1, inf = 0, maybe = 225631

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=225632, nc=305591, nnz=621841, k=4] [9.1 MB]
Building sparse matrix (transition rewards)... [n=225632, nc=305591, nnz=0, k=4] [2.0 MB]
Creating vector for state rewards... [1.7 MB]
Creating vector for inf... [1.7 MB]
Allocating iteration vectors... [2 x 1.7 MB]
TOTAL: [18.1 MB]

Starting iterations...

Iterative method: 94 iterations in 0.72 seconds (average 0.003319, setup 0.41)

Value in the initial state: 0.18892685248402202

Time for model checking: 0.836 seconds.

Result: 0.18892685248402202 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"drop"}min=? [ F "deadlock" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=2,Objy1=2,Objx2=5,Objy2=3

Prob0A: 4 iterations in 0.04 seconds (average 0.011000, setup 0.00)

Prob1E: 5 iterations in 0.05 seconds (average 0.009600, setup 0.00)

Warning: PRISM hasn't checked for zero-reward loops.
Your minimum rewards may be too low...

goal = 1, inf = 0, maybe = 225631

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=225632, nc=305591, nnz=621841, k=4] [9.1 MB]
Building sparse matrix (transition rewards)... [n=225632, nc=305591, nnz=0, k=4] [2.0 MB]
Creating vector for state rewards... [1.7 MB]
Creating vector for inf... [1.7 MB]
Allocating iteration vectors... [2 x 1.7 MB]
TOTAL: [18.1 MB]

Starting iterations...

Iterative method: 85 iterations in 0.73 seconds (average 0.003482, setup 0.43)

Value in the initial state: 0.15674274774353836

Time for model checking: 0.851 seconds.

Result: 0.15674274774353836 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"charge"}max=? [ F "deadlock" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=2,Objy1=2,Objx2=5,Objy2=3

Prob0E: 5 iterations in 0.02 seconds (average 0.004800, setup 0.00)

Prob1A: 1 iterations in 0.01 seconds (average 0.008000, setup 0.00)

goal = 1, inf = 0, maybe = 225631

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=225632, nc=305591, nnz=621841, k=4] [9.1 MB]
Building sparse matrix (transition rewards)... [n=225632, nc=305591, nnz=7532, k=4] [2.1 MB]
Creating vector for state rewards... [1.7 MB]
Creating vector for inf... [1.7 MB]
Allocating iteration vectors... [2 x 1.7 MB]
TOTAL: [18.1 MB]

Starting iterations...

Iterative method: 91 iterations in 0.63 seconds (average 0.003341, setup 0.32)

Value in the initial state: 0.27369609493283115

Time for model checking: 0.677 seconds.

Result: 0.27369609493283115 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"charge"}min=? [ F "deadlock" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=2,Objy1=2,Objx2=5,Objy2=3

Prob0A: 4 iterations in 0.02 seconds (average 0.006000, setup 0.00)

Prob1E: 5 iterations in 0.04 seconds (average 0.008800, setup 0.00)

Warning: PRISM hasn't checked for zero-reward loops.
Your minimum rewards may be too low...

goal = 1, inf = 0, maybe = 225631

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=225632, nc=305591, nnz=621841, k=4] [9.1 MB]
Building sparse matrix (transition rewards)... [n=225632, nc=305591, nnz=7532, k=4] [2.1 MB]
Creating vector for state rewards... [1.7 MB]
Creating vector for inf... [1.7 MB]
Allocating iteration vectors... [2 x 1.7 MB]
TOTAL: [18.1 MB]

Starting iterations...

Iterative method: 101 iterations in 0.65 seconds (average 0.003366, setup 0.31)

Value in the initial state: 0.027954685833799533

Time for model checking: 0.734 seconds.

Result: 0.027954685833799533 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"time"}max=? [ F "deadlock" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=2,Objy1=2,Objx2=5,Objy2=3

Prob0E: 5 iterations in 0.03 seconds (average 0.006400, setup 0.00)

Prob1A: 1 iterations in 0.00 seconds (average 0.004000, setup 0.00)

goal = 1, inf = 0, maybe = 225631

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=225632, nc=305591, nnz=621841, k=4] [9.1 MB]
Building sparse matrix (transition rewards)... [n=225632, nc=305591, nnz=113419, k=4] [3.3 MB]
Creating vector for state rewards... [1.7 MB]
Creating vector for inf... [1.7 MB]
Allocating iteration vectors... [2 x 1.7 MB]
TOTAL: [19.4 MB]

Starting iterations...

Iterative method: 96 iterations in 0.67 seconds (average 0.003375, setup 0.34)

Value in the initial state: 163.6794230700894

Time for model checking: 0.724 seconds.

Result: 163.6794230700894 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"time"}min=? [ F "deadlock" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=2,Objy1=2,Objx2=5,Objy2=3

Prob0A: 4 iterations in 0.03 seconds (average 0.007000, setup 0.00)

Prob1E: 5 iterations in 0.04 seconds (average 0.008800, setup 0.00)

Warning: PRISM hasn't checked for zero-reward loops.
Your minimum rewards may be too low...

goal = 1, inf = 0, maybe = 225631

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=225632, nc=305591, nnz=621841, k=4] [9.1 MB]
Building sparse matrix (transition rewards)... [n=225632, nc=305591, nnz=113419, k=4] [3.3 MB]
Creating vector for state rewards... [1.7 MB]
Creating vector for inf... [1.7 MB]
Allocating iteration vectors... [2 x 1.7 MB]
TOTAL: [19.4 MB]

Starting iterations...

Iterative method: 101 iterations in 0.85 seconds (average 0.004950, setup 0.35)

Value in the initial state: 85.92699854394395

Time for model checking: 0.935 seconds.

Result: 85.92699854394395 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"success"}max=? [ F "deadlock" ]/Pmin=? [ F "MissionSuccessful" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=2,Objy1=2,Objx2=5,Objy2=3

Prob0E: 5 iterations in 0.03 seconds (average 0.006400, setup 0.00)

Prob1A: 1 iterations in 0.01 seconds (average 0.008000, setup 0.00)

goal = 1, inf = 0, maybe = 225631

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=225632, nc=305591, nnz=621841, k=4] [9.1 MB]
Building sparse matrix (transition rewards)... [n=225632, nc=305591, nnz=2210, k=4] [2.1 MB]
Creating vector for state rewards... [1.7 MB]
Creating vector for inf... [1.7 MB]
Allocating iteration vectors... [2 x 1.7 MB]
TOTAL: [18.1 MB]

Starting iterations...

Iterative method: 96 iterations in 0.64 seconds (average 0.003375, setup 0.32)

Prob0E: 51 iterations in 0.38 seconds (average 0.007451, setup 0.00)

Prob1A: 3 iterations in 0.04 seconds (average 0.012000, setup 0.00)

yes = 1122, no = 189979, maybe = 34531

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=225632, nc=61134, nnz=173160, k=2] [2.3 MB]
Creating vector for yes... [1.7 MB]
Allocating iteration vectors... [2 x 1.7 MB]
TOTAL: [7.4 MB]

Starting iterations...

Iterative method: 91 iterations in 0.30 seconds (average 0.001538, setup 0.16)

Value in the initial state: 192.74684833205853

Time for model checking: 1.422 seconds.

Result: 192.74684833205853 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"success"}min=? [ F "deadlock" ]/Pmax=? [ F "MissionSuccessful" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=2,Objy1=2,Objx2=5,Objy2=3

Prob0A: 4 iterations in 0.10 seconds (average 0.026000, setup 0.00)

Prob1E: 5 iterations in 0.05 seconds (average 0.009600, setup 0.00)

Warning: PRISM hasn't checked for zero-reward loops.
Your minimum rewards may be too low...

goal = 1, inf = 0, maybe = 225631

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=225632, nc=305591, nnz=621841, k=4] [9.1 MB]
Building sparse matrix (transition rewards)... [n=225632, nc=305591, nnz=2210, k=4] [2.1 MB]
Creating vector for state rewards... [1.7 MB]
Creating vector for inf... [1.7 MB]
Allocating iteration vectors... [2 x 1.7 MB]
TOTAL: [18.1 MB]

Starting iterations...

Iterative method: 98 iterations in 0.68 seconds (average 0.003306, setup 0.36)

Prob0A: 51 iterations in 0.28 seconds (average 0.005490, setup 0.00)

Prob1E: 4 iterations in 0.04 seconds (average 0.009000, setup 0.00)

yes = 1122, no = 185345, maybe = 39165

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=225632, nc=69482, nnz=196919, k=2] [2.5 MB]
Creating vector for yes... [1.7 MB]
Allocating iteration vectors... [2 x 1.7 MB]
TOTAL: [7.7 MB]

Starting iterations...

Iterative method: 99 iterations in 0.32 seconds (average 0.001535, setup 0.17)

Value in the initial state: 77.01289236648549

Time for model checking: 1.489 seconds.

Result: 77.01289236648549 (value in the initial state)

---------------------------------------------------------------------

Note: There were 16 warnings during computation.

