PRISM
=====

Version: 4.3
Date: Wed Aug 03 15:54:57 BST 2016
Hostname: eglinton
Memory limits: cudd=1g, java(heap)=981.5m
Command line: prism uav_sim_mdp.prism -s uav_mdp.props -const 'Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=1,Objy1=1,Objx2=3,Objy2=3,'

Parsing model file "uav_sim_mdp.prism"...

Parsing properties file "uav_mdp.props"...

18 properties:
(1) Pmax=? [ F "MissionSuccessful" ]
(2) Pmin=? [ F "MissionSuccessful" ]
(3) Pmax=? [ F "MissionTimeUp" ]
(4) Pmin=? [ F "MissionTimeUp" ]
(5) Pmax=? [ F "Crash" ]
(6) Pmin=? [ F "Crash" ]
(7) Pmax=? [ F "AllAreaSearched" ]
(8) Pmin=? [ F "AllAreaSearched" ]
(9) Pmax=? [ F "MissionTimeUp"|"AllAreaSearched"|"Crash" ]
(10) Pmin=? [ F "MissionTimeUp"|"AllAreaSearched"|"Crash" ]
(11) R{"drop"}max=? [ F "deadlock" ]
(12) R{"drop"}min=? [ F "deadlock" ]
(13) R{"charge"}max=? [ F "deadlock" ]
(14) R{"charge"}min=? [ F "deadlock" ]
(15) R{"time"}max=? [ F "deadlock" ]
(16) R{"time"}min=? [ F "deadlock" ]
(17) R{"success"}max=? [ F "deadlock" ]/Pmin=? [ F "MissionSuccessful" ]
(18) R{"success"}min=? [ F "deadlock" ]/Pmax=? [ F "MissionSuccessful" ]

Type:        MDP
Modules:     Obj Move Robot T 
Variables:   NoOfObjs posx posy s1 retx rety b t c 

---------------------------------------------------------------------

Model checking: Pmax=? [ F "MissionSuccessful" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=1,Objy1=1,Objx2=3,Objy2=3

Building model...
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=1,Objy1=1,Objx2=3,Objy2=3

Warning: Guard for command 7 of module "Move" is never satisfied.

Warning: Guard for command 8 of module "Move" is never satisfied.

Warning: Guard for command 9 of module "Move" is never satisfied.

Warning: Guard for command 7 of module "Robot" is never satisfied.

Warning: Update 2 of command 3 of module "Robot" doesn't do anything ("(s1'=13)", line 210, column 44)

Warning: Update 2 of command 27 of module "T" doesn't do anything ("(c'=1)", line 323, column 108)

Warning: Guard for command 28 of module "T" is never satisfied.

Warning: Update 2 of command 9 of module "T" doesn't do anything ("(c'=1)", line 300, column 112)

Warning: Guard for command 10 of module "T" is never satisfied.

Warning: Update 2 of command 51 of module "T" doesn't do anything ("(c'=1)", line 354, column 113)

Warning: Guard for command 52 of module "T" is never satisfied.

Computing reachable states...

Reachability (BFS): 88 iterations in 1.18 seconds (average 0.013409, setup 0.00)

Time for model construction: 3.356 seconds.

Warning: Deadlocks detected and fixed in 1 states

Type:        MDP
States:      211226 (1 initial)
Transitions: 545941
Choices:     279520

Transition matrix: 212567 nodes (93 terminal), 545941 minterms, vars: 34r/34c/18nd

Prob0A: 52 iterations in 0.44 seconds (average 0.008462, setup 0.00)

Prob1E: 4 iterations in 0.06 seconds (average 0.016000, setup 0.00)

yes = 1171, no = 173603, maybe = 36452

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=211226, nc=62166, nnz=169637, k=2] [2.2 MB]
Creating vector for yes... [1.6 MB]
Allocating iteration vectors... [2 x 1.6 MB]
TOTAL: [7.0 MB]

Starting iterations...

Iterative method: 95 iterations in 0.41 seconds (average 0.001432, setup 0.27)

Value in the initial state: 0.9819028284340691

Time for model checking: 0.966 seconds.

Result: 0.9819028284340691 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmin=? [ F "MissionSuccessful" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=1,Objy1=1,Objx2=3,Objy2=3

Prob0E: 52 iterations in 0.50 seconds (average 0.009538, setup 0.00)

Prob1A: 3 iterations in 0.06 seconds (average 0.018667, setup 0.00)

yes = 1171, no = 179219, maybe = 30836

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=211226, nc=52431, nnz=142120, k=2] [1.9 MB]
Creating vector for yes... [1.6 MB]
Allocating iteration vectors... [2 x 1.6 MB]
TOTAL: [6.7 MB]

Starting iterations...

Iterative method: 91 iterations in 0.36 seconds (average 0.001407, setup 0.23)

Value in the initial state: 0.8538719540234423

Time for model checking: 0.952 seconds.

Result: 0.8538719540234423 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmax=? [ F "MissionTimeUp" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=1,Objy1=1,Objx2=3,Objy2=3

Prob0A: 74 iterations in 0.76 seconds (average 0.010270, setup 0.00)

Prob1E: 4 iterations in 0.06 seconds (average 0.014000, setup 0.00)

yes = 320, no = 125782, maybe = 85124

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=211226, nc=140079, nnz=372228, k=4] [4.6 MB]
Creating vector for yes... [1.6 MB]
Allocating iteration vectors... [2 x 1.6 MB]
TOTAL: [9.4 MB]

Starting iterations...

Iterative method: 98 iterations in 0.47 seconds (average 0.001878, setup 0.28)

Value in the initial state: 1.2114389152001912E-4

Time for model checking: 1.633 seconds.

Result: 1.2114389152001912E-4 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmin=? [ F "MissionTimeUp" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=1,Objy1=1,Objx2=3,Objy2=3

Prob0E: 88 iterations in 0.95 seconds (average 0.010818, setup 0.00)

Prob1A: 3 iterations in 0.04 seconds (average 0.014667, setup 0.00)

yes = 320, no = 126034, maybe = 84872

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=211226, nc=139646, nnz=371234, k=4] [4.6 MB]
Creating vector for yes... [1.6 MB]
Allocating iteration vectors... [2 x 1.6 MB]
TOTAL: [9.4 MB]

Starting iterations...

Iterative method: 116 iterations in 0.47 seconds (average 0.001793, setup 0.26)

Value in the initial state: 5.997978539309767E-8

Time for model checking: 1.592 seconds.

Result: 5.997978539309767E-8 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmax=? [ F "Crash" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=1,Objy1=1,Objx2=3,Objy2=3

Prob0A: 3 iterations in 0.09 seconds (average 0.029333, setup 0.00)

Prob1E: 196 iterations in 2.96 seconds (average 0.015122, setup 0.00)

yes = 102966, no = 3188, maybe = 105072

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=211226, nc=173366, nnz=439787, k=4] [5.4 MB]
Creating vector for yes... [1.6 MB]
Allocating iteration vectors... [2 x 1.6 MB]
TOTAL: [10.2 MB]

Starting iterations...

Iterative method: 95 iterations in 0.51 seconds (average 0.001979, setup 0.32)

Value in the initial state: 0.028941981563965026

Time for model checking: 3.817 seconds.

Result: 0.028941981563965026 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmin=? [ F "Crash" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=1,Objy1=1,Objx2=3,Objy2=3

Prob0E: 4 iterations in 0.08 seconds (average 0.019000, setup 0.00)

Prob1A: 52 iterations in 0.52 seconds (average 0.010077, setup 0.00)

yes = 102966, no = 3188, maybe = 105072

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=211226, nc=173366, nnz=439787, k=4] [5.4 MB]
Creating vector for yes... [1.6 MB]
Allocating iteration vectors... [2 x 1.6 MB]
TOTAL: [10.2 MB]

Starting iterations...

Iterative method: 86 iterations in 0.41 seconds (average 0.002047, setup 0.23)

Value in the initial state: 0.01605121164141655

Time for model checking: 1.215 seconds.

Result: 0.01605121164141655 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmax=? [ F "AllAreaSearched" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=1,Objy1=1,Objx2=3,Objy2=3

Prob0A: 52 iterations in 0.40 seconds (average 0.007615, setup 0.00)

Prob1E: 4 iterations in 0.05 seconds (average 0.013000, setup 0.00)

yes = 1696, no = 154030, maybe = 55500

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=211226, nc=89952, nnz=233225, k=2] [3.0 MB]
Creating vector for yes... [1.6 MB]
Allocating iteration vectors... [2 x 1.6 MB]
TOTAL: [7.8 MB]

Starting iterations...

Iterative method: 84 iterations in 0.34 seconds (average 0.001524, setup 0.21)

Value in the initial state: 0.12121321179232475

Time for model checking: 0.871 seconds.

Result: 0.12121321179232475 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmin=? [ F "AllAreaSearched" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=1,Objy1=1,Objx2=3,Objy2=3

Prob0E: 54 iterations in 0.42 seconds (average 0.007852, setup 0.00)

Prob1A: 3 iterations in 0.04 seconds (average 0.013333, setup 0.00)

yes = 1696, no = 161988, maybe = 47542

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=211226, nc=76480, nnz=195228, k=2] [2.5 MB]
Creating vector for yes... [1.6 MB]
Allocating iteration vectors... [2 x 1.6 MB]
TOTAL: [7.3 MB]

Starting iterations...

Iterative method: 95 iterations in 0.29 seconds (average 0.001558, setup 0.14)

Value in the initial state: 0.0018148449663547552

Time for model checking: 0.89 seconds.

Result: 0.0018148449663547552 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmax=? [ F "MissionTimeUp"|"AllAreaSearched"|"Crash" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=1,Objy1=1,Objx2=3,Objy2=3

Prob0A: 3 iterations in 0.11 seconds (average 0.036000, setup 0.00)

Prob1E: 200 iterations in 3.14 seconds (average 0.015680, setup 0.00)

yes = 179218, no = 1172, maybe = 30836

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=211226, nc=52431, nnz=142120, k=2] [1.9 MB]
Creating vector for yes... [1.6 MB]
Allocating iteration vectors... [2 x 1.6 MB]
TOTAL: [6.7 MB]

Starting iterations...

Iterative method: 85 iterations in 0.33 seconds (average 0.001412, setup 0.21)

Value in the initial state: 0.14612774591683578

Time for model checking: 4.223 seconds.

Result: 0.14612774591683578 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmin=? [ F "MissionTimeUp"|"AllAreaSearched"|"Crash" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=1,Objy1=1,Objx2=3,Objy2=3

Prob0E: 4 iterations in 0.10 seconds (average 0.024000, setup 0.00)

Prob1A: 52 iterations in 0.71 seconds (average 0.013692, setup 0.00)

yes = 173602, no = 1172, maybe = 36452

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=211226, nc=62166, nnz=169637, k=2] [2.2 MB]
Creating vector for yes... [1.6 MB]
Allocating iteration vectors... [2 x 1.6 MB]
TOTAL: [7.0 MB]

Starting iterations...

Iterative method: 93 iterations in 0.34 seconds (average 0.001548, setup 0.20)

Value in the initial state: 0.018095973079670147

Time for model checking: 1.269 seconds.

Result: 0.018095973079670147 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"drop"}max=? [ F "deadlock" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=1,Objy1=1,Objx2=3,Objy2=3

Prob0E: 5 iterations in 0.11 seconds (average 0.021600, setup 0.00)

Prob1A: 1 iterations in 0.01 seconds (average 0.012000, setup 0.00)

goal = 1, inf = 0, maybe = 211225

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=211226, nc=279519, nnz=545940, k=4] [8.1 MB]
Building sparse matrix (transition rewards)... [n=211226, nc=279519, nnz=0, k=4] [1.9 MB]
Creating vector for state rewards... [1.6 MB]
Creating vector for inf... [1.6 MB]
Allocating iteration vectors... [2 x 1.6 MB]
TOTAL: [16.4 MB]

Starting iterations...

Iterative method: 91 iterations in 0.69 seconds (average 0.003077, setup 0.41)

Value in the initial state: 0.14830918398468212

Time for model checking: 0.827 seconds.

Result: 0.14830918398468212 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"drop"}min=? [ F "deadlock" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=1,Objy1=1,Objx2=3,Objy2=3

Prob0A: 4 iterations in 0.04 seconds (average 0.010000, setup 0.00)

Prob1E: 5 iterations in 0.07 seconds (average 0.014400, setup 0.00)

Warning: PRISM hasn't checked for zero-reward loops.
Your minimum rewards may be too low...

goal = 1, inf = 0, maybe = 211225

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=211226, nc=279519, nnz=545940, k=4] [8.1 MB]
Building sparse matrix (transition rewards)... [n=211226, nc=279519, nnz=0, k=4] [1.9 MB]
Creating vector for state rewards... [1.6 MB]
Creating vector for inf... [1.6 MB]
Allocating iteration vectors... [2 x 1.6 MB]
TOTAL: [16.4 MB]

Starting iterations...

Iterative method: 84 iterations in 0.55 seconds (average 0.003048, setup 0.30)

Value in the initial state: 0.1353835421370976

Time for model checking: 0.681 seconds.

Result: 0.1353835421370976 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"charge"}max=? [ F "deadlock" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=1,Objy1=1,Objx2=3,Objy2=3

Prob0E: 5 iterations in 0.04 seconds (average 0.007200, setup 0.00)

Prob1A: 1 iterations in 0.01 seconds (average 0.008000, setup 0.00)

goal = 1, inf = 0, maybe = 211225

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=211226, nc=279519, nnz=545940, k=4] [8.1 MB]
Building sparse matrix (transition rewards)... [n=211226, nc=279519, nnz=5729, k=4] [1.9 MB]
Creating vector for state rewards... [1.6 MB]
Creating vector for inf... [1.6 MB]
Allocating iteration vectors... [2 x 1.6 MB]
TOTAL: [16.5 MB]

Starting iterations...

Iterative method: 93 iterations in 0.59 seconds (average 0.003097, setup 0.30)

Value in the initial state: 0.25391623904777066

Time for model checking: 0.661 seconds.

Result: 0.25391623904777066 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"charge"}min=? [ F "deadlock" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=1,Objy1=1,Objx2=3,Objy2=3

Prob0A: 4 iterations in 0.03 seconds (average 0.007000, setup 0.00)

Prob1E: 5 iterations in 0.06 seconds (average 0.012000, setup 0.00)

Warning: PRISM hasn't checked for zero-reward loops.
Your minimum rewards may be too low...

goal = 1, inf = 0, maybe = 211225

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=211226, nc=279519, nnz=545940, k=4] [8.1 MB]
Building sparse matrix (transition rewards)... [n=211226, nc=279519, nnz=5729, k=4] [1.9 MB]
Creating vector for state rewards... [1.6 MB]
Creating vector for inf... [1.6 MB]
Allocating iteration vectors... [2 x 1.6 MB]
TOTAL: [16.5 MB]

Starting iterations...

Iterative method: 98 iterations in 0.61 seconds (average 0.003102, setup 0.30)

Value in the initial state: 0.030857018294464852

Time for model checking: 0.798 seconds.

Result: 0.030857018294464852 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"time"}max=? [ F "deadlock" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=1,Objy1=1,Objx2=3,Objy2=3

Prob0E: 5 iterations in 0.04 seconds (average 0.008000, setup 0.00)

Prob1A: 1 iterations in 0.00 seconds (average 0.004000, setup 0.00)

goal = 1, inf = 0, maybe = 211225

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=211226, nc=279519, nnz=545940, k=4] [8.1 MB]
Building sparse matrix (transition rewards)... [n=211226, nc=279519, nnz=106152, k=4] [3.1 MB]
Creating vector for state rewards... [1.6 MB]
Creating vector for inf... [1.6 MB]
Allocating iteration vectors... [2 x 1.6 MB]
TOTAL: [17.7 MB]

Starting iterations...

Iterative method: 97 iterations in 0.66 seconds (average 0.003381, setup 0.34)

Value in the initial state: 159.7984304369062

Time for model checking: 0.741 seconds.

Result: 159.7984304369062 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"time"}min=? [ F "deadlock" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=1,Objy1=1,Objx2=3,Objy2=3

Prob0A: 4 iterations in 0.04 seconds (average 0.011000, setup 0.00)

Prob1E: 5 iterations in 0.08 seconds (average 0.016000, setup 0.00)

Warning: PRISM hasn't checked for zero-reward loops.
Your minimum rewards may be too low...

goal = 1, inf = 0, maybe = 211225

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=211226, nc=279519, nnz=545940, k=4] [8.1 MB]
Building sparse matrix (transition rewards)... [n=211226, nc=279519, nnz=106152, k=4] [3.1 MB]
Creating vector for state rewards... [1.6 MB]
Creating vector for inf... [1.6 MB]
Allocating iteration vectors... [2 x 1.6 MB]
TOTAL: [17.7 MB]

Starting iterations...

Iterative method: 99 iterations in 0.66 seconds (average 0.003313, setup 0.34)

Value in the initial state: 89.02229885822743

Time for model checking: 0.803 seconds.

Result: 89.02229885822743 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"success"}max=? [ F "deadlock" ]/Pmin=? [ F "MissionSuccessful" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=1,Objy1=1,Objx2=3,Objy2=3

Prob0E: 5 iterations in 0.04 seconds (average 0.008000, setup 0.00)

Prob1A: 1 iterations in 0.01 seconds (average 0.008000, setup 0.00)

goal = 1, inf = 0, maybe = 211225

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=211226, nc=279519, nnz=545940, k=4] [8.1 MB]
Building sparse matrix (transition rewards)... [n=211226, nc=279519, nnz=2308, k=4] [1.9 MB]
Creating vector for state rewards... [1.6 MB]
Creating vector for inf... [1.6 MB]
Allocating iteration vectors... [2 x 1.6 MB]
TOTAL: [16.5 MB]

Starting iterations...

Iterative method: 99 iterations in 0.60 seconds (average 0.003030, setup 0.30)

Prob0E: 52 iterations in 0.39 seconds (average 0.007462, setup 0.00)

Prob1A: 3 iterations in 0.05 seconds (average 0.017333, setup 0.00)

yes = 1171, no = 179219, maybe = 30836

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=211226, nc=52431, nnz=142120, k=2] [1.9 MB]
Creating vector for yes... [1.6 MB]
Allocating iteration vectors... [2 x 1.6 MB]
TOTAL: [6.7 MB]

Starting iterations...

Iterative method: 91 iterations in 0.29 seconds (average 0.001407, setup 0.16)

Value in the initial state: 183.68200493446375

Time for model checking: 1.415 seconds.

Result: 183.68200493446375 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"success"}min=? [ F "deadlock" ]/Pmax=? [ F "MissionSuccessful" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=1,Objy1=1,Objx2=3,Objy2=3

Prob0A: 4 iterations in 0.09 seconds (average 0.023000, setup 0.00)

Prob1E: 5 iterations in 0.06 seconds (average 0.012000, setup 0.00)

Warning: PRISM hasn't checked for zero-reward loops.
Your minimum rewards may be too low...

goal = 1, inf = 0, maybe = 211225

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=211226, nc=279519, nnz=545940, k=4] [8.1 MB]
Building sparse matrix (transition rewards)... [n=211226, nc=279519, nnz=2308, k=4] [1.9 MB]
Creating vector for state rewards... [1.6 MB]
Creating vector for inf... [1.6 MB]
Allocating iteration vectors... [2 x 1.6 MB]
TOTAL: [16.5 MB]

Starting iterations...

Iterative method: 95 iterations in 0.62 seconds (average 0.003032, setup 0.34)

Prob0A: 52 iterations in 0.34 seconds (average 0.006538, setup 0.00)

Prob1E: 4 iterations in 0.05 seconds (average 0.013000, setup 0.00)

yes = 1171, no = 173603, maybe = 36452

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=211226, nc=62166, nnz=169637, k=2] [2.2 MB]
Creating vector for yes... [1.6 MB]
Allocating iteration vectors... [2 x 1.6 MB]
TOTAL: [7.0 MB]

Starting iterations...

Iterative method: 95 iterations in 0.32 seconds (average 0.001474, setup 0.18)

Value in the initial state: 78.66508865097839

Time for model checking: 1.484 seconds.

Result: 78.66508865097839 (value in the initial state)

---------------------------------------------------------------------

Note: There were 16 warnings during computation.

