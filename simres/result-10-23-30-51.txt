PRISM
=====

Version: 4.3
Date: Wed Aug 03 16:13:05 BST 2016
Hostname: eglinton
Memory limits: cudd=1g, java(heap)=981.5m
Command line: prism uav_sim_mdp.prism -s uav_mdp.props -const 'Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=3,Objy1=0,Objx2=5,Objy2=1,'

Parsing model file "uav_sim_mdp.prism"...

Parsing properties file "uav_mdp.props"...

18 properties:
(1) Pmax=? [ F "MissionSuccessful" ]
(2) Pmin=? [ F "MissionSuccessful" ]
(3) Pmax=? [ F "MissionTimeUp" ]
(4) Pmin=? [ F "MissionTimeUp" ]
(5) Pmax=? [ F "Crash" ]
(6) Pmin=? [ F "Crash" ]
(7) Pmax=? [ F "AllAreaSearched" ]
(8) Pmin=? [ F "AllAreaSearched" ]
(9) Pmax=? [ F "MissionTimeUp"|"AllAreaSearched"|"Crash" ]
(10) Pmin=? [ F "MissionTimeUp"|"AllAreaSearched"|"Crash" ]
(11) R{"drop"}max=? [ F "deadlock" ]
(12) R{"drop"}min=? [ F "deadlock" ]
(13) R{"charge"}max=? [ F "deadlock" ]
(14) R{"charge"}min=? [ F "deadlock" ]
(15) R{"time"}max=? [ F "deadlock" ]
(16) R{"time"}min=? [ F "deadlock" ]
(17) R{"success"}max=? [ F "deadlock" ]/Pmin=? [ F "MissionSuccessful" ]
(18) R{"success"}min=? [ F "deadlock" ]/Pmax=? [ F "MissionSuccessful" ]

Type:        MDP
Modules:     Obj Move Robot T 
Variables:   NoOfObjs posx posy s1 retx rety b t c 

---------------------------------------------------------------------

Model checking: Pmax=? [ F "MissionSuccessful" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=3,Objy1=0,Objx2=5,Objy2=1

Building model...
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=3,Objy1=0,Objx2=5,Objy2=1

Warning: Guard for command 7 of module "Move" is never satisfied.

Warning: Guard for command 8 of module "Move" is never satisfied.

Warning: Guard for command 9 of module "Move" is never satisfied.

Warning: Guard for command 7 of module "Robot" is never satisfied.

Warning: Update 2 of command 3 of module "Robot" doesn't do anything ("(s1'=13)", line 210, column 44)

Warning: Update 2 of command 27 of module "T" doesn't do anything ("(c'=1)", line 323, column 108)

Warning: Guard for command 28 of module "T" is never satisfied.

Warning: Update 2 of command 9 of module "T" doesn't do anything ("(c'=1)", line 300, column 112)

Warning: Guard for command 10 of module "T" is never satisfied.

Warning: Update 2 of command 51 of module "T" doesn't do anything ("(c'=1)", line 354, column 113)

Warning: Guard for command 52 of module "T" is never satisfied.

Computing reachable states...

Reachability (BFS): 89 iterations in 1.67 seconds (average 0.018787, setup 0.00)

Time for model construction: 3.733 seconds.

Warning: Deadlocks detected and fixed in 1 states

Type:        MDP
States:      299658 (1 initial)
Transitions: 795570
Choices:     395735

Transition matrix: 231124 nodes (91 terminal), 795570 minterms, vars: 34r/34c/18nd

Prob0A: 47 iterations in 0.40 seconds (average 0.008596, setup 0.00)

Prob1E: 4 iterations in 0.06 seconds (average 0.016000, setup 0.00)

yes = 1104, no = 253363, maybe = 45191

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=299658, nc=80744, nnz=233643, k=2] [3.0 MB]
Creating vector for yes... [2.3 MB]
Allocating iteration vectors... [2 x 2.3 MB]
TOTAL: [9.9 MB]

Starting iterations...

Iterative method: 89 iterations in 0.47 seconds (average 0.002157, setup 0.28)

Value in the initial state: 0.984453815104559

Time for model checking: 0.969 seconds.

Result: 0.984453815104559 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmin=? [ F "MissionSuccessful" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=3,Objy1=0,Objx2=5,Objy2=1

Prob0E: 48 iterations in 0.70 seconds (average 0.014667, setup 0.00)

Prob1A: 3 iterations in 0.07 seconds (average 0.024000, setup 0.00)

yes = 1104, no = 260618, maybe = 37936

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=299658, nc=67423, nnz=194823, k=2] [2.6 MB]
Creating vector for yes... [2.3 MB]
Allocating iteration vectors... [2 x 2.3 MB]
TOTAL: [9.4 MB]

Starting iterations...

Iterative method: 84 iterations in 0.40 seconds (average 0.002095, setup 0.22)

Value in the initial state: 0.9148089022025151

Time for model checking: 1.21 seconds.

Result: 0.9148089022025151 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmax=? [ F "MissionTimeUp" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=3,Objy1=0,Objx2=5,Objy2=1

Prob0A: 67 iterations in 0.73 seconds (average 0.010925, setup 0.00)

Prob1E: 4 iterations in 0.06 seconds (average 0.015000, setup 0.00)

yes = 504, no = 183971, maybe = 115183

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=299658, nc=199996, nnz=551960, k=4] [6.8 MB]
Creating vector for yes... [2.3 MB]
Allocating iteration vectors... [2 x 2.3 MB]
TOTAL: [13.7 MB]

Starting iterations...

Iterative method: 96 iterations in 0.62 seconds (average 0.002708, setup 0.36)

Value in the initial state: 1.9915716674408123E-4

Time for model checking: 1.423 seconds.

Result: 1.9915716674408123E-4 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmin=? [ F "MissionTimeUp" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=3,Objy1=0,Objx2=5,Objy2=1

Prob0E: 82 iterations in 1.01 seconds (average 0.012341, setup 0.00)

Prob1A: 3 iterations in 0.06 seconds (average 0.018667, setup 0.00)

yes = 504, no = 184454, maybe = 114700

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=299658, nc=199127, nnz=550225, k=4] [6.8 MB]
Creating vector for yes... [2.3 MB]
Allocating iteration vectors... [2 x 2.3 MB]
TOTAL: [13.6 MB]

Starting iterations...

Iterative method: 113 iterations in 0.65 seconds (average 0.002761, setup 0.34)

Value in the initial state: 2.2284150193154055E-7

Time for model checking: 1.749 seconds.

Result: 2.2284150193154055E-7 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmax=? [ F "Crash" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=3,Objy1=0,Objx2=5,Objy2=1

Prob0A: 3 iterations in 0.10 seconds (average 0.034667, setup 0.00)

Prob1E: 179 iterations in 3.73 seconds (average 0.020827, setup 0.00)

yes = 146618, no = 3024, maybe = 150016

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=299658, nc=246093, nnz=645928, k=4] [7.9 MB]
Creating vector for yes... [2.3 MB]
Allocating iteration vectors... [2 x 2.3 MB]
TOTAL: [14.8 MB]

Starting iterations...

Iterative method: 89 iterations in 0.67 seconds (average 0.002921, setup 0.41)

Value in the initial state: 0.025326229370436586

Time for model checking: 4.536 seconds.

Result: 0.025326229370436586 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmin=? [ F "Crash" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=3,Objy1=0,Objx2=5,Objy2=1

Prob0E: 3 iterations in 0.11 seconds (average 0.037333, setup 0.00)

Prob1A: 46 iterations in 0.74 seconds (average 0.016174, setup 0.00)

yes = 146618, no = 3024, maybe = 150016

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=299658, nc=246093, nnz=645928, k=4] [7.9 MB]
Creating vector for yes... [2.3 MB]
Allocating iteration vectors... [2 x 2.3 MB]
TOTAL: [14.8 MB]

Starting iterations...

Iterative method: 80 iterations in 0.58 seconds (average 0.003000, setup 0.34)

Value in the initial state: 0.013495281431773478

Time for model checking: 1.458 seconds.

Result: 0.013495281431773478 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmax=? [ F "AllAreaSearched" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=3,Objy1=0,Objx2=5,Objy2=1

Prob0A: 62 iterations in 0.73 seconds (average 0.011742, setup 0.00)

Prob1E: 4 iterations in 0.06 seconds (average 0.016000, setup 0.00)

yes = 1415, no = 238615, maybe = 59628

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=299658, nc=88546, nnz=229335, k=2] [3.0 MB]
Creating vector for yes... [2.3 MB]
Allocating iteration vectors... [2 x 2.3 MB]
TOTAL: [9.9 MB]

Starting iterations...

Iterative method: 93 iterations in 0.40 seconds (average 0.002151, setup 0.20)

Value in the initial state: 0.0639569211314603

Time for model checking: 1.218 seconds.

Result: 0.0639569211314603 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmin=? [ F "AllAreaSearched" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=3,Objy1=0,Objx2=5,Objy2=1

Prob0E: 66 iterations in 0.52 seconds (average 0.007879, setup 0.00)

Prob1A: 3 iterations in 0.05 seconds (average 0.016000, setup 0.00)

yes = 1415, no = 245983, maybe = 52260

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=299658, nc=75734, nnz=193048, k=2] [2.6 MB]
Creating vector for yes... [2.3 MB]
Allocating iteration vectors... [2 x 2.3 MB]
TOTAL: [9.4 MB]

Starting iterations...

Iterative method: 96 iterations in 0.35 seconds (average 0.002042, setup 0.15)

Value in the initial state: 0.00181570998233795

Time for model checking: 0.943 seconds.

Result: 0.00181570998233795 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmax=? [ F "MissionTimeUp"|"AllAreaSearched"|"Crash" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=3,Objy1=0,Objx2=5,Objy2=1

Prob0A: 3 iterations in 0.10 seconds (average 0.034667, setup 0.00)

Prob1E: 184 iterations in 2.83 seconds (average 0.015370, setup 0.00)

yes = 260617, no = 1105, maybe = 37936

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=299658, nc=67423, nnz=194823, k=2] [2.6 MB]
Creating vector for yes... [2.3 MB]
Allocating iteration vectors... [2 x 2.3 MB]
TOTAL: [9.4 MB]

Starting iterations...

Iterative method: 78 iterations in 0.39 seconds (average 0.001897, setup 0.24)

Value in the initial state: 0.08519086193412596

Time for model checking: 3.361 seconds.

Result: 0.08519086193412596 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmin=? [ F "MissionTimeUp"|"AllAreaSearched"|"Crash" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=3,Objy1=0,Objx2=5,Objy2=1

Prob0E: 3 iterations in 0.10 seconds (average 0.033333, setup 0.00)

Prob1A: 47 iterations in 0.53 seconds (average 0.011234, setup 0.00)

yes = 253362, no = 1105, maybe = 45191

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=299658, nc=80744, nnz=233643, k=2] [3.0 MB]
Creating vector for yes... [2.3 MB]
Allocating iteration vectors... [2 x 2.3 MB]
TOTAL: [9.9 MB]

Starting iterations...

Iterative method: 88 iterations in 0.42 seconds (average 0.002091, setup 0.23)

Value in the initial state: 0.015544780585510391

Time for model checking: 1.077 seconds.

Result: 0.015544780585510391 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"drop"}max=? [ F "deadlock" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=3,Objy1=0,Objx2=5,Objy2=1

Prob0E: 4 iterations in 0.09 seconds (average 0.023000, setup 0.00)

Prob1A: 1 iterations in 0.01 seconds (average 0.008000, setup 0.00)

goal = 1, inf = 0, maybe = 299657

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=299658, nc=395734, nnz=795569, k=4] [11.8 MB]
Building sparse matrix (transition rewards)... [n=299658, nc=395734, nnz=0, k=4] [2.7 MB]
Creating vector for state rewards... [2.3 MB]
Creating vector for inf... [2.3 MB]
Allocating iteration vectors... [2 x 2.3 MB]
TOTAL: [23.6 MB]

Starting iterations...

Iterative method: 84 iterations in 0.91 seconds (average 0.004286, setup 0.55)

Value in the initial state: 0.23832128574462239

Time for model checking: 1.027 seconds.

Result: 0.23832128574462239 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"drop"}min=? [ F "deadlock" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=3,Objy1=0,Objx2=5,Objy2=1

Prob0A: 4 iterations in 0.05 seconds (average 0.012000, setup 0.00)

Prob1E: 5 iterations in 0.06 seconds (average 0.012000, setup 0.00)

Warning: PRISM hasn't checked for zero-reward loops.
Your minimum rewards may be too low...

goal = 1, inf = 0, maybe = 299657

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=299658, nc=395734, nnz=795569, k=4] [11.8 MB]
Building sparse matrix (transition rewards)... [n=299658, nc=395734, nnz=0, k=4] [2.7 MB]
Creating vector for state rewards... [2.3 MB]
Creating vector for inf... [2.3 MB]
Allocating iteration vectors... [2 x 2.3 MB]
TOTAL: [23.6 MB]

Starting iterations...

Iterative method: 76 iterations in 0.74 seconds (average 0.004368, setup 0.41)

Value in the initial state: 0.21606520142599525

Time for model checking: 0.878 seconds.

Result: 0.21606520142599525 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"charge"}max=? [ F "deadlock" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=3,Objy1=0,Objx2=5,Objy2=1

Prob0E: 4 iterations in 0.05 seconds (average 0.013000, setup 0.00)

Prob1A: 1 iterations in 0.00 seconds (average 0.004000, setup 0.00)

goal = 1, inf = 0, maybe = 299657

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=299658, nc=395734, nnz=795569, k=4] [11.8 MB]
Building sparse matrix (transition rewards)... [n=299658, nc=395734, nnz=9185, k=4] [2.8 MB]
Creating vector for state rewards... [2.3 MB]
Creating vector for inf... [2.3 MB]
Allocating iteration vectors... [2 x 2.3 MB]
TOTAL: [23.7 MB]

Starting iterations...

Iterative method: 82 iterations in 0.91 seconds (average 0.005707, setup 0.44)

Value in the initial state: 0.20521426225958625

Time for model checking: 0.998 seconds.

Result: 0.20521426225958625 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"charge"}min=? [ F "deadlock" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=3,Objy1=0,Objx2=5,Objy2=1

Prob0A: 4 iterations in 0.03 seconds (average 0.008000, setup 0.00)

Prob1E: 5 iterations in 0.05 seconds (average 0.010400, setup 0.00)

Warning: PRISM hasn't checked for zero-reward loops.
Your minimum rewards may be too low...

goal = 1, inf = 0, maybe = 299657

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=299658, nc=395734, nnz=795569, k=4] [11.8 MB]
Building sparse matrix (transition rewards)... [n=299658, nc=395734, nnz=9185, k=4] [2.8 MB]
Creating vector for state rewards... [2.3 MB]
Creating vector for inf... [2.3 MB]
Allocating iteration vectors... [2 x 2.3 MB]
TOTAL: [23.7 MB]

Starting iterations...

Iterative method: 93 iterations in 0.97 seconds (average 0.004903, setup 0.51)

Value in the initial state: 0.012156203762209912

Time for model checking: 1.071 seconds.

Result: 0.012156203762209912 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"time"}max=? [ F "deadlock" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=3,Objy1=0,Objx2=5,Objy2=1

Prob0E: 4 iterations in 0.03 seconds (average 0.008000, setup 0.00)

Prob1A: 1 iterations in 0.01 seconds (average 0.008000, setup 0.00)

goal = 1, inf = 0, maybe = 299657

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=299658, nc=395734, nnz=795569, k=4] [11.8 MB]
Building sparse matrix (transition rewards)... [n=299658, nc=395734, nnz=149640, k=4] [4.4 MB]
Creating vector for state rewards... [2.3 MB]
Creating vector for inf... [2.3 MB]
Allocating iteration vectors... [2 x 2.3 MB]
TOTAL: [25.3 MB]

Starting iterations...

Iterative method: 94 iterations in 0.90 seconds (average 0.004468, setup 0.48)

Value in the initial state: 139.90425610400834

Time for model checking: 0.961 seconds.

Result: 139.90425610400834 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"time"}min=? [ F "deadlock" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=3,Objy1=0,Objx2=5,Objy2=1

Prob0A: 4 iterations in 0.03 seconds (average 0.008000, setup 0.00)

Prob1E: 5 iterations in 0.06 seconds (average 0.012000, setup 0.00)

Warning: PRISM hasn't checked for zero-reward loops.
Your minimum rewards may be too low...

goal = 1, inf = 0, maybe = 299657

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=299658, nc=395734, nnz=795569, k=4] [11.8 MB]
Building sparse matrix (transition rewards)... [n=299658, nc=395734, nnz=149640, k=4] [4.4 MB]
Creating vector for state rewards... [2.3 MB]
Creating vector for inf... [2.3 MB]
Allocating iteration vectors... [2 x 2.3 MB]
TOTAL: [25.3 MB]

Starting iterations...

Iterative method: 95 iterations in 0.88 seconds (average 0.004421, setup 0.46)

Value in the initial state: 74.89898440510358

Time for model checking: 1.002 seconds.

Result: 74.89898440510358 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"success"}max=? [ F "deadlock" ]/Pmin=? [ F "MissionSuccessful" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=3,Objy1=0,Objx2=5,Objy2=1

Prob0E: 4 iterations in 0.03 seconds (average 0.008000, setup 0.00)

Prob1A: 1 iterations in 0.01 seconds (average 0.008000, setup 0.00)

goal = 1, inf = 0, maybe = 299657

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=299658, nc=395734, nnz=795569, k=4] [11.8 MB]
Building sparse matrix (transition rewards)... [n=299658, nc=395734, nnz=2174, k=4] [2.7 MB]
Creating vector for state rewards... [2.3 MB]
Creating vector for inf... [2.3 MB]
Allocating iteration vectors... [2 x 2.3 MB]
TOTAL: [23.6 MB]

Starting iterations...

Iterative method: 90 iterations in 0.80 seconds (average 0.004311, setup 0.42)

Prob0E: 48 iterations in 0.61 seconds (average 0.012750, setup 0.00)

Prob1A: 3 iterations in 0.06 seconds (average 0.018667, setup 0.00)

yes = 1104, no = 260618, maybe = 37936

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=299658, nc=67423, nnz=194823, k=2] [2.6 MB]
Creating vector for yes... [2.3 MB]
Allocating iteration vectors... [2 x 2.3 MB]
TOTAL: [9.4 MB]

Starting iterations...

Iterative method: 84 iterations in 0.38 seconds (average 0.002095, setup 0.21)

Value in the initial state: 150.1078559809884

Time for model checking: 1.96 seconds.

Result: 150.1078559809884 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"success"}min=? [ F "deadlock" ]/Pmax=? [ F "MissionSuccessful" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=3,Objy1=0,Objx2=5,Objy2=1

Prob0A: 4 iterations in 0.12 seconds (average 0.029000, setup 0.00)

Prob1E: 5 iterations in 0.08 seconds (average 0.015200, setup 0.00)

Warning: PRISM hasn't checked for zero-reward loops.
Your minimum rewards may be too low...

goal = 1, inf = 0, maybe = 299657

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=299658, nc=395734, nnz=795569, k=4] [11.8 MB]
Building sparse matrix (transition rewards)... [n=299658, nc=395734, nnz=2174, k=4] [2.7 MB]
Creating vector for state rewards... [2.3 MB]
Creating vector for inf... [2.3 MB]
Allocating iteration vectors... [2 x 2.3 MB]
TOTAL: [23.6 MB]

Starting iterations...

Iterative method: 90 iterations in 1.11 seconds (average 0.004489, setup 0.70)

Prob0A: 47 iterations in 0.32 seconds (average 0.006723, setup 0.00)

Prob1E: 4 iterations in 0.05 seconds (average 0.013000, setup 0.00)

yes = 1104, no = 253363, maybe = 45191

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=299658, nc=80744, nnz=233643, k=2] [3.0 MB]
Creating vector for yes... [2.3 MB]
Allocating iteration vectors... [2 x 2.3 MB]
TOTAL: [9.9 MB]

Starting iterations...

Iterative method: 89 iterations in 0.39 seconds (average 0.002022, setup 0.21)

Value in the initial state: 71.36777207913694

Time for model checking: 2.118 seconds.

Result: 71.36777207913694 (value in the initial state)

---------------------------------------------------------------------

Note: There were 16 warnings during computation.

