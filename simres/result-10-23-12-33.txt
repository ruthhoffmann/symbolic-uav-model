PRISM
=====

Version: 4.3
Date: Wed Aug 03 16:09:54 BST 2016
Hostname: eglinton
Memory limits: cudd=1g, java(heap)=981.5m
Command line: prism uav_sim_mdp.prism -s uav_mdp.props -const 'Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=1,Objy1=2,Objx2=3,Objy2=3,'

Parsing model file "uav_sim_mdp.prism"...

Parsing properties file "uav_mdp.props"...

18 properties:
(1) Pmax=? [ F "MissionSuccessful" ]
(2) Pmin=? [ F "MissionSuccessful" ]
(3) Pmax=? [ F "MissionTimeUp" ]
(4) Pmin=? [ F "MissionTimeUp" ]
(5) Pmax=? [ F "Crash" ]
(6) Pmin=? [ F "Crash" ]
(7) Pmax=? [ F "AllAreaSearched" ]
(8) Pmin=? [ F "AllAreaSearched" ]
(9) Pmax=? [ F "MissionTimeUp"|"AllAreaSearched"|"Crash" ]
(10) Pmin=? [ F "MissionTimeUp"|"AllAreaSearched"|"Crash" ]
(11) R{"drop"}max=? [ F "deadlock" ]
(12) R{"drop"}min=? [ F "deadlock" ]
(13) R{"charge"}max=? [ F "deadlock" ]
(14) R{"charge"}min=? [ F "deadlock" ]
(15) R{"time"}max=? [ F "deadlock" ]
(16) R{"time"}min=? [ F "deadlock" ]
(17) R{"success"}max=? [ F "deadlock" ]/Pmin=? [ F "MissionSuccessful" ]
(18) R{"success"}min=? [ F "deadlock" ]/Pmax=? [ F "MissionSuccessful" ]

Type:        MDP
Modules:     Obj Move Robot T 
Variables:   NoOfObjs posx posy s1 retx rety b t c 

---------------------------------------------------------------------

Model checking: Pmax=? [ F "MissionSuccessful" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=1,Objy1=2,Objx2=3,Objy2=3

Building model...
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=1,Objy1=2,Objx2=3,Objy2=3

Warning: Guard for command 7 of module "Move" is never satisfied.

Warning: Guard for command 8 of module "Move" is never satisfied.

Warning: Guard for command 9 of module "Move" is never satisfied.

Warning: Guard for command 7 of module "Robot" is never satisfied.

Warning: Update 2 of command 3 of module "Robot" doesn't do anything ("(s1'=13)", line 210, column 44)

Warning: Update 2 of command 27 of module "T" doesn't do anything ("(c'=1)", line 323, column 108)

Warning: Guard for command 28 of module "T" is never satisfied.

Warning: Update 2 of command 9 of module "T" doesn't do anything ("(c'=1)", line 300, column 112)

Warning: Guard for command 10 of module "T" is never satisfied.

Warning: Update 2 of command 51 of module "T" doesn't do anything ("(c'=1)", line 354, column 113)

Warning: Guard for command 52 of module "T" is never satisfied.

Computing reachable states...

Reachability (BFS): 87 iterations in 0.91 seconds (average 0.010437, setup 0.00)

Time for model construction: 3.086 seconds.

Warning: Deadlocks detected and fixed in 1 states

Type:        MDP
States:      164837 (1 initial)
Transitions: 423683
Choices:     218836

Transition matrix: 177008 nodes (87 terminal), 423683 minterms, vars: 34r/34c/18nd

Prob0A: 51 iterations in 0.34 seconds (average 0.006667, setup 0.00)

Prob1E: 4 iterations in 0.04 seconds (average 0.011000, setup 0.00)

yes = 1047, no = 133952, maybe = 29838

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=164837, nc=51296, nnz=138201, k=2] [1.8 MB]
Creating vector for yes... [1.3 MB]
Allocating iteration vectors... [2 x 1.3 MB]
TOTAL: [5.6 MB]

Starting iterations...

Iterative method: 91 iterations in 0.32 seconds (average 0.001143, setup 0.22)

Value in the initial state: 0.982831429566157

Time for model checking: 0.74 seconds.

Result: 0.982831429566157 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmin=? [ F "MissionSuccessful" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=1,Objy1=2,Objx2=3,Objy2=3

Prob0E: 51 iterations in 0.37 seconds (average 0.007216, setup 0.00)

Prob1A: 3 iterations in 0.04 seconds (average 0.013333, setup 0.00)

yes = 1047, no = 137680, maybe = 26110

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=164837, nc=44881, nnz=120394, k=2] [1.6 MB]
Creating vector for yes... [1.3 MB]
Allocating iteration vectors... [2 x 1.3 MB]
TOTAL: [5.4 MB]

Starting iterations...

Iterative method: 90 iterations in 0.27 seconds (average 0.001111, setup 0.17)

Value in the initial state: 0.871072536947162

Time for model checking: 0.72 seconds.

Result: 0.871072536947162 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmax=? [ F "MissionTimeUp" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=1,Objy1=2,Objx2=3,Objy2=3

Prob0A: 76 iterations in 0.60 seconds (average 0.007947, setup 0.00)

Prob1E: 4 iterations in 0.03 seconds (average 0.008000, setup 0.00)

yes = 244, no = 100495, maybe = 64098

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=164837, nc=106286, nnz=280869, k=4] [3.5 MB]
Creating vector for yes... [1.3 MB]
Allocating iteration vectors... [2 x 1.3 MB]
TOTAL: [7.2 MB]

Starting iterations...

Iterative method: 97 iterations in 0.35 seconds (average 0.001402, setup 0.22)

Value in the initial state: 2.7626526188819526E-5

Time for model checking: 1.012 seconds.

Result: 2.7626526188819526E-5 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmin=? [ F "MissionTimeUp" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=1,Objy1=2,Objx2=3,Objy2=3

Prob0E: 90 iterations in 0.58 seconds (average 0.006400, setup 0.00)

Prob1A: 3 iterations in 0.03 seconds (average 0.009333, setup 0.00)

yes = 244, no = 100719, maybe = 63874

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=164837, nc=105899, nnz=280003, k=4] [3.5 MB]
Creating vector for yes... [1.3 MB]
Allocating iteration vectors... [2 x 1.3 MB]
TOTAL: [7.2 MB]

Starting iterations...

Iterative method: 113 iterations in 0.32 seconds (average 0.001416, setup 0.16)

Value in the initial state: 1.4514447886359448E-8

Time for model checking: 0.95 seconds.

Result: 1.4514447886359448E-8 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmax=? [ F "Crash" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=1,Objy1=2,Objx2=3,Objy2=3

Prob0A: 3 iterations in 0.06 seconds (average 0.020000, setup 0.00)

Prob1E: 196 iterations in 2.19 seconds (average 0.011163, setup 0.00)

yes = 80305, no = 2826, maybe = 81706

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=164837, nc=135705, nnz=340552, k=4] [4.2 MB]
Creating vector for yes... [1.3 MB]
Allocating iteration vectors... [2 x 1.3 MB]
TOTAL: [8.0 MB]

Starting iterations...

Iterative method: 92 iterations in 0.39 seconds (average 0.001565, setup 0.24)

Value in the initial state: 0.027496044682663656

Time for model checking: 2.652 seconds.

Result: 0.027496044682663656 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmin=? [ F "Crash" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=1,Objy1=2,Objx2=3,Objy2=3

Prob0E: 3 iterations in 0.05 seconds (average 0.016000, setup 0.00)

Prob1A: 51 iterations in 0.40 seconds (average 0.007765, setup 0.00)

yes = 80305, no = 2826, maybe = 81706

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=164837, nc=135705, nnz=340552, k=4] [4.2 MB]
Creating vector for yes... [1.3 MB]
Allocating iteration vectors... [2 x 1.3 MB]
TOTAL: [8.0 MB]

Starting iterations...

Iterative method: 84 iterations in 0.29 seconds (average 0.001571, setup 0.16)

Value in the initial state: 0.015125677701619086

Time for model checking: 0.747 seconds.

Result: 0.015125677701619086 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmax=? [ F "AllAreaSearched" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=1,Objy1=2,Objx2=3,Objy2=3

Prob0A: 53 iterations in 0.40 seconds (average 0.007547, setup 0.00)

Prob1E: 4 iterations in 0.04 seconds (average 0.009000, setup 0.00)

yes = 1534, no = 116190, maybe = 47113

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=164837, nc=77126, nnz=199184, k=2] [2.5 MB]
Creating vector for yes... [1.3 MB]
Allocating iteration vectors... [2 x 1.3 MB]
TOTAL: [6.3 MB]

Starting iterations...

Iterative method: 82 iterations in 0.27 seconds (average 0.001268, setup 0.17)

Value in the initial state: 0.10516725901256085

Time for model checking: 0.731 seconds.

Result: 0.10516725901256085 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmin=? [ F "AllAreaSearched" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=1,Objy1=2,Objx2=3,Objy2=3

Prob0E: 53 iterations in 0.28 seconds (average 0.005358, setup 0.00)

Prob1A: 3 iterations in 0.02 seconds (average 0.008000, setup 0.00)

yes = 1534, no = 121658, maybe = 41645

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=164837, nc=67858, nnz=173542, k=2] [2.2 MB]
Creating vector for yes... [1.3 MB]
Allocating iteration vectors... [2 x 1.3 MB]
TOTAL: [6.0 MB]

Starting iterations...

Iterative method: 92 iterations in 0.22 seconds (average 0.001261, setup 0.10)

Value in the initial state: 0.001852396006118012

Time for model checking: 0.547 seconds.

Result: 0.001852396006118012 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmax=? [ F "MissionTimeUp"|"AllAreaSearched"|"Crash" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=1,Objy1=2,Objx2=3,Objy2=3

Prob0A: 3 iterations in 0.06 seconds (average 0.020000, setup 0.00)

Prob1E: 200 iterations in 2.21 seconds (average 0.011060, setup 0.00)

yes = 137679, no = 1048, maybe = 26110

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=164837, nc=44881, nnz=120394, k=2] [1.6 MB]
Creating vector for yes... [1.3 MB]
Allocating iteration vectors... [2 x 1.3 MB]
TOTAL: [5.4 MB]

Starting iterations...

Iterative method: 83 iterations in 0.25 seconds (average 0.001157, setup 0.16)

Value in the initial state: 0.12892714228381244

Time for model checking: 2.545 seconds.

Result: 0.12892714228381244 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmin=? [ F "MissionTimeUp"|"AllAreaSearched"|"Crash" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=1,Objy1=2,Objx2=3,Objy2=3

Prob0E: 3 iterations in 0.03 seconds (average 0.010667, setup 0.00)

Prob1A: 51 iterations in 0.40 seconds (average 0.007765, setup 0.00)

yes = 133951, no = 1048, maybe = 29838

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=164837, nc=51296, nnz=138201, k=2] [1.8 MB]
Creating vector for yes... [1.3 MB]
Allocating iteration vectors... [2 x 1.3 MB]
TOTAL: [5.6 MB]

Starting iterations...

Iterative method: 87 iterations in 0.23 seconds (average 0.001149, setup 0.13)

Value in the initial state: 0.01716724620677385

Time for model checking: 0.693 seconds.

Result: 0.01716724620677385 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"drop"}max=? [ F "deadlock" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=1,Objy1=2,Objx2=3,Objy2=3

Prob0E: 4 iterations in 0.04 seconds (average 0.011000, setup 0.00)

Prob1A: 1 iterations in 0.00 seconds (average 0.004000, setup 0.00)

goal = 1, inf = 0, maybe = 164836

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=164837, nc=218835, nnz=423682, k=4] [6.3 MB]
Building sparse matrix (transition rewards)... [n=164837, nc=218835, nnz=0, k=4] [1.5 MB]
Creating vector for state rewards... [1.3 MB]
Creating vector for inf... [1.3 MB]
Allocating iteration vectors... [2 x 1.3 MB]
TOTAL: [12.8 MB]

Starting iterations...

Iterative method: 87 iterations in 0.52 seconds (average 0.002345, setup 0.32)

Value in the initial state: 0.11981886238608327

Time for model checking: 0.579 seconds.

Result: 0.11981886238608327 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"drop"}min=? [ F "deadlock" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=1,Objy1=2,Objx2=3,Objy2=3

Prob0A: 4 iterations in 0.03 seconds (average 0.007000, setup 0.00)

Prob1E: 5 iterations in 0.04 seconds (average 0.008800, setup 0.00)

Warning: PRISM hasn't checked for zero-reward loops.
Your minimum rewards may be too low...

goal = 1, inf = 0, maybe = 164836

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=164837, nc=218835, nnz=423682, k=4] [6.3 MB]
Building sparse matrix (transition rewards)... [n=164837, nc=218835, nnz=0, k=4] [1.5 MB]
Creating vector for state rewards... [1.3 MB]
Creating vector for inf... [1.3 MB]
Allocating iteration vectors... [2 x 1.3 MB]
TOTAL: [12.8 MB]

Starting iterations...

Iterative method: 82 iterations in 0.42 seconds (average 0.002293, setup 0.24)

Value in the initial state: 0.10789489151888548

Time for model checking: 0.51 seconds.

Result: 0.10789489151888548 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"charge"}max=? [ F "deadlock" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=1,Objy1=2,Objx2=3,Objy2=3

Prob0E: 4 iterations in 0.02 seconds (average 0.005000, setup 0.00)

Prob1A: 1 iterations in 0.01 seconds (average 0.008000, setup 0.00)

goal = 1, inf = 0, maybe = 164836

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=164837, nc=218835, nnz=423682, k=4] [6.3 MB]
Building sparse matrix (transition rewards)... [n=164837, nc=218835, nnz=4398, k=4] [1.5 MB]
Creating vector for state rewards... [1.3 MB]
Creating vector for inf... [1.3 MB]
Allocating iteration vectors... [2 x 1.3 MB]
TOTAL: [12.9 MB]

Starting iterations...

Iterative method: 89 iterations in 0.44 seconds (average 0.002382, setup 0.23)

Value in the initial state: 0.2340707160582634

Time for model checking: 0.481 seconds.

Result: 0.2340707160582634 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"charge"}min=? [ F "deadlock" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=1,Objy1=2,Objx2=3,Objy2=3

Prob0A: 4 iterations in 0.02 seconds (average 0.005000, setup 0.00)

Prob1E: 5 iterations in 0.04 seconds (average 0.008800, setup 0.00)

Warning: PRISM hasn't checked for zero-reward loops.
Your minimum rewards may be too low...

goal = 1, inf = 0, maybe = 164836

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=164837, nc=218835, nnz=423682, k=4] [6.3 MB]
Building sparse matrix (transition rewards)... [n=164837, nc=218835, nnz=4398, k=4] [1.5 MB]
Creating vector for state rewards... [1.3 MB]
Creating vector for inf... [1.3 MB]
Allocating iteration vectors... [2 x 1.3 MB]
TOTAL: [12.9 MB]

Starting iterations...

Iterative method: 93 iterations in 0.46 seconds (average 0.002409, setup 0.24)

Value in the initial state: 0.01706134619920658

Time for model checking: 0.535 seconds.

Result: 0.01706134619920658 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"time"}max=? [ F "deadlock" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=1,Objy1=2,Objx2=3,Objy2=3

Prob0E: 4 iterations in 0.02 seconds (average 0.006000, setup 0.00)

Prob1A: 1 iterations in 0.00 seconds (average 0.004000, setup 0.00)

goal = 1, inf = 0, maybe = 164836

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=164837, nc=218835, nnz=423682, k=4] [6.3 MB]
Building sparse matrix (transition rewards)... [n=164837, nc=218835, nnz=83129, k=4] [2.4 MB]
Creating vector for state rewards... [1.3 MB]
Creating vector for inf... [1.3 MB]
Allocating iteration vectors... [2 x 1.3 MB]
TOTAL: [13.8 MB]

Starting iterations...

Iterative method: 95 iterations in 0.49 seconds (average 0.002400, setup 0.26)

Value in the initial state: 151.87732646869804

Time for model checking: 0.529 seconds.

Result: 151.87732646869804 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"time"}min=? [ F "deadlock" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=1,Objy1=2,Objx2=3,Objy2=3

Prob0A: 4 iterations in 0.02 seconds (average 0.006000, setup 0.00)

Prob1E: 5 iterations in 0.04 seconds (average 0.007200, setup 0.00)

Warning: PRISM hasn't checked for zero-reward loops.
Your minimum rewards may be too low...

goal = 1, inf = 0, maybe = 164836

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=164837, nc=218835, nnz=423682, k=4] [6.3 MB]
Building sparse matrix (transition rewards)... [n=164837, nc=218835, nnz=83129, k=4] [2.4 MB]
Creating vector for state rewards... [1.3 MB]
Creating vector for inf... [1.3 MB]
Allocating iteration vectors... [2 x 1.3 MB]
TOTAL: [13.8 MB]

Starting iterations...

Iterative method: 94 iterations in 0.49 seconds (average 0.002468, setup 0.26)

Value in the initial state: 83.9416198145486

Time for model checking: 0.563 seconds.

Result: 83.9416198145486 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"success"}max=? [ F "deadlock" ]/Pmin=? [ F "MissionSuccessful" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=1,Objy1=2,Objx2=3,Objy2=3

Prob0E: 4 iterations in 0.02 seconds (average 0.006000, setup 0.00)

Prob1A: 1 iterations in 0.01 seconds (average 0.008000, setup 0.00)

goal = 1, inf = 0, maybe = 164836

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=164837, nc=218835, nnz=423682, k=4] [6.3 MB]
Building sparse matrix (transition rewards)... [n=164837, nc=218835, nnz=2064, k=4] [1.5 MB]
Creating vector for state rewards... [1.3 MB]
Creating vector for inf... [1.3 MB]
Allocating iteration vectors... [2 x 1.3 MB]
TOTAL: [12.8 MB]

Starting iterations...

Iterative method: 95 iterations in 0.46 seconds (average 0.002358, setup 0.23)

Prob0E: 51 iterations in 0.34 seconds (average 0.006667, setup 0.00)

Prob1A: 3 iterations in 0.04 seconds (average 0.013333, setup 0.00)

yes = 1047, no = 137680, maybe = 26110

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=164837, nc=44881, nnz=120394, k=2] [1.6 MB]
Creating vector for yes... [1.3 MB]
Allocating iteration vectors... [2 x 1.3 MB]
TOTAL: [5.4 MB]

Starting iterations...

Iterative method: 90 iterations in 0.20 seconds (average 0.001067, setup 0.10)

Value in the initial state: 171.5957314018365

Time for model checking: 1.095 seconds.

Result: 171.5957314018365 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"success"}min=? [ F "deadlock" ]/Pmax=? [ F "MissionSuccessful" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=1,Objy1=2,Objx2=3,Objy2=3

Prob0A: 4 iterations in 0.03 seconds (average 0.007000, setup 0.00)

Prob1E: 5 iterations in 0.05 seconds (average 0.009600, setup 0.00)

Warning: PRISM hasn't checked for zero-reward loops.
Your minimum rewards may be too low...

goal = 1, inf = 0, maybe = 164836

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=164837, nc=218835, nnz=423682, k=4] [6.3 MB]
Building sparse matrix (transition rewards)... [n=164837, nc=218835, nnz=2064, k=4] [1.5 MB]
Creating vector for state rewards... [1.3 MB]
Creating vector for inf... [1.3 MB]
Allocating iteration vectors... [2 x 1.3 MB]
TOTAL: [12.8 MB]

Starting iterations...

Iterative method: 93 iterations in 0.46 seconds (average 0.002409, setup 0.24)

Prob0A: 51 iterations in 0.30 seconds (average 0.005804, setup 0.00)

Prob1E: 4 iterations in 0.03 seconds (average 0.008000, setup 0.00)

yes = 1047, no = 133952, maybe = 29838

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=164837, nc=51296, nnz=138201, k=2] [1.8 MB]
Creating vector for yes... [1.3 MB]
Allocating iteration vectors... [2 x 1.3 MB]
TOTAL: [5.6 MB]

Starting iterations...

Iterative method: 91 iterations in 0.22 seconds (average 0.001143, setup 0.11)

Value in the initial state: 77.81058085855287

Time for model checking: 1.107 seconds.

Result: 77.81058085855287 (value in the initial state)

---------------------------------------------------------------------

Note: There were 16 warnings during computation.

