PRISM
=====

Version: 4.3
Date: Wed Aug 03 15:56:06 BST 2016
Hostname: eglinton
Memory limits: cudd=1g, java(heap)=981.5m
Command line: prism uav_sim_mdp.prism -s uav_mdp.props -const 'Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=1,Objy1=3,Objx2=6,Objy2=2,'

Parsing model file "uav_sim_mdp.prism"...

Parsing properties file "uav_mdp.props"...

18 properties:
(1) Pmax=? [ F "MissionSuccessful" ]
(2) Pmin=? [ F "MissionSuccessful" ]
(3) Pmax=? [ F "MissionTimeUp" ]
(4) Pmin=? [ F "MissionTimeUp" ]
(5) Pmax=? [ F "Crash" ]
(6) Pmin=? [ F "Crash" ]
(7) Pmax=? [ F "AllAreaSearched" ]
(8) Pmin=? [ F "AllAreaSearched" ]
(9) Pmax=? [ F "MissionTimeUp"|"AllAreaSearched"|"Crash" ]
(10) Pmin=? [ F "MissionTimeUp"|"AllAreaSearched"|"Crash" ]
(11) R{"drop"}max=? [ F "deadlock" ]
(12) R{"drop"}min=? [ F "deadlock" ]
(13) R{"charge"}max=? [ F "deadlock" ]
(14) R{"charge"}min=? [ F "deadlock" ]
(15) R{"time"}max=? [ F "deadlock" ]
(16) R{"time"}min=? [ F "deadlock" ]
(17) R{"success"}max=? [ F "deadlock" ]/Pmin=? [ F "MissionSuccessful" ]
(18) R{"success"}min=? [ F "deadlock" ]/Pmax=? [ F "MissionSuccessful" ]

Type:        MDP
Modules:     Obj Move Robot T 
Variables:   NoOfObjs posx posy s1 retx rety b t c 

---------------------------------------------------------------------

Model checking: Pmax=? [ F "MissionSuccessful" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=1,Objy1=3,Objx2=6,Objy2=2

Building model...
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=1,Objy1=3,Objx2=6,Objy2=2

Warning: Guard for command 7 of module "Move" is never satisfied.

Warning: Guard for command 8 of module "Move" is never satisfied.

Warning: Guard for command 9 of module "Move" is never satisfied.

Warning: Guard for command 7 of module "Robot" is never satisfied.

Warning: Update 2 of command 3 of module "Robot" doesn't do anything ("(s1'=13)", line 210, column 44)

Warning: Update 2 of command 27 of module "T" doesn't do anything ("(c'=1)", line 323, column 108)

Warning: Guard for command 28 of module "T" is never satisfied.

Warning: Update 2 of command 9 of module "T" doesn't do anything ("(c'=1)", line 300, column 112)

Warning: Guard for command 10 of module "T" is never satisfied.

Warning: Update 2 of command 51 of module "T" doesn't do anything ("(c'=1)", line 354, column 113)

Warning: Guard for command 52 of module "T" is never satisfied.

Computing reachable states...

Reachability (BFS): 90 iterations in 1.10 seconds (average 0.012178, setup 0.00)

Time for model construction: 3.279 seconds.

Warning: Deadlocks detected and fixed in 1 states

Type:        MDP
States:      209857 (1 initial)
Transitions: 592704
Choices:     288320

Transition matrix: 227944 nodes (93 terminal), 592704 minterms, vars: 34r/34c/18nd

Prob0A: 56 iterations in 0.49 seconds (average 0.008714, setup 0.00)

Prob1E: 4 iterations in 0.06 seconds (average 0.016000, setup 0.00)

yes = 1057, no = 172699, maybe = 36101

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=209857, nc=63632, nnz=180603, k=2] [2.3 MB]
Creating vector for yes... [1.6 MB]
Allocating iteration vectors... [2 x 1.6 MB]
TOTAL: [7.1 MB]

Starting iterations...

Iterative method: 103 iterations in 0.42 seconds (average 0.001515, setup 0.26)

Value in the initial state: 0.9792043136946514

Time for model checking: 1.002 seconds.

Result: 0.9792043136946514 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmin=? [ F "MissionSuccessful" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=1,Objy1=3,Objx2=6,Objy2=2

Prob0E: 56 iterations in 0.54 seconds (average 0.009643, setup 0.00)

Prob1A: 3 iterations in 0.07 seconds (average 0.022667, setup 0.00)

yes = 1057, no = 179706, maybe = 29094

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=209857, nc=50825, nnz=142672, k=2] [1.9 MB]
Creating vector for yes... [1.6 MB]
Allocating iteration vectors... [2 x 1.6 MB]
TOTAL: [6.7 MB]

Starting iterations...

Iterative method: 94 iterations in 0.36 seconds (average 0.001532, setup 0.21)

Value in the initial state: 0.9621080307174363

Time for model checking: 0.99 seconds.

Result: 0.9621080307174363 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmax=? [ F "MissionTimeUp" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=1,Objy1=3,Objx2=6,Objy2=2

Prob0A: 78 iterations in 0.74 seconds (average 0.009436, setup 0.00)

Prob1E: 4 iterations in 0.05 seconds (average 0.013000, setup 0.00)

yes = 284, no = 121539, maybe = 88034

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=209857, nc=153609, nnz=424642, k=4] [5.2 MB]
Creating vector for yes... [1.6 MB]
Allocating iteration vectors... [2 x 1.6 MB]
TOTAL: [10.0 MB]

Starting iterations...

Iterative method: 101 iterations in 0.51 seconds (average 0.001901, setup 0.32)

Value in the initial state: 0.0012683347932659547

Time for model checking: 1.325 seconds.

Result: 0.0012683347932659547 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmin=? [ F "MissionTimeUp" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=1,Objy1=3,Objx2=6,Objy2=2

Prob0E: 93 iterations in 0.97 seconds (average 0.010452, setup 0.00)

Prob1A: 3 iterations in 0.05 seconds (average 0.016000, setup 0.00)

yes = 284, no = 121637, maybe = 87936

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=209857, nc=153455, nnz=424180, k=4] [5.2 MB]
Creating vector for yes... [1.6 MB]
Allocating iteration vectors... [2 x 1.6 MB]
TOTAL: [10.0 MB]

Starting iterations...

Iterative method: 116 iterations in 0.51 seconds (average 0.001931, setup 0.28)

Value in the initial state: 3.246856158176058E-6

Time for model checking: 1.564 seconds.

Result: 3.246856158176058E-6 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmax=? [ F "Crash" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=1,Objy1=3,Objx2=6,Objy2=2

Prob0A: 3 iterations in 0.09 seconds (average 0.029333, setup 0.00)

Prob1E: 212 iterations in 3.12 seconds (average 0.014736, setup 0.00)

yes = 102623, no = 3020, maybe = 104214

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=209857, nc=182677, nnz=487061, k=4] [5.9 MB]
Creating vector for yes... [1.6 MB]
Allocating iteration vectors... [2 x 1.6 MB]
TOTAL: [10.8 MB]

Starting iterations...

Iterative method: 99 iterations in 0.55 seconds (average 0.002020, setup 0.35)

Value in the initial state: 0.03110905755852053

Time for model checking: 3.791 seconds.

Result: 0.03110905755852053 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmin=? [ F "Crash" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=1,Objy1=3,Objx2=6,Objy2=2

Prob0E: 4 iterations in 0.08 seconds (average 0.019000, setup 0.00)

Prob1A: 55 iterations in 0.56 seconds (average 0.010182, setup 0.00)

yes = 102623, no = 3020, maybe = 104214

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=209857, nc=182677, nnz=487061, k=4] [5.9 MB]
Creating vector for yes... [1.6 MB]
Allocating iteration vectors... [2 x 1.6 MB]
TOTAL: [10.8 MB]

Starting iterations...

Iterative method: 103 iterations in 0.47 seconds (average 0.002097, setup 0.26)

Value in the initial state: 0.01892901771885569

Time for model checking: 1.129 seconds.

Result: 0.01892901771885569 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmax=? [ F "AllAreaSearched" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=1,Objy1=3,Objx2=6,Objy2=2

Prob0A: 55 iterations in 0.44 seconds (average 0.008073, setup 0.00)

Prob1E: 4 iterations in 0.07 seconds (average 0.017000, setup 0.00)

yes = 1678, no = 154568, maybe = 53611

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=209857, nc=92536, nnz=253707, k=2] [3.2 MB]
Creating vector for yes... [1.6 MB]
Allocating iteration vectors... [2 x 1.6 MB]
TOTAL: [8.0 MB]

Starting iterations...

Iterative method: 98 iterations in 0.40 seconds (average 0.001633, setup 0.24)

Value in the initial state: 0.006442482124636567

Time for model checking: 0.935 seconds.

Result: 0.006442482124636567 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmin=? [ F "AllAreaSearched" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=1,Objy1=3,Objx2=6,Objy2=2

Prob0E: 56 iterations in 0.48 seconds (average 0.008643, setup 0.00)

Prob1A: 3 iterations in 0.05 seconds (average 0.017333, setup 0.00)

yes = 1678, no = 162764, maybe = 45415

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=209857, nc=77712, nnz=210039, k=2] [2.7 MB]
Creating vector for yes... [1.6 MB]
Allocating iteration vectors... [2 x 1.6 MB]
TOTAL: [7.5 MB]

Starting iterations...

Iterative method: 100 iterations in 0.46 seconds (average 0.002640, setup 0.20)

Value in the initial state: 0.0018232219929861034

Time for model checking: 1.033 seconds.

Result: 0.0018232219929861034 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmax=? [ F "MissionTimeUp"|"AllAreaSearched"|"Crash" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=1,Objy1=3,Objx2=6,Objy2=2

Prob0A: 3 iterations in 0.10 seconds (average 0.033333, setup 0.00)

Prob1E: 216 iterations in 3.59 seconds (average 0.016630, setup 0.00)

yes = 179705, no = 1058, maybe = 29094

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=209857, nc=50825, nnz=142672, k=2] [1.9 MB]
Creating vector for yes... [1.6 MB]
Allocating iteration vectors... [2 x 1.6 MB]
TOTAL: [6.7 MB]

Starting iterations...

Iterative method: 91 iterations in 0.32 seconds (average 0.001363, setup 0.20)

Value in the initial state: 0.03789192321069188

Time for model checking: 4.058 seconds.

Result: 0.03789192321069188 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmin=? [ F "MissionTimeUp"|"AllAreaSearched"|"Crash" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=1,Objy1=3,Objx2=6,Objy2=2

Prob0E: 4 iterations in 0.09 seconds (average 0.023000, setup 0.00)

Prob1A: 56 iterations in 0.53 seconds (average 0.009500, setup 0.00)

yes = 172698, no = 1058, maybe = 36101

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=209857, nc=63632, nnz=180603, k=2] [2.3 MB]
Creating vector for yes... [1.6 MB]
Allocating iteration vectors... [2 x 1.6 MB]
TOTAL: [7.1 MB]

Starting iterations...

Iterative method: 101 iterations in 0.32 seconds (average 0.001465, setup 0.17)

Value in the initial state: 0.020794957828795764

Time for model checking: 0.95 seconds.

Result: 0.020794957828795764 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"drop"}max=? [ F "deadlock" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=1,Objy1=3,Objx2=6,Objy2=2

Prob0E: 5 iterations in 0.06 seconds (average 0.012000, setup 0.00)

Prob1A: 1 iterations in 0.01 seconds (average 0.008000, setup 0.00)

goal = 1, inf = 0, maybe = 209856

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=209857, nc=288319, nnz=592703, k=4] [8.7 MB]
Building sparse matrix (transition rewards)... [n=209857, nc=288319, nnz=0, k=4] [1.9 MB]
Creating vector for state rewards... [1.6 MB]
Creating vector for inf... [1.6 MB]
Allocating iteration vectors... [2 x 1.6 MB]
TOTAL: [17.0 MB]

Starting iterations...

Iterative method: 100 iterations in 0.74 seconds (average 0.003160, setup 0.42)

Value in the initial state: 0.20488606901202883

Time for model checking: 0.814 seconds.

Result: 0.20488606901202883 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"drop"}min=? [ F "deadlock" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=1,Objy1=3,Objx2=6,Objy2=2

Prob0A: 4 iterations in 0.04 seconds (average 0.010000, setup 0.00)

Prob1E: 5 iterations in 0.05 seconds (average 0.010400, setup 0.00)

Warning: PRISM hasn't checked for zero-reward loops.
Your minimum rewards may be too low...

goal = 1, inf = 0, maybe = 209856

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=209857, nc=288319, nnz=592703, k=4] [8.7 MB]
Building sparse matrix (transition rewards)... [n=209857, nc=288319, nnz=0, k=4] [1.9 MB]
Creating vector for state rewards... [1.6 MB]
Creating vector for inf... [1.6 MB]
Allocating iteration vectors... [2 x 1.6 MB]
TOTAL: [17.0 MB]

Starting iterations...

Iterative method: 89 iterations in 0.59 seconds (average 0.003056, setup 0.32)

Value in the initial state: 0.19150061371863714

Time for model checking: 0.697 seconds.

Result: 0.19150061371863714 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"charge"}max=? [ F "deadlock" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=1,Objy1=3,Objx2=6,Objy2=2

Prob0E: 5 iterations in 0.04 seconds (average 0.007200, setup 0.00)

Prob1A: 1 iterations in 0.00 seconds (average 0.004000, setup 0.00)

goal = 1, inf = 0, maybe = 209856

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=209857, nc=288319, nnz=592703, k=4] [8.7 MB]
Building sparse matrix (transition rewards)... [n=209857, nc=288319, nnz=7526, k=4] [2.0 MB]
Creating vector for state rewards... [1.6 MB]
Creating vector for inf... [1.6 MB]
Allocating iteration vectors... [2 x 1.6 MB]
TOTAL: [17.1 MB]

Starting iterations...

Iterative method: 95 iterations in 0.61 seconds (average 0.003074, setup 0.32)

Value in the initial state: 0.9852718781210462

Time for model checking: 0.664 seconds.

Result: 0.9852718781210462 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"charge"}min=? [ F "deadlock" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=1,Objy1=3,Objx2=6,Objy2=2

Prob0A: 4 iterations in 0.03 seconds (average 0.008000, setup 0.00)

Prob1E: 5 iterations in 0.05 seconds (average 0.009600, setup 0.00)

Warning: PRISM hasn't checked for zero-reward loops.
Your minimum rewards may be too low...

goal = 1, inf = 0, maybe = 209856

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=209857, nc=288319, nnz=592703, k=4] [8.7 MB]
Building sparse matrix (transition rewards)... [n=209857, nc=288319, nnz=7526, k=4] [2.0 MB]
Creating vector for state rewards... [1.6 MB]
Creating vector for inf... [1.6 MB]
Allocating iteration vectors... [2 x 1.6 MB]
TOTAL: [17.1 MB]

Starting iterations...

Iterative method: 104 iterations in 0.64 seconds (average 0.003154, setup 0.32)

Value in the initial state: 0.06316613626310784

Time for model checking: 0.739 seconds.

Result: 0.06316613626310784 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"time"}max=? [ F "deadlock" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=1,Objy1=3,Objx2=6,Objy2=2

Prob0E: 5 iterations in 0.03 seconds (average 0.006400, setup 0.00)

Prob1A: 1 iterations in 0.01 seconds (average 0.008000, setup 0.00)

goal = 1, inf = 0, maybe = 209856

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=209857, nc=288319, nnz=592703, k=4] [8.7 MB]
Building sparse matrix (transition rewards)... [n=209857, nc=288319, nnz=105641, k=4] [3.1 MB]
Creating vector for state rewards... [1.6 MB]
Creating vector for inf... [1.6 MB]
Allocating iteration vectors... [2 x 1.6 MB]
TOTAL: [18.2 MB]

Starting iterations...

Iterative method: 101 iterations in 0.66 seconds (average 0.003208, setup 0.34)

Value in the initial state: 171.58250340125392

Time for model checking: 0.715 seconds.

Result: 171.58250340125392 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"time"}min=? [ F "deadlock" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=1,Objy1=3,Objx2=6,Objy2=2

Prob0A: 4 iterations in 0.02 seconds (average 0.006000, setup 0.00)

Prob1E: 5 iterations in 0.04 seconds (average 0.008000, setup 0.00)

Warning: PRISM hasn't checked for zero-reward loops.
Your minimum rewards may be too low...

goal = 1, inf = 0, maybe = 209856

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=209857, nc=288319, nnz=592703, k=4] [8.7 MB]
Building sparse matrix (transition rewards)... [n=209857, nc=288319, nnz=105641, k=4] [3.1 MB]
Creating vector for state rewards... [1.6 MB]
Creating vector for inf... [1.6 MB]
Allocating iteration vectors... [2 x 1.6 MB]
TOTAL: [18.2 MB]

Starting iterations...

Iterative method: 109 iterations in 0.69 seconds (average 0.003193, setup 0.34)

Value in the initial state: 104.86540463180256

Time for model checking: 0.767 seconds.

Result: 104.86540463180256 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"success"}max=? [ F "deadlock" ]/Pmin=? [ F "MissionSuccessful" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=1,Objy1=3,Objx2=6,Objy2=2

Prob0E: 5 iterations in 0.02 seconds (average 0.004800, setup 0.00)

Prob1A: 1 iterations in 0.01 seconds (average 0.008000, setup 0.00)

goal = 1, inf = 0, maybe = 209856

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=209857, nc=288319, nnz=592703, k=4] [8.7 MB]
Building sparse matrix (transition rewards)... [n=209857, nc=288319, nnz=2080, k=4] [1.9 MB]
Creating vector for state rewards... [1.6 MB]
Creating vector for inf... [1.6 MB]
Allocating iteration vectors... [2 x 1.6 MB]
TOTAL: [17.0 MB]

Starting iterations...

Iterative method: 104 iterations in 0.63 seconds (average 0.003115, setup 0.31)

Prob0E: 56 iterations in 0.32 seconds (average 0.005714, setup 0.00)

Prob1A: 3 iterations in 0.06 seconds (average 0.018667, setup 0.00)

yes = 1057, no = 179706, maybe = 29094

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=209857, nc=50825, nnz=142672, k=2] [1.9 MB]
Creating vector for yes... [1.6 MB]
Allocating iteration vectors... [2 x 1.6 MB]
TOTAL: [6.7 MB]

Starting iterations...

Iterative method: 94 iterations in 0.32 seconds (average 0.001404, setup 0.18)

Value in the initial state: 175.03978209699036

Time for model checking: 1.382 seconds.

Result: 175.03978209699036 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"success"}min=? [ F "deadlock" ]/Pmax=? [ F "MissionSuccessful" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=1,Objy1=3,Objx2=6,Objy2=2

Prob0A: 4 iterations in 0.08 seconds (average 0.020000, setup 0.00)

Prob1E: 5 iterations in 0.04 seconds (average 0.008800, setup 0.00)

Warning: PRISM hasn't checked for zero-reward loops.
Your minimum rewards may be too low...

goal = 1, inf = 0, maybe = 209856

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=209857, nc=288319, nnz=592703, k=4] [8.7 MB]
Building sparse matrix (transition rewards)... [n=209857, nc=288319, nnz=2080, k=4] [1.9 MB]
Creating vector for state rewards... [1.6 MB]
Creating vector for inf... [1.6 MB]
Allocating iteration vectors... [2 x 1.6 MB]
TOTAL: [17.0 MB]

Starting iterations...

Iterative method: 99 iterations in 0.65 seconds (average 0.003192, setup 0.34)

Prob0A: 56 iterations in 0.28 seconds (average 0.005000, setup 0.00)

Prob1E: 4 iterations in 0.03 seconds (average 0.008000, setup 0.00)

yes = 1057, no = 172699, maybe = 36101

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=209857, nc=63632, nnz=180603, k=2] [2.3 MB]
Creating vector for yes... [1.6 MB]
Allocating iteration vectors... [2 x 1.6 MB]
TOTAL: [7.1 MB]

Starting iterations...

Iterative method: 103 iterations in 0.31 seconds (average 0.001437, setup 0.16)

Value in the initial state: 105.13644696634196

Time for model checking: 1.4 seconds.

Result: 105.13644696634196 (value in the initial state)

---------------------------------------------------------------------

Note: There were 16 warnings during computation.

