PRISM
=====

Version: 4.3
Date: Wed Aug 03 15:52:48 BST 2016
Hostname: eglinton
Memory limits: cudd=1g, java(heap)=981.5m
Command line: prism uav_sim_mdp.prism -s uav_mdp.props -const 'Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=0,Objy1=2,Objx2=4,Objy2=1,'

Parsing model file "uav_sim_mdp.prism"...

Parsing properties file "uav_mdp.props"...

18 properties:
(1) Pmax=? [ F "MissionSuccessful" ]
(2) Pmin=? [ F "MissionSuccessful" ]
(3) Pmax=? [ F "MissionTimeUp" ]
(4) Pmin=? [ F "MissionTimeUp" ]
(5) Pmax=? [ F "Crash" ]
(6) Pmin=? [ F "Crash" ]
(7) Pmax=? [ F "AllAreaSearched" ]
(8) Pmin=? [ F "AllAreaSearched" ]
(9) Pmax=? [ F "MissionTimeUp"|"AllAreaSearched"|"Crash" ]
(10) Pmin=? [ F "MissionTimeUp"|"AllAreaSearched"|"Crash" ]
(11) R{"drop"}max=? [ F "deadlock" ]
(12) R{"drop"}min=? [ F "deadlock" ]
(13) R{"charge"}max=? [ F "deadlock" ]
(14) R{"charge"}min=? [ F "deadlock" ]
(15) R{"time"}max=? [ F "deadlock" ]
(16) R{"time"}min=? [ F "deadlock" ]
(17) R{"success"}max=? [ F "deadlock" ]/Pmin=? [ F "MissionSuccessful" ]
(18) R{"success"}min=? [ F "deadlock" ]/Pmax=? [ F "MissionSuccessful" ]

Type:        MDP
Modules:     Obj Move Robot T 
Variables:   NoOfObjs posx posy s1 retx rety b t c 

---------------------------------------------------------------------

Model checking: Pmax=? [ F "MissionSuccessful" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=0,Objy1=2,Objx2=4,Objy2=1

Building model...
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=0,Objy1=2,Objx2=4,Objy2=1

Warning: Guard for command 7 of module "Move" is never satisfied.

Warning: Guard for command 8 of module "Move" is never satisfied.

Warning: Guard for command 9 of module "Move" is never satisfied.

Warning: Guard for command 7 of module "Robot" is never satisfied.

Warning: Update 2 of command 3 of module "Robot" doesn't do anything ("(s1'=13)", line 210, column 44)

Warning: Update 2 of command 27 of module "T" doesn't do anything ("(c'=1)", line 323, column 108)

Warning: Guard for command 28 of module "T" is never satisfied.

Warning: Update 2 of command 9 of module "T" doesn't do anything ("(c'=1)", line 300, column 112)

Warning: Guard for command 10 of module "T" is never satisfied.

Warning: Update 2 of command 51 of module "T" doesn't do anything ("(c'=1)", line 354, column 113)

Warning: Guard for command 52 of module "T" is never satisfied.

Computing reachable states...

Reachability (BFS): 88 iterations in 1.34 seconds (average 0.015227, setup 0.00)

Time for model construction: 4.748 seconds.

Warning: Deadlocks detected and fixed in 1 states

Type:        MDP
States:      243796 (1 initial)
Transitions: 637917
Choices:     321820

Transition matrix: 220011 nodes (93 terminal), 637917 minterms, vars: 34r/34c/18nd

Prob0A: 44 iterations in 0.40 seconds (average 0.009000, setup 0.00)

Prob1E: 4 iterations in 0.07 seconds (average 0.017000, setup 0.00)

yes = 1166, no = 204309, maybe = 38321

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=243796, nc=68363, nnz=193539, k=2] [2.5 MB]
Creating vector for yes... [1.9 MB]
Allocating iteration vectors... [2 x 1.9 MB]
TOTAL: [8.1 MB]

Starting iterations...

Iterative method: 88 iterations in 0.43 seconds (average 0.001727, setup 0.28)

Value in the initial state: 0.9844203742461275

Time for model checking: 1.661 seconds.

Result: 0.9844203742461275 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmin=? [ F "MissionSuccessful" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=0,Objy1=2,Objx2=4,Objy2=1

Prob0E: 45 iterations in 0.45 seconds (average 0.010044, setup 0.00)

Prob1A: 3 iterations in 0.06 seconds (average 0.020000, setup 0.00)

yes = 1166, no = 209688, maybe = 32942

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=243796, nc=58575, nnz=165384, k=2] [2.2 MB]
Creating vector for yes... [1.9 MB]
Allocating iteration vectors... [2 x 1.9 MB]
TOTAL: [7.8 MB]

Starting iterations...

Iterative method: 85 iterations in 0.36 seconds (average 0.001694, setup 0.22)

Value in the initial state: 0.9282982983758276

Time for model checking: 1.21 seconds.

Result: 0.9282982983758276 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmax=? [ F "MissionTimeUp" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=0,Objy1=2,Objx2=4,Objy2=1

Prob0A: 72 iterations in 0.76 seconds (average 0.010500, setup 0.00)

Prob1E: 4 iterations in 0.05 seconds (average 0.012000, setup 0.00)

yes = 392, no = 154714, maybe = 88690

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=243796, nc=154141, nnz=422325, k=4] [5.2 MB]
Creating vector for yes... [1.9 MB]
Allocating iteration vectors... [2 x 1.9 MB]
TOTAL: [10.8 MB]

Starting iterations...

Iterative method: 96 iterations in 0.50 seconds (average 0.002125, setup 0.30)

Value in the initial state: 6.406801540422587E-5

Time for model checking: 1.938 seconds.

Result: 6.406801540422587E-5 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmin=? [ F "MissionTimeUp" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=0,Objy1=2,Objx2=4,Objy2=1

Prob0E: 86 iterations in 0.92 seconds (average 0.010744, setup 0.00)

Prob1A: 3 iterations in 0.05 seconds (average 0.017333, setup 0.00)

yes = 392, no = 155140, maybe = 88264

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=243796, nc=153386, nnz=420707, k=4] [5.2 MB]
Creating vector for yes... [1.9 MB]
Allocating iteration vectors... [2 x 1.9 MB]
TOTAL: [10.8 MB]

Starting iterations...

Iterative method: 113 iterations in 0.51 seconds (average 0.002124, setup 0.27)

Value in the initial state: 4.112049832882839E-8

Time for model checking: 2.063 seconds.

Result: 4.112049832882839E-8 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmax=? [ F "Crash" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=0,Objy1=2,Objx2=4,Objy2=1

Prob0A: 3 iterations in 0.11 seconds (average 0.037333, setup 0.00)

Prob1E: 173 iterations in 3.00 seconds (average 0.017318, setup 0.00)

yes = 119555, no = 3103, maybe = 121138

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=243796, nc=199162, nnz=515259, k=4] [6.3 MB]
Creating vector for yes... [1.9 MB]
Allocating iteration vectors... [2 x 1.9 MB]
TOTAL: [11.9 MB]

Starting iterations...

Iterative method: 88 iterations in 0.57 seconds (average 0.002318, setup 0.36)

Value in the initial state: 0.023555010301964725

Time for model checking: 4.791 seconds.

Result: 0.023555010301964725 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmin=? [ F "Crash" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=0,Objy1=2,Objx2=4,Objy2=1

Prob0E: 4 iterations in 0.08 seconds (average 0.021000, setup 0.00)

Prob1A: 43 iterations in 0.56 seconds (average 0.013116, setup 0.00)

yes = 119555, no = 3103, maybe = 121138

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=243796, nc=199162, nnz=515259, k=4] [6.3 MB]
Creating vector for yes... [1.9 MB]
Allocating iteration vectors... [2 x 1.9 MB]
TOTAL: [11.9 MB]

Starting iterations...

Iterative method: 80 iterations in 0.46 seconds (average 0.002400, setup 0.26)

Value in the initial state: 0.013528681328432974

Time for model checking: 1.347 seconds.

Result: 0.013528681328432974 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmax=? [ F "AllAreaSearched" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=0,Objy1=2,Objx2=4,Objy2=1

Prob0A: 54 iterations in 0.52 seconds (average 0.009630, setup 0.00)

Prob1E: 4 iterations in 0.06 seconds (average 0.015000, setup 0.00)

yes = 1544, no = 181425, maybe = 60827

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=243796, nc=93803, nnz=245538, k=2] [3.1 MB]
Creating vector for yes... [1.9 MB]
Allocating iteration vectors... [2 x 1.9 MB]
TOTAL: [8.7 MB]

Starting iterations...

Iterative method: 89 iterations in 0.40 seconds (average 0.001933, setup 0.22)

Value in the initial state: 0.050321812741808934

Time for model checking: 1.22 seconds.

Result: 0.050321812741808934 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmin=? [ F "AllAreaSearched" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=0,Objy1=2,Objx2=4,Objy2=1

Prob0E: 62 iterations in 0.54 seconds (average 0.008774, setup 0.00)

Prob1A: 3 iterations in 0.04 seconds (average 0.014667, setup 0.00)

yes = 1544, no = 188498, maybe = 53754

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=243796, nc=81263, nnz=209367, k=2] [2.7 MB]
Creating vector for yes... [1.9 MB]
Allocating iteration vectors... [2 x 1.9 MB]
TOTAL: [8.3 MB]

Starting iterations...

Iterative method: 96 iterations in 0.32 seconds (average 0.001833, setup 0.15)

Value in the initial state: 0.0018497709303363155

Time for model checking: 0.98 seconds.

Result: 0.0018497709303363155 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmax=? [ F "MissionTimeUp"|"AllAreaSearched"|"Crash" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=0,Objy1=2,Objx2=4,Objy2=1

Prob0A: 3 iterations in 0.11 seconds (average 0.036000, setup 0.00)

Prob1E: 172 iterations in 2.80 seconds (average 0.016256, setup 0.00)

yes = 209687, no = 1167, maybe = 32942

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=243796, nc=58575, nnz=165384, k=2] [2.2 MB]
Creating vector for yes... [1.9 MB]
Allocating iteration vectors... [2 x 1.9 MB]
TOTAL: [7.8 MB]

Starting iterations...

Iterative method: 80 iterations in 0.34 seconds (average 0.001650, setup 0.21)

Value in the initial state: 0.0717014906025131

Time for model checking: 3.502 seconds.

Result: 0.0717014906025131 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmin=? [ F "MissionTimeUp"|"AllAreaSearched"|"Crash" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=0,Objy1=2,Objx2=4,Objy2=1

Prob0E: 4 iterations in 0.08 seconds (average 0.021000, setup 0.00)

Prob1A: 44 iterations in 0.47 seconds (average 0.010727, setup 0.00)

yes = 204308, no = 1167, maybe = 38321

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=243796, nc=68363, nnz=193539, k=2] [2.5 MB]
Creating vector for yes... [1.9 MB]
Allocating iteration vectors... [2 x 1.9 MB]
TOTAL: [8.1 MB]

Starting iterations...

Iterative method: 87 iterations in 0.34 seconds (average 0.001793, setup 0.18)

Value in the initial state: 0.015578093951598381

Time for model checking: 0.917 seconds.

Result: 0.015578093951598381 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"drop"}max=? [ F "deadlock" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=0,Objy1=2,Objx2=4,Objy2=1

Prob0E: 5 iterations in 0.08 seconds (average 0.016000, setup 0.00)

Prob1A: 1 iterations in 0.01 seconds (average 0.008000, setup 0.00)

goal = 1, inf = 0, maybe = 243795

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=243796, nc=321819, nnz=637916, k=4] [9.5 MB]
Building sparse matrix (transition rewards)... [n=243796, nc=321819, nnz=0, k=4] [2.2 MB]
Creating vector for state rewards... [1.9 MB]
Creating vector for inf... [1.9 MB]
Allocating iteration vectors... [2 x 1.9 MB]
TOTAL: [19.1 MB]

Starting iterations...

Iterative method: 84 iterations in 0.74 seconds (average 0.003571, setup 0.44)

Value in the initial state: 0.17537329010895295

Time for model checking: 1.175 seconds.

Result: 0.17537329010895295 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"drop"}min=? [ F "deadlock" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=0,Objy1=2,Objx2=4,Objy2=1

Prob0A: 4 iterations in 0.05 seconds (average 0.013000, setup 0.00)

Prob1E: 5 iterations in 0.06 seconds (average 0.012000, setup 0.00)

Warning: PRISM hasn't checked for zero-reward loops.
Your minimum rewards may be too low...

goal = 1, inf = 0, maybe = 243795

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=243796, nc=321819, nnz=637916, k=4] [9.5 MB]
Building sparse matrix (transition rewards)... [n=243796, nc=321819, nnz=0, k=4] [2.2 MB]
Creating vector for state rewards... [1.9 MB]
Creating vector for inf... [1.9 MB]
Allocating iteration vectors... [2 x 1.9 MB]
TOTAL: [19.1 MB]

Starting iterations...

Iterative method: 77 iterations in 0.62 seconds (average 0.003636, setup 0.34)

Value in the initial state: 0.16229722693251417

Time for model checking: 0.753 seconds.

Result: 0.16229722693251417 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"charge"}max=? [ F "deadlock" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=0,Objy1=2,Objx2=4,Objy2=1

Prob0E: 5 iterations in 0.04 seconds (average 0.008800, setup 0.00)

Prob1A: 1 iterations in 0.01 seconds (average 0.008000, setup 0.00)

goal = 1, inf = 0, maybe = 243795

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=243796, nc=321819, nnz=637916, k=4] [9.5 MB]
Building sparse matrix (transition rewards)... [n=243796, nc=321819, nnz=6908, k=4] [2.2 MB]
Creating vector for state rewards... [1.9 MB]
Creating vector for inf... [1.9 MB]
Allocating iteration vectors... [2 x 1.9 MB]
TOTAL: [19.1 MB]

Starting iterations...

Iterative method: 84 iterations in 0.63 seconds (average 0.003571, setup 0.33)

Value in the initial state: 0.1618674706468704

Time for model checking: 0.702 seconds.

Result: 0.1618674706468704 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"charge"}min=? [ F "deadlock" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=0,Objy1=2,Objx2=4,Objy2=1

Prob0A: 4 iterations in 0.03 seconds (average 0.008000, setup 0.00)

Prob1E: 5 iterations in 0.06 seconds (average 0.011200, setup 0.00)

Warning: PRISM hasn't checked for zero-reward loops.
Your minimum rewards may be too low...

goal = 1, inf = 0, maybe = 243795

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=243796, nc=321819, nnz=637916, k=4] [9.5 MB]
Building sparse matrix (transition rewards)... [n=243796, nc=321819, nnz=6908, k=4] [2.2 MB]
Creating vector for state rewards... [1.9 MB]
Creating vector for inf... [1.9 MB]
Allocating iteration vectors... [2 x 1.9 MB]
TOTAL: [19.1 MB]

Starting iterations...

Iterative method: 94 iterations in 0.69 seconds (average 0.003617, setup 0.35)

Value in the initial state: 0.008306262416458033

Time for model checking: 0.786 seconds.

Result: 0.008306262416458033 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"time"}max=? [ F "deadlock" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=0,Objy1=2,Objx2=4,Objy2=1

Prob0E: 5 iterations in 0.04 seconds (average 0.008000, setup 0.00)

Prob1A: 1 iterations in 0.01 seconds (average 0.008000, setup 0.00)

goal = 1, inf = 0, maybe = 243795

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=243796, nc=321819, nnz=637916, k=4] [9.5 MB]
Building sparse matrix (transition rewards)... [n=243796, nc=321819, nnz=122656, k=4] [3.6 MB]
Creating vector for state rewards... [1.9 MB]
Creating vector for inf... [1.9 MB]
Allocating iteration vectors... [2 x 1.9 MB]
TOTAL: [20.5 MB]

Starting iterations...

Iterative method: 92 iterations in 0.72 seconds (average 0.003609, setup 0.39)

Value in the initial state: 130.22444274308896

Time for model checking: 0.785 seconds.

Result: 130.22444274308896 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"time"}min=? [ F "deadlock" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=0,Objy1=2,Objx2=4,Objy2=1

Prob0A: 4 iterations in 0.03 seconds (average 0.007000, setup 0.00)

Prob1E: 5 iterations in 0.06 seconds (average 0.012000, setup 0.00)

Warning: PRISM hasn't checked for zero-reward loops.
Your minimum rewards may be too low...

goal = 1, inf = 0, maybe = 243795

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=243796, nc=321819, nnz=637916, k=4] [9.5 MB]
Building sparse matrix (transition rewards)... [n=243796, nc=321819, nnz=122656, k=4] [3.6 MB]
Creating vector for state rewards... [1.9 MB]
Creating vector for inf... [1.9 MB]
Allocating iteration vectors... [2 x 1.9 MB]
TOTAL: [20.5 MB]

Starting iterations...

Iterative method: 92 iterations in 0.71 seconds (average 0.003609, setup 0.38)

Value in the initial state: 75.10027322161524

Time for model checking: 0.816 seconds.

Result: 75.10027322161524 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"success"}max=? [ F "deadlock" ]/Pmin=? [ F "MissionSuccessful" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=0,Objy1=2,Objx2=4,Objy2=1

Prob0E: 5 iterations in 0.04 seconds (average 0.008000, setup 0.00)

Prob1A: 1 iterations in 0.01 seconds (average 0.008000, setup 0.00)

goal = 1, inf = 0, maybe = 243795

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=243796, nc=321819, nnz=637916, k=4] [9.5 MB]
Building sparse matrix (transition rewards)... [n=243796, nc=321819, nnz=2298, k=4] [2.2 MB]
Creating vector for state rewards... [1.9 MB]
Creating vector for inf... [1.9 MB]
Allocating iteration vectors... [2 x 1.9 MB]
TOTAL: [19.1 MB]

Starting iterations...

Iterative method: 91 iterations in 0.66 seconds (average 0.003473, setup 0.34)

Prob0E: 45 iterations in 0.34 seconds (average 0.007644, setup 0.00)

Prob1A: 3 iterations in 0.07 seconds (average 0.024000, setup 0.00)

yes = 1166, no = 209688, maybe = 32942

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=243796, nc=58575, nnz=165384, k=2] [2.2 MB]
Creating vector for yes... [1.9 MB]
Allocating iteration vectors... [2 x 1.9 MB]
TOTAL: [7.8 MB]

Starting iterations...

Iterative method: 85 iterations in 0.28 seconds (average 0.001506, setup 0.15)

Value in the initial state: 137.84901271551738

Time for model checking: 1.462 seconds.

Result: 137.84901271551738 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"success"}min=? [ F "deadlock" ]/Pmax=? [ F "MissionSuccessful" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=0,Objy1=2,Objx2=4,Objy2=1

Prob0A: 4 iterations in 0.08 seconds (average 0.019000, setup 0.00)

Prob1E: 5 iterations in 0.07 seconds (average 0.013600, setup 0.00)

Warning: PRISM hasn't checked for zero-reward loops.
Your minimum rewards may be too low...

goal = 1, inf = 0, maybe = 243795

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=243796, nc=321819, nnz=637916, k=4] [9.5 MB]
Building sparse matrix (transition rewards)... [n=243796, nc=321819, nnz=2298, k=4] [2.2 MB]
Creating vector for state rewards... [1.9 MB]
Creating vector for inf... [1.9 MB]
Allocating iteration vectors... [2 x 1.9 MB]
TOTAL: [19.1 MB]

Starting iterations...

Iterative method: 90 iterations in 0.71 seconds (average 0.003556, setup 0.39)

Prob0A: 44 iterations in 0.34 seconds (average 0.007636, setup 0.00)

Prob1E: 4 iterations in 0.04 seconds (average 0.011000, setup 0.00)

yes = 1166, no = 204309, maybe = 38321

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=243796, nc=68363, nnz=193539, k=2] [2.5 MB]
Creating vector for yes... [1.9 MB]
Allocating iteration vectors... [2 x 1.9 MB]
TOTAL: [8.1 MB]

Starting iterations...

Iterative method: 88 iterations in 0.33 seconds (average 0.001636, setup 0.19)

Value in the initial state: 72.65921197965774

Time for model checking: 1.579 seconds.

Result: 72.65921197965774 (value in the initial state)

---------------------------------------------------------------------

Note: There were 16 warnings during computation.

