PRISM
=====

Version: 4.3
Date: Wed Aug 03 16:14:17 BST 2016
Hostname: eglinton
Memory limits: cudd=1g, java(heap)=981.5m
Command line: prism uav_sim_mdp.prism -s uav_mdp.props -const 'Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=3,Objy1=2,Objx2=4,Objy2=3,'

Parsing model file "uav_sim_mdp.prism"...

Parsing properties file "uav_mdp.props"...

18 properties:
(1) Pmax=? [ F "MissionSuccessful" ]
(2) Pmin=? [ F "MissionSuccessful" ]
(3) Pmax=? [ F "MissionTimeUp" ]
(4) Pmin=? [ F "MissionTimeUp" ]
(5) Pmax=? [ F "Crash" ]
(6) Pmin=? [ F "Crash" ]
(7) Pmax=? [ F "AllAreaSearched" ]
(8) Pmin=? [ F "AllAreaSearched" ]
(9) Pmax=? [ F "MissionTimeUp"|"AllAreaSearched"|"Crash" ]
(10) Pmin=? [ F "MissionTimeUp"|"AllAreaSearched"|"Crash" ]
(11) R{"drop"}max=? [ F "deadlock" ]
(12) R{"drop"}min=? [ F "deadlock" ]
(13) R{"charge"}max=? [ F "deadlock" ]
(14) R{"charge"}min=? [ F "deadlock" ]
(15) R{"time"}max=? [ F "deadlock" ]
(16) R{"time"}min=? [ F "deadlock" ]
(17) R{"success"}max=? [ F "deadlock" ]/Pmin=? [ F "MissionSuccessful" ]
(18) R{"success"}min=? [ F "deadlock" ]/Pmax=? [ F "MissionSuccessful" ]

Type:        MDP
Modules:     Obj Move Robot T 
Variables:   NoOfObjs posx posy s1 retx rety b t c 

---------------------------------------------------------------------

Model checking: Pmax=? [ F "MissionSuccessful" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=3,Objy1=2,Objx2=4,Objy2=3

Building model...
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=3,Objy1=2,Objx2=4,Objy2=3

Warning: Guard for command 7 of module "Move" is never satisfied.

Warning: Guard for command 8 of module "Move" is never satisfied.

Warning: Guard for command 9 of module "Move" is never satisfied.

Warning: Guard for command 7 of module "Robot" is never satisfied.

Warning: Update 2 of command 3 of module "Robot" doesn't do anything ("(s1'=13)", line 210, column 44)

Warning: Update 2 of command 27 of module "T" doesn't do anything ("(c'=1)", line 323, column 108)

Warning: Guard for command 28 of module "T" is never satisfied.

Warning: Update 2 of command 9 of module "T" doesn't do anything ("(c'=1)", line 300, column 112)

Warning: Guard for command 10 of module "T" is never satisfied.

Warning: Update 2 of command 51 of module "T" doesn't do anything ("(c'=1)", line 354, column 113)

Warning: Guard for command 52 of module "T" is never satisfied.

Computing reachable states...

Reachability (BFS): 87 iterations in 0.90 seconds (average 0.010391, setup 0.00)

Time for model construction: 2.662 seconds.

Warning: Deadlocks detected and fixed in 1 states

Type:        MDP
States:      187697 (1 initial)
Transitions: 508077
Choices:     253370

Transition matrix: 197839 nodes (89 terminal), 508077 minterms, vars: 34r/34c/18nd

Prob0A: 51 iterations in 0.30 seconds (average 0.005882, setup 0.00)

Prob1E: 4 iterations in 0.04 seconds (average 0.011000, setup 0.00)

yes = 1073, no = 153223, maybe = 33401

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=187697, nc=58994, nnz=164235, k=2] [2.1 MB]
Creating vector for yes... [1.4 MB]
Allocating iteration vectors... [2 x 1.4 MB]
TOTAL: [6.4 MB]

Starting iterations...

Iterative method: 95 iterations in 0.33 seconds (average 0.001263, setup 0.21)

Value in the initial state: 0.9818931876195643

Time for model checking: 0.692 seconds.

Result: 0.9818931876195643 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmin=? [ F "MissionSuccessful" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=3,Objy1=2,Objx2=4,Objy2=3

Prob0E: 51 iterations in 0.32 seconds (average 0.006275, setup 0.00)

Prob1A: 3 iterations in 0.04 seconds (average 0.013333, setup 0.00)

yes = 1073, no = 157348, maybe = 29276

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=187697, nc=51615, nnz=143383, k=2] [1.9 MB]
Creating vector for yes... [1.4 MB]
Allocating iteration vectors... [2 x 1.4 MB]
TOTAL: [6.2 MB]

Starting iterations...

Iterative method: 90 iterations in 0.28 seconds (average 0.001244, setup 0.17)

Value in the initial state: 0.8523825814214623

Time for model checking: 0.662 seconds.

Result: 0.8523825814214623 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmax=? [ F "MissionTimeUp" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=3,Objy1=2,Objx2=4,Objy2=3

Prob0A: 78 iterations in 0.64 seconds (average 0.008256, setup 0.00)

Prob1E: 4 iterations in 0.04 seconds (average 0.011000, setup 0.00)

yes = 248, no = 113798, maybe = 73651

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=187697, nc=127341, nnz=347368, k=4] [4.3 MB]
Creating vector for yes... [1.4 MB]
Allocating iteration vectors... [2 x 1.4 MB]
TOTAL: [8.6 MB]

Starting iterations...

Iterative method: 98 iterations in 0.42 seconds (average 0.001633, setup 0.26)

Value in the initial state: 9.8000026192757E-5

Time for model checking: 1.13 seconds.

Result: 9.8000026192757E-5 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmin=? [ F "MissionTimeUp" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=3,Objy1=2,Objx2=4,Objy2=3

Prob0E: 91 iterations in 0.70 seconds (average 0.007648, setup 0.00)

Prob1A: 3 iterations in 0.04 seconds (average 0.012000, setup 0.00)

yes = 248, no = 114074, maybe = 73375

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=187697, nc=126855, nnz=346360, k=4] [4.3 MB]
Creating vector for yes... [1.4 MB]
Allocating iteration vectors... [2 x 1.4 MB]
TOTAL: [8.6 MB]

Starting iterations...

Iterative method: 113 iterations in 0.58 seconds (average 0.002301, setup 0.32)

Value in the initial state: 8.099748218486449E-8

Time for model checking: 1.335 seconds.

Result: 8.099748218486449E-8 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmax=? [ F "Crash" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=3,Objy1=2,Objx2=4,Objy2=3

Prob0A: 3 iterations in 0.11 seconds (average 0.036000, setup 0.00)

Prob1E: 196 iterations in 2.58 seconds (average 0.013163, setup 0.00)

yes = 91966, no = 2856, maybe = 92875

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=187697, nc=158548, nnz=413255, k=4] [5.1 MB]
Creating vector for yes... [1.4 MB]
Allocating iteration vectors... [2 x 1.4 MB]
TOTAL: [9.4 MB]

Starting iterations...

Iterative method: 92 iterations in 0.74 seconds (average 0.003174, setup 0.45)

Value in the initial state: 0.028993389271419884

Time for model checking: 3.445 seconds.

Result: 0.028993389271419884 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmin=? [ F "Crash" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=3,Objy1=2,Objx2=4,Objy2=3

Prob0E: 3 iterations in 0.08 seconds (average 0.025333, setup 0.00)

Prob1A: 51 iterations in 0.62 seconds (average 0.012235, setup 0.00)

yes = 91966, no = 2856, maybe = 92875

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=187697, nc=158548, nnz=413255, k=4] [5.1 MB]
Creating vector for yes... [1.4 MB]
Allocating iteration vectors... [2 x 1.4 MB]
TOTAL: [9.4 MB]

Starting iterations...

Iterative method: 85 iterations in 0.44 seconds (average 0.001882, setup 0.28)

Value in the initial state: 0.016034160851436573

Time for model checking: 1.158 seconds.

Result: 0.016034160851436573 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmax=? [ F "AllAreaSearched" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=3,Objy1=2,Objx2=4,Objy2=3

Prob0A: 54 iterations in 0.26 seconds (average 0.004889, setup 0.00)

Prob1E: 4 iterations in 0.04 seconds (average 0.009000, setup 0.00)

yes = 1534, no = 133250, maybe = 52913

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=187697, nc=88674, nnz=240648, k=2] [3.0 MB]
Creating vector for yes... [1.4 MB]
Allocating iteration vectors... [2 x 1.4 MB]
TOTAL: [7.3 MB]

Starting iterations...

Iterative method: 85 iterations in 0.36 seconds (average 0.002165, setup 0.18)

Value in the initial state: 0.12250069843247834

Time for model checking: 0.687 seconds.

Result: 0.12250069843247834 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmin=? [ F "AllAreaSearched" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=3,Objy1=2,Objx2=4,Objy2=3

Prob0E: 61 iterations in 0.38 seconds (average 0.006230, setup 0.00)

Prob1A: 3 iterations in 0.03 seconds (average 0.009333, setup 0.00)

yes = 1534, no = 139573, maybe = 46590

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=187697, nc=77410, nnz=208607, k=2] [2.6 MB]
Creating vector for yes... [1.4 MB]
Allocating iteration vectors... [2 x 1.4 MB]
TOTAL: [6.9 MB]

Starting iterations...

Iterative method: 94 iterations in 0.27 seconds (average 0.001447, setup 0.13)

Value in the initial state: 0.0018114265714807636

Time for model checking: 0.688 seconds.

Result: 0.0018114265714807636 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmax=? [ F "MissionTimeUp"|"AllAreaSearched"|"Crash" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=3,Objy1=2,Objx2=4,Objy2=3

Prob0A: 3 iterations in 0.06 seconds (average 0.021333, setup 0.00)

Prob1E: 200 iterations in 2.42 seconds (average 0.012120, setup 0.00)

yes = 157347, no = 1074, maybe = 29276

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=187697, nc=51615, nnz=143383, k=2] [1.9 MB]
Creating vector for yes... [1.4 MB]
Allocating iteration vectors... [2 x 1.4 MB]
TOTAL: [6.2 MB]

Starting iterations...

Iterative method: 84 iterations in 0.29 seconds (average 0.001286, setup 0.18)

Value in the initial state: 0.14761705549909268

Time for model checking: 2.801 seconds.

Result: 0.14761705549909268 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmin=? [ F "MissionTimeUp"|"AllAreaSearched"|"Crash" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=3,Objy1=2,Objx2=4,Objy2=3

Prob0E: 3 iterations in 0.04 seconds (average 0.012000, setup 0.00)

Prob1A: 51 iterations in 0.40 seconds (average 0.007922, setup 0.00)

yes = 153222, no = 1074, maybe = 33401

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=187697, nc=58994, nnz=164235, k=2] [2.1 MB]
Creating vector for yes... [1.4 MB]
Allocating iteration vectors... [2 x 1.4 MB]
TOTAL: [6.4 MB]

Starting iterations...

Iterative method: 92 iterations in 0.26 seconds (average 0.001304, setup 0.14)

Value in the initial state: 0.018105509141072083

Time for model checking: 0.719 seconds.

Result: 0.018105509141072083 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"drop"}max=? [ F "deadlock" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=3,Objy1=2,Objx2=4,Objy2=3

Prob0E: 4 iterations in 0.05 seconds (average 0.013000, setup 0.00)

Prob1A: 1 iterations in 0.00 seconds (average 0.004000, setup 0.00)

goal = 1, inf = 0, maybe = 187696

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=187697, nc=253369, nnz=508076, k=4] [7.5 MB]
Building sparse matrix (transition rewards)... [n=187697, nc=253369, nnz=0, k=4] [1.7 MB]
Creating vector for state rewards... [1.4 MB]
Creating vector for inf... [1.4 MB]
Allocating iteration vectors... [2 x 1.4 MB]
TOTAL: [14.9 MB]

Starting iterations...

Iterative method: 90 iterations in 0.60 seconds (average 0.002756, setup 0.36)

Value in the initial state: 0.15407430385524065

Time for model checking: 0.669 seconds.

Result: 0.15407430385524065 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"drop"}min=? [ F "deadlock" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=3,Objy1=2,Objx2=4,Objy2=3

Prob0A: 4 iterations in 0.03 seconds (average 0.007000, setup 0.00)

Prob1E: 5 iterations in 0.04 seconds (average 0.008000, setup 0.00)

Warning: PRISM hasn't checked for zero-reward loops.
Your minimum rewards may be too low...

goal = 1, inf = 0, maybe = 187696

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=187697, nc=253369, nnz=508076, k=4] [7.5 MB]
Building sparse matrix (transition rewards)... [n=187697, nc=253369, nnz=0, k=4] [1.7 MB]
Creating vector for state rewards... [1.4 MB]
Creating vector for inf... [1.4 MB]
Allocating iteration vectors... [2 x 1.4 MB]
TOTAL: [14.9 MB]

Starting iterations...

Iterative method: 84 iterations in 0.49 seconds (average 0.002762, setup 0.26)

Value in the initial state: 0.13208694607706478

Time for model checking: 0.569 seconds.

Result: 0.13208694607706478 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"charge"}max=? [ F "deadlock" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=3,Objy1=2,Objx2=4,Objy2=3

Prob0E: 4 iterations in 0.02 seconds (average 0.005000, setup 0.00)

Prob1A: 1 iterations in 0.00 seconds (average 0.004000, setup 0.00)

goal = 1, inf = 0, maybe = 187696

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=187697, nc=253369, nnz=508076, k=4] [7.5 MB]
Building sparse matrix (transition rewards)... [n=187697, nc=253369, nnz=5869, k=4] [1.7 MB]
Creating vector for state rewards... [1.4 MB]
Creating vector for inf... [1.4 MB]
Allocating iteration vectors... [2 x 1.4 MB]
TOTAL: [15.0 MB]

Starting iterations...

Iterative method: 89 iterations in 0.50 seconds (average 0.002742, setup 0.26)

Value in the initial state: 0.2541052354898012

Time for model checking: 0.549 seconds.

Result: 0.2541052354898012 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"charge"}min=? [ F "deadlock" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=3,Objy1=2,Objx2=4,Objy2=3

Prob0A: 4 iterations in 0.02 seconds (average 0.005000, setup 0.00)

Prob1E: 5 iterations in 0.04 seconds (average 0.007200, setup 0.00)

Warning: PRISM hasn't checked for zero-reward loops.
Your minimum rewards may be too low...

goal = 1, inf = 0, maybe = 187696

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=187697, nc=253369, nnz=508076, k=4] [7.5 MB]
Building sparse matrix (transition rewards)... [n=187697, nc=253369, nnz=5869, k=4] [1.7 MB]
Creating vector for state rewards... [1.4 MB]
Creating vector for inf... [1.4 MB]
Allocating iteration vectors... [2 x 1.4 MB]
TOTAL: [15.0 MB]

Starting iterations...

Iterative method: 96 iterations in 0.53 seconds (average 0.002792, setup 0.26)

Value in the initial state: 0.03363843349501506

Time for model checking: 0.596 seconds.

Result: 0.03363843349501506 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"time"}max=? [ F "deadlock" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=3,Objy1=2,Objx2=4,Objy2=3

Prob0E: 4 iterations in 0.02 seconds (average 0.006000, setup 0.00)

Prob1A: 1 iterations in 0.00 seconds (average 0.004000, setup 0.00)

goal = 1, inf = 0, maybe = 187696

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=187697, nc=253369, nnz=508076, k=4] [7.5 MB]
Building sparse matrix (transition rewards)... [n=187697, nc=253369, nnz=94820, k=4] [2.8 MB]
Creating vector for state rewards... [1.4 MB]
Creating vector for inf... [1.4 MB]
Allocating iteration vectors... [2 x 1.4 MB]
TOTAL: [16.0 MB]

Starting iterations...

Iterative method: 95 iterations in 0.56 seconds (average 0.002779, setup 0.29)

Value in the initial state: 160.0862534564361

Time for model checking: 0.597 seconds.

Result: 160.0862534564361 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"time"}min=? [ F "deadlock" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=3,Objy1=2,Objx2=4,Objy2=3

Prob0A: 4 iterations in 0.02 seconds (average 0.005000, setup 0.00)

Prob1E: 5 iterations in 0.04 seconds (average 0.008000, setup 0.00)

Warning: PRISM hasn't checked for zero-reward loops.
Your minimum rewards may be too low...

goal = 1, inf = 0, maybe = 187696

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=187697, nc=253369, nnz=508076, k=4] [7.5 MB]
Building sparse matrix (transition rewards)... [n=187697, nc=253369, nnz=94820, k=4] [2.8 MB]
Creating vector for state rewards... [1.4 MB]
Creating vector for inf... [1.4 MB]
Allocating iteration vectors... [2 x 1.4 MB]
TOTAL: [16.0 MB]

Starting iterations...

Iterative method: 96 iterations in 0.55 seconds (average 0.002792, setup 0.28)

Value in the initial state: 88.9276682715034

Time for model checking: 0.633 seconds.

Result: 88.9276682715034 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"success"}max=? [ F "deadlock" ]/Pmin=? [ F "MissionSuccessful" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=3,Objy1=2,Objx2=4,Objy2=3

Prob0E: 4 iterations in 0.02 seconds (average 0.006000, setup 0.00)

Prob1A: 1 iterations in 0.00 seconds (average 0.004000, setup 0.00)

goal = 1, inf = 0, maybe = 187696

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=187697, nc=253369, nnz=508076, k=4] [7.5 MB]
Building sparse matrix (transition rewards)... [n=187697, nc=253369, nnz=2114, k=4] [1.7 MB]
Creating vector for state rewards... [1.4 MB]
Creating vector for inf... [1.4 MB]
Allocating iteration vectors... [2 x 1.4 MB]
TOTAL: [14.9 MB]

Starting iterations...

Iterative method: 95 iterations in 0.52 seconds (average 0.002695, setup 0.27)

Prob0E: 51 iterations in 0.32 seconds (average 0.006353, setup 0.00)

Prob1A: 3 iterations in 0.04 seconds (average 0.014667, setup 0.00)

yes = 1073, no = 157348, maybe = 29276

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=187697, nc=51615, nnz=143383, k=2] [1.9 MB]
Creating vector for yes... [1.4 MB]
Allocating iteration vectors... [2 x 1.4 MB]
TOTAL: [6.2 MB]

Starting iterations...

Iterative method: 90 iterations in 0.21 seconds (average 0.001156, setup 0.10)

Value in the initial state: 184.31083693562135

Time for model checking: 1.155 seconds.

Result: 184.31083693562135 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"success"}min=? [ F "deadlock" ]/Pmax=? [ F "MissionSuccessful" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=3,Objy1=2,Objx2=4,Objy2=3

Prob0A: 4 iterations in 0.03 seconds (average 0.007000, setup 0.00)

Prob1E: 5 iterations in 0.04 seconds (average 0.008000, setup 0.00)

Warning: PRISM hasn't checked for zero-reward loops.
Your minimum rewards may be too low...

goal = 1, inf = 0, maybe = 187696

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=187697, nc=253369, nnz=508076, k=4] [7.5 MB]
Building sparse matrix (transition rewards)... [n=187697, nc=253369, nnz=2114, k=4] [1.7 MB]
Creating vector for state rewards... [1.4 MB]
Creating vector for inf... [1.4 MB]
Allocating iteration vectors... [2 x 1.4 MB]
TOTAL: [14.9 MB]

Starting iterations...

Iterative method: 96 iterations in 0.53 seconds (average 0.002708, setup 0.27)

Prob0A: 51 iterations in 0.32 seconds (average 0.006275, setup 0.00)

Prob1E: 4 iterations in 0.03 seconds (average 0.007000, setup 0.00)

yes = 1073, no = 153223, maybe = 33401

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=187697, nc=58994, nnz=164235, k=2] [2.1 MB]
Creating vector for yes... [1.4 MB]
Allocating iteration vectors... [2 x 1.4 MB]
TOTAL: [6.4 MB]

Starting iterations...

Iterative method: 95 iterations in 0.25 seconds (average 0.001263, setup 0.13)

Value in the initial state: 78.92020792602068

Time for model checking: 1.221 seconds.

Result: 78.92020792602068 (value in the initial state)

---------------------------------------------------------------------

Note: There were 16 warnings during computation.

