PRISM
=====

Version: 4.3
Date: Wed Aug 03 15:59:24 BST 2016
Hostname: eglinton
Memory limits: cudd=1g, java(heap)=981.5m
Command line: prism uav_sim_mdp.prism -s uav_mdp.props -const 'Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=3,Objy1=1,Objx2=5,Objy2=0,'

Parsing model file "uav_sim_mdp.prism"...

Parsing properties file "uav_mdp.props"...

18 properties:
(1) Pmax=? [ F "MissionSuccessful" ]
(2) Pmin=? [ F "MissionSuccessful" ]
(3) Pmax=? [ F "MissionTimeUp" ]
(4) Pmin=? [ F "MissionTimeUp" ]
(5) Pmax=? [ F "Crash" ]
(6) Pmin=? [ F "Crash" ]
(7) Pmax=? [ F "AllAreaSearched" ]
(8) Pmin=? [ F "AllAreaSearched" ]
(9) Pmax=? [ F "MissionTimeUp"|"AllAreaSearched"|"Crash" ]
(10) Pmin=? [ F "MissionTimeUp"|"AllAreaSearched"|"Crash" ]
(11) R{"drop"}max=? [ F "deadlock" ]
(12) R{"drop"}min=? [ F "deadlock" ]
(13) R{"charge"}max=? [ F "deadlock" ]
(14) R{"charge"}min=? [ F "deadlock" ]
(15) R{"time"}max=? [ F "deadlock" ]
(16) R{"time"}min=? [ F "deadlock" ]
(17) R{"success"}max=? [ F "deadlock" ]/Pmin=? [ F "MissionSuccessful" ]
(18) R{"success"}min=? [ F "deadlock" ]/Pmax=? [ F "MissionSuccessful" ]

Type:        MDP
Modules:     Obj Move Robot T 
Variables:   NoOfObjs posx posy s1 retx rety b t c 

---------------------------------------------------------------------

Model checking: Pmax=? [ F "MissionSuccessful" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=3,Objy1=1,Objx2=5,Objy2=0

Building model...
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=3,Objy1=1,Objx2=5,Objy2=0

Warning: Guard for command 7 of module "Move" is never satisfied.

Warning: Guard for command 8 of module "Move" is never satisfied.

Warning: Guard for command 9 of module "Move" is never satisfied.

Warning: Guard for command 7 of module "Robot" is never satisfied.

Warning: Update 2 of command 3 of module "Robot" doesn't do anything ("(s1'=13)", line 210, column 44)

Warning: Update 2 of command 27 of module "T" doesn't do anything ("(c'=1)", line 323, column 108)

Warning: Guard for command 28 of module "T" is never satisfied.

Warning: Update 2 of command 9 of module "T" doesn't do anything ("(c'=1)", line 300, column 112)

Warning: Guard for command 10 of module "T" is never satisfied.

Warning: Update 2 of command 51 of module "T" doesn't do anything ("(c'=1)", line 354, column 113)

Warning: Guard for command 52 of module "T" is never satisfied.

Computing reachable states...

Reachability (BFS): 86 iterations in 1.38 seconds (average 0.016047, setup 0.00)

Time for model construction: 3.425 seconds.

Warning: Deadlocks detected and fixed in 1 states

Type:        MDP
States:      257847 (1 initial)
Transitions: 671559
Choices:     338635

Transition matrix: 236094 nodes (93 terminal), 671559 minterms, vars: 34r/34c/18nd

Prob0A: 45 iterations in 0.39 seconds (average 0.008711, setup 0.00)

Prob1E: 4 iterations in 0.07 seconds (average 0.017000, setup 0.00)

yes = 1071, no = 219475, maybe = 37301

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=257847, nc=66582, nnz=190159, k=2] [2.5 MB]
Creating vector for yes... [2.0 MB]
Allocating iteration vectors... [2 x 2.0 MB]
TOTAL: [8.4 MB]

Starting iterations...

Iterative method: 87 iterations in 0.41 seconds (average 0.001747, setup 0.26)

Value in the initial state: 0.9842222289559142

Time for model checking: 0.899 seconds.

Result: 0.9842222289559142 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmin=? [ F "MissionSuccessful" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=3,Objy1=1,Objx2=5,Objy2=0

Prob0E: 47 iterations in 0.47 seconds (average 0.010043, setup 0.00)

Prob1A: 3 iterations in 0.06 seconds (average 0.021333, setup 0.00)

yes = 1071, no = 225317, maybe = 31459

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=257847, nc=55907, nnz=159071, k=2] [2.1 MB]
Creating vector for yes... [2.0 MB]
Allocating iteration vectors... [2 x 2.0 MB]
TOTAL: [8.0 MB]

Starting iterations...

Iterative method: 82 iterations in 0.33 seconds (average 0.001659, setup 0.19)

Value in the initial state: 0.9246136605247519

Time for model checking: 0.904 seconds.

Result: 0.9246136605247519 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmax=? [ F "MissionTimeUp" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=3,Objy1=1,Objx2=5,Objy2=0

Prob0A: 69 iterations in 0.75 seconds (average 0.010899, setup 0.00)

Prob1E: 4 iterations in 0.06 seconds (average 0.014000, setup 0.00)

yes = 472, no = 163196, maybe = 94179

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=257847, nc=163465, nnz=448229, k=4] [5.5 MB]
Creating vector for yes... [2.0 MB]
Allocating iteration vectors... [2 x 2.0 MB]
TOTAL: [11.4 MB]

Starting iterations...

Iterative method: 94 iterations in 0.52 seconds (average 0.002255, setup 0.31)

Value in the initial state: 1.7609767053756005E-4

Time for model checking: 1.364 seconds.

Result: 1.7609767053756005E-4 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmin=? [ F "MissionTimeUp" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=3,Objy1=1,Objx2=5,Objy2=0

Prob0E: 83 iterations in 1.03 seconds (average 0.012434, setup 0.00)

Prob1A: 3 iterations in 0.05 seconds (average 0.017333, setup 0.00)

yes = 472, no = 163491, maybe = 93884

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=257847, nc=162941, nnz=447182, k=4] [5.5 MB]
Creating vector for yes... [2.0 MB]
Allocating iteration vectors... [2 x 2.0 MB]
TOTAL: [11.4 MB]

Starting iterations...

Iterative method: 111 iterations in 0.56 seconds (average 0.002270, setup 0.31)

Value in the initial state: 1.6328204406307128E-7

Time for model checking: 1.675 seconds.

Result: 1.6328204406307128E-7 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmax=? [ F "Crash" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=3,Objy1=1,Objx2=5,Objy2=0

Prob0A: 3 iterations in 0.11 seconds (average 0.036000, setup 0.00)

Prob1E: 174 iterations in 3.13 seconds (average 0.017977, setup 0.00)

yes = 126327, no = 2965, maybe = 128555

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=257847, nc=209343, nnz=542267, k=4] [6.7 MB]
Creating vector for yes... [2.0 MB]
Allocating iteration vectors... [2 x 2.0 MB]
TOTAL: [12.6 MB]

Starting iterations...

Iterative method: 88 iterations in 0.63 seconds (average 0.002455, setup 0.41)

Value in the initial state: 0.024456810329501977

Time for model checking: 3.906 seconds.

Result: 0.024456810329501977 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmin=? [ F "Crash" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=3,Objy1=1,Objx2=5,Objy2=0

Prob0E: 4 iterations in 0.11 seconds (average 0.027000, setup 0.00)

Prob1A: 46 iterations in 0.75 seconds (average 0.016348, setup 0.00)

yes = 126327, no = 2965, maybe = 128555

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=257847, nc=209343, nnz=542267, k=4] [6.7 MB]
Creating vector for yes... [2.0 MB]
Allocating iteration vectors... [2 x 2.0 MB]
TOTAL: [12.6 MB]

Starting iterations...

Iterative method: 80 iterations in 0.51 seconds (average 0.002450, setup 0.32)

Value in the initial state: 0.01373262446682338

Time for model checking: 1.397 seconds.

Result: 0.01373262446682338 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmax=? [ F "AllAreaSearched" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=3,Objy1=1,Objx2=5,Objy2=0

Prob0A: 59 iterations in 0.67 seconds (average 0.011322, setup 0.00)

Prob1E: 4 iterations in 0.06 seconds (average 0.015000, setup 0.00)

yes = 1421, no = 200669, maybe = 55757

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=257847, nc=82611, nnz=210720, k=2] [2.7 MB]
Creating vector for yes... [2.0 MB]
Allocating iteration vectors... [2 x 2.0 MB]
TOTAL: [8.6 MB]

Starting iterations...

Iterative method: 92 iterations in 0.38 seconds (average 0.001913, setup 0.20)

Value in the initial state: 0.05493258271480571

Time for model checking: 1.1 seconds.

Result: 0.05493258271480571 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmin=? [ F "AllAreaSearched" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=3,Objy1=1,Objx2=5,Objy2=0

Prob0E: 63 iterations in 0.50 seconds (average 0.007873, setup 0.00)

Prob1A: 3 iterations in 0.05 seconds (average 0.016000, setup 0.00)

yes = 1421, no = 205818, maybe = 50608

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=257847, nc=73705, nnz=185833, k=2] [2.4 MB]
Creating vector for yes... [2.0 MB]
Allocating iteration vectors... [2 x 2.0 MB]
TOTAL: [8.3 MB]

Starting iterations...

Iterative method: 95 iterations in 0.33 seconds (average 0.001811, setup 0.16)

Value in the initial state: 0.001829829454284218

Time for model checking: 0.883 seconds.

Result: 0.001829829454284218 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmax=? [ F "MissionTimeUp"|"AllAreaSearched"|"Crash" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=3,Objy1=1,Objx2=5,Objy2=0

Prob0A: 3 iterations in 0.11 seconds (average 0.036000, setup 0.00)

Prob1E: 180 iterations in 2.81 seconds (average 0.015622, setup 0.00)

yes = 225316, no = 1072, maybe = 31459

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=257847, nc=55907, nnz=159071, k=2] [2.1 MB]
Creating vector for yes... [2.0 MB]
Allocating iteration vectors... [2 x 2.0 MB]
TOTAL: [8.0 MB]

Starting iterations...

Iterative method: 78 iterations in 0.36 seconds (average 0.001641, setup 0.23)

Value in the initial state: 0.07538606879693056

Time for model checking: 3.315 seconds.

Result: 0.07538606879693056 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmin=? [ F "MissionTimeUp"|"AllAreaSearched"|"Crash" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=3,Objy1=1,Objx2=5,Objy2=0

Prob0E: 4 iterations in 0.08 seconds (average 0.020000, setup 0.00)

Prob1A: 45 iterations in 0.52 seconds (average 0.011467, setup 0.00)

yes = 219474, no = 1072, maybe = 37301

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=257847, nc=66582, nnz=190159, k=2] [2.5 MB]
Creating vector for yes... [2.0 MB]
Allocating iteration vectors... [2 x 2.0 MB]
TOTAL: [8.4 MB]

Starting iterations...

Iterative method: 87 iterations in 0.35 seconds (average 0.001793, setup 0.20)

Value in the initial state: 0.015775885859947443

Time for model checking: 0.99 seconds.

Result: 0.015775885859947443 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"drop"}max=? [ F "deadlock" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=3,Objy1=1,Objx2=5,Objy2=0

Prob0E: 5 iterations in 0.10 seconds (average 0.020000, setup 0.00)

Prob1A: 1 iterations in 0.01 seconds (average 0.008000, setup 0.00)

goal = 1, inf = 0, maybe = 257846

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=257847, nc=338634, nnz=671558, k=4] [10.0 MB]
Building sparse matrix (transition rewards)... [n=257847, nc=338634, nnz=0, k=4] [2.3 MB]
Creating vector for state rewards... [2.0 MB]
Creating vector for inf... [2.0 MB]
Allocating iteration vectors... [2 x 2.0 MB]
TOTAL: [20.1 MB]

Starting iterations...

Iterative method: 83 iterations in 0.82 seconds (average 0.003759, setup 0.51)

Value in the initial state: 0.20450829749379446

Time for model checking: 0.948 seconds.

Result: 0.20450829749379446 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"drop"}min=? [ F "deadlock" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=3,Objy1=1,Objx2=5,Objy2=0

Prob0A: 4 iterations in 0.06 seconds (average 0.014000, setup 0.00)

Prob1E: 5 iterations in 0.06 seconds (average 0.012000, setup 0.00)

Warning: PRISM hasn't checked for zero-reward loops.
Your minimum rewards may be too low...

goal = 1, inf = 0, maybe = 257846

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=257847, nc=338634, nnz=671558, k=4] [10.0 MB]
Building sparse matrix (transition rewards)... [n=257847, nc=338634, nnz=0, k=4] [2.3 MB]
Creating vector for state rewards... [2.0 MB]
Creating vector for inf... [2.0 MB]
Allocating iteration vectors... [2 x 2.0 MB]
TOTAL: [20.1 MB]

Starting iterations...

Iterative method: 76 iterations in 0.64 seconds (average 0.003737, setup 0.36)

Value in the initial state: 0.18987597058408914

Time for model checking: 0.785 seconds.

Result: 0.18987597058408914 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"charge"}max=? [ F "deadlock" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=3,Objy1=1,Objx2=5,Objy2=0

Prob0E: 5 iterations in 0.04 seconds (average 0.008800, setup 0.00)

Prob1A: 1 iterations in 0.01 seconds (average 0.008000, setup 0.00)

goal = 1, inf = 0, maybe = 257846

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=257847, nc=338634, nnz=671558, k=4] [10.0 MB]
Building sparse matrix (transition rewards)... [n=257847, nc=338634, nnz=7309, k=4] [2.4 MB]
Creating vector for state rewards... [2.0 MB]
Creating vector for inf... [2.0 MB]
Allocating iteration vectors... [2 x 2.0 MB]
TOTAL: [20.2 MB]

Starting iterations...

Iterative method: 82 iterations in 0.68 seconds (average 0.003707, setup 0.38)

Value in the initial state: 0.18380939972889196

Time for model checking: 0.744 seconds.

Result: 0.18380939972889196 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"charge"}min=? [ F "deadlock" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=3,Objy1=1,Objx2=5,Objy2=0

Prob0A: 4 iterations in 0.03 seconds (average 0.008000, setup 0.00)

Prob1E: 5 iterations in 0.06 seconds (average 0.011200, setup 0.00)

Warning: PRISM hasn't checked for zero-reward loops.
Your minimum rewards may be too low...

goal = 1, inf = 0, maybe = 257846

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=257847, nc=338634, nnz=671558, k=4] [10.0 MB]
Building sparse matrix (transition rewards)... [n=257847, nc=338634, nnz=7309, k=4] [2.4 MB]
Creating vector for state rewards... [2.0 MB]
Creating vector for inf... [2.0 MB]
Allocating iteration vectors... [2 x 2.0 MB]
TOTAL: [20.2 MB]

Starting iterations...

Iterative method: 92 iterations in 1.01 seconds (average 0.006304, setup 0.43)

Value in the initial state: 0.010836585401753895

Time for model checking: 1.119 seconds.

Result: 0.010836585401753895 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"time"}max=? [ F "deadlock" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=3,Objy1=1,Objx2=5,Objy2=0

Prob0E: 5 iterations in 0.04 seconds (average 0.008800, setup 0.00)

Prob1A: 1 iterations in 0.01 seconds (average 0.008000, setup 0.00)

goal = 1, inf = 0, maybe = 257846

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=257847, nc=338634, nnz=671558, k=4] [10.0 MB]
Building sparse matrix (transition rewards)... [n=257847, nc=338634, nnz=129290, k=4] [3.8 MB]
Creating vector for state rewards... [2.0 MB]
Creating vector for inf... [2.0 MB]
Allocating iteration vectors... [2 x 2.0 MB]
TOTAL: [21.6 MB]

Starting iterations...

Iterative method: 92 iterations in 0.90 seconds (average 0.004000, setup 0.53)

Value in the initial state: 135.14462904641917

Time for model checking: 0.964 seconds.

Result: 135.14462904641917 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"time"}min=? [ F "deadlock" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=3,Objy1=1,Objx2=5,Objy2=0

Prob0A: 4 iterations in 0.03 seconds (average 0.008000, setup 0.00)

Prob1E: 5 iterations in 0.06 seconds (average 0.011200, setup 0.00)

Warning: PRISM hasn't checked for zero-reward loops.
Your minimum rewards may be too low...

goal = 1, inf = 0, maybe = 257846

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=257847, nc=338634, nnz=671558, k=4] [10.0 MB]
Building sparse matrix (transition rewards)... [n=257847, nc=338634, nnz=129290, k=4] [3.8 MB]
Creating vector for state rewards... [2.0 MB]
Creating vector for inf... [2.0 MB]
Allocating iteration vectors... [2 x 2.0 MB]
TOTAL: [21.6 MB]

Starting iterations...

Iterative method: 94 iterations in 0.90 seconds (average 0.005277, setup 0.40)

Value in the initial state: 76.22025944799074

Time for model checking: 1.009 seconds.

Result: 76.22025944799074 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"success"}max=? [ F "deadlock" ]/Pmin=? [ F "MissionSuccessful" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=3,Objy1=1,Objx2=5,Objy2=0

Prob0E: 5 iterations in 0.04 seconds (average 0.008000, setup 0.00)

Prob1A: 1 iterations in 0.00 seconds (average 0.004000, setup 0.00)

goal = 1, inf = 0, maybe = 257846

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=257847, nc=338634, nnz=671558, k=4] [10.0 MB]
Building sparse matrix (transition rewards)... [n=257847, nc=338634, nnz=2110, k=4] [2.3 MB]
Creating vector for state rewards... [2.0 MB]
Creating vector for inf... [2.0 MB]
Allocating iteration vectors... [2 x 2.0 MB]
TOTAL: [20.1 MB]

Starting iterations...

Iterative method: 90 iterations in 0.71 seconds (average 0.003778, setup 0.37)

Prob0E: 47 iterations in 0.39 seconds (average 0.008340, setup 0.00)

Prob1A: 3 iterations in 0.05 seconds (average 0.016000, setup 0.00)

yes = 1071, no = 225317, maybe = 31459

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=257847, nc=55907, nnz=159071, k=2] [2.1 MB]
Creating vector for yes... [2.0 MB]
Allocating iteration vectors... [2 x 2.0 MB]
TOTAL: [8.0 MB]

Starting iterations...

Iterative method: 82 iterations in 0.32 seconds (average 0.001659, setup 0.18)

Value in the initial state: 143.53424082483363

Time for model checking: 1.544 seconds.

Result: 143.53424082483363 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"success"}min=? [ F "deadlock" ]/Pmax=? [ F "MissionSuccessful" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=3,Objy1=1,Objx2=5,Objy2=0

Prob0A: 4 iterations in 0.13 seconds (average 0.032000, setup 0.00)

Prob1E: 5 iterations in 0.05 seconds (average 0.010400, setup 0.00)

Warning: PRISM hasn't checked for zero-reward loops.
Your minimum rewards may be too low...

goal = 1, inf = 0, maybe = 257846

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=257847, nc=338634, nnz=671558, k=4] [10.0 MB]
Building sparse matrix (transition rewards)... [n=257847, nc=338634, nnz=2110, k=4] [2.3 MB]
Creating vector for state rewards... [2.0 MB]
Creating vector for inf... [2.0 MB]
Allocating iteration vectors... [2 x 2.0 MB]
TOTAL: [20.1 MB]

Starting iterations...

Iterative method: 88 iterations in 0.74 seconds (average 0.003636, setup 0.42)

Prob0A: 45 iterations in 0.26 seconds (average 0.005689, setup 0.00)

Prob1E: 4 iterations in 0.04 seconds (average 0.011000, setup 0.00)

yes = 1071, no = 219475, maybe = 37301

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=257847, nc=66582, nnz=190159, k=2] [2.5 MB]
Creating vector for yes... [2.0 MB]
Allocating iteration vectors... [2 x 2.0 MB]
TOTAL: [8.4 MB]

Starting iterations...

Iterative method: 87 iterations in 0.31 seconds (average 0.001609, setup 0.17)

Value in the initial state: 72.8888473981326

Time for model checking: 1.556 seconds.

Result: 72.8888473981326 (value in the initial state)

---------------------------------------------------------------------

Note: There were 16 warnings during computation.

