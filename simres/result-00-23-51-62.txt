PRISM
=====

Version: 4.3
Date: Wed Aug 03 16:03:39 BST 2016
Hostname: eglinton
Memory limits: cudd=1g, java(heap)=981.5m
Command line: prism uav_sim_mdp.prism -s uav_mdp.props -const 'Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=5,Objy1=1,Objx2=6,Objy2=2,'

Parsing model file "uav_sim_mdp.prism"...

Parsing properties file "uav_mdp.props"...

18 properties:
(1) Pmax=? [ F "MissionSuccessful" ]
(2) Pmin=? [ F "MissionSuccessful" ]
(3) Pmax=? [ F "MissionTimeUp" ]
(4) Pmin=? [ F "MissionTimeUp" ]
(5) Pmax=? [ F "Crash" ]
(6) Pmin=? [ F "Crash" ]
(7) Pmax=? [ F "AllAreaSearched" ]
(8) Pmin=? [ F "AllAreaSearched" ]
(9) Pmax=? [ F "MissionTimeUp"|"AllAreaSearched"|"Crash" ]
(10) Pmin=? [ F "MissionTimeUp"|"AllAreaSearched"|"Crash" ]
(11) R{"drop"}max=? [ F "deadlock" ]
(12) R{"drop"}min=? [ F "deadlock" ]
(13) R{"charge"}max=? [ F "deadlock" ]
(14) R{"charge"}min=? [ F "deadlock" ]
(15) R{"time"}max=? [ F "deadlock" ]
(16) R{"time"}min=? [ F "deadlock" ]
(17) R{"success"}max=? [ F "deadlock" ]/Pmin=? [ F "MissionSuccessful" ]
(18) R{"success"}min=? [ F "deadlock" ]/Pmax=? [ F "MissionSuccessful" ]

Type:        MDP
Modules:     Obj Move Robot T 
Variables:   NoOfObjs posx posy s1 retx rety b t c 

---------------------------------------------------------------------

Model checking: Pmax=? [ F "MissionSuccessful" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=5,Objy1=1,Objx2=6,Objy2=2

Building model...
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=5,Objy1=1,Objx2=6,Objy2=2

Warning: Guard for command 7 of module "Move" is never satisfied.

Warning: Guard for command 8 of module "Move" is never satisfied.

Warning: Guard for command 9 of module "Move" is never satisfied.

Warning: Guard for command 7 of module "Robot" is never satisfied.

Warning: Update 2 of command 3 of module "Robot" doesn't do anything ("(s1'=13)", line 210, column 44)

Warning: Update 2 of command 27 of module "T" doesn't do anything ("(c'=1)", line 323, column 108)

Warning: Guard for command 28 of module "T" is never satisfied.

Warning: Update 2 of command 9 of module "T" doesn't do anything ("(c'=1)", line 300, column 112)

Warning: Guard for command 10 of module "T" is never satisfied.

Warning: Update 2 of command 51 of module "T" doesn't do anything ("(c'=1)", line 354, column 113)

Warning: Guard for command 52 of module "T" is never satisfied.

Computing reachable states...

Reachability (BFS): 89 iterations in 1.25 seconds (average 0.014067, setup 0.00)

Time for model construction: 2.593 seconds.

Warning: Deadlocks detected and fixed in 1 states

Type:        MDP
States:      339503 (1 initial)
Transitions: 937849
Choices:     456163

Transition matrix: 224770 nodes (97 terminal), 937849 minterms, vars: 34r/34c/18nd

Prob0A: 54 iterations in 0.33 seconds (average 0.006148, setup 0.00)

Prob1E: 4 iterations in 0.05 seconds (average 0.012000, setup 0.00)

yes = 1237, no = 285722, maybe = 52544

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=339503, nc=91994, nnz=266276, k=2] [3.5 MB]
Creating vector for yes... [2.6 MB]
Allocating iteration vectors... [2 x 2.6 MB]
TOTAL: [11.2 MB]

Starting iterations...

Iterative method: 103 iterations in 0.45 seconds (average 0.002214, setup 0.22)

Value in the initial state: 0.9801601575860823

Time for model checking: 0.864 seconds.

Result: 0.9801601575860823 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmin=? [ F "MissionSuccessful" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=5,Objy1=1,Objx2=6,Objy2=2

Prob0E: 56 iterations in 0.38 seconds (average 0.006857, setup 0.00)

Prob1A: 3 iterations in 0.06 seconds (average 0.020000, setup 0.00)

yes = 1237, no = 293407, maybe = 44859

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=339503, nc=78320, nnz=226545, k=2] [3.0 MB]
Creating vector for yes... [2.6 MB]
Allocating iteration vectors... [2 x 2.6 MB]
TOTAL: [10.8 MB]

Starting iterations...

Iterative method: 93 iterations in 0.42 seconds (average 0.002108, setup 0.22)

Value in the initial state: 0.7868470365898865

Time for model checking: 0.878 seconds.

Result: 0.7868470365898865 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmax=? [ F "MissionTimeUp" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=5,Objy1=1,Objx2=6,Objy2=2

Prob0A: 70 iterations in 0.75 seconds (average 0.010743, setup 0.00)

Prob1E: 4 iterations in 0.04 seconds (average 0.011000, setup 0.00)

yes = 472, no = 195652, maybe = 143379

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=339503, nc=246309, nnz=684080, k=4] [8.4 MB]
Creating vector for yes... [2.6 MB]
Allocating iteration vectors... [2 x 2.6 MB]
TOTAL: [16.2 MB]

Starting iterations...

Iterative method: 100 iterations in 0.64 seconds (average 0.002960, setup 0.34)

Value in the initial state: 0.0010084585082260871

Time for model checking: 1.444 seconds.

Result: 0.0010084585082260871 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmin=? [ F "MissionTimeUp" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=5,Objy1=1,Objx2=6,Objy2=2

Prob0E: 85 iterations in 0.70 seconds (average 0.008235, setup 0.00)

Prob1A: 3 iterations in 0.05 seconds (average 0.016000, setup 0.00)

yes = 472, no = 196065, maybe = 142966

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=339503, nc=245564, nnz=682685, k=4] [8.4 MB]
Creating vector for yes... [2.6 MB]
Allocating iteration vectors... [2 x 2.6 MB]
TOTAL: [16.1 MB]

Starting iterations...

Iterative method: 115 iterations in 0.68 seconds (average 0.002957, setup 0.34)

Value in the initial state: 2.9395380949726393E-6

Time for model checking: 1.451 seconds.

Result: 2.9395380949726393E-6 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmax=? [ F "Crash" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=5,Objy1=1,Objx2=6,Objy2=2

Prob0A: 3 iterations in 0.08 seconds (average 0.026667, setup 0.00)

Prob1E: 196 iterations in 2.62 seconds (average 0.013347, setup 0.00)

yes = 165999, no = 3426, maybe = 170078

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=339503, nc=286738, nnz=768424, k=4] [9.4 MB]
Creating vector for yes... [2.6 MB]
Allocating iteration vectors... [2 x 2.6 MB]
TOTAL: [17.2 MB]

Starting iterations...

Iterative method: 95 iterations in 0.72 seconds (average 0.003074, setup 0.42)

Value in the initial state: 0.032893714582483934

Time for model checking: 3.436 seconds.

Result: 0.032893714582483934 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmin=? [ F "Crash" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=5,Objy1=1,Objx2=6,Objy2=2

Prob0E: 4 iterations in 0.08 seconds (average 0.019000, setup 0.00)

Prob1A: 49 iterations in 0.58 seconds (average 0.011755, setup 0.00)

yes = 165999, no = 3426, maybe = 170078

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=339503, nc=286738, nnz=768424, k=4] [9.4 MB]
Creating vector for yes... [2.6 MB]
Allocating iteration vectors... [2 x 2.6 MB]
TOTAL: [17.2 MB]

Starting iterations...

Iterative method: 101 iterations in 0.66 seconds (average 0.003168, setup 0.34)

Value in the initial state: 0.017680940628485874

Time for model checking: 1.342 seconds.

Result: 0.017680940628485874 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmax=? [ F "AllAreaSearched" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=5,Objy1=1,Objx2=6,Objy2=2

Prob0A: 58 iterations in 0.42 seconds (average 0.007172, setup 0.00)

Prob1E: 4 iterations in 0.04 seconds (average 0.011000, setup 0.00)

yes = 1716, no = 263322, maybe = 74465

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=339503, nc=121801, nnz=338246, k=2] [4.3 MB]
Creating vector for yes... [2.6 MB]
Allocating iteration vectors... [2 x 2.6 MB]
TOTAL: [12.1 MB]

Starting iterations...

Iterative method: 93 iterations in 0.43 seconds (average 0.002366, setup 0.21)

Value in the initial state: 0.18590794281295095

Time for model checking: 0.917 seconds.

Result: 0.18590794281295095 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmin=? [ F "AllAreaSearched" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=5,Objy1=1,Objx2=6,Objy2=2

Prob0E: 65 iterations in 0.36 seconds (average 0.005477, setup 0.00)

Prob1A: 3 iterations in 0.04 seconds (average 0.012000, setup 0.00)

yes = 1716, no = 272861, maybe = 64926

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=339503, nc=105560, nnz=292367, k=2] [3.8 MB]
Creating vector for yes... [2.6 MB]
Allocating iteration vectors... [2 x 2.6 MB]
TOTAL: [11.5 MB]

Starting iterations...

Iterative method: 99 iterations in 0.38 seconds (average 0.002222, setup 0.16)

Value in the initial state: 0.0017403376607847364

Time for model checking: 0.795 seconds.

Result: 0.0017403376607847364 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmax=? [ F "MissionTimeUp"|"AllAreaSearched"|"Crash" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=5,Objy1=1,Objx2=6,Objy2=2

Prob0A: 3 iterations in 0.09 seconds (average 0.029333, setup 0.00)

Prob1E: 216 iterations in 2.54 seconds (average 0.011741, setup 0.00)

yes = 293406, no = 1238, maybe = 44859

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=339503, nc=78320, nnz=226545, k=2] [3.0 MB]
Creating vector for yes... [2.6 MB]
Allocating iteration vectors... [2 x 2.6 MB]
TOTAL: [10.8 MB]

Starting iterations...

Iterative method: 88 iterations in 0.40 seconds (average 0.002091, setup 0.21)

Value in the initial state: 0.21315269206131204

Time for model checking: 3.055 seconds.

Result: 0.21315269206131204 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmin=? [ F "MissionTimeUp"|"AllAreaSearched"|"Crash" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=5,Objy1=1,Objx2=6,Objy2=2

Prob0E: 4 iterations in 0.08 seconds (average 0.020000, setup 0.00)

Prob1A: 54 iterations in 0.48 seconds (average 0.008889, setup 0.00)

yes = 285721, no = 1238, maybe = 52544

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=339503, nc=91994, nnz=266276, k=2] [3.5 MB]
Creating vector for yes... [2.6 MB]
Allocating iteration vectors... [2 x 2.6 MB]
TOTAL: [11.2 MB]

Starting iterations...

Iterative method: 100 iterations in 0.43 seconds (average 0.002240, setup 0.20)

Value in the initial state: 0.019839193039022648

Time for model checking: 1.033 seconds.

Result: 0.019839193039022648 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"drop"}max=? [ F "deadlock" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=5,Objy1=1,Objx2=6,Objy2=2

Prob0E: 5 iterations in 0.09 seconds (average 0.018400, setup 0.00)

Prob1A: 1 iterations in 0.01 seconds (average 0.008000, setup 0.00)

goal = 1, inf = 0, maybe = 339502

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=339503, nc=456162, nnz=937848, k=4] [13.8 MB]
Building sparse matrix (transition rewards)... [n=339503, nc=456162, nnz=0, k=4] [3.0 MB]
Creating vector for state rewards... [2.6 MB]
Creating vector for inf... [2.6 MB]
Allocating iteration vectors... [2 x 2.6 MB]
TOTAL: [27.2 MB]

Starting iterations...

Iterative method: 97 iterations in 0.99 seconds (average 0.004825, setup 0.52)

Value in the initial state: 0.2833237750268316

Time for model checking: 1.118 seconds.

Result: 0.2833237750268316 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"drop"}min=? [ F "deadlock" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=5,Objy1=1,Objx2=6,Objy2=2

Prob0A: 4 iterations in 0.04 seconds (average 0.010000, setup 0.00)

Prob1E: 5 iterations in 0.18 seconds (average 0.035200, setup 0.00)

Warning: PRISM hasn't checked for zero-reward loops.
Your minimum rewards may be too low...

goal = 1, inf = 0, maybe = 339502

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=339503, nc=456162, nnz=937848, k=4] [13.8 MB]
Building sparse matrix (transition rewards)... [n=339503, nc=456162, nnz=0, k=4] [3.0 MB]
Creating vector for state rewards... [2.6 MB]
Creating vector for inf... [2.6 MB]
Allocating iteration vectors... [2 x 2.6 MB]
TOTAL: [27.2 MB]

Starting iterations...

Iterative method: 87 iterations in 0.88 seconds (average 0.004782, setup 0.46)

Value in the initial state: 0.23216542358775413

Time for model checking: 1.129 seconds.

Result: 0.23216542358775413 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"charge"}max=? [ F "deadlock" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=5,Objy1=1,Objx2=6,Objy2=2

Prob0E: 5 iterations in 0.04 seconds (average 0.008800, setup 0.00)

Prob1A: 1 iterations in 0.01 seconds (average 0.008000, setup 0.00)

goal = 1, inf = 0, maybe = 339502

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=339503, nc=456162, nnz=937848, k=4] [13.8 MB]
Building sparse matrix (transition rewards)... [n=339503, nc=456162, nnz=11551, k=4] [3.2 MB]
Creating vector for state rewards... [2.6 MB]
Creating vector for inf... [2.6 MB]
Allocating iteration vectors... [2 x 2.6 MB]
TOTAL: [27.3 MB]

Starting iterations...

Iterative method: 93 iterations in 0.88 seconds (average 0.004774, setup 0.44)

Value in the initial state: 0.32937782045813285

Time for model checking: 0.944 seconds.

Result: 0.32937782045813285 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"charge"}min=? [ F "deadlock" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=5,Objy1=1,Objx2=6,Objy2=2

Prob0A: 4 iterations in 0.02 seconds (average 0.006000, setup 0.00)

Prob1E: 5 iterations in 0.05 seconds (average 0.010400, setup 0.00)

Warning: PRISM hasn't checked for zero-reward loops.
Your minimum rewards may be too low...

goal = 1, inf = 0, maybe = 339502

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=339503, nc=456162, nnz=937848, k=4] [13.8 MB]
Building sparse matrix (transition rewards)... [n=339503, nc=456162, nnz=11551, k=4] [3.2 MB]
Creating vector for state rewards... [2.6 MB]
Creating vector for inf... [2.6 MB]
Allocating iteration vectors... [2 x 2.6 MB]
TOTAL: [27.3 MB]

Starting iterations...

Iterative method: 104 iterations in 0.94 seconds (average 0.004846, setup 0.44)

Value in the initial state: 0.06747904756329307

Time for model checking: 1.029 seconds.

Result: 0.06747904756329307 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"time"}max=? [ F "deadlock" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=5,Objy1=1,Objx2=6,Objy2=2

Prob0E: 5 iterations in 0.04 seconds (average 0.007200, setup 0.00)

Prob1A: 1 iterations in 0.00 seconds (average 0.004000, setup 0.00)

goal = 1, inf = 0, maybe = 339502

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=339503, nc=456162, nnz=937848, k=4] [13.8 MB]
Building sparse matrix (transition rewards)... [n=339503, nc=456162, nnz=169423, k=4] [5.0 MB]
Creating vector for state rewards... [2.6 MB]
Creating vector for inf... [2.6 MB]
Allocating iteration vectors... [2 x 2.6 MB]
TOTAL: [29.1 MB]

Starting iterations...

Iterative method: 98 iterations in 0.95 seconds (average 0.004816, setup 0.48)

Value in the initial state: 181.3503775357875

Time for model checking: 1.004 seconds.

Result: 181.3503775357875 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"time"}min=? [ F "deadlock" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=5,Objy1=1,Objx2=6,Objy2=2

Prob0A: 4 iterations in 0.02 seconds (average 0.006000, setup 0.00)

Prob1E: 5 iterations in 0.04 seconds (average 0.008000, setup 0.00)

Warning: PRISM hasn't checked for zero-reward loops.
Your minimum rewards may be too low...

goal = 1, inf = 0, maybe = 339502

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=339503, nc=456162, nnz=937848, k=4] [13.8 MB]
Building sparse matrix (transition rewards)... [n=339503, nc=456162, nnz=169423, k=4] [5.0 MB]
Creating vector for state rewards... [2.6 MB]
Creating vector for inf... [2.6 MB]
Allocating iteration vectors... [2 x 2.6 MB]
TOTAL: [29.1 MB]

Starting iterations...

Iterative method: 107 iterations in 0.96 seconds (average 0.004860, setup 0.44)

Value in the initial state: 97.95090222918466

Time for model checking: 1.044 seconds.

Result: 97.95090222918466 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"success"}max=? [ F "deadlock" ]/Pmin=? [ F "MissionSuccessful" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=5,Objy1=1,Objx2=6,Objy2=2

Prob0E: 5 iterations in 0.03 seconds (average 0.006400, setup 0.00)

Prob1A: 1 iterations in 0.00 seconds (average 0.004000, setup 0.00)

goal = 1, inf = 0, maybe = 339502

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=339503, nc=456162, nnz=937848, k=4] [13.8 MB]
Building sparse matrix (transition rewards)... [n=339503, nc=456162, nnz=2436, k=4] [3.1 MB]
Creating vector for state rewards... [2.6 MB]
Creating vector for inf... [2.6 MB]
Allocating iteration vectors... [2 x 2.6 MB]
TOTAL: [27.2 MB]

Starting iterations...

Iterative method: 99 iterations in 0.86 seconds (average 0.004525, setup 0.41)

Prob0E: 56 iterations in 0.25 seconds (average 0.004500, setup 0.00)

Prob1A: 3 iterations in 0.04 seconds (average 0.014667, setup 0.00)

yes = 1237, no = 293407, maybe = 44859

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=339503, nc=78320, nnz=226545, k=2] [3.0 MB]
Creating vector for yes... [2.6 MB]
Allocating iteration vectors... [2 x 2.6 MB]
TOTAL: [10.8 MB]

Starting iterations...

Iterative method: 93 iterations in 0.36 seconds (average 0.001935, setup 0.18)

Value in the initial state: 225.5986348883144

Time for model checking: 1.594 seconds.

Result: 225.5986348883144 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"success"}min=? [ F "deadlock" ]/Pmax=? [ F "MissionSuccessful" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=5,Objy1=1,Objx2=6,Objy2=2

Prob0A: 4 iterations in 0.07 seconds (average 0.018000, setup 0.00)

Prob1E: 5 iterations in 0.05 seconds (average 0.009600, setup 0.00)

Warning: PRISM hasn't checked for zero-reward loops.
Your minimum rewards may be too low...

goal = 1, inf = 0, maybe = 339502

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=339503, nc=456162, nnz=937848, k=4] [13.8 MB]
Building sparse matrix (transition rewards)... [n=339503, nc=456162, nnz=2436, k=4] [3.1 MB]
Creating vector for state rewards... [2.6 MB]
Creating vector for inf... [2.6 MB]
Allocating iteration vectors... [2 x 2.6 MB]
TOTAL: [27.2 MB]

Starting iterations...

Iterative method: 100 iterations in 0.87 seconds (average 0.004520, setup 0.42)

Prob0A: 54 iterations in 0.24 seconds (average 0.004519, setup 0.00)

Prob1E: 4 iterations in 0.03 seconds (average 0.008000, setup 0.00)

yes = 1237, no = 285722, maybe = 52544

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=339503, nc=91994, nnz=266276, k=2] [3.5 MB]
Creating vector for yes... [2.6 MB]
Allocating iteration vectors... [2 x 2.6 MB]
TOTAL: [11.2 MB]

Starting iterations...

Iterative method: 103 iterations in 0.39 seconds (average 0.002019, setup 0.18)

Value in the initial state: 78.77684342232

Time for model checking: 1.678 seconds.

Result: 78.77684342232 (value in the initial state)

---------------------------------------------------------------------

Note: There were 16 warnings during computation.

