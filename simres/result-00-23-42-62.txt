PRISM
=====

Version: 4.3
Date: Wed Aug 03 16:02:04 BST 2016
Hostname: eglinton
Memory limits: cudd=1g, java(heap)=981.5m
Command line: prism uav_sim_mdp.prism -s uav_mdp.props -const 'Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=4,Objy1=2,Objx2=6,Objy2=2,'

Parsing model file "uav_sim_mdp.prism"...

Parsing properties file "uav_mdp.props"...

18 properties:
(1) Pmax=? [ F "MissionSuccessful" ]
(2) Pmin=? [ F "MissionSuccessful" ]
(3) Pmax=? [ F "MissionTimeUp" ]
(4) Pmin=? [ F "MissionTimeUp" ]
(5) Pmax=? [ F "Crash" ]
(6) Pmin=? [ F "Crash" ]
(7) Pmax=? [ F "AllAreaSearched" ]
(8) Pmin=? [ F "AllAreaSearched" ]
(9) Pmax=? [ F "MissionTimeUp"|"AllAreaSearched"|"Crash" ]
(10) Pmin=? [ F "MissionTimeUp"|"AllAreaSearched"|"Crash" ]
(11) R{"drop"}max=? [ F "deadlock" ]
(12) R{"drop"}min=? [ F "deadlock" ]
(13) R{"charge"}max=? [ F "deadlock" ]
(14) R{"charge"}min=? [ F "deadlock" ]
(15) R{"time"}max=? [ F "deadlock" ]
(16) R{"time"}min=? [ F "deadlock" ]
(17) R{"success"}max=? [ F "deadlock" ]/Pmin=? [ F "MissionSuccessful" ]
(18) R{"success"}min=? [ F "deadlock" ]/Pmax=? [ F "MissionSuccessful" ]

Type:        MDP
Modules:     Obj Move Robot T 
Variables:   NoOfObjs posx posy s1 retx rety b t c 

---------------------------------------------------------------------

Model checking: Pmax=? [ F "MissionSuccessful" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=4,Objy1=2,Objx2=6,Objy2=2

Building model...
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=4,Objy1=2,Objx2=6,Objy2=2

Warning: Guard for command 7 of module "Move" is never satisfied.

Warning: Guard for command 8 of module "Move" is never satisfied.

Warning: Guard for command 9 of module "Move" is never satisfied.

Warning: Guard for command 7 of module "Robot" is never satisfied.

Warning: Update 2 of command 3 of module "Robot" doesn't do anything ("(s1'=13)", line 210, column 44)

Warning: Update 2 of command 27 of module "T" doesn't do anything ("(c'=1)", line 323, column 108)

Warning: Guard for command 28 of module "T" is never satisfied.

Warning: Update 2 of command 9 of module "T" doesn't do anything ("(c'=1)", line 300, column 112)

Warning: Guard for command 10 of module "T" is never satisfied.

Warning: Update 2 of command 51 of module "T" doesn't do anything ("(c'=1)", line 354, column 113)

Warning: Guard for command 52 of module "T" is never satisfied.

Computing reachable states...

Reachability (BFS): 89 iterations in 0.86 seconds (average 0.009708, setup 0.00)

Time for model construction: 2.979 seconds.

Warning: Deadlocks detected and fixed in 1 states

Type:        MDP
States:      258803 (1 initial)
Transitions: 742924
Choices:     355708

Transition matrix: 210298 nodes (93 terminal), 742924 minterms, vars: 34r/34c/18nd

Prob0A: 51 iterations in 0.32 seconds (average 0.006196, setup 0.00)

Prob1E: 4 iterations in 0.05 seconds (average 0.012000, setup 0.00)

yes = 1111, no = 213515, maybe = 44177

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=258803, nc=80198, nnz=234193, k=2] [3.0 MB]
Creating vector for yes... [2.0 MB]
Allocating iteration vectors... [2 x 2.0 MB]
TOTAL: [8.9 MB]

Starting iterations...

Iterative method: 102 iterations in 0.41 seconds (average 0.001765, setup 0.23)

Value in the initial state: 0.9805638959379223

Time for model checking: 0.785 seconds.

Result: 0.9805638959379223 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmin=? [ F "MissionSuccessful" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=4,Objy1=2,Objx2=6,Objy2=2

Prob0E: 51 iterations in 0.47 seconds (average 0.009255, setup 0.00)

Prob1A: 3 iterations in 0.07 seconds (average 0.024000, setup 0.00)

yes = 1111, no = 219278, maybe = 38414

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=258803, nc=69402, nnz=202769, k=2] [2.6 MB]
Creating vector for yes... [2.0 MB]
Allocating iteration vectors... [2 x 2.0 MB]
TOTAL: [8.6 MB]

Starting iterations...

Iterative method: 92 iterations in 0.45 seconds (average 0.002696, setup 0.20)

Value in the initial state: 0.8006055081950425

Time for model checking: 1.004 seconds.

Result: 0.8006055081950425 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmax=? [ F "MissionTimeUp" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=4,Objy1=2,Objx2=6,Objy2=2

Prob0A: 79 iterations in 0.64 seconds (average 0.008152, setup 0.00)

Prob1E: 4 iterations in 0.14 seconds (average 0.036000, setup 0.00)

yes = 332, no = 152664, maybe = 105807

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=258803, nc=190458, nnz=538709, k=4] [6.6 MB]
Creating vector for yes... [2.0 MB]
Allocating iteration vectors... [2 x 2.0 MB]
TOTAL: [12.5 MB]

Starting iterations...

Iterative method: 99 iterations in 0.55 seconds (average 0.002263, setup 0.33)

Value in the initial state: 7.474101315223708E-4

Time for model checking: 1.347 seconds.

Result: 7.474101315223708E-4 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmin=? [ F "MissionTimeUp" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=4,Objy1=2,Objx2=6,Objy2=2

Prob0E: 91 iterations in 0.60 seconds (average 0.006549, setup 0.00)

Prob1A: 3 iterations in 0.04 seconds (average 0.014667, setup 0.00)

yes = 332, no = 153049, maybe = 105422

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=258803, nc=189759, nnz=537442, k=4] [6.6 MB]
Creating vector for yes... [2.0 MB]
Allocating iteration vectors... [2 x 2.0 MB]
TOTAL: [12.5 MB]

Starting iterations...

Iterative method: 113 iterations in 0.55 seconds (average 0.002372, setup 0.28)

Value in the initial state: 2.047088129478953E-6

Time for model checking: 1.214 seconds.

Result: 2.047088129478953E-6 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmax=? [ F "Crash" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=4,Objy1=2,Objx2=6,Objy2=2

Prob0A: 3 iterations in 0.07 seconds (average 0.022667, setup 0.00)

Prob1E: 192 iterations in 2.14 seconds (average 0.011125, setup 0.00)

yes = 126297, no = 2972, maybe = 129534

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=258803, nc=226439, nnz=613655, k=4] [7.5 MB]
Creating vector for yes... [2.0 MB]
Allocating iteration vectors... [2 x 2.0 MB]
TOTAL: [13.4 MB]

Starting iterations...

Iterative method: 94 iterations in 0.58 seconds (average 0.002468, setup 0.35)

Value in the initial state: 0.03203027407953884

Time for model checking: 2.822 seconds.

Result: 0.03203027407953884 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmin=? [ F "Crash" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=4,Objy1=2,Objx2=6,Objy2=2

Prob0E: 4 iterations in 0.06 seconds (average 0.014000, setup 0.00)

Prob1A: 51 iterations in 0.40 seconds (average 0.007765, setup 0.00)

yes = 126297, no = 2972, maybe = 129534

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=258803, nc=226439, nnz=613655, k=4] [7.5 MB]
Creating vector for yes... [2.0 MB]
Allocating iteration vectors... [2 x 2.0 MB]
TOTAL: [13.4 MB]

Starting iterations...

Iterative method: 93 iterations in 0.51 seconds (average 0.002581, setup 0.27)

Value in the initial state: 0.01729172576700221

Time for model checking: 0.983 seconds.

Result: 0.01729172576700221 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmax=? [ F "AllAreaSearched" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=4,Objy1=2,Objx2=6,Objy2=2

Prob0A: 55 iterations in 0.33 seconds (average 0.006036, setup 0.00)

Prob1E: 4 iterations in 0.04 seconds (average 0.010000, setup 0.00)

yes = 1528, no = 192976, maybe = 64299

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=258803, nc=108258, nnz=304185, k=2] [3.8 MB]
Creating vector for yes... [2.0 MB]
Allocating iteration vectors... [2 x 2.0 MB]
TOTAL: [9.8 MB]

Starting iterations...

Iterative method: 91 iterations in 0.40 seconds (average 0.001890, setup 0.22)

Value in the initial state: 0.17198957905057297

Time for model checking: 0.791 seconds.

Result: 0.17198957905057297 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmin=? [ F "AllAreaSearched" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=4,Objy1=2,Objx2=6,Objy2=2

Prob0E: 64 iterations in 0.39 seconds (average 0.006125, setup 0.00)

Prob1A: 3 iterations in 0.03 seconds (average 0.010667, setup 0.00)

yes = 1528, no = 201229, maybe = 56046

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=258803, nc=92860, nnz=258644, k=2] [3.3 MB]
Creating vector for yes... [2.0 MB]
Allocating iteration vectors... [2 x 2.0 MB]
TOTAL: [9.2 MB]

Starting iterations...

Iterative method: 98 iterations in 0.34 seconds (average 0.001878, setup 0.15)

Value in the initial state: 0.0017483804720197236

Time for model checking: 0.77 seconds.

Result: 0.0017483804720197236 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmax=? [ F "MissionTimeUp"|"AllAreaSearched"|"Crash" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=4,Objy1=2,Objx2=6,Objy2=2

Prob0A: 3 iterations in 0.07 seconds (average 0.024000, setup 0.00)

Prob1E: 196 iterations in 1.98 seconds (average 0.010102, setup 0.00)

yes = 219277, no = 1112, maybe = 38414

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=258803, nc=69402, nnz=202769, k=2] [2.6 MB]
Creating vector for yes... [2.0 MB]
Allocating iteration vectors... [2 x 2.0 MB]
TOTAL: [8.6 MB]

Starting iterations...

Iterative method: 87 iterations in 0.35 seconds (average 0.001701, setup 0.20)

Value in the initial state: 0.1993942237768439

Time for model checking: 2.439 seconds.

Result: 0.1993942237768439 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmin=? [ F "MissionTimeUp"|"AllAreaSearched"|"Crash" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=4,Objy1=2,Objx2=6,Objy2=2

Prob0E: 4 iterations in 0.04 seconds (average 0.011000, setup 0.00)

Prob1A: 51 iterations in 0.34 seconds (average 0.006745, setup 0.00)

yes = 213514, no = 1112, maybe = 44177

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=258803, nc=80198, nnz=234193, k=2] [3.0 MB]
Creating vector for yes... [2.0 MB]
Allocating iteration vectors... [2 x 2.0 MB]
TOTAL: [8.9 MB]

Starting iterations...

Iterative method: 98 iterations in 0.36 seconds (average 0.001796, setup 0.18)

Value in the initial state: 0.01943548123728601

Time for model checking: 0.768 seconds.

Result: 0.01943548123728601 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"drop"}max=? [ F "deadlock" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=4,Objy1=2,Objx2=6,Objy2=2

Prob0E: 5 iterations in 0.06 seconds (average 0.011200, setup 0.00)

Prob1A: 1 iterations in 0.00 seconds (average 0.004000, setup 0.00)

goal = 1, inf = 0, maybe = 258802

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=258803, nc=355707, nnz=742923, k=4] [10.8 MB]
Building sparse matrix (transition rewards)... [n=258803, nc=355707, nnz=0, k=4] [2.3 MB]
Creating vector for state rewards... [2.0 MB]
Creating vector for inf... [2.0 MB]
Allocating iteration vectors... [2 x 2.0 MB]
TOTAL: [21.1 MB]

Starting iterations...

Iterative method: 96 iterations in 0.82 seconds (average 0.003833, setup 0.46)

Value in the initial state: 0.25385785387243337

Time for model checking: 0.883 seconds.

Result: 0.25385785387243337 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"drop"}min=? [ F "deadlock" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=4,Objy1=2,Objx2=6,Objy2=2

Prob0A: 4 iterations in 0.04 seconds (average 0.009000, setup 0.00)

Prob1E: 5 iterations in 0.04 seconds (average 0.008000, setup 0.00)

Warning: PRISM hasn't checked for zero-reward loops.
Your minimum rewards may be too low...

goal = 1, inf = 0, maybe = 258802

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=258803, nc=355707, nnz=742923, k=4] [10.8 MB]
Building sparse matrix (transition rewards)... [n=258803, nc=355707, nnz=0, k=4] [2.3 MB]
Creating vector for state rewards... [2.0 MB]
Creating vector for inf... [2.0 MB]
Allocating iteration vectors... [2 x 2.0 MB]
TOTAL: [21.1 MB]

Starting iterations...

Iterative method: 86 iterations in 0.68 seconds (average 0.003814, setup 0.36)

Value in the initial state: 0.20546554093119543

Time for model checking: 0.773 seconds.

Result: 0.20546554093119543 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"charge"}max=? [ F "deadlock" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=4,Objy1=2,Objx2=6,Objy2=2

Prob0E: 5 iterations in 0.02 seconds (average 0.004800, setup 0.00)

Prob1A: 1 iterations in 0.00 seconds (average 0.004000, setup 0.00)

goal = 1, inf = 0, maybe = 258802

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=258803, nc=355707, nnz=742923, k=4] [10.8 MB]
Building sparse matrix (transition rewards)... [n=258803, nc=355707, nnz=9596, k=4] [2.5 MB]
Creating vector for state rewards... [2.0 MB]
Creating vector for inf... [2.0 MB]
Allocating iteration vectors... [2 x 2.0 MB]
TOTAL: [21.2 MB]

Starting iterations...

Iterative method: 91 iterations in 0.70 seconds (average 0.003824, setup 0.36)

Value in the initial state: 0.3117670961985525

Time for model checking: 0.746 seconds.

Result: 0.3117670961985525 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"charge"}min=? [ F "deadlock" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=4,Objy1=2,Objx2=6,Objy2=2

Prob0A: 4 iterations in 0.02 seconds (average 0.005000, setup 0.00)

Prob1E: 5 iterations in 0.03 seconds (average 0.005600, setup 0.00)

Warning: PRISM hasn't checked for zero-reward loops.
Your minimum rewards may be too low...

goal = 1, inf = 0, maybe = 258802

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=258803, nc=355707, nnz=742923, k=4] [10.8 MB]
Building sparse matrix (transition rewards)... [n=258803, nc=355707, nnz=9596, k=4] [2.5 MB]
Creating vector for state rewards... [2.0 MB]
Creating vector for inf... [2.0 MB]
Allocating iteration vectors... [2 x 2.0 MB]
TOTAL: [21.2 MB]

Starting iterations...

Iterative method: 103 iterations in 0.76 seconds (average 0.003883, setup 0.36)

Value in the initial state: 0.060645405550257464

Time for model checking: 0.816 seconds.

Result: 0.060645405550257464 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"time"}max=? [ F "deadlock" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=4,Objy1=2,Objx2=6,Objy2=2

Prob0E: 5 iterations in 0.02 seconds (average 0.004800, setup 0.00)

Prob1A: 1 iterations in 0.00 seconds (average 0.004000, setup 0.00)

goal = 1, inf = 0, maybe = 258802

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=258803, nc=355707, nnz=742923, k=4] [10.8 MB]
Building sparse matrix (transition rewards)... [n=258803, nc=355707, nnz=129267, k=4] [3.8 MB]
Creating vector for state rewards... [2.0 MB]
Creating vector for inf... [2.0 MB]
Allocating iteration vectors... [2 x 2.0 MB]
TOTAL: [22.6 MB]

Starting iterations...

Iterative method: 97 iterations in 0.77 seconds (average 0.003835, setup 0.40)

Value in the initial state: 176.64913185187214

Time for model checking: 0.813 seconds.

Result: 176.64913185187214 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"time"}min=? [ F "deadlock" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=4,Objy1=2,Objx2=6,Objy2=2

Prob0A: 4 iterations in 0.02 seconds (average 0.005000, setup 0.00)

Prob1E: 5 iterations in 0.03 seconds (average 0.005600, setup 0.00)

Warning: PRISM hasn't checked for zero-reward loops.
Your minimum rewards may be too low...

goal = 1, inf = 0, maybe = 258802

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=258803, nc=355707, nnz=742923, k=4] [10.8 MB]
Building sparse matrix (transition rewards)... [n=258803, nc=355707, nnz=129267, k=4] [3.8 MB]
Creating vector for state rewards... [2.0 MB]
Creating vector for inf... [2.0 MB]
Allocating iteration vectors... [2 x 2.0 MB]
TOTAL: [22.6 MB]

Starting iterations...

Iterative method: 106 iterations in 0.81 seconds (average 0.003925, setup 0.40)

Value in the initial state: 95.81792059344788

Time for model checking: 0.869 seconds.

Result: 95.81792059344788 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"success"}max=? [ F "deadlock" ]/Pmin=? [ F "MissionSuccessful" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=4,Objy1=2,Objx2=6,Objy2=2

Prob0E: 5 iterations in 0.02 seconds (average 0.004800, setup 0.00)

Prob1A: 1 iterations in 0.00 seconds (average 0.004000, setup 0.00)

goal = 1, inf = 0, maybe = 258802

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=258803, nc=355707, nnz=742923, k=4] [10.8 MB]
Building sparse matrix (transition rewards)... [n=258803, nc=355707, nnz=2188, k=4] [2.4 MB]
Creating vector for state rewards... [2.0 MB]
Creating vector for inf... [2.0 MB]
Allocating iteration vectors... [2 x 2.0 MB]
TOTAL: [21.1 MB]

Starting iterations...

Iterative method: 98 iterations in 0.72 seconds (average 0.003714, setup 0.36)

Prob0E: 51 iterations in 0.27 seconds (average 0.005255, setup 0.00)

Prob1A: 3 iterations in 0.05 seconds (average 0.016000, setup 0.00)

yes = 1111, no = 219278, maybe = 38414

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=258803, nc=69402, nnz=202769, k=2] [2.6 MB]
Creating vector for yes... [2.0 MB]
Allocating iteration vectors... [2 x 2.0 MB]
TOTAL: [8.6 MB]

Starting iterations...

Iterative method: 92 iterations in 0.33 seconds (average 0.001739, setup 0.17)

Value in the initial state: 216.05670823867143

Time for model checking: 1.437 seconds.

Result: 216.05670823867143 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"success"}min=? [ F "deadlock" ]/Pmax=? [ F "MissionSuccessful" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=4,Objy1=2,Objx2=6,Objy2=2

Prob0A: 4 iterations in 0.04 seconds (average 0.011000, setup 0.00)

Prob1E: 5 iterations in 0.04 seconds (average 0.008000, setup 0.00)

Warning: PRISM hasn't checked for zero-reward loops.
Your minimum rewards may be too low...

goal = 1, inf = 0, maybe = 258802

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=258803, nc=355707, nnz=742923, k=4] [10.8 MB]
Building sparse matrix (transition rewards)... [n=258803, nc=355707, nnz=2188, k=4] [2.4 MB]
Creating vector for state rewards... [2.0 MB]
Creating vector for inf... [2.0 MB]
Allocating iteration vectors... [2 x 2.0 MB]
TOTAL: [21.1 MB]

Starting iterations...

Iterative method: 99 iterations in 0.76 seconds (average 0.003758, setup 0.39)

Prob0A: 51 iterations in 0.25 seconds (average 0.004941, setup 0.00)

Prob1E: 4 iterations in 0.03 seconds (average 0.008000, setup 0.00)

yes = 1111, no = 213515, maybe = 44177

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=258803, nc=80198, nnz=234193, k=2] [3.0 MB]
Creating vector for yes... [2.0 MB]
Allocating iteration vectors... [2 x 2.0 MB]
TOTAL: [8.9 MB]

Starting iterations...

Iterative method: 102 iterations in 0.35 seconds (average 0.001686, setup 0.18)

Value in the initial state: 78.60657180688696

Time for model checking: 1.514 seconds.

Result: 78.60657180688696 (value in the initial state)

---------------------------------------------------------------------

Note: There were 16 warnings during computation.

