PRISM
=====

Version: 4.3
Date: Wed Aug 03 16:07:57 BST 2016
Hostname: eglinton
Memory limits: cudd=1g, java(heap)=981.5m
Command line: prism uav_sim_mdp.prism -s uav_mdp.props -const 'Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=0,Objy1=2,Objx2=0,Objy2=3,'

Parsing model file "uav_sim_mdp.prism"...

Parsing properties file "uav_mdp.props"...

18 properties:
(1) Pmax=? [ F "MissionSuccessful" ]
(2) Pmin=? [ F "MissionSuccessful" ]
(3) Pmax=? [ F "MissionTimeUp" ]
(4) Pmin=? [ F "MissionTimeUp" ]
(5) Pmax=? [ F "Crash" ]
(6) Pmin=? [ F "Crash" ]
(7) Pmax=? [ F "AllAreaSearched" ]
(8) Pmin=? [ F "AllAreaSearched" ]
(9) Pmax=? [ F "MissionTimeUp"|"AllAreaSearched"|"Crash" ]
(10) Pmin=? [ F "MissionTimeUp"|"AllAreaSearched"|"Crash" ]
(11) R{"drop"}max=? [ F "deadlock" ]
(12) R{"drop"}min=? [ F "deadlock" ]
(13) R{"charge"}max=? [ F "deadlock" ]
(14) R{"charge"}min=? [ F "deadlock" ]
(15) R{"time"}max=? [ F "deadlock" ]
(16) R{"time"}min=? [ F "deadlock" ]
(17) R{"success"}max=? [ F "deadlock" ]/Pmin=? [ F "MissionSuccessful" ]
(18) R{"success"}min=? [ F "deadlock" ]/Pmax=? [ F "MissionSuccessful" ]

Type:        MDP
Modules:     Obj Move Robot T 
Variables:   NoOfObjs posx posy s1 retx rety b t c 

---------------------------------------------------------------------

Model checking: Pmax=? [ F "MissionSuccessful" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=0,Objy1=2,Objx2=0,Objy2=3

Building model...
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=0,Objy1=2,Objx2=0,Objy2=3

Warning: Guard for command 7 of module "Move" is never satisfied.

Warning: Guard for command 8 of module "Move" is never satisfied.

Warning: Guard for command 9 of module "Move" is never satisfied.

Warning: Guard for command 6 of module "Robot" is never satisfied.

Warning: Update 2 of command 3 of module "Robot" doesn't do anything ("(s1'=13)", line 210, column 44)

Warning: Update 2 of command 27 of module "T" doesn't do anything ("(c'=1)", line 323, column 108)

Warning: Guard for command 28 of module "T" is never satisfied.

Warning: Update 2 of command 9 of module "T" doesn't do anything ("(c'=1)", line 300, column 112)

Warning: Guard for command 10 of module "T" is never satisfied.

Warning: Update 2 of command 51 of module "T" doesn't do anything ("(c'=1)", line 354, column 113)

Warning: Guard for command 52 of module "T" is never satisfied.

Computing reachable states...

Reachability (BFS): 89 iterations in 1.14 seconds (average 0.012764, setup 0.00)

Time for model construction: 3.528 seconds.

Warning: Deadlocks detected and fixed in 1 states

Type:        MDP
States:      185271 (1 initial)
Transitions: 507644
Choices:     249281

Transition matrix: 209245 nodes (87 terminal), 507644 minterms, vars: 34r/34c/18nd

Prob0A: 56 iterations in 0.58 seconds (average 0.010429, setup 0.00)

Prob1E: 4 iterations in 0.07 seconds (average 0.018000, setup 0.00)

yes = 868, no = 141805, maybe = 42598

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=185271, nc=73694, nnz=204627, k=2] [2.6 MB]
Creating vector for yes... [1.4 MB]
Allocating iteration vectors... [2 x 1.4 MB]
TOTAL: [6.8 MB]

Starting iterations...

Iterative method: 96 iterations in 0.41 seconds (average 0.001375, setup 0.28)

Value in the initial state: 0.9354454879855728

Time for model checking: 1.133 seconds.

Result: 0.9354454879855728 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmin=? [ F "MissionSuccessful" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=0,Objy1=2,Objx2=0,Objy2=3

Prob0E: 57 iterations in 0.62 seconds (average 0.010807, setup 0.00)

Prob1A: 3 iterations in 0.07 seconds (average 0.024000, setup 0.00)

yes = 868, no = 150597, maybe = 33806

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=185271, nc=58135, nnz=160419, k=2] [2.1 MB]
Creating vector for yes... [1.4 MB]
Allocating iteration vectors... [2 x 1.4 MB]
TOTAL: [6.3 MB]

Starting iterations...

Iterative method: 98 iterations in 0.39 seconds (average 0.001306, setup 0.26)

Value in the initial state: 0.7088707641200844

Time for model checking: 1.142 seconds.

Result: 0.7088707641200844 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmax=? [ F "MissionTimeUp" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=0,Objy1=2,Objx2=0,Objy2=3

Prob0A: 75 iterations in 0.44 seconds (average 0.005920, setup 0.00)

Prob1E: 4 iterations in 0.05 seconds (average 0.012000, setup 0.00)

yes = 256, no = 154437, maybe = 30578

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=185271, nc=53844, nnz=148102, k=4] [1.9 MB]
Creating vector for yes... [1.4 MB]
Allocating iteration vectors... [2 x 1.4 MB]
TOTAL: [6.2 MB]

Starting iterations...

Iterative method: 97 iterations in 0.27 seconds (average 0.001237, setup 0.15)

Value in the initial state: 2.820700296634758E-6

Time for model checking: 0.822 seconds.

Result: 2.820700296634758E-6 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmin=? [ F "MissionTimeUp" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=0,Objy1=2,Objx2=0,Objy2=3

Prob0E: 89 iterations in 0.61 seconds (average 0.006831, setup 0.00)

Prob1A: 3 iterations in 0.03 seconds (average 0.010667, setup 0.00)

yes = 256, no = 154687, maybe = 30328

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=185271, nc=53400, nnz=147104, k=4] [1.9 MB]
Creating vector for yes... [1.4 MB]
Allocating iteration vectors... [2 x 1.4 MB]
TOTAL: [6.2 MB]

Starting iterations...

Iterative method: 116 iterations in 0.24 seconds (average 0.001310, setup 0.08)

Value in the initial state: 1.1615489142216322E-9

Time for model checking: 1.297 seconds.

Result: 1.1615489142216322E-9 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmax=? [ F "Crash" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=0,Objy1=2,Objx2=0,Objy2=3

Prob0A: 3 iterations in 0.10 seconds (average 0.034667, setup 0.00)

Prob1E: 204 iterations in 3.18 seconds (average 0.015569, setup 0.00)

yes = 90712, no = 1365, maybe = 93194

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=185271, nc=157204, nnz=415567, k=4] [5.1 MB]
Creating vector for yes... [1.4 MB]
Allocating iteration vectors... [2 x 1.4 MB]
TOTAL: [9.3 MB]

Starting iterations...

Iterative method: 94 iterations in 0.48 seconds (average 0.001745, setup 0.31)

Value in the initial state: 0.01863897826479214

Time for model checking: 4.06 seconds.

Result: 0.01863897826479214 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmin=? [ F "Crash" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=0,Objy1=2,Objx2=0,Objy2=3

Prob0E: 3 iterations in 0.06 seconds (average 0.021333, setup 0.00)

Prob1A: 53 iterations in 0.54 seconds (average 0.010189, setup 0.00)

yes = 90712, no = 1365, maybe = 93194

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=185271, nc=157204, nnz=415567, k=4] [5.1 MB]
Creating vector for yes... [1.4 MB]
Allocating iteration vectors... [2 x 1.4 MB]
TOTAL: [9.3 MB]

Starting iterations...

Iterative method: 90 iterations in 0.36 seconds (average 0.001867, setup 0.20)

Value in the initial state: 0.015415661390242322

Time for model checking: 1.036 seconds.

Result: 0.015415661390242322 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmax=? [ F "AllAreaSearched" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=0,Objy1=2,Objx2=0,Objy2=3

Prob0A: 53 iterations in 0.49 seconds (average 0.009208, setup 0.00)

Prob1E: 4 iterations in 0.05 seconds (average 0.013000, setup 0.00)

yes = 240, no = 111820, maybe = 73211

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=185271, nc=122129, nnz=332148, k=2] [4.1 MB]
Creating vector for yes... [1.4 MB]
Allocating iteration vectors... [2 x 1.4 MB]
TOTAL: [8.3 MB]

Starting iterations...

Iterative method: 98 iterations in 0.42 seconds (average 0.001592, setup 0.27)

Value in the initial state: 0.27381183946241244

Time for model checking: 1.025 seconds.

Result: 0.27381183946241244 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmin=? [ F "AllAreaSearched" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=0,Objy1=2,Objx2=0,Objy2=3

Prob0E: 65 iterations in 0.71 seconds (average 0.010892, setup 0.00)

Prob1A: 3 iterations in 0.05 seconds (average 0.017333, setup 0.00)

yes = 240, no = 113767, maybe = 71264

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=185271, nc=118459, nnz=321383, k=2] [4.0 MB]
Creating vector for yes... [1.4 MB]
Allocating iteration vectors... [2 x 1.4 MB]
TOTAL: [8.2 MB]

Starting iterations...

Iterative method: 104 iterations in 0.42 seconds (average 0.001615, setup 0.25)

Value in the initial state: 0.04887911679140075

Time for model checking: 1.404 seconds.

Result: 0.04887911679140075 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmax=? [ F "MissionTimeUp"|"AllAreaSearched"|"Crash" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=0,Objy1=2,Objx2=0,Objy2=3

Prob0A: 3 iterations in 0.10 seconds (average 0.034667, setup 0.00)

Prob1E: 221 iterations in 3.41 seconds (average 0.015421, setup 0.00)

yes = 150596, no = 869, maybe = 33806

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=185271, nc=58135, nnz=160419, k=2] [2.1 MB]
Creating vector for yes... [1.4 MB]
Allocating iteration vectors... [2 x 1.4 MB]
TOTAL: [6.3 MB]

Starting iterations...

Iterative method: 89 iterations in 0.32 seconds (average 0.001303, setup 0.21)

Value in the initial state: 0.2911288478490846

Time for model checking: 4.765 seconds.

Result: 0.2911288478490846 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmin=? [ F "MissionTimeUp"|"AllAreaSearched"|"Crash" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=0,Objy1=2,Objx2=0,Objy2=3

Prob0E: 3 iterations in 0.08 seconds (average 0.028000, setup 0.00)

Prob1A: 56 iterations in 0.56 seconds (average 0.009929, setup 0.00)

yes = 141804, no = 869, maybe = 42598

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=185271, nc=73694, nnz=204627, k=2] [2.6 MB]
Creating vector for yes... [1.4 MB]
Allocating iteration vectors... [2 x 1.4 MB]
TOTAL: [6.8 MB]

Starting iterations...

Iterative method: 90 iterations in 0.31 seconds (average 0.001422, setup 0.18)

Value in the initial state: 0.06455238208663472

Time for model checking: 1.046 seconds.

Result: 0.06455238208663472 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"drop"}max=? [ F "deadlock" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=0,Objy1=2,Objx2=0,Objy2=3

Prob0E: 4 iterations in 0.06 seconds (average 0.016000, setup 0.00)

Prob1A: 1 iterations in 0.01 seconds (average 0.008000, setup 0.00)

goal = 1, inf = 0, maybe = 185270

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=185271, nc=249280, nnz=507643, k=4] [7.5 MB]
Building sparse matrix (transition rewards)... [n=185271, nc=249280, nnz=0, k=4] [1.7 MB]
Creating vector for state rewards... [1.4 MB]
Creating vector for inf... [1.4 MB]
Allocating iteration vectors... [2 x 1.4 MB]
TOTAL: [14.8 MB]

Starting iterations...

Iterative method: 94 iterations in 0.63 seconds (average 0.002723, setup 0.38)

Value in the initial state: 0.16201716191711632

Time for model checking: 0.876 seconds.

Result: 0.16201716191711632 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"drop"}min=? [ F "deadlock" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=0,Objy1=2,Objx2=0,Objy2=3

Prob0A: 4 iterations in 0.06 seconds (average 0.016000, setup 0.00)

Prob1E: 5 iterations in 0.06 seconds (average 0.012800, setup 0.00)

Warning: PRISM hasn't checked for zero-reward loops.
Your minimum rewards may be too low...

goal = 1, inf = 0, maybe = 185270

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=185271, nc=249280, nnz=507643, k=4] [7.5 MB]
Building sparse matrix (transition rewards)... [n=185271, nc=249280, nnz=0, k=4] [1.7 MB]
Creating vector for state rewards... [1.4 MB]
Creating vector for inf... [1.4 MB]
Allocating iteration vectors... [2 x 1.4 MB]
TOTAL: [14.8 MB]

Starting iterations...

Iterative method: 90 iterations in 0.54 seconds (average 0.002800, setup 0.28)

Value in the initial state: 0.14509057263798078

Time for model checking: 0.725 seconds.

Result: 0.14509057263798078 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"charge"}max=? [ F "deadlock" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=0,Objy1=2,Objx2=0,Objy2=3

Prob0E: 4 iterations in 0.03 seconds (average 0.008000, setup 0.00)

Prob1A: 1 iterations in 0.00 seconds (average 0.004000, setup 0.00)

goal = 1, inf = 0, maybe = 185270

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=185271, nc=249280, nnz=507643, k=4] [7.5 MB]
Building sparse matrix (transition rewards)... [n=185271, nc=249280, nnz=6150, k=4] [1.7 MB]
Creating vector for state rewards... [1.4 MB]
Creating vector for inf... [1.4 MB]
Allocating iteration vectors... [2 x 1.4 MB]
TOTAL: [14.8 MB]

Starting iterations...

Iterative method: 96 iterations in 0.55 seconds (average 0.002708, setup 0.29)

Value in the initial state: 0.2748278088408907

Time for model checking: 0.664 seconds.

Result: 0.2748278088408907 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"charge"}min=? [ F "deadlock" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=0,Objy1=2,Objx2=0,Objy2=3

Prob0A: 4 iterations in 0.02 seconds (average 0.006000, setup 0.00)

Prob1E: 5 iterations in 0.06 seconds (average 0.012000, setup 0.00)

Warning: PRISM hasn't checked for zero-reward loops.
Your minimum rewards may be too low...

goal = 1, inf = 0, maybe = 185270

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=185271, nc=249280, nnz=507643, k=4] [7.5 MB]
Building sparse matrix (transition rewards)... [n=185271, nc=249280, nnz=6150, k=4] [1.7 MB]
Creating vector for state rewards... [1.4 MB]
Creating vector for inf... [1.4 MB]
Allocating iteration vectors... [2 x 1.4 MB]
TOTAL: [14.8 MB]

Starting iterations...

Iterative method: 102 iterations in 0.58 seconds (average 0.002784, setup 0.29)

Value in the initial state: 0.04768124823532332

Time for model checking: 0.733 seconds.

Result: 0.04768124823532332 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"time"}max=? [ F "deadlock" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=0,Objy1=2,Objx2=0,Objy2=3

Prob0E: 4 iterations in 0.03 seconds (average 0.008000, setup 0.00)

Prob1A: 1 iterations in 0.00 seconds (average 0.004000, setup 0.00)

goal = 1, inf = 0, maybe = 185270

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=185271, nc=249280, nnz=507643, k=4] [7.5 MB]
Building sparse matrix (transition rewards)... [n=185271, nc=249280, nnz=92075, k=4] [2.7 MB]
Creating vector for state rewards... [1.4 MB]
Creating vector for inf... [1.4 MB]
Allocating iteration vectors... [2 x 1.4 MB]
TOTAL: [15.8 MB]

Starting iterations...

Iterative method: 96 iterations in 0.59 seconds (average 0.002833, setup 0.32)

Value in the initial state: 103.4708789862678

Time for model checking: 0.653 seconds.

Result: 103.4708789862678 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"time"}min=? [ F "deadlock" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=0,Objy1=2,Objx2=0,Objy2=3

Prob0A: 4 iterations in 0.03 seconds (average 0.007000, setup 0.00)

Prob1E: 5 iterations in 0.06 seconds (average 0.012000, setup 0.00)

Warning: PRISM hasn't checked for zero-reward loops.
Your minimum rewards may be too low...

goal = 1, inf = 0, maybe = 185270

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=185271, nc=249280, nnz=507643, k=4] [7.5 MB]
Building sparse matrix (transition rewards)... [n=185271, nc=249280, nnz=92075, k=4] [2.7 MB]
Creating vector for state rewards... [1.4 MB]
Creating vector for inf... [1.4 MB]
Allocating iteration vectors... [2 x 1.4 MB]
TOTAL: [15.8 MB]

Starting iterations...

Iterative method: 105 iterations in 0.60 seconds (average 0.002819, setup 0.31)

Value in the initial state: 85.60920934589018

Time for model checking: 0.75 seconds.

Result: 85.60920934589018 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"success"}max=? [ F "deadlock" ]/Pmin=? [ F "MissionSuccessful" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=0,Objy1=2,Objx2=0,Objy2=3

Prob0E: 4 iterations in 0.03 seconds (average 0.008000, setup 0.00)

Prob1A: 1 iterations in 0.00 seconds (average 0.004000, setup 0.00)

goal = 1, inf = 0, maybe = 185270

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=185271, nc=249280, nnz=507643, k=4] [7.5 MB]
Building sparse matrix (transition rewards)... [n=185271, nc=249280, nnz=1718, k=4] [1.7 MB]
Creating vector for state rewards... [1.4 MB]
Creating vector for inf... [1.4 MB]
Allocating iteration vectors... [2 x 1.4 MB]
TOTAL: [14.8 MB]

Starting iterations...

Iterative method: 103 iterations in 0.56 seconds (average 0.002757, setup 0.28)

Prob0E: 57 iterations in 0.49 seconds (average 0.008632, setup 0.00)

Prob1A: 3 iterations in 0.06 seconds (average 0.020000, setup 0.00)

yes = 868, no = 150597, maybe = 33806

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=185271, nc=58135, nnz=160419, k=2] [2.1 MB]
Creating vector for yes... [1.4 MB]
Allocating iteration vectors... [2 x 1.4 MB]
TOTAL: [6.3 MB]

Starting iterations...

Iterative method: 98 iterations in 0.31 seconds (average 0.001306, setup 0.18)

Value in the initial state: 124.33010955867505

Time for model checking: 1.51 seconds.

Result: 124.33010955867505 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"success"}min=? [ F "deadlock" ]/Pmax=? [ F "MissionSuccessful" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=0,Objy1=2,Objx2=0,Objy2=3

Prob0A: 4 iterations in 0.07 seconds (average 0.017000, setup 0.00)

Prob1E: 5 iterations in 0.06 seconds (average 0.012800, setup 0.00)

Warning: PRISM hasn't checked for zero-reward loops.
Your minimum rewards may be too low...

goal = 1, inf = 0, maybe = 185270

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=185271, nc=249280, nnz=507643, k=4] [7.5 MB]
Building sparse matrix (transition rewards)... [n=185271, nc=249280, nnz=1718, k=4] [1.7 MB]
Creating vector for state rewards... [1.4 MB]
Creating vector for inf... [1.4 MB]
Allocating iteration vectors... [2 x 1.4 MB]
TOTAL: [14.8 MB]

Starting iterations...

Iterative method: 101 iterations in 0.59 seconds (average 0.002812, setup 0.31)

Prob0A: 56 iterations in 0.41 seconds (average 0.007357, setup 0.00)

Prob1E: 4 iterations in 0.04 seconds (average 0.010000, setup 0.00)

yes = 868, no = 141805, maybe = 42598

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=185271, nc=73694, nnz=204627, k=2] [2.6 MB]
Creating vector for yes... [1.4 MB]
Allocating iteration vectors... [2 x 1.4 MB]
TOTAL: [6.8 MB]

Starting iterations...

Iterative method: 96 iterations in 0.30 seconds (average 0.001292, setup 0.18)

Value in the initial state: 66.29000864482059

Time for model checking: 1.52 seconds.

Result: 66.29000864482059 (value in the initial state)

---------------------------------------------------------------------

Note: There were 16 warnings during computation.

