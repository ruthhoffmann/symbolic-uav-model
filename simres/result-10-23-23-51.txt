PRISM
=====

Version: 4.3
Date: Wed Aug 03 16:12:46 BST 2016
Hostname: eglinton
Memory limits: cudd=1g, java(heap)=981.5m
Command line: prism uav_sim_mdp.prism -s uav_mdp.props -const 'Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=2,Objy1=3,Objx2=5,Objy2=1,'

Parsing model file "uav_sim_mdp.prism"...

Parsing properties file "uav_mdp.props"...

18 properties:
(1) Pmax=? [ F "MissionSuccessful" ]
(2) Pmin=? [ F "MissionSuccessful" ]
(3) Pmax=? [ F "MissionTimeUp" ]
(4) Pmin=? [ F "MissionTimeUp" ]
(5) Pmax=? [ F "Crash" ]
(6) Pmin=? [ F "Crash" ]
(7) Pmax=? [ F "AllAreaSearched" ]
(8) Pmin=? [ F "AllAreaSearched" ]
(9) Pmax=? [ F "MissionTimeUp"|"AllAreaSearched"|"Crash" ]
(10) Pmin=? [ F "MissionTimeUp"|"AllAreaSearched"|"Crash" ]
(11) R{"drop"}max=? [ F "deadlock" ]
(12) R{"drop"}min=? [ F "deadlock" ]
(13) R{"charge"}max=? [ F "deadlock" ]
(14) R{"charge"}min=? [ F "deadlock" ]
(15) R{"time"}max=? [ F "deadlock" ]
(16) R{"time"}min=? [ F "deadlock" ]
(17) R{"success"}max=? [ F "deadlock" ]/Pmin=? [ F "MissionSuccessful" ]
(18) R{"success"}min=? [ F "deadlock" ]/Pmax=? [ F "MissionSuccessful" ]

Type:        MDP
Modules:     Obj Move Robot T 
Variables:   NoOfObjs posx posy s1 retx rety b t c 

---------------------------------------------------------------------

Model checking: Pmax=? [ F "MissionSuccessful" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=2,Objy1=3,Objx2=5,Objy2=1

Building model...
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=2,Objy1=3,Objx2=5,Objy2=1

Warning: Guard for command 7 of module "Move" is never satisfied.

Warning: Guard for command 8 of module "Move" is never satisfied.

Warning: Guard for command 9 of module "Move" is never satisfied.

Warning: Guard for command 7 of module "Robot" is never satisfied.

Warning: Update 2 of command 3 of module "Robot" doesn't do anything ("(s1'=13)", line 210, column 44)

Warning: Update 2 of command 27 of module "T" doesn't do anything ("(c'=1)", line 323, column 108)

Warning: Guard for command 28 of module "T" is never satisfied.

Warning: Update 2 of command 9 of module "T" doesn't do anything ("(c'=1)", line 300, column 112)

Warning: Guard for command 10 of module "T" is never satisfied.

Warning: Update 2 of command 51 of module "T" doesn't do anything ("(c'=1)", line 354, column 113)

Warning: Guard for command 52 of module "T" is never satisfied.

Computing reachable states...

Reachability (BFS): 87 iterations in 0.78 seconds (average 0.008966, setup 0.00)

Time for model construction: 2.651 seconds.

Warning: Deadlocks detected and fixed in 1 states

Type:        MDP
States:      142664 (1 initial)
Transitions: 342659
Choices:     180029

Transition matrix: 176676 nodes (91 terminal), 342659 minterms, vars: 34r/34c/18nd

Prob0A: 60 iterations in 0.28 seconds (average 0.004600, setup 0.00)

Prob1E: 4 iterations in 0.04 seconds (average 0.010000, setup 0.00)

yes = 210, no = 135505, maybe = 6949

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=142664, nc=12723, nnz=37477, k=2] [590.9 KB]
Creating vector for yes... [1.1 MB]
Allocating iteration vectors... [2 x 1.1 MB]
TOTAL: [3.8 MB]

Starting iterations...

Iterative method: 92 iterations in 0.23 seconds (average 0.001174, setup 0.12)

Value in the initial state: 0.001002383762852472

Time for model checking: 0.571 seconds.

Result: 0.001002383762852472 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmin=? [ F "MissionSuccessful" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=2,Objy1=3,Objx2=5,Objy2=1

Prob0E: 22 iterations in 0.14 seconds (average 0.006364, setup 0.00)

Prob1A: 3 iterations in 0.03 seconds (average 0.010667, setup 0.00)

yes = 210, no = 137071, maybe = 5383

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=142664, nc=9838, nnz=28717, k=2] [485.5 KB]
Creating vector for yes... [1.1 MB]
Allocating iteration vectors... [2 x 1.1 MB]
TOTAL: [3.7 MB]

Starting iterations...

Iterative method: 42 iterations in 0.07 seconds (average 0.000762, setup 0.04)

Value in the initial state: 0.0

Time for model checking: 0.254 seconds.

Result: 0.0 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmax=? [ F "MissionTimeUp" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=2,Objy1=3,Objx2=5,Objy2=1

Prob0A: 71 iterations in 0.65 seconds (average 0.009127, setup 0.00)

Prob1E: 4 iterations in 0.03 seconds (average 0.008000, setup 0.00)

yes = 344, no = 101211, maybe = 41109

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=142664, nc=71260, nnz=195663, k=4] [2.4 MB]
Creating vector for yes... [1.1 MB]
Allocating iteration vectors... [2 x 1.1 MB]
TOTAL: [5.7 MB]

Starting iterations...

Iterative method: 97 iterations in 0.28 seconds (average 0.001155, setup 0.17)

Value in the initial state: 1.914214260008922E-5

Time for model checking: 0.977 seconds.

Result: 1.914214260008922E-5 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmin=? [ F "MissionTimeUp" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=2,Objy1=3,Objx2=5,Objy2=1

Prob0E: 85 iterations in 0.60 seconds (average 0.007012, setup 0.00)

Prob1A: 3 iterations in 0.04 seconds (average 0.013333, setup 0.00)

yes = 344, no = 101480, maybe = 40840

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=142664, nc=70778, nnz=194626, k=4] [2.4 MB]
Creating vector for yes... [1.1 MB]
Allocating iteration vectors... [2 x 1.1 MB]
TOTAL: [5.7 MB]

Starting iterations...

Iterative method: 115 iterations in 0.30 seconds (average 0.001113, setup 0.17)

Value in the initial state: 1.532510689241939E-8

Time for model checking: 0.95 seconds.

Result: 1.532510689241939E-8 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmax=? [ F "Crash" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=2,Objy1=3,Objx2=5,Objy2=1

Prob0A: 3 iterations in 0.08 seconds (average 0.028000, setup 0.00)

Prob1E: 176 iterations in 2.30 seconds (average 0.013045, setup 0.00)

yes = 69966, no = 1881, maybe = 70817

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=142664, nc=108182, nnz=270812, k=4] [3.3 MB]
Creating vector for yes... [1.1 MB]
Allocating iteration vectors... [2 x 1.1 MB]
TOTAL: [6.6 MB]

Starting iterations...

Iterative method: 92 iterations in 0.36 seconds (average 0.001261, setup 0.24)

Value in the initial state: 0.014715983746554197

Time for model checking: 2.784 seconds.

Result: 0.014715983746554197 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmin=? [ F "Crash" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=2,Objy1=3,Objx2=5,Objy2=1

Prob0E: 3 iterations in 0.05 seconds (average 0.017333, setup 0.00)

Prob1A: 46 iterations in 0.51 seconds (average 0.011043, setup 0.00)

yes = 69966, no = 1881, maybe = 70817

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=142664, nc=108182, nnz=270812, k=4] [3.3 MB]
Creating vector for yes... [1.1 MB]
Allocating iteration vectors... [2 x 1.1 MB]
TOTAL: [6.6 MB]

Starting iterations...

Iterative method: 78 iterations in 0.28 seconds (average 0.001333, setup 0.18)

Value in the initial state: 0.011589168834985886

Time for model checking: 0.87 seconds.

Result: 0.011589168834985886 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmax=? [ F "AllAreaSearched" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=2,Objy1=3,Objx2=5,Objy2=1

Prob0A: 46 iterations in 0.32 seconds (average 0.006870, setup 0.00)

Prob1E: 4 iterations in 0.04 seconds (average 0.009000, setup 0.00)

yes = 1326, no = 98051, maybe = 43287

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=142664, nc=61417, nnz=152236, k=2] [1.9 MB]
Creating vector for yes... [1.1 MB]
Allocating iteration vectors... [2 x 1.1 MB]
TOTAL: [5.2 MB]

Starting iterations...

Iterative method: 88 iterations in 0.23 seconds (average 0.001045, setup 0.14)

Value in the initial state: 0.9884087998966437

Time for model checking: 0.574 seconds.

Result: 0.9884087998966437 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmin=? [ F "AllAreaSearched" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=2,Objy1=3,Objx2=5,Objy2=1

Prob0E: 50 iterations in 0.28 seconds (average 0.005520, setup 0.00)

Prob1A: 3 iterations in 0.02 seconds (average 0.008000, setup 0.00)

yes = 1326, no = 100572, maybe = 40766

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=142664, nc=56641, nnz=138069, k=2] [1.8 MB]
Creating vector for yes... [1.1 MB]
Allocating iteration vectors... [2 x 1.1 MB]
TOTAL: [5.0 MB]

Starting iterations...

Iterative method: 92 iterations in 0.19 seconds (average 0.001043, setup 0.10)

Value in the initial state: 0.9842941162503119

Time for model checking: 0.503 seconds.

Result: 0.9842941162503119 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmax=? [ F "MissionTimeUp"|"AllAreaSearched"|"Crash" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=2,Objy1=3,Objx2=5,Objy2=1

Prob0A: 3 iterations in 0.07 seconds (average 0.022667, setup 0.00)

Prob1E: 84 iterations in 0.83 seconds (average 0.009905, setup 0.00)

yes = 137070, no = 211, maybe = 5383

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=142664, nc=9838, nnz=28717, k=2] [485.5 KB]
Creating vector for yes... [1.1 MB]
Allocating iteration vectors... [2 x 1.1 MB]
TOTAL: [3.7 MB]

Starting iterations...

Iterative method: 41 iterations in 0.08 seconds (average 0.000780, setup 0.04)

Value in the initial state: 1.0

Time for model checking: 0.993 seconds.

Result: 1.0 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmin=? [ F "MissionTimeUp"|"AllAreaSearched"|"Crash" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=2,Objy1=3,Objx2=5,Objy2=1

Prob0E: 3 iterations in 0.04 seconds (average 0.012000, setup 0.00)

Prob1A: 60 iterations in 0.38 seconds (average 0.006333, setup 0.00)

yes = 135504, no = 211, maybe = 6949

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=142664, nc=12723, nnz=37477, k=2] [590.9 KB]
Creating vector for yes... [1.1 MB]
Allocating iteration vectors... [2 x 1.1 MB]
TOTAL: [3.8 MB]

Starting iterations...

Iterative method: 55 iterations in 0.10 seconds (average 0.000800, setup 0.06)

Value in the initial state: 0.9989937549623784

Time for model checking: 0.526 seconds.

Result: 0.9989937549623784 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"drop"}max=? [ F "deadlock" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=2,Objy1=3,Objx2=5,Objy2=1

Prob0E: 4 iterations in 0.05 seconds (average 0.012000, setup 0.00)

Prob1A: 1 iterations in 0.01 seconds (average 0.008000, setup 0.00)

goal = 1, inf = 0, maybe = 142663

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=142664, nc=180028, nnz=342658, k=4] [5.2 MB]
Building sparse matrix (transition rewards)... [n=142664, nc=180028, nnz=0, k=4] [1.2 MB]
Creating vector for state rewards... [1.1 MB]
Creating vector for inf... [1.1 MB]
Allocating iteration vectors... [2 x 1.1 MB]
TOTAL: [10.7 MB]

Starting iterations...

Iterative method: 70 iterations in 0.43 seconds (average 0.002000, setup 0.29)

Value in the initial state: 0.11001898354720173

Time for model checking: 0.494 seconds.

Result: 0.11001898354720173 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"drop"}min=? [ F "deadlock" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=2,Objy1=3,Objx2=5,Objy2=1

Prob0A: 4 iterations in 0.02 seconds (average 0.006000, setup 0.00)

Prob1E: 5 iterations in 0.04 seconds (average 0.007200, setup 0.00)

Warning: PRISM hasn't checked for zero-reward loops.
Your minimum rewards may be too low...

goal = 1, inf = 0, maybe = 142663

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=142664, nc=180028, nnz=342658, k=4] [5.2 MB]
Building sparse matrix (transition rewards)... [n=142664, nc=180028, nnz=0, k=4] [1.2 MB]
Creating vector for state rewards... [1.1 MB]
Creating vector for inf... [1.1 MB]
Allocating iteration vectors... [2 x 1.1 MB]
TOTAL: [10.7 MB]

Starting iterations...

Iterative method: 62 iterations in 0.32 seconds (average 0.002000, setup 0.19)

Value in the initial state: 0.10981605198749746

Time for model checking: 0.391 seconds.

Result: 0.10981605198749746 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"charge"}max=? [ F "deadlock" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=2,Objy1=3,Objx2=5,Objy2=1

Prob0E: 4 iterations in 0.02 seconds (average 0.005000, setup 0.00)

Prob1A: 1 iterations in 0.00 seconds (average 0.004000, setup 0.00)

goal = 1, inf = 0, maybe = 142663

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=142664, nc=180028, nnz=342658, k=4] [5.2 MB]
Building sparse matrix (transition rewards)... [n=142664, nc=180028, nnz=3162, k=4] [1.3 MB]
Creating vector for state rewards... [1.1 MB]
Creating vector for inf... [1.1 MB]
Allocating iteration vectors... [2 x 1.1 MB]
TOTAL: [10.8 MB]

Starting iterations...

Iterative method: 82 iterations in 0.36 seconds (average 0.001951, setup 0.20)

Value in the initial state: 0.0021518310955123752

Time for model checking: 0.398 seconds.

Result: 0.0021518310955123752 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"charge"}min=? [ F "deadlock" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=2,Objy1=3,Objx2=5,Objy2=1

Prob0A: 4 iterations in 0.02 seconds (average 0.005000, setup 0.00)

Prob1E: 5 iterations in 0.04 seconds (average 0.007200, setup 0.00)

Warning: PRISM hasn't checked for zero-reward loops.
Your minimum rewards may be too low...

goal = 1, inf = 0, maybe = 142663

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=142664, nc=180028, nnz=342658, k=4] [5.2 MB]
Building sparse matrix (transition rewards)... [n=142664, nc=180028, nnz=3162, k=4] [1.3 MB]
Creating vector for state rewards... [1.1 MB]
Creating vector for inf... [1.1 MB]
Allocating iteration vectors... [2 x 1.1 MB]
TOTAL: [10.8 MB]

Starting iterations...

Iterative method: 93 iterations in 0.38 seconds (average 0.001978, setup 0.20)

Value in the initial state: 1.201860320076693E-4

Time for model checking: 0.45 seconds.

Result: 1.201860320076693E-4 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"time"}max=? [ F "deadlock" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=2,Objy1=3,Objx2=5,Objy2=1

Prob0E: 4 iterations in 0.02 seconds (average 0.005000, setup 0.00)

Prob1A: 1 iterations in 0.01 seconds (average 0.008000, setup 0.00)

goal = 1, inf = 0, maybe = 142663

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=142664, nc=180028, nnz=342658, k=4] [5.2 MB]
Building sparse matrix (transition rewards)... [n=142664, nc=180028, nnz=71845, k=4] [2.1 MB]
Creating vector for state rewards... [1.1 MB]
Creating vector for inf... [1.1 MB]
Allocating iteration vectors... [2 x 1.1 MB]
TOTAL: [11.6 MB]

Starting iterations...

Iterative method: 96 iterations in 0.42 seconds (average 0.002000, setup 0.23)

Value in the initial state: 81.6383801884869

Time for model checking: 0.457 seconds.

Result: 81.6383801884869 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"time"}min=? [ F "deadlock" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=2,Objy1=3,Objx2=5,Objy2=1

Prob0A: 4 iterations in 0.02 seconds (average 0.005000, setup 0.00)

Prob1E: 5 iterations in 0.04 seconds (average 0.007200, setup 0.00)

Warning: PRISM hasn't checked for zero-reward loops.
Your minimum rewards may be too low...

goal = 1, inf = 0, maybe = 142663

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=142664, nc=180028, nnz=342658, k=4] [5.2 MB]
Building sparse matrix (transition rewards)... [n=142664, nc=180028, nnz=71845, k=4] [2.1 MB]
Creating vector for state rewards... [1.1 MB]
Creating vector for inf... [1.1 MB]
Allocating iteration vectors... [2 x 1.1 MB]
TOTAL: [11.6 MB]

Starting iterations...

Iterative method: 96 iterations in 0.42 seconds (average 0.001958, setup 0.23)

Value in the initial state: 64.35726829293684

Time for model checking: 0.48 seconds.

Result: 64.35726829293684 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"success"}max=? [ F "deadlock" ]/Pmin=? [ F "MissionSuccessful" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=2,Objy1=3,Objx2=5,Objy2=1

Prob0E: 4 iterations in 0.02 seconds (average 0.005000, setup 0.00)

Prob1A: 1 iterations in 0.00 seconds (average 0.004000, setup 0.00)

goal = 1, inf = 0, maybe = 142663

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=142664, nc=180028, nnz=342658, k=4] [5.2 MB]
Building sparse matrix (transition rewards)... [n=142664, nc=180028, nnz=414, k=4] [1.2 MB]
Creating vector for state rewards... [1.1 MB]
Creating vector for inf... [1.1 MB]
Allocating iteration vectors... [2 x 1.1 MB]
TOTAL: [10.7 MB]

Starting iterations...

Iterative method: 89 iterations in 0.37 seconds (average 0.001933, setup 0.20)

Prob0E: 22 iterations in 0.06 seconds (average 0.002545, setup 0.00)

Prob1A: 3 iterations in 0.01 seconds (average 0.004000, setup 0.00)

yes = 210, no = 137071, maybe = 5383

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=142664, nc=9838, nnz=28717, k=2] [485.5 KB]
Creating vector for yes... [1.1 MB]
Allocating iteration vectors... [2 x 1.1 MB]
TOTAL: [3.7 MB]

Starting iterations...

Iterative method: 42 iterations in 0.06 seconds (average 0.000762, setup 0.03)

Value in the initial state: Infinity

Time for model checking: 0.534 seconds.

Result: Infinity (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"success"}min=? [ F "deadlock" ]/Pmax=? [ F "MissionSuccessful" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=2,Objy1=3,Objx2=5,Objy2=1

Prob0A: 4 iterations in 0.04 seconds (average 0.010000, setup 0.00)

Prob1E: 5 iterations in 0.04 seconds (average 0.007200, setup 0.00)

Warning: PRISM hasn't checked for zero-reward loops.
Your minimum rewards may be too low...

goal = 1, inf = 0, maybe = 142663

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=142664, nc=180028, nnz=342658, k=4] [5.2 MB]
Building sparse matrix (transition rewards)... [n=142664, nc=180028, nnz=414, k=4] [1.2 MB]
Creating vector for state rewards... [1.1 MB]
Creating vector for inf... [1.1 MB]
Allocating iteration vectors... [2 x 1.1 MB]
TOTAL: [10.7 MB]

Starting iterations...

Iterative method: 49 iterations in 0.28 seconds (average 0.001878, setup 0.19)

Prob0A: 60 iterations in 0.15 seconds (average 0.002467, setup 0.00)

Prob1E: 4 iterations in 0.02 seconds (average 0.005000, setup 0.00)

yes = 210, no = 135505, maybe = 6949

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=142664, nc=12723, nnz=37477, k=2] [590.9 KB]
Creating vector for yes... [1.1 MB]
Allocating iteration vectors... [2 x 1.1 MB]
TOTAL: [3.8 MB]

Starting iterations...

Iterative method: 92 iterations in 0.11 seconds (average 0.000739, setup 0.04)

Value in the initial state: 0.0

Time for model checking: 0.62 seconds.

Result: 0.0 (value in the initial state)

---------------------------------------------------------------------

Note: There were 16 warnings during computation.

