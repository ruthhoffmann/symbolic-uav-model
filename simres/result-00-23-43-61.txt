PRISM
=====

Version: 4.3
Date: Wed Aug 03 16:02:35 BST 2016
Hostname: eglinton
Memory limits: cudd=1g, java(heap)=981.5m
Command line: prism uav_sim_mdp.prism -s uav_mdp.props -const 'Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=4,Objy1=3,Objx2=6,Objy2=1,'

Parsing model file "uav_sim_mdp.prism"...

Parsing properties file "uav_mdp.props"...

18 properties:
(1) Pmax=? [ F "MissionSuccessful" ]
(2) Pmin=? [ F "MissionSuccessful" ]
(3) Pmax=? [ F "MissionTimeUp" ]
(4) Pmin=? [ F "MissionTimeUp" ]
(5) Pmax=? [ F "Crash" ]
(6) Pmin=? [ F "Crash" ]
(7) Pmax=? [ F "AllAreaSearched" ]
(8) Pmin=? [ F "AllAreaSearched" ]
(9) Pmax=? [ F "MissionTimeUp"|"AllAreaSearched"|"Crash" ]
(10) Pmin=? [ F "MissionTimeUp"|"AllAreaSearched"|"Crash" ]
(11) R{"drop"}max=? [ F "deadlock" ]
(12) R{"drop"}min=? [ F "deadlock" ]
(13) R{"charge"}max=? [ F "deadlock" ]
(14) R{"charge"}min=? [ F "deadlock" ]
(15) R{"time"}max=? [ F "deadlock" ]
(16) R{"time"}min=? [ F "deadlock" ]
(17) R{"success"}max=? [ F "deadlock" ]/Pmin=? [ F "MissionSuccessful" ]
(18) R{"success"}min=? [ F "deadlock" ]/Pmax=? [ F "MissionSuccessful" ]

Type:        MDP
Modules:     Obj Move Robot T 
Variables:   NoOfObjs posx posy s1 retx rety b t c 

---------------------------------------------------------------------

Model checking: Pmax=? [ F "MissionSuccessful" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=4,Objy1=3,Objx2=6,Objy2=1

Building model...
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=4,Objy1=3,Objx2=6,Objy2=1

Warning: Guard for command 7 of module "Move" is never satisfied.

Warning: Guard for command 8 of module "Move" is never satisfied.

Warning: Guard for command 9 of module "Move" is never satisfied.

Warning: Guard for command 7 of module "Robot" is never satisfied.

Warning: Update 2 of command 3 of module "Robot" doesn't do anything ("(s1'=13)", line 210, column 44)

Warning: Update 2 of command 27 of module "T" doesn't do anything ("(c'=1)", line 323, column 108)

Warning: Guard for command 28 of module "T" is never satisfied.

Warning: Update 2 of command 9 of module "T" doesn't do anything ("(c'=1)", line 300, column 112)

Warning: Guard for command 10 of module "T" is never satisfied.

Warning: Update 2 of command 51 of module "T" doesn't do anything ("(c'=1)", line 354, column 113)

Warning: Guard for command 52 of module "T" is never satisfied.

Computing reachable states...

Reachability (BFS): 90 iterations in 1.54 seconds (average 0.017067, setup 0.00)

Time for model construction: 3.351 seconds.

Warning: Deadlocks detected and fixed in 1 states

Type:        MDP
States:      294862 (1 initial)
Transitions: 789368
Choices:     391730

Transition matrix: 237965 nodes (97 terminal), 789368 minterms, vars: 34r/34c/18nd

Prob0A: 57 iterations in 0.50 seconds (average 0.008772, setup 0.00)

Prob1E: 4 iterations in 0.06 seconds (average 0.016000, setup 0.00)

yes = 1175, no = 251807, maybe = 41880

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=294862, nc=71379, nnz=200729, k=2] [2.6 MB]
Creating vector for yes... [2.2 MB]
Allocating iteration vectors... [2 x 2.2 MB]
TOTAL: [9.4 MB]

Starting iterations...

Iterative method: 102 iterations in 0.46 seconds (average 0.001922, setup 0.26)

Value in the initial state: 0.9796845215494113

Time for model checking: 1.061 seconds.

Result: 0.9796845215494113 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmin=? [ F "MissionSuccessful" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=4,Objy1=3,Objx2=6,Objy2=1

Prob0E: 59 iterations in 0.49 seconds (average 0.008271, setup 0.00)

Prob1A: 3 iterations in 0.04 seconds (average 0.014667, setup 0.00)

yes = 1175, no = 258492, maybe = 35195

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=294862, nc=60136, nnz=168286, k=2] [2.3 MB]
Creating vector for yes... [2.2 MB]
Allocating iteration vectors... [2 x 2.2 MB]
TOTAL: [9.0 MB]

Starting iterations...

Iterative method: 95 iterations in 0.35 seconds (average 0.001811, setup 0.18)

Value in the initial state: 0.737809003503424

Time for model checking: 0.902 seconds.

Result: 0.737809003503424 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmax=? [ F "MissionTimeUp" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=4,Objy1=3,Objx2=6,Objy2=1

Prob0A: 69 iterations in 0.73 seconds (average 0.010551, setup 0.00)

Prob1E: 4 iterations in 0.06 seconds (average 0.014000, setup 0.00)

yes = 412, no = 169503, maybe = 124947

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=294862, nc=208146, nnz=565904, k=4] [7.0 MB]
Creating vector for yes... [2.2 MB]
Allocating iteration vectors... [2 x 2.2 MB]
TOTAL: [13.7 MB]

Starting iterations...

Iterative method: 102 iterations in 0.63 seconds (average 0.002549, setup 0.37)

Value in the initial state: 7.183881097233301E-4

Time for model checking: 1.432 seconds.

Result: 7.183881097233301E-4 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmin=? [ F "MissionTimeUp" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=4,Objy1=3,Objx2=6,Objy2=1

Prob0E: 84 iterations in 1.19 seconds (average 0.014143, setup 0.00)

Prob1A: 3 iterations in 0.04 seconds (average 0.014667, setup 0.00)

yes = 412, no = 169840, maybe = 124610

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=294862, nc=207553, nnz=564665, k=4] [6.9 MB]
Creating vector for yes... [2.2 MB]
Allocating iteration vectors... [2 x 2.2 MB]
TOTAL: [13.7 MB]

Starting iterations...

Iterative method: 115 iterations in 0.62 seconds (average 0.002609, setup 0.32)

Value in the initial state: 2.5600422640161284E-6

Time for model checking: 1.899 seconds.

Result: 2.5600422640161284E-6 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmax=? [ F "Crash" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=4,Objy1=3,Objx2=6,Objy2=1

Prob0A: 3 iterations in 0.08 seconds (average 0.025333, setup 0.00)

Prob1E: 204 iterations in 2.82 seconds (average 0.013824, setup 0.00)

yes = 144355, no = 3358, maybe = 147149

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=294862, nc=244017, nnz=641655, k=4] [7.9 MB]
Creating vector for yes... [2.2 MB]
Allocating iteration vectors... [2 x 2.2 MB]
TOTAL: [14.6 MB]

Starting iterations...

Iterative method: 97 iterations in 0.66 seconds (average 0.002722, setup 0.40)

Value in the initial state: 0.03200859706933111

Time for model checking: 3.599 seconds.

Result: 0.03200859706933111 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmin=? [ F "Crash" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=4,Objy1=3,Objx2=6,Objy2=1

Prob0E: 4 iterations in 0.11 seconds (average 0.027000, setup 0.00)

Prob1A: 49 iterations in 0.75 seconds (average 0.015265, setup 0.00)

yes = 144355, no = 3358, maybe = 147149

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=294862, nc=244017, nnz=641655, k=4] [7.9 MB]
Creating vector for yes... [2.2 MB]
Allocating iteration vectors... [2 x 2.2 MB]
TOTAL: [14.6 MB]

Starting iterations...

Iterative method: 102 iterations in 0.64 seconds (average 0.002745, setup 0.36)

Value in the initial state: 0.018221638496477403

Time for model checking: 1.531 seconds.

Result: 0.018221638496477403 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmax=? [ F "AllAreaSearched" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=4,Objy1=3,Objx2=6,Objy2=1

Prob0A: 58 iterations in 0.38 seconds (average 0.006483, setup 0.00)

Prob1E: 4 iterations in 0.05 seconds (average 0.012000, setup 0.00)

yes = 1770, no = 231891, maybe = 61201

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=294862, nc=98938, nnz=265349, k=2] [3.4 MB]
Creating vector for yes... [2.2 MB]
Allocating iteration vectors... [2 x 2.2 MB]
TOTAL: [10.2 MB]

Starting iterations...

Iterative method: 88 iterations in 0.38 seconds (average 0.002045, setup 0.20)

Value in the initial state: 0.23283117866402353

Time for model checking: 0.834 seconds.

Result: 0.23283117866402353 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmin=? [ F "AllAreaSearched" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=4,Objy1=3,Objx2=6,Objy2=1

Prob0E: 64 iterations in 0.44 seconds (average 0.006938, setup 0.00)

Prob1A: 3 iterations in 0.04 seconds (average 0.014667, setup 0.00)

yes = 1770, no = 240399, maybe = 52693

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=294862, nc=85622, nnz=229167, k=2] [3.0 MB]
Creating vector for yes... [2.2 MB]
Allocating iteration vectors... [2 x 2.2 MB]
TOTAL: [9.7 MB]

Starting iterations...

Iterative method: 97 iterations in 0.37 seconds (average 0.002021, setup 0.17)

Value in the initial state: 0.0017809308715425153

Time for model checking: 0.868 seconds.

Result: 0.0017809308715425153 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmax=? [ F "MissionTimeUp"|"AllAreaSearched"|"Crash" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=4,Objy1=3,Objx2=6,Objy2=1

Prob0A: 3 iterations in 0.13 seconds (average 0.042667, setup 0.00)

Prob1E: 228 iterations in 3.45 seconds (average 0.015123, setup 0.00)

yes = 258491, no = 1176, maybe = 35195

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=294862, nc=60136, nnz=168286, k=2] [2.3 MB]
Creating vector for yes... [2.2 MB]
Allocating iteration vectors... [2 x 2.2 MB]
TOTAL: [9.0 MB]

Starting iterations...

Iterative method: 89 iterations in 0.37 seconds (average 0.001798, setup 0.21)

Value in the initial state: 0.26219066345593056

Time for model checking: 3.977 seconds.

Result: 0.26219066345593056 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmin=? [ F "MissionTimeUp"|"AllAreaSearched"|"Crash" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=4,Objy1=3,Objx2=6,Objy2=1

Prob0E: 4 iterations in 0.09 seconds (average 0.023000, setup 0.00)

Prob1A: 57 iterations in 0.80 seconds (average 0.014105, setup 0.00)

yes = 251806, no = 1176, maybe = 41880

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=294862, nc=71379, nnz=200729, k=2] [2.6 MB]
Creating vector for yes... [2.2 MB]
Allocating iteration vectors... [2 x 2.2 MB]
TOTAL: [9.4 MB]

Starting iterations...

Iterative method: 101 iterations in 0.40 seconds (average 0.001901, setup 0.21)

Value in the initial state: 0.020314738989352116

Time for model checking: 1.339 seconds.

Result: 0.020314738989352116 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"drop"}max=? [ F "deadlock" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=4,Objy1=3,Objx2=6,Objy2=1

Prob0E: 5 iterations in 0.10 seconds (average 0.020800, setup 0.00)

Prob1A: 1 iterations in 0.02 seconds (average 0.016000, setup 0.00)

goal = 1, inf = 0, maybe = 294861

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=294862, nc=391729, nnz=789367, k=4] [11.7 MB]
Building sparse matrix (transition rewards)... [n=294862, nc=391729, nnz=0, k=4] [2.6 MB]
Creating vector for state rewards... [2.2 MB]
Creating vector for inf... [2.2 MB]
Allocating iteration vectors... [2 x 2.2 MB]
TOTAL: [23.3 MB]

Starting iterations...

Iterative method: 98 iterations in 0.89 seconds (average 0.004122, setup 0.49)

Value in the initial state: 0.24060223029270714

Time for model checking: 1.023 seconds.

Result: 0.24060223029270714 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"drop"}min=? [ F "deadlock" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=4,Objy1=3,Objx2=6,Objy2=1

Prob0A: 4 iterations in 0.03 seconds (average 0.008000, setup 0.00)

Prob1E: 5 iterations in 0.06 seconds (average 0.012000, setup 0.00)

Warning: PRISM hasn't checked for zero-reward loops.
Your minimum rewards may be too low...

goal = 1, inf = 0, maybe = 294861

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=294862, nc=391729, nnz=789367, k=4] [11.7 MB]
Building sparse matrix (transition rewards)... [n=294862, nc=391729, nnz=0, k=4] [2.6 MB]
Creating vector for state rewards... [2.2 MB]
Creating vector for inf... [2.2 MB]
Allocating iteration vectors... [2 x 2.2 MB]
TOTAL: [23.3 MB]

Starting iterations...

Iterative method: 88 iterations in 0.74 seconds (average 0.004136, setup 0.38)

Value in the initial state: 0.21242439920120276

Time for model checking: 0.851 seconds.

Result: 0.21242439920120276 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"charge"}max=? [ F "deadlock" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=4,Objy1=3,Objx2=6,Objy2=1

Prob0E: 5 iterations in 0.04 seconds (average 0.007200, setup 0.00)

Prob1A: 1 iterations in 0.01 seconds (average 0.008000, setup 0.00)

goal = 1, inf = 0, maybe = 294861

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=294862, nc=391729, nnz=789367, k=4] [11.7 MB]
Building sparse matrix (transition rewards)... [n=294862, nc=391729, nnz=9508, k=4] [2.7 MB]
Creating vector for state rewards... [2.2 MB]
Creating vector for inf... [2.2 MB]
Allocating iteration vectors... [2 x 2.2 MB]
TOTAL: [23.4 MB]

Starting iterations...

Iterative method: 96 iterations in 0.95 seconds (average 0.005875, setup 0.38)

Value in the initial state: 0.3100896103980578

Time for model checking: 1.008 seconds.

Result: 0.3100896103980578 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"charge"}min=? [ F "deadlock" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=4,Objy1=3,Objx2=6,Objy2=1

Prob0A: 4 iterations in 0.03 seconds (average 0.007000, setup 0.00)

Prob1E: 5 iterations in 0.05 seconds (average 0.009600, setup 0.00)

Warning: PRISM hasn't checked for zero-reward loops.
Your minimum rewards may be too low...

goal = 1, inf = 0, maybe = 294861

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=294862, nc=391729, nnz=789367, k=4] [11.7 MB]
Building sparse matrix (transition rewards)... [n=294862, nc=391729, nnz=9508, k=4] [2.7 MB]
Creating vector for state rewards... [2.2 MB]
Creating vector for inf... [2.2 MB]
Allocating iteration vectors... [2 x 2.2 MB]
TOTAL: [23.4 MB]

Starting iterations...

Iterative method: 102 iterations in 0.94 seconds (average 0.004431, setup 0.49)

Value in the initial state: 0.057536085498246584

Time for model checking: 1.039 seconds.

Result: 0.057536085498246584 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"time"}max=? [ F "deadlock" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=4,Objy1=3,Objx2=6,Objy2=1

Prob0E: 5 iterations in 0.03 seconds (average 0.006400, setup 0.00)

Prob1A: 1 iterations in 0.00 seconds (average 0.004000, setup 0.00)

goal = 1, inf = 0, maybe = 294861

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=294862, nc=391729, nnz=789367, k=4] [11.7 MB]
Building sparse matrix (transition rewards)... [n=294862, nc=391729, nnz=147711, k=4] [4.3 MB]
Creating vector for state rewards... [2.2 MB]
Creating vector for inf... [2.2 MB]
Allocating iteration vectors... [2 x 2.2 MB]
TOTAL: [25.0 MB]

Starting iterations...

Iterative method: 99 iterations in 0.80 seconds (average 0.003960, setup 0.41)

Value in the initial state: 176.5710938796844

Time for model checking: 0.861 seconds.

Result: 176.5710938796844 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"time"}min=? [ F "deadlock" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=4,Objy1=3,Objx2=6,Objy2=1

Prob0A: 4 iterations in 0.02 seconds (average 0.006000, setup 0.00)

Prob1E: 5 iterations in 0.04 seconds (average 0.008000, setup 0.00)

Warning: PRISM hasn't checked for zero-reward loops.
Your minimum rewards may be too low...

goal = 1, inf = 0, maybe = 294861

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=294862, nc=391729, nnz=789367, k=4] [11.7 MB]
Building sparse matrix (transition rewards)... [n=294862, nc=391729, nnz=147711, k=4] [4.3 MB]
Creating vector for state rewards... [2.2 MB]
Creating vector for inf... [2.2 MB]
Allocating iteration vectors... [2 x 2.2 MB]
TOTAL: [25.0 MB]

Starting iterations...

Iterative method: 108 iterations in 0.84 seconds (average 0.004074, setup 0.40)

Value in the initial state: 100.94899799084627

Time for model checking: 0.909 seconds.

Result: 100.94899799084627 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"success"}max=? [ F "deadlock" ]/Pmin=? [ F "MissionSuccessful" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=4,Objy1=3,Objx2=6,Objy2=1

Prob0E: 5 iterations in 0.03 seconds (average 0.005600, setup 0.00)

Prob1A: 1 iterations in 0.00 seconds (average 0.004000, setup 0.00)

goal = 1, inf = 0, maybe = 294861

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=294862, nc=391729, nnz=789367, k=4] [11.7 MB]
Building sparse matrix (transition rewards)... [n=294862, nc=391729, nnz=2312, k=4] [2.6 MB]
Creating vector for state rewards... [2.2 MB]
Creating vector for inf... [2.2 MB]
Allocating iteration vectors... [2 x 2.2 MB]
TOTAL: [23.3 MB]

Starting iterations...

Iterative method: 102 iterations in 0.74 seconds (average 0.003804, setup 0.36)

Prob0E: 59 iterations in 0.26 seconds (average 0.004475, setup 0.00)

Prob1A: 3 iterations in 0.04 seconds (average 0.014667, setup 0.00)

yes = 1175, no = 258492, maybe = 35195

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=294862, nc=60136, nnz=168286, k=2] [2.3 MB]
Creating vector for yes... [2.2 MB]
Allocating iteration vectors... [2 x 2.2 MB]
TOTAL: [9.0 MB]

Starting iterations...

Iterative method: 95 iterations in 0.30 seconds (average 0.001684, setup 0.14)

Value in the initial state: 234.54941636724791

Time for model checking: 1.421 seconds.

Result: 234.54941636724791 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"success"}min=? [ F "deadlock" ]/Pmax=? [ F "MissionSuccessful" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=4,Objy1=3,Objx2=6,Objy2=1

Prob0A: 4 iterations in 0.08 seconds (average 0.021000, setup 0.00)

Prob1E: 5 iterations in 0.04 seconds (average 0.008800, setup 0.00)

Warning: PRISM hasn't checked for zero-reward loops.
Your minimum rewards may be too low...

goal = 1, inf = 0, maybe = 294861

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=294862, nc=391729, nnz=789367, k=4] [11.7 MB]
Building sparse matrix (transition rewards)... [n=294862, nc=391729, nnz=2312, k=4] [2.6 MB]
Creating vector for state rewards... [2.2 MB]
Creating vector for inf... [2.2 MB]
Allocating iteration vectors... [2 x 2.2 MB]
TOTAL: [23.3 MB]

Starting iterations...

Iterative method: 100 iterations in 0.78 seconds (average 0.003800, setup 0.40)

Prob0A: 57 iterations in 0.27 seconds (average 0.004772, setup 0.00)

Prob1E: 4 iterations in 0.04 seconds (average 0.009000, setup 0.00)

yes = 1175, no = 251807, maybe = 41880

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=294862, nc=71379, nnz=200729, k=2] [2.6 MB]
Creating vector for yes... [2.2 MB]
Allocating iteration vectors... [2 x 2.2 MB]
TOTAL: [9.4 MB]

Starting iterations...

Iterative method: 102 iterations in 0.34 seconds (average 0.001725, setup 0.16)

Value in the initial state: 80.24525988646835

Time for model checking: 1.58 seconds.

Result: 80.24525988646835 (value in the initial state)

---------------------------------------------------------------------

Note: There were 16 warnings during computation.

