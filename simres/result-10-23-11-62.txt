PRISM
=====

Version: 4.3
Date: Wed Aug 03 16:09:17 BST 2016
Hostname: eglinton
Memory limits: cudd=1g, java(heap)=981.5m
Command line: prism uav_sim_mdp.prism -s uav_mdp.props -const 'Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=1,Objy1=1,Objx2=6,Objy2=2,'

Parsing model file "uav_sim_mdp.prism"...

Parsing properties file "uav_mdp.props"...

18 properties:
(1) Pmax=? [ F "MissionSuccessful" ]
(2) Pmin=? [ F "MissionSuccessful" ]
(3) Pmax=? [ F "MissionTimeUp" ]
(4) Pmin=? [ F "MissionTimeUp" ]
(5) Pmax=? [ F "Crash" ]
(6) Pmin=? [ F "Crash" ]
(7) Pmax=? [ F "AllAreaSearched" ]
(8) Pmin=? [ F "AllAreaSearched" ]
(9) Pmax=? [ F "MissionTimeUp"|"AllAreaSearched"|"Crash" ]
(10) Pmin=? [ F "MissionTimeUp"|"AllAreaSearched"|"Crash" ]
(11) R{"drop"}max=? [ F "deadlock" ]
(12) R{"drop"}min=? [ F "deadlock" ]
(13) R{"charge"}max=? [ F "deadlock" ]
(14) R{"charge"}min=? [ F "deadlock" ]
(15) R{"time"}max=? [ F "deadlock" ]
(16) R{"time"}min=? [ F "deadlock" ]
(17) R{"success"}max=? [ F "deadlock" ]/Pmin=? [ F "MissionSuccessful" ]
(18) R{"success"}min=? [ F "deadlock" ]/Pmax=? [ F "MissionSuccessful" ]

Type:        MDP
Modules:     Obj Move Robot T 
Variables:   NoOfObjs posx posy s1 retx rety b t c 

---------------------------------------------------------------------

Model checking: Pmax=? [ F "MissionSuccessful" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=1,Objy1=1,Objx2=6,Objy2=2

Building model...
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=1,Objy1=1,Objx2=6,Objy2=2

Warning: Guard for command 7 of module "Move" is never satisfied.

Warning: Guard for command 8 of module "Move" is never satisfied.

Warning: Guard for command 9 of module "Move" is never satisfied.

Warning: Guard for command 7 of module "Robot" is never satisfied.

Warning: Update 2 of command 3 of module "Robot" doesn't do anything ("(s1'=13)", line 210, column 44)

Warning: Update 2 of command 27 of module "T" doesn't do anything ("(c'=1)", line 323, column 108)

Warning: Guard for command 28 of module "T" is never satisfied.

Warning: Update 2 of command 9 of module "T" doesn't do anything ("(c'=1)", line 300, column 112)

Warning: Guard for command 10 of module "T" is never satisfied.

Warning: Update 2 of command 51 of module "T" doesn't do anything ("(c'=1)", line 354, column 113)

Warning: Guard for command 52 of module "T" is never satisfied.

Computing reachable states...

Reachability (BFS): 88 iterations in 1.34 seconds (average 0.015182, setup 0.00)

Time for model construction: 3.756 seconds.

Warning: Deadlocks detected and fixed in 1 states

Type:        MDP
States:      304549 (1 initial)
Transitions: 846709
Choices:     411401

Transition matrix: 222777 nodes (93 terminal), 846709 minterms, vars: 34r/34c/18nd

Prob0A: 51 iterations in 0.51 seconds (average 0.009961, setup 0.00)

Prob1E: 4 iterations in 0.07 seconds (average 0.018000, setup 0.00)

yes = 1198, no = 253030, maybe = 50321

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=304549, nc=88942, nnz=256812, k=2] [3.3 MB]
Creating vector for yes... [2.3 MB]
Allocating iteration vectors... [2 x 2.3 MB]
TOTAL: [10.3 MB]

Starting iterations...

Iterative method: 102 iterations in 0.52 seconds (average 0.002157, setup 0.30)

Value in the initial state: 0.9805928681513516

Time for model checking: 1.265 seconds.

Result: 0.9805928681513516 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmin=? [ F "MissionSuccessful" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=1,Objy1=1,Objx2=6,Objy2=2

Prob0E: 51 iterations in 0.52 seconds (average 0.010196, setup 0.00)

Prob1A: 3 iterations in 0.05 seconds (average 0.016000, setup 0.00)

yes = 1198, no = 259667, maybe = 43684

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=304549, nc=76927, nnz=222246, k=2] [2.9 MB]
Creating vector for yes... [2.3 MB]
Allocating iteration vectors... [2 x 2.3 MB]
TOTAL: [9.9 MB]

Starting iterations...

Iterative method: 92 iterations in 0.40 seconds (average 0.002130, setup 0.20)

Value in the initial state: 0.8008587485457631

Time for model checking: 1.18 seconds.

Result: 0.8008587485457631 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmax=? [ F "MissionTimeUp" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=1,Objy1=1,Objx2=6,Objy2=2

Prob0A: 75 iterations in 0.77 seconds (average 0.010293, setup 0.00)

Prob1E: 4 iterations in 0.05 seconds (average 0.012000, setup 0.00)

yes = 424, no = 177213, maybe = 126912

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=304549, nc=220582, nnz=613958, k=4] [7.5 MB]
Creating vector for yes... [2.3 MB]
Allocating iteration vectors... [2 x 2.3 MB]
TOTAL: [14.5 MB]

Starting iterations...

Iterative method: 99 iterations in 0.68 seconds (average 0.002869, setup 0.40)

Value in the initial state: 7.661910298398562E-4

Time for model checking: 1.588 seconds.

Result: 7.661910298398562E-4 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmin=? [ F "MissionTimeUp" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=1,Objy1=1,Objx2=6,Objy2=2

Prob0E: 88 iterations in 1.07 seconds (average 0.012136, setup 0.00)

Prob1A: 3 iterations in 0.06 seconds (average 0.018667, setup 0.00)

yes = 424, no = 177593, maybe = 126532

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=304549, nc=219898, nnz=612666, k=4] [7.5 MB]
Creating vector for yes... [2.3 MB]
Allocating iteration vectors... [2 x 2.3 MB]
TOTAL: [14.5 MB]

Starting iterations...

Iterative method: 114 iterations in 0.68 seconds (average 0.002842, setup 0.36)

Value in the initial state: 1.7292740065102277E-6

Time for model checking: 1.963 seconds.

Result: 1.7292740065102277E-6 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmax=? [ F "Crash" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=1,Objy1=1,Objx2=6,Objy2=2

Prob0A: 3 iterations in 0.10 seconds (average 0.032000, setup 0.00)

Prob1E: 196 iterations in 3.43 seconds (average 0.017490, setup 0.00)

yes = 148905, no = 3285, maybe = 152359

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=304549, nc=259211, nnz=694519, k=4] [8.5 MB]
Creating vector for yes... [2.3 MB]
Allocating iteration vectors... [2 x 2.3 MB]
TOTAL: [15.5 MB]

Starting iterations...

Iterative method: 94 iterations in 0.72 seconds (average 0.002979, setup 0.44)

Value in the initial state: 0.03188530754730941

Time for model checking: 4.606 seconds.

Result: 0.03188530754730941 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmin=? [ F "Crash" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=1,Objy1=1,Objx2=6,Objy2=2

Prob0E: 3 iterations in 0.09 seconds (average 0.029333, setup 0.00)

Prob1A: 51 iterations in 0.69 seconds (average 0.013569, setup 0.00)

yes = 148905, no = 3285, maybe = 152359

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=304549, nc=259211, nnz=694519, k=4] [8.5 MB]
Creating vector for yes... [2.3 MB]
Allocating iteration vectors... [2 x 2.3 MB]
TOTAL: [15.5 MB]

Starting iterations...

Iterative method: 92 iterations in 0.61 seconds (average 0.003000, setup 0.33)

Value in the initial state: 0.017271176303051305

Time for model checking: 1.641 seconds.

Result: 0.017271176303051305 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmax=? [ F "AllAreaSearched" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=1,Objy1=1,Objx2=6,Objy2=2

Prob0A: 53 iterations in 0.57 seconds (average 0.010717, setup 0.00)

Prob1E: 4 iterations in 0.06 seconds (average 0.015000, setup 0.00)

yes = 1662, no = 229610, maybe = 73277

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=304549, nc=120788, nnz=336159, k=2] [4.3 MB]
Creating vector for yes... [2.3 MB]
Allocating iteration vectors... [2 x 2.3 MB]
TOTAL: [11.2 MB]

Starting iterations...

Iterative method: 92 iterations in 0.50 seconds (average 0.002565, setup 0.26)

Value in the initial state: 0.17179001885339737

Time for model checking: 1.149 seconds.

Result: 0.17179001885339737 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmin=? [ F "AllAreaSearched" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=1,Objy1=1,Objx2=6,Objy2=2

Prob0E: 64 iterations in 0.60 seconds (average 0.009437, setup 0.00)

Prob1A: 3 iterations in 0.05 seconds (average 0.017333, setup 0.00)

yes = 1662, no = 238918, maybe = 63969

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=304549, nc=104213, nnz=288034, k=2] [3.7 MB]
Creating vector for yes... [2.3 MB]
Allocating iteration vectors... [2 x 2.3 MB]
TOTAL: [10.7 MB]

Starting iterations...

Iterative method: 98 iterations in 0.44 seconds (average 0.002367, setup 0.21)

Value in the initial state: 0.0017499296750016547

Time for model checking: 1.344 seconds.

Result: 0.0017499296750016547 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmax=? [ F "MissionTimeUp"|"AllAreaSearched"|"Crash" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=1,Objy1=1,Objx2=6,Objy2=2

Prob0A: 3 iterations in 0.10 seconds (average 0.033333, setup 0.00)

Prob1E: 200 iterations in 2.92 seconds (average 0.014600, setup 0.00)

yes = 259666, no = 1199, maybe = 43684

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=304549, nc=76927, nnz=222246, k=2] [2.9 MB]
Creating vector for yes... [2.3 MB]
Allocating iteration vectors... [2 x 2.3 MB]
TOTAL: [9.9 MB]

Starting iterations...

Iterative method: 87 iterations in 0.43 seconds (average 0.002115, setup 0.24)

Value in the initial state: 0.19914095283364674

Time for model checking: 3.76 seconds.

Result: 0.19914095283364674 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmin=? [ F "MissionTimeUp"|"AllAreaSearched"|"Crash" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=1,Objy1=1,Objx2=6,Objy2=2

Prob0E: 3 iterations in 0.06 seconds (average 0.021333, setup 0.00)

Prob1A: 51 iterations in 0.57 seconds (average 0.011216, setup 0.00)

yes = 253029, no = 1199, maybe = 50321

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=304549, nc=88942, nnz=256812, k=2] [3.3 MB]
Creating vector for yes... [2.3 MB]
Allocating iteration vectors... [2 x 2.3 MB]
TOTAL: [10.3 MB]

Starting iterations...

Iterative method: 98 iterations in 0.44 seconds (average 0.002204, setup 0.23)

Value in the initial state: 0.019406614082655316

Time for model checking: 1.15 seconds.

Result: 0.019406614082655316 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"drop"}max=? [ F "deadlock" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=1,Objy1=1,Objx2=6,Objy2=2

Prob0E: 4 iterations in 0.09 seconds (average 0.023000, setup 0.00)

Prob1A: 1 iterations in 0.01 seconds (average 0.008000, setup 0.00)

goal = 1, inf = 0, maybe = 304548

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=304549, nc=411400, nnz=846708, k=4] [12.4 MB]
Building sparse matrix (transition rewards)... [n=304549, nc=411400, nnz=0, k=4] [2.7 MB]
Creating vector for state rewards... [2.3 MB]
Creating vector for inf... [2.3 MB]
Allocating iteration vectors... [2 x 2.3 MB]
TOTAL: [24.4 MB]

Starting iterations...

Iterative method: 96 iterations in 0.98 seconds (average 0.004542, setup 0.54)

Value in the initial state: 0.2539353152128752

Time for model checking: 1.093 seconds.

Result: 0.2539353152128752 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"drop"}min=? [ F "deadlock" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=1,Objy1=1,Objx2=6,Objy2=2

Prob0A: 4 iterations in 0.04 seconds (average 0.010000, setup 0.00)

Prob1E: 5 iterations in 0.05 seconds (average 0.010400, setup 0.00)

Warning: PRISM hasn't checked for zero-reward loops.
Your minimum rewards may be too low...

goal = 1, inf = 0, maybe = 304548

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=304549, nc=411400, nnz=846708, k=4] [12.4 MB]
Building sparse matrix (transition rewards)... [n=304549, nc=411400, nnz=0, k=4] [2.7 MB]
Creating vector for state rewards... [2.3 MB]
Creating vector for inf... [2.3 MB]
Allocating iteration vectors... [2 x 2.3 MB]
TOTAL: [24.4 MB]

Starting iterations...

Iterative method: 86 iterations in 0.92 seconds (average 0.004512, setup 0.54)

Value in the initial state: 0.2056050604585834

Time for model checking: 1.041 seconds.

Result: 0.2056050604585834 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"charge"}max=? [ F "deadlock" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=1,Objy1=1,Objx2=6,Objy2=2

Prob0E: 4 iterations in 0.05 seconds (average 0.013000, setup 0.00)

Prob1A: 1 iterations in 0.00 seconds (average 0.004000, setup 0.00)

goal = 1, inf = 0, maybe = 304548

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=304549, nc=411400, nnz=846708, k=4] [12.4 MB]
Building sparse matrix (transition rewards)... [n=304549, nc=411400, nnz=10272, k=4] [2.8 MB]
Creating vector for state rewards... [2.3 MB]
Creating vector for inf... [2.3 MB]
Allocating iteration vectors... [2 x 2.3 MB]
TOTAL: [24.6 MB]

Starting iterations...

Iterative method: 91 iterations in 0.82 seconds (average 0.004484, setup 0.42)

Value in the initial state: 0.31155251247261107

Time for model checking: 0.902 seconds.

Result: 0.31155251247261107 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"charge"}min=? [ F "deadlock" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=1,Objy1=1,Objx2=6,Objy2=2

Prob0A: 4 iterations in 0.03 seconds (average 0.008000, setup 0.00)

Prob1E: 5 iterations in 0.05 seconds (average 0.009600, setup 0.00)

Warning: PRISM hasn't checked for zero-reward loops.
Your minimum rewards may be too low...

goal = 1, inf = 0, maybe = 304548

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=304549, nc=411400, nnz=846708, k=4] [12.4 MB]
Building sparse matrix (transition rewards)... [n=304549, nc=411400, nnz=10272, k=4] [2.8 MB]
Creating vector for state rewards... [2.3 MB]
Creating vector for inf... [2.3 MB]
Allocating iteration vectors... [2 x 2.3 MB]
TOTAL: [24.6 MB]

Starting iterations...

Iterative method: 103 iterations in 0.89 seconds (average 0.004583, setup 0.42)

Value in the initial state: 0.06055293836507729

Time for model checking: 0.981 seconds.

Result: 0.06055293836507729 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"time"}max=? [ F "deadlock" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=1,Objy1=1,Objx2=6,Objy2=2

Prob0E: 4 iterations in 0.03 seconds (average 0.007000, setup 0.00)

Prob1A: 1 iterations in 0.01 seconds (average 0.008000, setup 0.00)

goal = 1, inf = 0, maybe = 304548

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=304549, nc=411400, nnz=846708, k=4] [12.4 MB]
Building sparse matrix (transition rewards)... [n=304549, nc=411400, nnz=152188, k=4] [4.5 MB]
Creating vector for state rewards... [2.3 MB]
Creating vector for inf... [2.3 MB]
Allocating iteration vectors... [2 x 2.3 MB]
TOTAL: [26.2 MB]

Starting iterations...

Iterative method: 97 iterations in 0.90 seconds (average 0.004619, setup 0.46)

Value in the initial state: 175.85968362928145

Time for model checking: 0.96 seconds.

Result: 175.85968362928145 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"time"}min=? [ F "deadlock" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=1,Objy1=1,Objx2=6,Objy2=2

Prob0A: 4 iterations in 0.02 seconds (average 0.006000, setup 0.00)

Prob1E: 5 iterations in 0.04 seconds (average 0.008800, setup 0.00)

Warning: PRISM hasn't checked for zero-reward loops.
Your minimum rewards may be too low...

goal = 1, inf = 0, maybe = 304548

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=304549, nc=411400, nnz=846708, k=4] [12.4 MB]
Building sparse matrix (transition rewards)... [n=304549, nc=411400, nnz=152188, k=4] [4.5 MB]
Creating vector for state rewards... [2.3 MB]
Creating vector for inf... [2.3 MB]
Allocating iteration vectors... [2 x 2.3 MB]
TOTAL: [26.2 MB]

Starting iterations...

Iterative method: 106 iterations in 0.95 seconds (average 0.004679, setup 0.46)

Value in the initial state: 95.70622022315469

Time for model checking: 1.03 seconds.

Result: 95.70622022315469 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"success"}max=? [ F "deadlock" ]/Pmin=? [ F "MissionSuccessful" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=1,Objy1=1,Objx2=6,Objy2=2

Prob0E: 4 iterations in 0.02 seconds (average 0.006000, setup 0.00)

Prob1A: 1 iterations in 0.00 seconds (average 0.004000, setup 0.00)

goal = 1, inf = 0, maybe = 304548

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=304549, nc=411400, nnz=846708, k=4] [12.4 MB]
Building sparse matrix (transition rewards)... [n=304549, nc=411400, nnz=2360, k=4] [2.8 MB]
Creating vector for state rewards... [2.3 MB]
Creating vector for inf... [2.3 MB]
Allocating iteration vectors... [2 x 2.3 MB]
TOTAL: [24.5 MB]

Starting iterations...

Iterative method: 98 iterations in 0.84 seconds (average 0.004449, setup 0.41)

Prob0E: 51 iterations in 0.48 seconds (average 0.009333, setup 0.00)

Prob1A: 3 iterations in 0.06 seconds (average 0.020000, setup 0.00)

yes = 1198, no = 259667, maybe = 43684

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=304549, nc=76927, nnz=222246, k=2] [2.9 MB]
Creating vector for yes... [2.3 MB]
Allocating iteration vectors... [2 x 2.3 MB]
TOTAL: [9.9 MB]

Starting iterations...

Iterative method: 92 iterations in 0.38 seconds (average 0.002000, setup 0.20)

Value in the initial state: 215.0730880430062

Time for model checking: 1.844 seconds.

Result: 215.0730880430062 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"success"}min=? [ F "deadlock" ]/Pmax=? [ F "MissionSuccessful" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=1,Objy1=1,Objx2=6,Objy2=2

Prob0A: 4 iterations in 0.08 seconds (average 0.021000, setup 0.00)

Prob1E: 5 iterations in 0.05 seconds (average 0.010400, setup 0.00)

Warning: PRISM hasn't checked for zero-reward loops.
Your minimum rewards may be too low...

goal = 1, inf = 0, maybe = 304548

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=304549, nc=411400, nnz=846708, k=4] [12.4 MB]
Building sparse matrix (transition rewards)... [n=304549, nc=411400, nnz=2360, k=4] [2.8 MB]
Creating vector for state rewards... [2.3 MB]
Creating vector for inf... [2.3 MB]
Allocating iteration vectors... [2 x 2.3 MB]
TOTAL: [24.5 MB]

Starting iterations...

Iterative method: 99 iterations in 0.89 seconds (average 0.004323, setup 0.46)

Prob0A: 51 iterations in 0.42 seconds (average 0.008157, setup 0.00)

Prob1E: 4 iterations in 0.05 seconds (average 0.013000, setup 0.00)

yes = 1198, no = 253030, maybe = 50321

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=304549, nc=88942, nnz=256812, k=2] [3.3 MB]
Creating vector for yes... [2.3 MB]
Allocating iteration vectors... [2 x 2.3 MB]
TOTAL: [10.3 MB]

Starting iterations...

Iterative method: 102 iterations in 0.36 seconds (average 0.001843, setup 0.17)

Value in the initial state: 78.53170269598272

Time for model checking: 1.884 seconds.

Result: 78.53170269598272 (value in the initial state)

---------------------------------------------------------------------

Note: There were 16 warnings during computation.

