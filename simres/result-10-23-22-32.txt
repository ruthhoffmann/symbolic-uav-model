PRISM
=====

Version: 4.3
Date: Wed Aug 03 16:12:14 BST 2016
Hostname: eglinton
Memory limits: cudd=1g, java(heap)=981.5m
Command line: prism uav_sim_mdp.prism -s uav_mdp.props -const 'Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=2,Objy1=2,Objx2=3,Objy2=2,'

Parsing model file "uav_sim_mdp.prism"...

Parsing properties file "uav_mdp.props"...

18 properties:
(1) Pmax=? [ F "MissionSuccessful" ]
(2) Pmin=? [ F "MissionSuccessful" ]
(3) Pmax=? [ F "MissionTimeUp" ]
(4) Pmin=? [ F "MissionTimeUp" ]
(5) Pmax=? [ F "Crash" ]
(6) Pmin=? [ F "Crash" ]
(7) Pmax=? [ F "AllAreaSearched" ]
(8) Pmin=? [ F "AllAreaSearched" ]
(9) Pmax=? [ F "MissionTimeUp"|"AllAreaSearched"|"Crash" ]
(10) Pmin=? [ F "MissionTimeUp"|"AllAreaSearched"|"Crash" ]
(11) R{"drop"}max=? [ F "deadlock" ]
(12) R{"drop"}min=? [ F "deadlock" ]
(13) R{"charge"}max=? [ F "deadlock" ]
(14) R{"charge"}min=? [ F "deadlock" ]
(15) R{"time"}max=? [ F "deadlock" ]
(16) R{"time"}min=? [ F "deadlock" ]
(17) R{"success"}max=? [ F "deadlock" ]/Pmin=? [ F "MissionSuccessful" ]
(18) R{"success"}min=? [ F "deadlock" ]/Pmax=? [ F "MissionSuccessful" ]

Type:        MDP
Modules:     Obj Move Robot T 
Variables:   NoOfObjs posx posy s1 retx rety b t c 

---------------------------------------------------------------------

Model checking: Pmax=? [ F "MissionSuccessful" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=2,Objy1=2,Objx2=3,Objy2=2

Building model...
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=2,Objy1=2,Objx2=3,Objy2=2

Warning: Guard for command 7 of module "Move" is never satisfied.

Warning: Guard for command 8 of module "Move" is never satisfied.

Warning: Guard for command 9 of module "Move" is never satisfied.

Warning: Guard for command 7 of module "Robot" is never satisfied.

Warning: Update 2 of command 3 of module "Robot" doesn't do anything ("(s1'=13)", line 210, column 44)

Warning: Update 2 of command 27 of module "T" doesn't do anything ("(c'=1)", line 323, column 108)

Warning: Guard for command 28 of module "T" is never satisfied.

Warning: Update 2 of command 9 of module "T" doesn't do anything ("(c'=1)", line 300, column 112)

Warning: Guard for command 10 of module "T" is never satisfied.

Warning: Update 2 of command 51 of module "T" doesn't do anything ("(c'=1)", line 354, column 113)

Warning: Guard for command 52 of module "T" is never satisfied.

Computing reachable states...

Reachability (BFS): 87 iterations in 0.70 seconds (average 0.008092, setup 0.00)

Time for model construction: 2.598 seconds.

Warning: Deadlocks detected and fixed in 1 states

Type:        MDP
States:      159664 (1 initial)
Transitions: 417716
Choices:     212728

Transition matrix: 169241 nodes (87 terminal), 417716 minterms, vars: 34r/34c/18nd

Prob0A: 44 iterations in 0.24 seconds (average 0.005545, setup 0.00)

Prob1E: 4 iterations in 0.04 seconds (average 0.011000, setup 0.00)

yes = 1062, no = 131243, maybe = 27359

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=159664, nc=50011, nnz=139194, k=2] [1.8 MB]
Creating vector for yes... [1.2 MB]
Allocating iteration vectors... [2 x 1.2 MB]
TOTAL: [5.4 MB]

Starting iterations...

Iterative method: 84 iterations in 0.27 seconds (average 0.001095, setup 0.18)

Value in the initial state: 0.9844812846390238

Time for model checking: 0.581 seconds.

Result: 0.9844812846390238 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmin=? [ F "MissionSuccessful" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=2,Objy1=2,Objx2=3,Objy2=2

Prob0E: 44 iterations in 0.25 seconds (average 0.005727, setup 0.00)

Prob1A: 3 iterations in 0.03 seconds (average 0.010667, setup 0.00)

yes = 1062, no = 134599, maybe = 24003

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=159664, nc=43787, nnz=121300, k=2] [1.6 MB]
Creating vector for yes... [1.2 MB]
Allocating iteration vectors... [2 x 1.2 MB]
TOTAL: [5.2 MB]

Starting iterations...

Iterative method: 83 iterations in 0.22 seconds (average 0.001060, setup 0.13)

Value in the initial state: 0.881098768525211

Time for model checking: 0.52 seconds.

Result: 0.881098768525211 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmax=? [ F "MissionTimeUp" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=2,Objy1=2,Objx2=3,Objy2=2

Prob0A: 77 iterations in 0.49 seconds (average 0.006338, setup 0.00)

Prob1E: 4 iterations in 0.03 seconds (average 0.007000, setup 0.00)

yes = 232, no = 106861, maybe = 52571

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=159664, nc=94161, nnz=258827, k=4] [3.2 MB]
Creating vector for yes... [1.2 MB]
Allocating iteration vectors... [2 x 1.2 MB]
TOTAL: [6.9 MB]

Starting iterations...

Iterative method: 94 iterations in 0.32 seconds (average 0.001277, setup 0.20)

Value in the initial state: 1.4494064568321261E-5

Time for model checking: 0.82 seconds.

Result: 1.4494064568321261E-5 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmin=? [ F "MissionTimeUp" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=2,Objy1=2,Objx2=3,Objy2=2

Prob0E: 91 iterations in 0.47 seconds (average 0.005143, setup 0.00)

Prob1A: 3 iterations in 0.03 seconds (average 0.010667, setup 0.00)

yes = 232, no = 107184, maybe = 52248

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=159664, nc=93598, nnz=257532, k=4] [3.2 MB]
Creating vector for yes... [1.2 MB]
Allocating iteration vectors... [2 x 1.2 MB]
TOTAL: [6.8 MB]

Starting iterations...

Iterative method: 111 iterations in 0.34 seconds (average 0.001333, setup 0.19)

Value in the initial state: 4.459147927123116E-9

Time for model checking: 0.852 seconds.

Result: 4.459147927123116E-9 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmax=? [ F "Crash" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=2,Objy1=2,Objx2=3,Objy2=2

Prob0A: 3 iterations in 0.06 seconds (average 0.018667, setup 0.00)

Prob1E: 168 iterations in 1.71 seconds (average 0.010190, setup 0.00)

yes = 78369, no = 2755, maybe = 78540

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=159664, nc=131604, nnz=336592, k=4] [4.1 MB]
Creating vector for yes... [1.2 MB]
Allocating iteration vectors... [2 x 1.2 MB]
TOTAL: [7.8 MB]

Starting iterations...

Iterative method: 86 iterations in 0.36 seconds (average 0.001488, setup 0.24)

Value in the initial state: 0.02163299792276274

Time for model checking: 2.145 seconds.

Result: 0.02163299792276274 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmin=? [ F "Crash" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=2,Objy1=2,Objx2=3,Objy2=2

Prob0E: 3 iterations in 0.05 seconds (average 0.017333, setup 0.00)

Prob1A: 44 iterations in 0.32 seconds (average 0.007364, setup 0.00)

yes = 78369, no = 2755, maybe = 78540

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=159664, nc=131604, nnz=336592, k=4] [4.1 MB]
Creating vector for yes... [1.2 MB]
Allocating iteration vectors... [2 x 1.2 MB]
TOTAL: [7.8 MB]

Starting iterations...

Iterative method: 77 iterations in 0.27 seconds (average 0.001558, setup 0.15)

Value in the initial state: 0.013465243171158324

Time for model checking: 0.653 seconds.

Result: 0.013465243171158324 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmax=? [ F "AllAreaSearched" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=2,Objy1=2,Objx2=3,Objy2=2

Prob0A: 53 iterations in 0.26 seconds (average 0.004906, setup 0.00)

Prob1E: 4 iterations in 0.03 seconds (average 0.007000, setup 0.00)

yes = 1460, no = 113134, maybe = 45070

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=159664, nc=71549, nnz=185920, k=2] [2.3 MB]
Creating vector for yes... [1.2 MB]
Allocating iteration vectors... [2 x 1.2 MB]
TOTAL: [6.0 MB]

Starting iterations...

Iterative method: 83 iterations in 0.25 seconds (average 0.001205, setup 0.15)

Value in the initial state: 0.09822323932476679

Time for model checking: 0.565 seconds.

Result: 0.09822323932476679 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmin=? [ F "AllAreaSearched" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=2,Objy1=2,Objx2=3,Objy2=2

Prob0E: 60 iterations in 0.33 seconds (average 0.005467, setup 0.00)

Prob1A: 3 iterations in 0.03 seconds (average 0.009333, setup 0.00)

yes = 1460, no = 118371, maybe = 39833

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=159664, nc=61837, nnz=157867, k=2] [2.0 MB]
Creating vector for yes... [1.2 MB]
Allocating iteration vectors... [2 x 1.2 MB]
TOTAL: [5.7 MB]

Starting iterations...

Iterative method: 92 iterations in 0.24 seconds (average 0.001217, setup 0.13)

Value in the initial state: 0.0018869433740139854

Time for model checking: 0.602 seconds.

Result: 0.0018869433740139854 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmax=? [ F "MissionTimeUp"|"AllAreaSearched"|"Crash" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=2,Objy1=2,Objx2=3,Objy2=2

Prob0A: 3 iterations in 0.05 seconds (average 0.017333, setup 0.00)

Prob1E: 172 iterations in 1.53 seconds (average 0.008884, setup 0.00)

yes = 134598, no = 1063, maybe = 24003

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=159664, nc=43787, nnz=121300, k=2] [1.6 MB]
Creating vector for yes... [1.2 MB]
Allocating iteration vectors... [2 x 1.2 MB]
TOTAL: [5.2 MB]

Starting iterations...

Iterative method: 76 iterations in 0.22 seconds (average 0.001053, setup 0.14)

Value in the initial state: 0.11890064867417308

Time for model checking: 1.82 seconds.

Result: 0.11890064867417308 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmin=? [ F "MissionTimeUp"|"AllAreaSearched"|"Crash" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=2,Objy1=2,Objx2=3,Objy2=2

Prob0E: 3 iterations in 0.02 seconds (average 0.008000, setup 0.00)

Prob1A: 44 iterations in 0.29 seconds (average 0.006636, setup 0.00)

yes = 131242, no = 1063, maybe = 27359

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=159664, nc=50011, nnz=139194, k=2] [1.8 MB]
Creating vector for yes... [1.2 MB]
Allocating iteration vectors... [2 x 1.2 MB]
TOTAL: [5.4 MB]

Starting iterations...

Iterative method: 79 iterations in 0.20 seconds (average 0.001114, setup 0.11)

Value in the initial state: 0.015517252456809207

Time for model checking: 0.522 seconds.

Result: 0.015517252456809207 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"drop"}max=? [ F "deadlock" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=2,Objy1=2,Objx2=3,Objy2=2

Prob0E: 4 iterations in 0.04 seconds (average 0.009000, setup 0.00)

Prob1A: 1 iterations in 0.00 seconds (average 0.004000, setup 0.00)

goal = 1, inf = 0, maybe = 159663

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=159664, nc=212727, nnz=417715, k=4] [6.2 MB]
Building sparse matrix (transition rewards)... [n=159664, nc=212727, nnz=0, k=4] [1.4 MB]
Creating vector for state rewards... [1.2 MB]
Creating vector for inf... [1.2 MB]
Allocating iteration vectors... [2 x 1.2 MB]
TOTAL: [12.5 MB]

Starting iterations...

Iterative method: 80 iterations in 0.46 seconds (average 0.002300, setup 0.28)

Value in the initial state: 0.11443069935474598

Time for model checking: 0.518 seconds.

Result: 0.11443069935474598 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"drop"}min=? [ F "deadlock" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=2,Objy1=2,Objx2=3,Objy2=2

Prob0A: 4 iterations in 0.02 seconds (average 0.006000, setup 0.00)

Prob1E: 5 iterations in 0.04 seconds (average 0.007200, setup 0.00)

Warning: PRISM hasn't checked for zero-reward loops.
Your minimum rewards may be too low...

goal = 1, inf = 0, maybe = 159663

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=159664, nc=212727, nnz=417715, k=4] [6.2 MB]
Building sparse matrix (transition rewards)... [n=159664, nc=212727, nnz=0, k=4] [1.4 MB]
Creating vector for state rewards... [1.2 MB]
Creating vector for inf... [1.2 MB]
Allocating iteration vectors... [2 x 1.2 MB]
TOTAL: [12.5 MB]

Starting iterations...

Iterative method: 75 iterations in 0.40 seconds (average 0.002347, setup 0.22)

Value in the initial state: 0.1084742087777475

Time for model checking: 0.461 seconds.

Result: 0.1084742087777475 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"charge"}max=? [ F "deadlock" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=2,Objy1=2,Objx2=3,Objy2=2

Prob0E: 4 iterations in 0.02 seconds (average 0.005000, setup 0.00)

Prob1A: 1 iterations in 0.00 seconds (average 0.004000, setup 0.00)

goal = 1, inf = 0, maybe = 159663

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=159664, nc=212727, nnz=417715, k=4] [6.2 MB]
Building sparse matrix (transition rewards)... [n=159664, nc=212727, nnz=4278, k=4] [1.5 MB]
Creating vector for state rewards... [1.2 MB]
Creating vector for inf... [1.2 MB]
Allocating iteration vectors... [2 x 1.2 MB]
TOTAL: [12.5 MB]

Starting iterations...

Iterative method: 84 iterations in 0.42 seconds (average 0.002286, setup 0.22)

Value in the initial state: 0.1283673261322411

Time for model checking: 0.447 seconds.

Result: 0.1283673261322411 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"charge"}min=? [ F "deadlock" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=2,Objy1=2,Objx2=3,Objy2=2

Prob0A: 4 iterations in 0.02 seconds (average 0.004000, setup 0.00)

Prob1E: 5 iterations in 0.03 seconds (average 0.006400, setup 0.00)

Warning: PRISM hasn't checked for zero-reward loops.
Your minimum rewards may be too low...

goal = 1, inf = 0, maybe = 159663

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=159664, nc=212727, nnz=417715, k=4] [6.2 MB]
Building sparse matrix (transition rewards)... [n=159664, nc=212727, nnz=4278, k=4] [1.5 MB]
Creating vector for state rewards... [1.2 MB]
Creating vector for inf... [1.2 MB]
Allocating iteration vectors... [2 x 1.2 MB]
TOTAL: [12.5 MB]

Starting iterations...

Iterative method: 70 iterations in 0.38 seconds (average 0.002286, setup 0.22)

Value in the initial state: 0.005957842247563812

Time for model checking: 0.44 seconds.

Result: 0.005957842247563812 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"time"}max=? [ F "deadlock" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=2,Objy1=2,Objx2=3,Objy2=2

Prob0E: 4 iterations in 0.02 seconds (average 0.005000, setup 0.00)

Prob1A: 1 iterations in 0.00 seconds (average 0.004000, setup 0.00)

goal = 1, inf = 0, maybe = 159663

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=159664, nc=212727, nnz=417715, k=4] [6.2 MB]
Building sparse matrix (transition rewards)... [n=159664, nc=212727, nnz=81122, k=4] [2.3 MB]
Creating vector for state rewards... [1.2 MB]
Creating vector for inf... [1.2 MB]
Allocating iteration vectors... [2 x 1.2 MB]
TOTAL: [13.4 MB]

Starting iterations...

Iterative method: 90 iterations in 0.46 seconds (average 0.002356, setup 0.25)

Value in the initial state: 119.71530740413266

Time for model checking: 0.491 seconds.

Result: 119.71530740413266 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"time"}min=? [ F "deadlock" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=2,Objy1=2,Objx2=3,Objy2=2

Prob0A: 4 iterations in 0.02 seconds (average 0.004000, setup 0.00)

Prob1E: 5 iterations in 0.03 seconds (average 0.005600, setup 0.00)

Warning: PRISM hasn't checked for zero-reward loops.
Your minimum rewards may be too low...

goal = 1, inf = 0, maybe = 159663

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=159664, nc=212727, nnz=417715, k=4] [6.2 MB]
Building sparse matrix (transition rewards)... [n=159664, nc=212727, nnz=81122, k=4] [2.3 MB]
Creating vector for state rewards... [1.2 MB]
Creating vector for inf... [1.2 MB]
Allocating iteration vectors... [2 x 1.2 MB]
TOTAL: [13.4 MB]

Starting iterations...

Iterative method: 88 iterations in 0.46 seconds (average 0.002364, setup 0.25)

Value in the initial state: 74.75721946445915

Time for model checking: 0.514 seconds.

Result: 74.75721946445915 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"success"}max=? [ F "deadlock" ]/Pmin=? [ F "MissionSuccessful" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=2,Objy1=2,Objx2=3,Objy2=2

Prob0E: 4 iterations in 0.02 seconds (average 0.005000, setup 0.00)

Prob1A: 1 iterations in 0.00 seconds (average 0.004000, setup 0.00)

goal = 1, inf = 0, maybe = 159663

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=159664, nc=212727, nnz=417715, k=4] [6.2 MB]
Building sparse matrix (transition rewards)... [n=159664, nc=212727, nnz=2097, k=4] [1.4 MB]
Creating vector for state rewards... [1.2 MB]
Creating vector for inf... [1.2 MB]
Allocating iteration vectors... [2 x 1.2 MB]
TOTAL: [12.5 MB]

Starting iterations...

Iterative method: 89 iterations in 0.42 seconds (average 0.002247, setup 0.22)

Prob0E: 44 iterations in 0.19 seconds (average 0.004364, setup 0.00)

Prob1A: 3 iterations in 0.02 seconds (average 0.008000, setup 0.00)

yes = 1062, no = 134599, maybe = 24003

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=159664, nc=43787, nnz=121300, k=2] [1.6 MB]
Creating vector for yes... [1.2 MB]
Allocating iteration vectors... [2 x 1.2 MB]
TOTAL: [5.2 MB]

Starting iterations...

Iterative method: 83 iterations in 0.16 seconds (average 0.001060, setup 0.07)

Value in the initial state: 133.623162440021

Time for model checking: 0.846 seconds.

Result: 133.623162440021 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"success"}min=? [ F "deadlock" ]/Pmax=? [ F "MissionSuccessful" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=2,Objy1=2,Objx2=3,Objy2=2

Prob0A: 4 iterations in 0.02 seconds (average 0.005000, setup 0.00)

Prob1E: 5 iterations in 0.04 seconds (average 0.007200, setup 0.00)

Warning: PRISM hasn't checked for zero-reward loops.
Your minimum rewards may be too low...

goal = 1, inf = 0, maybe = 159663

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=159664, nc=212727, nnz=417715, k=4] [6.2 MB]
Building sparse matrix (transition rewards)... [n=159664, nc=212727, nnz=2097, k=4] [1.4 MB]
Creating vector for state rewards... [1.2 MB]
Creating vector for inf... [1.2 MB]
Allocating iteration vectors... [2 x 1.2 MB]
TOTAL: [12.5 MB]

Starting iterations...

Iterative method: 87 iterations in 0.43 seconds (average 0.002299, setup 0.23)

Prob0A: 44 iterations in 0.19 seconds (average 0.004273, setup 0.00)

Prob1E: 4 iterations in 0.02 seconds (average 0.006000, setup 0.00)

yes = 1062, no = 131243, maybe = 27359

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=159664, nc=50011, nnz=139194, k=2] [1.8 MB]
Creating vector for yes... [1.2 MB]
Allocating iteration vectors... [2 x 1.2 MB]
TOTAL: [5.4 MB]

Starting iterations...

Iterative method: 84 iterations in 0.18 seconds (average 0.001048, setup 0.10)

Value in the initial state: 73.24356450327254

Time for model checking: 0.896 seconds.

Result: 73.24356450327254 (value in the initial state)

---------------------------------------------------------------------

Note: There were 16 warnings during computation.

