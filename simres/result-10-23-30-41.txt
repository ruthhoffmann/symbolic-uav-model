PRISM
=====

Version: 4.3
Date: Wed Aug 03 16:13:05 BST 2016
Hostname: eglinton
Memory limits: cudd=1g, java(heap)=981.5m
Command line: prism uav_sim_mdp.prism -s uav_mdp.props -const 'Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=3,Objy1=0,Objx2=4,Objy2=1,'

Parsing model file "uav_sim_mdp.prism"...

Parsing properties file "uav_mdp.props"...

18 properties:
(1) Pmax=? [ F "MissionSuccessful" ]
(2) Pmin=? [ F "MissionSuccessful" ]
(3) Pmax=? [ F "MissionTimeUp" ]
(4) Pmin=? [ F "MissionTimeUp" ]
(5) Pmax=? [ F "Crash" ]
(6) Pmin=? [ F "Crash" ]
(7) Pmax=? [ F "AllAreaSearched" ]
(8) Pmin=? [ F "AllAreaSearched" ]
(9) Pmax=? [ F "MissionTimeUp"|"AllAreaSearched"|"Crash" ]
(10) Pmin=? [ F "MissionTimeUp"|"AllAreaSearched"|"Crash" ]
(11) R{"drop"}max=? [ F "deadlock" ]
(12) R{"drop"}min=? [ F "deadlock" ]
(13) R{"charge"}max=? [ F "deadlock" ]
(14) R{"charge"}min=? [ F "deadlock" ]
(15) R{"time"}max=? [ F "deadlock" ]
(16) R{"time"}min=? [ F "deadlock" ]
(17) R{"success"}max=? [ F "deadlock" ]/Pmin=? [ F "MissionSuccessful" ]
(18) R{"success"}min=? [ F "deadlock" ]/Pmax=? [ F "MissionSuccessful" ]

Type:        MDP
Modules:     Obj Move Robot T 
Variables:   NoOfObjs posx posy s1 retx rety b t c 

---------------------------------------------------------------------

Model checking: Pmax=? [ F "MissionSuccessful" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=3,Objy1=0,Objx2=4,Objy2=1

Building model...
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=3,Objy1=0,Objx2=4,Objy2=1

Warning: Guard for command 7 of module "Move" is never satisfied.

Warning: Guard for command 8 of module "Move" is never satisfied.

Warning: Guard for command 9 of module "Move" is never satisfied.

Warning: Guard for command 7 of module "Robot" is never satisfied.

Warning: Update 2 of command 3 of module "Robot" doesn't do anything ("(s1'=13)", line 210, column 44)

Warning: Update 2 of command 27 of module "T" doesn't do anything ("(c'=1)", line 323, column 108)

Warning: Guard for command 28 of module "T" is never satisfied.

Warning: Update 2 of command 9 of module "T" doesn't do anything ("(c'=1)", line 300, column 112)

Warning: Guard for command 10 of module "T" is never satisfied.

Warning: Update 2 of command 51 of module "T" doesn't do anything ("(c'=1)", line 354, column 113)

Warning: Guard for command 52 of module "T" is never satisfied.

Computing reachable states...

Reachability (BFS): 88 iterations in 1.51 seconds (average 0.017136, setup 0.00)

Time for model construction: 3.527 seconds.

Warning: Deadlocks detected and fixed in 1 states

Type:        MDP
States:      270685 (1 initial)
Transitions: 702264
Choices:     354478

Transition matrix: 209596 nodes (89 terminal), 702264 minterms, vars: 34r/34c/18nd

Prob0A: 46 iterations in 0.34 seconds (average 0.007478, setup 0.00)

Prob1E: 4 iterations in 0.05 seconds (average 0.013000, setup 0.00)

yes = 1086, no = 228656, maybe = 40943

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=270685, nc=72606, nnz=207196, k=2] [2.7 MB]
Creating vector for yes... [2.1 MB]
Allocating iteration vectors... [2 x 2.1 MB]
TOTAL: [8.9 MB]

Starting iterations...

Iterative method: 86 iterations in 0.44 seconds (average 0.002000, setup 0.27)

Value in the initial state: 0.9846526921274603

Time for model checking: 0.885 seconds.

Result: 0.9846526921274603 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmin=? [ F "MissionSuccessful" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=3,Objy1=0,Objx2=4,Objy2=1

Prob0E: 48 iterations in 0.51 seconds (average 0.010667, setup 0.00)

Prob1A: 3 iterations in 0.06 seconds (average 0.021333, setup 0.00)

yes = 1086, no = 235452, maybe = 34147

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=270685, nc=60251, nnz=171341, k=2] [2.3 MB]
Creating vector for yes... [2.1 MB]
Allocating iteration vectors... [2 x 2.1 MB]
TOTAL: [8.5 MB]

Starting iterations...

Iterative method: 83 iterations in 0.39 seconds (average 0.001831, setup 0.24)

Value in the initial state: 0.924415902868805

Time for model checking: 1.008 seconds.

Result: 0.924415902868805 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmax=? [ F "MissionTimeUp" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=3,Objy1=0,Objx2=4,Objy2=1

Prob0A: 67 iterations in 0.84 seconds (average 0.012597, setup 0.00)

Prob1E: 4 iterations in 0.04 seconds (average 0.011000, setup 0.00)

yes = 480, no = 169356, maybe = 100849

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=270685, nc=173375, nnz=473650, k=4] [5.8 MB]
Creating vector for yes... [2.1 MB]
Allocating iteration vectors... [2 x 2.1 MB]
TOTAL: [12.0 MB]

Starting iterations...

Iterative method: 95 iterations in 0.52 seconds (average 0.002316, setup 0.30)

Value in the initial state: 9.95680770533909E-5

Time for model checking: 1.427 seconds.

Result: 9.95680770533909E-5 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmin=? [ F "MissionTimeUp" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=3,Objy1=0,Objx2=4,Objy2=1

Prob0E: 82 iterations in 0.76 seconds (average 0.009317, setup 0.00)

Prob1A: 3 iterations in 0.05 seconds (average 0.016000, setup 0.00)

yes = 480, no = 169778, maybe = 100427

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=270685, nc=172621, nnz=472096, k=4] [5.8 MB]
Creating vector for yes... [2.1 MB]
Allocating iteration vectors... [2 x 2.1 MB]
TOTAL: [12.0 MB]

Starting iterations...

Iterative method: 113 iterations in 0.56 seconds (average 0.002372, setup 0.29)

Value in the initial state: 9.113055554866057E-8

Time for model checking: 1.365 seconds.

Result: 9.113055554866057E-8 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmax=? [ F "Crash" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=3,Objy1=0,Objx2=4,Objy2=1

Prob0A: 3 iterations in 0.10 seconds (average 0.033333, setup 0.00)

Prob1E: 178 iterations in 2.94 seconds (average 0.016539, setup 0.00)

yes = 132617, no = 2988, maybe = 135080

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=270685, nc=218873, nnz=566659, k=4] [7.0 MB]
Creating vector for yes... [2.1 MB]
Allocating iteration vectors... [2 x 2.1 MB]
TOTAL: [13.1 MB]

Starting iterations...

Iterative method: 87 iterations in 0.58 seconds (average 0.002437, setup 0.36)

Value in the initial state: 0.024241990549670335

Time for model checking: 3.655 seconds.

Result: 0.024241990549670335 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmin=? [ F "Crash" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=3,Objy1=0,Objx2=4,Objy2=1

Prob0E: 3 iterations in 0.08 seconds (average 0.026667, setup 0.00)

Prob1A: 47 iterations in 0.62 seconds (average 0.013191, setup 0.00)

yes = 132617, no = 2988, maybe = 135080

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=270685, nc=218873, nnz=566659, k=4] [7.0 MB]
Creating vector for yes... [2.1 MB]
Allocating iteration vectors... [2 x 2.1 MB]
TOTAL: [13.1 MB]

Starting iterations...

Iterative method: 78 iterations in 0.48 seconds (average 0.002564, setup 0.28)

Value in the initial state: 0.013303653014042108

Time for model checking: 1.214 seconds.

Result: 0.013303653014042108 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmax=? [ F "AllAreaSearched" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=3,Objy1=0,Objx2=4,Objy2=1

Prob0A: 61 iterations in 0.59 seconds (average 0.009639, setup 0.00)

Prob1E: 4 iterations in 0.06 seconds (average 0.015000, setup 0.00)

yes = 1421, no = 213139, maybe = 56125

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=270685, nc=82408, nnz=209225, k=2] [2.7 MB]
Creating vector for yes... [2.1 MB]
Allocating iteration vectors... [2 x 2.1 MB]
TOTAL: [8.9 MB]

Starting iterations...

Iterative method: 91 iterations in 0.39 seconds (average 0.001978, setup 0.21)

Value in the initial state: 0.055196147736991795

Time for model checking: 1.06 seconds.

Result: 0.055196147736991795 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmin=? [ F "AllAreaSearched" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=3,Objy1=0,Objx2=4,Objy2=1

Prob0E: 64 iterations in 0.53 seconds (average 0.008313, setup 0.00)

Prob1A: 3 iterations in 0.05 seconds (average 0.016000, setup 0.00)

yes = 1421, no = 219741, maybe = 49523

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=270685, nc=71201, nnz=178130, k=2] [2.4 MB]
Creating vector for yes... [2.1 MB]
Allocating iteration vectors... [2 x 2.1 MB]
TOTAL: [8.6 MB]

Starting iterations...

Iterative method: 95 iterations in 0.31 seconds (average 0.001811, setup 0.14)

Value in the initial state: 0.001830224229903865

Time for model checking: 0.919 seconds.

Result: 0.001830224229903865 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmax=? [ F "MissionTimeUp"|"AllAreaSearched"|"Crash" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=3,Objy1=0,Objx2=4,Objy2=1

Prob0A: 3 iterations in 0.11 seconds (average 0.036000, setup 0.00)

Prob1E: 184 iterations in 2.98 seconds (average 0.016174, setup 0.00)

yes = 235451, no = 1087, maybe = 34147

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=270685, nc=60251, nnz=171341, k=2] [2.3 MB]
Creating vector for yes... [2.1 MB]
Allocating iteration vectors... [2 x 2.1 MB]
TOTAL: [8.5 MB]

Starting iterations...

Iterative method: 77 iterations in 0.33 seconds (average 0.001766, setup 0.20)

Value in the initial state: 0.07558375459014208

Time for model checking: 3.443 seconds.

Result: 0.07558375459014208 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmin=? [ F "MissionTimeUp"|"AllAreaSearched"|"Crash" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=3,Objy1=0,Objx2=4,Objy2=1

Prob0E: 3 iterations in 0.08 seconds (average 0.028000, setup 0.00)

Prob1A: 46 iterations in 0.52 seconds (average 0.011391, setup 0.00)

yes = 228655, no = 1087, maybe = 40943

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=270685, nc=72606, nnz=207196, k=2] [2.7 MB]
Creating vector for yes... [2.1 MB]
Allocating iteration vectors... [2 x 2.1 MB]
TOTAL: [8.9 MB]

Starting iterations...

Iterative method: 86 iterations in 0.35 seconds (average 0.001860, setup 0.19)

Value in the initial state: 0.01534511604448343

Time for model checking: 0.974 seconds.

Result: 0.01534511604448343 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"drop"}max=? [ F "deadlock" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=3,Objy1=0,Objx2=4,Objy2=1

Prob0E: 4 iterations in 0.07 seconds (average 0.018000, setup 0.00)

Prob1A: 1 iterations in 0.01 seconds (average 0.008000, setup 0.00)

goal = 1, inf = 0, maybe = 270684

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=270685, nc=354477, nnz=702263, k=4] [10.4 MB]
Building sparse matrix (transition rewards)... [n=270685, nc=354477, nnz=0, k=4] [2.4 MB]
Creating vector for state rewards... [2.1 MB]
Creating vector for inf... [2.1 MB]
Allocating iteration vectors... [2 x 2.1 MB]
TOTAL: [21.1 MB]

Starting iterations...

Iterative method: 82 iterations in 0.78 seconds (average 0.003902, setup 0.46)

Value in the initial state: 0.2046427698034725

Time for model checking: 0.872 seconds.

Result: 0.2046427698034725 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"drop"}min=? [ F "deadlock" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=3,Objy1=0,Objx2=4,Objy2=1

Prob0A: 4 iterations in 0.05 seconds (average 0.013000, setup 0.00)

Prob1E: 5 iterations in 0.07 seconds (average 0.013600, setup 0.00)

Warning: PRISM hasn't checked for zero-reward loops.
Your minimum rewards may be too low...

goal = 1, inf = 0, maybe = 270684

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=270685, nc=354477, nnz=702263, k=4] [10.4 MB]
Building sparse matrix (transition rewards)... [n=270685, nc=354477, nnz=0, k=4] [2.4 MB]
Creating vector for state rewards... [2.1 MB]
Creating vector for inf... [2.1 MB]
Allocating iteration vectors... [2 x 2.1 MB]
TOTAL: [21.1 MB]

Starting iterations...

Iterative method: 75 iterations in 0.66 seconds (average 0.003787, setup 0.37)

Value in the initial state: 0.1900646316276758

Time for model checking: 0.791 seconds.

Result: 0.1900646316276758 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"charge"}max=? [ F "deadlock" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=3,Objy1=0,Objx2=4,Objy2=1

Prob0E: 4 iterations in 0.04 seconds (average 0.009000, setup 0.00)

Prob1A: 1 iterations in 0.01 seconds (average 0.008000, setup 0.00)

goal = 1, inf = 0, maybe = 270684

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=270685, nc=354477, nnz=702263, k=4] [10.4 MB]
Building sparse matrix (transition rewards)... [n=270685, nc=354477, nnz=7522, k=4] [2.5 MB]
Creating vector for state rewards... [2.1 MB]
Creating vector for inf... [2.1 MB]
Allocating iteration vectors... [2 x 2.1 MB]
TOTAL: [21.2 MB]

Starting iterations...

Iterative method: 82 iterations in 0.68 seconds (average 0.003805, setup 0.37)

Value in the initial state: 0.18380307852177874

Time for model checking: 0.745 seconds.

Result: 0.18380307852177874 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"charge"}min=? [ F "deadlock" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=3,Objy1=0,Objx2=4,Objy2=1

Prob0A: 4 iterations in 0.03 seconds (average 0.008000, setup 0.00)

Prob1E: 5 iterations in 0.06 seconds (average 0.012800, setup 0.00)

Warning: PRISM hasn't checked for zero-reward loops.
Your minimum rewards may be too low...

goal = 1, inf = 0, maybe = 270684

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=270685, nc=354477, nnz=702263, k=4] [10.4 MB]
Building sparse matrix (transition rewards)... [n=270685, nc=354477, nnz=7522, k=4] [2.5 MB]
Creating vector for state rewards... [2.1 MB]
Creating vector for inf... [2.1 MB]
Allocating iteration vectors... [2 x 2.1 MB]
TOTAL: [21.2 MB]

Starting iterations...

Iterative method: 93 iterations in 0.74 seconds (average 0.003871, setup 0.38)

Value in the initial state: 0.008935899381701307

Time for model checking: 0.85 seconds.

Result: 0.008935899381701307 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"time"}max=? [ F "deadlock" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=3,Objy1=0,Objx2=4,Objy2=1

Prob0E: 4 iterations in 0.04 seconds (average 0.010000, setup 0.00)

Prob1A: 1 iterations in 0.01 seconds (average 0.008000, setup 0.00)

goal = 1, inf = 0, maybe = 270684

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=270685, nc=354477, nnz=702263, k=4] [10.4 MB]
Building sparse matrix (transition rewards)... [n=270685, nc=354477, nnz=135603, k=4] [3.9 MB]
Creating vector for state rewards... [2.1 MB]
Creating vector for inf... [2.1 MB]
Allocating iteration vectors... [2 x 2.1 MB]
TOTAL: [22.6 MB]

Starting iterations...

Iterative method: 93 iterations in 0.79 seconds (average 0.004000, setup 0.42)

Value in the initial state: 133.9699845837137

Time for model checking: 0.856 seconds.

Result: 133.9699845837137 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"time"}min=? [ F "deadlock" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=3,Objy1=0,Objx2=4,Objy2=1

Prob0A: 4 iterations in 0.03 seconds (average 0.008000, setup 0.00)

Prob1E: 5 iterations in 0.06 seconds (average 0.012800, setup 0.00)

Warning: PRISM hasn't checked for zero-reward loops.
Your minimum rewards may be too low...

goal = 1, inf = 0, maybe = 270684

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=270685, nc=354477, nnz=702263, k=4] [10.4 MB]
Building sparse matrix (transition rewards)... [n=270685, nc=354477, nnz=135603, k=4] [3.9 MB]
Creating vector for state rewards... [2.1 MB]
Creating vector for inf... [2.1 MB]
Allocating iteration vectors... [2 x 2.1 MB]
TOTAL: [22.6 MB]

Starting iterations...

Iterative method: 92 iterations in 0.79 seconds (average 0.004043, setup 0.42)

Value in the initial state: 73.84537480139204

Time for model checking: 0.899 seconds.

Result: 73.84537480139204 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"success"}max=? [ F "deadlock" ]/Pmin=? [ F "MissionSuccessful" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=3,Objy1=0,Objx2=4,Objy2=1

Prob0E: 4 iterations in 0.04 seconds (average 0.009000, setup 0.00)

Prob1A: 1 iterations in 0.00 seconds (average 0.004000, setup 0.00)

goal = 1, inf = 0, maybe = 270684

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=270685, nc=354477, nnz=702263, k=4] [10.4 MB]
Building sparse matrix (transition rewards)... [n=270685, nc=354477, nnz=2140, k=4] [2.4 MB]
Creating vector for state rewards... [2.1 MB]
Creating vector for inf... [2.1 MB]
Allocating iteration vectors... [2 x 2.1 MB]
TOTAL: [21.1 MB]

Starting iterations...

Iterative method: 90 iterations in 0.72 seconds (average 0.003956, setup 0.36)

Prob0E: 48 iterations in 0.40 seconds (average 0.008250, setup 0.00)

Prob1A: 3 iterations in 0.06 seconds (average 0.020000, setup 0.00)

yes = 1086, no = 235452, maybe = 34147

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=270685, nc=60251, nnz=171341, k=2] [2.3 MB]
Creating vector for yes... [2.1 MB]
Allocating iteration vectors... [2 x 2.1 MB]
TOTAL: [8.5 MB]

Starting iterations...

Iterative method: 83 iterations in 0.32 seconds (average 0.001735, setup 0.18)

Value in the initial state: 142.33991703256243

Time for model checking: 1.6 seconds.

Result: 142.33991703256243 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"success"}min=? [ F "deadlock" ]/Pmax=? [ F "MissionSuccessful" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=3,Objy1=0,Objx2=4,Objy2=1

Prob0A: 4 iterations in 0.08 seconds (average 0.021000, setup 0.00)

Prob1E: 5 iterations in 0.07 seconds (average 0.013600, setup 0.00)

Warning: PRISM hasn't checked for zero-reward loops.
Your minimum rewards may be too low...

goal = 1, inf = 0, maybe = 270684

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=270685, nc=354477, nnz=702263, k=4] [10.4 MB]
Building sparse matrix (transition rewards)... [n=270685, nc=354477, nnz=2140, k=4] [2.4 MB]
Creating vector for state rewards... [2.1 MB]
Creating vector for inf... [2.1 MB]
Allocating iteration vectors... [2 x 2.1 MB]
TOTAL: [21.1 MB]

Starting iterations...

Iterative method: 88 iterations in 0.74 seconds (average 0.003909, setup 0.40)

Prob0A: 46 iterations in 0.34 seconds (average 0.007478, setup 0.00)

Prob1E: 4 iterations in 0.05 seconds (average 0.012000, setup 0.00)

yes = 1086, no = 228656, maybe = 40943

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=270685, nc=72606, nnz=207196, k=2] [2.7 MB]
Creating vector for yes... [2.1 MB]
Allocating iteration vectors... [2 x 2.1 MB]
TOTAL: [8.9 MB]

Starting iterations...

Iterative method: 86 iterations in 0.36 seconds (average 0.001907, setup 0.20)

Value in the initial state: 71.23421404794848

Time for model checking: 1.702 seconds.

Result: 71.23421404794848 (value in the initial state)

---------------------------------------------------------------------

Note: There were 16 warnings during computation.

