PRISM
=====

Version: 4.3
Date: Wed Aug 03 16:01:33 BST 2016
Hostname: eglinton
Memory limits: cudd=1g, java(heap)=981.5m
Command line: prism uav_sim_mdp.prism -s uav_mdp.props -const 'Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=4,Objy1=1,Objx2=4,Objy2=3,'

Parsing model file "uav_sim_mdp.prism"...

Parsing properties file "uav_mdp.props"...

18 properties:
(1) Pmax=? [ F "MissionSuccessful" ]
(2) Pmin=? [ F "MissionSuccessful" ]
(3) Pmax=? [ F "MissionTimeUp" ]
(4) Pmin=? [ F "MissionTimeUp" ]
(5) Pmax=? [ F "Crash" ]
(6) Pmin=? [ F "Crash" ]
(7) Pmax=? [ F "AllAreaSearched" ]
(8) Pmin=? [ F "AllAreaSearched" ]
(9) Pmax=? [ F "MissionTimeUp"|"AllAreaSearched"|"Crash" ]
(10) Pmin=? [ F "MissionTimeUp"|"AllAreaSearched"|"Crash" ]
(11) R{"drop"}max=? [ F "deadlock" ]
(12) R{"drop"}min=? [ F "deadlock" ]
(13) R{"charge"}max=? [ F "deadlock" ]
(14) R{"charge"}min=? [ F "deadlock" ]
(15) R{"time"}max=? [ F "deadlock" ]
(16) R{"time"}min=? [ F "deadlock" ]
(17) R{"success"}max=? [ F "deadlock" ]/Pmin=? [ F "MissionSuccessful" ]
(18) R{"success"}min=? [ F "deadlock" ]/Pmax=? [ F "MissionSuccessful" ]

Type:        MDP
Modules:     Obj Move Robot T 
Variables:   NoOfObjs posx posy s1 retx rety b t c 

---------------------------------------------------------------------

Model checking: Pmax=? [ F "MissionSuccessful" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=4,Objy1=1,Objx2=4,Objy2=3

Building model...
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=4,Objy1=1,Objx2=4,Objy2=3

Warning: Guard for command 7 of module "Move" is never satisfied.

Warning: Guard for command 8 of module "Move" is never satisfied.

Warning: Guard for command 9 of module "Move" is never satisfied.

Warning: Guard for command 7 of module "Robot" is never satisfied.

Warning: Update 2 of command 3 of module "Robot" doesn't do anything ("(s1'=13)", line 210, column 44)

Warning: Update 2 of command 27 of module "T" doesn't do anything ("(c'=1)", line 323, column 108)

Warning: Guard for command 28 of module "T" is never satisfied.

Warning: Update 2 of command 9 of module "T" doesn't do anything ("(c'=1)", line 300, column 112)

Warning: Guard for command 10 of module "T" is never satisfied.

Warning: Update 2 of command 51 of module "T" doesn't do anything ("(c'=1)", line 354, column 113)

Warning: Guard for command 52 of module "T" is never satisfied.

Computing reachable states...

Reachability (BFS): 88 iterations in 1.18 seconds (average 0.013455, setup 0.00)

Time for model construction: 3.006 seconds.

Warning: Deadlocks detected and fixed in 1 states

Type:        MDP
States:      237486 (1 initial)
Transitions: 623720
Choices:     314218

Transition matrix: 211881 nodes (93 terminal), 623720 minterms, vars: 34r/34c/18nd

Prob0A: 53 iterations in 0.58 seconds (average 0.010943, setup 0.00)

Prob1E: 4 iterations in 0.07 seconds (average 0.017000, setup 0.00)

yes = 1140, no = 197979, maybe = 38367

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=237486, nc=65651, nnz=182226, k=2] [2.4 MB]
Creating vector for yes... [1.8 MB]
Allocating iteration vectors... [2 x 1.8 MB]
TOTAL: [7.8 MB]

Starting iterations...

Iterative method: 97 iterations in 0.39 seconds (average 0.001567, setup 0.24)

Value in the initial state: 0.9815123849944584

Time for model checking: 1.084 seconds.

Result: 0.9815123849944584 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmin=? [ F "MissionSuccessful" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=4,Objy1=1,Objx2=4,Objy2=3

Prob0E: 54 iterations in 0.57 seconds (average 0.010593, setup 0.00)

Prob1A: 3 iterations in 0.05 seconds (average 0.016000, setup 0.00)

yes = 1140, no = 203650, maybe = 32696

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=237486, nc=55882, nnz=154450, k=2] [2.0 MB]
Creating vector for yes... [1.8 MB]
Allocating iteration vectors... [2 x 1.8 MB]
TOTAL: [7.5 MB]

Starting iterations...

Iterative method: 92 iterations in 0.30 seconds (average 0.001565, setup 0.16)

Value in the initial state: 0.8364814446941624

Time for model checking: 0.955 seconds.

Result: 0.8364814446941624 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmax=? [ F "MissionTimeUp" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=4,Objy1=1,Objx2=4,Objy2=3

Prob0A: 72 iterations in 0.69 seconds (average 0.009556, setup 0.00)

Prob1E: 4 iterations in 0.04 seconds (average 0.010000, setup 0.00)

yes = 376, no = 140133, maybe = 96977

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=237486, nc=160760, nnz=432603, k=4] [5.3 MB]
Creating vector for yes... [1.8 MB]
Allocating iteration vectors... [2 x 1.8 MB]
TOTAL: [10.8 MB]

Starting iterations...

Iterative method: 99 iterations in 0.50 seconds (average 0.002020, setup 0.30)

Value in the initial state: 1.5714703236050062E-4

Time for model checking: 1.214 seconds.

Result: 1.5714703236050062E-4 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmin=? [ F "MissionTimeUp" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=4,Objy1=1,Objx2=4,Objy2=3

Prob0E: 86 iterations in 0.98 seconds (average 0.011349, setup 0.00)

Prob1A: 3 iterations in 0.05 seconds (average 0.016000, setup 0.00)

yes = 376, no = 140423, maybe = 96687

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=237486, nc=160251, nnz=431531, k=4] [5.3 MB]
Creating vector for yes... [1.8 MB]
Allocating iteration vectors... [2 x 1.8 MB]
TOTAL: [10.8 MB]

Starting iterations...

Iterative method: 114 iterations in 0.50 seconds (average 0.002070, setup 0.26)

Value in the initial state: 2.0379292281479504E-7

Time for model checking: 1.554 seconds.

Result: 2.0379292281479504E-7 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmax=? [ F "Crash" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=4,Objy1=1,Objx2=4,Objy2=3

Prob0A: 3 iterations in 0.09 seconds (average 0.029333, setup 0.00)

Prob1E: 200 iterations in 2.82 seconds (average 0.014120, setup 0.00)

yes = 116436, no = 3163, maybe = 117887

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=237486, nc=194619, nnz=504121, k=4] [6.2 MB]
Creating vector for yes... [1.8 MB]
Allocating iteration vectors... [2 x 1.8 MB]
TOTAL: [11.6 MB]

Starting iterations...

Iterative method: 93 iterations in 0.52 seconds (average 0.002237, setup 0.32)

Value in the initial state: 0.02989241359989405

Time for model checking: 3.472 seconds.

Result: 0.02989241359989405 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmin=? [ F "Crash" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=4,Objy1=1,Objx2=4,Objy2=3

Prob0E: 4 iterations in 0.07 seconds (average 0.018000, setup 0.00)

Prob1A: 49 iterations in 0.52 seconds (average 0.010531, setup 0.00)

yes = 116436, no = 3163, maybe = 117887

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=237486, nc=194619, nnz=504121, k=4] [6.2 MB]
Creating vector for yes... [1.8 MB]
Allocating iteration vectors... [2 x 1.8 MB]
TOTAL: [11.6 MB]

Starting iterations...

Iterative method: 88 iterations in 0.46 seconds (average 0.002227, setup 0.26)

Value in the initial state: 0.016403691272584925

Time for model checking: 1.064 seconds.

Result: 0.016403691272584925 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmax=? [ F "AllAreaSearched" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=4,Objy1=1,Objx2=4,Objy2=3

Prob0A: 54 iterations in 0.39 seconds (average 0.007259, setup 0.00)

Prob1E: 4 iterations in 0.05 seconds (average 0.012000, setup 0.00)

yes = 1646, no = 176345, maybe = 59495

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=237486, nc=96580, nnz=259307, k=2] [3.3 MB]
Creating vector for yes... [1.8 MB]
Allocating iteration vectors... [2 x 1.8 MB]
TOTAL: [8.7 MB]

Starting iterations...

Iterative method: 86 iterations in 0.36 seconds (average 0.001767, setup 0.20)

Value in the initial state: 0.13867788701637118

Time for model checking: 0.814 seconds.

Result: 0.13867788701637118 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmin=? [ F "AllAreaSearched" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=4,Objy1=1,Objx2=4,Objy2=3

Prob0E: 62 iterations in 0.45 seconds (average 0.007290, setup 0.00)

Prob1A: 3 iterations in 0.04 seconds (average 0.014667, setup 0.00)

yes = 1646, no = 183718, maybe = 52122

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=237486, nc=84394, nnz=225118, k=2] [2.9 MB]
Creating vector for yes... [1.8 MB]
Allocating iteration vectors... [2 x 1.8 MB]
TOTAL: [8.3 MB]

Starting iterations...

Iterative method: 96 iterations in 0.32 seconds (average 0.001750, setup 0.15)

Value in the initial state: 0.0018012638495226655

Time for model checking: 0.832 seconds.

Result: 0.0018012638495226655 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmax=? [ F "MissionTimeUp"|"AllAreaSearched"|"Crash" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=4,Objy1=1,Objx2=4,Objy2=3

Prob0A: 3 iterations in 0.09 seconds (average 0.029333, setup 0.00)

Prob1E: 208 iterations in 2.80 seconds (average 0.013462, setup 0.00)

yes = 203649, no = 1141, maybe = 32696

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=237486, nc=55882, nnz=154450, k=2] [2.0 MB]
Creating vector for yes... [1.8 MB]
Allocating iteration vectors... [2 x 1.8 MB]
TOTAL: [7.5 MB]

Starting iterations...

Iterative method: 86 iterations in 0.30 seconds (average 0.001442, setup 0.18)

Value in the initial state: 0.16351831335886718

Time for model checking: 3.223 seconds.

Result: 0.16351831335886718 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmin=? [ F "MissionTimeUp"|"AllAreaSearched"|"Crash" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=4,Objy1=1,Objx2=4,Objy2=3

Prob0E: 4 iterations in 0.06 seconds (average 0.014000, setup 0.00)

Prob1A: 53 iterations in 0.48 seconds (average 0.008981, setup 0.00)

yes = 197978, no = 1141, maybe = 38367

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=237486, nc=65651, nnz=182226, k=2] [2.4 MB]
Creating vector for yes... [1.8 MB]
Allocating iteration vectors... [2 x 1.8 MB]
TOTAL: [7.8 MB]

Starting iterations...

Iterative method: 95 iterations in 0.30 seconds (average 0.001642, setup 0.14)

Value in the initial state: 0.018486314863632107

Time for model checking: 0.869 seconds.

Result: 0.018486314863632107 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"drop"}max=? [ F "deadlock" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=4,Objy1=1,Objx2=4,Objy2=3

Prob0E: 5 iterations in 0.06 seconds (average 0.012800, setup 0.00)

Prob1A: 1 iterations in 0.01 seconds (average 0.008000, setup 0.00)

goal = 1, inf = 0, maybe = 237485

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=237486, nc=314217, nnz=623719, k=4] [9.2 MB]
Building sparse matrix (transition rewards)... [n=237486, nc=314217, nnz=0, k=4] [2.1 MB]
Creating vector for state rewards... [1.8 MB]
Creating vector for inf... [1.8 MB]
Allocating iteration vectors... [2 x 1.8 MB]
TOTAL: [18.6 MB]

Starting iterations...

Iterative method: 93 iterations in 0.72 seconds (average 0.003441, setup 0.40)

Value in the initial state: 0.1828558739200878

Time for model checking: 0.801 seconds.

Result: 0.1828558739200878 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"drop"}min=? [ F "deadlock" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=4,Objy1=1,Objx2=4,Objy2=3

Prob0A: 4 iterations in 0.06 seconds (average 0.014000, setup 0.00)

Prob1E: 5 iterations in 0.05 seconds (average 0.009600, setup 0.00)

Warning: PRISM hasn't checked for zero-reward loops.
Your minimum rewards may be too low...

goal = 1, inf = 0, maybe = 237485

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=237486, nc=314217, nnz=623719, k=4] [9.2 MB]
Building sparse matrix (transition rewards)... [n=237486, nc=314217, nnz=0, k=4] [2.1 MB]
Creating vector for state rewards... [1.8 MB]
Creating vector for inf... [1.8 MB]
Allocating iteration vectors... [2 x 1.8 MB]
TOTAL: [18.6 MB]

Starting iterations...

Iterative method: 85 iterations in 0.61 seconds (average 0.003482, setup 0.32)

Value in the initial state: 0.15929304455274204

Time for model checking: 0.727 seconds.

Result: 0.15929304455274204 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"charge"}max=? [ F "deadlock" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=4,Objy1=1,Objx2=4,Objy2=3

Prob0E: 5 iterations in 0.03 seconds (average 0.006400, setup 0.00)

Prob1A: 1 iterations in 0.01 seconds (average 0.008000, setup 0.00)

goal = 1, inf = 0, maybe = 237485

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=237486, nc=314217, nnz=623719, k=4] [9.2 MB]
Building sparse matrix (transition rewards)... [n=237486, nc=314217, nnz=6908, k=4] [2.2 MB]
Creating vector for state rewards... [1.8 MB]
Creating vector for inf... [1.8 MB]
Allocating iteration vectors... [2 x 1.8 MB]
TOTAL: [18.7 MB]

Starting iterations...

Iterative method: 92 iterations in 0.64 seconds (average 0.003435, setup 0.32)

Value in the initial state: 0.27321499522706794

Time for model checking: 0.686 seconds.

Result: 0.27321499522706794 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"charge"}min=? [ F "deadlock" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=4,Objy1=1,Objx2=4,Objy2=3

Prob0A: 4 iterations in 0.03 seconds (average 0.008000, setup 0.00)

Prob1E: 5 iterations in 0.05 seconds (average 0.010400, setup 0.00)

Warning: PRISM hasn't checked for zero-reward loops.
Your minimum rewards may be too low...

goal = 1, inf = 0, maybe = 237485

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=237486, nc=314217, nnz=623719, k=4] [9.2 MB]
Building sparse matrix (transition rewards)... [n=237486, nc=314217, nnz=6908, k=4] [2.2 MB]
Creating vector for state rewards... [1.8 MB]
Creating vector for inf... [1.8 MB]
Allocating iteration vectors... [2 x 1.8 MB]
TOTAL: [18.7 MB]

Starting iterations...

Iterative method: 98 iterations in 0.66 seconds (average 0.003469, setup 0.32)

Value in the initial state: 0.03873696127768559

Time for model checking: 0.75 seconds.

Result: 0.03873696127768559 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"time"}max=? [ F "deadlock" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=4,Objy1=1,Objx2=4,Objy2=3

Prob0E: 5 iterations in 0.04 seconds (average 0.007200, setup 0.00)

Prob1A: 1 iterations in 0.01 seconds (average 0.008000, setup 0.00)

goal = 1, inf = 0, maybe = 237485

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=237486, nc=314217, nnz=623719, k=4] [9.2 MB]
Building sparse matrix (transition rewards)... [n=237486, nc=314217, nnz=119597, k=4] [3.5 MB]
Creating vector for state rewards... [1.8 MB]
Creating vector for inf... [1.8 MB]
Allocating iteration vectors... [2 x 1.8 MB]
TOTAL: [20.0 MB]

Starting iterations...

Iterative method: 96 iterations in 0.70 seconds (average 0.003500, setup 0.36)

Value in the initial state: 165.00287558058182

Time for model checking: 0.748 seconds.

Result: 165.00287558058182 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"time"}min=? [ F "deadlock" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=4,Objy1=1,Objx2=4,Objy2=3

Prob0A: 4 iterations in 0.03 seconds (average 0.007000, setup 0.00)

Prob1E: 5 iterations in 0.05 seconds (average 0.010400, setup 0.00)

Warning: PRISM hasn't checked for zero-reward loops.
Your minimum rewards may be too low...

goal = 1, inf = 0, maybe = 237485

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=237486, nc=314217, nnz=623719, k=4] [9.2 MB]
Building sparse matrix (transition rewards)... [n=237486, nc=314217, nnz=119597, k=4] [3.5 MB]
Creating vector for state rewards... [1.8 MB]
Creating vector for inf... [1.8 MB]
Allocating iteration vectors... [2 x 1.8 MB]
TOTAL: [20.0 MB]

Starting iterations...

Iterative method: 101 iterations in 0.71 seconds (average 0.003485, setup 0.36)

Value in the initial state: 90.95319093489627

Time for model checking: 0.798 seconds.

Result: 90.95319093489627 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"success"}max=? [ F "deadlock" ]/Pmin=? [ F "MissionSuccessful" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=4,Objy1=1,Objx2=4,Objy2=3

Prob0E: 5 iterations in 0.03 seconds (average 0.005600, setup 0.00)

Prob1A: 1 iterations in 0.01 seconds (average 0.008000, setup 0.00)

goal = 1, inf = 0, maybe = 237485

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=237486, nc=314217, nnz=623719, k=4] [9.2 MB]
Building sparse matrix (transition rewards)... [n=237486, nc=314217, nnz=2246, k=4] [2.1 MB]
Creating vector for state rewards... [1.8 MB]
Creating vector for inf... [1.8 MB]
Allocating iteration vectors... [2 x 1.8 MB]
TOTAL: [18.6 MB]

Starting iterations...

Iterative method: 98 iterations in 0.65 seconds (average 0.003388, setup 0.32)

Prob0E: 54 iterations in 0.32 seconds (average 0.005852, setup 0.00)

Prob1A: 3 iterations in 0.05 seconds (average 0.016000, setup 0.00)

yes = 1140, no = 203650, maybe = 32696

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=237486, nc=55882, nnz=154450, k=2] [2.0 MB]
Creating vector for yes... [1.8 MB]
Allocating iteration vectors... [2 x 1.8 MB]
TOTAL: [7.5 MB]

Starting iterations...

Iterative method: 92 iterations in 0.30 seconds (average 0.001522, setup 0.16)

Value in the initial state: 193.52448176198962

Time for model checking: 1.393 seconds.

Result: 193.52448176198962 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"success"}min=? [ F "deadlock" ]/Pmax=? [ F "MissionSuccessful" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=4,Objy1=1,Objx2=4,Objy2=3

Prob0A: 4 iterations in 0.06 seconds (average 0.016000, setup 0.00)

Prob1E: 5 iterations in 0.04 seconds (average 0.008800, setup 0.00)

Warning: PRISM hasn't checked for zero-reward loops.
Your minimum rewards may be too low...

goal = 1, inf = 0, maybe = 237485

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=237486, nc=314217, nnz=623719, k=4] [9.2 MB]
Building sparse matrix (transition rewards)... [n=237486, nc=314217, nnz=2246, k=4] [2.1 MB]
Creating vector for state rewards... [1.8 MB]
Creating vector for inf... [1.8 MB]
Allocating iteration vectors... [2 x 1.8 MB]
TOTAL: [18.6 MB]

Starting iterations...

Iterative method: 97 iterations in 0.66 seconds (average 0.003381, setup 0.34)

Prob0A: 53 iterations in 0.26 seconds (average 0.004906, setup 0.00)

Prob1E: 4 iterations in 0.04 seconds (average 0.009000, setup 0.00)

yes = 1140, no = 197979, maybe = 38367

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=237486, nc=65651, nnz=182226, k=2] [2.4 MB]
Creating vector for yes... [1.8 MB]
Allocating iteration vectors... [2 x 1.8 MB]
TOTAL: [7.8 MB]

Starting iterations...

Iterative method: 97 iterations in 0.30 seconds (average 0.001567, setup 0.14)

Value in the initial state: 78.70255598090438

Time for model checking: 1.402 seconds.

Result: 78.70255598090438 (value in the initial state)

---------------------------------------------------------------------

Note: There were 16 warnings during computation.

