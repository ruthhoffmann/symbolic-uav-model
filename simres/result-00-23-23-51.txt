PRISM
=====

Version: 4.3
Date: Wed Aug 03 15:58:26 BST 2016
Hostname: eglinton
Memory limits: cudd=1g, java(heap)=981.5m
Command line: prism uav_sim_mdp.prism -s uav_mdp.props -const 'Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=2,Objy1=3,Objx2=5,Objy2=1,'

Parsing model file "uav_sim_mdp.prism"...

Parsing properties file "uav_mdp.props"...

18 properties:
(1) Pmax=? [ F "MissionSuccessful" ]
(2) Pmin=? [ F "MissionSuccessful" ]
(3) Pmax=? [ F "MissionTimeUp" ]
(4) Pmin=? [ F "MissionTimeUp" ]
(5) Pmax=? [ F "Crash" ]
(6) Pmin=? [ F "Crash" ]
(7) Pmax=? [ F "AllAreaSearched" ]
(8) Pmin=? [ F "AllAreaSearched" ]
(9) Pmax=? [ F "MissionTimeUp"|"AllAreaSearched"|"Crash" ]
(10) Pmin=? [ F "MissionTimeUp"|"AllAreaSearched"|"Crash" ]
(11) R{"drop"}max=? [ F "deadlock" ]
(12) R{"drop"}min=? [ F "deadlock" ]
(13) R{"charge"}max=? [ F "deadlock" ]
(14) R{"charge"}min=? [ F "deadlock" ]
(15) R{"time"}max=? [ F "deadlock" ]
(16) R{"time"}min=? [ F "deadlock" ]
(17) R{"success"}max=? [ F "deadlock" ]/Pmin=? [ F "MissionSuccessful" ]
(18) R{"success"}min=? [ F "deadlock" ]/Pmax=? [ F "MissionSuccessful" ]

Type:        MDP
Modules:     Obj Move Robot T 
Variables:   NoOfObjs posx posy s1 retx rety b t c 

---------------------------------------------------------------------

Model checking: Pmax=? [ F "MissionSuccessful" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=2,Objy1=3,Objx2=5,Objy2=1

Building model...
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=2,Objy1=3,Objx2=5,Objy2=1

Warning: Guard for command 7 of module "Move" is never satisfied.

Warning: Guard for command 8 of module "Move" is never satisfied.

Warning: Guard for command 9 of module "Move" is never satisfied.

Warning: Guard for command 7 of module "Robot" is never satisfied.

Warning: Update 2 of command 3 of module "Robot" doesn't do anything ("(s1'=13)", line 210, column 44)

Warning: Update 2 of command 27 of module "T" doesn't do anything ("(c'=1)", line 323, column 108)

Warning: Guard for command 28 of module "T" is never satisfied.

Warning: Update 2 of command 9 of module "T" doesn't do anything ("(c'=1)", line 300, column 112)

Warning: Guard for command 10 of module "T" is never satisfied.

Warning: Update 2 of command 51 of module "T" doesn't do anything ("(c'=1)", line 354, column 113)

Warning: Guard for command 52 of module "T" is never satisfied.

Computing reachable states...

Reachability (BFS): 88 iterations in 0.89 seconds (average 0.010136, setup 0.00)

Time for model construction: 2.847 seconds.

Warning: Deadlocks detected and fixed in 1 states

Type:        MDP
States:      151023 (1 initial)
Transitions: 362123
Choices:     190419

Transition matrix: 177542 nodes (95 terminal), 362123 minterms, vars: 34r/34c/18nd

Prob0A: 60 iterations in 0.29 seconds (average 0.004867, setup 0.00)

Prob1E: 4 iterations in 0.04 seconds (average 0.010000, setup 0.00)

yes = 207, no = 143941, maybe = 6875

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=151023, nc=12586, nnz=37076, k=2] [594.3 KB]
Creating vector for yes... [1.2 MB]
Allocating iteration vectors... [2 x 1.2 MB]
TOTAL: [4.0 MB]

Starting iterations...

Iterative method: 92 iterations in 0.18 seconds (average 0.000826, setup 0.10)

Value in the initial state: 9.453660802473656E-4

Time for model checking: 0.524 seconds.

Result: 9.453660802473656E-4 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmin=? [ F "MissionSuccessful" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=2,Objy1=3,Objx2=5,Objy2=1

Prob0E: 22 iterations in 0.08 seconds (average 0.003455, setup 0.00)

Prob1A: 3 iterations in 0.02 seconds (average 0.006667, setup 0.00)

yes = 207, no = 145535, maybe = 5281

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=151023, nc=9649, nnz=28162, k=2] [486.9 KB]
Creating vector for yes... [1.2 MB]
Allocating iteration vectors... [2 x 1.2 MB]
TOTAL: [3.9 MB]

Starting iterations...

Iterative method: 42 iterations in 0.07 seconds (average 0.000857, setup 0.03)

Value in the initial state: 0.0

Time for model checking: 0.174 seconds.

Result: 0.0 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmax=? [ F "MissionTimeUp" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=2,Objy1=3,Objx2=5,Objy2=1

Prob0A: 70 iterations in 0.57 seconds (average 0.008114, setup 0.00)

Prob1E: 4 iterations in 0.03 seconds (average 0.008000, setup 0.00)

yes = 352, no = 106942, maybe = 43729

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=151023, nc=75642, nnz=207217, k=4] [2.6 MB]
Creating vector for yes... [1.2 MB]
Allocating iteration vectors... [2 x 1.2 MB]
TOTAL: [6.0 MB]

Starting iterations...

Iterative method: 99 iterations in 0.28 seconds (average 0.001131, setup 0.17)

Value in the initial state: 1.5871705791491004E-5

Time for model checking: 0.903 seconds.

Result: 1.5871705791491004E-5 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmin=? [ F "MissionTimeUp" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=2,Objy1=3,Objx2=5,Objy2=1

Prob0E: 85 iterations in 0.54 seconds (average 0.006353, setup 0.00)

Prob1A: 3 iterations in 0.03 seconds (average 0.009333, setup 0.00)

yes = 352, no = 107228, maybe = 43443

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=151023, nc=75130, nnz=206107, k=4] [2.6 MB]
Creating vector for yes... [1.2 MB]
Allocating iteration vectors... [2 x 1.2 MB]
TOTAL: [6.0 MB]

Starting iterations...

Iterative method: 116 iterations in 0.27 seconds (average 0.001172, setup 0.13)

Value in the initial state: 1.628496165678981E-8

Time for model checking: 0.825 seconds.

Result: 1.628496165678981E-8 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmax=? [ F "Crash" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=2,Objy1=3,Objx2=5,Objy2=1

Prob0A: 3 iterations in 0.07 seconds (average 0.024000, setup 0.00)

Prob1E: 172 iterations in 2.07 seconds (average 0.012047, setup 0.00)

yes = 74101, no = 1914, maybe = 75008

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=151023, nc=114404, nnz=286108, k=4] [3.5 MB]
Creating vector for yes... [1.2 MB]
Allocating iteration vectors... [2 x 1.2 MB]
TOTAL: [7.0 MB]

Starting iterations...

Iterative method: 91 iterations in 0.37 seconds (average 0.001363, setup 0.24)

Value in the initial state: 0.01424455938807026

Time for model checking: 2.524 seconds.

Result: 0.01424455938807026 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmin=? [ F "Crash" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=2,Objy1=3,Objx2=5,Objy2=1

Prob0E: 4 iterations in 0.08 seconds (average 0.020000, setup 0.00)

Prob1A: 46 iterations in 0.42 seconds (average 0.009043, setup 0.00)

yes = 74101, no = 1914, maybe = 75008

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=151023, nc=114404, nnz=286108, k=4] [3.5 MB]
Creating vector for yes... [1.2 MB]
Allocating iteration vectors... [2 x 1.2 MB]
TOTAL: [7.0 MB]

Starting iterations...

Iterative method: 78 iterations in 0.25 seconds (average 0.001333, setup 0.15)

Value in the initial state: 0.011402049774352638

Time for model checking: 0.76 seconds.

Result: 0.011402049774352638 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmax=? [ F "AllAreaSearched" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=2,Objy1=3,Objx2=5,Objy2=1

Prob0A: 46 iterations in 0.39 seconds (average 0.008522, setup 0.00)

Prob1E: 4 iterations in 0.04 seconds (average 0.011000, setup 0.00)

yes = 1354, no = 104302, maybe = 45367

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=151023, nc=64166, nnz=158763, k=2] [2.0 MB]
Creating vector for yes... [1.2 MB]
Allocating iteration vectors... [2 x 1.2 MB]
TOTAL: [5.5 MB]

Starting iterations...

Iterative method: 88 iterations in 0.25 seconds (average 0.001091, setup 0.15)

Value in the initial state: 0.9885959037875202

Time for model checking: 0.707 seconds.

Result: 0.9885959037875202 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmin=? [ F "AllAreaSearched" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=2,Objy1=3,Objx2=5,Objy2=1

Prob0E: 50 iterations in 0.30 seconds (average 0.006000, setup 0.00)

Prob1A: 3 iterations in 0.03 seconds (average 0.010667, setup 0.00)

yes = 1354, no = 107005, maybe = 42664

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=151023, nc=59045, nnz=143577, k=2] [1.8 MB]
Creating vector for yes... [1.2 MB]
Allocating iteration vectors... [2 x 1.2 MB]
TOTAL: [5.3 MB]

Starting iterations...

Iterative method: 93 iterations in 0.20 seconds (average 0.001075, setup 0.10)

Value in the initial state: 0.984818292713338

Time for model checking: 0.547 seconds.

Result: 0.984818292713338 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmax=? [ F "MissionTimeUp"|"AllAreaSearched"|"Crash" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=2,Objy1=3,Objx2=5,Objy2=1

Prob0A: 3 iterations in 0.06 seconds (average 0.021333, setup 0.00)

Prob1E: 84 iterations in 0.79 seconds (average 0.009381, setup 0.00)

yes = 145534, no = 208, maybe = 5281

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=151023, nc=9649, nnz=28162, k=2] [486.9 KB]
Creating vector for yes... [1.2 MB]
Allocating iteration vectors... [2 x 1.2 MB]
TOTAL: [3.9 MB]

Starting iterations...

Iterative method: 40 iterations in 0.08 seconds (average 0.000700, setup 0.05)

Value in the initial state: 1.0

Time for model checking: 0.941 seconds.

Result: 1.0 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmin=? [ F "MissionTimeUp"|"AllAreaSearched"|"Crash" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=2,Objy1=3,Objx2=5,Objy2=1

Prob0E: 4 iterations in 0.04 seconds (average 0.009000, setup 0.00)

Prob1A: 60 iterations in 0.36 seconds (average 0.006067, setup 0.00)

yes = 143940, no = 208, maybe = 6875

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=151023, nc=12586, nnz=37076, k=2] [594.3 KB]
Creating vector for yes... [1.2 MB]
Allocating iteration vectors... [2 x 1.2 MB]
TOTAL: [4.0 MB]

Starting iterations...

Iterative method: 55 iterations in 0.10 seconds (average 0.000727, setup 0.06)

Value in the initial state: 0.9990509760192123

Time for model checking: 0.518 seconds.

Result: 0.9990509760192123 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"drop"}max=? [ F "deadlock" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=2,Objy1=3,Objx2=5,Objy2=1

Prob0E: 5 iterations in 0.04 seconds (average 0.008800, setup 0.00)

Prob1A: 1 iterations in 0.00 seconds (average 0.004000, setup 0.00)

goal = 1, inf = 0, maybe = 151022

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=151023, nc=190418, nnz=362122, k=4] [5.4 MB]
Building sparse matrix (transition rewards)... [n=151023, nc=190418, nnz=0, k=4] [1.3 MB]
Creating vector for state rewards... [1.2 MB]
Creating vector for inf... [1.2 MB]
Allocating iteration vectors... [2 x 1.2 MB]
TOTAL: [11.4 MB]

Starting iterations...

Iterative method: 70 iterations in 0.43 seconds (average 0.002057, setup 0.29)

Value in the initial state: 0.11003303333699073

Time for model checking: 0.492 seconds.

Result: 0.11003303333699073 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"drop"}min=? [ F "deadlock" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=2,Objy1=3,Objx2=5,Objy2=1

Prob0A: 4 iterations in 0.02 seconds (average 0.006000, setup 0.00)

Prob1E: 5 iterations in 0.04 seconds (average 0.008000, setup 0.00)

Warning: PRISM hasn't checked for zero-reward loops.
Your minimum rewards may be too low...

goal = 1, inf = 0, maybe = 151022

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=151023, nc=190418, nnz=362122, k=4] [5.4 MB]
Building sparse matrix (transition rewards)... [n=151023, nc=190418, nnz=0, k=4] [1.3 MB]
Creating vector for state rewards... [1.2 MB]
Creating vector for inf... [1.2 MB]
Allocating iteration vectors... [2 x 1.2 MB]
TOTAL: [11.4 MB]

Starting iterations...

Iterative method: 62 iterations in 0.34 seconds (average 0.002129, setup 0.21)

Value in the initial state: 0.10983799428325593

Time for model checking: 0.412 seconds.

Result: 0.10983799428325593 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"charge"}max=? [ F "deadlock" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=2,Objy1=3,Objx2=5,Objy2=1

Prob0E: 5 iterations in 0.03 seconds (average 0.005600, setup 0.00)

Prob1A: 1 iterations in 0.00 seconds (average 0.004000, setup 0.00)

goal = 1, inf = 0, maybe = 151022

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=151023, nc=190418, nnz=362122, k=4] [5.4 MB]
Building sparse matrix (transition rewards)... [n=151023, nc=190418, nnz=3375, k=4] [1.3 MB]
Creating vector for state rewards... [1.2 MB]
Creating vector for inf... [1.2 MB]
Allocating iteration vectors... [2 x 1.2 MB]
TOTAL: [11.4 MB]

Starting iterations...

Iterative method: 82 iterations in 0.38 seconds (average 0.002098, setup 0.21)

Value in the initial state: 0.0020433732104239415

Time for model checking: 0.42 seconds.

Result: 0.0020433732104239415 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"charge"}min=? [ F "deadlock" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=2,Objy1=3,Objx2=5,Objy2=1

Prob0A: 4 iterations in 0.02 seconds (average 0.006000, setup 0.00)

Prob1E: 5 iterations in 0.03 seconds (average 0.006400, setup 0.00)

Warning: PRISM hasn't checked for zero-reward loops.
Your minimum rewards may be too low...

goal = 1, inf = 0, maybe = 151022

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=151023, nc=190418, nnz=362122, k=4] [5.4 MB]
Building sparse matrix (transition rewards)... [n=151023, nc=190418, nnz=3375, k=4] [1.3 MB]
Creating vector for state rewards... [1.2 MB]
Creating vector for inf... [1.2 MB]
Allocating iteration vectors... [2 x 1.2 MB]
TOTAL: [11.4 MB]

Starting iterations...

Iterative method: 94 iterations in 0.41 seconds (average 0.002128, setup 0.21)

Value in the initial state: 1.0697942228228692E-4

Time for model checking: 0.468 seconds.

Result: 1.0697942228228692E-4 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"time"}max=? [ F "deadlock" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=2,Objy1=3,Objx2=5,Objy2=1

Prob0E: 5 iterations in 0.03 seconds (average 0.005600, setup 0.00)

Prob1A: 1 iterations in 0.00 seconds (average 0.004000, setup 0.00)

goal = 1, inf = 0, maybe = 151022

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=151023, nc=190418, nnz=362122, k=4] [5.4 MB]
Building sparse matrix (transition rewards)... [n=151023, nc=190418, nnz=76013, k=4] [2.2 MB]
Creating vector for state rewards... [1.2 MB]
Creating vector for inf... [1.2 MB]
Allocating iteration vectors... [2 x 1.2 MB]
TOTAL: [12.2 MB]

Starting iterations...

Iterative method: 96 iterations in 0.44 seconds (average 0.002125, setup 0.24)

Value in the initial state: 79.03101995283588

Time for model checking: 0.488 seconds.

Result: 79.03101995283588 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"time"}min=? [ F "deadlock" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=2,Objy1=3,Objx2=5,Objy2=1

Prob0A: 4 iterations in 0.02 seconds (average 0.006000, setup 0.00)

Prob1E: 5 iterations in 0.03 seconds (average 0.006400, setup 0.00)

Warning: PRISM hasn't checked for zero-reward loops.
Your minimum rewards may be too low...

goal = 1, inf = 0, maybe = 151022

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=151023, nc=190418, nnz=362122, k=4] [5.4 MB]
Building sparse matrix (transition rewards)... [n=151023, nc=190418, nnz=76013, k=4] [2.2 MB]
Creating vector for state rewards... [1.2 MB]
Creating vector for inf... [1.2 MB]
Allocating iteration vectors... [2 x 1.2 MB]
TOTAL: [12.2 MB]

Starting iterations...

Iterative method: 96 iterations in 0.44 seconds (average 0.002167, setup 0.23)

Value in the initial state: 63.31872882964449

Time for model checking: 0.505 seconds.

Result: 63.31872882964449 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"success"}max=? [ F "deadlock" ]/Pmin=? [ F "MissionSuccessful" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=2,Objy1=3,Objx2=5,Objy2=1

Prob0E: 5 iterations in 0.03 seconds (average 0.005600, setup 0.00)

Prob1A: 1 iterations in 0.00 seconds (average 0.004000, setup 0.00)

goal = 1, inf = 0, maybe = 151022

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=151023, nc=190418, nnz=362122, k=4] [5.4 MB]
Building sparse matrix (transition rewards)... [n=151023, nc=190418, nnz=408, k=4] [1.3 MB]
Creating vector for state rewards... [1.2 MB]
Creating vector for inf... [1.2 MB]
Allocating iteration vectors... [2 x 1.2 MB]
TOTAL: [11.4 MB]

Starting iterations...

Iterative method: 89 iterations in 0.39 seconds (average 0.002067, setup 0.20)

Prob0E: 22 iterations in 0.06 seconds (average 0.002727, setup 0.00)

Prob1A: 3 iterations in 0.03 seconds (average 0.009333, setup 0.00)

yes = 207, no = 145535, maybe = 5281

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=151023, nc=9649, nnz=28162, k=2] [486.9 KB]
Creating vector for yes... [1.2 MB]
Allocating iteration vectors... [2 x 1.2 MB]
TOTAL: [3.9 MB]

Starting iterations...

Iterative method: 42 iterations in 0.06 seconds (average 0.000762, setup 0.02)

Value in the initial state: Infinity

Time for model checking: 0.584 seconds.

Result: Infinity (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"success"}min=? [ F "deadlock" ]/Pmax=? [ F "MissionSuccessful" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=2,Objy1=3,Objx2=5,Objy2=1

Prob0A: 4 iterations in 0.04 seconds (average 0.010000, setup 0.00)

Prob1E: 5 iterations in 0.04 seconds (average 0.007200, setup 0.00)

Warning: PRISM hasn't checked for zero-reward loops.
Your minimum rewards may be too low...

goal = 1, inf = 0, maybe = 151022

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=151023, nc=190418, nnz=362122, k=4] [5.4 MB]
Building sparse matrix (transition rewards)... [n=151023, nc=190418, nnz=408, k=4] [1.3 MB]
Creating vector for state rewards... [1.2 MB]
Creating vector for inf... [1.2 MB]
Allocating iteration vectors... [2 x 1.2 MB]
TOTAL: [11.4 MB]

Starting iterations...

Iterative method: 48 iterations in 0.30 seconds (average 0.002083, setup 0.20)

Prob0A: 60 iterations in 0.16 seconds (average 0.002733, setup 0.00)

Prob1E: 4 iterations in 0.02 seconds (average 0.005000, setup 0.00)

yes = 207, no = 143941, maybe = 6875

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=151023, nc=12586, nnz=37076, k=2] [594.3 KB]
Creating vector for yes... [1.2 MB]
Allocating iteration vectors... [2 x 1.2 MB]
TOTAL: [4.0 MB]

Starting iterations...

Iterative method: 92 iterations in 0.11 seconds (average 0.000783, setup 0.04)

Value in the initial state: 0.0

Time for model checking: 0.674 seconds.

Result: 0.0 (value in the initial state)

---------------------------------------------------------------------

Note: There were 16 warnings during computation.

