PRISM
=====

Version: 4.3
Date: Wed Aug 03 16:11:41 BST 2016
Hostname: eglinton
Memory limits: cudd=1g, java(heap)=981.5m
Command line: prism uav_sim_mdp.prism -s uav_mdp.props -const 'Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=2,Objy1=1,Objx2=4,Objy2=1,'

Parsing model file "uav_sim_mdp.prism"...

Parsing properties file "uav_mdp.props"...

18 properties:
(1) Pmax=? [ F "MissionSuccessful" ]
(2) Pmin=? [ F "MissionSuccessful" ]
(3) Pmax=? [ F "MissionTimeUp" ]
(4) Pmin=? [ F "MissionTimeUp" ]
(5) Pmax=? [ F "Crash" ]
(6) Pmin=? [ F "Crash" ]
(7) Pmax=? [ F "AllAreaSearched" ]
(8) Pmin=? [ F "AllAreaSearched" ]
(9) Pmax=? [ F "MissionTimeUp"|"AllAreaSearched"|"Crash" ]
(10) Pmin=? [ F "MissionTimeUp"|"AllAreaSearched"|"Crash" ]
(11) R{"drop"}max=? [ F "deadlock" ]
(12) R{"drop"}min=? [ F "deadlock" ]
(13) R{"charge"}max=? [ F "deadlock" ]
(14) R{"charge"}min=? [ F "deadlock" ]
(15) R{"time"}max=? [ F "deadlock" ]
(16) R{"time"}min=? [ F "deadlock" ]
(17) R{"success"}max=? [ F "deadlock" ]/Pmin=? [ F "MissionSuccessful" ]
(18) R{"success"}min=? [ F "deadlock" ]/Pmax=? [ F "MissionSuccessful" ]

Type:        MDP
Modules:     Obj Move Robot T 
Variables:   NoOfObjs posx posy s1 retx rety b t c 

---------------------------------------------------------------------

Model checking: Pmax=? [ F "MissionSuccessful" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=2,Objy1=1,Objx2=4,Objy2=1

Building model...
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=2,Objy1=1,Objx2=4,Objy2=1

Warning: Guard for command 7 of module "Move" is never satisfied.

Warning: Guard for command 8 of module "Move" is never satisfied.

Warning: Guard for command 9 of module "Move" is never satisfied.

Warning: Guard for command 7 of module "Robot" is never satisfied.

Warning: Update 2 of command 3 of module "Robot" doesn't do anything ("(s1'=13)", line 210, column 44)

Warning: Update 2 of command 27 of module "T" doesn't do anything ("(c'=1)", line 323, column 108)

Warning: Guard for command 28 of module "T" is never satisfied.

Warning: Update 2 of command 9 of module "T" doesn't do anything ("(c'=1)", line 300, column 112)

Warning: Guard for command 10 of module "T" is never satisfied.

Warning: Update 2 of command 51 of module "T" doesn't do anything ("(c'=1)", line 354, column 113)

Warning: Guard for command 52 of module "T" is never satisfied.

Computing reachable states...

Reachability (BFS): 90 iterations in 1.21 seconds (average 0.013467, setup 0.00)

Time for model construction: 3.319 seconds.

Warning: Deadlocks detected and fixed in 1 states

Type:        MDP
States:      239387 (1 initial)
Transitions: 627897
Choices:     316309

Transition matrix: 215113 nodes (89 terminal), 627897 minterms, vars: 34r/34c/18nd

Prob0A: 42 iterations in 0.33 seconds (average 0.007905, setup 0.00)

Prob1E: 4 iterations in 0.06 seconds (average 0.015000, setup 0.00)

yes = 1142, no = 200576, maybe = 37669

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=239387, nc=68430, nnz=195258, k=2] [2.5 MB]
Creating vector for yes... [1.8 MB]
Allocating iteration vectors... [2 x 1.8 MB]
TOTAL: [8.0 MB]

Starting iterations...

Iterative method: 85 iterations in 0.38 seconds (average 0.001600, setup 0.24)

Value in the initial state: 0.9848408745191934

Time for model checking: 0.807 seconds.

Result: 0.9848408745191934 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmin=? [ F "MissionSuccessful" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=2,Objy1=1,Objx2=4,Objy2=1

Prob0E: 42 iterations in 0.38 seconds (average 0.008952, setup 0.00)

Prob1A: 3 iterations in 0.06 seconds (average 0.020000, setup 0.00)

yes = 1142, no = 205642, maybe = 32603

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=239387, nc=59019, nnz=167932, k=2] [2.2 MB]
Creating vector for yes... [1.8 MB]
Allocating iteration vectors... [2 x 1.8 MB]
TOTAL: [7.7 MB]

Starting iterations...

Iterative method: 83 iterations in 0.35 seconds (average 0.001590, setup 0.22)

Value in the initial state: 0.93273867600252

Time for model checking: 0.804 seconds.

Result: 0.93273867600252 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmax=? [ F "MissionTimeUp" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=2,Objy1=1,Objx2=4,Objy2=1

Prob0A: 72 iterations in 0.73 seconds (average 0.010111, setup 0.00)

Prob1E: 4 iterations in 0.05 seconds (average 0.013000, setup 0.00)

yes = 400, no = 154765, maybe = 84222

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=239387, nc=149000, nnz=411153, k=4] [5.1 MB]
Creating vector for yes... [1.8 MB]
Allocating iteration vectors... [2 x 1.8 MB]
TOTAL: [10.6 MB]

Starting iterations...

Iterative method: 96 iterations in 0.48 seconds (average 0.002000, setup 0.29)

Value in the initial state: 5.180856300119063E-5

Time for model checking: 1.295 seconds.

Result: 5.180856300119063E-5 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmin=? [ F "MissionTimeUp" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=2,Objy1=1,Objx2=4,Objy2=1

Prob0E: 86 iterations in 0.74 seconds (average 0.008558, setup 0.00)

Prob1A: 3 iterations in 0.04 seconds (average 0.014667, setup 0.00)

yes = 400, no = 155168, maybe = 83819

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=239387, nc=148284, nnz=409638, k=4] [5.1 MB]
Creating vector for yes... [1.8 MB]
Allocating iteration vectors... [2 x 1.8 MB]
TOTAL: [10.5 MB]

Starting iterations...

Iterative method: 113 iterations in 0.49 seconds (average 0.002053, setup 0.26)

Value in the initial state: 2.83750128579644E-8

Time for model checking: 1.297 seconds.

Result: 2.83750128579644E-8 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmax=? [ F "Crash" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=2,Objy1=1,Objx2=4,Objy2=1

Prob0A: 3 iterations in 0.08 seconds (average 0.026667, setup 0.00)

Prob1E: 161 iterations in 2.71 seconds (average 0.016820, setup 0.00)

yes = 117366, no = 3059, maybe = 118962

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=239387, nc=195884, nnz=507472, k=4] [6.2 MB]
Creating vector for yes... [1.8 MB]
Allocating iteration vectors... [2 x 1.8 MB]
TOTAL: [11.7 MB]

Starting iterations...

Iterative method: 86 iterations in 0.54 seconds (average 0.002233, setup 0.35)

Value in the initial state: 0.023152421589184315

Time for model checking: 3.354 seconds.

Result: 0.023152421589184315 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmin=? [ F "Crash" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=2,Objy1=1,Objx2=4,Objy2=1

Prob0E: 3 iterations in 0.07 seconds (average 0.022667, setup 0.00)

Prob1A: 43 iterations in 0.47 seconds (average 0.010977, setup 0.00)

yes = 117366, no = 3059, maybe = 118962

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=239387, nc=195884, nnz=507472, k=4] [6.2 MB]
Creating vector for yes... [1.8 MB]
Allocating iteration vectors... [2 x 1.8 MB]
TOTAL: [11.7 MB]

Starting iterations...

Iterative method: 77 iterations in 0.43 seconds (average 0.002286, setup 0.25)

Value in the initial state: 0.013117701662863041

Time for model checking: 0.987 seconds.

Result: 0.013117701662863041 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmax=? [ F "AllAreaSearched" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=2,Objy1=1,Objx2=4,Objy2=1

Prob0A: 54 iterations in 0.42 seconds (average 0.007778, setup 0.00)

Prob1E: 4 iterations in 0.07 seconds (average 0.017000, setup 0.00)

yes = 1516, no = 178472, maybe = 59399

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=239387, nc=90380, nnz=234691, k=2] [3.0 MB]
Creating vector for yes... [1.8 MB]
Allocating iteration vectors... [2 x 1.8 MB]
TOTAL: [8.5 MB]

Starting iterations...

Iterative method: 90 iterations in 0.38 seconds (average 0.001733, setup 0.22)

Value in the initial state: 0.0466309546004387

Time for model checking: 0.884 seconds.

Result: 0.0466309546004387 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmin=? [ F "AllAreaSearched" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=2,Objy1=1,Objx2=4,Objy2=1

Prob0E: 62 iterations in 0.46 seconds (average 0.007355, setup 0.00)

Prob1A: 3 iterations in 0.04 seconds (average 0.013333, setup 0.00)

yes = 1516, no = 184802, maybe = 53069

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=239387, nc=78832, nnz=200955, k=2] [2.6 MB]
Creating vector for yes... [1.8 MB]
Allocating iteration vectors... [2 x 1.8 MB]
TOTAL: [8.1 MB]

Starting iterations...

Iterative method: 96 iterations in 0.31 seconds (average 0.001750, setup 0.14)

Value in the initial state: 0.0018468071744077285

Time for model checking: 0.812 seconds.

Result: 0.0018468071744077285 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmax=? [ F "MissionTimeUp"|"AllAreaSearched"|"Crash" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=2,Objy1=1,Objx2=4,Objy2=1

Prob0A: 3 iterations in 0.12 seconds (average 0.041333, setup 0.00)

Prob1E: 160 iterations in 2.59 seconds (average 0.016175, setup 0.00)

yes = 205641, no = 1143, maybe = 32603

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=239387, nc=59019, nnz=167932, k=2] [2.2 MB]
Creating vector for yes... [1.8 MB]
Allocating iteration vectors... [2 x 1.8 MB]
TOTAL: [7.7 MB]

Starting iterations...

Iterative method: 77 iterations in 0.34 seconds (average 0.001610, setup 0.21)

Value in the initial state: 0.06726108382639578

Time for model checking: 3.049 seconds.

Result: 0.06726108382639578 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmin=? [ F "MissionTimeUp"|"AllAreaSearched"|"Crash" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=2,Objy1=1,Objx2=4,Objy2=1

Prob0E: 3 iterations in 0.05 seconds (average 0.017333, setup 0.00)

Prob1A: 42 iterations in 0.50 seconds (average 0.011810, setup 0.00)

yes = 200575, no = 1143, maybe = 37669

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=239387, nc=68430, nnz=195258, k=2] [2.5 MB]
Creating vector for yes... [1.8 MB]
Allocating iteration vectors... [2 x 1.8 MB]
TOTAL: [8.0 MB]

Starting iterations...

Iterative method: 84 iterations in 0.32 seconds (average 0.001619, setup 0.18)

Value in the initial state: 0.015157482100779433

Time for model checking: 0.906 seconds.

Result: 0.015157482100779433 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"drop"}max=? [ F "deadlock" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=2,Objy1=1,Objx2=4,Objy2=1

Prob0E: 4 iterations in 0.07 seconds (average 0.018000, setup 0.00)

Prob1A: 1 iterations in 0.01 seconds (average 0.008000, setup 0.00)

goal = 1, inf = 0, maybe = 239386

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=239387, nc=316308, nnz=627896, k=4] [9.3 MB]
Building sparse matrix (transition rewards)... [n=239387, nc=316308, nnz=0, k=4] [2.1 MB]
Creating vector for state rewards... [1.8 MB]
Creating vector for inf... [1.8 MB]
Allocating iteration vectors... [2 x 1.8 MB]
TOTAL: [18.7 MB]

Starting iterations...

Iterative method: 81 iterations in 0.73 seconds (average 0.003506, setup 0.44)

Value in the initial state: 0.17533850604675277

Time for model checking: 0.829 seconds.

Result: 0.17533850604675277 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"drop"}min=? [ F "deadlock" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=2,Objy1=1,Objx2=4,Objy2=1

Prob0A: 4 iterations in 0.05 seconds (average 0.013000, setup 0.00)

Prob1E: 5 iterations in 0.06 seconds (average 0.012000, setup 0.00)

Warning: PRISM hasn't checked for zero-reward loops.
Your minimum rewards may be too low...

goal = 1, inf = 0, maybe = 239386

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=239387, nc=316308, nnz=627896, k=4] [9.3 MB]
Building sparse matrix (transition rewards)... [n=239387, nc=316308, nnz=0, k=4] [2.1 MB]
Creating vector for state rewards... [1.8 MB]
Creating vector for inf... [1.8 MB]
Allocating iteration vectors... [2 x 1.8 MB]
TOTAL: [18.7 MB]

Starting iterations...

Iterative method: 74 iterations in 0.60 seconds (average 0.003514, setup 0.34)

Value in the initial state: 0.16247238903912006

Time for model checking: 0.714 seconds.

Result: 0.16247238903912006 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"charge"}max=? [ F "deadlock" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=2,Objy1=1,Objx2=4,Objy2=1

Prob0E: 4 iterations in 0.03 seconds (average 0.008000, setup 0.00)

Prob1A: 1 iterations in 0.00 seconds (average 0.004000, setup 0.00)

goal = 1, inf = 0, maybe = 239386

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=239387, nc=316308, nnz=627896, k=4] [9.3 MB]
Building sparse matrix (transition rewards)... [n=239387, nc=316308, nnz=6589, k=4] [2.2 MB]
Creating vector for state rewards... [1.8 MB]
Creating vector for inf... [1.8 MB]
Allocating iteration vectors... [2 x 1.8 MB]
TOTAL: [18.8 MB]

Starting iterations...

Iterative method: 82 iterations in 0.62 seconds (average 0.003512, setup 0.33)

Value in the initial state: 0.1618117296468788

Time for model checking: 0.668 seconds.

Result: 0.1618117296468788 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"charge"}min=? [ F "deadlock" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=2,Objy1=1,Objx2=4,Objy2=1

Prob0A: 4 iterations in 0.03 seconds (average 0.008000, setup 0.00)

Prob1E: 5 iterations in 0.06 seconds (average 0.012000, setup 0.00)

Warning: PRISM hasn't checked for zero-reward loops.
Your minimum rewards may be too low...

goal = 1, inf = 0, maybe = 239386

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=239387, nc=316308, nnz=627896, k=4] [9.3 MB]
Building sparse matrix (transition rewards)... [n=239387, nc=316308, nnz=6589, k=4] [2.2 MB]
Creating vector for state rewards... [1.8 MB]
Creating vector for inf... [1.8 MB]
Allocating iteration vectors... [2 x 1.8 MB]
TOTAL: [18.8 MB]

Starting iterations...

Iterative method: 92 iterations in 0.65 seconds (average 0.003478, setup 0.33)

Value in the initial state: 0.006725133273768489

Time for model checking: 0.764 seconds.

Result: 0.006725133273768489 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"time"}max=? [ F "deadlock" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=2,Objy1=1,Objx2=4,Objy2=1

Prob0E: 4 iterations in 0.03 seconds (average 0.007000, setup 0.00)

Prob1A: 1 iterations in 0.01 seconds (average 0.008000, setup 0.00)

goal = 1, inf = 0, maybe = 239386

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=239387, nc=316308, nnz=627896, k=4] [9.3 MB]
Building sparse matrix (transition rewards)... [n=239387, nc=316308, nnz=120423, k=4] [3.5 MB]
Creating vector for state rewards... [1.8 MB]
Creating vector for inf... [1.8 MB]
Allocating iteration vectors... [2 x 1.8 MB]
TOTAL: [20.1 MB]

Starting iterations...

Iterative method: 92 iterations in 0.69 seconds (average 0.003435, setup 0.37)

Value in the initial state: 128.00388109589838

Time for model checking: 0.753 seconds.

Result: 128.00388109589838 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"time"}min=? [ F "deadlock" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=2,Objy1=1,Objx2=4,Objy2=1

Prob0A: 4 iterations in 0.03 seconds (average 0.007000, setup 0.00)

Prob1E: 5 iterations in 0.06 seconds (average 0.011200, setup 0.00)

Warning: PRISM hasn't checked for zero-reward loops.
Your minimum rewards may be too low...

goal = 1, inf = 0, maybe = 239386

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=239387, nc=316308, nnz=627896, k=4] [9.3 MB]
Building sparse matrix (transition rewards)... [n=239387, nc=316308, nnz=120423, k=4] [3.5 MB]
Creating vector for state rewards... [1.8 MB]
Creating vector for inf... [1.8 MB]
Allocating iteration vectors... [2 x 1.8 MB]
TOTAL: [20.1 MB]

Starting iterations...

Iterative method: 90 iterations in 0.70 seconds (average 0.003600, setup 0.38)

Value in the initial state: 72.82286368599007

Time for model checking: 0.798 seconds.

Result: 72.82286368599007 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"success"}max=? [ F "deadlock" ]/Pmin=? [ F "MissionSuccessful" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=2,Objy1=1,Objx2=4,Objy2=1

Prob0E: 4 iterations in 0.03 seconds (average 0.007000, setup 0.00)

Prob1A: 1 iterations in 0.01 seconds (average 0.008000, setup 0.00)

goal = 1, inf = 0, maybe = 239386

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=239387, nc=316308, nnz=627896, k=4] [9.3 MB]
Building sparse matrix (transition rewards)... [n=239387, nc=316308, nnz=2252, k=4] [2.1 MB]
Creating vector for state rewards... [1.8 MB]
Creating vector for inf... [1.8 MB]
Allocating iteration vectors... [2 x 1.8 MB]
TOTAL: [18.8 MB]

Starting iterations...

Iterative method: 89 iterations in 0.64 seconds (average 0.003461, setup 0.33)

Prob0E: 42 iterations in 0.38 seconds (average 0.008952, setup 0.00)

Prob1A: 3 iterations in 0.05 seconds (average 0.016000, setup 0.00)

yes = 1142, no = 205642, maybe = 32603

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=239387, nc=59019, nnz=167932, k=2] [2.2 MB]
Creating vector for yes... [1.8 MB]
Allocating iteration vectors... [2 x 1.8 MB]
TOTAL: [7.7 MB]

Starting iterations...

Iterative method: 83 iterations in 0.30 seconds (average 0.001542, setup 0.18)

Value in the initial state: 134.85646980989245

Time for model checking: 1.429 seconds.

Result: 134.85646980989245 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"success"}min=? [ F "deadlock" ]/Pmax=? [ F "MissionSuccessful" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=2,Objy1=1,Objx2=4,Objy2=1

Prob0A: 4 iterations in 0.10 seconds (average 0.026000, setup 0.00)

Prob1E: 5 iterations in 0.04 seconds (average 0.008800, setup 0.00)

Warning: PRISM hasn't checked for zero-reward loops.
Your minimum rewards may be too low...

goal = 1, inf = 0, maybe = 239386

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=239387, nc=316308, nnz=627896, k=4] [9.3 MB]
Building sparse matrix (transition rewards)... [n=239387, nc=316308, nnz=2252, k=4] [2.1 MB]
Creating vector for state rewards... [1.8 MB]
Creating vector for inf... [1.8 MB]
Allocating iteration vectors... [2 x 1.8 MB]
TOTAL: [18.8 MB]

Starting iterations...

Iterative method: 87 iterations in 0.68 seconds (average 0.003494, setup 0.38)

Prob0A: 42 iterations in 0.24 seconds (average 0.005619, setup 0.00)

Prob1E: 4 iterations in 0.04 seconds (average 0.011000, setup 0.00)

yes = 1142, no = 200576, maybe = 37669

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=239387, nc=68430, nnz=195258, k=2] [2.5 MB]
Creating vector for yes... [1.8 MB]
Allocating iteration vectors... [2 x 1.8 MB]
TOTAL: [8.0 MB]

Starting iterations...

Iterative method: 85 iterations in 0.33 seconds (average 0.001647, setup 0.19)

Value in the initial state: 70.89859243811566

Time for model checking: 1.452 seconds.

Result: 70.89859243811566 (value in the initial state)

---------------------------------------------------------------------

Note: There were 16 warnings during computation.

