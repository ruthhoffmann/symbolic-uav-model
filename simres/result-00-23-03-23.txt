PRISM
=====

Version: 4.3
Date: Wed Aug 03 15:53:27 BST 2016
Hostname: eglinton
Memory limits: cudd=1g, java(heap)=981.5m
Command line: prism uav_sim_mdp.prism -s uav_mdp.props -const 'Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=0,Objy1=3,Objx2=2,Objy2=3,'

Parsing model file "uav_sim_mdp.prism"...

Parsing properties file "uav_mdp.props"...

18 properties:
(1) Pmax=? [ F "MissionSuccessful" ]
(2) Pmin=? [ F "MissionSuccessful" ]
(3) Pmax=? [ F "MissionTimeUp" ]
(4) Pmin=? [ F "MissionTimeUp" ]
(5) Pmax=? [ F "Crash" ]
(6) Pmin=? [ F "Crash" ]
(7) Pmax=? [ F "AllAreaSearched" ]
(8) Pmin=? [ F "AllAreaSearched" ]
(9) Pmax=? [ F "MissionTimeUp"|"AllAreaSearched"|"Crash" ]
(10) Pmin=? [ F "MissionTimeUp"|"AllAreaSearched"|"Crash" ]
(11) R{"drop"}max=? [ F "deadlock" ]
(12) R{"drop"}min=? [ F "deadlock" ]
(13) R{"charge"}max=? [ F "deadlock" ]
(14) R{"charge"}min=? [ F "deadlock" ]
(15) R{"time"}max=? [ F "deadlock" ]
(16) R{"time"}min=? [ F "deadlock" ]
(17) R{"success"}max=? [ F "deadlock" ]/Pmin=? [ F "MissionSuccessful" ]
(18) R{"success"}min=? [ F "deadlock" ]/Pmax=? [ F "MissionSuccessful" ]

Type:        MDP
Modules:     Obj Move Robot T 
Variables:   NoOfObjs posx posy s1 retx rety b t c 

---------------------------------------------------------------------

Model checking: Pmax=? [ F "MissionSuccessful" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=0,Objy1=3,Objx2=2,Objy2=3

Building model...
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=0,Objy1=3,Objx2=2,Objy2=3

Warning: Guard for command 7 of module "Move" is never satisfied.

Warning: Guard for command 8 of module "Move" is never satisfied.

Warning: Guard for command 9 of module "Move" is never satisfied.

Warning: Guard for command 5 of module "Robot" is never satisfied.

Warning: Guard for command 6 of module "Robot" is never satisfied.

Warning: Update 2 of command 3 of module "Robot" doesn't do anything ("(s1'=13)", line 210, column 44)

Warning: Update 2 of command 27 of module "T" doesn't do anything ("(c'=1)", line 323, column 108)

Warning: Guard for command 28 of module "T" is never satisfied.

Warning: Update 2 of command 9 of module "T" doesn't do anything ("(c'=1)", line 300, column 112)

Warning: Guard for command 10 of module "T" is never satisfied.

Warning: Update 2 of command 51 of module "T" doesn't do anything ("(c'=1)", line 354, column 113)

Warning: Guard for command 52 of module "T" is never satisfied.

Computing reachable states...

Reachability (BFS): 58 iterations in 0.09 seconds (average 0.001586, setup 0.00)

Time for model construction: 2.283 seconds.

Warning: Deadlocks detected and fixed in 1 states

Type:        MDP
States:      4079 (1 initial)
Transitions: 12108
Choices:     5789

Transition matrix: 47728 nodes (61 terminal), 12108 minterms, vars: 34r/34c/18nd

Prob0A: 57 iterations in 0.07 seconds (average 0.001193, setup 0.00)

Prob1E: 4 iterations in 0.01 seconds (average 0.003000, setup 0.00)

yes = 19, no = 3509, maybe = 551

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=4079, nc=926, nnz=2505, k=2] [34.2 KB]
Creating vector for yes... [31.9 KB]
Allocating iteration vectors... [2 x 31.9 KB]
TOTAL: [129.8 KB]

Starting iterations...

Iterative method: 64 iterations in 0.03 seconds (average 0.000000, setup 0.03)

Value in the initial state: 0.9127698828746527

Time for model checking: 0.115 seconds.

Result: 0.9127698828746527 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmin=? [ F "MissionSuccessful" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=0,Objy1=3,Objx2=2,Objy2=3

Prob0E: 57 iterations in 0.05 seconds (average 0.000912, setup 0.00)

Prob1A: 3 iterations in 0.01 seconds (average 0.002667, setup 0.00)

yes = 19, no = 3699, maybe = 361

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=4079, nc=573, nnz=1471, k=2] [21.8 KB]
Creating vector for yes... [31.9 KB]
Allocating iteration vectors... [2 x 31.9 KB]
TOTAL: [117.4 KB]

Starting iterations...

Iterative method: 57 iterations in 0.01 seconds (average 0.000000, setup 0.01)

Value in the initial state: 0.7074805693705843

Time for model checking: 0.072 seconds.

Result: 0.7074805693705843 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmax=? [ F "MissionTimeUp" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=0,Objy1=3,Objx2=2,Objy2=3

yes = 0, no = 4079, maybe = 0

Value in the initial state: 0.0

Time for model checking: 0.002 seconds.

Result: 0.0 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmin=? [ F "MissionTimeUp" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=0,Objy1=3,Objx2=2,Objy2=3

yes = 0, no = 4079, maybe = 0

Value in the initial state: 0.0

Time for model checking: 0.0 seconds.

Result: 0.0 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmax=? [ F "Crash" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=0,Objy1=3,Objx2=2,Objy2=3

Prob0A: 3 iterations in 0.02 seconds (average 0.006667, setup 0.00)

Prob1E: 188 iterations in 0.40 seconds (average 0.002149, setup 0.00)

yes = 1953, no = 40, maybe = 2086

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=4079, nc=3796, nnz=10115, k=2] [126.2 KB]
Creating vector for yes... [31.9 KB]
Allocating iteration vectors... [2 x 31.9 KB]
TOTAL: [221.8 KB]

Starting iterations...

Iterative method: 62 iterations in 0.04 seconds (average 0.000000, setup 0.04)

Value in the initial state: 0.01814239320701085

Time for model checking: 0.494 seconds.

Result: 0.01814239320701085 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmin=? [ F "Crash" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=0,Objy1=3,Objx2=2,Objy2=3

Prob0E: 4 iterations in 0.01 seconds (average 0.003000, setup 0.00)

Prob1A: 52 iterations in 0.08 seconds (average 0.001462, setup 0.00)

yes = 1953, no = 40, maybe = 2086

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=4079, nc=3796, nnz=10115, k=2] [126.2 KB]
Creating vector for yes... [31.9 KB]
Allocating iteration vectors... [2 x 31.9 KB]
TOTAL: [221.8 KB]

Starting iterations...

Iterative method: 65 iterations in 0.02 seconds (average 0.000000, setup 0.02)

Value in the initial state: 0.015528771548834154

Time for model checking: 0.107 seconds.

Result: 0.015528771548834154 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmax=? [ F "AllAreaSearched" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=0,Objy1=3,Objx2=2,Objy2=3

Prob0A: 52 iterations in 0.07 seconds (average 0.001385, setup 0.00)

Prob1E: 4 iterations in 0.01 seconds (average 0.003000, setup 0.00)

yes = 20, no = 2041, maybe = 2018

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=4079, nc=3661, nnz=9862, k=2] [123.1 KB]
Creating vector for yes... [31.9 KB]
Allocating iteration vectors... [2 x 31.9 KB]
TOTAL: [218.7 KB]

Starting iterations...

Iterative method: 59 iterations in 0.02 seconds (average 0.000000, setup 0.02)

Value in the initial state: 0.27461917125682606

Time for model checking: 0.104 seconds.

Result: 0.27461917125682606 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmin=? [ F "AllAreaSearched" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=0,Objy1=3,Objx2=2,Objy2=3

Prob0E: 57 iterations in 0.09 seconds (average 0.001544, setup 0.00)

Prob1A: 3 iterations in 0.01 seconds (average 0.002667, setup 0.00)

yes = 20, no = 2041, maybe = 2018

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=4079, nc=3661, nnz=9862, k=2] [123.1 KB]
Creating vector for yes... [31.9 KB]
Allocating iteration vectors... [2 x 31.9 KB]
TOTAL: [218.7 KB]

Starting iterations...

Iterative method: 66 iterations in 0.02 seconds (average 0.000061, setup 0.01)

Value in the initial state: 0.071635738351227

Time for model checking: 0.115 seconds.

Result: 0.071635738351227 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmax=? [ F "MissionTimeUp"|"AllAreaSearched"|"Crash" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=0,Objy1=3,Objx2=2,Objy2=3

Prob0A: 3 iterations in 0.01 seconds (average 0.004000, setup 0.00)

Prob1E: 220 iterations in 0.48 seconds (average 0.002182, setup 0.00)

yes = 3698, no = 20, maybe = 361

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=4079, nc=573, nnz=1471, k=2] [21.8 KB]
Creating vector for yes... [31.9 KB]
Allocating iteration vectors... [2 x 31.9 KB]
TOTAL: [117.4 KB]

Starting iterations...

Iterative method: 57 iterations in 0.02 seconds (average 0.000070, setup 0.01)

Value in the initial state: 0.2925194306294155

Time for model checking: 0.621 seconds.

Result: 0.2925194306294155 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmin=? [ F "MissionTimeUp"|"AllAreaSearched"|"Crash" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=0,Objy1=3,Objx2=2,Objy2=3

Prob0E: 4 iterations in 0.01 seconds (average 0.002000, setup 0.00)

Prob1A: 57 iterations in 0.08 seconds (average 0.001333, setup 0.00)

yes = 3508, no = 20, maybe = 551

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=4079, nc=926, nnz=2505, k=2] [34.2 KB]
Creating vector for yes... [31.9 KB]
Allocating iteration vectors... [2 x 31.9 KB]
TOTAL: [129.8 KB]

Starting iterations...

Iterative method: 64 iterations in 0.02 seconds (average 0.000000, setup 0.02)

Value in the initial state: 0.08723011712534756

Time for model checking: 0.13 seconds.

Result: 0.08723011712534756 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"drop"}max=? [ F "deadlock" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=0,Objy1=3,Objx2=2,Objy2=3

Prob0E: 5 iterations in 0.01 seconds (average 0.001600, setup 0.00)

Prob1A: 1 iterations in 0.00 seconds (average 0.004000, setup 0.00)

goal = 1, inf = 0, maybe = 4078

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=4079, nc=5788, nnz=12107, k=2] [180.4 KB]
Building sparse matrix (transition rewards)... [n=4079, nc=5788, nnz=0, k=2] [38.5 KB]
Creating vector for state rewards... [31.9 KB]
Creating vector for inf... [31.9 KB]
Allocating iteration vectors... [2 x 31.9 KB]
TOTAL: [346.4 KB]

Starting iterations...

Iterative method: 61 iterations in 0.04 seconds (average 0.000066, setup 0.04)

Value in the initial state: 0.16104662921355747

Time for model checking: 0.053 seconds.

Result: 0.16104662921355747 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"drop"}min=? [ F "deadlock" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=0,Objy1=3,Objx2=2,Objy2=3

Prob0A: 4 iterations in 0.00 seconds (average 0.001000, setup 0.00)

Prob1E: 5 iterations in 0.01 seconds (average 0.002400, setup 0.00)

Warning: PRISM hasn't checked for zero-reward loops.
Your minimum rewards may be too low...

goal = 1, inf = 0, maybe = 4078

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=4079, nc=5788, nnz=12107, k=2] [180.4 KB]
Building sparse matrix (transition rewards)... [n=4079, nc=5788, nnz=0, k=2] [38.5 KB]
Creating vector for state rewards... [31.9 KB]
Creating vector for inf... [31.9 KB]
Allocating iteration vectors... [2 x 31.9 KB]
TOTAL: [346.4 KB]

Starting iterations...

Iterative method: 55 iterations in 0.02 seconds (average 0.000073, setup 0.02)

Value in the initial state: 0.14463793791243873

Time for model checking: 0.041 seconds.

Result: 0.14463793791243873 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"charge"}max=? [ F "deadlock" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=0,Objy1=3,Objx2=2,Objy2=3

Prob0E: 5 iterations in 0.01 seconds (average 0.001600, setup 0.00)

Prob1A: 1 iterations in 0.00 seconds (average 0.000000, setup 0.00)

goal = 1, inf = 0, maybe = 4078

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=4079, nc=5788, nnz=12107, k=2] [180.4 KB]
Building sparse matrix (transition rewards)... [n=4079, nc=5788, nnz=309, k=2] [42.2 KB]
Creating vector for state rewards... [31.9 KB]
Creating vector for inf... [31.9 KB]
Allocating iteration vectors... [2 x 31.9 KB]
TOTAL: [350.1 KB]

Starting iterations...

Iterative method: 59 iterations in 0.02 seconds (average 0.000068, setup 0.02)

Value in the initial state: 0.2749168617918056

Time for model checking: 0.033 seconds.

Result: 0.2749168617918056 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"charge"}min=? [ F "deadlock" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=0,Objy1=3,Objx2=2,Objy2=3

Prob0A: 4 iterations in 0.00 seconds (average 0.001000, setup 0.00)

Prob1E: 5 iterations in 0.01 seconds (average 0.001600, setup 0.00)

Warning: PRISM hasn't checked for zero-reward loops.
Your minimum rewards may be too low...

goal = 1, inf = 0, maybe = 4078

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=4079, nc=5788, nnz=12107, k=2] [180.4 KB]
Building sparse matrix (transition rewards)... [n=4079, nc=5788, nnz=309, k=2] [42.2 KB]
Creating vector for state rewards... [31.9 KB]
Creating vector for inf... [31.9 KB]
Allocating iteration vectors... [2 x 31.9 KB]
TOTAL: [350.1 KB]

Starting iterations...

Iterative method: 63 iterations in 0.02 seconds (average 0.000063, setup 0.02)

Value in the initial state: 0.06511794349498552

Time for model checking: 0.041 seconds.

Result: 0.06511794349498552 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"time"}max=? [ F "deadlock" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=0,Objy1=3,Objx2=2,Objy2=3

Prob0E: 5 iterations in 0.00 seconds (average 0.000800, setup 0.00)

Prob1A: 1 iterations in 0.00 seconds (average 0.004000, setup 0.00)

goal = 1, inf = 0, maybe = 4078

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=4079, nc=5788, nnz=12107, k=2] [180.4 KB]
Building sparse matrix (transition rewards)... [n=4079, nc=5788, nnz=1991, k=2] [61.9 KB]
Creating vector for state rewards... [31.9 KB]
Creating vector for inf... [31.9 KB]
Allocating iteration vectors... [2 x 31.9 KB]
TOTAL: [369.8 KB]

Starting iterations...

Iterative method: 63 iterations in 0.03 seconds (average 0.000063, setup 0.02)

Value in the initial state: 100.75012092682992

Time for model checking: 0.037 seconds.

Result: 100.75012092682992 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"time"}min=? [ F "deadlock" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=0,Objy1=3,Objx2=2,Objy2=3

Prob0A: 4 iterations in 0.00 seconds (average 0.001000, setup 0.00)

Prob1E: 5 iterations in 0.01 seconds (average 0.001600, setup 0.00)

Warning: PRISM hasn't checked for zero-reward loops.
Your minimum rewards may be too low...

goal = 1, inf = 0, maybe = 4078

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=4079, nc=5788, nnz=12107, k=2] [180.4 KB]
Building sparse matrix (transition rewards)... [n=4079, nc=5788, nnz=1991, k=2] [61.9 KB]
Creating vector for state rewards... [31.9 KB]
Creating vector for inf... [31.9 KB]
Allocating iteration vectors... [2 x 31.9 KB]
TOTAL: [369.8 KB]

Starting iterations...

Iterative method: 67 iterations in 0.02 seconds (average 0.000060, setup 0.02)

Value in the initial state: 86.23971974560445

Time for model checking: 0.042 seconds.

Result: 86.23971974560445 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"success"}max=? [ F "deadlock" ]/Pmin=? [ F "MissionSuccessful" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=0,Objy1=3,Objx2=2,Objy2=3

Prob0E: 5 iterations in 0.00 seconds (average 0.000800, setup 0.00)

Prob1A: 1 iterations in 0.00 seconds (average 0.000000, setup 0.00)

goal = 1, inf = 0, maybe = 4078

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=4079, nc=5788, nnz=12107, k=2] [180.4 KB]
Building sparse matrix (transition rewards)... [n=4079, nc=5788, nnz=37, k=2] [39.0 KB]
Creating vector for state rewards... [31.9 KB]
Creating vector for inf... [31.9 KB]
Allocating iteration vectors... [2 x 31.9 KB]
TOTAL: [346.9 KB]

Starting iterations...

Iterative method: 65 iterations in 0.02 seconds (average 0.000062, setup 0.02)

Prob0E: 57 iterations in 0.05 seconds (average 0.000912, setup 0.00)

Prob1A: 3 iterations in 0.01 seconds (average 0.002667, setup 0.00)

yes = 19, no = 3699, maybe = 361

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=4079, nc=573, nnz=1471, k=2] [21.8 KB]
Creating vector for yes... [31.9 KB]
Allocating iteration vectors... [2 x 31.9 KB]
TOTAL: [117.4 KB]

Starting iterations...

Iterative method: 57 iterations in 0.01 seconds (average 0.000070, setup 0.00)

Value in the initial state: 120.03229015515522

Time for model checking: 0.105 seconds.

Result: 120.03229015515522 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"success"}min=? [ F "deadlock" ]/Pmax=? [ F "MissionSuccessful" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=0,Objy1=3,Objx2=2,Objy2=3

Prob0A: 4 iterations in 0.00 seconds (average 0.001000, setup 0.00)

Prob1E: 5 iterations in 0.01 seconds (average 0.001600, setup 0.00)

Warning: PRISM hasn't checked for zero-reward loops.
Your minimum rewards may be too low...

goal = 1, inf = 0, maybe = 4078

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=4079, nc=5788, nnz=12107, k=2] [180.4 KB]
Building sparse matrix (transition rewards)... [n=4079, nc=5788, nnz=37, k=2] [39.0 KB]
Creating vector for state rewards... [31.9 KB]
Creating vector for inf... [31.9 KB]
Allocating iteration vectors... [2 x 31.9 KB]
TOTAL: [346.9 KB]

Starting iterations...

Iterative method: 61 iterations in 0.02 seconds (average 0.000066, setup 0.02)

Prob0A: 57 iterations in 0.06 seconds (average 0.001053, setup 0.00)

Prob1E: 4 iterations in 0.01 seconds (average 0.002000, setup 0.00)

yes = 19, no = 3509, maybe = 551

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=4079, nc=926, nnz=2505, k=2] [34.2 KB]
Creating vector for yes... [31.9 KB]
Allocating iteration vectors... [2 x 31.9 KB]
TOTAL: [129.8 KB]

Starting iterations...

Iterative method: 64 iterations in 0.01 seconds (average 0.000063, setup 0.01)

Value in the initial state: 67.65059530451309

Time for model checking: 0.145 seconds.

Result: 67.65059530451309 (value in the initial state)

---------------------------------------------------------------------

Note: There were 17 warnings during computation.

