PRISM
=====

Version: 4.3
Date: Wed Aug 03 15:52:06 BST 2016
Hostname: eglinton
Memory limits: cudd=1g, java(heap)=981.5m
Command line: prism uav_sim_mdp.prism -s uav_mdp.props -const 'Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=0,Objy1=1,Objx2=3,Objy2=1,'

Parsing model file "uav_sim_mdp.prism"...

Parsing properties file "uav_mdp.props"...

18 properties:
(1) Pmax=? [ F "MissionSuccessful" ]
(2) Pmin=? [ F "MissionSuccessful" ]
(3) Pmax=? [ F "MissionTimeUp" ]
(4) Pmin=? [ F "MissionTimeUp" ]
(5) Pmax=? [ F "Crash" ]
(6) Pmin=? [ F "Crash" ]
(7) Pmax=? [ F "AllAreaSearched" ]
(8) Pmin=? [ F "AllAreaSearched" ]
(9) Pmax=? [ F "MissionTimeUp"|"AllAreaSearched"|"Crash" ]
(10) Pmin=? [ F "MissionTimeUp"|"AllAreaSearched"|"Crash" ]
(11) R{"drop"}max=? [ F "deadlock" ]
(12) R{"drop"}min=? [ F "deadlock" ]
(13) R{"charge"}max=? [ F "deadlock" ]
(14) R{"charge"}min=? [ F "deadlock" ]
(15) R{"time"}max=? [ F "deadlock" ]
(16) R{"time"}min=? [ F "deadlock" ]
(17) R{"success"}max=? [ F "deadlock" ]/Pmin=? [ F "MissionSuccessful" ]
(18) R{"success"}min=? [ F "deadlock" ]/Pmax=? [ F "MissionSuccessful" ]

Type:        MDP
Modules:     Obj Move Robot T 
Variables:   NoOfObjs posx posy s1 retx rety b t c 

---------------------------------------------------------------------

Model checking: Pmax=? [ F "MissionSuccessful" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=0,Objy1=1,Objx2=3,Objy2=1

Building model...
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=0,Objy1=1,Objx2=3,Objy2=1

Warning: Guard for command 4 of module "Move" is never satisfied.

Warning: Guard for command 7 of module "Move" is never satisfied.

Warning: Guard for command 8 of module "Move" is never satisfied.

Warning: Guard for command 9 of module "Move" is never satisfied.

Warning: Guard for command 7 of module "Robot" is never satisfied.

Warning: Update 2 of command 3 of module "Robot" doesn't do anything ("(s1'=13)", line 210, column 44)

Warning: Update 2 of command 27 of module "T" doesn't do anything ("(c'=1)", line 323, column 108)

Warning: Guard for command 28 of module "T" is never satisfied.

Warning: Update 2 of command 9 of module "T" doesn't do anything ("(c'=1)", line 300, column 112)

Warning: Guard for command 10 of module "T" is never satisfied.

Warning: Update 2 of command 51 of module "T" doesn't do anything ("(c'=1)", line 354, column 113)

Warning: Guard for command 52 of module "T" is never satisfied.

Computing reachable states...

Reachability (BFS): 90 iterations in 1.22 seconds (average 0.013556, setup 0.00)

Time for model construction: 4.03 seconds.

Warning: Deadlocks detected and fixed in 1 states

Type:        MDP
States:      236016 (1 initial)
Transitions: 622378
Choices:     312737

Transition matrix: 214024 nodes (91 terminal), 622378 minterms, vars: 34r/34c/18nd

Prob0A: 43 iterations in 0.34 seconds (average 0.007907, setup 0.00)

Prob1E: 4 iterations in 0.06 seconds (average 0.014000, setup 0.00)

yes = 1141, no = 196995, maybe = 37880

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=236016, nc=68372, nnz=194560, k=2] [2.5 MB]
Creating vector for yes... [1.8 MB]
Allocating iteration vectors... [2 x 1.8 MB]
TOTAL: [7.9 MB]

Starting iterations...

Iterative method: 87 iterations in 0.39 seconds (average 0.001655, setup 0.25)

Value in the initial state: 0.9846076358882441

Time for model checking: 1.377 seconds.

Result: 0.9846076358882441 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmin=? [ F "MissionSuccessful" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=0,Objy1=1,Objx2=3,Objy2=1

Prob0E: 43 iterations in 0.35 seconds (average 0.008186, setup 0.00)

Prob1A: 3 iterations in 0.05 seconds (average 0.017333, setup 0.00)

yes = 1141, no = 201859, maybe = 33016

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=236016, nc=59401, nnz=168639, k=2] [2.2 MB]
Creating vector for yes... [1.8 MB]
Allocating iteration vectors... [2 x 1.8 MB]
TOTAL: [7.6 MB]

Starting iterations...

Iterative method: 85 iterations in 0.34 seconds (average 0.001600, setup 0.20)

Value in the initial state: 0.9326072984257112

Time for model checking: 0.886 seconds.

Result: 0.9326072984257112 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmax=? [ F "MissionTimeUp" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=0,Objy1=1,Objx2=3,Objy2=1

Prob0A: 73 iterations in 0.74 seconds (average 0.010137, setup 0.00)

Prob1E: 4 iterations in 0.06 seconds (average 0.015000, setup 0.00)

yes = 392, no = 150939, maybe = 84685

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=236016, nc=149205, nnz=411357, k=4] [5.1 MB]
Creating vector for yes... [1.8 MB]
Allocating iteration vectors... [2 x 1.8 MB]
TOTAL: [10.5 MB]

Starting iterations...

Iterative method: 96 iterations in 0.58 seconds (average 0.002083, setup 0.38)

Value in the initial state: 5.201512420537015E-5

Time for model checking: 2.262 seconds.

Result: 5.201512420537015E-5 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmin=? [ F "MissionTimeUp" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=0,Objy1=1,Objx2=3,Objy2=1

Prob0E: 87 iterations in 0.77 seconds (average 0.008874, setup 0.00)

Prob1A: 3 iterations in 0.06 seconds (average 0.020000, setup 0.00)

yes = 392, no = 151342, maybe = 84282

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=236016, nc=148489, nnz=409842, k=4] [5.1 MB]
Creating vector for yes... [1.8 MB]
Allocating iteration vectors... [2 x 1.8 MB]
TOTAL: [10.5 MB]

Starting iterations...

Iterative method: 113 iterations in 0.51 seconds (average 0.002053, setup 0.28)

Value in the initial state: 3.288700447133299E-8

Time for model checking: 1.96 seconds.

Result: 3.288700447133299E-8 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmax=? [ F "Crash" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=0,Objy1=1,Objx2=3,Objy2=1

Prob0A: 3 iterations in 0.09 seconds (average 0.029333, setup 0.00)

Prob1E: 165 iterations in 2.75 seconds (average 0.016679, setup 0.00)

yes = 115749, no = 3007, maybe = 117260

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=236016, nc=193981, nnz=503622, k=4] [6.2 MB]
Creating vector for yes... [1.8 MB]
Allocating iteration vectors... [2 x 1.8 MB]
TOTAL: [11.6 MB]

Starting iterations...

Iterative method: 88 iterations in 0.56 seconds (average 0.002273, setup 0.36)

Value in the initial state: 0.023110355715291554

Time for model checking: 4.679 seconds.

Result: 0.023110355715291554 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmin=? [ F "Crash" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=0,Objy1=1,Objx2=3,Objy2=1

Prob0E: 4 iterations in 0.08 seconds (average 0.019000, setup 0.00)

Prob1A: 42 iterations in 0.49 seconds (average 0.011714, setup 0.00)

yes = 115749, no = 3007, maybe = 117260

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=236016, nc=193981, nnz=503622, k=4] [6.2 MB]
Creating vector for yes... [1.8 MB]
Allocating iteration vectors... [2 x 1.8 MB]
TOTAL: [11.6 MB]

Starting iterations...

Iterative method: 79 iterations in 0.44 seconds (average 0.002329, setup 0.25)

Value in the initial state: 0.01334810925249081

Time for model checking: 1.304 seconds.

Result: 0.01334810925249081 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmax=? [ F "AllAreaSearched" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=0,Objy1=1,Objx2=3,Objy2=1

Prob0A: 53 iterations in 0.42 seconds (average 0.008000, setup 0.00)

Prob1E: 4 iterations in 0.05 seconds (average 0.013000, setup 0.00)

yes = 1473, no = 175267, maybe = 59276

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=236016, nc=91505, nnz=239611, k=2] [3.1 MB]
Creating vector for yes... [1.8 MB]
Allocating iteration vectors... [2 x 1.8 MB]
TOTAL: [8.5 MB]

Starting iterations...

Iterative method: 89 iterations in 0.39 seconds (average 0.001843, setup 0.22)

Value in the initial state: 0.04754083249350918

Time for model checking: 1.242 seconds.

Result: 0.04754083249350918 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmin=? [ F "AllAreaSearched" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=0,Objy1=1,Objx2=3,Objy2=1

Prob0E: 62 iterations in 0.44 seconds (average 0.007097, setup 0.00)

Prob1A: 3 iterations in 0.04 seconds (average 0.013333, setup 0.00)

yes = 1473, no = 181912, maybe = 52631

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=236016, nc=79453, nnz=204548, k=2] [2.6 MB]
Creating vector for yes... [1.8 MB]
Allocating iteration vectors... [2 x 1.8 MB]
TOTAL: [8.0 MB]

Starting iterations...

Iterative method: 96 iterations in 0.33 seconds (average 0.001917, setup 0.14)

Value in the initial state: 0.001848827913152603

Time for model checking: 0.966 seconds.

Result: 0.001848827913152603 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmax=? [ F "MissionTimeUp"|"AllAreaSearched"|"Crash" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=0,Objy1=1,Objx2=3,Objy2=1

Prob0A: 3 iterations in 0.09 seconds (average 0.029333, setup 0.00)

Prob1E: 164 iterations in 2.73 seconds (average 0.016659, setup 0.00)

yes = 201858, no = 1142, maybe = 33016

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=236016, nc=59401, nnz=168639, k=2] [2.2 MB]
Creating vector for yes... [1.8 MB]
Allocating iteration vectors... [2 x 1.8 MB]
TOTAL: [7.6 MB]

Starting iterations...

Iterative method: 79 iterations in 0.33 seconds (average 0.001620, setup 0.20)

Value in the initial state: 0.06739253421833612

Time for model checking: 4.323 seconds.

Result: 0.06739253421833612 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmin=? [ F "MissionTimeUp"|"AllAreaSearched"|"Crash" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=0,Objy1=1,Objx2=3,Objy2=1

Prob0E: 4 iterations in 0.07 seconds (average 0.018000, setup 0.00)

Prob1A: 43 iterations in 0.46 seconds (average 0.010698, setup 0.00)

yes = 196994, no = 1142, maybe = 37880

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=236016, nc=68372, nnz=194560, k=2] [2.5 MB]
Creating vector for yes... [1.8 MB]
Allocating iteration vectors... [2 x 1.8 MB]
TOTAL: [7.9 MB]

Starting iterations...

Iterative method: 86 iterations in 0.32 seconds (average 0.001628, setup 0.18)

Value in the initial state: 0.015390711431027236

Time for model checking: 1.431 seconds.

Result: 0.015390711431027236 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"drop"}max=? [ F "deadlock" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=0,Objy1=1,Objx2=3,Objy2=1

Prob0E: 5 iterations in 0.06 seconds (average 0.012000, setup 0.00)

Prob1A: 1 iterations in 0.01 seconds (average 0.008000, setup 0.00)

goal = 1, inf = 0, maybe = 236015

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=236016, nc=312736, nnz=622377, k=4] [9.2 MB]
Building sparse matrix (transition rewards)... [n=236016, nc=312736, nnz=0, k=4] [2.1 MB]
Creating vector for state rewards... [1.8 MB]
Creating vector for inf... [1.8 MB]
Allocating iteration vectors... [2 x 1.8 MB]
TOTAL: [18.5 MB]

Starting iterations...

Iterative method: 83 iterations in 0.72 seconds (average 0.003422, setup 0.44)

Value in the initial state: 0.1752423183883417

Time for model checking: 0.975 seconds.

Result: 0.1752423183883417 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"drop"}min=? [ F "deadlock" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=0,Objy1=1,Objx2=3,Objy2=1

Prob0A: 4 iterations in 0.05 seconds (average 0.012000, setup 0.00)

Prob1E: 5 iterations in 0.06 seconds (average 0.011200, setup 0.00)

Warning: PRISM hasn't checked for zero-reward loops.
Your minimum rewards may be too low...

goal = 1, inf = 0, maybe = 236015

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=236016, nc=312736, nnz=622377, k=4] [9.2 MB]
Building sparse matrix (transition rewards)... [n=236016, nc=312736, nnz=0, k=4] [2.1 MB]
Creating vector for state rewards... [1.8 MB]
Creating vector for inf... [1.8 MB]
Allocating iteration vectors... [2 x 1.8 MB]
TOTAL: [18.5 MB]

Starting iterations...

Iterative method: 76 iterations in 0.60 seconds (average 0.003474, setup 0.33)

Value in the initial state: 0.1624761162244396

Time for model checking: 0.949 seconds.

Result: 0.1624761162244396 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"charge"}max=? [ F "deadlock" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=0,Objy1=1,Objx2=3,Objy2=1

Prob0E: 5 iterations in 0.04 seconds (average 0.007200, setup 0.00)

Prob1A: 1 iterations in 0.00 seconds (average 0.004000, setup 0.00)

goal = 1, inf = 0, maybe = 236015

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=236016, nc=312736, nnz=622377, k=4] [9.2 MB]
Building sparse matrix (transition rewards)... [n=236016, nc=312736, nnz=6589, k=4] [2.2 MB]
Creating vector for state rewards... [1.8 MB]
Creating vector for inf... [1.8 MB]
Allocating iteration vectors... [2 x 1.8 MB]
TOTAL: [18.6 MB]

Starting iterations...

Iterative method: 84 iterations in 0.61 seconds (average 0.003429, setup 0.32)

Value in the initial state: 0.16181361956608278

Time for model checking: 1.021 seconds.

Result: 0.16181361956608278 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"charge"}min=? [ F "deadlock" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=0,Objy1=1,Objx2=3,Objy2=1

Prob0A: 4 iterations in 0.03 seconds (average 0.007000, setup 0.00)

Prob1E: 5 iterations in 0.06 seconds (average 0.011200, setup 0.00)

Warning: PRISM hasn't checked for zero-reward loops.
Your minimum rewards may be too low...

goal = 1, inf = 0, maybe = 236015

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=236016, nc=312736, nnz=622377, k=4] [9.2 MB]
Building sparse matrix (transition rewards)... [n=236016, nc=312736, nnz=6589, k=4] [2.2 MB]
Creating vector for state rewards... [1.8 MB]
Creating vector for inf... [1.8 MB]
Allocating iteration vectors... [2 x 1.8 MB]
TOTAL: [18.6 MB]

Starting iterations...

Iterative method: 94 iterations in 0.66 seconds (average 0.003489, setup 0.33)

Value in the initial state: 0.008178420410607455

Time for model checking: 1.226 seconds.

Result: 0.008178420410607455 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"time"}max=? [ F "deadlock" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=0,Objy1=1,Objx2=3,Objy2=1

Prob0E: 5 iterations in 0.04 seconds (average 0.007200, setup 0.00)

Prob1A: 1 iterations in 0.01 seconds (average 0.008000, setup 0.00)

goal = 1, inf = 0, maybe = 236015

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=236016, nc=312736, nnz=622377, k=4] [9.2 MB]
Building sparse matrix (transition rewards)... [n=236016, nc=312736, nnz=118754, k=4] [3.5 MB]
Creating vector for state rewards... [1.8 MB]
Creating vector for inf... [1.8 MB]
Allocating iteration vectors... [2 x 1.8 MB]
TOTAL: [19.9 MB]

Starting iterations...

Iterative method: 92 iterations in 0.71 seconds (average 0.003652, setup 0.37)

Value in the initial state: 127.77240524279536

Time for model checking: 0.82 seconds.

Result: 127.77240524279536 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"time"}min=? [ F "deadlock" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=0,Objy1=1,Objx2=3,Objy2=1

Prob0A: 4 iterations in 0.03 seconds (average 0.008000, setup 0.00)

Prob1E: 5 iterations in 0.06 seconds (average 0.012000, setup 0.00)

Warning: PRISM hasn't checked for zero-reward loops.
Your minimum rewards may be too low...

goal = 1, inf = 0, maybe = 236015

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=236016, nc=312736, nnz=622377, k=4] [9.2 MB]
Building sparse matrix (transition rewards)... [n=236016, nc=312736, nnz=118754, k=4] [3.5 MB]
Creating vector for state rewards... [1.8 MB]
Creating vector for inf... [1.8 MB]
Allocating iteration vectors... [2 x 1.8 MB]
TOTAL: [19.9 MB]

Starting iterations...

Iterative method: 91 iterations in 0.68 seconds (average 0.003604, setup 0.36)

Value in the initial state: 74.097872261234

Time for model checking: 1.04 seconds.

Result: 74.097872261234 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"success"}max=? [ F "deadlock" ]/Pmin=? [ F "MissionSuccessful" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=0,Objy1=1,Objx2=3,Objy2=1

Prob0E: 5 iterations in 0.04 seconds (average 0.008000, setup 0.00)

Prob1A: 1 iterations in 0.01 seconds (average 0.008000, setup 0.00)

goal = 1, inf = 0, maybe = 236015

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=236016, nc=312736, nnz=622377, k=4] [9.2 MB]
Building sparse matrix (transition rewards)... [n=236016, nc=312736, nnz=2250, k=4] [2.1 MB]
Creating vector for state rewards... [1.8 MB]
Creating vector for inf... [1.8 MB]
Allocating iteration vectors... [2 x 1.8 MB]
TOTAL: [18.5 MB]

Starting iterations...

Iterative method: 90 iterations in 0.64 seconds (average 0.003422, setup 0.34)

Prob0E: 43 iterations in 0.34 seconds (average 0.007907, setup 0.00)

Prob1A: 3 iterations in 0.05 seconds (average 0.016000, setup 0.00)

yes = 1141, no = 201859, maybe = 33016

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=236016, nc=59401, nnz=168639, k=2] [2.2 MB]
Creating vector for yes... [1.8 MB]
Allocating iteration vectors... [2 x 1.8 MB]
TOTAL: [7.6 MB]

Starting iterations...

Iterative method: 85 iterations in 0.29 seconds (average 0.001600, setup 0.16)

Value in the initial state: 134.65144236528064

Time for model checking: 1.433 seconds.

Result: 134.65144236528064 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"success"}min=? [ F "deadlock" ]/Pmax=? [ F "MissionSuccessful" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=0,Objy1=1,Objx2=3,Objy2=1

Prob0A: 4 iterations in 0.05 seconds (average 0.012000, setup 0.00)

Prob1E: 5 iterations in 0.06 seconds (average 0.012800, setup 0.00)

Warning: PRISM hasn't checked for zero-reward loops.
Your minimum rewards may be too low...

goal = 1, inf = 0, maybe = 236015

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=236016, nc=312736, nnz=622377, k=4] [9.2 MB]
Building sparse matrix (transition rewards)... [n=236016, nc=312736, nnz=2250, k=4] [2.1 MB]
Creating vector for state rewards... [1.8 MB]
Creating vector for inf... [1.8 MB]
Allocating iteration vectors... [2 x 1.8 MB]
TOTAL: [18.5 MB]

Starting iterations...

Iterative method: 89 iterations in 0.67 seconds (average 0.003461, setup 0.36)

Prob0A: 43 iterations in 0.30 seconds (average 0.006884, setup 0.00)

Prob1E: 4 iterations in 0.06 seconds (average 0.016000, setup 0.00)

yes = 1141, no = 196995, maybe = 37880

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=236016, nc=68372, nnz=194560, k=2] [2.5 MB]
Creating vector for yes... [1.8 MB]
Allocating iteration vectors... [2 x 1.8 MB]
TOTAL: [7.9 MB]

Starting iterations...

Iterative method: 87 iterations in 0.31 seconds (average 0.001609, setup 0.17)

Value in the initial state: 71.81213413285683

Time for model checking: 1.486 seconds.

Result: 71.81213413285683 (value in the initial state)

---------------------------------------------------------------------

Note: There were 17 warnings during computation.

