PRISM
=====

Version: 4.3
Date: Wed Aug 03 15:53:27 BST 2016
Hostname: eglinton
Memory limits: cudd=1g, java(heap)=981.5m
Command line: prism uav_sim_mdp.prism -s uav_mdp.props -const 'Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=0,Objy1=3,Objx2=3,Objy2=1,'

Parsing model file "uav_sim_mdp.prism"...

Parsing properties file "uav_mdp.props"...

18 properties:
(1) Pmax=? [ F "MissionSuccessful" ]
(2) Pmin=? [ F "MissionSuccessful" ]
(3) Pmax=? [ F "MissionTimeUp" ]
(4) Pmin=? [ F "MissionTimeUp" ]
(5) Pmax=? [ F "Crash" ]
(6) Pmin=? [ F "Crash" ]
(7) Pmax=? [ F "AllAreaSearched" ]
(8) Pmin=? [ F "AllAreaSearched" ]
(9) Pmax=? [ F "MissionTimeUp"|"AllAreaSearched"|"Crash" ]
(10) Pmin=? [ F "MissionTimeUp"|"AllAreaSearched"|"Crash" ]
(11) R{"drop"}max=? [ F "deadlock" ]
(12) R{"drop"}min=? [ F "deadlock" ]
(13) R{"charge"}max=? [ F "deadlock" ]
(14) R{"charge"}min=? [ F "deadlock" ]
(15) R{"time"}max=? [ F "deadlock" ]
(16) R{"time"}min=? [ F "deadlock" ]
(17) R{"success"}max=? [ F "deadlock" ]/Pmin=? [ F "MissionSuccessful" ]
(18) R{"success"}min=? [ F "deadlock" ]/Pmax=? [ F "MissionSuccessful" ]

Type:        MDP
Modules:     Obj Move Robot T 
Variables:   NoOfObjs posx posy s1 retx rety b t c 

---------------------------------------------------------------------

Model checking: Pmax=? [ F "MissionSuccessful" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=0,Objy1=3,Objx2=3,Objy2=1

Building model...
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=0,Objy1=3,Objx2=3,Objy2=1

Warning: Guard for command 7 of module "Move" is never satisfied.

Warning: Guard for command 8 of module "Move" is never satisfied.

Warning: Guard for command 9 of module "Move" is never satisfied.

Warning: Guard for command 6 of module "Robot" is never satisfied.

Warning: Update 2 of command 3 of module "Robot" doesn't do anything ("(s1'=13)", line 210, column 44)

Warning: Update 2 of command 27 of module "T" doesn't do anything ("(c'=1)", line 323, column 108)

Warning: Guard for command 28 of module "T" is never satisfied.

Warning: Update 2 of command 9 of module "T" doesn't do anything ("(c'=1)", line 300, column 112)

Warning: Guard for command 10 of module "T" is never satisfied.

Warning: Update 2 of command 51 of module "T" doesn't do anything ("(c'=1)", line 354, column 113)

Warning: Guard for command 52 of module "T" is never satisfied.

Computing reachable states...

Reachability (BFS): 89 iterations in 1.16 seconds (average 0.013034, setup 0.00)

Time for model construction: 4.515 seconds.

Warning: Deadlocks detected and fixed in 1 states

Type:        MDP
States:      202962 (1 initial)
Transitions: 543927
Choices:     269941

Transition matrix: 235711 nodes (91 terminal), 543927 minterms, vars: 34r/34c/18nd

Prob0A: 56 iterations in 0.58 seconds (average 0.010357, setup 0.00)

Prob1E: 4 iterations in 0.08 seconds (average 0.020000, setup 0.00)

yes = 920, no = 158321, maybe = 43721

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=202962, nc=74824, nnz=206562, k=2] [2.6 MB]
Creating vector for yes... [1.5 MB]
Allocating iteration vectors... [2 x 1.5 MB]
TOTAL: [7.3 MB]

Starting iterations...

Iterative method: 96 iterations in 0.42 seconds (average 0.001458, setup 0.28)

Value in the initial state: 0.9328185994159333

Time for model checking: 1.162 seconds.

Result: 0.9328185994159333 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmin=? [ F "MissionSuccessful" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=0,Objy1=3,Objx2=3,Objy2=1

Prob0E: 61 iterations in 0.64 seconds (average 0.010492, setup 0.00)

Prob1A: 3 iterations in 0.06 seconds (average 0.021333, setup 0.00)

yes = 920, no = 166576, maybe = 35466

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=202962, nc=60032, nnz=163976, k=2] [2.1 MB]
Creating vector for yes... [1.5 MB]
Allocating iteration vectors... [2 x 1.5 MB]
TOTAL: [6.8 MB]

Starting iterations...

Iterative method: 97 iterations in 0.34 seconds (average 0.001443, setup 0.20)

Value in the initial state: 0.7090477040858905

Time for model checking: 1.317 seconds.

Result: 0.7090477040858905 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmax=? [ F "MissionTimeUp" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=0,Objy1=3,Objx2=3,Objy2=1

Prob0A: 73 iterations in 0.53 seconds (average 0.007233, setup 0.00)

Prob1E: 4 iterations in 0.05 seconds (average 0.012000, setup 0.00)

yes = 300, no = 170149, maybe = 32513

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=202962, nc=56223, nnz=152489, k=4] [2.0 MB]
Creating vector for yes... [1.5 MB]
Allocating iteration vectors... [2 x 1.5 MB]
TOTAL: [6.6 MB]

Starting iterations...

Iterative method: 98 iterations in 0.28 seconds (average 0.001306, setup 0.16)

Value in the initial state: 3.115305881312795E-6

Time for model checking: 1.004 seconds.

Result: 3.115305881312795E-6 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmin=? [ F "MissionTimeUp" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=0,Objy1=3,Objx2=3,Objy2=1

Prob0E: 87 iterations in 0.60 seconds (average 0.006897, setup 0.00)

Prob1A: 3 iterations in 0.04 seconds (average 0.012000, setup 0.00)

yes = 300, no = 170316, maybe = 32346

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=202962, nc=55931, nnz=151840, k=4] [2.0 MB]
Creating vector for yes... [1.5 MB]
Allocating iteration vectors... [2 x 1.5 MB]
TOTAL: [6.6 MB]

Starting iterations...

Iterative method: 115 iterations in 0.25 seconds (average 0.001357, setup 0.09)

Value in the initial state: 1.2731685469226718E-9

Time for model checking: 1.014 seconds.

Result: 1.2731685469226718E-9 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmax=? [ F "Crash" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=0,Objy1=3,Objx2=3,Objy2=1

Prob0A: 3 iterations in 0.11 seconds (average 0.036000, setup 0.00)

Prob1E: 200 iterations in 3.50 seconds (average 0.017500, setup 0.00)

yes = 99431, no = 1481, maybe = 102050

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=202962, nc=169029, nnz=443015, k=4] [5.4 MB]
Creating vector for yes... [1.5 MB]
Allocating iteration vectors... [2 x 1.5 MB]
TOTAL: [10.1 MB]

Starting iterations...

Iterative method: 93 iterations in 0.56 seconds (average 0.001935, setup 0.38)

Value in the initial state: 0.01847030674232644

Time for model checking: 4.768 seconds.

Result: 0.01847030674232644 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmin=? [ F "Crash" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=0,Objy1=3,Objx2=3,Objy2=1

Prob0E: 4 iterations in 0.10 seconds (average 0.024000, setup 0.00)

Prob1A: 53 iterations in 0.81 seconds (average 0.015245, setup 0.00)

yes = 99431, no = 1481, maybe = 102050

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=202962, nc=169029, nnz=443015, k=4] [5.4 MB]
Creating vector for yes... [1.5 MB]
Allocating iteration vectors... [2 x 1.5 MB]
TOTAL: [10.1 MB]

Starting iterations...

Iterative method: 88 iterations in 0.50 seconds (average 0.001955, setup 0.33)

Value in the initial state: 0.015415851299452613

Time for model checking: 1.652 seconds.

Result: 0.015415851299452613 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmax=? [ F "AllAreaSearched" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=0,Objy1=3,Objx2=3,Objy2=1

Prob0A: 53 iterations in 0.52 seconds (average 0.009736, setup 0.00)

Prob1E: 4 iterations in 0.06 seconds (average 0.015000, setup 0.00)

yes = 260, no = 124524, maybe = 78178

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=202962, nc=127931, nnz=344681, k=2] [4.3 MB]
Creating vector for yes... [1.5 MB]
Allocating iteration vectors... [2 x 1.5 MB]
TOTAL: [8.9 MB]

Starting iterations...

Iterative method: 98 iterations in 0.46 seconds (average 0.001714, setup 0.29)

Value in the initial state: 0.27376792292969887

Time for model checking: 1.334 seconds.

Result: 0.27376792292969887 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmin=? [ F "AllAreaSearched" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=0,Objy1=3,Objx2=3,Objy2=1

Prob0E: 69 iterations in 0.79 seconds (average 0.011478, setup 0.00)

Prob1A: 3 iterations in 0.07 seconds (average 0.024000, setup 0.00)

yes = 260, no = 126094, maybe = 76608

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=202962, nc=124959, nnz=335931, k=2] [4.2 MB]
Creating vector for yes... [1.5 MB]
Allocating iteration vectors... [2 x 1.5 MB]
TOTAL: [8.8 MB]

Starting iterations...

Iterative method: 104 iterations in 0.46 seconds (average 0.001731, setup 0.28)

Value in the initial state: 0.05161845893106001

Time for model checking: 1.467 seconds.

Result: 0.05161845893106001 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmax=? [ F "MissionTimeUp"|"AllAreaSearched"|"Crash" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=0,Objy1=3,Objx2=3,Objy2=1

Prob0A: 3 iterations in 0.11 seconds (average 0.037333, setup 0.00)

Prob1E: 225 iterations in 4.46 seconds (average 0.019840, setup 0.00)

yes = 166575, no = 921, maybe = 35466

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=202962, nc=60032, nnz=163976, k=2] [2.1 MB]
Creating vector for yes... [1.5 MB]
Allocating iteration vectors... [2 x 1.5 MB]
TOTAL: [6.8 MB]

Starting iterations...

Iterative method: 88 iterations in 0.37 seconds (average 0.001364, setup 0.25)

Value in the initial state: 0.29095189962842866

Time for model checking: 5.485 seconds.

Result: 0.29095189962842866 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmin=? [ F "MissionTimeUp"|"AllAreaSearched"|"Crash" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=0,Objy1=3,Objx2=3,Objy2=1

Prob0E: 4 iterations in 0.10 seconds (average 0.024000, setup 0.00)

Prob1A: 56 iterations in 0.61 seconds (average 0.010929, setup 0.00)

yes = 158320, no = 921, maybe = 43721

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=202962, nc=74824, nnz=206562, k=2] [2.6 MB]
Creating vector for yes... [1.5 MB]
Allocating iteration vectors... [2 x 1.5 MB]
TOTAL: [7.3 MB]

Starting iterations...

Iterative method: 87 iterations in 0.36 seconds (average 0.001517, setup 0.22)

Value in the initial state: 0.06717926539884281

Time for model checking: 1.096 seconds.

Result: 0.06717926539884281 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"drop"}max=? [ F "deadlock" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=0,Objy1=3,Objx2=3,Objy2=1

Prob0E: 5 iterations in 0.08 seconds (average 0.016000, setup 0.00)

Prob1A: 1 iterations in 0.01 seconds (average 0.008000, setup 0.00)

goal = 1, inf = 0, maybe = 202961

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=202962, nc=269940, nnz=543926, k=4] [8.0 MB]
Building sparse matrix (transition rewards)... [n=202962, nc=269940, nnz=0, k=4] [1.8 MB]
Creating vector for state rewards... [1.5 MB]
Creating vector for inf... [1.5 MB]
Allocating iteration vectors... [2 x 1.5 MB]
TOTAL: [16.0 MB]

Starting iterations...

Iterative method: 92 iterations in 0.70 seconds (average 0.002957, setup 0.42)

Value in the initial state: 0.16204288625117635

Time for model checking: 0.801 seconds.

Result: 0.16204288625117635 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"drop"}min=? [ F "deadlock" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=0,Objy1=3,Objx2=3,Objy2=1

Prob0A: 4 iterations in 0.07 seconds (average 0.018000, setup 0.00)

Prob1E: 5 iterations in 0.06 seconds (average 0.012000, setup 0.00)

Warning: PRISM hasn't checked for zero-reward loops.
Your minimum rewards may be too low...

goal = 1, inf = 0, maybe = 202961

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=202962, nc=269940, nnz=543926, k=4] [8.0 MB]
Building sparse matrix (transition rewards)... [n=202962, nc=269940, nnz=0, k=4] [1.8 MB]
Creating vector for state rewards... [1.5 MB]
Creating vector for inf... [1.5 MB]
Allocating iteration vectors... [2 x 1.5 MB]
TOTAL: [16.0 MB]

Starting iterations...

Iterative method: 89 iterations in 0.57 seconds (average 0.003011, setup 0.30)

Value in the initial state: 0.14833466086302224

Time for model checking: 0.715 seconds.

Result: 0.14833466086302224 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"charge"}max=? [ F "deadlock" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=0,Objy1=3,Objx2=3,Objy2=1

Prob0E: 5 iterations in 0.04 seconds (average 0.008000, setup 0.00)

Prob1A: 1 iterations in 0.01 seconds (average 0.008000, setup 0.00)

goal = 1, inf = 0, maybe = 202961

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=202962, nc=269940, nnz=543926, k=4] [8.0 MB]
Building sparse matrix (transition rewards)... [n=202962, nc=269940, nnz=6459, k=4] [1.9 MB]
Creating vector for state rewards... [1.5 MB]
Creating vector for inf... [1.5 MB]
Allocating iteration vectors... [2 x 1.5 MB]
TOTAL: [16.1 MB]

Starting iterations...

Iterative method: 95 iterations in 0.59 seconds (average 0.002947, setup 0.31)

Value in the initial state: 0.27431160742610194

Time for model checking: 0.65 seconds.

Result: 0.27431160742610194 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"charge"}min=? [ F "deadlock" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=0,Objy1=3,Objx2=3,Objy2=1

Prob0A: 4 iterations in 0.03 seconds (average 0.007000, setup 0.00)

Prob1E: 5 iterations in 0.06 seconds (average 0.011200, setup 0.00)

Warning: PRISM hasn't checked for zero-reward loops.
Your minimum rewards may be too low...

goal = 1, inf = 0, maybe = 202961

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=202962, nc=269940, nnz=543926, k=4] [8.0 MB]
Building sparse matrix (transition rewards)... [n=202962, nc=269940, nnz=6459, k=4] [1.9 MB]
Creating vector for state rewards... [1.5 MB]
Creating vector for inf... [1.5 MB]
Allocating iteration vectors... [2 x 1.5 MB]
TOTAL: [16.1 MB]

Starting iterations...

Iterative method: 101 iterations in 0.60 seconds (average 0.003010, setup 0.29)

Value in the initial state: 0.048393876938214724

Time for model checking: 0.706 seconds.

Result: 0.048393876938214724 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"time"}max=? [ F "deadlock" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=0,Objy1=3,Objx2=3,Objy2=1

Prob0E: 5 iterations in 0.04 seconds (average 0.008000, setup 0.00)

Prob1A: 1 iterations in 0.01 seconds (average 0.008000, setup 0.00)

goal = 1, inf = 0, maybe = 202961

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=202962, nc=269940, nnz=543926, k=4] [8.0 MB]
Building sparse matrix (transition rewards)... [n=202962, nc=269940, nnz=100910, k=4] [3.0 MB]
Creating vector for state rewards... [1.5 MB]
Creating vector for inf... [1.5 MB]
Allocating iteration vectors... [2 x 1.5 MB]
TOTAL: [17.2 MB]

Starting iterations...

Iterative method: 96 iterations in 0.63 seconds (average 0.002958, setup 0.34)

Value in the initial state: 102.53543012715113

Time for model checking: 0.69 seconds.

Result: 102.53543012715113 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"time"}min=? [ F "deadlock" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=0,Objy1=3,Objx2=3,Objy2=1

Prob0A: 4 iterations in 0.03 seconds (average 0.007000, setup 0.00)

Prob1E: 5 iterations in 0.06 seconds (average 0.012000, setup 0.00)

Warning: PRISM hasn't checked for zero-reward loops.
Your minimum rewards may be too low...

goal = 1, inf = 0, maybe = 202961

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=202962, nc=269940, nnz=543926, k=4] [8.0 MB]
Building sparse matrix (transition rewards)... [n=202962, nc=269940, nnz=100910, k=4] [3.0 MB]
Creating vector for state rewards... [1.5 MB]
Creating vector for inf... [1.5 MB]
Allocating iteration vectors... [2 x 1.5 MB]
TOTAL: [17.2 MB]

Starting iterations...

Iterative method: 105 iterations in 0.65 seconds (average 0.003048, setup 0.33)

Value in the initial state: 85.61017988742739

Time for model checking: 0.761 seconds.

Result: 85.61017988742739 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"success"}max=? [ F "deadlock" ]/Pmin=? [ F "MissionSuccessful" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=0,Objy1=3,Objx2=3,Objy2=1

Prob0E: 5 iterations in 0.04 seconds (average 0.008000, setup 0.00)

Prob1A: 1 iterations in 0.01 seconds (average 0.008000, setup 0.00)

goal = 1, inf = 0, maybe = 202961

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=202962, nc=269940, nnz=543926, k=4] [8.0 MB]
Building sparse matrix (transition rewards)... [n=202962, nc=269940, nnz=1821, k=4] [1.8 MB]
Creating vector for state rewards... [1.5 MB]
Creating vector for inf... [1.5 MB]
Allocating iteration vectors... [2 x 1.5 MB]
TOTAL: [16.0 MB]

Starting iterations...

Iterative method: 103 iterations in 0.61 seconds (average 0.002951, setup 0.31)

Prob0E: 61 iterations in 0.50 seconds (average 0.008262, setup 0.00)

Prob1A: 3 iterations in 0.07 seconds (average 0.024000, setup 0.00)

yes = 920, no = 166576, maybe = 35466

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=202962, nc=60032, nnz=163976, k=2] [2.1 MB]
Creating vector for yes... [1.5 MB]
Allocating iteration vectors... [2 x 1.5 MB]
TOTAL: [6.8 MB]

Starting iterations...

Iterative method: 97 iterations in 0.34 seconds (average 0.001402, setup 0.20)

Value in the initial state: 122.90595462685376

Time for model checking: 1.621 seconds.

Result: 122.90595462685376 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"success"}min=? [ F "deadlock" ]/Pmax=? [ F "MissionSuccessful" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=0,Objy1=3,Objx2=3,Objy2=1

Prob0A: 4 iterations in 0.10 seconds (average 0.026000, setup 0.00)

Prob1E: 5 iterations in 0.08 seconds (average 0.015200, setup 0.00)

Warning: PRISM hasn't checked for zero-reward loops.
Your minimum rewards may be too low...

goal = 1, inf = 0, maybe = 202961

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=202962, nc=269940, nnz=543926, k=4] [8.0 MB]
Building sparse matrix (transition rewards)... [n=202962, nc=269940, nnz=1821, k=4] [1.8 MB]
Creating vector for state rewards... [1.5 MB]
Creating vector for inf... [1.5 MB]
Allocating iteration vectors... [2 x 1.5 MB]
TOTAL: [16.0 MB]

Starting iterations...

Iterative method: 100 iterations in 0.66 seconds (average 0.002960, setup 0.36)

Prob0A: 56 iterations in 0.44 seconds (average 0.007929, setup 0.00)

Prob1E: 4 iterations in 0.04 seconds (average 0.010000, setup 0.00)

yes = 920, no = 158321, maybe = 43721

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=202962, nc=74824, nnz=206562, k=2] [2.6 MB]
Creating vector for yes... [1.5 MB]
Allocating iteration vectors... [2 x 1.5 MB]
TOTAL: [7.3 MB]

Starting iterations...

Iterative method: 96 iterations in 0.34 seconds (average 0.001458, setup 0.20)

Value in the initial state: 66.48829384974947

Time for model checking: 1.7 seconds.

Result: 66.48829384974947 (value in the initial state)

---------------------------------------------------------------------

Note: There were 16 warnings during computation.

