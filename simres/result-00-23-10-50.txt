PRISM
=====

Version: 4.3
Date: Wed Aug 03 15:54:11 BST 2016
Hostname: eglinton
Memory limits: cudd=1g, java(heap)=981.5m
Command line: prism uav_sim_mdp.prism -s uav_mdp.props -const 'Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=1,Objy1=0,Objx2=5,Objy2=0,'

Parsing model file "uav_sim_mdp.prism"...

Parsing properties file "uav_mdp.props"...

18 properties:
(1) Pmax=? [ F "MissionSuccessful" ]
(2) Pmin=? [ F "MissionSuccessful" ]
(3) Pmax=? [ F "MissionTimeUp" ]
(4) Pmin=? [ F "MissionTimeUp" ]
(5) Pmax=? [ F "Crash" ]
(6) Pmin=? [ F "Crash" ]
(7) Pmax=? [ F "AllAreaSearched" ]
(8) Pmin=? [ F "AllAreaSearched" ]
(9) Pmax=? [ F "MissionTimeUp"|"AllAreaSearched"|"Crash" ]
(10) Pmin=? [ F "MissionTimeUp"|"AllAreaSearched"|"Crash" ]
(11) R{"drop"}max=? [ F "deadlock" ]
(12) R{"drop"}min=? [ F "deadlock" ]
(13) R{"charge"}max=? [ F "deadlock" ]
(14) R{"charge"}min=? [ F "deadlock" ]
(15) R{"time"}max=? [ F "deadlock" ]
(16) R{"time"}min=? [ F "deadlock" ]
(17) R{"success"}max=? [ F "deadlock" ]/Pmin=? [ F "MissionSuccessful" ]
(18) R{"success"}min=? [ F "deadlock" ]/Pmax=? [ F "MissionSuccessful" ]

Type:        MDP
Modules:     Obj Move Robot T 
Variables:   NoOfObjs posx posy s1 retx rety b t c 

---------------------------------------------------------------------

Model checking: Pmax=? [ F "MissionSuccessful" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=1,Objy1=0,Objx2=5,Objy2=0

Building model...
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=1,Objy1=0,Objx2=5,Objy2=0

Warning: Guard for command 7 of module "Move" is never satisfied.

Warning: Guard for command 8 of module "Move" is never satisfied.

Warning: Guard for command 9 of module "Move" is never satisfied.

Warning: Guard for command 7 of module "Robot" is never satisfied.

Warning: Update 2 of command 3 of module "Robot" doesn't do anything ("(s1'=13)", line 210, column 44)

Warning: Update 2 of command 27 of module "T" doesn't do anything ("(c'=1)", line 323, column 108)

Warning: Guard for command 28 of module "T" is never satisfied.

Warning: Update 2 of command 9 of module "T" doesn't do anything ("(c'=1)", line 300, column 112)

Warning: Guard for command 10 of module "T" is never satisfied.

Warning: Update 2 of command 51 of module "T" doesn't do anything ("(c'=1)", line 354, column 113)

Warning: Guard for command 52 of module "T" is never satisfied.

Computing reachable states...

Reachability (BFS): 89 iterations in 1.76 seconds (average 0.019820, setup 0.00)

Time for model construction: 4.74 seconds.

Warning: Deadlocks detected and fixed in 1 states

Type:        MDP
States:      308900 (1 initial)
Transitions: 816198
Choices:     406957

Transition matrix: 223586 nodes (93 terminal), 816198 minterms, vars: 34r/34c/18nd

Prob0A: 47 iterations in 0.47 seconds (average 0.009957, setup 0.00)

Prob1E: 4 iterations in 0.08 seconds (average 0.020000, setup 0.00)

yes = 1131, no = 261350, maybe = 46419

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=308900, nc=83416, nnz=241888, k=2] [3.1 MB]
Creating vector for yes... [2.4 MB]
Allocating iteration vectors... [2 x 2.4 MB]
TOTAL: [10.2 MB]

Starting iterations...

Iterative method: 86 iterations in 0.49 seconds (average 0.002279, setup 0.30)

Value in the initial state: 0.9853480821824444

Time for model checking: 1.268 seconds.

Result: 0.9853480821824444 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmin=? [ F "MissionSuccessful" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=1,Objy1=0,Objx2=5,Objy2=0

Prob0E: 47 iterations in 0.54 seconds (average 0.011574, setup 0.00)

Prob1A: 3 iterations in 0.05 seconds (average 0.017333, setup 0.00)

yes = 1131, no = 268294, maybe = 39475

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=308900, nc=70594, nnz=204458, k=2] [2.7 MB]
Creating vector for yes... [2.4 MB]
Allocating iteration vectors... [2 x 2.4 MB]
TOTAL: [9.8 MB]

Starting iterations...

Iterative method: 82 iterations in 0.38 seconds (average 0.002195, setup 0.20)

Value in the initial state: 0.9273822329998849

Time for model checking: 1.339 seconds.

Result: 0.9273822329998849 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmax=? [ F "MissionTimeUp" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=1,Objy1=0,Objx2=5,Objy2=0

Prob0A: 67 iterations in 0.71 seconds (average 0.010627, setup 0.00)

Prob1E: 4 iterations in 0.05 seconds (average 0.013000, setup 0.00)

yes = 544, no = 190319, maybe = 118037

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=308900, nc=205014, nnz=565108, k=4] [7.0 MB]
Creating vector for yes... [2.4 MB]
Allocating iteration vectors... [2 x 2.4 MB]
TOTAL: [14.0 MB]

Starting iterations...

Iterative method: 97 iterations in 0.62 seconds (average 0.002763, setup 0.36)

Value in the initial state: 9.702510413080122E-5

Time for model checking: 1.85 seconds.

Result: 9.702510413080122E-5 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmin=? [ F "MissionTimeUp" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=1,Objy1=0,Objx2=5,Objy2=0

Prob0E: 81 iterations in 1.07 seconds (average 0.013235, setup 0.00)

Prob1A: 3 iterations in 0.06 seconds (average 0.018667, setup 0.00)

yes = 544, no = 190685, maybe = 117671

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=308900, nc=204353, nnz=563880, k=4] [6.9 MB]
Creating vector for yes... [2.4 MB]
Allocating iteration vectors... [2 x 2.4 MB]
TOTAL: [14.0 MB]

Starting iterations...

Iterative method: 113 iterations in 0.67 seconds (average 0.002832, setup 0.35)

Value in the initial state: 1.45100651288463E-7

Time for model checking: 2.247 seconds.

Result: 1.45100651288463E-7 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmax=? [ F "Crash" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=1,Objy1=0,Objx2=5,Objy2=0

Prob0A: 3 iterations in 0.24 seconds (average 0.081333, setup 0.00)

Prob1E: 184 iterations in 3.76 seconds (average 0.020457, setup 0.00)

yes = 151096, no = 3006, maybe = 154798

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=308900, nc=252855, nnz=662096, k=4] [8.1 MB]
Creating vector for yes... [2.4 MB]
Allocating iteration vectors... [2 x 2.4 MB]
TOTAL: [15.2 MB]

Starting iterations...

Iterative method: 88 iterations in 0.73 seconds (average 0.003000, setup 0.46)

Value in the initial state: 0.01919762298826128

Time for model checking: 5.911 seconds.

Result: 0.01919762298826128 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmin=? [ F "Crash" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=1,Objy1=0,Objx2=5,Objy2=0

Prob0E: 4 iterations in 0.11 seconds (average 0.028000, setup 0.00)

Prob1A: 45 iterations in 0.94 seconds (average 0.020889, setup 0.00)

yes = 151096, no = 3006, maybe = 154798

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=308900, nc=252855, nnz=662096, k=4] [8.1 MB]
Creating vector for yes... [2.4 MB]
Allocating iteration vectors... [2 x 2.4 MB]
TOTAL: [15.2 MB]

Starting iterations...

Iterative method: 83 iterations in 0.67 seconds (average 0.002988, setup 0.42)

Value in the initial state: 0.012609539554968343

Time for model checking: 1.773 seconds.

Result: 0.012609539554968343 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmax=? [ F "AllAreaSearched" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=1,Objy1=0,Objx2=5,Objy2=0

Prob0A: 64 iterations in 0.59 seconds (average 0.009187, setup 0.00)

Prob1E: 4 iterations in 0.07 seconds (average 0.018000, setup 0.00)

yes = 1330, no = 251025, maybe = 56545

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=308900, nc=81752, nnz=207789, k=2] [2.8 MB]
Creating vector for yes... [2.4 MB]
Allocating iteration vectors... [2 x 2.4 MB]
TOTAL: [9.8 MB]

Starting iterations...

Iterative method: 94 iterations in 0.42 seconds (average 0.002255, setup 0.21)

Value in the initial state: 0.05439145966481747

Time for model checking: 1.259 seconds.

Result: 0.05439145966481747 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmin=? [ F "AllAreaSearched" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=1,Objy1=0,Objx2=5,Objy2=0

Prob0E: 68 iterations in 0.61 seconds (average 0.009000, setup 0.00)

Prob1A: 3 iterations in 0.06 seconds (average 0.020000, setup 0.00)

yes = 1330, no = 257042, maybe = 50528

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=308900, nc=71107, nnz=177472, k=2] [2.4 MB]
Creating vector for yes... [2.4 MB]
Allocating iteration vectors... [2 x 2.4 MB]
TOTAL: [9.5 MB]

Starting iterations...

Iterative method: 97 iterations in 0.38 seconds (average 0.002351, setup 0.15)

Value in the initial state: 0.0019192952608007184

Time for model checking: 1.086 seconds.

Result: 0.0019192952608007184 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmax=? [ F "MissionTimeUp"|"AllAreaSearched"|"Crash" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=1,Objy1=0,Objx2=5,Objy2=0

Prob0A: 3 iterations in 0.11 seconds (average 0.036000, setup 0.00)

Prob1E: 180 iterations in 3.40 seconds (average 0.018911, setup 0.00)

yes = 268293, no = 1132, maybe = 39475

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=308900, nc=70594, nnz=204458, k=2] [2.7 MB]
Creating vector for yes... [2.4 MB]
Allocating iteration vectors... [2 x 2.4 MB]
TOTAL: [9.8 MB]

Starting iterations...

Iterative method: 76 iterations in 0.43 seconds (average 0.002105, setup 0.27)

Value in the initial state: 0.07261763456312095

Time for model checking: 4.318 seconds.

Result: 0.07261763456312095 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmin=? [ F "MissionTimeUp"|"AllAreaSearched"|"Crash" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=1,Objy1=0,Objx2=5,Objy2=0

Prob0E: 4 iterations in 0.09 seconds (average 0.023000, setup 0.00)

Prob1A: 47 iterations in 0.69 seconds (average 0.014638, setup 0.00)

yes = 261349, no = 1132, maybe = 46419

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=308900, nc=83416, nnz=241888, k=2] [3.1 MB]
Creating vector for yes... [2.4 MB]
Allocating iteration vectors... [2 x 2.4 MB]
TOTAL: [10.2 MB]

Starting iterations...

Iterative method: 85 iterations in 0.52 seconds (average 0.002259, setup 0.32)

Value in the initial state: 0.014649948300193501

Time for model checking: 1.617 seconds.

Result: 0.014649948300193501 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"drop"}max=? [ F "deadlock" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=1,Objy1=0,Objx2=5,Objy2=0

Prob0E: 5 iterations in 0.16 seconds (average 0.032800, setup 0.00)

Prob1A: 1 iterations in 0.02 seconds (average 0.020000, setup 0.00)

goal = 1, inf = 0, maybe = 308899

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=308900, nc=406956, nnz=816197, k=4] [12.1 MB]
Building sparse matrix (transition rewards)... [n=308900, nc=406956, nnz=0, k=4] [2.7 MB]
Creating vector for state rewards... [2.4 MB]
Creating vector for inf... [2.4 MB]
Allocating iteration vectors... [2 x 2.4 MB]
TOTAL: [24.2 MB]

Starting iterations...

Iterative method: 82 iterations in 0.93 seconds (average 0.004585, setup 0.56)

Value in the initial state: 0.22653169777399892

Time for model checking: 1.533 seconds.

Result: 0.22653169777399892 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"drop"}min=? [ F "deadlock" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=1,Objy1=0,Objx2=5,Objy2=0

Prob0A: 4 iterations in 0.04 seconds (average 0.011000, setup 0.00)

Prob1E: 5 iterations in 0.07 seconds (average 0.014400, setup 0.00)

Warning: PRISM hasn't checked for zero-reward loops.
Your minimum rewards may be too low...

goal = 1, inf = 0, maybe = 308899

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=308900, nc=406956, nnz=816197, k=4] [12.1 MB]
Building sparse matrix (transition rewards)... [n=308900, nc=406956, nnz=0, k=4] [2.7 MB]
Creating vector for state rewards... [2.4 MB]
Creating vector for inf... [2.4 MB]
Allocating iteration vectors... [2 x 2.4 MB]
TOTAL: [24.2 MB]

Starting iterations...

Iterative method: 73 iterations in 0.76 seconds (average 0.004548, setup 0.42)

Value in the initial state: 0.21739707205006956

Time for model checking: 1.197 seconds.

Result: 0.21739707205006956 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"charge"}max=? [ F "deadlock" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=1,Objy1=0,Objx2=5,Objy2=0

Prob0E: 5 iterations in 0.05 seconds (average 0.009600, setup 0.00)

Prob1A: 1 iterations in 0.01 seconds (average 0.008000, setup 0.00)

goal = 1, inf = 0, maybe = 308899

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=308900, nc=406956, nnz=816197, k=4] [12.1 MB]
Building sparse matrix (transition rewards)... [n=308900, nc=406956, nnz=9185, k=4] [2.8 MB]
Creating vector for state rewards... [2.4 MB]
Creating vector for inf... [2.4 MB]
Allocating iteration vectors... [2 x 2.4 MB]
TOTAL: [24.3 MB]

Starting iterations...

Iterative method: 81 iterations in 0.80 seconds (average 0.004593, setup 0.43)

Value in the initial state: 0.08114174265470667

Time for model checking: 1.035 seconds.

Result: 0.08114174265470667 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"charge"}min=? [ F "deadlock" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=1,Objy1=0,Objx2=5,Objy2=0

Prob0A: 4 iterations in 0.04 seconds (average 0.009000, setup 0.00)

Prob1E: 5 iterations in 0.08 seconds (average 0.015200, setup 0.00)

Warning: PRISM hasn't checked for zero-reward loops.
Your minimum rewards may be too low...

goal = 1, inf = 0, maybe = 308899

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=308900, nc=406956, nnz=816197, k=4] [12.1 MB]
Building sparse matrix (transition rewards)... [n=308900, nc=406956, nnz=9185, k=4] [2.8 MB]
Creating vector for state rewards... [2.4 MB]
Creating vector for inf... [2.4 MB]
Allocating iteration vectors... [2 x 2.4 MB]
TOTAL: [24.3 MB]

Starting iterations...

Iterative method: 92 iterations in 0.86 seconds (average 0.004609, setup 0.43)

Value in the initial state: 0.007685088600771279

Time for model checking: 1.005 seconds.

Result: 0.007685088600771279 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"time"}max=? [ F "deadlock" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=1,Objy1=0,Objx2=5,Objy2=0

Prob0E: 5 iterations in 0.05 seconds (average 0.010400, setup 0.00)

Prob1A: 1 iterations in 0.01 seconds (average 0.012000, setup 0.00)

goal = 1, inf = 0, maybe = 308899

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=308900, nc=406956, nnz=816197, k=4] [12.1 MB]
Building sparse matrix (transition rewards)... [n=308900, nc=406956, nnz=154100, k=4] [4.5 MB]
Creating vector for state rewards... [2.4 MB]
Creating vector for inf... [2.4 MB]
Allocating iteration vectors... [2 x 2.4 MB]
TOTAL: [26.0 MB]

Starting iterations...

Iterative method: 95 iterations in 0.93 seconds (average 0.004674, setup 0.48)

Value in the initial state: 106.30248205602443

Time for model checking: 1.006 seconds.

Result: 106.30248205602443 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"time"}min=? [ F "deadlock" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=1,Objy1=0,Objx2=5,Objy2=0

Prob0A: 4 iterations in 0.04 seconds (average 0.010000, setup 0.00)

Prob1E: 5 iterations in 0.07 seconds (average 0.013600, setup 0.00)

Warning: PRISM hasn't checked for zero-reward loops.
Your minimum rewards may be too low...

goal = 1, inf = 0, maybe = 308899

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=308900, nc=406956, nnz=816197, k=4] [12.1 MB]
Building sparse matrix (transition rewards)... [n=308900, nc=406956, nnz=154100, k=4] [4.5 MB]
Creating vector for state rewards... [2.4 MB]
Creating vector for inf... [2.4 MB]
Allocating iteration vectors... [2 x 2.4 MB]
TOTAL: [26.0 MB]

Starting iterations...

Iterative method: 95 iterations in 0.92 seconds (average 0.004674, setup 0.47)

Value in the initial state: 69.99527244066064

Time for model checking: 1.04 seconds.

Result: 69.99527244066064 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"success"}max=? [ F "deadlock" ]/Pmin=? [ F "MissionSuccessful" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=1,Objy1=0,Objx2=5,Objy2=0

Prob0E: 5 iterations in 0.04 seconds (average 0.008000, setup 0.00)

Prob1A: 1 iterations in 0.00 seconds (average 0.004000, setup 0.00)

goal = 1, inf = 0, maybe = 308899

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=308900, nc=406956, nnz=816197, k=4] [12.1 MB]
Building sparse matrix (transition rewards)... [n=308900, nc=406956, nnz=2228, k=4] [2.8 MB]
Creating vector for state rewards... [2.4 MB]
Creating vector for inf... [2.4 MB]
Allocating iteration vectors... [2 x 2.4 MB]
TOTAL: [24.3 MB]

Starting iterations...

Iterative method: 89 iterations in 0.83 seconds (average 0.004494, setup 0.43)

Prob0E: 47 iterations in 0.33 seconds (average 0.007064, setup 0.00)

Prob1A: 3 iterations in 0.06 seconds (average 0.018667, setup 0.00)

yes = 1131, no = 268294, maybe = 39475

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=308900, nc=70594, nnz=204458, k=2] [2.7 MB]
Creating vector for yes... [2.4 MB]
Allocating iteration vectors... [2 x 2.4 MB]
TOTAL: [9.8 MB]

Starting iterations...

Iterative method: 82 iterations in 0.38 seconds (average 0.002049, setup 0.22)

Value in the initial state: 112.71234117463898

Time for model checking: 1.696 seconds.

Result: 112.71234117463898 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"success"}min=? [ F "deadlock" ]/Pmax=? [ F "MissionSuccessful" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=1,Objy1=0,Objx2=5,Objy2=0

Prob0A: 4 iterations in 0.12 seconds (average 0.029000, setup 0.00)

Prob1E: 5 iterations in 0.08 seconds (average 0.015200, setup 0.00)

Warning: PRISM hasn't checked for zero-reward loops.
Your minimum rewards may be too low...

goal = 1, inf = 0, maybe = 308899

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=308900, nc=406956, nnz=816197, k=4] [12.1 MB]
Building sparse matrix (transition rewards)... [n=308900, nc=406956, nnz=2228, k=4] [2.8 MB]
Creating vector for state rewards... [2.4 MB]
Creating vector for inf... [2.4 MB]
Allocating iteration vectors... [2 x 2.4 MB]
TOTAL: [24.3 MB]

Starting iterations...

Iterative method: 87 iterations in 0.85 seconds (average 0.004506, setup 0.46)

Prob0A: 47 iterations in 0.32 seconds (average 0.006809, setup 0.00)

Prob1E: 4 iterations in 0.05 seconds (average 0.013000, setup 0.00)

yes = 1131, no = 261350, maybe = 46419

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=308900, nc=83416, nnz=241888, k=2] [3.1 MB]
Creating vector for yes... [2.4 MB]
Allocating iteration vectors... [2 x 2.4 MB]
TOTAL: [10.2 MB]

Starting iterations...

Iterative method: 86 iterations in 0.38 seconds (average 0.002047, setup 0.21)

Value in the initial state: 68.00673830768739

Time for model checking: 1.818 seconds.

Result: 68.00673830768739 (value in the initial state)

---------------------------------------------------------------------

Note: There were 16 warnings during computation.

