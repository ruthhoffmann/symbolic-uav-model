PRISM
=====

Version: 4.3
Date: Wed Aug 03 16:08:38 BST 2016
Hostname: eglinton
Memory limits: cudd=1g, java(heap)=981.5m
Command line: prism uav_sim_mdp.prism -s uav_mdp.props -const 'Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=0,Objy1=3,Objx2=5,Objy2=2,'

Parsing model file "uav_sim_mdp.prism"...

Parsing properties file "uav_mdp.props"...

18 properties:
(1) Pmax=? [ F "MissionSuccessful" ]
(2) Pmin=? [ F "MissionSuccessful" ]
(3) Pmax=? [ F "MissionTimeUp" ]
(4) Pmin=? [ F "MissionTimeUp" ]
(5) Pmax=? [ F "Crash" ]
(6) Pmin=? [ F "Crash" ]
(7) Pmax=? [ F "AllAreaSearched" ]
(8) Pmin=? [ F "AllAreaSearched" ]
(9) Pmax=? [ F "MissionTimeUp"|"AllAreaSearched"|"Crash" ]
(10) Pmin=? [ F "MissionTimeUp"|"AllAreaSearched"|"Crash" ]
(11) R{"drop"}max=? [ F "deadlock" ]
(12) R{"drop"}min=? [ F "deadlock" ]
(13) R{"charge"}max=? [ F "deadlock" ]
(14) R{"charge"}min=? [ F "deadlock" ]
(15) R{"time"}max=? [ F "deadlock" ]
(16) R{"time"}min=? [ F "deadlock" ]
(17) R{"success"}max=? [ F "deadlock" ]/Pmin=? [ F "MissionSuccessful" ]
(18) R{"success"}min=? [ F "deadlock" ]/Pmax=? [ F "MissionSuccessful" ]

Type:        MDP
Modules:     Obj Move Robot T 
Variables:   NoOfObjs posx posy s1 retx rety b t c 

---------------------------------------------------------------------

Model checking: Pmax=? [ F "MissionSuccessful" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=0,Objy1=3,Objx2=5,Objy2=2

Building model...
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=0,Objy1=3,Objx2=5,Objy2=2

Warning: Guard for command 7 of module "Move" is never satisfied.

Warning: Guard for command 8 of module "Move" is never satisfied.

Warning: Guard for command 9 of module "Move" is never satisfied.

Warning: Guard for command 6 of module "Robot" is never satisfied.

Warning: Update 2 of command 3 of module "Robot" doesn't do anything ("(s1'=13)", line 210, column 44)

Warning: Update 2 of command 27 of module "T" doesn't do anything ("(c'=1)", line 323, column 108)

Warning: Guard for command 28 of module "T" is never satisfied.

Warning: Update 2 of command 9 of module "T" doesn't do anything ("(c'=1)", line 300, column 112)

Warning: Guard for command 10 of module "T" is never satisfied.

Warning: Update 2 of command 51 of module "T" doesn't do anything ("(c'=1)", line 354, column 113)

Warning: Guard for command 52 of module "T" is never satisfied.

Computing reachable states...

Reachability (BFS): 88 iterations in 0.98 seconds (average 0.011091, setup 0.00)

Time for model construction: 3.52 seconds.

Warning: Deadlocks detected and fixed in 1 states

Type:        MDP
States:      181756 (1 initial)
Transitions: 525343
Choices:     250861

Transition matrix: 209877 nodes (87 terminal), 525343 minterms, vars: 34r/34c/18nd

Prob0A: 57 iterations in 0.44 seconds (average 0.007789, setup 0.00)

Prob1E: 4 iterations in 0.06 seconds (average 0.016000, setup 0.00)

yes = 873, no = 138441, maybe = 42442

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=181756, nc=75296, nnz=213333, k=2] [2.7 MB]
Creating vector for yes... [1.4 MB]
Allocating iteration vectors... [2 x 1.4 MB]
TOTAL: [6.8 MB]

Starting iterations...

Iterative method: 101 iterations in 0.39 seconds (average 0.001307, setup 0.26)

Value in the initial state: 0.8184382093838146

Time for model checking: 1.118 seconds.

Result: 0.8184382093838146 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmin=? [ F "MissionSuccessful" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=0,Objy1=3,Objx2=5,Objy2=2

Prob0E: 76 iterations in 0.68 seconds (average 0.009000, setup 0.00)

Prob1A: 3 iterations in 0.07 seconds (average 0.024000, setup 0.00)

yes = 873, no = 147487, maybe = 33396

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=181756, nc=58733, nnz=165129, k=2] [2.1 MB]
Creating vector for yes... [1.4 MB]
Allocating iteration vectors... [2 x 1.4 MB]
TOTAL: [6.3 MB]

Starting iterations...

Iterative method: 99 iterations in 0.36 seconds (average 0.001293, setup 0.23)

Value in the initial state: 0.003906506244656097

Time for model checking: 1.28 seconds.

Result: 0.003906506244656097 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmax=? [ F "MissionTimeUp" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=0,Objy1=3,Objx2=5,Objy2=2

Prob0A: 78 iterations in 0.58 seconds (average 0.007436, setup 0.00)

Prob1E: 4 iterations in 0.05 seconds (average 0.012000, setup 0.00)

yes = 228, no = 145891, maybe = 35637

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=181756, nc=64077, nnz=180706, k=4] [2.3 MB]
Creating vector for yes... [1.4 MB]
Allocating iteration vectors... [2 x 1.4 MB]
TOTAL: [6.5 MB]

Starting iterations...

Iterative method: 98 iterations in 0.33 seconds (average 0.001306, setup 0.20)

Value in the initial state: 1.9775225525194588E-5

Time for model checking: 1.091 seconds.

Result: 1.9775225525194588E-5 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmin=? [ F "MissionTimeUp" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=0,Objy1=3,Objx2=5,Objy2=2

Prob0E: 92 iterations in 0.73 seconds (average 0.007957, setup 0.00)

Prob1A: 3 iterations in 0.05 seconds (average 0.017333, setup 0.00)

yes = 228, no = 146160, maybe = 35368

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=181756, nc=63595, nnz=179669, k=4] [2.3 MB]
Creating vector for yes... [1.4 MB]
Allocating iteration vectors... [2 x 1.4 MB]
TOTAL: [6.5 MB]

Starting iterations...

Iterative method: 115 iterations in 0.32 seconds (average 0.001287, setup 0.17)

Value in the initial state: 2.039399982806722E-8

Time for model checking: 1.2 seconds.

Result: 2.039399982806722E-8 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmax=? [ F "Crash" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=0,Objy1=3,Objx2=5,Objy2=2

Prob0A: 3 iterations in 0.12 seconds (average 0.038667, setup 0.00)

Prob1E: 204 iterations in 3.10 seconds (average 0.015176, setup 0.00)

yes = 88739, no = 1342, maybe = 91675

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=181756, nc=160780, nnz=435262, k=4] [5.3 MB]
Creating vector for yes... [1.4 MB]
Allocating iteration vectors... [2 x 1.4 MB]
TOTAL: [9.5 MB]

Starting iterations...

Iterative method: 95 iterations in 0.49 seconds (average 0.001811, setup 0.32)

Value in the initial state: 0.019471565085469992

Time for model checking: 3.973 seconds.

Result: 0.019471565085469992 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmin=? [ F "Crash" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=0,Objy1=3,Objx2=5,Objy2=2

Prob0E: 3 iterations in 0.10 seconds (average 0.032000, setup 0.00)

Prob1A: 53 iterations in 0.63 seconds (average 0.011849, setup 0.00)

yes = 88739, no = 1342, maybe = 91675

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=181756, nc=160780, nnz=435262, k=4] [5.3 MB]
Creating vector for yes... [1.4 MB]
Allocating iteration vectors... [2 x 1.4 MB]
TOTAL: [9.5 MB]

Starting iterations...

Iterative method: 95 iterations in 0.40 seconds (average 0.001853, setup 0.22)

Value in the initial state: 0.01612043090619653

Time for model checking: 1.174 seconds.

Result: 0.01612043090619653 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmax=? [ F "AllAreaSearched" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=0,Objy1=3,Objx2=5,Objy2=2

Prob0A: 53 iterations in 0.58 seconds (average 0.010868, setup 0.00)

Prob1E: 4 iterations in 0.06 seconds (average 0.015000, setup 0.00)

yes = 240, no = 111202, maybe = 70314

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=181756, nc=122111, nnz=340548, k=2] [4.2 MB]
Creating vector for yes... [1.4 MB]
Allocating iteration vectors... [2 x 1.4 MB]
TOTAL: [8.3 MB]

Starting iterations...

Iterative method: 96 iterations in 0.50 seconds (average 0.001625, setup 0.34)

Value in the initial state: 0.9787182235852505

Time for model checking: 1.191 seconds.

Result: 0.9787182235852505 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmin=? [ F "AllAreaSearched" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=0,Objy1=3,Objx2=5,Objy2=2

Prob0E: 61 iterations in 0.58 seconds (average 0.009574, setup 0.00)

Prob1A: 3 iterations in 0.05 seconds (average 0.017333, setup 0.00)

yes = 240, no = 113723, maybe = 67793

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=181756, nc=117335, nnz=326381, k=2] [4.0 MB]
Creating vector for yes... [1.4 MB]
Allocating iteration vectors... [2 x 1.4 MB]
TOTAL: [8.2 MB]

Starting iterations...

Iterative method: 104 iterations in 0.43 seconds (average 0.001654, setup 0.26)

Value in the initial state: 0.16456910591682553

Time for model checking: 1.12 seconds.

Result: 0.16456910591682553 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmax=? [ F "MissionTimeUp"|"AllAreaSearched"|"Crash" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=0,Objy1=3,Objx2=5,Objy2=2

Prob0A: 3 iterations in 0.10 seconds (average 0.032000, setup 0.00)

Prob1E: 285 iterations in 3.93 seconds (average 0.013782, setup 0.00)

yes = 147486, no = 874, maybe = 33396

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=181756, nc=58733, nnz=165129, k=2] [2.1 MB]
Creating vector for yes... [1.4 MB]
Allocating iteration vectors... [2 x 1.4 MB]
TOTAL: [6.3 MB]

Starting iterations...

Iterative method: 89 iterations in 0.37 seconds (average 0.001258, setup 0.26)

Value in the initial state: 0.9960926472275615

Time for model checking: 4.598 seconds.

Result: 0.9960926472275615 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmin=? [ F "MissionTimeUp"|"AllAreaSearched"|"Crash" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=0,Objy1=3,Objx2=5,Objy2=2

Prob0E: 3 iterations in 0.07 seconds (average 0.024000, setup 0.00)

Prob1A: 57 iterations in 0.57 seconds (average 0.010035, setup 0.00)

yes = 138440, no = 874, maybe = 42442

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=181756, nc=75296, nnz=213333, k=2] [2.7 MB]
Creating vector for yes... [1.4 MB]
Allocating iteration vectors... [2 x 1.4 MB]
TOTAL: [6.8 MB]

Starting iterations...

Iterative method: 93 iterations in 0.35 seconds (average 0.001419, setup 0.22)

Value in the initial state: 0.18156074096241429

Time for model checking: 1.029 seconds.

Result: 0.18156074096241429 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"drop"}max=? [ F "deadlock" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=0,Objy1=3,Objx2=5,Objy2=2

Prob0E: 4 iterations in 0.20 seconds (average 0.050000, setup 0.00)

Prob1A: 1 iterations in 0.02 seconds (average 0.016000, setup 0.00)

goal = 1, inf = 0, maybe = 181755

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=181756, nc=250860, nnz=525342, k=4] [7.7 MB]
Building sparse matrix (transition rewards)... [n=181756, nc=250860, nnz=0, k=4] [1.7 MB]
Creating vector for state rewards... [1.4 MB]
Creating vector for inf... [1.4 MB]
Allocating iteration vectors... [2 x 1.4 MB]
TOTAL: [14.9 MB]

Starting iterations...

Iterative method: 99 iterations in 0.67 seconds (average 0.002747, setup 0.40)

Value in the initial state: 0.18854092978725298

Time for model checking: 0.915 seconds.

Result: 0.18854092978725298 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"drop"}min=? [ F "deadlock" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=0,Objy1=3,Objx2=5,Objy2=2

Prob0A: 4 iterations in 0.05 seconds (average 0.013000, setup 0.00)

Prob1E: 5 iterations in 0.06 seconds (average 0.012800, setup 0.00)

Warning: PRISM hasn't checked for zero-reward loops.
Your minimum rewards may be too low...

goal = 1, inf = 0, maybe = 181755

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=181756, nc=250860, nnz=525342, k=4] [7.7 MB]
Building sparse matrix (transition rewards)... [n=181756, nc=250860, nnz=0, k=4] [1.7 MB]
Creating vector for state rewards... [1.4 MB]
Creating vector for inf... [1.4 MB]
Allocating iteration vectors... [2 x 1.4 MB]
TOTAL: [14.9 MB]

Starting iterations...

Iterative method: 90 iterations in 0.54 seconds (average 0.002800, setup 0.28)

Value in the initial state: 0.16846792711321798

Time for model checking: 0.659 seconds.

Result: 0.16846792711321798 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"charge"}max=? [ F "deadlock" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=0,Objy1=3,Objx2=5,Objy2=2

Prob0E: 4 iterations in 0.03 seconds (average 0.008000, setup 0.00)

Prob1A: 1 iterations in 0.01 seconds (average 0.008000, setup 0.00)

goal = 1, inf = 0, maybe = 181755

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=181756, nc=250860, nnz=525342, k=4] [7.7 MB]
Building sparse matrix (transition rewards)... [n=181756, nc=250860, nnz=6870, k=4] [1.7 MB]
Creating vector for state rewards... [1.4 MB]
Creating vector for inf... [1.4 MB]
Allocating iteration vectors... [2 x 1.4 MB]
TOTAL: [14.9 MB]

Starting iterations...

Iterative method: 94 iterations in 0.54 seconds (average 0.002809, setup 0.28)

Value in the initial state: 0.9841197028077353

Time for model checking: 0.596 seconds.

Result: 0.9841197028077353 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"charge"}min=? [ F "deadlock" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=0,Objy1=3,Objx2=5,Objy2=2

Prob0A: 4 iterations in 0.03 seconds (average 0.008000, setup 0.00)

Prob1E: 5 iterations in 0.06 seconds (average 0.012000, setup 0.00)

Warning: PRISM hasn't checked for zero-reward loops.
Your minimum rewards may be too low...

goal = 1, inf = 0, maybe = 181755

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=181756, nc=250860, nnz=525342, k=4] [7.7 MB]
Building sparse matrix (transition rewards)... [n=181756, nc=250860, nnz=6870, k=4] [1.7 MB]
Creating vector for state rewards... [1.4 MB]
Creating vector for inf... [1.4 MB]
Allocating iteration vectors... [2 x 1.4 MB]
TOTAL: [14.9 MB]

Starting iterations...

Iterative method: 102 iterations in 0.56 seconds (average 0.002824, setup 0.28)

Value in the initial state: 0.1635525164794652

Time for model checking: 0.673 seconds.

Result: 0.1635525164794652 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"time"}max=? [ F "deadlock" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=0,Objy1=3,Objx2=5,Objy2=2

Prob0E: 4 iterations in 0.03 seconds (average 0.007000, setup 0.00)

Prob1A: 1 iterations in 0.01 seconds (average 0.008000, setup 0.00)

goal = 1, inf = 0, maybe = 181755

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=181756, nc=250860, nnz=525342, k=4] [7.7 MB]
Building sparse matrix (transition rewards)... [n=181756, nc=250860, nnz=90079, k=4] [2.7 MB]
Creating vector for state rewards... [1.4 MB]
Creating vector for inf... [1.4 MB]
Allocating iteration vectors... [2 x 1.4 MB]
TOTAL: [15.9 MB]

Starting iterations...

Iterative method: 97 iterations in 0.58 seconds (average 0.002763, setup 0.31)

Value in the initial state: 108.02867203373283

Time for model checking: 0.637 seconds.

Result: 108.02867203373283 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"time"}min=? [ F "deadlock" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=0,Objy1=3,Objx2=5,Objy2=2

Prob0A: 4 iterations in 0.03 seconds (average 0.008000, setup 0.00)

Prob1E: 5 iterations in 0.06 seconds (average 0.012800, setup 0.00)

Warning: PRISM hasn't checked for zero-reward loops.
Your minimum rewards may be too low...

goal = 1, inf = 0, maybe = 181755

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=181756, nc=250860, nnz=525342, k=4] [7.7 MB]
Building sparse matrix (transition rewards)... [n=181756, nc=250860, nnz=90079, k=4] [2.7 MB]
Creating vector for state rewards... [1.4 MB]
Creating vector for inf... [1.4 MB]
Allocating iteration vectors... [2 x 1.4 MB]
TOTAL: [15.9 MB]

Starting iterations...

Iterative method: 107 iterations in 0.60 seconds (average 0.002841, setup 0.30)

Value in the initial state: 89.51597857012287

Time for model checking: 0.718 seconds.

Result: 89.51597857012287 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"success"}max=? [ F "deadlock" ]/Pmin=? [ F "MissionSuccessful" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=0,Objy1=3,Objx2=5,Objy2=2

Prob0E: 4 iterations in 0.03 seconds (average 0.008000, setup 0.00)

Prob1A: 1 iterations in 0.00 seconds (average 0.004000, setup 0.00)

goal = 1, inf = 0, maybe = 181755

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=181756, nc=250860, nnz=525342, k=4] [7.7 MB]
Building sparse matrix (transition rewards)... [n=181756, nc=250860, nnz=1728, k=4] [1.7 MB]
Creating vector for state rewards... [1.4 MB]
Creating vector for inf... [1.4 MB]
Allocating iteration vectors... [2 x 1.4 MB]
TOTAL: [14.9 MB]

Starting iterations...

Iterative method: 104 iterations in 0.57 seconds (average 0.002731, setup 0.28)

Prob0E: 76 iterations in 0.52 seconds (average 0.006789, setup 0.00)

Prob1A: 3 iterations in 0.05 seconds (average 0.016000, setup 0.00)

yes = 873, no = 147487, maybe = 33396

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=181756, nc=58733, nnz=165129, k=2] [2.1 MB]
Creating vector for yes... [1.4 MB]
Allocating iteration vectors... [2 x 1.4 MB]
TOTAL: [6.3 MB]

Starting iterations...

Iterative method: 99 iterations in 0.31 seconds (average 0.001333, setup 0.18)

Value in the initial state: 22209.329285083437

Time for model checking: 1.518 seconds.

Result: 22209.329285083437 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"success"}min=? [ F "deadlock" ]/Pmax=? [ F "MissionSuccessful" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=0,Objy1=3,Objx2=5,Objy2=2

Prob0A: 4 iterations in 0.10 seconds (average 0.024000, setup 0.00)

Prob1E: 5 iterations in 0.06 seconds (average 0.012800, setup 0.00)

Warning: PRISM hasn't checked for zero-reward loops.
Your minimum rewards may be too low...

goal = 1, inf = 0, maybe = 181755

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=181756, nc=250860, nnz=525342, k=4] [7.7 MB]
Building sparse matrix (transition rewards)... [n=181756, nc=250860, nnz=1728, k=4] [1.7 MB]
Creating vector for state rewards... [1.4 MB]
Creating vector for inf... [1.4 MB]
Allocating iteration vectors... [2 x 1.4 MB]
TOTAL: [14.9 MB]

Starting iterations...

Iterative method: 103 iterations in 0.60 seconds (average 0.002757, setup 0.31)

Prob0A: 57 iterations in 0.37 seconds (average 0.006456, setup 0.00)

Prob1E: 4 iterations in 0.04 seconds (average 0.009000, setup 0.00)

yes = 873, no = 138441, maybe = 42442

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=181756, nc=75296, nnz=213333, k=2] [2.7 MB]
Creating vector for yes... [1.4 MB]
Allocating iteration vectors... [2 x 1.4 MB]
TOTAL: [6.8 MB]

Starting iterations...

Iterative method: 101 iterations in 0.33 seconds (average 0.001347, setup 0.19)

Value in the initial state: 1.6619733948936317

Time for model checking: 1.503 seconds.

Result: 1.6619733948936317 (value in the initial state)

---------------------------------------------------------------------

Note: There were 16 warnings during computation.

