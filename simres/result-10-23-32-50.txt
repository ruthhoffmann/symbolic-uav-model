PRISM
=====

Version: 4.3
Date: Wed Aug 03 16:14:17 BST 2016
Hostname: eglinton
Memory limits: cudd=1g, java(heap)=981.5m
Command line: prism uav_sim_mdp.prism -s uav_mdp.props -const 'Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=3,Objy1=2,Objx2=5,Objy2=0,'

Parsing model file "uav_sim_mdp.prism"...

Parsing properties file "uav_mdp.props"...

18 properties:
(1) Pmax=? [ F "MissionSuccessful" ]
(2) Pmin=? [ F "MissionSuccessful" ]
(3) Pmax=? [ F "MissionTimeUp" ]
(4) Pmin=? [ F "MissionTimeUp" ]
(5) Pmax=? [ F "Crash" ]
(6) Pmin=? [ F "Crash" ]
(7) Pmax=? [ F "AllAreaSearched" ]
(8) Pmin=? [ F "AllAreaSearched" ]
(9) Pmax=? [ F "MissionTimeUp"|"AllAreaSearched"|"Crash" ]
(10) Pmin=? [ F "MissionTimeUp"|"AllAreaSearched"|"Crash" ]
(11) R{"drop"}max=? [ F "deadlock" ]
(12) R{"drop"}min=? [ F "deadlock" ]
(13) R{"charge"}max=? [ F "deadlock" ]
(14) R{"charge"}min=? [ F "deadlock" ]
(15) R{"time"}max=? [ F "deadlock" ]
(16) R{"time"}min=? [ F "deadlock" ]
(17) R{"success"}max=? [ F "deadlock" ]/Pmin=? [ F "MissionSuccessful" ]
(18) R{"success"}min=? [ F "deadlock" ]/Pmax=? [ F "MissionSuccessful" ]

Type:        MDP
Modules:     Obj Move Robot T 
Variables:   NoOfObjs posx posy s1 retx rety b t c 

---------------------------------------------------------------------

Model checking: Pmax=? [ F "MissionSuccessful" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=3,Objy1=2,Objx2=5,Objy2=0

Building model...
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=3,Objy1=2,Objx2=5,Objy2=0

Warning: Guard for command 7 of module "Move" is never satisfied.

Warning: Guard for command 8 of module "Move" is never satisfied.

Warning: Guard for command 9 of module "Move" is never satisfied.

Warning: Guard for command 7 of module "Robot" is never satisfied.

Warning: Update 2 of command 3 of module "Robot" doesn't do anything ("(s1'=13)", line 210, column 44)

Warning: Update 2 of command 27 of module "T" doesn't do anything ("(c'=1)", line 323, column 108)

Warning: Guard for command 28 of module "T" is never satisfied.

Warning: Update 2 of command 9 of module "T" doesn't do anything ("(c'=1)", line 300, column 112)

Warning: Guard for command 10 of module "T" is never satisfied.

Warning: Update 2 of command 51 of module "T" doesn't do anything ("(c'=1)", line 354, column 113)

Warning: Guard for command 52 of module "T" is never satisfied.

Computing reachable states...

Reachability (BFS): 87 iterations in 1.58 seconds (average 0.018207, setup 0.00)

Time for model construction: 3.606 seconds.

Warning: Deadlocks detected and fixed in 1 states

Type:        MDP
States:      234033 (1 initial)
Transitions: 594725
Choices:     305005

Transition matrix: 220503 nodes (89 terminal), 594725 minterms, vars: 34r/34c/18nd

Prob0A: 51 iterations in 0.49 seconds (average 0.009569, setup 0.00)

Prob1E: 4 iterations in 0.06 seconds (average 0.016000, setup 0.00)

yes = 1124, no = 198420, maybe = 34489

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=234033, nc=59324, nnz=163753, k=2] [2.2 MB]
Creating vector for yes... [1.8 MB]
Allocating iteration vectors... [2 x 1.8 MB]
TOTAL: [7.5 MB]

Starting iterations...

Iterative method: 92 iterations in 0.41 seconds (average 0.001652, setup 0.26)

Value in the initial state: 0.9824761384469828

Time for model checking: 1.002 seconds.

Result: 0.9824761384469828 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmin=? [ F "MissionSuccessful" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=3,Objy1=2,Objx2=5,Objy2=0

Prob0E: 53 iterations in 0.66 seconds (average 0.012453, setup 0.00)

Prob1A: 3 iterations in 0.08 seconds (average 0.026667, setup 0.00)

yes = 1124, no = 204041, maybe = 28868

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=234033, nc=49550, nnz=135511, k=2] [1.8 MB]
Creating vector for yes... [1.8 MB]
Allocating iteration vectors... [2 x 1.8 MB]
TOTAL: [7.2 MB]

Starting iterations...

Iterative method: 88 iterations in 0.49 seconds (average 0.002455, setup 0.28)

Value in the initial state: 0.8388814962874376

Time for model checking: 1.23 seconds.

Result: 0.8388814962874376 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmax=? [ F "MissionTimeUp" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=3,Objy1=2,Objx2=5,Objy2=0

Prob0A: 68 iterations in 0.74 seconds (average 0.010941, setup 0.00)

Prob1E: 4 iterations in 0.06 seconds (average 0.014000, setup 0.00)

yes = 404, no = 145119, maybe = 88510

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=234033, nc=147328, nnz=394142, k=4] [4.9 MB]
Creating vector for yes... [1.8 MB]
Allocating iteration vectors... [2 x 1.8 MB]
TOTAL: [10.2 MB]

Starting iterations...

Iterative method: 94 iterations in 0.49 seconds (average 0.001957, setup 0.31)

Value in the initial state: 1.4347146974710417E-4

Time for model checking: 1.311 seconds.

Result: 1.4347146974710417E-4 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmin=? [ F "MissionTimeUp" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=3,Objy1=2,Objx2=5,Objy2=0

Prob0E: 83 iterations in 0.98 seconds (average 0.011855, setup 0.00)

Prob1A: 3 iterations in 0.05 seconds (average 0.017333, setup 0.00)

yes = 404, no = 145465, maybe = 88164

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=234033, nc=146726, nnz=392744, k=4] [4.9 MB]
Creating vector for yes... [1.8 MB]
Allocating iteration vectors... [2 x 1.8 MB]
TOTAL: [10.2 MB]

Starting iterations...

Iterative method: 110 iterations in 0.63 seconds (average 0.003200, setup 0.28)

Value in the initial state: 2.768024543134056E-7

Time for model checking: 1.698 seconds.

Result: 2.768024543134056E-7 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmax=? [ F "Crash" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=3,Objy1=2,Objx2=5,Objy2=0

Prob0A: 3 iterations in 0.09 seconds (average 0.029333, setup 0.00)

Prob1E: 195 iterations in 2.70 seconds (average 0.013846, setup 0.00)

yes = 114809, no = 3081, maybe = 116143

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=234033, nc=187115, nnz=476835, k=4] [5.9 MB]
Creating vector for yes... [1.8 MB]
Allocating iteration vectors... [2 x 1.8 MB]
TOTAL: [11.2 MB]

Starting iterations...

Iterative method: 90 iterations in 0.64 seconds (average 0.002978, setup 0.37)

Value in the initial state: 0.029080820066164224

Time for model checking: 3.457 seconds.

Result: 0.029080820066164224 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmin=? [ F "Crash" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=3,Objy1=2,Objx2=5,Objy2=0

Prob0E: 3 iterations in 0.08 seconds (average 0.028000, setup 0.00)

Prob1A: 46 iterations in 0.55 seconds (average 0.012000, setup 0.00)

yes = 114809, no = 3081, maybe = 116143

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=234033, nc=187115, nnz=476835, k=4] [5.9 MB]
Creating vector for yes... [1.8 MB]
Allocating iteration vectors... [2 x 1.8 MB]
TOTAL: [11.2 MB]

Starting iterations...

Iterative method: 82 iterations in 0.45 seconds (average 0.002244, setup 0.26)

Value in the initial state: 0.015491041953115356

Time for model checking: 1.103 seconds.

Result: 0.015491041953115356 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmax=? [ F "AllAreaSearched" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=3,Objy1=2,Objx2=5,Objy2=0

Prob0A: 58 iterations in 0.47 seconds (average 0.008138, setup 0.00)

Prob1E: 4 iterations in 0.05 seconds (average 0.013000, setup 0.00)

yes = 1552, no = 182063, maybe = 50418

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=234033, nc=76269, nnz=192672, k=2] [2.5 MB]
Creating vector for yes... [1.8 MB]
Allocating iteration vectors... [2 x 1.8 MB]
TOTAL: [7.9 MB]

Starting iterations...

Iterative method: 86 iterations in 0.34 seconds (average 0.001674, setup 0.20)

Value in the initial state: 0.13565075042600994

Time for model checking: 0.887 seconds.

Result: 0.13565075042600994 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmin=? [ F "AllAreaSearched" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=3,Objy1=2,Objx2=5,Objy2=0

Prob0E: 62 iterations in 0.50 seconds (average 0.008129, setup 0.00)

Prob1A: 3 iterations in 0.05 seconds (average 0.016000, setup 0.00)

yes = 1552, no = 188361, maybe = 44120

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=234033, nc=66339, nnz=166248, k=2] [2.2 MB]
Creating vector for yes... [1.8 MB]
Allocating iteration vectors... [2 x 1.8 MB]
TOTAL: [7.5 MB]

Starting iterations...

Iterative method: 92 iterations in 0.29 seconds (average 0.001609, setup 0.14)

Value in the initial state: 0.001768573834337778

Time for model checking: 0.856 seconds.

Result: 0.001768573834337778 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmax=? [ F "MissionTimeUp"|"AllAreaSearched"|"Crash" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=3,Objy1=2,Objx2=5,Objy2=0

Prob0A: 3 iterations in 0.08 seconds (average 0.026667, setup 0.00)

Prob1E: 204 iterations in 2.55 seconds (average 0.012510, setup 0.00)

yes = 204040, no = 1125, maybe = 28868

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=234033, nc=49550, nnz=135511, k=2] [1.8 MB]
Creating vector for yes... [1.8 MB]
Allocating iteration vectors... [2 x 1.8 MB]
TOTAL: [7.2 MB]

Starting iterations...

Iterative method: 81 iterations in 0.31 seconds (average 0.001531, setup 0.18)

Value in the initial state: 0.16111829631925007

Time for model checking: 2.972 seconds.

Result: 0.16111829631925007 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmin=? [ F "MissionTimeUp"|"AllAreaSearched"|"Crash" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=3,Objy1=2,Objx2=5,Objy2=0

Prob0E: 3 iterations in 0.06 seconds (average 0.020000, setup 0.00)

Prob1A: 51 iterations in 0.48 seconds (average 0.009490, setup 0.00)

yes = 198419, no = 1125, maybe = 34489

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=234033, nc=59324, nnz=163753, k=2] [2.2 MB]
Creating vector for yes... [1.8 MB]
Allocating iteration vectors... [2 x 1.8 MB]
TOTAL: [7.5 MB]

Starting iterations...

Iterative method: 91 iterations in 0.32 seconds (average 0.001626, setup 0.17)

Value in the initial state: 0.017522865758504546

Time for model checking: 0.89 seconds.

Result: 0.017522865758504546 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"drop"}max=? [ F "deadlock" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=3,Objy1=2,Objx2=5,Objy2=0

Prob0E: 4 iterations in 0.07 seconds (average 0.018000, setup 0.00)

Prob1A: 1 iterations in 0.01 seconds (average 0.008000, setup 0.00)

goal = 1, inf = 0, maybe = 234032

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=234033, nc=305004, nnz=594724, k=4] [8.9 MB]
Building sparse matrix (transition rewards)... [n=234033, nc=305004, nnz=0, k=4] [2.1 MB]
Creating vector for state rewards... [1.8 MB]
Creating vector for inf... [1.8 MB]
Allocating iteration vectors... [2 x 1.8 MB]
TOTAL: [18.1 MB]

Starting iterations...

Iterative method: 88 iterations in 0.71 seconds (average 0.003318, setup 0.42)

Value in the initial state: 0.17663968150195383

Time for model checking: 0.813 seconds.

Result: 0.17663968150195383 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"drop"}min=? [ F "deadlock" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=3,Objy1=2,Objx2=5,Objy2=0

Prob0A: 4 iterations in 0.04 seconds (average 0.009000, setup 0.00)

Prob1E: 5 iterations in 0.05 seconds (average 0.009600, setup 0.00)

Warning: PRISM hasn't checked for zero-reward loops.
Your minimum rewards may be too low...

goal = 1, inf = 0, maybe = 234032

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=234033, nc=305004, nnz=594724, k=4] [8.9 MB]
Building sparse matrix (transition rewards)... [n=234033, nc=305004, nnz=0, k=4] [2.1 MB]
Creating vector for state rewards... [1.8 MB]
Creating vector for inf... [1.8 MB]
Allocating iteration vectors... [2 x 1.8 MB]
TOTAL: [18.1 MB]

Starting iterations...

Iterative method: 80 iterations in 0.58 seconds (average 0.003300, setup 0.32)

Value in the initial state: 0.16333677818474465

Time for model checking: 0.688 seconds.

Result: 0.16333677818474465 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"charge"}max=? [ F "deadlock" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=3,Objy1=2,Objx2=5,Objy2=0

Prob0E: 4 iterations in 0.03 seconds (average 0.007000, setup 0.00)

Prob1A: 1 iterations in 0.00 seconds (average 0.004000, setup 0.00)

goal = 1, inf = 0, maybe = 234032

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=234033, nc=305004, nnz=594724, k=4] [8.9 MB]
Building sparse matrix (transition rewards)... [n=234033, nc=305004, nnz=6061, k=4] [2.1 MB]
Creating vector for state rewards... [1.8 MB]
Creating vector for inf... [1.8 MB]
Allocating iteration vectors... [2 x 1.8 MB]
TOTAL: [18.1 MB]

Starting iterations...

Iterative method: 87 iterations in 0.61 seconds (average 0.003356, setup 0.32)

Value in the initial state: 0.2724431547993035

Time for model checking: 0.657 seconds.

Result: 0.2724431547993035 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"charge"}min=? [ F "deadlock" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=3,Objy1=2,Objx2=5,Objy2=0

Prob0A: 4 iterations in 0.03 seconds (average 0.007000, setup 0.00)

Prob1E: 5 iterations in 0.05 seconds (average 0.009600, setup 0.00)

Warning: PRISM hasn't checked for zero-reward loops.
Your minimum rewards may be too low...

goal = 1, inf = 0, maybe = 234032

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=234033, nc=305004, nnz=594724, k=4] [8.9 MB]
Building sparse matrix (transition rewards)... [n=234033, nc=305004, nnz=6061, k=4] [2.1 MB]
Creating vector for state rewards... [1.8 MB]
Creating vector for inf... [1.8 MB]
Allocating iteration vectors... [2 x 1.8 MB]
TOTAL: [18.1 MB]

Starting iterations...

Iterative method: 95 iterations in 0.64 seconds (average 0.003326, setup 0.33)

Value in the initial state: 0.02476407371520913

Time for model checking: 0.729 seconds.

Result: 0.02476407371520913 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"time"}max=? [ F "deadlock" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=3,Objy1=2,Objx2=5,Objy2=0

Prob0E: 4 iterations in 0.02 seconds (average 0.006000, setup 0.00)

Prob1A: 1 iterations in 0.00 seconds (average 0.004000, setup 0.00)

goal = 1, inf = 0, maybe = 234032

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=234033, nc=305004, nnz=594724, k=4] [8.9 MB]
Building sparse matrix (transition rewards)... [n=234033, nc=305004, nnz=117888, k=4] [3.4 MB]
Creating vector for state rewards... [1.8 MB]
Creating vector for inf... [1.8 MB]
Allocating iteration vectors... [2 x 1.8 MB]
TOTAL: [19.4 MB]

Starting iterations...

Iterative method: 92 iterations in 0.68 seconds (average 0.003391, setup 0.36)

Value in the initial state: 160.53156423645552

Time for model checking: 0.719 seconds.

Result: 160.53156423645552 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"time"}min=? [ F "deadlock" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=3,Objy1=2,Objx2=5,Objy2=0

Prob0A: 4 iterations in 0.02 seconds (average 0.005000, setup 0.00)

Prob1E: 5 iterations in 0.04 seconds (average 0.008000, setup 0.00)

Warning: PRISM hasn't checked for zero-reward loops.
Your minimum rewards may be too low...

goal = 1, inf = 0, maybe = 234032

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=234033, nc=305004, nnz=594724, k=4] [8.9 MB]
Building sparse matrix (transition rewards)... [n=234033, nc=305004, nnz=117888, k=4] [3.4 MB]
Creating vector for state rewards... [1.8 MB]
Creating vector for inf... [1.8 MB]
Allocating iteration vectors... [2 x 1.8 MB]
TOTAL: [19.4 MB]

Starting iterations...

Iterative method: 98 iterations in 0.69 seconds (average 0.003429, setup 0.36)

Value in the initial state: 85.93440792531047

Time for model checking: 0.764 seconds.

Result: 85.93440792531047 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"success"}max=? [ F "deadlock" ]/Pmin=? [ F "MissionSuccessful" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=3,Objy1=2,Objx2=5,Objy2=0

Prob0E: 4 iterations in 0.02 seconds (average 0.006000, setup 0.00)

Prob1A: 1 iterations in 0.00 seconds (average 0.004000, setup 0.00)

goal = 1, inf = 0, maybe = 234032

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=234033, nc=305004, nnz=594724, k=4] [8.9 MB]
Building sparse matrix (transition rewards)... [n=234033, nc=305004, nnz=2216, k=4] [2.1 MB]
Creating vector for state rewards... [1.8 MB]
Creating vector for inf... [1.8 MB]
Allocating iteration vectors... [2 x 1.8 MB]
TOTAL: [18.1 MB]

Starting iterations...

Iterative method: 95 iterations in 0.62 seconds (average 0.003284, setup 0.31)

Prob0E: 53 iterations in 0.30 seconds (average 0.005660, setup 0.00)

Prob1A: 3 iterations in 0.04 seconds (average 0.014667, setup 0.00)

yes = 1124, no = 204041, maybe = 28868

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=234033, nc=49550, nnz=135511, k=2] [1.8 MB]
Creating vector for yes... [1.8 MB]
Allocating iteration vectors... [2 x 1.8 MB]
TOTAL: [7.2 MB]

Starting iterations...

Iterative method: 88 iterations in 0.29 seconds (average 0.001545, setup 0.15)

Value in the initial state: 187.8284723740782

Time for model checking: 1.357 seconds.

Result: 187.8284723740782 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"success"}min=? [ F "deadlock" ]/Pmax=? [ F "MissionSuccessful" ]
Model constants: Basex=1,Basey=0,Depotx=2,Depoty=3,Objx1=3,Objy1=2,Objx2=5,Objy2=0

Prob0A: 4 iterations in 0.08 seconds (average 0.019000, setup 0.00)

Prob1E: 5 iterations in 0.06 seconds (average 0.012000, setup 0.00)

Warning: PRISM hasn't checked for zero-reward loops.
Your minimum rewards may be too low...

goal = 1, inf = 0, maybe = 234032

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=234033, nc=305004, nnz=594724, k=4] [8.9 MB]
Building sparse matrix (transition rewards)... [n=234033, nc=305004, nnz=2216, k=4] [2.1 MB]
Creating vector for state rewards... [1.8 MB]
Creating vector for inf... [1.8 MB]
Allocating iteration vectors... [2 x 1.8 MB]
TOTAL: [18.1 MB]

Starting iterations...

Iterative method: 91 iterations in 0.86 seconds (average 0.004132, setup 0.48)

Prob0A: 51 iterations in 0.50 seconds (average 0.009804, setup 0.00)

Prob1E: 4 iterations in 0.06 seconds (average 0.015000, setup 0.00)

yes = 1124, no = 198420, maybe = 34489

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=234033, nc=59324, nnz=163753, k=2] [2.2 MB]
Creating vector for yes... [1.8 MB]
Allocating iteration vectors... [2 x 1.8 MB]
TOTAL: [7.5 MB]

Starting iterations...

Iterative method: 92 iterations in 0.30 seconds (average 0.001565, setup 0.16)

Value in the initial state: 77.92192146822816

Time for model checking: 1.899 seconds.

Result: 77.92192146822816 (value in the initial state)

---------------------------------------------------------------------

Note: There were 16 warnings during computation.

