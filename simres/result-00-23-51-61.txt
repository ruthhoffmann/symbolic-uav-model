PRISM
=====

Version: 4.3
Date: Wed Aug 03 16:03:39 BST 2016
Hostname: eglinton
Memory limits: cudd=1g, java(heap)=981.5m
Command line: prism uav_sim_mdp.prism -s uav_mdp.props -const 'Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=5,Objy1=1,Objx2=6,Objy2=1,'

Parsing model file "uav_sim_mdp.prism"...

Parsing properties file "uav_mdp.props"...

18 properties:
(1) Pmax=? [ F "MissionSuccessful" ]
(2) Pmin=? [ F "MissionSuccessful" ]
(3) Pmax=? [ F "MissionTimeUp" ]
(4) Pmin=? [ F "MissionTimeUp" ]
(5) Pmax=? [ F "Crash" ]
(6) Pmin=? [ F "Crash" ]
(7) Pmax=? [ F "AllAreaSearched" ]
(8) Pmin=? [ F "AllAreaSearched" ]
(9) Pmax=? [ F "MissionTimeUp"|"AllAreaSearched"|"Crash" ]
(10) Pmin=? [ F "MissionTimeUp"|"AllAreaSearched"|"Crash" ]
(11) R{"drop"}max=? [ F "deadlock" ]
(12) R{"drop"}min=? [ F "deadlock" ]
(13) R{"charge"}max=? [ F "deadlock" ]
(14) R{"charge"}min=? [ F "deadlock" ]
(15) R{"time"}max=? [ F "deadlock" ]
(16) R{"time"}min=? [ F "deadlock" ]
(17) R{"success"}max=? [ F "deadlock" ]/Pmin=? [ F "MissionSuccessful" ]
(18) R{"success"}min=? [ F "deadlock" ]/Pmax=? [ F "MissionSuccessful" ]

Type:        MDP
Modules:     Obj Move Robot T 
Variables:   NoOfObjs posx posy s1 retx rety b t c 

---------------------------------------------------------------------

Model checking: Pmax=? [ F "MissionSuccessful" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=5,Objy1=1,Objx2=6,Objy2=1

Building model...
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=5,Objy1=1,Objx2=6,Objy2=1

Warning: Guard for command 7 of module "Move" is never satisfied.

Warning: Guard for command 8 of module "Move" is never satisfied.

Warning: Guard for command 9 of module "Move" is never satisfied.

Warning: Guard for command 7 of module "Robot" is never satisfied.

Warning: Update 2 of command 3 of module "Robot" doesn't do anything ("(s1'=13)", line 210, column 44)

Warning: Update 2 of command 27 of module "T" doesn't do anything ("(c'=1)", line 323, column 108)

Warning: Guard for command 28 of module "T" is never satisfied.

Warning: Update 2 of command 9 of module "T" doesn't do anything ("(c'=1)", line 300, column 112)

Warning: Guard for command 10 of module "T" is never satisfied.

Warning: Update 2 of command 51 of module "T" doesn't do anything ("(c'=1)", line 354, column 113)

Warning: Guard for command 52 of module "T" is never satisfied.

Computing reachable states...

Reachability (BFS): 91 iterations in 1.20 seconds (average 0.013231, setup 0.00)

Time for model construction: 2.672 seconds.

Warning: Deadlocks detected and fixed in 1 states

Type:        MDP
States:      300890 (1 initial)
Transitions: 825815
Choices:     402461

Transition matrix: 210739 nodes (97 terminal), 825815 minterms, vars: 34r/34c/18nd

Prob0A: 43 iterations in 0.34 seconds (average 0.007814, setup 0.00)

Prob1E: 4 iterations in 0.06 seconds (average 0.016000, setup 0.00)

yes = 1097, no = 253375, maybe = 46418

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=300890, nc=84430, nnz=249281, k=2] [3.2 MB]
Creating vector for yes... [2.3 MB]
Allocating iteration vectors... [2 x 2.3 MB]
TOTAL: [10.1 MB]

Starting iterations...

Iterative method: 92 iterations in 0.54 seconds (average 0.002870, setup 0.28)

Value in the initial state: 0.9842277027023452

Time for model checking: 0.986 seconds.

Result: 0.9842277027023452 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmin=? [ F "MissionSuccessful" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=5,Objy1=1,Objx2=6,Objy2=1

Prob0E: 45 iterations in 0.45 seconds (average 0.010044, setup 0.00)

Prob1A: 3 iterations in 0.04 seconds (average 0.013333, setup 0.00)

yes = 1097, no = 261730, maybe = 38063

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=300890, nc=68718, nnz=202496, k=2] [2.7 MB]
Creating vector for yes... [2.3 MB]
Allocating iteration vectors... [2 x 2.3 MB]
TOTAL: [9.6 MB]

Starting iterations...

Iterative method: 84 iterations in 0.34 seconds (average 0.002000, setup 0.18)

Value in the initial state: 0.9065573503289763

Time for model checking: 0.856 seconds.

Result: 0.9065573503289763 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmax=? [ F "MissionTimeUp" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=5,Objy1=1,Objx2=6,Objy2=1

Prob0A: 69 iterations in 0.53 seconds (average 0.007710, setup 0.00)

Prob1E: 4 iterations in 0.04 seconds (average 0.009000, setup 0.00)

yes = 460, no = 184968, maybe = 115462

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=300890, nc=205618, nnz=580303, k=4] [7.1 MB]
Creating vector for yes... [2.3 MB]
Allocating iteration vectors... [2 x 2.3 MB]
TOTAL: [14.0 MB]

Starting iterations...

Iterative method: 97 iterations in 0.58 seconds (average 0.002515, setup 0.33)

Value in the initial state: 6.235740282657803E-4

Time for model checking: 1.135 seconds.

Result: 6.235740282657803E-4 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmin=? [ F "MissionTimeUp" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=5,Objy1=1,Objx2=6,Objy2=1

Prob0E: 84 iterations in 0.84 seconds (average 0.009952, setup 0.00)

Prob1A: 3 iterations in 0.05 seconds (average 0.016000, setup 0.00)

yes = 460, no = 185470, maybe = 114960

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=300890, nc=204711, nnz=578529, k=4] [7.1 MB]
Creating vector for yes... [2.3 MB]
Allocating iteration vectors... [2 x 2.3 MB]
TOTAL: [14.0 MB]

Starting iterations...

Iterative method: 114 iterations in 0.61 seconds (average 0.002596, setup 0.31)

Value in the initial state: 7.373736668781885E-7

Time for model checking: 1.51 seconds.

Result: 7.373736668781885E-7 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmax=? [ F "Crash" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=5,Objy1=1,Objx2=6,Objy2=1

Prob0A: 3 iterations in 0.10 seconds (average 0.032000, setup 0.00)

Prob1E: 167 iterations in 2.36 seconds (average 0.014156, setup 0.00)

yes = 147103, no = 2958, maybe = 150829

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=300890, nc=252400, nnz=675754, k=4] [8.3 MB]
Creating vector for yes... [2.3 MB]
Allocating iteration vectors... [2 x 2.3 MB]
TOTAL: [15.1 MB]

Starting iterations...

Iterative method: 90 iterations in 0.61 seconds (average 0.002711, setup 0.37)

Value in the initial state: 0.02638936494590923

Time for model checking: 3.1 seconds.

Result: 0.02638936494590923 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmin=? [ F "Crash" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=5,Objy1=1,Objx2=6,Objy2=1

Prob0E: 4 iterations in 0.06 seconds (average 0.016000, setup 0.00)

Prob1A: 41 iterations in 0.47 seconds (average 0.011415, setup 0.00)

yes = 147103, no = 2958, maybe = 150829

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=300890, nc=252400, nnz=675754, k=4] [8.3 MB]
Creating vector for yes... [2.3 MB]
Allocating iteration vectors... [2 x 2.3 MB]
TOTAL: [15.1 MB]

Starting iterations...

Iterative method: 83 iterations in 0.56 seconds (average 0.002843, setup 0.32)

Value in the initial state: 0.013712770789755425

Time for model checking: 1.108 seconds.

Result: 0.013712770789755425 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmax=? [ F "AllAreaSearched" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=5,Objy1=1,Objx2=6,Objy2=1

Prob0A: 59 iterations in 0.42 seconds (average 0.007051, setup 0.00)

Prob1E: 4 iterations in 0.05 seconds (average 0.012000, setup 0.00)

yes = 1400, no = 240241, maybe = 59249

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=300890, nc=88749, nnz=232633, k=2] [3.0 MB]
Creating vector for yes... [2.3 MB]
Allocating iteration vectors... [2 x 2.3 MB]
TOTAL: [9.9 MB]

Starting iterations...

Iterative method: 96 iterations in 0.38 seconds (average 0.002000, setup 0.19)

Value in the initial state: 0.07156543868957464

Time for model checking: 0.865 seconds.

Result: 0.07156543868957464 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmin=? [ F "AllAreaSearched" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=5,Objy1=1,Objx2=6,Objy2=1

Prob0E: 65 iterations in 0.54 seconds (average 0.008308, setup 0.00)

Prob1A: 3 iterations in 0.04 seconds (average 0.012000, setup 0.00)

yes = 1400, no = 247538, maybe = 51952

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=300890, nc=75697, nnz=194473, k=2] [2.6 MB]
Creating vector for yes... [2.3 MB]
Allocating iteration vectors... [2 x 2.3 MB]
TOTAL: [9.5 MB]

Starting iterations...

Iterative method: 97 iterations in 0.34 seconds (average 0.001938, setup 0.15)

Value in the initial state: 0.0017982008641091608

Time for model checking: 0.935 seconds.

Result: 0.0017982008641091608 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmax=? [ F "MissionTimeUp"|"AllAreaSearched"|"Crash" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=5,Objy1=1,Objx2=6,Objy2=1

Prob0A: 3 iterations in 0.07 seconds (average 0.024000, setup 0.00)

Prob1E: 172 iterations in 2.01 seconds (average 0.011674, setup 0.00)

yes = 261729, no = 1098, maybe = 38063

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=300890, nc=68718, nnz=202496, k=2] [2.7 MB]
Creating vector for yes... [2.3 MB]
Allocating iteration vectors... [2 x 2.3 MB]
TOTAL: [9.6 MB]

Starting iterations...

Iterative method: 79 iterations in 0.34 seconds (average 0.001873, setup 0.20)

Value in the initial state: 0.09344248387033308

Time for model checking: 2.455 seconds.

Result: 0.09344248387033308 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmin=? [ F "MissionTimeUp"|"AllAreaSearched"|"Crash" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=5,Objy1=1,Objx2=6,Objy2=1

Prob0E: 4 iterations in 0.08 seconds (average 0.021000, setup 0.00)

Prob1A: 43 iterations in 0.36 seconds (average 0.008372, setup 0.00)

yes = 253374, no = 1098, maybe = 46418

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=300890, nc=84430, nnz=249281, k=2] [3.2 MB]
Creating vector for yes... [2.3 MB]
Allocating iteration vectors... [2 x 2.3 MB]
TOTAL: [10.1 MB]

Starting iterations...

Iterative method: 91 iterations in 0.37 seconds (average 0.001934, setup 0.19)

Value in the initial state: 0.01577110049454044

Time for model checking: 0.838 seconds.

Result: 0.01577110049454044 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"drop"}max=? [ F "deadlock" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=5,Objy1=1,Objx2=6,Objy2=1

Prob0E: 5 iterations in 0.07 seconds (average 0.013600, setup 0.00)

Prob1A: 1 iterations in 0.01 seconds (average 0.008000, setup 0.00)

goal = 1, inf = 0, maybe = 300889

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=300890, nc=402460, nnz=825814, k=4] [12.1 MB]
Building sparse matrix (transition rewards)... [n=300890, nc=402460, nnz=0, k=4] [2.7 MB]
Creating vector for state rewards... [2.3 MB]
Creating vector for inf... [2.3 MB]
Allocating iteration vectors... [2 x 2.3 MB]
TOTAL: [24.0 MB]

Starting iterations...

Iterative method: 87 iterations in 0.84 seconds (average 0.004276, setup 0.47)

Value in the initial state: 0.26801656780799266

Time for model checking: 0.931 seconds.

Result: 0.26801656780799266 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"drop"}min=? [ F "deadlock" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=5,Objy1=1,Objx2=6,Objy2=1

Prob0A: 4 iterations in 0.05 seconds (average 0.012000, setup 0.00)

Prob1E: 5 iterations in 0.05 seconds (average 0.009600, setup 0.00)

Warning: PRISM hasn't checked for zero-reward loops.
Your minimum rewards may be too low...

goal = 1, inf = 0, maybe = 300889

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=300890, nc=402460, nnz=825814, k=4] [12.1 MB]
Building sparse matrix (transition rewards)... [n=300890, nc=402460, nnz=0, k=4] [2.7 MB]
Creating vector for state rewards... [2.3 MB]
Creating vector for inf... [2.3 MB]
Allocating iteration vectors... [2 x 2.3 MB]
TOTAL: [24.0 MB]

Starting iterations...

Iterative method: 77 iterations in 0.73 seconds (average 0.004260, setup 0.40)

Value in the initial state: 0.24338660775307627

Time for model checking: 0.83 seconds.

Result: 0.24338660775307627 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"charge"}max=? [ F "deadlock" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=5,Objy1=1,Objx2=6,Objy2=1

Prob0E: 5 iterations in 0.03 seconds (average 0.006400, setup 0.00)

Prob1A: 1 iterations in 0.00 seconds (average 0.004000, setup 0.00)

goal = 1, inf = 0, maybe = 300889

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=300890, nc=402460, nnz=825814, k=4] [12.1 MB]
Building sparse matrix (transition rewards)... [n=300890, nc=402460, nnz=10074, k=4] [2.8 MB]
Creating vector for state rewards... [2.3 MB]
Creating vector for inf... [2.3 MB]
Allocating iteration vectors... [2 x 2.3 MB]
TOTAL: [24.1 MB]

Starting iterations...

Iterative method: 83 iterations in 0.75 seconds (average 0.004193, setup 0.40)

Value in the initial state: 0.22634557405944772

Time for model checking: 0.801 seconds.

Result: 0.22634557405944772 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"charge"}min=? [ F "deadlock" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=5,Objy1=1,Objx2=6,Objy2=1

Prob0A: 4 iterations in 0.03 seconds (average 0.007000, setup 0.00)

Prob1E: 5 iterations in 0.04 seconds (average 0.008800, setup 0.00)

Warning: PRISM hasn't checked for zero-reward loops.
Your minimum rewards may be too low...

goal = 1, inf = 0, maybe = 300889

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=300890, nc=402460, nnz=825814, k=4] [12.1 MB]
Building sparse matrix (transition rewards)... [n=300890, nc=402460, nnz=10074, k=4] [2.8 MB]
Creating vector for state rewards... [2.3 MB]
Creating vector for inf... [2.3 MB]
Allocating iteration vectors... [2 x 2.3 MB]
TOTAL: [24.1 MB]

Starting iterations...

Iterative method: 95 iterations in 0.81 seconds (average 0.004253, setup 0.41)

Value in the initial state: 0.014976025226365

Time for model checking: 0.895 seconds.

Result: 0.014976025226365 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"time"}max=? [ F "deadlock" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=5,Objy1=1,Objx2=6,Objy2=1

Prob0E: 5 iterations in 0.03 seconds (average 0.006400, setup 0.00)

Prob1A: 1 iterations in 0.00 seconds (average 0.004000, setup 0.00)

goal = 1, inf = 0, maybe = 300889

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=300890, nc=402460, nnz=825814, k=4] [12.1 MB]
Building sparse matrix (transition rewards)... [n=300890, nc=402460, nnz=150059, k=4] [4.4 MB]
Creating vector for state rewards... [2.3 MB]
Creating vector for inf... [2.3 MB]
Allocating iteration vectors... [2 x 2.3 MB]
TOTAL: [25.7 MB]

Starting iterations...

Iterative method: 94 iterations in 0.85 seconds (average 0.004255, setup 0.45)

Value in the initial state: 145.68483228866273

Time for model checking: 0.897 seconds.

Result: 145.68483228866273 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"time"}min=? [ F "deadlock" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=5,Objy1=1,Objx2=6,Objy2=1

Prob0A: 4 iterations in 0.02 seconds (average 0.006000, setup 0.00)

Prob1E: 5 iterations in 0.04 seconds (average 0.007200, setup 0.00)

Warning: PRISM hasn't checked for zero-reward loops.
Your minimum rewards may be too low...

goal = 1, inf = 0, maybe = 300889

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=300890, nc=402460, nnz=825814, k=4] [12.1 MB]
Building sparse matrix (transition rewards)... [n=300890, nc=402460, nnz=150059, k=4] [4.4 MB]
Creating vector for state rewards... [2.3 MB]
Creating vector for inf... [2.3 MB]
Allocating iteration vectors... [2 x 2.3 MB]
TOTAL: [25.7 MB]

Starting iterations...

Iterative method: 98 iterations in 0.86 seconds (average 0.004408, setup 0.43)

Value in the initial state: 76.09434460366639

Time for model checking: 0.95 seconds.

Result: 76.09434460366639 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"success"}max=? [ F "deadlock" ]/Pmin=? [ F "MissionSuccessful" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=5,Objy1=1,Objx2=6,Objy2=1

Prob0E: 5 iterations in 0.03 seconds (average 0.006400, setup 0.00)

Prob1A: 1 iterations in 0.00 seconds (average 0.004000, setup 0.00)

goal = 1, inf = 0, maybe = 300889

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=300890, nc=402460, nnz=825814, k=4] [12.1 MB]
Building sparse matrix (transition rewards)... [n=300890, nc=402460, nnz=2163, k=4] [2.7 MB]
Creating vector for state rewards... [2.3 MB]
Creating vector for inf... [2.3 MB]
Allocating iteration vectors... [2 x 2.3 MB]
TOTAL: [24.0 MB]

Starting iterations...

Iterative method: 93 iterations in 0.78 seconds (average 0.004172, setup 0.40)

Prob0E: 45 iterations in 0.27 seconds (average 0.006044, setup 0.00)

Prob1A: 3 iterations in 0.05 seconds (average 0.017333, setup 0.00)

yes = 1097, no = 261730, maybe = 38063

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=300890, nc=68718, nnz=202496, k=2] [2.7 MB]
Creating vector for yes... [2.3 MB]
Allocating iteration vectors... [2 x 2.3 MB]
TOTAL: [9.6 MB]

Starting iterations...

Iterative method: 84 iterations in 0.34 seconds (average 0.001857, setup 0.18)

Value in the initial state: 157.59292155662678

Time for model checking: 1.536 seconds.

Result: 157.59292155662678 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"success"}min=? [ F "deadlock" ]/Pmax=? [ F "MissionSuccessful" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=5,Objy1=1,Objx2=6,Objy2=1

Prob0A: 4 iterations in 0.10 seconds (average 0.024000, setup 0.00)

Prob1E: 5 iterations in 0.06 seconds (average 0.012000, setup 0.00)

Warning: PRISM hasn't checked for zero-reward loops.
Your minimum rewards may be too low...

goal = 1, inf = 0, maybe = 300889

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=300890, nc=402460, nnz=825814, k=4] [12.1 MB]
Building sparse matrix (transition rewards)... [n=300890, nc=402460, nnz=2163, k=4] [2.7 MB]
Creating vector for state rewards... [2.3 MB]
Creating vector for inf... [2.3 MB]
Allocating iteration vectors... [2 x 2.3 MB]
TOTAL: [24.0 MB]

Starting iterations...

Iterative method: 90 iterations in 1.00 seconds (average 0.006133, setup 0.44)

Prob0A: 43 iterations in 0.25 seconds (average 0.005767, setup 0.00)

Prob1E: 4 iterations in 0.04 seconds (average 0.010000, setup 0.00)

yes = 1097, no = 253375, maybe = 46418

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=300890, nc=84430, nnz=249281, k=2] [3.2 MB]
Creating vector for yes... [2.3 MB]
Allocating iteration vectors... [2 x 2.3 MB]
TOTAL: [10.1 MB]

Starting iterations...

Iterative method: 92 iterations in 0.37 seconds (average 0.002043, setup 0.18)

Value in the initial state: 71.60711096989033

Time for model checking: 1.834 seconds.

Result: 71.60711096989033 (value in the initial state)

---------------------------------------------------------------------

Note: There were 16 warnings during computation.

