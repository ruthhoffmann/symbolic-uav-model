PRISM
=====

Version: 4.3
Date: Wed Aug 03 15:54:11 BST 2016
Hostname: eglinton
Memory limits: cudd=1g, java(heap)=981.5m
Command line: prism uav_sim_mdp.prism -s uav_mdp.props -const 'Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=1,Objy1=0,Objx2=6,Objy2=2,'

Parsing model file "uav_sim_mdp.prism"...

Parsing properties file "uav_mdp.props"...

18 properties:
(1) Pmax=? [ F "MissionSuccessful" ]
(2) Pmin=? [ F "MissionSuccessful" ]
(3) Pmax=? [ F "MissionTimeUp" ]
(4) Pmin=? [ F "MissionTimeUp" ]
(5) Pmax=? [ F "Crash" ]
(6) Pmin=? [ F "Crash" ]
(7) Pmax=? [ F "AllAreaSearched" ]
(8) Pmin=? [ F "AllAreaSearched" ]
(9) Pmax=? [ F "MissionTimeUp"|"AllAreaSearched"|"Crash" ]
(10) Pmin=? [ F "MissionTimeUp"|"AllAreaSearched"|"Crash" ]
(11) R{"drop"}max=? [ F "deadlock" ]
(12) R{"drop"}min=? [ F "deadlock" ]
(13) R{"charge"}max=? [ F "deadlock" ]
(14) R{"charge"}min=? [ F "deadlock" ]
(15) R{"time"}max=? [ F "deadlock" ]
(16) R{"time"}min=? [ F "deadlock" ]
(17) R{"success"}max=? [ F "deadlock" ]/Pmin=? [ F "MissionSuccessful" ]
(18) R{"success"}min=? [ F "deadlock" ]/Pmax=? [ F "MissionSuccessful" ]

Type:        MDP
Modules:     Obj Move Robot T 
Variables:   NoOfObjs posx posy s1 retx rety b t c 

---------------------------------------------------------------------

Model checking: Pmax=? [ F "MissionSuccessful" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=1,Objy1=0,Objx2=6,Objy2=2

Building model...
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=1,Objy1=0,Objx2=6,Objy2=2

Warning: Guard for command 7 of module "Move" is never satisfied.

Warning: Guard for command 8 of module "Move" is never satisfied.

Warning: Guard for command 9 of module "Move" is never satisfied.

Warning: Guard for command 7 of module "Robot" is never satisfied.

Warning: Update 2 of command 3 of module "Robot" doesn't do anything ("(s1'=13)", line 210, column 44)

Warning: Update 2 of command 27 of module "T" doesn't do anything ("(c'=1)", line 323, column 108)

Warning: Guard for command 28 of module "T" is never satisfied.

Warning: Update 2 of command 9 of module "T" doesn't do anything ("(c'=1)", line 300, column 112)

Warning: Guard for command 10 of module "T" is never satisfied.

Warning: Update 2 of command 51 of module "T" doesn't do anything ("(c'=1)", line 354, column 113)

Warning: Guard for command 52 of module "T" is never satisfied.

Computing reachable states...

Reachability (BFS): 89 iterations in 1.93 seconds (average 0.021663, setup 0.00)

Time for model construction: 4.675 seconds.

Warning: Deadlocks detected and fixed in 1 states

Type:        MDP
States:      367860 (1 initial)
Transitions: 989441
Choices:     487517

Transition matrix: 234910 nodes (97 terminal), 989441 minterms, vars: 34r/34c/18nd

Prob0A: 61 iterations in 0.72 seconds (average 0.011803, setup 0.00)

Prob1E: 4 iterations in 0.09 seconds (average 0.023000, setup 0.00)

yes = 1248, no = 313096, maybe = 53516

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=367860, nc=92154, nnz=264406, k=2] [3.5 MB]
Creating vector for yes... [2.8 MB]
Allocating iteration vectors... [2 x 2.8 MB]
TOTAL: [11.9 MB]

Starting iterations...

Iterative method: 103 iterations in 0.60 seconds (average 0.002757, setup 0.32)

Value in the initial state: 0.9797675843347594

Time for model checking: 1.516 seconds.

Result: 0.9797675843347594 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmin=? [ F "MissionSuccessful" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=1,Objy1=0,Objx2=6,Objy2=2

Prob0E: 63 iterations in 0.65 seconds (average 0.010349, setup 0.00)

Prob1A: 3 iterations in 0.06 seconds (average 0.020000, setup 0.00)

yes = 1248, no = 320252, maybe = 46360

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=367860, nc=80009, nnz=229796, k=2] [3.1 MB]
Creating vector for yes... [2.8 MB]
Allocating iteration vectors... [2 x 2.8 MB]
TOTAL: [11.5 MB]

Starting iterations...

Iterative method: 95 iterations in 0.47 seconds (average 0.002611, setup 0.22)

Value in the initial state: 0.7882084342676987

Time for model checking: 1.306 seconds.

Result: 0.7882084342676987 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmax=? [ F "MissionTimeUp" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=1,Objy1=0,Objx2=6,Objy2=2

Prob0A: 69 iterations in 0.96 seconds (average 0.013855, setup 0.00)

Prob1E: 4 iterations in 0.06 seconds (average 0.015000, setup 0.00)

yes = 572, no = 209758, maybe = 157530

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=367860, nc=263424, nnz=721401, k=4] [8.9 MB]
Creating vector for yes... [2.8 MB]
Allocating iteration vectors... [2 x 2.8 MB]
TOTAL: [17.3 MB]

Starting iterations...

Iterative method: 100 iterations in 0.79 seconds (average 0.003520, setup 0.44)

Value in the initial state: 8.314872350094904E-4

Time for model checking: 2.089 seconds.

Result: 8.314872350094904E-4 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmin=? [ F "MissionTimeUp" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=1,Objy1=0,Objx2=6,Objy2=2

Prob0E: 81 iterations in 1.40 seconds (average 0.017235, setup 0.00)

Prob1A: 3 iterations in 0.06 seconds (average 0.020000, setup 0.00)

yes = 572, no = 210152, maybe = 157136

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=367860, nc=262717, nnz=720045, k=4] [8.8 MB]
Creating vector for yes... [2.8 MB]
Allocating iteration vectors... [2 x 2.8 MB]
TOTAL: [17.3 MB]

Starting iterations...

Iterative method: 115 iterations in 0.82 seconds (average 0.003443, setup 0.43)

Value in the initial state: 2.9207669005710927E-6

Time for model checking: 2.342 seconds.

Result: 2.9207669005710927E-6 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmax=? [ F "Crash" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=1,Objy1=0,Objx2=6,Objy2=2

Prob0A: 3 iterations in 0.10 seconds (average 0.032000, setup 0.00)

Prob1E: 217 iterations in 4.56 seconds (average 0.021014, setup 0.00)

yes = 180079, no = 3535, maybe = 184246

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=367860, nc=303903, nnz=805827, k=4] [9.9 MB]
Creating vector for yes... [2.8 MB]
Allocating iteration vectors... [2 x 2.8 MB]
TOTAL: [18.3 MB]

Starting iterations...

Iterative method: 96 iterations in 0.87 seconds (average 0.003625, setup 0.52)

Value in the initial state: 0.03288387330718461

Time for model checking: 5.729 seconds.

Result: 0.03288387330718461 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmin=? [ F "Crash" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=1,Objy1=0,Objx2=6,Objy2=2

Prob0E: 4 iterations in 0.12 seconds (average 0.029000, setup 0.00)

Prob1A: 53 iterations in 1.00 seconds (average 0.018943, setup 0.00)

yes = 180079, no = 3535, maybe = 184246

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=367860, nc=303903, nnz=805827, k=4] [9.9 MB]
Creating vector for yes... [2.8 MB]
Allocating iteration vectors... [2 x 2.8 MB]
TOTAL: [18.3 MB]

Starting iterations...

Iterative method: 101 iterations in 0.84 seconds (average 0.003604, setup 0.48)

Value in the initial state: 0.018055328065037807

Time for model checking: 2.042 seconds.

Result: 0.018055328065037807 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmax=? [ F "AllAreaSearched" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=1,Objy1=0,Objx2=6,Objy2=2

Prob0A: 65 iterations in 0.58 seconds (average 0.008985, setup 0.00)

Prob1E: 4 iterations in 0.06 seconds (average 0.016000, setup 0.00)

yes = 1714, no = 290119, maybe = 76027

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=367860, nc=123284, nnz=340425, k=2] [4.4 MB]
Creating vector for yes... [2.8 MB]
Allocating iteration vectors... [2 x 2.8 MB]
TOTAL: [12.8 MB]

Starting iterations...

Iterative method: 93 iterations in 0.53 seconds (average 0.002968, setup 0.25)

Value in the initial state: 0.1846685555501803

Time for model checking: 1.317 seconds.

Result: 0.1846685555501803 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmin=? [ F "AllAreaSearched" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=1,Objy1=0,Objx2=6,Objy2=2

Prob0E: 69 iterations in 0.66 seconds (average 0.009565, setup 0.00)

Prob1A: 3 iterations in 0.06 seconds (average 0.020000, setup 0.00)

yes = 1714, no = 300114, maybe = 66032

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=367860, nc=106959, nnz=295164, k=2] [3.8 MB]
Creating vector for yes... [2.8 MB]
Allocating iteration vectors... [2 x 2.8 MB]
TOTAL: [12.3 MB]

Starting iterations...

Iterative method: 99 iterations in 0.49 seconds (average 0.002909, setup 0.20)

Value in the initial state: 0.0017416441127410304

Time for model checking: 1.279 seconds.

Result: 0.0017416441127410304 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmax=? [ F "MissionTimeUp"|"AllAreaSearched"|"Crash" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=1,Objy1=0,Objx2=6,Objy2=2

Prob0A: 3 iterations in 0.12 seconds (average 0.038667, setup 0.00)

Prob1E: 244 iterations in 5.33 seconds (average 0.021836, setup 0.00)

yes = 320251, no = 1249, maybe = 46360

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=367860, nc=80009, nnz=229796, k=2] [3.1 MB]
Creating vector for yes... [2.8 MB]
Allocating iteration vectors... [2 x 2.8 MB]
TOTAL: [11.5 MB]

Starting iterations...

Iterative method: 88 iterations in 0.54 seconds (average 0.002682, setup 0.30)

Value in the initial state: 0.21179128566909644

Time for model checking: 6.473 seconds.

Result: 0.21179128566909644 (value in the initial state)

---------------------------------------------------------------------

Model checking: Pmin=? [ F "MissionTimeUp"|"AllAreaSearched"|"Crash" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=1,Objy1=0,Objx2=6,Objy2=2

Prob0E: 4 iterations in 0.11 seconds (average 0.028000, setup 0.00)

Prob1A: 61 iterations in 1.02 seconds (average 0.016721, setup 0.00)

yes = 313095, no = 1249, maybe = 53516

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=367860, nc=92154, nnz=264406, k=2] [3.5 MB]
Creating vector for yes... [2.8 MB]
Allocating iteration vectors... [2 x 2.8 MB]
TOTAL: [11.9 MB]

Starting iterations...

Iterative method: 100 iterations in 0.54 seconds (average 0.002680, setup 0.28)

Value in the initial state: 0.020231723354686575

Time for model checking: 1.883 seconds.

Result: 0.020231723354686575 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"drop"}max=? [ F "deadlock" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=1,Objy1=0,Objx2=6,Objy2=2

Prob0E: 5 iterations in 0.16 seconds (average 0.031200, setup 0.00)

Prob1A: 1 iterations in 0.01 seconds (average 0.012000, setup 0.00)

goal = 1, inf = 0, maybe = 367859

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=367860, nc=487516, nnz=989440, k=4] [14.6 MB]
Building sparse matrix (transition rewards)... [n=367860, nc=487516, nnz=0, k=4] [3.3 MB]
Creating vector for state rewards... [2.8 MB]
Creating vector for inf... [2.8 MB]
Allocating iteration vectors... [2 x 2.8 MB]
TOTAL: [29.1 MB]

Starting iterations...

Iterative method: 97 iterations in 1.15 seconds (average 0.005443, setup 0.62)

Value in the initial state: 0.28320168985504796

Time for model checking: 1.468 seconds.

Result: 0.28320168985504796 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"drop"}min=? [ F "deadlock" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=1,Objy1=0,Objx2=6,Objy2=2

Prob0A: 4 iterations in 0.05 seconds (average 0.012000, setup 0.00)

Prob1E: 5 iterations in 0.07 seconds (average 0.014400, setup 0.00)

Warning: PRISM hasn't checked for zero-reward loops.
Your minimum rewards may be too low...

goal = 1, inf = 0, maybe = 367859

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=367860, nc=487516, nnz=989440, k=4] [14.6 MB]
Building sparse matrix (transition rewards)... [n=367860, nc=487516, nnz=0, k=4] [3.3 MB]
Creating vector for state rewards... [2.8 MB]
Creating vector for inf... [2.8 MB]
Allocating iteration vectors... [2 x 2.8 MB]
TOTAL: [29.1 MB]

Starting iterations...

Iterative method: 87 iterations in 0.96 seconds (average 0.005333, setup 0.49)

Value in the initial state: 0.2323826761680529

Time for model checking: 1.107 seconds.

Result: 0.2323826761680529 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"charge"}max=? [ F "deadlock" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=1,Objy1=0,Objx2=6,Objy2=2

Prob0E: 5 iterations in 0.04 seconds (average 0.008800, setup 0.00)

Prob1A: 1 iterations in 0.01 seconds (average 0.008000, setup 0.00)

goal = 1, inf = 0, maybe = 367859

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=367860, nc=487516, nnz=989440, k=4] [14.6 MB]
Building sparse matrix (transition rewards)... [n=367860, nc=487516, nnz=11551, k=4] [3.4 MB]
Creating vector for state rewards... [2.8 MB]
Creating vector for inf... [2.8 MB]
Allocating iteration vectors... [2 x 2.8 MB]
TOTAL: [29.2 MB]

Starting iterations...

Iterative method: 95 iterations in 1.00 seconds (average 0.005389, setup 0.49)

Value in the initial state: 0.3283400775883286

Time for model checking: 1.066 seconds.

Result: 0.3283400775883286 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"charge"}min=? [ F "deadlock" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=1,Objy1=0,Objx2=6,Objy2=2

Prob0A: 4 iterations in 0.03 seconds (average 0.008000, setup 0.00)

Prob1E: 5 iterations in 0.06 seconds (average 0.012000, setup 0.00)

Warning: PRISM hasn't checked for zero-reward loops.
Your minimum rewards may be too low...

goal = 1, inf = 0, maybe = 367859

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=367860, nc=487516, nnz=989440, k=4] [14.6 MB]
Building sparse matrix (transition rewards)... [n=367860, nc=487516, nnz=11551, k=4] [3.4 MB]
Creating vector for state rewards... [2.8 MB]
Creating vector for inf... [2.8 MB]
Allocating iteration vectors... [2 x 2.8 MB]
TOTAL: [29.2 MB]

Starting iterations...

Iterative method: 103 iterations in 1.05 seconds (average 0.005437, setup 0.49)

Value in the initial state: 0.07251965230202446

Time for model checking: 1.154 seconds.

Result: 0.07251965230202446 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"time"}max=? [ F "deadlock" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=1,Objy1=0,Objx2=6,Objy2=2

Prob0E: 5 iterations in 0.04 seconds (average 0.008000, setup 0.00)

Prob1A: 1 iterations in 0.01 seconds (average 0.008000, setup 0.00)

goal = 1, inf = 0, maybe = 367859

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=367860, nc=487516, nnz=989440, k=4] [14.6 MB]
Building sparse matrix (transition rewards)... [n=367860, nc=487516, nnz=183612, k=4] [5.4 MB]
Creating vector for state rewards... [2.8 MB]
Creating vector for inf... [2.8 MB]
Allocating iteration vectors... [2 x 2.8 MB]
TOTAL: [31.2 MB]

Starting iterations...

Iterative method: 99 iterations in 1.08 seconds (average 0.005495, setup 0.54)

Value in the initial state: 181.3148807735556

Time for model checking: 1.149 seconds.

Result: 181.3148807735556 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"time"}min=? [ F "deadlock" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=1,Objy1=0,Objx2=6,Objy2=2

Prob0A: 4 iterations in 0.03 seconds (average 0.008000, setup 0.00)

Prob1E: 5 iterations in 0.06 seconds (average 0.012800, setup 0.00)

Warning: PRISM hasn't checked for zero-reward loops.
Your minimum rewards may be too low...

goal = 1, inf = 0, maybe = 367859

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=367860, nc=487516, nnz=989440, k=4] [14.6 MB]
Building sparse matrix (transition rewards)... [n=367860, nc=487516, nnz=183612, k=4] [5.4 MB]
Creating vector for state rewards... [2.8 MB]
Creating vector for inf... [2.8 MB]
Allocating iteration vectors... [2 x 2.8 MB]
TOTAL: [31.2 MB]

Starting iterations...

Iterative method: 107 iterations in 1.12 seconds (average 0.005495, setup 0.54)

Value in the initial state: 100.01187197540764

Time for model checking: 1.24 seconds.

Result: 100.01187197540764 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"success"}max=? [ F "deadlock" ]/Pmin=? [ F "MissionSuccessful" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=1,Objy1=0,Objx2=6,Objy2=2

Prob0E: 5 iterations in 0.04 seconds (average 0.007200, setup 0.00)

Prob1A: 1 iterations in 0.01 seconds (average 0.008000, setup 0.00)

goal = 1, inf = 0, maybe = 367859

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=367860, nc=487516, nnz=989440, k=4] [14.6 MB]
Building sparse matrix (transition rewards)... [n=367860, nc=487516, nnz=2458, k=4] [3.3 MB]
Creating vector for state rewards... [2.8 MB]
Creating vector for inf... [2.8 MB]
Allocating iteration vectors... [2 x 2.8 MB]
TOTAL: [29.1 MB]

Starting iterations...

Iterative method: 100 iterations in 1.01 seconds (average 0.005360, setup 0.48)

Prob0E: 63 iterations in 0.36 seconds (average 0.005778, setup 0.00)

Prob1A: 3 iterations in 0.05 seconds (average 0.017333, setup 0.00)

yes = 1248, no = 320252, maybe = 46360

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=367860, nc=80009, nnz=229796, k=2] [3.1 MB]
Creating vector for yes... [2.8 MB]
Allocating iteration vectors... [2 x 2.8 MB]
TOTAL: [11.5 MB]

Starting iterations...

Iterative method: 95 iterations in 0.43 seconds (average 0.002400, setup 0.20)

Value in the initial state: 225.17486434304308

Time for model checking: 1.947 seconds.

Result: 225.17486434304308 (value in the initial state)

---------------------------------------------------------------------

Model checking: R{"success"}min=? [ F "deadlock" ]/Pmax=? [ F "MissionSuccessful" ]
Model constants: Basex=0,Basey=0,Depotx=2,Depoty=3,Objx1=1,Objy1=0,Objx2=6,Objy2=2

Prob0A: 4 iterations in 0.10 seconds (average 0.024000, setup 0.00)

Prob1E: 5 iterations in 0.04 seconds (average 0.008800, setup 0.00)

Warning: PRISM hasn't checked for zero-reward loops.
Your minimum rewards may be too low...

goal = 1, inf = 0, maybe = 367859

Computing remaining rewards...
Engine: Sparse

Building sparse matrix (transitions)... [n=367860, nc=487516, nnz=989440, k=4] [14.6 MB]
Building sparse matrix (transition rewards)... [n=367860, nc=487516, nnz=2458, k=4] [3.3 MB]
Creating vector for state rewards... [2.8 MB]
Creating vector for inf... [2.8 MB]
Allocating iteration vectors... [2 x 2.8 MB]
TOTAL: [29.1 MB]

Starting iterations...

Iterative method: 100 iterations in 0.97 seconds (average 0.004960, setup 0.48)

Prob0A: 61 iterations in 0.32 seconds (average 0.005311, setup 0.00)

Prob1E: 4 iterations in 0.06 seconds (average 0.015000, setup 0.00)

yes = 1248, no = 313096, maybe = 53516

Computing remaining probabilities...
Engine: Sparse

Building sparse matrix... [n=367860, nc=92154, nnz=264406, k=2] [3.5 MB]
Creating vector for yes... [2.8 MB]
Allocating iteration vectors... [2 x 2.8 MB]
TOTAL: [11.9 MB]

Starting iterations...

Iterative method: 103 iterations in 0.55 seconds (average 0.002447, setup 0.30)

Value in the initial state: 79.13227306329593

Time for model checking: 2.086 seconds.

Result: 79.13227306329593 (value in the initial state)

---------------------------------------------------------------------

Note: There were 16 warnings during computation.

