#!/usr/bin/python

import subprocess

NOPROPS = 18
X = 7
Y = 4
grid = [[0 for x in range(2)] for x in range(X*Y)]
count = 0
for i in range(X):
    for j in range(Y):
        grid[count][0] = i
        grid[count][1] = j
        count += 1

# Theoretical corners
#for i in range(4):
#    grid.append([X+i,Y+i])


for b in range(len(grid)-4):
    tmpout = [[] for x in range(NOPROPS)]
    outfile = open("summary-%d%d.txt"%(grid[b][0],grid[b][1]), "w")
    tmpfile = open("tmp-%d%d.txt"%(grid[b][0],grid[b][1]),"w")
    depot = range(len(grid)-4)
    del depot[b]
    for d in depot:
        for i in range(len(grid)-1):
            if i != b:
                for j in range(i+1,len(grid)):
                    if j != b:
                        grarg = ["grep", "Result", "./results/results-%d%d/result-%d%d-%d%d-%d%d-%d%d.txt"%(grid[b][0], grid[b][1], grid[b][0], grid[b][1], grid[d][0], grid[d][1], grid[i][0], grid[i][1], grid[j][0], grid[j][1])]
                        p1 = subprocess.Popen(grarg, stdout=tmpfile)

    tmpfile.close()
    tmpfile = open("tmp-%d%d.txt"%(grid[b][0],grid[b][1]),"r")
    count = 0
    for line in tmpfile:
        fields = line.strip().split()
        tmpout[count%NOPROPS].append(fields[1])
        count += 1
    tmpfile.close()

    for x in range(NOPROPS):
        for y in range(len(tmpout[x])):
            if y < len(tmpout[x])-1 :
                outfile.write(tmpout[x][y] + " , ")
            else:
                outfile.write(tmpout[x][y])
        outfile.write("\n")
    outfile.close()

# First evaluations, basic averages.
    res = []
    for i in range(len(tmpout)):
        res.append(sum(float(j) for j in tmpout[i]))
        res[i] = res[i]/len(tmpout[i])

    resfile = open("result-%d%d.txt"%(grid[b][0],grid[b][1]),"w")
    for i in range(len(res)):
        resfile.write(str(i+1) + " " + str(res[i]) + "\n")
